<style type="text/css">
  .zoomContainer{ z-index: 9999;}
  .zoomWindow{ z-index: 9999;}
</style>
<script type="text/javascript">
$(document).ready(function() {
  /* Setup jQuery datepicker ui */
  $('#SettingFyStart').datepicker({
    dateFormat: $("#SettingDateFormat").val().split('|')[1],  /* Read the Javascript date format value */
    numberOfMonths: 1,
    onClose: function(selectedDate) {
      $("#SettingFyEnd").datepicker("option", "minDate", selectedDate);
    }
  });
  $('#SettingFyEnd').datepicker({
    dateFormat: $("#SettingDateFormat").val().split('|')[1],  /* Read the Javascript date format value */
    numberOfMonths: 1,
    onClose: function(selectedDate) {
      $("#SettingFyStart").datepicker("option", "maxDate", selectedDate);
    }
  });

  $("#SettingDateFormat").change(function() {
    /* Read the Javascript date format value */
    dateFormat = $(this).val().split('|')[1];
    $("#SettingFyStart").datepicker("option", "dateFormat", dateFormat);
    $("#SettingFyEnd").datepicker("option", "dateFormat", dateFormat);
  });
});
</script>
<div class="row wrapper border-bottom white-bg page-heading">
  <div class="col-lg-8">
    <h2><?php echo $page_title; ?></h2>
    <ol class="breadcrumb">
      <li>
        <a href="login/index">Inicio</a> <?php //En el controlador en la función llamada, se revisa si el usuario está logueado y lo envía a la vista principal según permisos ?>
      </li>
      <li class="active">
        <strong><?php echo $page_title; ?></strong>
      </li>
    </ol>
  </div><!-- /.col -->
  <div class="col-lg-4">
    <div class="title-action">
    </div>
  </div>
</div><!-- /.row -->   
<div class="row wrapper wrapper-content animated fadeInRight">
  <div class="box float-e-margins">
    <div class="ibox-content contentBackground">
    <?= form_open(); ?>
        <?php  
        $data = array(
          'class' => 'form-control',
        ); 
        ?>
        <div class="row">
            <div class="form-group col-sm-4">
              <?php  
              $data['name'] = 'name';
              $data['value'] = set_value('name', $this->mAccountSettings->name);
              echo form_label(lang('settings_views_main_label_company_name'), 'name');
              echo form_input($data);
              ?>
            </div>
            <div class="form-group col-sm-4">
              <?php  
              $data['name'] = 'address';
              $data['value'] = set_value('address', $this->mAccountSettings->address);
              echo form_label(lang('settings_views_main_label_address'), 'address');
              echo form_input($data);
              ?>
            </div>
            <div class="form-group col-sm-4">
              <?php  
              $data['name'] = 'email';
              $data['value'] = set_value('email', $this->mAccountSettings->email);
              echo form_label(lang('settings_views_main_label_email'), 'email');
              echo form_input($data);
              ?>
            </div>
            <div class="form-group col-sm-4">
              <?php  
              $data['name'] = 'currency_symbol';
              $data['value'] = set_value('currency_symbol', $this->mAccountSettings->currency_symbol);
              echo form_label(lang('settings_views_main_label_currency_symbol'), 'currency_symbol');
              echo form_input($data);
              ?>
            </div>
            <div class="form-group col-sm-4">
              <?php
              $currency_format = array(
                '##,###.##' => '##,###.##',
                '##,##.##' => '##,##.##',
                '###,###.##' => '###,###.##',
              );
              echo form_label(lang('settings_views_main_label_currency_format'), 'currency_format');
              echo form_dropdown('currency_format', $currency_format, set_value('currency_format', $this->mAccountSettings->currency_format) ,array('class'=>'form-control'));
              ?>
            </div>
            <div class="form-group col-sm-4">
              <?php
              $date_format = array(
                'd-M-Y|dd-M-yy' => lang('settings_views_main_date_format_1'),
                'M-d-Y|M-dd-yy' => lang('settings_views_main_date_format_2'),
                'Y-M-d|yy-M-dd' => lang('settings_views_main_date_format_3'),
              );
              echo form_label(lang('settings_views_main_label_date_format'), 'date_format');
              echo form_dropdown('date_format', $date_format, set_value('date_format', $this->mAccountSettings->date_format) ,array('class'=>'form-control', 'id'=>'SettingDateFormat'));
              ?>
            </div>
            <div class="form-group col-sm-4">
              <?php 
              $data['name'] = 'fy_start';
              $data['value'] = set_value('fy_start', $this->functionscore->dateFromSql($this->mAccountSettings->fy_start));
              $data['id'] = 'SettingFyStart';
              echo form_label(lang('settings_views_main_label_financial_year_start'), 'fy_start');
              echo form_input($data);
              ?>
            </div>
            <div class="form-group col-sm-4">
              <?php 
              $data['name'] = 'fy_end';
              $data['value'] = set_value('fy_end', $this->functionscore->dateFromSql($this->mAccountSettings->fy_end));
              $data['id'] = 'SettingFyEnd';
              echo form_label(lang('settings_views_main_label_financial_year_end'), 'fy_end');
              echo form_input($data);
              ?>
            </div>
            <div class="form-group col-sm-4">
              <?php 
              $data['name'] = 'nit';
              $data['value'] = set_value('nit', $this->mAccountSettings->nit);
              $data['id'] = 'SettingNit';
              echo form_label(lang('settings_views_main_label_nit'), 'nit');
              echo form_input($data);
              ?>
            </div>
            <div class="form-group col-sm-4">
              <?php 
              $data['name'] = 'phone';
              $data['value'] = set_value('phone', $this->mAccountSettings->phone);
              $data['id'] = 'SettingPhone';
              echo form_label(lang('settings_views_main_label_phone'), 'phone');
              echo form_input($data);
              ?>
            </div>

            <div class="form-group col-sm-4">
              <?= lang('account_parameter_method', 'account_parameter_method') ?>
              <select name="account_parameter_method" id="account_parameter_method" class="form-control">
                <option value="1" <?= $this->mAccountSettings->account_parameter_method == 1 ? 'selected="selected"' : '' ?> ><?= lang('account_parameter_method_tax') ?></option>
                <option value="2" <?= $this->mAccountSettings->account_parameter_method == 2 ? 'selected="selected"' : '' ?>><?= lang('account_parameter_method_category') ?></option>
              </select>
            </div>

            <div class="form-group col-sm-4">
              <?= lang('default_loading_accounts_index', 'default_loading_accounts_index') ?>
              <select name="default_loading_accounts_index" id="default_loading_accounts_index" class="form-control">
                <option value="1" <?= $this->mAccountSettings->default_loading_accounts_index == 1 ? 'selected="selected"' : '' ?> ><?= lang('with_closing_balance') ?></option>
                <option value="2" <?= $this->mAccountSettings->default_loading_accounts_index == 2 ? 'selected="selected"' : '' ?>><?= lang('without_closing_balance') ?></option>
              </select>
            </div>

            <div class="clearfix"></div>

            <div class="form-group col-sm-4" style="margin-top: 30px; text-align: center;">
              <button type="button" class="btn btn-success" data-toggle="modal" data-target="#logo" style="width: 80%;"> <span class="fa fa-upload"></span> <?= lang('settings_views_main_label_update_logo'); ?></button>
            </div>

            <hr class="col-sm-11">
            <div class="col-sm-12">
              <h3><?= lang('signatures') ?></h3>
            </div>

            <div class="form-group col-sm-3" style="padding-bottom: 2%;">
                <?= lang('create_account_accountant_name', 'accountant_name'); ?>
                <input type="text" name="accountant_name" id="accountant_name" class="form-control" value="<?= ($this->mAccountSettings->accountant_name) ?>">
            </div>

            <div class="form-group col-sm-3" style="padding-bottom: 2%;">
                <?= lang('create_account_fiscal_name', 'fiscal_name'); ?>
                <input type="text" name="fiscal_name" id="fiscal_name" class="form-control" value="<?= ($this->mAccountSettings->fiscal_name) ?>">
            </div>

            <div class="form-group col-sm-3" style="padding-bottom: 2%;">
                <?= lang('create_account_representant_name', 'representant_name'); ?>
                <input type="text" name="representant_name" id="representant_name" class="form-control" value="<?= ($this->mAccountSettings->representant_name) ?>">
            </div>

            <hr class="col-sm-11">

            <div class="form-group col-sm-3" style="padding-bottom: 2%;">
              <label>
                <input type="checkbox" name="modulary" class="form-control" <?= ($this->mAccountSettings->modulary) ? "checked" : "" ?>>
              <?= lang('create_account_pos_modulary'); ?></label>
            </div>
            <div class="form-group col-sm-3" style="padding-bottom: 2%;">
              <label>
                <input type="checkbox" name="cost_center" class="form-control" <?= ($this->mAccountSettings->cost_center) ? "checked" : "" ?>>
              <?= lang('create_account_cost_center'); ?></label>
            </div>
        </div>
        <div class="form-group col-sm-4">
          <?php
          echo form_submit('submit', lang('settings_views_main_label_submit'), array('class'=> 'btn btn-success'));
          ?>
          <a href="<?= base_url() ?>dashboard/index" class="btn btn-default"><?= lang('ledgers_views_edit_label_cancel_btn') ?></a>
        </div>
      </div>
    <?= form_close(); ?>
  </div>
</div>
<!-- /.content -->

<div class="modal fade" tabindex="-1" role="dialog" id="logo" aria-labelledby="updateLogo">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><?= lang('settings_views_main_modal_title'); ?></h4>
      </div>
      <?php $attributes = array('id' => 'updateLogo');
          echo form_open_multipart('' ,$attributes); ?>
        <div class="modal-body">
          <div class="msg">
          </div>
          <div class="row">
            <div class="col-xs-12">
              <div style="margin-left: 20px;">
                  <label><?= lang('settings_views_main_modal_label_select_image'); ?></label>
                  <input type="file" name="companylogoupdate" id="companylogoupdate">
              </div>  
              <div style="margin-top: 20px; text-align: center;">
                <img id="previewImage" src="" style="max-width: 100%; height: auto;" />
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal"><?= lang('settings_views_main_modal_btn_close'); ?></button>
          <button type="submit" name="uploadimage" class="btn btn-primary"><?= lang('settings_views_main_modal_btn_upload'); ?></button>
        </div>
      <?php echo form_close(); ?>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
  function readURL(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#previewImage').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

$("#companylogoupdate").change(function(){
    readURL(this);
});

$('#updateLogo').submit(function(event){  
  event.preventDefault();
  var label = '<?= $_SESSION['active_account']->label; ?>';
  var data = new FormData();
  jQuery.each(jQuery('#companylogoupdate')[0].files, function(i, file) {
    data.append('companylogoupdate', file);
  });
  jQuery.ajax({  
    url:"<?= base_url(); ?>account_settings/updateLogo/"+label,  
    data: data,
    cache: false,
    contentType: false,
    processData: false,
    dataType: "json",
    type: 'POST',
    success:function(data){
      var msg = '';
      if(data){
        if (data.status == 'success') {
          msg = '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert">&times;</a><?= lang('strong_success_label'); ?>'+ data.msg +'</div><br>';
        }else{
          msg = '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert">&times;</a><?= lang('strong_error_label'); ?>'+ data.msg +'</div><br>';
        }      
      }
      $('.msg').html(msg);
      $('#logo').animate({ scrollTop: 0 }, 'fast');
    }  
  });  
});
</script>