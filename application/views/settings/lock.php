<?php
// echo "<pre>";
// print_r($this);
// echo "</pre>";
// die();
?>
<div class="row wrapper wrapper-content animated fadeInRight">
      <div class="box float-e-margins">
        <div class="ibox-content contentBackground">
          <div class="col-sm-12">
        <div class="box-header with-border">
          <h3 class="box-title"><?= lang('lock_account_title'); ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <?= form_open(); ?>
            <div class="form-group">
              <input type="hidden" value="0" name="locked">
              <label><input type="checkbox" value="1" name="locked" <?= ($locked) ? 'checked' : '' ?>> <?= lang('lock_account_btn'); ?></label>
              <small><?= lang('lock_account_span'); ?></small>
            </div>
            <div class="form-group">
            <?php
            echo form_submit('submit', 'Enviar', array('class'=> 'btn btn-success'));
            ?>
              <a href="<?= base_url() ?>dashboard/index" class="btn btn-default"><?= lang('ledgers_views_edit_label_cancel_btn') ?></a>
            </div>
          <?= form_close(); ?>
        </div>
      </div>
  </div>
  <!-- /.row -->
</div>
<!-- /.content -->