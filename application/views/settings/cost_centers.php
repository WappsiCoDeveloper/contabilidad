<div class="row wrapper border-bottom white-bg page-heading">
  <div class="col-lg-8">
    <h2><?php echo $page_title; ?></h2>
    <ol class="breadcrumb">
      <li>
        <a href="login/index">Inicio</a> <?php //En el controlador en la función llamada, se revisa si el usuario está logueado y lo envía a la vista principal según permisos ?>
      </li>
      <li class="active">
        <strong><?php echo $page_title; ?></strong>
      </li>
    </ol>
  </div><!-- /.col -->
  <div class="col-lg-4">
    <div class="title-action">
      <button class="btn btn-success" href="#modalAddCostCenter" data-toggle="modal" id="addcostcenter"><span class="fa fa-plus "></span> <?= lang('cost_centers_index_add'); ?></button>
    </div>
  </div>
</div><!-- /.row -->

<div class="row wrapper wrapper-content animated fadeInRight">
  <div class="ibox float-e-margins">
 	  <div class="ibox-content contentBackground">
      <table class="table" id="tablePaymentMethods">
        <thead>
          <tr>
            <th><?= lang('cost_centers_index_th_code') ?></th>
            <th><?= lang('cost_centers_index_th_name') ?></th>
            <th><?= lang('cost_centers_index_th_actions') ?></th>
          </tr>
        </thead>

        <tbody>

        </tbody>

        <tfoot>
          <tr>
            <th><?= lang('cost_centers_index_th_code') ?></th>
            <th><?= lang('cost_centers_index_th_name') ?></th>
            <th><?= lang('cost_centers_index_th_actions') ?></th>
          </tr>
        </tfoot>
      </table>
    </div>
  </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="modalAddCostCenter" aria-labelledby="addcostcenter">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><?= lang('cost_centers_index_add'); ?></h4>
      </div>
        <div class="modal-body">
          <form id="formAddCostCenter">
            <div class="form-group">
              <label><?= lang('cost_centers_add_label_code') ?></label>
              <input type="text" name="code" id="code" class="form-control" required>
              <span class="text-danger code_exists" style="display: none;"><?= lang('cost_centers_add_code_exists') ?></span>
            </div>
            <div class="form-group">
              <label><?= lang('cost_centers_add_label_name') ?></label>
              <input type="text" name="name" id="name" class="form-control" required>
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button class="btn btn-default" data-dismiss="modal"><?=lang('cancel');?></button>
          <button class="btn btn-success submit_cost_center" ><?=lang('send');?></button>
        </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" tabindex="-1" role="dialog" id="modalEditCostCenter" aria-labelledby="addcostcenter">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><?= lang('cost_centers_index_edit'); ?></h4>
      </div>
        <div class="modal-body">
          <form id="formEditCostCenter">
            <input type="hidden" name="id_cost_center" id="id_cost_center">
            <div class="form-group">
              <label><?= lang('cost_centers_add_label_code') ?></label>
              <input type="text" name="code" id="code_edit" class="form-control" required>
              <span class="text-danger code_exists_edit" style="display: none;"><?= lang('cost_centers_add_code_exists') ?></span>
            </div>
            <div class="form-group">
              <label><?= lang('cost_centers_add_label_name') ?></label>
              <input type="text" name="name" id="name_edit" class="form-control" required>
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button class="btn btn-default" data-dismiss="modal"><?=lang('cancel');?></button>
          <button class="btn btn-success submit_edit_cost_center" ><?=lang('send');?></button>
        </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<script type="text/javascript">
  $('#loader').fadeIn();
var dataset1 = $('#tablePaymentMethods').DataTable({
    ajax: {
        method: 'POST',
        url: '<?=base_url("account_settings/getCostCenters"); ?>',
        dataType: 'json',
      },
      order: [ 0, 'asc' ],

    columns:[
        { data: 'code'},
        { data: 'name'},
        { data: 'actions'},
      ],
          /*order: [ 0, 'asc' ],*/
    pageLength: 25,
    responsive: true,
    dom : '<"html5buttons" B>lr<"containerBtn"><"inputFiltro"f>tip',
    buttons : [{extend:'excel', title:'Payment Methods', className:'btnExportarExcel', exportOptions: {columns : [0,1,2]}}],
    oLanguage: {
      sLengthMenu: 'Mostrando _MENU_ registros por página',
      sZeroRecords: 'No se encontraron registros',
      sInfo: 'Mostrando _START_ a _END_ de _TOTAL_ registros',
      sInfoEmpty: 'Mostrando 0 a 0 de 0 registros',
      sInfoFiltered: '(Filtrado desde _MAX_ registros)',
      sSearch:         'Buscar: ',
      oPaginate:{
        sFirst:    'Primero',
        sLast:     'Último',
        sNext:     'Siguiente',
        sPrevious: 'Anterior'
      }
    },
    "preDrawCallback": function( settings ) {

      },
    "rowCallback": function( row, data ) {

      }
    }).on("draw", function(){

     $('#loader').fadeOut();

   });


    $(document).on('click', '.submit_cost_center', function(){
      if ($('#formAddCostCenter').valid()) {
        $('#loader').fadeIn();

        var datos = $('#formAddCostCenter').serialize();
        $('#modalAddCostCenter').modal('hide');
        $.ajax({
          url:"<?= base_url('account_settings/add_cost_center') ?>",
          type:"post",
          data : datos
        }).done(function(data){
          if (data == true) {
            $('#formAddCostCenter')[0].reset();
            dataset1.ajax.reload()
            $('#loader').fadeOut();
          } else {

          }
        });

      }
    });

    $(document).on('click', '.submit_edit_cost_center', function(){
      if ($('#formAddCostCenter').valid()) {
        $('#loader').fadeIn();

        var datos = $('#formEditCostCenter').serialize();
        $('#modalEditCostCenter').modal('hide');
        $.ajax({
          url:"<?= base_url('account_settings/edit_cost_center') ?>",
          type:"post",
          data : datos
        }).done(function(data){
          if (data == true) {
            $('#formAddCostCenter')[0].reset();
            dataset1.ajax.reload()
            $('#loader').fadeOut();
          } else {

          }
        });

      }
    });

    $(document).on('keyup', '#code', function(){

      var code = $(this).val();

      $.ajax({
        url:"<?= base_url('account_settings/validate_cost_center_code') ?>",
        type:"post",
        data: {"code" : code},
      }).done(function(data){
        if (data == true) {
          $('.code_exists').css('display', '');
          $('.submit_cost_center').prop('disabled', true);
        } else {
          $('.code_exists').css('display', 'none');
          $('.submit_cost_center').prop('disabled', false);
        }
      });

    });

    $(document).on('keyup', '#code_edit', function(){

      var code = $(this).val();

      $.ajax({
        url:"<?= base_url('account_settings/validate_cost_center_code') ?>",
        type:"post",
        data: {"code" : code, "id" : $('#id_cost_center').val()},
      }).done(function(data){
        if (data == true) {
          $('.code_exists_edit').css('display', '');
          $('.submit_edit_cost_center').prop('disabled', true);
        } else {
          $('.code_exists_edit').css('display', 'none');
          $('.submit_edit_cost_center').prop('disabled', false);
        }
      });

    });

$('#modalEditCostCenter').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget);
      id = button.data('id');
      code = button.data('code');
      name = button.data('name');
      $('#id_cost_center').val(id);
      $('#code_edit').val(code);
      $('#name_edit').val(name);
});



</script>