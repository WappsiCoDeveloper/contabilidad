<script type="text/javascript">
	$(document).ready(function(){
		$('.ledger-dropdown').select2();
	});


	$(document).on('change', '#typemov', function(){
		type = $('option:selected', this).data('typeparent');
		$('#type').val(type);
	});

	$(document).on('submit', '#formEditParameter', function(event){
		$('#loader').fadeIn();
	});
	
	$(document).on('change', '#typemov', function(){
		if ($(this).val() == 'Impto consumo compra') {
			$.each($('#tax_id option'), function(index, option){
				if ($(option).data('typetax') == 1) {
					$(option).prop('disabled', true);
				}
				if ($(option).data('typetax') == 2) {
					$(option).prop('disabled', false);
				}
			});
		} else {
			$.each($('#tax_id option'), function(index, option){
				if ($(option).data('typetax') == 2) {
					$(option).prop('disabled', true);
				}
				if ($(option).data('typetax') == 1) {
					$(option).prop('disabled', false);
				}
			});
		}
		setTimeout(function() {
			$('#tax_id').select2();
		}, 550);
	});

</script>

<?php 

$tipos = array(
			1 =>
				array(
					'Ingreso' => 'Ingreso', 
					'Iva' => 'Iva (Venta)', 
					'Inventario' => 
					'Inventario (Venta)', 
					'Costo' => 'Costo', 
					'Dev vta' => 'Dev vta', 
					'Iva Dev vta' => 'Iva Dev vta', 
					'Impto consumo vta' => 
					'Impto consumo vta', 
					'Descuento' => 'Descuento (Venta)', 
					'Impto verde' => 'Impto verde', 
					'Envio vta' => 'Envio vta', 
					'Propina vta' => 'Propina vta', 
					'Autorretencion Debito' => 'Autorretención Cuenta Débito', 
					'Autorretencion Credito' => 'Autorretención Cuenta Crédito'),
		  2 =>
				array(
					'Inventario' => 'Inventario (Compra)', 
					'Iva' => 'Iva (Compra)', 
					'Dev compra' => 'Dev compra', 
					'Iva Dev compra' => 'Iva Dev compra', 
					'Impto consumo compra' => 'Segundo impuesto', 
					'Descuento' => 'Descuento (Compra)', 
					'Envio compra' => 'Envio compra',
					'Ajuste Importacion' => 'Ajuste Importacion'
				),
			3 =>
				array(
						'Pago' => 'Pago',
				),
			);

 ?>

<div class="row wrapper border-bottom white-bg page-heading">
  <div class="col-lg-8">
    <h2><?php echo $page_title; ?></h2>
    <ol class="breadcrumb">
      <li>
        <a href="login/index">Inicio</a> <?php //En el controlador en la función llamada, se revisa si el usuario está logueado y lo envía a la vista principal según permisos ?>
      </li>
      <li class="active">
        <strong><?php echo $page_title; ?></strong>
      </li>
    </ol>
  </div><!-- /.col -->
  <div class="col-lg-4">
    <div class="title-action">
      <button class="btn btn-success" onclick="$('#formEditParameter').submit();"><span class="fa fa-check "></span> <?= lang('ledgers_views_add_label_submit_btn'); ?></button>	
    </div>
  </div>
</div><!-- /.row -->    

<div class="row wrapper wrapper-content animated fadeInRight">
	<div class="ibox float-e-margins">
		<div class="ibox-content contentBackground">
			<form class="form row" method="post" id="formEditParameter">
				<div class="col-sm-4 form-group">
					<label><?= lang('accounts_parameter_add_label_type') ?></label><span class='input_required'> *</span>
					<select name="typemov" id="typemov" class="form-control ledger-dropdown" disabled required>
						<option value=""><?= lang('accounts_parameter_add_first_option') ?></option>
						<?php foreach ($tipos as $tipoP => $arr): ?>
							<?php if ($tipoP==1): ?>
								<optgroup label="VENTA">
							<?php elseif ($tipoP == 2): ?>
								<optgroup label="COMPRA">
							<?php endif ?>
							<?php foreach ($arr as $tipoC => $tipoC2): ?>
								<option value="<?= $tipoC ?>" data-typeparent="<?= $tipoP ?>" <?= ($parameter['typemov'] == $tipoC && $parameter['type'] == $tipoP ) ? "selected" : ""; ?>><?= $tipoC2 ?></option>
							<?php endforeach ?>
							</optgroup>
						<?php endforeach ?>
					</select>
					<input type="hidden" name="type" id="type" value="<?= $parameter['type'] ?>">
				</div>
				<?php if (isset($taxes)): ?>
					<div class="col-sm-4 form-group">
						<label><?= lang('accounts_parameter_add_label_tax') ?></label>
						<select name="tax_id" id="tax_id" class="form-control ledger-dropdown2" disabled>
							<option value="0"><?= lang('accounts_parameter_add_first_option') ?></option>
							<?php foreach ($taxes as $tax): ?>
								<option value="<?= $tax->id; ?>"  <?= ($parameter['id_tax'] == $tax->id) ? "selected" : "" ?>><?= $tax->name; ?></option>
							<?php endforeach; ?>
							<?php if (isset($taxes2)): ?>
								<?php foreach ($taxes2 as $tax): ?>
									<option value="<?= $tax->id; ?>" data-typetax="2" <?= ($parameter['id_tax'] == $tax->id && $parameter['typemov'] == 'Impto consumo compra') ? "selected" : "disabled" ?>><?= $tax->description; ?></option>
								<?php endforeach; ?>
							<?php endif ?>
						</select>
					</div>
				<?php endif ?>
				<?php if (isset($categories)): ?>
					<div class="col-sm-4 form-group">
						<label><?= lang('accounts_parameter_add_label_category') ?></label>
						<select name="category_id" class="form-control ledger-dropdown2" disabled>
							<option value="0"><?= lang('accounts_parameter_add_first_option') ?></option>
							<?php foreach ($categories as $category): ?>
								<option value="<?= $category->id; ?>" <?= ($parameter['id_category'] == $category->id) ? "selected" : "" ?>><?= $category->name; ?></option>
							<?php endforeach; ?>
						</select>
					</div>
				<?php endif ?>
				<div class="col-sm-4 form-group">
					<label><?= lang('accounts_parameter_add_label_ledger') ?></label><span class='input_required'> *</span>
					<select name="ledger_id" class="form-control ledger-dropdown" required>
						<?php foreach ($ledgers as $id => $ledger): ?>
							<option value="<?= $id; ?>" <?= ($id < 0) ? 'disabled' : "" ?> <?= ($parameter['ledger_id'] == $id) ? 'selected' : ''?>><?= $ledger; ?></option>
						<?php endforeach; ?>
					</select>
				</div>

				<div class="col-sm-12">
					<button class="btn btn-success"><span class="fa fa-check"></span>  <?= lang('accounts_parameter_add_submit') ?></button>
				</div>
			</form>
		</div>
	</div>
</div>