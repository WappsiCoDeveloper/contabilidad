<script type="text/javascript">
	$(document).ready(function(){
		$('.ledger-dropdown').select2();

			$("#type option").each(function(){
			   if ($(this).val() == '<?= $paymentmethod['type'] ?>') {
			   		$('#type').val($(this).val()).change();
			   }
			});

			$("#entity option").each(function(){
			   if ($(this).val() == '<?= $paymentmethod['entity'] ?>') {
			   		$('#entity').val($(this).val()).change();
			   }
			});
	});

	$(document).on('submit', '#formEditPaymentMethod', function(event){
		$('#loader').fadeIn();
	});

	$(document).on('change', '#type', function(){
		tipo = $(this).val();
		if (tipo == "Tarjeta de Credito") {
			$('.entity').css('display', '');
		} else {
			$('.entity').css('display', 'none');
		}
	});
</script>

<div class="row wrapper border-bottom white-bg page-heading">
  <div class="col-lg-8">
    <h2><?php echo $page_title; ?></h2>
    <ol class="breadcrumb">
      <li>
        <a href="login/index">Inicio</a> <?php //En el controlador en la función llamada, se revisa si el usuario está logueado y lo envía a la vista principal según permisos ?>
      </li>
      <li class="active">
        <strong><?php echo $page_title; ?></strong>
      </li>
    </ol>
  </div><!-- /.col -->
  <div class="col-lg-4">
    <div class="title-action">
      <button class="btn btn-success" onclick="$('#formEditPaymentMethod').submit();"><span class="fa fa-check "></span> <?= lang('payment_methods_add_submit'); ?></button>	
    </div>
  </div>
</div><!-- /.row -->    

<div class="row wrapper wrapper-content animated fadeInRight">
	<div class="ibox float-e-margins">
		<div class="ibox-content contentBackground">
			<form class="form row" method="post" id="formEditPaymentMethod">
				<div class="col-sm-4 form-group">
					<label><?= lang('payment_methods_add_label_type') ?></label><span class='input_required'> *</span>
					<select name="type" id="type" class="form-control ledger-dropdown" disabled required>
						<option value=""><?= lang('select') ?></option>
						<?php if (isset($payment_methods)): ?>
							<?php foreach ($payment_methods as $pmethod): ?>
								<option value="<?= $pmethod->code ?>"><?= $pmethod->name ?></option>
							<?php endforeach ?>
						<?php endif ?>
					</select>
				</div>

				<div class="col-sm-4 form-group entity" style="display: none;">
					<label><?= lang('payment_methods_add_label_entity') ?></label><span class='input_required'> *</span>
					<select name="entity" id="entity" class="form-control ledger-dropdown" disabled style="width: 100%;">
					   	<option value=""><?= lang('payment_methods_add_first_option') ?></option>
                       	<option value="Visa">Visa</option>
                       	<option value="MasterCard">MasterCard</option>
                       	<option value="Amex">Amex</option>
                       	<option value="Discover">Discover</option>
                   </select>
				</div>

				<div class="col-sm-4 form-group">
					<label><?= lang('payment_methods_add_label_ledger_receipt') ?></label><span class='input_required'> *</span>
					<select name="receipt_ledger_id" class="form-control ledger-dropdown" required>
						<?php foreach ($ledgers as $id => $ledger): ?>
							<option value="<?= $id; ?>" <?= ($id < 0) ? 'disabled' : "" ?> <?= ($id == $paymentmethod['receipt_ledger_id']) ? "selected='selected'" : "" ?>><?= $ledger; ?></option>
						<?php endforeach; ?>
					</select>
				</div>

				<div class="col-sm-4 form-group">
					<label><?= lang('payment_methods_add_label_ledger_payment') ?></label>
					<select name="payment_ledger_id" class="form-control ledger-dropdown">
						<?php foreach ($ledgers as $id => $ledger): ?>
							<option value="<?= $id; ?>" <?= ($id < 0) ? 'disabled' : "" ?> <?= ($id == $paymentmethod['payment_ledger_id']) ? "selected='selected'" : "" ?>><?= $ledger; ?></option>
						<?php endforeach; ?>
					</select>
				</div>

				<div class="col-sm-12">
					<button class="btn btn-success"><span class="fa fa-check"></span>  <?= lang('payment_methods_add_submit') ?></button>
				</div>
			</form>
		</div>
	</div>
</div>