<div class="row wrapper border-bottom white-bg page-heading">
  <div class="col-lg-8">
    <h2><?php echo $page_title; ?></h2>
    <ol class="breadcrumb">
      <li>
        <a href="login/index">Inicio</a> <?php //En el controlador en la función llamada, se revisa si el usuario está logueado y lo envía a la vista principal según permisos ?>
      </li>
      <li class="active">
        <strong><?php echo $page_title; ?></strong>
      </li>
    </ol>
  </div><!-- /.col -->
  <div class="col-lg-4">
    <div class="title-action">
      <button class="btn btn-success" onclick="$('#formAddEntryType').submit();"><span class="fa fa-check "></span> <?= lang('ledgers_views_add_label_submit_btn'); ?></button>	
    </div>
  </div>
</div><!-- /.row -->    

<div class="row wrapper wrapper-content animated fadeInRight">
  <div class="ibox float-e-margins">
    <div class="ibox-content contentBackground">
        <form class="form row" id="formAddEntryType" method="post">
           <div class="col-md-12 col-lg-6 input-field">
              <div class="form-group">
                  <label><?= lang('settings_views_entrytypes_modal_label_label'); ?></label><span class='input_required'> *</span>
                  <div class="input-group">
                      <div class="input-group-addon">
                          <i class="fa fa-user"></i>
                      </div>
                      <input name="et_label" id="et_label" type="text" class="validate form-control" required>
                      <span class="input-group-addon" id="basic-addon2" data-toggle="tooltip" data-placement="left" title="Abreviatura Ej: FC"><i class="fa fa-info-circle"></i></span>
                  </div>
                 
              </div>
          </div>
          <div class="col-md-12 col-lg-6 input-field">
              <div class="form-group">
                  <label><?= lang('settings_views_entrytypes_modal_label_name'); ?></label><span class='input_required'> *</span>
                  <div class="input-group">
                      <div class="input-group-addon">
                          <i class="fa fa-user"></i>
                      </div>
                      <input name="et_name" id="et_name" type="text" class="validate form-control" required>
                  </div>
                 
              </div>
          </div>
          <div class="col-md-12 col-lg-6 input-field">
              <div class="form-group">
                  <label><?= lang('settings_views_entrytypes_modal_label_description'); ?></label><span class='input_required'> *</span>
                  <div class="input-group">
                      <div class="input-group-addon">
                          <i class="fa  fa-user"></i>
                      </div>
                      <input name="description" id="description" type="text" class="validate form-control" required>
                  </div>
              </div>
          </div>
          <div class="col-md-12 col-lg-6 input-field">
              <div class="form-group">
                  <label><?= lang('settings_views_entrytypes_modal_label_numbering'); ?></label>
                  <div class="input-group">
                      <div class="input-group-addon">
                          <i class="fa fa-user"></i>
                      </div>
                      <select name="numbering" id="numbering" class="form-control">
                          <option value="1"><?= lang('settings_views_entrytypes_modal_numbering_option_1'); ?></option>
                          <option value="2"><?= lang('settings_views_entrytypes_modal_numbering_option_2'); ?></option>
                          <option value="3"><?= lang('settings_views_entrytypes_modal_numbering_option_3'); ?></option>
                      </select>
                  </div>
                 
              </div>
          </div>
          <div class="col-md-12 col-lg-6 input-field">
              <div class="form-group">
                  <label><?= lang('settings_views_entrytypes_modal_label_perfix'); ?></label><span class='input_required'> *</span>
                  <div class="input-group">
                      <div class="input-group-addon">
                          <i class="fa fa-user"></i>
                      </div>
                      <input name="prefix" id="prefix" type="text" class="validate form-control" required>
                  </div>
                 
              </div>
          </div>
          <div class="col-md-12 col-lg-6 input-field">
              <div class="form-group">
                  <label><?= lang('settings_views_entrytypes_modal_label_suffix'); ?></label>
                  <div class="input-group">
                      <div class="input-group-addon">
                          <i class="fa fa-user"></i>
                      </div>
                      <input name="suffix" id="suffix" type="text" class="validate form-control">
                  </div>
                 
              </div>
          </div>
          <div class="col-md-12 col-lg-6 input-field">
              <div class="form-group">
                  <label><?= lang('settings_views_entrytypes_modal_label_zero_padding'); ?></label><span class='input_required'> *</span>
                  <div class="input-group">
                      <div class="input-group-addon">
                          <i class="fa fa-user"></i>
                      </div>
                      <input name="zero_padding" id="zero_padding" type="text" class="validate form-control" value="0" required>
                  </div>
                 
              </div>
          </div>
          <div class="col-md-12 col-lg-6 input-field">
              <div class="form-group">
                  <label><?= lang('settings_views_entrytypes_modal_label_restrictions'); ?></label><span class='input_required'> *</span>
                  <div class="input-group">
                      <div class="input-group-addon">
                          <i class="fa fa-user"></i>
                      </div>
                      <select id="restriction_bankcash" name="restriction_bankcash" class="form-control" required="required">
                          <option value="1"><?= lang('settings_views_entrytypes_modal_restrictions_option_1'); ?></option>
                          <option value="2"><?= lang('settings_views_entrytypes_modal_restrictions_option_2'); ?></option>
                          <option value="3"><?= lang('settings_views_entrytypes_modal_restrictions_option_3'); ?></option>
                          <option value="4"><?= lang('settings_views_entrytypes_modal_restrictions_option_4'); ?></option>
                          <option value="5"><?= lang('settings_views_entrytypes_modal_restrictions_option_5'); ?></option>
                      </select>
                  </div>
              </div>
          </div>
          <div class="col-md-12 col-lg-6 input-field">
              <div class="form-group">
                  <label><?= lang('settings_views_entrytypes_modal_label_origin'); ?></label><br>
                  <label>
                      <input name="entrytype_origin" id="entrytype_origin" type="checkbox" class="validate form-control" value="1" checked><?= lang('settings_views_entrytypes_modal_label_origin_checkbox') ?>
                  </label>
              </div>
          </div>
          <div class="col-md-12">
            <button class="btn btn-success"><span class="fa fa-check"></span> <?= lang('ledgers_views_add_label_submit_btn'); ?></button>
            <button class="btn btn-default" type="button" onclick="window.location.href='<?=  base_url().'account_settings/entrytypes' ?>';"><?= lang('ledgers_views_edit_label_cancel_btn') ?></button>
          </div>
        </form>
    </div>
  </div>
</div>

<script type="text/javascript">
  $(function () {
  $('[data-toggle="tooltip"]').tooltip()
})
</script>