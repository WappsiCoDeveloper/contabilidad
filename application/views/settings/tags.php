<script>
    $(function() {
        $('#cp1').colorpicker();
        $('#cp2').colorpicker();
    });
    function color(x){
        return '<button class="btn btn-sm" style=" height:20px; background-color: #'+x+'"></button>'
    }
    $(document).ready(function () {
        dataset1 = $('#dynamic-table').DataTable({
        ajax: {
            method: 'POST',
            url: '<?=base_url(); ?>account_settings/getAllTags',
            dataType: 'json',
          },
          order: [ 0, 'asc' ],
          order: [ 1, 'desc' ],

        columns:[
            { data: '0'},
            { data: '1'},
            { data: '2'},
            { data: '3'},
          ],
              /*order: [ 0, 'asc' ],*/
        pageLength: 25,
        responsive: true,
        dom : '<"html5buttons" B>lr<"containerBtn"><"inputFiltro"f>tip',
        buttons : [{extend:'excel', title:'Entries', className:'btnExportarExcel', exportOptions: {columns : [0,1,2,3,4,5,6]}}],
        oLanguage: {
          sLengthMenu: 'Mostrando _MENU_ registros por página',
          sZeroRecords: 'No se encontraron registros',
          sInfo: 'Mostrando _START_ a _END_ de _TOTAL_ registros',
          sInfoEmpty: 'Mostrando 0 a 0 de 0 registros',
          sInfoFiltered: '(Filtrado desde _MAX_ registros)',
          sSearch:         'Buscar: ',
          oPaginate:{
            sFirst:    'Primero',
            sLast:     'Último',
            sNext:     'Siguiente',
            sPrevious: 'Anterior'
          }
        },
        "preDrawCallback": function( settings ) {
            
          }
        }).on("draw", function(){
         $('#loader').fadeOut();
       });
              
    });


</script>

<div class="row wrapper border-bottom white-bg page-heading">
  <div class="col-lg-8">
    <h2><?php echo $page_title; ?></h2>
    <ol class="breadcrumb">
      <li>
        <a href="login/index">Inicio</a> <?php //En el controlador en la función llamada, se revisa si el usuario está logueado y lo envía a la vista principal según permisos ?>
      </li>
      <li class="active">
        <strong><?php echo $page_title; ?></strong>
      </li>
    </ol>
  </div><!-- /.col -->
  <div class="col-lg-4">
    <div class="title-action">
      <div class="btn-group">
        <button href="#clientmodal" class="btn btn-success add_c"><span class="fa fa-plus"></span> <?= lang('tag_add') ?></button>

        <!-- 
                <button href="#clientmodal" class="add_c btn btn-primary pull-right">
                    <i class="fa fa-plus-circle"></i><?= lang('tag_add'); ?>
                </button> -->
      </div>  
    </div>
  </div>
</div><!-- /.row -->

<!-- ============= MODAL MODIFICA CLIENTI ============= -->
<div class="modal fade" id="clientmodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="titclienti"></h4>
            </div>
            <div class="modal-body">
                <div class="panel-body">
                    <p class="tips custip"></p>
                    <div class="row">
                        <form class="col s12" id="myForm">
                            <div class="col-md-12 col-lg-6 input-field">
                                <div class="form-group">
                                    <?= lang('tag_name', 'tag_name'); ?>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa  fa-user"></i>
                                        </div>
                                        <input name="tag_name" id="tag_name" type="text" class="validate form-control" required>
                                    </div>
                                   
                                </div>
                            </div>

                            <div class="col-md-12 col-lg-6 input-field">
                                <div class="form-group">
                                    <?= lang('tag_color', 'tag_color'); ?>
                                    <div id="cp2" class="input-group colorpicker-component">
                                        <input type="text" name="tag_color" id="tag_color" value="#000" class="form-control"  required />
                                        <span class="input-group-addon"><i></i></span>
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="col-md-12 col-lg-6 input-field">
                                <div class="form-group">
                                    <?= lang('tag_backgroud', 'tag_bg'); ?>
                                    <div id="cp1" class="input-group colorpicker-component">
                                        <input type="text" name="tag_bg" id="tag_bg" value="#FFF" class="form-control"  required />
                                        <span class="input-group-addon"><i></i></span>
                                    </div>
                                    
                                </div>
                            </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer" id="footerClient1">
                  <!--    -->
            </div>
        </div>
    </div>
</div>
</div>

<div class="row wrapper wrapper-content animated fadeInRight">
      <div class="box float-e-margins">
        <div class="ibox-content contentBackground">
          <div class="col-sm-12">
            <div class="box-header">
                <h3 class="box-title"><?= lang('tags_title'); ?></h3>
            </div>
            
                <div class="adv-table">
                    <table class="display compact table table-bordered table-striped" id="dynamic-table">
                        <thead>
                            <tr>
                                <th><?= lang('tag_name'); ?></th>
                                <th><?= lang('tag_color'); ?></th>
                                <th><?= lang('tag_backgroud'); ?></th>
                                <th><?= lang('actions'); ?></th>
                            </tr>
                        </thead>
                            
                        <tfoot>
                            <tr>
                                <th><?= lang('tag_name'); ?></th>
                                <th><?= lang('tag_color'); ?></th>
                                <th><?= lang('tag_backgroud'); ?></th>
                                <th><?= lang('actions'); ?></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="delete_tag" aria-labelledby="deleteaccount">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><?= lang('accounts_index_delete_tag_alert_title'); ?></h4>
      </div>
        <div class="modal-body">
          <input type="hidden" name="tag_id_delete" id="tag_id_delete">
          <?= lang('accounts_index_delete_tag_alert_text'); ?>
        </div>
        <div class="modal-footer">
          <button class="btn btn-default" data-dismiss="modal"><?=lang('no');?></button>
          <button class="btn btn-danger confirm_delete_tag" ><?=lang('yes');?></button>
        </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">

    jQuery(".add_c").on("click", function (e) {
        $('#clientmodal').modal('show');

        jQuery('#tag_name').val('');
        jQuery('#tag_color').val('');
        jQuery('#tag_bg').val('');
        
        jQuery('#titclienti').html("<?= lang('tag_add'); ?>");

        jQuery('#footerClient1').html('<button data-dismiss="modal" class="btn btn-default" type="button"><?= lang('settings_views_entrytypes_modal_footer_btn_cancel'); ?></button><button id="submit" class="btn btn-success" form="myForm" role="submit" data-mode="add"><i class="fa fa-check"></i> <?= lang('tag_add'); ?></button>');
    });

    jQuery(document).on("click", "#modify", function () {
        jQuery('#titclienti').html('<?= lang('tag_edit'); ?>');
        var num = jQuery(this).data("num");
            jQuery.ajax({
                type: "POST",
                url: "<?= base_url(); ?>account_settings/tags/getByID",
                data: "id=" + encodeURI(num),
                cache: false,
                dataType: "json",
                success: function (data) {
                    jQuery('#tag_name').val(data.title);
                    jQuery('#tag_color').val(data.color);
                    jQuery('#tag_bg').val(data.background);

                    jQuery('#footerClient1').html('<button data-dismiss="modal" class="btn btn-default" type="button"> <?= lang('settings_views_entrytypes_modal_footer_btn_cancel'); ?></button><button id="submit" form="myForm" role="submit" class="btn btn-success" data-mode="modify" data-num="' + encodeURI(num) + '"><i class="fa fa-check"></i> <?= lang('tag_save'); ?></button>')
                }
            });
        });

    $( "#myForm" ).submit(function( event ) {
        var mode = jQuery('#submit').data("mode");
        var id = jQuery('#submit').data("num");

        //validate
        var valid = true;;

        if (valid) {
            var url = "";
            var dataString = $('form').serialize();
            if (mode == "add") {
                url = "<?= base_url(); ?>" + "account_settings/tags/add";
                jQuery.ajax({
                    type: "POST",
                    url: url,
                    data: dataString,
                    cache: false,
                    success: function (data) {
                        toastr['success']("<?= lang('tag_added'); ?>");
                        setTimeout(function () {
                            $('#clientmodal').modal('hide');
                            $('#dynamic-table').DataTable().ajax.reload();
                        }, 500);
                    }
                });
            } else {
                url = "<?= base_url(); ?>" + "account_settings/tags/edit";
                dataString += "&id=" + encodeURI(id);
                jQuery.ajax({
                    type: "POST",
                    url: url,
                    data: dataString,
                    cache: false,
                    success: function (data) {
                        toastr['success']("<?= lang('tag_saved'); ?>");
                        setTimeout(function () {
                            $('#clientmodal').modal('hide');
                            $('#dynamic-table').DataTable().ajax.reload();
                        }, 500);
                    }
                });
            }
        }
        return false;
    });

     $('#delete_tag').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        tagid = button.data('num');
        $('#tag_id_delete').val(tagid);
    });

    $('.confirm_delete_tag').on('click', function(){
        tagid = $('#tag_id_delete').val();
        jQuery.ajax({
            type: "POST",
            url: "<?=base_url(); ?>" + "account_settings/tags/delete",
            data: "id=" + encodeURI(tagid),
            cache: false,
            dataType: "json",
            success: function (data) {
                toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "progressBar": true,
                    "positionClass": "toast-bottom-right",
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                }
                if (data == 'true') {
                    toastr['success']("<?= lang('tag_deleted_success'); ?>");
                    $('#delete_tag').modal('hide');
                }else{
                    toastr['error']("<?= lang('tag_deleted_error'); ?>");
                }
                $('#dynamic-table').DataTable().ajax.reload();
            }
        });
    });
</script>
