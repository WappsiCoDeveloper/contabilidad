<div class="row wrapper border-bottom white-bg page-heading">
  <div class="col-lg-8">
    <h2><?php echo $page_title; ?></h2>
    <ol class="breadcrumb">
      <li>
        <a href="login/index">Inicio</a> <?php //En el controlador en la función llamada, se revisa si el usuario está logueado y lo envía a la vista principal según permisos ?>
      </li>
      <li class="active">
        <strong><?php echo $page_title; ?></strong>
      </li>
    </ol>
  </div><!-- /.col -->
  <div class="col-lg-4">
    <div class="title-action">
      <a href="<?= base_url()."account_settings/addpaymentmethod" ?>" class="btn btn-success"><span class="fa fa-plus "></span> <?= lang('payment_methods_index_add'); ?></a>
    </div>
  </div>
</div><!-- /.row -->

<div class="row wrapper wrapper-content animated fadeInRight">
  <div class="ibox float-e-margins">
 	  <div class="ibox-content contentBackground">
      <table class="table" id="tablePaymentMethods">
        <thead>
          <tr>
            <th><?= lang('payment_methods_index_th_type') ?></th>
            <!-- <th><?= lang('payment_methods_index_th_entity') ?></th> -->
            <th><?= lang('payment_methods_index_th_ledger_receipt') ?></th>
            <th><?= lang('payment_methods_index_th_ledger_payment') ?></th>
            <th><?= lang('payment_methods_index_th_actions') ?></th>
          </tr>
        </thead>

        <tbody>

        </tbody>

        <tfoot>
          <tr>
            <th><?= lang('payment_methods_index_th_type') ?></th>
            <!-- <th><?= lang('payment_methods_index_th_entity') ?></th> -->
            <th><?= lang('payment_methods_index_th_ledger_receipt') ?></th>
            <th><?= lang('payment_methods_index_th_ledger_payment') ?></th>
            <th><?= lang('payment_methods_index_th_actions') ?></th>
          </tr>
        </tfoot>
      </table>
    </div>
  </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="modalDeletePaymentMethod" aria-labelledby="deletepaymentmethod">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><?= lang('payment_methods_index_modal_delete_title'); ?></h4>
      </div>
        <div class="modal-body">
          <input type="hidden" name="paymentmethod_id_delete" id="paymentmethod_id_delete">
          <?= lang('payment_methods_index_modal_delete_text'); ?>
        </div>
        <div class="modal-footer">
          <button class="btn btn-default" data-dismiss="modal"><?=lang('no');?></button>
          <button class="btn btn-danger confirm_delete_paymentmethod" ><?=lang('yes');?></button>
        </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<script type="text/javascript">
  $('#loader').fadeIn();
dataset1 = $('#tablePaymentMethods').DataTable({
    ajax: {
        method: 'POST',
        url: '<?=base_url("account_settings/getPaymentsMethods"); ?>',
        dataType: 'json',
      },
      order: [ 0, 'asc' ],

    columns:[
        { data: 'type'},
        // { data: 'entity'},
        { data: 'receipt_ledger'},
        { data: 'payment_ledger'},
        { data: 'actions'},
      ],
          /*order: [ 0, 'asc' ],*/
    pageLength: 25,
    responsive: true,
    dom : '<"html5buttons" B>lr<"containerBtn"><"inputFiltro"f>tip',
    buttons : [{extend:'excel', title:'Payment Methods', className:'btnExportarExcel', exportOptions: {columns : [0,1,2,3,4,5,6]}}],
    oLanguage: {
      sLengthMenu: 'Mostrando _MENU_ registros por página',
      sZeroRecords: 'No se encontraron registros',
      sInfo: 'Mostrando _START_ a _END_ de _TOTAL_ registros',
      sInfoEmpty: 'Mostrando 0 a 0 de 0 registros',
      sInfoFiltered: '(Filtrado desde _MAX_ registros)',
      sSearch:         'Buscar: ',
      oPaginate:{
        sFirst:    'Primero',
        sLast:     'Último',
        sNext:     'Siguiente',
        sPrevious: 'Anterior'
      }
    },
    "preDrawCallback": function( settings ) {

      },
    "rowCallback": function( row, data ) {

      }
    }).on("draw", function(){

     $('#loader').fadeOut();

   });


$('#modalDeletePaymentMethod').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget);
      paymentmethod = button.data('paymentmethod');
      $('#paymentmethod_id_delete').val(paymentmethod);
});

$('.confirm_delete_paymentmethod').on('click', function(){
  paymentmethod = $('#paymentmethod_id_delete').val();
  window.location.href = "<?= base_url(); ?>account_settings/deletepaymentmethod/"+paymentmethod;
});


</script>