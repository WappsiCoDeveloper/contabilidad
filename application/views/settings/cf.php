<script type="text/javascript">
    $(document).ready(function() {
        /* Setup jQuery datepicker ui */
        $('#fiscal_start').datepicker({
            dateFormat: $("#date_format").val().split('|')[1],  /* Read the Javascript date format value */
            numberOfMonths: 1,
            onClose: function(selectedDate) {
                $("#fiscal_end").datepicker("option", "minDate", selectedDate);
            }
        });
        $('#fiscal_end').datepicker({
            dateFormat: $("#date_format").val().split('|')[1],  /* Read the Javascript date format value */
            numberOfMonths: 1,
            onClose: function(selectedDate) {
                $("#fiscal_start").datepicker("option", "maxDate", selectedDate);
            }
        });

        $("#date_format").change(function() {
            /* Read the Javascript date format value */
            dateFormat = $(this).val().split('|')[1];
            $("#fiscal_start").datepicker("option", "dateFormat", dateFormat);
            $("#fiscal_end").datepicker("option", "dateFormat", dateFormat);
        });
    });
</script>
<div class="row wrapper border-bottom white-bg page-heading">
  <div class="col-lg-8">
    <h2><?php echo $page_title; ?></h2>
    <ol class="breadcrumb">
      <li>
        <a href="login/index">Inicio</a> <?php //En el controlador en la función llamada, se revisa si el usuario está logueado y lo envía a la vista principal según permisos ?>
      </li>
      <li class="active">
        <strong><?php echo $page_title; ?></strong>
      </li>
    </ol>
  </div><!-- /.col -->
  <div class="col-lg-4">
    <div class="title-action">
    </div>
  </div>
</div><!-- /.row -->   
<!-- Main content -->
<div class="row wrapper wrapper-content animated fadeInRight">
  <div class="box float-e-margins">
    <div class="ibox-content contentBackground">
      <div class="col-sm-12">
        <div class="box-header with-border">
        </div>
        <!-- /.box-header -->
        <div class="box-body">
        	<h4><?= lang('settings_views_cf_subtitle'); ?></h4>
    			<table class="table">
    				<tr>
    					<td width="150px"><?php echo (lang('settings_views_cf_label_name')); ?></td>
    					<td><?php echo ($this->mAccountSettings->name); ?></td>
    				</tr>
    				<tr>
    					<td><?php echo (lang('settings_views_cf_label_email')); ?></td>
    					<td><?php echo ($this->mAccountSettings->email); ?></td>
    				</tr>
    				<tr>
    					<td><?php echo (lang('settings_views_cf_label_currency')); ?></td>
    					<td><?php echo ($this->mAccountSettings->currency_symbol); ?></td>
    				</tr>
    				<tr>
    					<td><?php echo (lang('settings_views_cf_label_fiscal_year')); ?></td>
    					<td><?php echo $this->functionscore->dateFromSql($this->mAccountSettings->fy_start) . ' to ' . $this->functionscore->dateFromSql($this->mAccountSettings->fy_end); ?></td>
    				</tr>
    				<tr>
    					<td><?php echo (lang('settings_views_cf_label_status')); ?></td>
    					<?php
    						if ($this->mAccountSettings->account_locked == 0) {
    							echo '<td>' . (lang('settings_views_cf_label_unlocked')) . '</td>';
    						} else {
    							echo '<td>' . (lang('settings_views_cf_label_locked')) . '</td>';
    						}
    					?>
    				</tr>
    				<tr>
    					<td><?php echo (lang('settings_views_cf_label_database')); ?></td>
    					<td><?= $this->DB1->database; ?></td>
    				</tr>
    			</table>

          <?php 

          $step = $this->mAccountSettings->close_year_step;
          $step1 = "";
          $step2 = "";
          $progressbar1 = 0;
          $progressbar2 = 0;
          $label1 = 'label-warning';
          $label2 = 'label-warning';
          $textlabel1 = lang('pending');
          $textlabel2 = lang('pending');
          $cf_newyear_form = "";
          $cf_balance_form = "";
          if ($step >= 1) {
            $step1 = "disabled";
            $cf_newyear_form = "display:none;";
            $progressbar1 = 100;
            $label1 = 'label-primary';
            $textlabel1 = lang('done');
          }
          if ($step < 1) {
            $cf_balance_form = "display:none;";
            $step2 = "disabled";
          }

          if ($step == 3) {
            $step2 = "disabled";
            $progressbar2 = 100;
            $label2 = 'label-primary';
            $textlabel2 = lang('done');
          }
           ?>

          <table class="table"> 
            <thead>
              <tr>
                <th style="width: 10%;"><?= lang('step') ?></th>
                <th style="width: 10%;"><?= lang('action') ?></th>
                <th style="width: 70%;"><?= lang('progress') ?></th>
                <th style="width: 10%;"><?= lang('status') ?></th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>
                  <?= lang('year_creation') ?>
                </td>
                <td>
                  <button type="button" class="btn btn-primary btn_actions" id="year_creation" <?= $step1 ?>><?= lang('submit') ?></button>
                </td>
                <td>
                  <div class="progress">
                    <div class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" id="year_creation_progress" style="width: <?= $progressbar1 ?>%">
                    </div>
                  </div>
                </td>
                <td>
                  <label class="label <?= $label1 ?> label-year_creation"><?= $textlabel1 ?></label>
                </td>
              </tr>
              <tr>
                <td>
                  <?= lang('initial_balance_transfer') ?>
                </td>
                <td>
              <?php 
                if (isset($error_msg)) {
                  $step2 = 'disabled';
                }
               ?>
                  <button type="button" class="btn btn-primary btn_actions" id="initial_balance_transfer" <?= $step2 ?> ><?= lang('submit') ?></button>
                </td>
                <td>
                  <div class="progress">
                    <div class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" id="initial_balance_transfer_progress" style="width: <?= $progressbar2 ?>%">
                    </div>
                  </div>
                </td>
                <td>
                  <label class="label <?= $label2 ?> label-initial_balance_transfer"><?= $textlabel2 ?></label>
                </td>
              </tr>
              <?php if (isset($error_msg)): ?>
                <tr>
                  <th colspan="4">
                    <em class="text-danger"><?= $error_msg ?></em>
                  </th>
                </tr>
              <?php endif ?>
              <?php if ($this->mAccountSettings->close_year_step == 3): ?>
              <tr>
                <td>
                  <?= lang('initial_balance_transfer_again') ?>
                </td>
                <td>
                  <button type="button" class="btn btn-primary btn_actions" id="initial_balance_transfer_again"><?= lang('redo') ?></button>
                </td>
                <td>
                  <div class="progress">
                    <div class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" id="initial_balance_transfer_again_progress" style="width: 0%">
                    </div>
                  </div>
                </td>
                <td>
                  <label class="label label-warning label-initial_balance_transfer_again"><?= lang('pending') ?></label>
                </td>
              </tr>
              <?php endif ?>
            </tbody>
          </table>
          <?php if ($user_activities): ?>
            <table class="table">
              <tr>
                <th>Fecha cierre</th>
                <th>Descripción</th>
              </tr>
              <?php foreach ($user_activities as $ua): ?>
                <tr>
                  <td><?= $ua->date ?></td>
                  <td><?= $ua->description ?></td>
                </tr>
              <?php endforeach ?>
            </table>
          <?php endif ?>
          <div class="div_display_error">
            
          </div>
    			<br />
          <?= form_open('', array('id'=>'formCf')); ?>
            <div class="row" style="<?= $cf_newyear_form ?>">
              <div class="col-md-4">
                <div class="form-group">
                  <label><?= lang('settings_views_cf_label_label'); ?></label>
                  <div class="input-group">
                    <?php echo form_input($label);?>
                    <div class="input-group-addon">
                      <i>
                        <div class="fa fa-info-circle" data-toggle="tooltip" title="<?= lang('settings_views_cf_label_tooltip'); ?>">
                      </div>
                      </i>
                    </div>
                  </div>
                </div>    	
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label><?= lang('settings_views_cf_label_company_name'); ?></label>
                  <?php echo form_input($name);?>
                </div>    	
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label><?= lang('settings_views_cf_label_date_format'); ?></label>
                  <?php if ($this->mAccountSettings->date_format == 'd-M-Y|dd-M-yy'): ?>
                    <input type="text" name="date_format" id="date_format" class="form-control" value="<?= lang('settings_views_cf_date_format_option_1'); ?>" disabled>
                  <?php elseif ($this->mAccountSettings->date_format == 'M-d-Y|M-dd-yy'): ?>
                    <input type="text" name="date_format" id="date_format" class="form-control" value="<?= lang('settings_views_cf_date_format_option_2'); ?>" disabled>
                  <?php elseif ($this->mAccountSettings->date_format == 'Y-M-d|yy-M-dd'): ?>
                    <input type="text" name="date_format" id="date_format" class="form-control" value="<?= lang('settings_views_cf_date_format_option_3'); ?>" disabled>
                  <?php endif ?>
                </div>
              </div>
            </div>
            <div class="row" style="<?= $cf_newyear_form ?>">
          		<div class="col-md-4">
          			<div class="form-group">
  						      <label><?= lang('settings_views_cf_label_fy_start'); ?></label>
  			            <?php echo form_input($fy_start);?>
  	            </div>    	
          		</div>
          		<div class="col-md-4">
          			<div class="form-group">
  						    <label><?= lang('settings_views_cf_label_fy_end'); ?></label>
  			         <?php echo form_input($fy_end);?>
  	                </div>    	
          		</div>
            </div>
            <div class="row" style="<?= $cf_balance_form ?>">
              <hr class="col-md-11">
              <div class="col-md-12">
                <h4><?= lang('transfer_profit_or_loss_balances') ?></h4>
              </div>
              <div class="col-md-4 form-group">
                <label><?= lang('entries_views_add_items_th_ledger', 'ledger_id'); ?></label>
                <select class="ledger-dropdown ledger_select form-control" name="ledger_id" id="ledger_id" required>
                    <option value=""><?= lang('entries_views_add_company_first_option'); ?></option>
                </select>
              </div>
              <div class="col-md-4 form-group">
                <label><?= lang('entries_views_add_label_company', 'companies_id') ?></label>
                <select name="companies_id" id="companies_id" class="companies_select form-control third ledger-dropdown2" tabindex='<?= $tabindex ?>' required>
                  <option value=""><?= lang('entries_views_add_company_first_option'); ?></option>
                </select>
              </div>
              <div class="col-md-12 form-group">
                <b>
                <?php 
                  if ($this->functionscore->calculate($pandl['gross_pl'], 0, '>=')) {
                    echo lang('cf_profit_loss_gp')." ".$this->functionscore->toCurrency('', $pandl['gross_pl']);
                  } else {
                    echo lang('cf_profit_loss_glcd')." ".$this->functionscore->toCurrency('', $pandl['gross_pl']);
                  }
                 ?>
                </b>
              </div>
          	</div>
        	<?= form_close(); ?>

        </div>
      </div>
  </div>
  <!-- /.row -->
</div>
<!-- /.content -->

<div class="modal fade" tabindex="-1" role="dialog" id="close_fiscal_year" aria-labelledby="approveentry">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title cy_title"></h4>
      </div>
        <div class="modal-body">
          <div class="cy_text">
            
          </div>
          <br>
          <br>
          <div class="progress si_progress row" style="display: none;">

            <div class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" id="si_progress" style="width: 0%">
            </div>
            <br>
            <br>
            <button type="button" class="btn btn-primary" id="si_continue" disabled><?= lang('continue') ?></button>

          </div>
        </div>
        <div class="modal-footer">
          <input type="hidden" id="cy_step">
          <button class="btn btn-default cancel_close_fiscal_year" data-dismiss="modal"><?=lang('no');?></button>
          <button class="btn btn-danger confirm_close_fiscal_year"><?=lang('yes');?></button>
        </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">

  $(document).ready(function(){
    setLedgersSelect();
    setCompaniesSelect();
  });

  var progress_bar = 0;
  var grow_progress_bar_interval;

  $(document).on('click', '#year_creation', function(){
    $('.cy_title').text('<?= lang('year_creation_title') ?>');
    $('.cy_text').text('<?= lang('year_creation_text') ?>');
    $('#cy_step').val(1);
    $('#close_fiscal_year').modal('show');
  });

  $(document).on('click', '#initial_balance_transfer', function(){
    if ($('#formCf').valid()) {
      $('.cy_title').text('<?= lang('initial_balance_transfer_title') ?>');
      $('.cy_text').text('<?= lang('initial_balance_transfer_text') ?>');
      $('#cy_step').val(2);
      $('#close_fiscal_year').modal('show');
    }
  });

  $(document).on('click', '#initial_balance_transfer_again', function(){
    if ($('#formCf').valid()) {
      $('.cy_title').text('<?= lang('initial_balance_transfer_again_title') ?>');
      $('.cy_text').text('<?= lang('initial_balance_transfer_again_text') ?>');
      $('#cy_step').val(3);
      $('#close_fiscal_year').modal('show');
    }
  });

  
  
	$(document).on('click', '.confirm_close_fiscal_year', function(){
    data_step = $('#cy_step').val();
    if (data_step == 1) {
      $('#year_creation').prop('disabled', true);
      $('#close_fiscal_year').modal('hide');
      $('.label-year_creation').text('<?= lang('processing') ?>');
      $('.label-year_creation').removeClass('label-warning').addClass('label-default');
      grow_progress_bar_interval = setInterval(
        function(){
          progress_bar += 1;
          if (progress_bar >= 60) {
            clearInterval(grow_progress_bar_interval);
          }
          $('#year_creation_progress').css('width', progress_bar+'%');
        }
        ,500);
      datos = $('#formCf').serialize();
      $.ajax({
        url : "<?= base_url('account_settings/close_year_create_new_year') ?>",
        type : "POST",
        dataType : "JSON",
        data : datos
      }).done(function(data){
        if (data.response == 1) {
          clearInterval(grow_progress_bar_interval);
          $('#year_creation_progress').css('width', '100%');
          $('.label-year_creation').text('<?= lang('done') ?>');
          $('.label-year_creation').removeClass('label-default').addClass('label-primary');
          command: toastr.success('Se creó exitosamente el año nuevo.', '¡Año creado!', {
              "showDuration": "500",
              "hideDuration": "1000",
              "timeOut": "4000",
              "extendedTimeOut": "1000",
              onHidden : function(){
                  $('#loader').fadeIn();
                  location.reload();
              }
          });
        } else {
          clearInterval(grow_progress_bar_interval);
          $('#year_creation_progress').css('width', '100%');
          $('.label-year_creation').text('<?= lang('error') ?>');
          $('.label-year_creation').removeClass('label-default').addClass('label-primary');
          command: toastr.error('Error.', '¡Error!', {
              "showDuration": "500",
              "hideDuration": "1000",
              "timeOut": "4000",
              "extendedTimeOut": "1000",
              onHidden : function(){
                  $('#loader').fadeIn();
                  location.reload();
              }
          });
        }
      }).fail(function(data){
          $('.label-year_creation').text('Error');
          $('.label-year_creation').removeClass('label-default').addClass('label-danger');
          $('#year_creation_progress').css('width', '0%');
          console.log(data);
          console.log(data.responseText);
          $('.div_display_error').html(data.responseText);
          $('body').css('margin', '0px');
      });
    } else if (data_step == 2) {
      $('#initial_balance_transfer').prop('disabled', true);
      $('#close_fiscal_year').modal('hide');
      $('.label-initial_balance_transfer').text('<?= lang('processing') ?>');
      $('.label-initial_balance_transfer').removeClass('label-warning').addClass('label-default');
      progress_bar = 0;
      grow_progress_bar_interval = setInterval(
        function(){
          progress_bar += 1;
          if (progress_bar >= 60) {
            clearInterval(grow_progress_bar_interval);
          }
          $('#initial_balance_transfer_progress').css('width', progress_bar+'%');
        }
        ,2000);
      datos = $('#formCf').serialize();
      $.ajax({
        url : "<?= base_url('account_settings/close_year_initial_balance_transfer') ?>",
        type : "POST",
        dataType : "JSON",
        data : datos
      }).done(function(data){
        if (data.response == 1) {
          clearInterval(grow_progress_bar_interval);
          $('#initial_balance_transfer_progress').css('width', '100%');
          $('#si_continue').prop('disabled', false);
          $('.label-initial_balance_transfer').text('<?= lang('done') ?>');
          $('.label-initial_balance_transfer').removeClass('label-default').addClass('label-primary');
          command: toastr.success('Se creó exitosamente el año nuevo.', '¡Año creado!', {
              "showDuration": "500",
              "hideDuration": "1000",
              "timeOut": "4000",
              "extendedTimeOut": "1000",
              onHidden : function(){
                  $('#loader').fadeIn();
                  location.reload();
              }
          });
        } else {
          clearInterval(grow_progress_bar_interval);
          $('#initial_balance_transfer_progress').css('width', '100%');
          $('#si_continue').prop('disabled', false);
          $('.label-initial_balance_transfer').text('<?= lang('error') ?>');
          $('.label-initial_balance_transfer').removeClass('label-default').addClass('label-primary');
          command: toastr.error('Error.', '¡Error!', {
              "showDuration": "500",
              "hideDuration": "1000",
              "timeOut": "4000",
              "extendedTimeOut": "1000",
              onHidden : function(){
                  $('#loader').fadeIn();
                  location.reload();
              }
          });
        }
      }).fail(function(data){
          $('.label-initial_balance_transfer').text('Error');
          $('.label-initial_balance_transfer').removeClass('label-default').addClass('label-danger');
          $('#initial_balance_transfer_progress').css('width', '0%');
          console.log(data.responseText);
          $('.div_display_error').html(data.responseText);
          $('body').css('margin', '0px');
      });
    } else if (data_step == 3) {
      $('#initial_balance_transfer_again').prop('disabled', true);
      $('#close_fiscal_year').modal('hide');
      $('.label-initial_balance_transfer_again').text('<?= lang('processing') ?>');
      $('.label-initial_balance_transfer_again').removeClass('label-warning').addClass('label-default');
      progress_bar = 0;
      grow_progress_bar_interval = setInterval(
        function(){
          progress_bar += 1;
          if (progress_bar >= 60) {
            clearInterval(grow_progress_bar_interval);
          }
          $('#initial_balance_transfer_again_progress').css('width', progress_bar+'%');
        }
        ,2000);
      datos = $('#formCf').serialize();
      $.ajax({
        url : "<?= base_url('account_settings/close_year_initial_balance_transfer') ?>",
        type : "POST",
        dataType : "JSON",
        data : datos
      }).done(function(data){
        if (data.response == 1) {
          clearInterval(grow_progress_bar_interval);
          $('#initial_balance_transfer_again_progress').css('width', '100%');
          $('#si_continue').prop('disabled', false);
          $('.label-initial_balance_transfer_again').text('<?= lang('done') ?>');
          $('.label-initial_balance_transfer_again').removeClass('label-default').addClass('label-primary');
          command: toastr.success('Se creó exitosamente el año nuevo.', '¡Año creado!', {
              "showDuration": "500",
              "hideDuration": "1000",
              "timeOut": "4000",
              "extendedTimeOut": "1000",
              onHidden : function(){
                  $('#loader').fadeIn();
                  location.reload();
              }
          });
        }
      }).fail(function(){
          $('.label-initial_balance_transfer_again').text('Error');
          $('.label-initial_balance_transfer_again').removeClass('label-default').addClass('label-danger');
          $('#initial_balance_transfer_again_progress').css('width', '0%');
          console.log(data);
          console.log(data.responseText);
          $('.div_display_error').html(data.responseText);
          $('body').css('margin', '0px');
      });
    }
	});

  function setLedgersSelect(){
    $('.ledger_select').select2({
        minimumInputLength: 1,
        width:'100%',
        ajax: {
            url: "<?= base_url('entries/ledgersoptions') ?>",
            dataType: 'json',
            quietMillis: 15,
            data: function (term, page) {
                return {
                    term: term,
                    limit: 10
                };
            },
            results: function (data, page) {
                if (data.results != null) {
                    return {results: data.results};
                } else {
                    return {results: [{id: '', text: 'No Match Found'}]};
                }
            }
        }
    });
  }

  function setCompaniesSelect(){
  $('.companies_select').select2({
      minimumInputLength: 1,
      width:'100%',
      ajax: {
          url: "<?= base_url('entries/companiesoptions') ?>",
          dataType: 'json',
          quietMillis: 15,
          data: function (term, page) {
              return {
                  term: term,
                  limit: 10
              };
          },
          results: function (data, page) {
              if (data.results != null) {
                  return {results: data.results};
              } else {
                  return {results: [{id: '', text: 'No Match Found'}]};
              }
          }
      }
  });
}

</script>