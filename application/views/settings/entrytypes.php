<script>

    $(document).ready(function () {
        $('#loader').fadeIn();
        dataset1 = $('#dynamic-table').DataTable({
        ajax: {
            method: 'POST',
            url: '<?=base_url(); ?>account_settings/getAllET',
            dataType: 'json',
          },
          order: [ 0, 'asc' ],
          order: [ 1, 'desc' ],

        columns:[
            { data: '0'},
            { data: '1'},
            { data: '2'},
            { data: '3'},
            { data: '4'},
            { data: '5'},
            { data: '6'},
            { data: '7'},
          ],
              /*order: [ 0, 'asc' ],*/
        pageLength: 25,
        responsive: true,
        dom : '<"html5buttons" B>lr<"containerBtn"><"inputFiltro"f>tip',
        buttons : [{extend:'excel', title:'Entries', className:'btnExportarExcel', exportOptions: {columns : [0,1,2,3,4,5,6]}}],
        oLanguage: {
          sLengthMenu: 'Mostrando _MENU_ registros por página',
          sZeroRecords: 'No se encontraron registros',
          sInfo: 'Mostrando _START_ a _END_ de _TOTAL_ registros',
          sInfoEmpty: 'Mostrando 0 a 0 de 0 registros',
          sInfoFiltered: '(Filtrado desde _MAX_ registros)',
          sSearch:         'Buscar: ',
          oPaginate:{
            sFirst:    'Primero',
            sLast:     'Último',
            sNext:     'Siguiente',
            sPrevious: 'Anterior'
          }
        },
        "preDrawCallback": function( settings ) {
            
          }
        }).on("draw", function(){
         $('#loader').fadeOut();
       });
              
    });

</script>

<div class="row wrapper border-bottom white-bg page-heading">
  <div class="col-lg-8">
    <h2><?php echo $page_title; ?></h2>
    <ol class="breadcrumb">
      <li>
        <a href="login/index">Inicio</a> <?php //En el controlador en la función llamada, se revisa si el usuario está logueado y lo envía a la vista principal según permisos ?>
      </li>
      <li class="active">
        <strong><?php echo $page_title; ?></strong>
      </li>
    </ol>
  </div><!-- /.col -->
  <div class="col-lg-4">
    <div class="title-action">
      <div class="btn-group">
        <a href="<?=  base_url().'account_settings/addentrytype' ?>" class="btn btn-success"><span class="fa fa-plus"></span> <?= lang('settings_views_entrytypes_btn_add') ?></a>
      </div>  
    </div>
  </div>
</div><!-- /.row -->

<div class="row wrapper wrapper-content animated fadeInRight">
      <div class="box float-e-margins">
        <div class="ibox-content contentBackground">
          <div class="col-sm-12">
        <div class="box-header with-border">
            
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="responsive-table">
                <table class="display compact table table-bordered table-striped" id="dynamic-table">
                    <thead>
                        <tr>
                            <th><?= lang('settings_views_entrytypes_thead_label'); ?></th>
                            <th><?= lang('settings_views_entrytypes_thead_name'); ?></th>
                            <th><?= lang('settings_views_entrytypes_thead_description'); ?></th>
                            <th><?= lang('settings_views_entrytypes_thead_prefix'); ?></th>
                            <th><?= lang('settings_views_entrytypes_thead_suffix'); ?></th>
                            <th><?= lang('settings_views_entrytypes_thead_zero_padding'); ?></th>
                            <th>Origen</th>
                            <th><?= lang('settings_views_entrytypes_thead_actions'); ?></th>
                        </tr>
                    </thead>

                    <tbody>
                        
                    </tbody>
                        
                    <tfoot>
                        <tr>
                            <th><?= lang('settings_views_entrytypes_tfoot_label'); ?></th>
                            <th><?= lang('settings_views_entrytypes_tfoot_name'); ?></th>
                            <th><?= lang('settings_views_entrytypes_tfoot_description'); ?></th>
                            <th><?= lang('settings_views_entrytypes_tfoot_prefix'); ?></th>
                            <th><?= lang('settings_views_entrytypes_tfoot_suffix'); ?></th>
                            <th><?= lang('settings_views_entrytypes_tfoot_zero_padding'); ?></th>
                            <th>Origen</th>
                            <th><?= lang('settings_views_entrytypes_tfoot_actions'); ?></th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
      </div>
  </div>
  <!-- /.row -->
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="delete_entrytype" aria-labelledby="deleteaccount">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><?= lang('accounts_index_delete_entrytype_alert_title'); ?></h4>
      </div>
        <div class="modal-body">
          <input type="hidden" name="entrytype_id_delete" id="entrytype_id_delete">
          <?= lang('accounts_index_delete_entrytype_alert_text'); ?>
        </div>
        <div class="modal-footer">
          <button class="btn btn-default" data-dismiss="modal"><?=lang('no');?></button>
          <button class="btn btn-danger confirm_delete_entrytype" ><?=lang('yes');?></button>
        </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">

    jQuery(".add_c").on("click", function (e) {
        $('#clientmodal').modal('show');

        jQuery('#et_label').val('');
        jQuery('#et_name').val('');
        jQuery('#description').val('');
        jQuery('#numbering').val('');
        jQuery('#prefix').val('');
        jQuery('#suffix').val('');
        jQuery('#zero_padding').val('');
        jQuery('#restriction_bankcash').val('');
            
        
        jQuery('#titclienti').html("<?= lang('settings_views_entrytypes_btn_add');?>");

        jQuery('#footerClient1').html('<button data-dismiss="modal" class="btn btn-default" type="button"><?= lang('settings_views_entrytypes_modal_footer_btn_cancel'); ?></button><button role="submit" form="myForm" id="submit" class="btn btn-success" data-mode="add"><i class="fa fa-check"></i><?= lang('settings_views_entrytypes_modal_footer_btn_submit'); ?></button>');
    });

    jQuery(document).on("click", "#modify", function () {
        jQuery('#titclienti').html("<?= lang('settings_views_entrytypes_edit');?>");
        var num = jQuery(this).data("num");
            jQuery.ajax({
                type: "POST",
                url: "<?= base_url(); ?>account_settings/entrytypes/getByID",
                data: "id=" + encodeURI(num),
                cache: false,
                dataType: "json",
                success: function (data) {
                    jQuery('#et_label').val(data.label);
                    jQuery('#et_name').val(data.name);
                    jQuery('#description').val(data.description);
                    jQuery('#numbering').val(data.numbering);
                    jQuery('#prefix').val(data.prefix);
                    jQuery('#suffix').val(data.suffix);
                    jQuery('#zero_padding').val(data.zero_padding);
                    jQuery('#restriction_bankcash').val(data.restriction_bankcash);

                    jQuery('#footerClient1').html('<button data-dismiss="modal" class="btn btn-default" type="button"> <?= lang('settings_views_entrytypes_modal_footer_btn_cancel'); ?></button><button role="submit" form="myForm" id="submit" class="btn btn-success" data-mode="modify" data-num="' + encodeURI(num) + '"><i class="fa fa-check"></i> <?= lang('settings_views_entrytypes_btn_save');?></button>')
                }
            });
        });

$( "#myForm" ).submit(function( event ) {
        event.preventDefault();
        var mode = jQuery('#submit').data("mode");
        var id = jQuery('#submit').data("num");

        //validate
        var valid = true;

        if (valid) {
            var url = "";
            var dataString = $('form').serialize();

            if (mode == "add") {
                url = "<?= base_url(); ?>" + "account_settings/entrytypes/add";
                jQuery.ajax({
                    type: "POST",
                    url: url,
                    data: dataString,
                    cache: false,
                    success: function (data) {
                        toastr['success']("<?= lang('settings_views_entrytypes_added'); ?>");
                        setTimeout(function () {
                            $('#clientmodal').modal('hide');
                            $('#dynamic-table').DataTable().ajax.reload();
                        }, 500);
                    }
                });
            } else {
                url = "<?= base_url(); ?>" + "account_settings/entrytypes/edit";
                dataString += "&id=" + encodeURI(id);
                jQuery.ajax({
                    type: "POST",
                    url: url,
                    data: dataString,
                    cache: false,
                    success: function (data) {
                        toastr['success']("<?= lang('settings_views_entrytypes_update'); ?>");
                        setTimeout(function () {
                            $('#clientmodal').modal('hide');
                            $('#dynamic-table').DataTable().ajax.reload();
                        }, 500);
                    }
                });
            }
        }
        return false;
    });


    $('#delete_entrytype').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        ETid = button.data('num');
        $('#entrytype_id_delete').val(ETid);
    });

    $('.confirm_delete_entrytype').on('click', function(){
         toastr.options = {
                      "closeButton": true,
                      "debug": false,
                      "progressBar": true,
                      "preventDuplicates": false,
                      "positionClass": "toast-top-right",
                      "onclick": null,
                      "showDuration": "400",
                      "hideDuration": "1000",
                      "timeOut": "2000",
                      "extendedTimeOut": "1000",
                      "showEasing": "swing",
                      "hideEasing": "linear",
                      "showMethod": "fadeIn",
                      "hideMethod": "fadeOut"
                    }

        ETid = $('#entrytype_id_delete').val();
         jQuery.ajax({
            type: "POST",
            url: "<?=base_url(); ?>" + "account_settings/entrytypes/delete",
            data: "id=" + encodeURI(ETid),
            cache: false,
            dataType: "json",
            success: function (data) {
                if (data == 'true') {
                    toastr['success']("<?= lang('delete'); ?>: ", "<?= lang('settings_views_entrytypes_deleted'); ?>");
                    $('#delete_entrytype').modal('hide');
                }else{
                    toastr['error']("<?= lang('settings_views_entrytypes_deleted_error'); ?>");

                }
                $('#dynamic-table').DataTable().ajax.reload();
            }
        });
    });
</script>