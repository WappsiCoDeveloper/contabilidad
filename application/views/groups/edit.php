<script type="text/javascript">
$(document).ready(function() {
	/**
	 * On changing the parent group select box check whether the selected value
	 * should show the "Affects Gross Profit/Loss Calculations".
	 */


	 var niveles = <?= json_encode($niveles); ?>;

    console.log(niveles);

	var niveles = <?= json_encode($niveles); ?>;

    console.log(niveles);

	$('#GroupParentId').change(function() {
		if ($(this).val() == '3' || $(this).val() == '4') {
			$('#AffectsGross').show();
		} else {
			$('#AffectsGross').hide();
		}

		var parentGroup = $(this).val();


		if ($.inArray(parentGroup, niveles[1]) !== -1) {
			$('#g_code').prop('maxlength', '2');
		} else if ($.inArray(parentGroup, niveles[2]) !== -1) {
			$('#g_code').prop('maxlength', '4');
		} else if ($.inArray(parentGroup, niveles[3]) !== -1) {
			$('#g_code').prop('maxlength', '6');
		} else if ($.inArray(parentGroup, niveles[4]) !== -1 || $.inArray(parentGroup, niveles[5]) !== -1){
			$('#g_code').removeAttr('maxlength');
		}

	});

	$('#GroupParentId').trigger('change');

	$("#GroupParentId").select2({width:'100%'});
});
</script>

<style type="text/css">
.select2-container--default .select2-results__option {
	font-weight: bold;
	color: #333;
}
</style>

<div class="row wrapper border-bottom white-bg page-heading">
  <div class="col-lg-8">
    <h2><?php echo $page_title; ?></h2>
    <ol class="breadcrumb">
      <li>
        <a href="login/index">Inicio</a> <?php //En el controlador en la función llamada, se revisa si el usuario está logueado y lo envía a la vista principal según permisos ?>
      </li>
      <li class="active">
        <strong><?php echo $page_title; ?></strong>
      </li>
    </ol>
  </div><!-- /.col -->
  <div class="col-lg-4">
    <div class="title-action">
    	<button class="btn btn-success submit"><span class="fa fa-check"></span> <?= lang('entries_views_edit_label_submit_btn'); ?></button>
    </div>
  </div>
</div>
<!-- Main content -->
    <section class="row wrapper wrapper-content animated fadeInRight">
      <!-- Small boxes (Stat box) -->
      <div class="ibox float-e-margins">
        <!-- ./col -->
        <div class="ibox-content contentBackground">
          <div class="col-xs-12">
            <div class="box-header with-border">
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            
				<div class="groups edit form">
				<?php
					echo form_open('', array('id' => 'formEditGroup'));	
					echo "<div class='row'>";

						if ($group['parent_id'] == 0) {
						echo "<div class='col-xs-5' style='display:none;'>";
						$input = form_input('parent_id','0', array('class' => 'form-control'));
						} else {
						echo "<div class='col-xs-5'>";
						$input = form_dropdown('parent_id', $parents, set_value('parent_id', $group['parent_id']),array('class' => 'form-control', 'id'=>'GroupParentId'));
						}
							echo '<div class="form-group">';
							echo form_label(lang('groups_views_edit_label_parent_group'), 'parent_id')."<span class='input_required'> *</span>";
							echo $input;
							echo "</div>";
						echo "</div>";

						echo "<div class='col-xs-2'>";
							echo '<div class="form-group">';
							echo form_label(lang('groups_views_edit_label_group_code'), 'code')."<span class='input_required'> *</span>";
							if ($group['parent_id'] == 0) {
							echo form_input('code',set_value('code', $group['code']) ,array('class' => 'form-control', 'readonly' => 'readonly'));
							} else {
							echo form_input('code',set_value('code', $group['code']) ,array('class' => 'form-control', 'id' => 'g_code'));
							}
								
							echo "</div>";
						echo "</div>";
						echo "<div class='col-xs-5'>";
							echo '<div class="form-group">';
							echo form_label(lang('groups_views_edit_label_group_name'), 'name')."<span class='input_required'> *</span>";
							echo form_input('name',set_value('name', $group['name']) ,array('class' => 'form-control'));
							echo "</div>";
						echo "</div>";
					echo "</div>";
					
					echo '<div class="form-group">';
					echo form_submit('submit', lang('entries_views_edit_label_submit_btn'), array('class' => 'btn btn-success btnSubmit')); ?>
						<a href="<?=base_url(); ?>accounts" class="btn btn-default" style="margin-right: 5px;"><?= lang('ledgers_views_edit_label_cancel_btn'); ?></a>
					<?php
					echo '<span class="link-pad"></span>';
					// echo anchor('accounts/index', lang('entries_views_edit_label_cancel_btn'), array('class' => 'btn btn-default pull-right', 'style' => "margin-right: 5px;"));
					echo '</div>';

					echo form_close();
					?>
					
				</div>
            </div>
          </div>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->

    <script type="text/javascript">
    	$('.submit').on('click', function(){
    		$('.btnSubmit').click();
    	});
    </script>