<script type="text/javascript">
$(document).ready(function() {
	/**
	 * On changing the parent group select box check whether the selected value
	 * should show the "Affects Gross Profit/Loss Calculations".
	 */
	$("#GroupParentId").select2({width:'100%'});


	$.ajax({
    	type:"POST",
        url: "<?=base_url(); ?>" + "groups/getLastParent"
    }).done(function(msg){
    	console.log(msg);
    	if (msg > 9) {
    		$('#g_code').val(0).prop('disabled', true);
    		$('#g_name').prop('disabled', true);
    		$('.submit').prop('disabled', true);
    	} else {
    		$('#g_code').val(msg);
    	}
    });


    $(document).on('keypress', 'form', function(e){   
        if(e == 13){
          return false;
        }
      });

    $(document).on('keypress', 'input', function(e){
        if(e.which == 13){
          return false;
        }
      });

});

</script>

<style type="text/css">
.select2-container--default .select2-results__option {
	font-weight: bold;
	color: #333;
}
</style>

<div class="row wrapper border-bottom white-bg page-heading">
  <div class="col-lg-8">
    <h2><?= $page_title; ?></h2>
    <ol class="breadcrumb">
      <li>
        <a href="login/index">Inicio</a> <?php //En el controlador en la función llamada, se revisa si el usuario está logueado y lo envía a la vista principal según permisos ?>
      </li>
      <li class="active">
        <strong><?= $page_title; ?></strong>
      </li>
    </ol>
  </div><!-- /.col -->
  <div class="col-lg-4">
    <div class="title-action">
    	<button class="btn btn-success submit" onclick='$("#formAddGroup").submit();'><span class="fa fa-check"></span> <?= lang('entries_views_edit_label_submit_btn'); ?></button>
    </div>
  </div>
</div>


<!-- Main content -->
    <div class="row wrapper wrapper-content animated fadeInRight">
      <!-- Small boxes (Stat box) -->
      <div class="ibox float-e-margins">
        
        <!-- ./col -->
        <div class="ibox-content contentBackground">
          <div class="col-xs-12">
            <div class="box-header with-border">
              <h3 class="box-title"><?= lang('entries_views_add_title') ?></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            	<?= validation_errors('<div class="alert alert-danger">', '</div>'); ?>
             	<div class="groups add form">
					<?php
						echo form_open('', array('id' => 'formAddGroup'));

						echo "<div class='row'>";					
						echo "<div class='col-xs-2'>";
							echo '<div class="form-group">';
							echo form_label(lang('groups_views_add_label_group_code'), 'code')."<span class='input_required'> *</span>";
							echo form_input('code', set_value('code') ,array('class' => 'form-control', 'id'=> 'g_code'));
							echo "</div>";	
						echo "</div>";
						echo "<div class='col-xs-5'>";
							echo '<div class="form-group">';
							echo form_label(lang('groups_views_add_label_group_name'), 'name')."<span class='input_required'> *</span>";
							echo form_input('name', set_value('name') ,array('class' => 'form-control', 'id'=>'g_name'));
							echo "</div>";		
						echo "</div>";
						echo "</div>";
					
						echo '<div class="form-group">';
						echo form_submit('submit', lang('entries_views_add_label_submit_btn'), array('class' => 'btn btn-success submit'));
						echo '<a href="'.base_url().'accounts" class="btn btn-default">'.lang('ledgers_views_add_label_cancel_btn').'</a>';
						echo '</div>';

						echo form_close();
					?>
				</div>
            </div>
          </div>
      </div>
      <!-- /.row -->
    </div>
    <!-- /.content -->