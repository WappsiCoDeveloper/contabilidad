<style type="text/css">
  .pnl-dashboard {
    height: 260px;
    overflow: auto;
  }
  .pnl-stream-activity {
    height: 550px;
    overflow: auto;
  }
</style>


<div class="row wrapper wrapper-content animated fadeInRight">
      <div class="ibox float-e-margins col-sm-12">
        <div class="ibox-content contentBackground panel-group" id="accordion" role="tablist">
          <?php
          /* Check if BC Math Library is present */
          if (!extension_loaded('bcmath')) {
            echo '<div><div role="alert" class="alert alert-danger">'. lang('user_views_dashboard_error_alert_php_bc_math_lib_missing') .'</div></div>';
          }
          ?>
          <h3></h3>
          <div class="">
            <div class="row">

  <?php 

  $fys = $this->db->get('accounts');
  $num_fys = $fys->num_rows();
   ?>

      <div class="col-sm-3">
          <div class="ibox">
              <div class="ibox-content pnl pnl-1 row">
                  <div class="col-sm-4 bnc" onclick="location.href='<?= base_url(); ?>entries';" style="cursor:pointer;">
                    <span class="fa fa-file-text-o pnl-icon"></span>
                  </div>
                  <div class="col-sm-8">
                    <h3><?= lang('dashboard_entries') ?></h3>
                    <div class="dropdown">
                      <h1 class="dropdown-toggle" id="entries" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="cursor: pointer;"><span class="fa fa-plus-square-o"></span> Crear <span class="caret"></span></h1>
                      <ul class="dropdown-menu" aria-labelledby="entries">
                        <?php 
                          foreach($this->DB1->where('entrytypes'.$this->DB1->dbsuffix.'.origin', 1)->get('entrytypes'.$this->DB1->dbsuffix)->result_array() as $entrytype): ?>
                            <li>
                              <a href="<?= base_url(); ?>entries/add/<?=$entrytype['label']?>" style="color:black; font-weight:600;"><?= $entrytype['name']; ?></a>
                            </li>
                          <?php endforeach; ?>
                      </ul>
                    </div>
                  </div>
              </div>
          </div>
      </div>

      <div class="col-sm-3">
          <div class="ibox">
              <div class="ibox-content pnl pnl-2 row">
                  <div class="col-sm-4 bnc" onclick="location.href='<?= base_url(); ?>accounts';" style="cursor:pointer;">
                    <span class="fa fa-sitemap pnl-icon"></span>
                  </div>
                  <div class="col-sm-8">
                    <h3><?= lang('dashboard_account_list') ?></h3>
                    <div class="dropdown">
                      <h1 class="dropdown-toggle" id="entries" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="cursor: pointer;"><span class="fa fa-plus-square-o"></span> Crear <span class="caret"></span></h1>
                      <ul class="dropdown-menu" aria-labelledby="entries">
                         <li>
                            <a href="<?= base_url(); ?>groups/addParent" style="color:black; font-weight:600;"><?= lang('accounts_index_add_parent_group_btn'); ?></a>
                         </li>
                         <li>
                            <a href="<?= base_url(); ?>groups/add" style="color:black; font-weight:600;"><?= lang('accounts_index_add_group_btn'); ?></a>
                         </li>
                         <li>
                            <a href="<?= base_url(); ?>ledgers/add" style="color:black; font-weight:600;"><?= lang('accounts_index_add_ledger_btn'); ?></a>
                         </li>
                      </ul>
                    </div>
                  </div>
              </div>
          </div>
      </div>

      <div class="col-sm-3">
          <div class="ibox">
              <div class="ibox-content pnl pnl-3 row">
                  <div class="col-sm-4 bnc" onclick="location.href='<?= base_url(); ?>reports/trialbalance';" style="cursor:pointer;">
                    <span class="fa fa-bar-chart pnl-icon"></span>
                  </div>
                  <div class="col-sm-8">
                    <h3><?= lang('dashboard_reports') ?></h3>
                    <div class="dropdown">
                      <h1 class="dropdown-toggle" id="entries" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="cursor: pointer;"><span class="fa fa-external-link"></span> Ver <span class="caret"></span></h1>
                      <ul class="dropdown-menu" aria-labelledby="entries">
                         <li>
                            <a href="<?= base_url(); ?>reports/balancesheet" style="color:black; font-weight:600;">Balance general</a>
                         </li>
                         <li>
                            <a href="<?= base_url(); ?>reports/profitloss" style="color:black; font-weight:600;">Ganancias y pérdidas</a>
                         </li>
                         <li>
                            <a href="<?= base_url(); ?>reports/trialbalance" style="color:black; font-weight:600;">Balance de prueba</a>
                         </li>
                         <li>
                            <a href="<?= base_url(); ?>reports/ledgerstatement" style="color:black; font-weight:600;">Auxiliar de cuentas</a>
                         </li>
                         <li>
                            <a href="<?= base_url(); ?>reports/thirdsaccounts" style="color:black; font-weight:600;">Auxiliar de cuentas/terceros</a>
                         </li>
                      </ul>
                    </div>
                  </div>
              </div>
          </div>
      </div>  

      <div class="col-sm-3">
          <div class="ibox">
              <div class="ibox-content pnl pnl-4 row">
                  <div class="col-sm-4 bnc" onclick="location.href='<?= base_url(); ?>thirds';" style="cursor:pointer;">
                    <span class="fa fa-handshake-o pnl-icon"></span>
                  </div>
                  <div class="col-sm-8">
                    <h3><?= lang('dashboard_thirds') ?></h3>
                      <h1 class="dropdown-toggle" id="entries" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="cursor: pointer;" onclick="location.href='<?= base_url(); ?>thirds/add';"><span class="fa fa-plus-square-o"></span> Crear </h1>
                  </div>
              </div>
          </div>
      </div>             
            </div>
          </div>
          <hr class="col-sm-12">

          <!-- small box -->
             <div class="col-sm-6">
              <div class="panel">
                <!-- Add the bg color to the header using any of the bg-* classes -->
                <div class="panel-heading">
                  <h3 class="panel-title"><?= lang('user_dashboard_account_info_title'); ?></h3>
                </div>
                <div class="panel-body pnl-dashboard">
                  <table class="table">
                    <tr>
                      <th><?php echo lang('settings_views_main_label_company_name'); ?> </th>
                      <td><?php echo ($this->mAccountSettings->name); ?></td>
                    </tr>
                    <tr>
                      <th><?php echo lang('settings_views_cf_label_fiscal_year'); ?> </th>
                      <td><?php echo $this->functionscore->dateFromSql($this->mAccountSettings->fy_start) . ' to ' . $this->functionscore->dateFromSql($this->mAccountSettings->fy_end); ?></td>
                    </tr>
                    <tr>
                      <th><?php echo lang('settings_views_main_label_email'); ?></th>
                      <td><?php echo ($this->mAccountSettings->email); ?></td>
                    </tr>
                    <tr>
                      <th><?php echo lang('dashboard_company_role'); ?></th>
                      <td><?= ($this->ion_auth->get_users_groups()->row()->description); ?></td>
                    </tr>
                    <tr>
                      <th><?php echo lang('admin_cntrler_create_account_validation_currency'); ?></th>
                      <td><?php echo ($this->mAccountSettings->currency_symbol); ?></td>
                    </tr>
                    <tr>
                      <th><?php echo lang('settings_views_cf_label_status'); ?></th>
                      <td><?= ($this->mAccountSettings->account_locked == 0) ? '<span class="pull-right badge bg-green">'.lang('settings_views_cf_label_unlocked').'</span>' : '<span class="pull-right badge bg-red">'.lang('settings_views_cf_label_locked').'</span>';
                        ?></td>
                    </tr>
                  </table>
                </div>
              </div>
           </div>
           <div class="col-sm-6">
              <div class="panel">
                <div class="panel-heading">
                  <h3 class="panel-title">
                    <?= lang('dashboard_bc_summary'); ?>  
                  </h3>
                  
                  <!-- /.box-tools -->
                </div>
                <!-- /.box-header -->
                <div class="panel-body panel-collapse collapse in pnl-dashboard" id="collapseOne" role="tabpanel">
                  <table class="table">
                  <?php
                    foreach ($ledgers as $ledger) {
                      echo '<tr>';
                      echo '<td>' . $ledger['name'] . '</td>';
                      echo '<td>' . $this->functionscore->toCurrency($ledger['balance']['dc'], $ledger['balance']['amount']) . '</td>';
                      echo '</tr>';
                    }
                  ?>
                  </table>
                </div>
                <!-- /.box-body -->
              </div>
           </div>
         </div>
      </div>
      <div class="ibox float-e-margins col-sm-12">
        <div class="ibox-content contentBackground panel-group" id="accordion" role="tablist">
           <div class="">
               <div class="panel">
                <div class="panel-heading">
                  <h3 class="panel-title">
                    Income And Expense
                  </h3>
                  <!-- /.box-tools -->
                </div>
                <!-- /.box-header -->
                <div class="panel-body panel-collapse collapse in" id="collapseTwo" role="tabpanel">
                  <div id="d_chart" style="height: 350px;"></div>
                </div>
                <!-- /.box-body -->
              </div>
           </div>
         </div>
      </div>
      <div class="col-sm-6" style="padding-left: 0 !important;">
        <div class="ibox float-e-margins">
          <div class="ibox-content contentBackground panel-group" id="accordion" role="tablist">
                 <div class="panel">
                  <div class="panel-heading">
                    <h3 class="panel-title">
                      <?= lang('dashboard_b_summary'); ?>
                    </h3>

                    <div class="box-tools pull-right">
                    </div>
                    <!-- /.box-tools -->
                  </div>
                  <!-- /.box-header -->
                  <div class="panel-body panel-collapse collapse in" id="collapseThree" role="tabpanel">
                    <div id="balance_summary" style="height: 350px;"></div>
                    
                    <table class="table">
                      <tr>
                        <td><?php echo ('Assets'); ?></td>
                        <td><?php echo $this->functionscore->toCurrency($accsummary['assets_total_dc'], $accsummary['assets_total']); ?></td>
                      </tr>
                      <tr>
                        <td><?php echo (' Liabilities and Owners Equity'); ?></td>
                        <td><?php echo $this->functionscore->toCurrency($accsummary['liabilities_total_dc'], $accsummary['liabilities_total']); ?></td>
                      </tr>
                      <tr>
                        <td><?php echo ('Income'); ?></td>
                        <td><?php echo $this->functionscore->toCurrency($accsummary['income_total_dc'], $accsummary['income_total']); ?></td>
                      </tr>
                      <tr>
                        <td><?php echo ('Expense'); ?></td>
                        <td><?php echo $this->functionscore->toCurrency($accsummary['expense_total_dc'], $accsummary['expense_total']); ?></td>
                      </tr>
                    </table>

                  </div>
                  <!-- /.box-body -->
                </div>
           </div>
        </div>
      </div>
      <div class="col-sm-6" style="padding-right: 0 !important;">
        <div class="ibox ">
          <div class="ibox-title">
              <h5><?= lang('recent_activity'); ?></h5>
          </div>

          <div class="ibox-content pnl-stream-activity">

              <div class="activity-stream">
                    <?php

                      if (count($logs) <= 0) {
                        echo lang('no_records_found');
                      } else {
                        foreach ($logs as $row => $data) { 
                          $usuario = $users[$data['user_id']];

                          $fecha_hora = explode(" ", $data['date']);

                          // var_dump($users);
                          ?>
                          <div class="stream">
                            <div class="stream-badge">
                                <i class="fa fa-pencil"></i>
                            </div>
                            <div class="stream-panel">
                                <div class="stream-info">
                                    <b>
                                        <img src="<?= base_url().'assets/uploads/users/'.$usuario['image'] ?>">
                                        <span><?= $usuario['first_name']." ".$usuario['last_name'] ?></span>
                                        <span class="date"><?= $this->functionscore->dateFromSql($fecha_hora[0]) ?></span>
                                    </b>
                                </div>
                                <?= $data['message'] ?>
                            </div>
                          </div>
                        <?php }
                      }
                    ?>
              </div>
          </div>
        </div>
      </div>
      <!-- /.row -->
    </div>
    <!-- /.content -->
  <script src="<?= base_url(); ?>assets/bootstrap/js/plugins/echarts/echarts.min.js"></script>

    <script type="text/javascript">
        var c_month = '<?= date('F'); ?>';
        var c_year = '<?= date('Y'); ?>';
        var ib_graph_primary_color = '#2196f3';
        var ib_graph_secondary_color = '#eb3c00';



    $.getJSON( "<?= base_url(); ?>dashboard/getIncomeExpenseChart/", function( data ) {
        // console.log(data.Months);

        var c3_opt =  {
            title : {
                text: c_month + ', ' + c_year,
            },
            tooltip : {
                trigger: 'axis'
            },
            legend: {
                data:['Income','Expense']
            },
            toolbox: {
                show : true,
                feature : {
                    mark : {show: true},
                    dataView : {show: true, readOnly: false, title : 'Data View',
                        lang: ['Data View', 'Cancel', 'Reset'] },
                    magicType : {show: true, title : {
                        line : 'Line',
                        bar : 'Bar',
                        stack : 'Stack',
                        tiled : 'Tiled',
                        force: 'Force',
                        chord: 'Chord',
                        pie: 'Pie',
                        funnel: 'Funnel'
                    }, type: ['line', 'bar', 'stack', 'tiled']},
                    restore : {show: true, title : 'Reset'},
                    saveAsImage : {
                      show: true, 
                      title : 'Save as Image',
                      type : 'png',
                      lang : ['Click to Save']
                    }
                }
            },
            calculable : true,
            xAxis : [
                {
                    type : 'category',
                    boundaryGap : false,
                    data : [
                        '01',
                        '02',
                        '03',
                        '04',
                        '05',
                        '06',
                        '07',
                        '08',
                        '09',
                        '10',
                        '11',
                        '12',
                        '13',
                        '14',
                        '15',
                        '16',
                        '17',
                        '18',
                        '19',
                        '20',
                        '21',
                        '22',
                        '23',
                        '24',
                        '25',
                        '26',
                        '27',
                        '28',
                        '29',
                        '30',
                        '31'
                    ]
                }
            ],
            yAxis : [
                {
                    type : 'value'
                }
            ],
            series : [
                {
                    name:'Income',
                    type:'line',
                    color: [
                        ib_graph_primary_color
                    ],
                    smooth:true,
                    itemStyle: {normal: {areaStyle: {type: 'default'}}},
                    data:data.Income
                },
                {
                    name:'Expense',
                    type:'line',
                    color: [
                        ib_graph_secondary_color
                    ],
                    smooth:true,
                    itemStyle: {normal: {areaStyle: {type: 'default'}}},
                    data:data.Expense
                }
            ]
        };


        var c3_d = echarts.init(document.getElementById('d_chart'));
        c3_d.setOption(c3_opt);

    });
    pie_options = {
      title : {
          text: 'Balance Summary',
          x:'center'
      },
      tooltip : {
          trigger: 'item',
          formatter: "{a} <br/>{b} : {c} ({d}%)"
      },
      legend: {
          orient: 'vertical',
          left: 'left',
          data: ['Assets','Liabilities and Owners Equity','Income','Expense']
      },
      series : [
          {
              name: 'Balance Summary',
              type: 'pie',
              radius : '55%',
              center: ['50%', '60%'],
              data:[
                  {value:<?php echo $accsummary['assets_total'] ?>, name:'Assets'},
                  {value:<?php echo $accsummary['liabilities_total']; ?>, name:'Liabilities and Owners Equity'},
                  {value:<?php echo $accsummary['income_total']; ?>, name:'Income'},
                  {value:<?php echo $accsummary['expense_total']; ?>, name:'Expense'},
              ],
              itemStyle: {
                  emphasis: {
                      shadowBlur: 10,
                      shadowOffsetX: 0,
                      shadowColor: 'rgba(0, 0, 0, 0.5)'
                  }
              }
          }
      ]
  };
  var pie = echarts.init(document.getElementById('balance_summary'));
  pie.setOption(pie_options);

    </script>