
<style type="text/css">
  
.caja-login {
    color: white;
}

h2{
  margin-top: 3% !important;
  margin-bottom: 8% !important;
}

.middle-box{
  -webkit-box-shadow: 0px 20px 24px 1px rgba(0,0,0,0.75);
  -moz-box-shadow: 0px 20px 24px 1px rgba(0,0,0,0.75);
  box-shadow: 0px 20px 24px 1px rgba(0,0,0,0.75);
}

.icon{
      color: black;
    background-color: #00000021;
    font-weight: 600;
}

.loginscreen{
  margin-top: 13%;
}


.pace-done {
    background-color: #1c84c6 !important;
}

.form-control {
  color:black;
}

</style>

<div class="row">
  <div class="col-sm-3">
    
  </div>
</div>
<div class="middle-box text-center loginscreen animated fadeInDown caja-login">
  <div class="">
    <!-- <h2 onclick="location.href='<?= base_url(); ?>'"><?= $settings->sitename; ?></h2> -->
    <img src="../../contabilidad/assets/uploads/logo_contabilidad.png" style="width: 260px;">
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <div class="flash-msg">
      <?php if (validation_errors()) { ?>
        <div class="alert alert-danger">
          <?= validation_errors(); ?>
        </div>
      <?php } ?>
      <?php if (isset($_SESSION['message'])) { ?>
        <div class="alert alert-success">
          <?= $_SESSION['message']; ?>
        </div>
      <?php } ?>
      <?php if (isset($_SESSION['error'])) { ?>
        <div class="alert alert-danger">
          <?= ($_SESSION['error']); ?>
        </div>
      <?php } ?>
      <?php if (isset($_SESSION['warning'])) { ?>
          <div class="alert alert-warning">
              <?= ($_SESSION['warning']); ?>
          </div>
    <?php } ?>
    </div>
    <b><?= lang('login_subheading'); ?></b>



    <?= form_open('login'); ?>
      <div class="form-group has-feedback">
        <?php echo form_input($identity); ?>
        <span class="glyphicon glyphicon-envelope form-control-feedback icon"></span>
      </div>
      <div class="form-group has-feedback">
        <?php echo form_input($password); ?>
        <span class="glyphicon glyphicon-lock form-control-feedback icon"></span>
      </div>
      <div class="row">
        <!-- /.col -->
        <div class="col-xs-12">
          <button type="submit" class="btn btn-success " style="width: 100%"><b><?= lang('login_submit_btn'); ?></b></button>
        </div>
        <!-- /.col -->
        <div class="col-xs-12">
              <div class="checkbox">
                <input type="checkbox"> <?= lang('login_remember_label'); ?>
              </div>
        </div>
      </div>
    <?= form_close(); ?>
      <div class="row">
        <div class="col-xs-12">
          <span>¿Olvidó su contraseña?, para recuperarla haga <a href="#">clic aquí</a></span>
        </div>
      </div>
  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<script type="text/javascript">
  $('.pace-done').addClass('fnd-login');
</script>

  