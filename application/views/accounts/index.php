

<style type="text/css">
  .tr-root-group{
    background-color: #e4f3fd;
    color: #333;
  }
  .paddingbotton{
    padding-bottom: 5%;
  }
</style>

<script type="text/javascript">$('#loader').fadeIn();</script>

<div class="row wrapper border-bottom white-bg page-heading">
  <div class="col-lg-8">
    <h2><?php echo $page_title; ?></h2>
    <ol class="breadcrumb">
      <li>
        <a href="login/index">Inicio</a> <?php //En el controlador en la función llamada, se revisa si el usuario está logueado y lo envía a la vista principal según permisos ?>
      </li>
      <li class="active">
        <strong><?php echo $page_title; ?></strong>
      </li>
    </ol>
  </div><!-- /.col -->
  <div class="col-lg-4">
    <div class="title-action">
      <div class="btn-group">
        <div class="dropdown">
          <button class="btn btn-success" type="button" id="accionesGrupo" data-toggle="dropdown" aria-haspopup="true">
            <span class="fa fa-plus"></span> Nuevo
            <span class="caret"></span>
          </button>
          <ul class="dropdown-menu pull-right" aria-labelledby="accionesGrupo">
           <li>
              <a href="<?= base_url(); ?>groups/addParent"><span class="fa fa-plus"></span>  <?= lang('accounts_index_add_parent_group_btn'); ?></a>
           </li>
           <li>
              <a href="<?= base_url(); ?>groups/add"><span class="fa fa-plus"></span>  <?= lang('accounts_index_add_group_btn'); ?></a>
           </li>
           <li>
              <a href="<?= base_url(); ?>ledgers/add"><span class="fa fa-plus"></span>  <?= lang('accounts_index_add_ledger_btn'); ?></a>
           </li>
          </ul>
        </div>
      </div>  
    </div>
  </div>
</div><!-- /.row -->

<?php


function print_account_chart($account, $c, $THIS)
{
  $account = isset($account[0]) ? $account[0] : $account;
  if (empty($account)) {
    return [];
  }
  $CI =& get_instance();

  $counter = $c;
  /* Print groups */
  if ($account['id'] != 0) {
    if (strlen($account['code']) == 1) {
      echo '<tr class="tr-root-group">';
    } else {
      echo '<tr>';
    }
    echo '<th>';
    // echo print_space($counter);
    echo $account['code'];
    echo '</th>';
    echo '<th class="td-group">';
    // echo print_space($counter);
    echo $account['name'];
    echo '</th>';

    echo '<th>'.lang('accounts_index_td_label_group').'</th>';

    echo '<th style="text-align:right;">' . $CI->functionscore->toCurrency(( $account['op_total'] <0 ? 'C' : 'D'), ( $account['op_total'] < 0 ?  $account['op_total'] * -1 :  $account['op_total'])) . '</th>';

    // if ($THIS->mAccountSettings->default_loading_accounts_index == 1) {
        echo '<th style="text-align:right;">' . $CI->functionscore->toCurrency(($account['cl_total'] < 0 ? 'C' : 'D'), ($account['cl_total'] < 0 ? $account['cl_total'] * -1 : $account['cl_total'])) . '</th>';    
    // }
      

    /* If group id less than 4 dont show edit and delete links */
    if (strlen($account['code']) == 1) {
      echo '<td class="td-actions">';
      $dropdown =
      '<div class="btn-group">
          <div class="dropdown">
            <button class="btn btn-success btn-sm" type="button" id="accionesGrupo" data-toggle="dropdown" aria-haspopup="true">
              Acciones
              <span class="caret"></span>
            </button>
            <ul class="dropdown-menu pull-right" aria-labelledby="accionesGrupo">
             <li>'
              .anchor('groups/edit/'.$account['id'], '<i class="glyphicon glyphicon-edit"></i>'.lang('accounts_index_edit_btn'), array('class' => 'no-hover font-normal', 'escape' => false)).
             '</li>
             <li>
              <a href="#delete_account_modal" data-toggle="modal" data-account="'.$account['id'].'" data-ledger="0"><span class="fa fa-trash"></span> Eliminar</a>
             </li>
            </ul>
          </div>
        </div>';
        echo $dropdown;
    } else {
      $dropdown =
      '<div class="btn-group">
          <div class="dropdown">
            <button class="btn btn-success btn-sm" type="button" id="accionesGrupo" data-toggle="dropdown" aria-haspopup="true">
              Acciones
              <span class="caret"></span>
            </button>
            <ul class="dropdown-menu pull-right" aria-labelledby="accionesGrupo">
             <li>'
              .anchor('groups/edit/'.$account['id'], '<i class="glyphicon glyphicon-edit"></i>'.lang('accounts_index_edit_btn'), array('class' => 'no-hover font-normal', 'escape' => false)).
             '</li><li>
              <a href="#delete_account_modal" data-toggle="modal" data-account="'.$account['id'].'" data-ledger="0"><span class="fa fa-trash"></span> Eliminar</a>
             </li>
            </ul>
          </div>
        </div>';
      echo '<td class="td-actions">';
      echo $dropdown;
      echo '</td>';
    }
    echo '</tr>';
  }

  /* Print child groups recursively */
  if (isset($account['children_groups'])) {
    foreach ($account['children_groups'] as $id => $data) {
      $counter++;
      print_account_chart($data, $counter, $THIS);
      $counter--;
    }
  }
    

  /* Print child ledgers */
  if (isset($account['children_ledgers']) && count($account['children_ledgers']) >= 1) {
    $counter++;
    foreach ($account['children_ledgers'] as $id => $data) {
      echo '<tr class="tr-ledger">';
      echo '<td class="td-ledger">';
      // echo print_space($counter);
      echo anchor('reports/ledgerstatement/ledgerid/'.$data['id'], $data['code']);
      echo '</td>';
      echo '<td class="td-ledger">';
      // echo print_space($counter);
      //to change later
      echo anchor('reports/ledgerstatement/ledgerid/'.$data['id'], $data['name']); 
      echo '</td>';
      echo '<td>'.lang('accounts_index_td_label_ledger').'</td>';

      echo '<td style="text-align:right">';
      echo $CI->functionscore->toCurrency(( $data['op_total'] <0 ? 'C' : 'D'), ( $data['op_total'] < 0 ?  $data['op_total'] * -1 :  $data['op_total']));
      echo '</td>';
      echo '<td style="text-align:right">';
      echo $CI->functionscore->toCurrency(( $data['cl_total'] <0 ? 'C' : 'D'), ( $data['cl_total'] < 0 ?  $data['cl_total'] * -1 :  $data['cl_total']));
      echo '</td>';
        

      $dropdown =
      '<div class="btn-group">
          <div class="dropdown">
            <button class="btn btn-success btn-sm" type="button" id="accionesGrupo" data-toggle="dropdown" aria-haspopup="true">
              Acciones
              <span class="caret"></span>
            </button>
            <ul class="dropdown-menu pull-right" aria-labelledby="accionesGrupo">
             <li>'
              .anchor('ledgers/edit/'.$data['id'], '<i class="glyphicon glyphicon-edit"></i>'.lang('accounts_index_edit_btn'), 
                array('class' => 'no-hover', 'escape' => false)).
             '</li><li>
              <a href="#delete_account_modal" data-toggle="modal" data-account="'.$data['id'].'" data-ledger="1"><span class="fa fa-trash"></span> Eliminar</a>
             </li>
            </ul>
          </div>
        </div>';
      echo '<td class="td-actions">';
      echo $dropdown;
      // echo anchor('ledgers/delete/'.$data['id'], '<i class="glyphicon glyphicon-trash"></i>'.lang('accounts_index_delete_btn'), 
      //     array('class' => 'no-hover', 'escape' => false, 'confirm' => (lang('accounts_index_delete_ledger_alert')))
      // );
      echo '</tr>';
    }
    $counter--;
  }
}

function print_space($count)
{
  $html = '';
  for ($i = 1; $i <= $count; $i++) {
    $html .= '&nbsp;&nbsp;&nbsp;';
  }
  return $html;
}

?>

<script type="text/javascript">
  
$(window).scroll(function(){
  var scrollTop = $(window).scrollTop();
  localStorage.setItem('scrollTopPos', scrollTop);
});

$(document).ready(function(){
  if (scrollTop = localStorage.getItem('scrollTopPos')) {
    $("html, body").animate({
        scrollTop: scrollTop
    }, 2000);
  }
});

</script>


<!-- Main content -->
    <div class="row wrapper wrapper-content animated fadeInRight">
      <!-- Small boxes (Stat box) -->
      <?php if ($this->functionscore->calculate($opdiff['opdiff_balance'], 0, '!=')) {
        echo '<div><div role="alert" class="alert alert-danger">' .
          sprintf(lang('accounts_index_label_difference_bw_balance'), $this->functionscore->toCurrency($opdiff['opdiff_balance_dc'], $opdiff['opdiff_balance'])) .
          
          '</div></div>';
      }; ?>

      <div class="ibox float-e-margins">
        <div class="ibox-content contentBackground">
          <div class="col-xs-12">
            <?php if (isset($cost_centers) && $cost_centers): ?>
              <div class="col-xs-4">
                <div class="row">
                  <div class="col-xs-4">
                    <label for="cost_center" style="padding-top: 7%"><?= lang('cost_center_label'); ?> :</label>
                  </div>
                  <div class="col-xs-8">
                    <select class="form-control ledger-dropdown" id="cost_center" name="cost_center">
                      <option value="">Todos</option>
                      <?php foreach ($cost_centers as $cost_center): ?>
                        <option value="<?= $cost_center['id'] ?>" <?= (isset($_GET['cc']) &&  $_GET['cc'] == $cost_center['id']) ? "selected='true'" : "" ?>><?= $cost_center['name'] ?></option>
                      <?php endforeach ?>
                    </select>
                  </div>
                </div>
              </div>
            <?php endif ?>
              <?php
                echo '<table id="ledgertable" class="table paddingbotton">';
                echo '<thead>';
                echo '<th>' . (lang('accounts_index_account_code')) . '</th>';
                echo '<th>' . (lang('accounts_index_account_name')) . '</th>';
                echo '<th>' . (lang('accounts_index_type')) . '</th>';
                echo '<th>' . (lang('accounts_index_op_balance')) . ' (' .$this->mAccountSettings->currency_symbol. ')' . '</th>';
                // if ($this->mAccountSettings->default_loading_accounts_index == 1) {
                  echo '<th>' . (lang('accounts_index_cl_balance')) . ' (' .$this->mAccountSettings->currency_symbol. ')' . '</th>';
                // }
                echo '<th>' . (lang('accounts_index_action')) . '</th>';
                echo '</thead>';
                echo "<tbody>";
                foreach ($accountlist as $dataRow) {
                  print_account_chart($dataRow, -1, $this);
                }
                echo "</tbody>";
                echo '</table>';
              ?>
          </div>
      </div>
      <!-- /.row -->
    </div>
    <!-- /.content -->

<div class="modal fade" tabindex="-1" role="dialog" id="delete_account_modal" aria-labelledby="deleteaccount">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><?= lang('accounts_index_delete_account_alert_title'); ?></h4>
      </div>
        <div class="modal-body">
          <input type="hidden" name="account_id_delete" id="account_id_delete">
          <input type="hidden" name="type_ledger" id="type_ledger">
          <?= lang('accounts_index_delete_account_alert_text'); ?>
        </div>
        <div class="modal-footer">
          <button class="btn btn-default" data-dismiss="modal"><?=lang('no');?></button>
          <button class="btn btn-danger confirm_delete_account" ><?=lang('yes');?></button>
        </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">

  

  dataset1 = $('#ledgertable').DataTable({
    /*order: [ 0, 'asc' ],*/
    pageLength: -1,
    responsive: true,
    "order" : [],
     "ordering": false,
    dom : '<"html5buttons" B><"containerBtn"><"inputFiltro"f>',
    buttons : [{extend:'excel', title:'Account List', className:'btnExportarExcel', exportOptions: {columns : [0,1,2,3,4]}}],
    oLanguage: {
      sLengthMenu: 'Mostrando _MENU_ registros por página',
      sZeroRecords: 'No se encontraron registros',
      sInfo: 'Mostrando _START_ a _END_ de _TOTAL_ registros',
      sInfoEmpty: 'Mostrando 0 a 0 de 0 registros',
      sInfoFiltered: '(Filtrado desde _MAX_ registros)',
      sSearch:         'Buscar: ',
      oPaginate:{
        sFirst:    'Primero',
        sLast:     'Último',
        sNext:     'Siguiente',
        sPrevious: 'Anterior'
      }
    }
    });
  var btnAcciones = '<div class="dropdown pull-right" id="">'+
  '<button class="btn btn-primary btn-sm btn-outline" type="button" id="accionesTabla" data-toggle="dropdown" aria-haspopup="true">Acciones<span class="caret"></span></button>'+
  '<ul class="dropdown-menu pull-right" aria-labelledby="accionesTabla">'+
  '<li><a onclick="$(\'.btnExportarExcel\').click()"><span class="glyphicon glyphicon-export"></span> <?= lang('export_xls') ?> </a></li>'+
  '<li><a onclick="window.location.href = \'<?= base_url(); ?>accounts/uploader\'"><span class="glyphicon glyphicon-import"></span> <?= lang('accounts_index_import_from_csv_btn') ?> </a></li>'+
  '</ul>'+
  '</div>';

  $('.containerBtn').html(btnAcciones);


$('#delete_account_modal').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget);
      account = button.data('account');
      isLedger = button.data('ledger');
      $('#account_id_delete').val(account);
      $('#type_ledger').val(isLedger);
});

$('.confirm_delete_account').on('click', function(){
  account = $('#account_id_delete').val();
  isLedger = $('#type_ledger').val();

  if (isLedger == "1") {
    window.location.href = "<?= base_url(); ?>ledgers/delete/"+account;
  } else if (isLedger == "0") {
    window.location.href = "<?= base_url(); ?>groups/delete/"+account;
  }
});

$(document).ready(function(){
   $('#loader').fadeOut();
});

$(document).on('change', '#cost_center', function(){
  $('#loader').fadeIn();
  if ($(this).val()) {
    location.href = 'accounts/index?cc='+$(this).val();
  } else {
    location.href = 'accounts/index';
  }
});
</script>