

<!-- iCheck -->
<script src="<?= base_url(); ?>assets/bootstrap/js/plugins/iCheck/icheck.min.js"></script>
<script>
	$(function () {
	    $('input[type="checkbox"],[type="radio"]').iCheck({
	      checkboxClass: 'icheckbox_square-blue',
	      radioClass: 'iradio_square-blue',
	      increaseArea: '20%' // optional
	    });
	});

</script>
<!-- Morris.js charts -->
	<script src="//raw.github.com/botmonster/jquery-bootpag/master/lib/jquery.bootpag.min.js"></script>

<script src="<?= base_url(); ?>assets/bootstrap/js/plugins/raphael/raphael.js"></script>
<script src="<?= base_url(); ?>assets/bootstrap/js/plugins/morris/morris.min.js"></script>
<script src="<?= base_url(); ?>assets/bootstrap/js/plugins/bootstrap-pagination/jquery.bootpag.min.js"></script>
<!-- Sparkline -->
<script src="<?= base_url(); ?>assets/bootstrap/js/plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="<?= base_url(); ?>assets/bootstrap/js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?= base_url(); ?>assets/bootstrap/js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="<?= base_url(); ?>assets/bootstrap/js/plugins/knob/jquery.knob.js"></script>
<!-- daterangepicker -->
<script src="<?= base_url(); ?>assets/bootstrap/js/plugins/moment/moment.js"></script>
<script src="<?= base_url(); ?>assets/bootstrap/js/plugins/daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<!-- <script src="<?= base_url(); ?>assets/bootstrap/js/plugins/datepicker/locales/bootstrap-datepicker.es.js"></script>
<script src="<?= base_url(); ?>assets/bootstrap/js/plugins/datepicker/bootstrap-datepicker.js"></script> -->
<!-- Bootstrap WYSIHTML5 -->
<script src="<?= base_url(); ?>assets/bootstrap/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="<?= base_url(); ?>assets/bootstrap/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?= base_url(); ?>assets/bootstrap/js/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<!-- <script src="<?= base_url(); ?>assets/dist/js/app.js"></script> -->
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!-- <script src="<?= base_url(); ?>assets/dist/js/pages/dashboard.js"></script> -->
<!-- AdminLTE for demo purposes -->
<!-- <script src="<?= base_url(); ?>assets/dist/js/demo.js"></script> -->


</body>
</html>