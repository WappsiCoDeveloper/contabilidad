<!DOCTYPE html>
<html lang="en">
<head>
	<?php 
header('Content-Type: text/html; charset=UTF-8'); ?>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<base href="<?php echo base_url(); ?>" />

	<title><?php echo $page_title; ?></title>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<!-- Bootstrap 3.3.6 -->
	<link rel="stylesheet" href="<?= base_url(); ?>assets/bootstrap/css/bootstrap.min.css">
	<!-- Toastr style -->
    <link href="<?= base_url(); ?>assets/bootstrap/css/plugins/toastr/toastr.min.css" rel="stylesheet">
    <!-- Gritter -->
    <link href="<?= base_url(); ?>assets/bootstrap/js/plugins/gritter/jquery.gritter.css" rel="stylesheet">
    <link href="<?= base_url(); ?>assets/bootstrap/css/animate.css" rel="stylesheet">
    <link href="<?= base_url(); ?>assets/bootstrap/css/style.css" rel="stylesheet">

	<!-- Font Awesome -->
	<link rel="stylesheet" href="<?= base_url(); ?>assets/bootstrap/js/plugins/font-awesome/css/font-awesome.min.css">
	<!-- Ionicons -->
	<link rel="stylesheet" href="<?= base_url(); ?>assets/bootstrap/js/plugins/ionicons/css/ionicons.min.css">
	<!-- Select2 -->
	<link rel="stylesheet" href="<?= base_url(); ?>assets/bootstrap/js/plugins/select2/select2.min.css">
	<!-- Theme style -->
	<!-- <link rel="stylesheet" href="<?= base_url(); ?>assets/dist/css/AdminLTE.min.css"> -->
	<!-- Custom style -->
	<!-- <link rel="stylesheet" href="<?= base_url(); ?>assets/dist/css/mystyle.css"> -->
	<!-- AdminLTE Skins. Choose a skin from the css/skins
	   folder instead of downloading all of them to reduce the load. -->
	<!-- <link rel="stylesheet" href="<?= base_url(); ?>assets/dist/css/skins/_all-skins.min.css"> -->
	<!-- iCheck -->
	<link rel="stylesheet" href="<?= base_url(); ?>assets/bootstrap/js/plugins/iCheck/square/blue.css">
	<!-- Morris chart -->
	<link rel="stylesheet" href="<?= base_url(); ?>assets/bootstrap/js/plugins/morris/morris.css">
	<!-- jvectormap -->
	<link rel="stylesheet" href="<?= base_url(); ?>assets/bootstrap/js/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
	<!-- Date Picker -->
	<!-- <link rel="stylesheet" href="<?= base_url(); ?>assets/bootstrap/js/plugins/datepicker/datepicker3.css"> -->
	<!-- Daterange picker -->
	<link rel="stylesheet" href="<?= base_url(); ?>assets/bootstrap/js/plugins/daterangepicker/daterangepicker.css">
	<!-- bootstrap wysihtml5 - text editor -->
	<link rel="stylesheet" href="<?= base_url(); ?>assets/bootstrap/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
	<!-- jQuery UI 1.11.4 -->
	<link rel="stylesheet" href="<?= base_url(); ?>assets/bootstrap/js/plugins/jquery-ui/jquery-ui.css">
	<!-- Bootstrap Color Picker -->
 	<link rel="stylesheet" href="<?= base_url(); ?>assets/bootstrap/js/plugins/colorpicker/bootstrap-colorpicker.min.css">

 	<link href="<?= base_url(); ?>assets/bootstrap/js/plugins/bootstrap-toggle/bootstrap-toggle.css" rel="stylesheet">

 	<link rel="icon" type="image/png" href="<?= base_url(); ?>assets/uploads/favicon.ico">

	<!-- jQuery 3.2.1 -->
	<script src="<?= base_url(); ?>assets/bootstrap/js/plugins/jQuery/jquery-3.2.1.min.js"></script>
	<!-- jQuery UI 1.11.4 -->
	<script src="<?= base_url(); ?>assets/bootstrap/js/plugins/jquery-ui/jquery-ui.js"></script>
	<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
	<script>
	  $.widget.bridge('uibutton', $.ui.button);
	</script>
	<!-- Bootstrap 3.3.6 -->
	<script src="<?= base_url(); ?>assets/bootstrap/js/bootstrap.min.js"></script>
	<script src="<?= base_url(); ?>assets/bootstrap/js/plugins/select2/select2.min.js"></script>
	<!-- Toastr -->
	<script src="<?= base_url();?>assets/bootstrap/js/plugins/toastr/toastr.min.js"></script>

	<script src="<?= base_url(); ?>assets/bootstrap/js/plugins/validate/jquery.validate.min.js"></script>

	<script src="<?= base_url(); ?>assets/bootstrap/js/plugins/jasny/jasny-bootstrap.min.js"></script>
	<link rel="stylesheet" href="<?= base_url(); ?>assets/bootstrap/css/plugins/jasny/jasny-bootstrap.min.css">
	<!-- DataTables -->
	<link rel="stylesheet" href="<?= base_url(); ?>assets/bootstrap/js/plugins/dataTables/media/css/dataTables.bootstrap.min.css">
	<link rel="stylesheet" href="<?= base_url(); ?>assets/bootstrap/js/plugins/dataTables/extensions/Buttons/css/buttons.bootstrap.min.css">
	<!-- DataTables -->
	<script src="<?= base_url();?>assets/bootstrap/js/plugins/dataTables/media/js/jquery.dataTables.min.js"></script>
	<script src="<?= base_url();?>assets/bootstrap/js/plugins/dataTables/media/js/dataTables.bootstrap.min.js"></script>
	<!-- bootstrap color picker -->
	<script src="<?= base_url(); ?>assets/bootstrap/js/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>
	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	<script src="<?= base_url(); ?>assets/bootstrap/js/plugins/metisMenu/jquery.metisMenu.js"></script>
	<script src="<?= base_url(); ?>assets/bootstrap/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<script src="<?= base_url(); ?>assets/bootstrap/js/inspinia.js"></script>
	<script src="<?= base_url(); ?>assets/bootstrap/js/plugins/bootstrap-toggle/bootstrap-toggle.min.js"></script>


	<link rel="stylesheet" href="<?= base_url(); ?>assets/bootstrap/css/plugins/inputmask/inputmask.css">
	<script src="<?= base_url(); ?>assets/bootstrap/js/plugins/inputmask/jquery.inputmask.bundle.js"></script>

	<script src="https://cdn.datatables.net/buttons/1.4.2/js/dataTables.buttons.min.js"></script>
	<script src="//cdn.datatables.net/buttons/1.4.2/js/buttons.flash.min.js"></script>
	<script src="//cdn.datatables.net/buttons/1.4.2/js/buttons.html5.min.js"></script>
	<script src="//cdn.datatables.net/buttons/1.4.2/js/buttons.print.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>

</head>
<body class="pace-done skin-1">