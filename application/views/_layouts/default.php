<?php $this->load->view('_partials/navbar'); ?>

<script type="text/javascript">
	toastr.options = {
  "closeButton": true,
  "debug": false,
  "progressBar": true,
  "preventDuplicates": false,
  "positionClass": "toast-bottom-right",
  "onclick": null,
  "showDuration": "400",
  "hideDuration": "1000",
  "timeOut": "7000",
  "extendedTimeOut": "1000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
}

jQuery.extend(jQuery.validator.messages, {//Configuración jquery valid
      required: "Este campo es obligatorio.",
      remote: "Por favor, rellena este campo.",
      email: "Por favor, escribe una dirección de correo válida",
      url: "Por favor, escribe una URL válida.",
      date: "Por favor, escribe una fecha válida.",
      dateISO: "Por favor, escribe una fecha (ISO) válida.",
      number: "Por favor, escribe un número entero válido.",
      digits: "Por favor, escribe sólo dígitos.",
      creditcard: "Por favor, escribe un número de tarjeta válido.",
      equalTo: "Por favor, escribe el mismo valor de nuevo.",
      accept: "Por favor, escribe un valor con una extensión aceptada.",
      maxlength: jQuery.validator.format("Por favor, no escribas más de {0} caracteres."),
      minlength: jQuery.validator.format("Por favor, no escribas menos de {0} caracteres."),
      rangelength: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1} caracteres."),
      range: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1}."),
      max: jQuery.validator.format("Por favor, escribe un valor menor o igual a {0}."),
      min: jQuery.validator.format("Por favor, escribe un valor mayor o igual a {0}.")
    });
</script>

<div class="content-wrapper">

	<section class="content-header">
		<?php if (validation_errors()) { ?>
	      	<div class="alert alert-danger">
	          	<button data-dismiss="alert" class="close" type="button">×</button>
	          	<?= validation_errors(); ?>
	      	</div>
	  	<?php } ?>
		<?php if ($this->session->flashdata('message')) { ?>
			<script type="text/javascript">
				Command: toastr.success('<?= $_SESSION['message']; ?>', '<?= lang("toast_success_title"); ?>', {onHidden : function(){}})
			</script>
	      	<!-- <div class="alert alert-success">
	          	<button data-dismiss="alert" class="close" type="button">×</button>
	          	<?= $_SESSION['message']; ?>
	      	</div> -->
	  	<?php } ?>
	  	<?php if ($this->session->flashdata('error')) { ?>
	  		<script type="text/javascript">
	  			console.log('<?= $this->session->flashdata('error') ?>');
				Command: toastr.error('<?= $_SESSION['error']; ?>', '<?= lang("toast_error_title"); ?>', {onHidden : function(){}})
			</script>
	      	<!-- <div class="alert alert-danger">
	          	<button data-dismiss="alert" class="close" type="button">×</button>
	          	<?= ($_SESSION['error']); ?>
	      	</div> -->
	  	<?php } ?>
	  	<?php if ($this->session->flashdata('warning')) { ?>
	  		<script type="text/javascript">
				Command: toastr.warning('<?= $_SESSION['warning']; ?>', '<?= lang("toast_warning_title"); ?>', {onHidden : function(){}})
			</script>
	      	<!-- <div class="alert alert-warning">
	          	<button data-dismiss="alert" class="close" type="button">×</button>
	          	<?= ($_SESSION['warning']); ?>
	      	</div> -->
	 	<?php } ?>
	</section>

	<?php $this->load->view($inner_view); ?>
</div>
<?php $this->load->view('_partials/footer'); ?>
<?php
	if ($view_log)
	{
		$this->load->view('_partials/right_navbar');
	}
?>