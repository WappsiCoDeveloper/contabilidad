<?php


	$group_names_spanish = array('biller' => 'Facturador', 'customer' => 'Cliente', 'Partner' => 'Cliente', 'supplier' => 'Proveedor', 'creditor' => 'Acreedor');

	// Generate a random id to use in below form array
	$i = time() + rand  (0, time()) + rand  (0, time()) + rand  (0, time());


	if (isset($entryitems)) {
		foreach ($entryitems as $row => $item) {
			echo '<tr class="ajax-add">';

			if ($this->mSettings->drcr_toby == 'toby') {
				echo '<td>' .
						'<div class="form-group-entryitem required">
							<select id="Entryitem' . $i . 'Dc" class="dc-dropdown form-control" name="Entryitem[' . $i . '][dc]">
								<option '.($item['dc'] == "D" ? "selected='selected'" : "").' value="D">'.lang('entries_views_addrow_label_dc_toby_D').'</option>
								<option '.($item['dc'] == "C" ? "selected='selected'" : "").' value="C">'.lang('entries_views_addrow_label_dc_toby_C').'</option>
							</select>
						</div>' .
					  '</td>';
			} else {
				echo '<td>' .
						'<div class="form-group-entryitem required">
							<select id="Entryitem' . $i . 'Dc" class="dc-dropdown form-control" name="Entryitem[' . $i . '][dc]">
								<option '.($item['dc'] == "D" ? "selected='selected'" : "").' value="D">'.lang('entries_views_addrow_label_dc_drcr_D').'</option>
								<option '.($item['dc'] == "C" ? "selected='selected'" : "").' value="C">'.lang('entries_views_addrow_label_dc_drcr_C').'</option>
							</select>
						</div>' .
					'</td>';
			}

			echo '<td>' .
					'<div class="form-group-entryitem required">
						<select id="Entryitem' . $i . 'LedgerId" class="ledger_select ledger-dropdown form-control" name="Entryitem[' . $i . '][ledger_id]" required="true">
							<option value="">'.lang("entries_views_add_company_first_option").'</option>';
				echo '</select>
					</div>' .
				'</td>';

			echo '<td>' .
					'<div class="form-group-entryitem">
						<input type="text" id="Entryitem' . $i . 'DrAmount" class="dr-item form-control" name="Entryitem[' . $i . '][dr_amount]" disabled="" '.(isset($item['dr_amount']) ? "value='".$item['dr_amount']."'" : "").' min="0" required="true">
					</div>' .
				'</td>';

			echo '<td>' .
					'<div class="form-group-entryitem"><input type="text" id="Entryitem' . $i . 'CrAmount" class="cr-item form-control" name="Entryitem[' . $i . '][cr_amount]" disabled="" '.(isset($item['cr_amount']) ? "value='".$item['cr_amount']."'" : "").' min="0" required="true"></div>' .
					'</td>';
			$data = array(
				'type'  => "text",
				'name'  => 'Entryitem[' . $i . '][narration]',
				'class' => 'form-control',
				'id' => 'Entryitem' . $i . 'Narration',
				'value' => $item['narration'],
			);
			echo "<td>
					<div class='form-group-entryitem'>";
			echo form_input($data);
			echo "</div>
				</td>";
			/* BASE */
			echo '<td>'; ?>
				<input type="number" class="form-control" name="<?= 'Entryitem['.$i.'][base]' ?>"  value="<?= $item['base'] ?>"  min='0'>
			<?php echo '</td>';
			/* BASE */

			/* TERCERO */
			echo "<td>"; ?>

			<select name="<?= 'Entryitem['.$i.'][companies_id]' ?>" class="companies_select form-control third ledger-dropdown2" required>
				<option value=""><?= lang('entries_views_add_company_first_option'); ?></option>
				<?= $option ?>
			</select>

			<?php
			echo "</td>";
			/* TERCERO */
			if ($this->mAccountSettings->cost_center) {
				/* CENTRO DE COSTO */
				echo "<td>";
				?>
				<select name="<?= 'Entryitem['.$i.'][cost_center_id]' ?>" class="cost_center_select form-control" required>
					<?php if ($cost_centers): ?>
						<option value=""><?= lang('select') ?></option>
						<?php foreach ($cost_centers as $cost_center): ?>
							<option value="<?= $cost_center['id'] ?>"><?= $cost_center['name'] ?></option>
						<?php endforeach ?>
					<?php else: ?>
						<option value=""><?= lang('cost_centers_not_founded') ?></option>
					<?php endif ?>
				</select>

				<?php
				echo "</td>";
				/* CENTRO DE COSTO */
			}

			echo '<td>';
			echo '<button class="deleterow btn btn-danger btn-sm" type="button"><span class="glyphicon glyphicon-trash" escape="false"></span></button>';
			echo '</td>';
			echo '</tr>';
		}
	} else {
		echo '<tr class="ajax-add">';

		if ($this->mSettings->drcr_toby == 'toby') {
			echo '<td>' . '<div class="form-group-entryitem required"><select id="Entryitem' . $i . 'Dc" class="dc-dropdown form-control" name="Entryitem[' . $i . '][dc]"><option selected="selected" value="D">'.lang('entries_views_addrow_label_dc_toby_D').'</option><option value="C">'.lang('entries_views_addrow_label_dc_toby_C').'</option></select></div>' . '</td>';
		} else {
			echo '<td>' . '<div class="form-group-entryitem required"><select id="Entryitem' . $i . 'Dc" class="dc-dropdown form-control" name="Entryitem[' . $i . '][dc]"><option selected="selected" value="D">'.lang('entries_views_addrow_label_dc_drcr_D').'</option><option value="C">'.lang('entries_views_addrow_label_dc_drcr_C').'</option></select></div>' . '</td>';
		}

		echo '<td>' . '<div class="form-group-entryitem required"><select id="Entryitem' . $i . 'LedgerId" class="ledger_select ledger-dropdown form-control" name="Entryitem[' . $i . '][ledger_id]" required="true">
							<option value="">'.lang("entries_views_add_company_first_option").'</option>';

		echo '</select></div>' . '</td>';

		echo '<td>' . '<div class="form-group-entryitem"><input type="text" id="Entryitem' . $i . 'DrAmount" class="dr-item form-control" name="Entryitem[' . $i . '][dr_amount]" disabled=""  min="0" required="true"></div>' . '</td>';

		echo '<td>' . '<div class="form-group-entryitem"><input type="text" id="Entryitem' . $i . 'CrAmount" class="cr-item form-control" name="Entryitem[' . $i . '][cr_amount]" disabled="" min="0" required="true"></div>' . '</td>';
		$data = array(
			'type'  => "text",
			'name'  => 'Entryitem[' . $i . '][narration]',
			'class' => 'form-control',
			'id' => 'Entryitem' . $i . 'Narration'
		);
		echo "<td><div class='form-group-entryitem'>";
		echo form_input($data);
		echo "</div></td>";
		/* BASE */
		echo '<td>'; ?>
			<input type="number" class="form-control" name="<?= 'Entryitem['.$i.'][base]' ?>"  value="0"  min='0'>
		<?php echo '</td>';
		/* BASE */

		/* TERCERO */
		echo "<td>"; ?>

		<select name="<?= 'Entryitem['.$i.'][companies_id]' ?>" class="companies_select form-control third ledger-dropdown2" required>
			<option value=""><?= lang('entries_views_add_company_first_option'); ?></option>
		</select>

		<?php
		echo "</td>";
		/* TERCERO */
		if ($this->mAccountSettings->cost_center) {
			/* CENTRO DE COSTO */
			echo "<td>";
			?>

			<select name="<?= 'Entryitem['.$i.'][cost_center_id]' ?>" class="cost_center_select form-control" required>
				<?php if ($cost_centers): ?>
					<option value=""><?= lang('select') ?></option>
					<?php foreach ($cost_centers as $cost_center): ?>
						<option value="<?= $cost_center['id'] ?>"><?= $cost_center['name'] ?></option>
					<?php endforeach ?>
				<?php else: ?>
					<option value=""><?= lang('cost_centers_not_founded') ?></option>
				<?php endif ?>
			</select>

			<?php
			echo "</td>";
			/* CENTRO DE COSTO */
		}

		echo '<td>';
		echo '<button class="deleterow btn btn-danger btn-sm" type="button"><span class="glyphicon glyphicon-trash" escape="false"></span></button>';
		echo '</td>';
		echo '</tr>';
	}

?>