<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/contabilidad/vendor/fpdf/fpdf.php';

class PDF extends FPDF
{
    function Header()
    {
      $this->Image(( isset($this->settings->logo) ? base_url().'assets/uploads/companies/'.$this->settings->logo : '' ) ,7,9,30);

      $this->setXY(40, 7);
      $this->SetFont('Arial','B',8);
      $name = 'LOREM IPSUM DOLOR SIT AMET LOREM IPSUM DOLOR SIT AMET LOREM IPSUM DOLOR SIT AMET';
      $this->MultiCell(70,4, utf8_decode(mb_strtoupper($this->settings->name)),'','L');
      $cy = $this->getY();
      $this->setXY(40, $cy);
      $this->SetFont('Arial','B',8);
      $this->Cell(65, 4, utf8_decode("NIT : ".$this->settings->nit),0,1,'L');
      $cy = $this->getY();
      $this->setXY(40, $cy);
      $this->SetFont('Arial','B',8);
      $this->Cell(65, 4, utf8_decode($this->settings->address),0,1,'L');


      $end_x = $this->getX();
      $end_y = $this->getY();

      $this->setXY(110, 7);
      $this->SetFont('Arial','B',10);
      $this->Cell(0, 4, utf8_decode("COMPROBANTE DE ASIENTO CONTABLE"),0,1);
      $cy = $this->getY();
      $this->setXY(110, $cy);
      $this->SetFont('Arial','B',10);
      $this->Cell(12, 4, utf8_decode("N°"),0,0);
      $this->Cell(0, 4, utf8_decode(":  ".$this->datos['label']."-".$this->datos['number']),0,1);
      $cy = $this->getY();
      $this->setXY(110, $cy);
      $this->Cell(12, 4, utf8_decode("FECHA"),0,0);
      $this->Cell(0, 4, utf8_decode(":  ".$this->datos['date']),0,1);
      $cy = $this->getY();
      $this->setXY(110, $cy);
      $this->Cell(65, 4, utf8_decode($this->settings->phone),0,1,'L');

      $this->setXY($end_x, $end_y+5);

      $this->SetFont('Arial','B',8);
      $this->Cell(65, 4, utf8_decode('TIPO DE NOTA'),0,0,'L');
      $this->Cell(65, 4, utf8_decode('TERCERO'),0,1,'L');
      $this->SetFont('Arial','B',10);
      $this->Cell(65, 4, utf8_decode($this->entrytype['name']),0,0,'L');
      $this->Cell(65, 4, utf8_decode($this->datos['third_name']),0,1,'L');

    }
  }


$pdf = new PDF('P', 'mm', 'A4');
$pdf->settings = $settings;
$pdf->datos = $datos;
$pdf->entrytype = $entrytype;
$pdf->AliasNbPages();
$pdf->SetMargins(7, 7);

//INICIO PDF
$pdf->AddPage();

$pdf->Ln(10);

$pdf->SetFont('Arial','',7);
$pdf->Cell(196, 5 , utf8_decode(""),'T',1,'R');
$pdf->Cell(66, 5 , utf8_decode("Auxiliar"),'B',0,'L');
$pdf->Cell(15, 5 , utf8_decode("Débito"),'B',0,'L');
$pdf->Cell(15, 5 , utf8_decode("Crédito"),'B',0,'L');
$pdf->Cell(50, 5 , utf8_decode("Descripción"),'B',0,'L');
$pdf->Cell(14, 5 , utf8_decode("Base"),'B',0,'L');
$pdf->Cell(36, 5 , utf8_decode("Tercero"),'B',1,'L');

$sum_debit = $sum_credit = 0;

$pdf->SetFont('Arial','',6.5);
// exit(var_dump($entryitems));
foreach ($entryitems as $row => $item)
{
  $columns = [];
  $inicio_fila_X = $pdf->getX();
  $inicio_fila_Y = $pdf->getY();
  $sum_debit += (double) $item['dr_amount'];
  $sum_credit += (double) $item['cr_amount'];
  $columns[1]['inicio_X'] = $pdf->getX();
  $pdf->Cell(66, 5 , utf8_decode($item['ledger_name']),'',0,'L');
  $columns[1]['fin_X'] = $pdf->getX();
  $columns[2]['inicio_X'] = $pdf->getX();
  $pdf->Cell(15, 5 , (! empty($item['dr_amount'])) ? number_format((double) $item['dr_amount'], 2, ',', '.') : '','',0,'R');
  $columns[2]['fin_X'] = $pdf->getX();
  $columns[3]['inicio_X'] = $pdf->getX();
  $pdf->Cell(15, 5 , (! empty($item['cr_amount'])) ? number_format((double) $item['cr_amount'], 2, ',', '.') : '','',0,'R');
  $columns[3]['fin_X'] = $pdf->getX();
  $columns[4]['inicio_X'] = $pdf->getX();
  $cX = $pdf->getX();
  $cY = $pdf->getY();
  $inicio_altura_fila = $cY;
  $pdf->MultiCell(50,3, utf8_decode($item['narration']),'','L');
  $fin_altura_fila = $pdf->getY();
  $columns[4]['fin_X'] = $pdf->getX();
  $pdf->setXY($cX+50, $cY);
  $columns[5]['inicio_X'] = $pdf->getX();
  $pdf->Cell(14, 5 , utf8_decode(number_format((double) $item['base'], 2, ',', '.')),'',0,'R');
  $columns[5]['fin_X'] = $pdf->getX();
  $columns[6]['inicio_X'] = $pdf->getX();
  $cX = $pdf->getX();
  $cY = $pdf->getY();
  $pdf->MultiCell(36,3, utf8_decode($item['third_name']),'','L');
  $columns[6]['fin_X'] = $pdf->getX();
  if ($pdf->getY() > $fin_altura_fila) {
    $fin_altura_fila = $pdf->getY();
  }
  $pdf->setXY($cX+36, $cY);
  $fin_fila_X = $pdf->getX();
  $fin_fila_Y = $pdf->getY();
  $ancho_fila = $fin_fila_X - $inicio_fila_X;
  $altura_fila = $fin_altura_fila - $inicio_altura_fila;
  $pdf->setXY($inicio_fila_X, $inicio_fila_Y);
  foreach ($columns as $key => $data) {
    $altura_fila = $altura_fila >= 5 ? $altura_fila : 5;
    $ancho_column = $data['fin_X'] - $data['inicio_X'];
  }
  $pdf->ln($altura_fila);
  if ($pdf->getY() > 270) {
    $pdf->AddPage();
  }
}

$difference = $sum_debit - $sum_credit;

if ($datos["state"] == 1)
{
  $pdf->SetFont('ZapfDingbats','', 10);
  $pdf->Cell(8, 5 , "3", 'T', 0, "C");
  $pdf->SetFont('Arial','',6.5);
  $pdf->Cell(58, 5 , "Aprobado", 'T', 0,'L');
}
else if ($datos["state"] == 2)
{
  $pdf->SetFont('ZapfDingbats','', 10);
  $pdf->Cell(8, 5 , "7", 'T', 0, "C");
  $pdf->SetFont('Arial','',6.5);
  $pdf->Cell(58, 5 , "Desaprobado", 'T', 0,'L');
}
else
{
  $pdf->SetFont('ZapfDingbats','', 10);
  $pdf->Cell(8, 5 , "m", 'T', 0, "C");
  $pdf->SetFont('Arial','',6.5);
  $pdf->Cell(58, 5 , "Anulado", 'T', 0,'L');
}

$pdf->SetFont('Arial','',6.5);
$pdf->Cell(15, 5 , number_format($sum_debit, 2, ',', '.'),'T',0,'R');
$pdf->Cell(15, 5 , number_format($sum_credit, 2, ',', '.'),'T',0,'R');
$pdf->SetFont("", "B");
$pdf->Cell(97, 5 , utf8_decode('Diferencia: ').number_format($difference, 2, ',', '.'),'T',1);

$pdf->ln(5);
$pdf->Cell(10, 4 , utf8_decode("NOTA: "),0,0,'L');
$pdf->multicell(0, 4, utf8_decode($datos['notes']),0,'L');
$pdf->ln(20);

$cx = $pdf->getX();
$cy = $pdf->getY();

$pdf->Cell(4, 5 , utf8_decode(""),0,0,'R');
$pdf->Cell(41.75, 5 , utf8_decode(""),'B',0,'R');
$pdf->Cell(8, 5 , utf8_decode(""),'',0,'R');
$pdf->Cell(41.75, 5 , utf8_decode(""),'B',0,'R');
$pdf->Cell(8, 5 , utf8_decode(""),'',0,'R');
$pdf->Cell(41.75, 5 , utf8_decode(""),'B',0,'R');
$pdf->Cell(8, 5 , utf8_decode(""),'',0,'R');
$pdf->Cell(41.75, 5 , utf8_decode(""),'B',1,'R');

$pdf->setXY($cx, $cy+5);

$pdf->SetFont('Arial','',7);
$pdf->Cell(4, 5 , utf8_decode(""),'',0,'R');
$pdf->Cell(41.75, 5 , utf8_decode(mb_strtoupper("ELABORÓ")),'',0,'R');
$pdf->Cell(8, 5 , utf8_decode(""),'',0,'R');
$pdf->Cell(41.75, 5 , utf8_decode("REVISÓ"),'',0,'R');
$pdf->Cell(8, 5 , utf8_decode(""),'',0,'R');
$pdf->Cell(41.75, 5 , utf8_decode("APROBÓ"),'',0,'R');
$pdf->Cell(8, 5 , utf8_decode(""),'',0,'R');
$pdf->Cell(41.75, 5 , utf8_decode("RECIBIÓ"),'',1,'R');

$pdf->Ln(5);
$pdf->SetFont('Arial','',7);
$pdf->Cell(0, 4, "Impreso por: Wappsi el ". date("Y-m-d") ." por el usuario ". $this->session->userdata('username') .".");

$pdf->Output("LIBRO_NIT_CUENTAS.pdf", "I");