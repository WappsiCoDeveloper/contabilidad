<div class="row wrapper border-bottom white-bg page-heading">
  <div class="col-lg-8">
    <h2><?php echo $page_title; ?></h2>
    <ol class="breadcrumb">
      <li>
        <a href="login/index">Inicio</a> <?php //En el controlador en la función llamada, se revisa si el usuario está logueado y lo envía a la vista principal según permisos ?>
      </li>
      <li class="active">
        <strong><?php echo $page_title; ?></strong>
      </li>
    </ol>
  </div><!-- /.col -->
  <div class="col-lg-4">
    <div class="title-action">
      <?php if ($p_entrytype_label != FALSE): ?>
        <a href="<?= base_url().'entries/add/'.$p_entrytype_label ?>" class="btn btn-success"><i class="fa fa-plus-square"></i><?= lang('entries_views_index_add_entry_btn'); ?></a>
      <?php endif ?>
        <a href="<?= base_url().'entries/import/' ?>" class="btn btn-success"> <i class="fa fa-upload" aria-hidden="true"></i> <?= lang('entries_views_index_import_entries'); ?></a>
    </div>
  </div>
</div><!-- /.row -->    

<style type="text/css">
	/*#loader{
		display: block;
	}*/

  .float-e-margins .btn {
    margin-bottom: 0px !important;
  }
</style>

<div class="row wrapper wrapper-content animated fadeInRight">

   <div class="ibox collapsed">
      <div class="ibox-title">
          <h5>Filtrar</h5>
          <div class="ibox-tools">
              <a class="collapse-link">
                  <i class="fa fa-chevron-down" id="show_hide"></i>
              </a>
          </div>
      </div>
      <div class="ibox-content" style="display: none;">
        <form class="row" method="post">
          <div class="col-xs-3">
            <label>Número de documento</label>
            <input type="text" name="numero_nota" id="numero_nota" class="form-control" value="<?= isset($_POST['numero_nota']) ? $_POST['numero_nota'] : '' ?>">
          </div>
          <div class="col-xs-3">
            <label>Tipo de Nota</label>
            <select name="tipo_nota" id="tipo_nota" class="form-control" style="width: 100%;">
              <option value="">Seleccione...</option>
              <?php foreach ($entry_types as $row => $et): ?>
                <option value="<?= $et['id'] ?>" <?= isset($_POST['tipo_nota']) && $et['id'] == $_POST['tipo_nota'] ? 'selected="selected"' : '' ?> ><?= ucfirst(mb_strtolower($et['name'])) ?> <?= $et['label'] != "" ? "(".$et['label'].")" : "" ?></option>
              <?php endforeach ?>
            </select>
          </div>
          <?php if (isset($cost_centers) && $cost_centers): ?>
              <div class="col-xs-3">
                <div>
                  <label for="cost_center"><?= lang('cost_center_label'); ?></label>
                  <select class="form-control ledger-dropdown" id="cost_center" name="cost_center">
                    <option value="">Todos</option>
                    <?php foreach ($cost_centers as $cost_center): ?>
                      <option value="<?= $cost_center['id'] ?>" <?= (isset($_POST['cost_center']) &&  $_POST['cost_center'] == $cost_center['id']) ? "selected='true'" : "" ?>><?= $cost_center['name'] ?></option>
                    <?php endforeach ?>
                  </select>
                            </div>
              </div>
            <?php endif ?>
          <div class="col-xs-3">
            <label>Fecha de inicio</label>
            <input type="date" name="fecha_inicio" id="fecha_inicio" class="form-control" value="<?= isset($_POST['fecha_inicio']) ? $_POST['fecha_inicio'] : $this->start_date_filter ?>">
          </div>
          <div class="col-xs-3">
            <label>Fecha de finalización</label>
            <input type="date" name="fecha_fin" id="fecha_fin" class="form-control" value="<?= isset($_POST['fecha_fin']) ? $_POST['fecha_fin'] : $this->end_date_filter ?>">
          </div>
          <div class="col-xs-12" style="padding-top: 5px;">
            <input type="submit" name="buscar" class="btn btn-primary">
          </div>
        </form>
      </div>
  </div>


 <div class="ibox float-e-margins">
 	<div class="ibox-content contentBackground">
          <div class="col-sm-12" style="padding-bottom: 10%;">
              <table class="table table-hover" id="tableEntries">
				<thead>
					<tr>
            <th><?= lang('entries_views_index_th_number'); ?></th>
						<th><?= lang('entries_views_index_th_date'); ?></th>
						<th><?= lang('entries_views_index_th_note'); ?></th>
						<th><?= lang('entries_views_index_th_type'); ?></th>
						<th><?= lang('entries_views_index_th_third'); ?></th>
						<th><?= lang('entries_views_index_th_total_amount'); ?></th>
            <th><?= lang('entries_views_index_th_state'); ?></th>
						<th><?= lang('entries_views_index_th_actions'); ?></th>
					</tr>
				</thead>
				<tbody>
				</tbody>
				</table>
          </div>
      </div>
 </div>

</div>

<div class="modal fade" tabindex="-1" role="dialog" id="delete_entry_modal" aria-labelledby="deleteentry">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><?= lang('entrys_index_delete_entry_alert_title'); ?></h4>
      </div>
        <div class="modal-body">
          <input type="hidden" name="entry_id_delete" id="entry_id_delete">
          <input type="hidden" name="entry_type_delete" id="entry_type_delete">
          <?= lang('entrys_index_delete_entry_alert_text'); ?>
        </div>
        <div class="modal-footer">
          <button class="btn btn-default" data-dismiss="modal"><?=lang('no');?></button>
          <button class="btn btn-danger confirm_delete_entry" ><?=lang('yes');?></button>
        </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div class="modal fade" tabindex="-1" role="dialog" id="approve_entry_modal" aria-labelledby="approveentry">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><?= lang('entrys_index_approve_entry_alert_title'); ?></h4>
      </div>
        <div class="modal-body">
          <input type="hidden" name="entry_id_approve" id="entry_id_approve">
          <input type="hidden" name="entry_type_approve" id="entry_type_approve">
          <?= lang('entrys_index_approve_entry_alert_text'); ?>
        </div>
        <div class="modal-footer">
          <button class="btn btn-default" data-dismiss="modal"><?=lang('no');?></button>
          <button class="btn btn-primary confirm_approve_entry" ><?=lang('yes');?></button>
        </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

  <script type="text/javascript">
    $(document).ready(function(){
        $('#show_hide').click();
    });
  </script>

<script type="text/javascript">
<?php if (isset($_POST['fecha_inicio'])): ?>
$('#loader').fadeIn();
  dataset1 = $('#tableEntries').DataTable({
      ajax: {
          method: 'POST',
          url: '<?=base_url("entries/getEntries"); ?>',
          data: {
                  'fecha_inicio' : $('#fecha_inicio').val(), 
                  'fecha_fin' : $('#fecha_fin').val(), 
                  'tipo_nota' : $('#tipo_nota').val(), 
                  'cost_center' : $('#cost_center').val(), 
                  'numero_nota' : $('#numero_nota').val() 
                },
          dataType: 'json',
        },
      "order" : [],
      columns:[
          { data: 'number'},
          { data: 'date'},
          { data: 'note'},
          { data: 'type'},
          { data: 'third'},
          { data: 'amount'},
          { data: 'state'},
          { data: 'actions'},
        ],
      pageLength: 25,
      responsive: true,
      dom : '<"html5buttons" B>lr<"containerBtn"><"inputFiltro"f>tip',
      buttons : [{extend:'excel', title:'Entries', className:'btnExportarExcel', exportOptions: {columns : [0,1,2,3,4,5,6]}}],
      oLanguage: {
        sLengthMenu: 'Mostrando _MENU_ registros por página',
        sZeroRecords: 'No se encontraron registros',
        sInfo: 'Mostrando _START_ a _END_ de _TOTAL_ registros',
        sInfoEmpty: 'Mostrando 0 a 0 de 0 registros',
        sInfoFiltered: '(Filtrado desde _MAX_ registros)',
        sSearch:         'Buscar: ',
        oPaginate:{
          sFirst:    'Primero',
          sLast:     'Último',
          sNext:     'Siguiente',
          sPrevious: 'Anterior'
        }
      },
        "preDrawCallback": function( settings ) {
          
        },
        fnRowCallback: function (nRow, aData, iDisplayIndex)
        {
            nRow.dataset.label = aData['entryLabel'];
            nRow.dataset.number = aData['entryNumber'];
            nRow.className = "entry_link";
            return nRow;
        },
        "rowCallback": function( row, data ) {
            $('td:eq(5)', row).addClass( 'text-right' );
        }
      }).on("draw", function(){
       $('#loader').fadeOut();
       $('.entry_link td:not(:last-child)').css('cursor', 'pointer');

        $('[data-toggle="toggle"]').bootstrapToggle('destroy');
        $('[data-toggle="toggle"]').bootstrapToggle();

     });
  
<?php endif ?>

var btnEstado;

$(document).ready(function(){
  $('#tipo_nota').select2();
});

$('#delete_entry_modal').on('show.bs.modal', function (event) {
	var button = $(event.relatedTarget);
      btnEstado = button;
  	entry = button.data('entry');
  	entrytype = button.data('entrytype');
  	$('#entry_id_delete').val(entry);
  	$('#entry_type_delete').val(entrytype);
});

$('#delete_entry_modal').on('hide.bs.modal', function (event) {
    btnEstado.find('input').bootstrapToggle('on');
});

$('.confirm_delete_entry').on('click', function(){
  	entry = $('#entry_id_delete').val();
  	entrytype = $('#entry_type_delete').val();
  	window.location.href = "<?= base_url();?>entries/delete/"+entrytype+"/"+entry;
});

$('#approve_entry_modal').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget);
    btnEstado = button;
    entry = button.data('entry');
    entrytype = button.data('entrytype');
    $('#entry_id_approve').val(entry);
    $('#entry_type_approve').val(entrytype);
});

$('#approve_entry_modal').on('hide.bs.modal', function (event) {
    btnEstado.find('input').bootstrapToggle('off');
});

$('.confirm_approve_entry').on('click', function(){
    entry = $('#entry_id_approve').val();
    entrytype = $('#entry_type_approve').val();
    window.location.href = "<?= base_url();?>entries/approve/"+entrytype+"/"+entry;
});

$('#numero_nota').on('keyup', function(){
  val = $(this).val();

  if(val != ''){
    $('#tipo_nota').prop('required', true);
  } else {
    $('#tipo_nota').prop('required', false);
  }

});

$(document).on('click', '.cambiarEstado', function(event){
    event.stopPropagation();
  });



$(document).on('click', '.entry_link td:not(:last-child)', function(){
  tr = $(this).parent('.entry_link');
  // console.log('<?=base_url("entries/view/"); ?>'+tr.data('label')+'/'+tr.data('number'));
  window.open('<?=base_url("entries/view/"); ?>'+tr.data('label')+'/'+tr.data('number'), "_self", "noopener,noreferrer");
});

</script>

