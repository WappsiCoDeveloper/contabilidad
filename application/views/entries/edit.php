<script type="text/javascript">

$(document).ready(function() {

	/* javascript floating point operations */
	var jsFloatOps = function(param1, param2, op) {
		param1 = parseFloat(param1);
		param2 = parseFloat(param2);
		var result = 0;
		if (op == '+') {
			result = param1 + param2;
			return Math.round(result * 100) / 100;
		}
		if (op == '-') {
			result = param1 - param2;
			return Math.round(result * 100) / 100;
		}
		if (op == '!=') {
			if (param1 != param2)
				return true;
			else
				return false;
		}
		if (op == '==') {
			if (param1 == param2)
				return true;
			else
				return false;
		}
		if (op == '>') {
			if (param1 > param2)
				return true;
			else
				return false;
		}
		if (op == '<') {
			if (param1 < param2)
				return true;
			else
				return false;
		}
	}

	/* Calculating Dr and Cr total */
	$(document).on('change', '.dr-item', function() {
		var drTotal = 0;
		$("table tr .dr-item").each(function() {
			var curDr = $(this).prop('value');
			curDr = parseFloat(curDr);
			if (isNaN(curDr))
				curDr = 0;
			drTotal = jsFloatOps(drTotal, curDr, '+');
		});
		$("table tr #dr-total").text(drTotal);
		var crTotal = 0;
		$("table tr .cr-item").each(function() {
			var curCr = $(this).prop('value');
			curCr = parseFloat(curCr);
			if (isNaN(curCr))
				curCr = 0;
			crTotal = jsFloatOps(crTotal, curCr, '+');
		});
		$("table tr #cr-total").text(crTotal);

		if (jsFloatOps(drTotal, crTotal, '==')) {
			$("table tr #dr-total").css("background-color", "#FFFF99");
			$("table tr #cr-total").css("background-color", "#FFFF99");
			$("table tr #dr-diff").text("-");
			$("table tr #cr-diff").text("");
		} else {
			$("table tr #dr-total").css("background-color", "#FFE9E8");
			$("table tr #cr-total").css("background-color", "#FFE9E8");
			if (jsFloatOps(drTotal, crTotal, '>')) {
				$("table tr #dr-diff").text("");
				$("table tr #cr-diff").text(jsFloatOps(drTotal, crTotal, '-'));
			} else {
				$("table tr #dr-diff").text(jsFloatOps(crTotal, drTotal, '-'));
				$("table tr #cr-diff").text("");
			}
		}
	});

	$(document).on('change', '.cr-item', function() {
		var drTotal = 0;
		$("table tr .dr-item").each(function() {
			var curDr = $(this).prop('value')
			curDr = parseFloat(curDr);
			if (isNaN(curDr))
				curDr = 0;
			drTotal = jsFloatOps(drTotal, curDr, '+');
		});
		$("table tr #dr-total").text(drTotal);
		var crTotal = 0;
		$("table tr .cr-item").each(function() {
			var curCr = $(this).prop('value')
			curCr = parseFloat(curCr);
			if (isNaN(curCr))
				curCr = 0;
			crTotal = jsFloatOps(crTotal, curCr, '+');
		});
		$("table tr #cr-total").text(crTotal);

		if (jsFloatOps(drTotal, crTotal, '==')) {
			$("table tr #dr-total").css("background-color", "#FFFF99");
			$("table tr #cr-total").css("background-color", "#FFFF99");
			$("table tr #dr-diff").text("-");
			$("table tr #cr-diff").text("");
		} else {
			$("table tr #dr-total").css("background-color", "#FFE9E8");
			$("table tr #cr-total").css("background-color", "#FFE9E8");
			if (jsFloatOps(drTotal, crTotal, '>')) {
				$("table tr #dr-diff").text("");
				$("table tr #cr-diff").text(jsFloatOps(drTotal, crTotal, '-'));
			} else {
				$("table tr #dr-diff").text(jsFloatOps(crTotal, drTotal, '-'));
				$("table tr #cr-diff").text("");
			}
		}
	});

	/* Dr - Cr dropdown changed */
	$(document).on('change', '.dc-dropdown', function() {
		var drValue = $(this).parent().parent().next().next().children().children().prop('value');
		var crValue = $(this).parent().parent().next().next().next().children().children().prop('value');

		if ($(this).parent().parent().next().children().children().val() == "0") {
			return;
		}

		drValue = parseFloat(drValue);
		if (isNaN(drValue))
			drValue = 0;

		crValue = parseFloat(crValue);
		if (isNaN(crValue))
			crValue = 0;

		if ($(this).prop('value') == "D") {
			if (drValue == 0 && crValue != 0) {
				$(this).parent().parent().next().next().children().children().prop('value', crValue);
			}
			$(this).parent().parent().next().next().next().children().children().prop('value', "");
			$(this).parent().parent().next().next().next().children().children().prop('disabled', 'disabled');
			$(this).parent().parent().next().next().children().children().prop('disabled', '');
		} else {
			if (crValue == 0 && drValue != 0) {
				$(this).parent().parent().next().next().next().children().prop('value', drValue);
			}
			$(this).parent().parent().next().next().children().children().prop('value', "");
			$(this).parent().parent().next().next().children().children().prop('disabled', 'disabled');
			$(this).parent().parent().next().next().next().children().children().prop('disabled', '');
		}
		/* Recalculate Total */
		$('.dr-item:first').trigger('change');
		$('.cr-item:first').trigger('change');
	});

	/* Ledger dropdown changed */
	$(document).on('change', '.ledger-dropdown', function() {
		if ($(this).val() == "0") {
			/* Reset and diable dr and cr amount */
			$(this).parent().parent().next().children().children().prop('value', "");
			$(this).parent().parent().next().next().children().children().prop('value', "");
			$(this).parent().parent().next().children().children().prop('disabled', 'disabled');
			$(this).parent().parent().next().next().children().children().prop('disabled', 'disabled');
		} else {
			/* Enable dr and cr amount and trigger Dr/Cr change */
			$(this).parent().parent().next().children().children().prop('disabled', '');
			$(this).parent().parent().next().next().children().children().prop('disabled', '');
			$(this).parent().parent().prev().children().children().trigger('change');
		}
		/* Trigger dr and cr change */
		$(this).parent().parent().next().children().children().trigger('change');
		$(this).parent().parent().next().next().children().children().trigger('change');

		var ledgerid = $(this).val();
		var rowid = $(this);
		if (ledgerid > 0) {
			$.ajax({
				url: '<?=base_url("ledgers/cl"); ?>',
				data: 'id=' + ledgerid,
				dataType: 'json',
				success: function(data)
				{
					var ledger_bal = parseFloat(data['cl']['amount']);

					var prefix = '';
					var suffix = '';
					if (data['cl']['status'] == 'neg') {
						prefix = '<span class="error-text">';
						suffix = '</span>';
					}

					if (data['cl']['dc'] == 'D') {
						rowid.parent().parent().next().next().next().next().children().html(prefix + "Dr " + ledger_bal + suffix);
					} else if (data['cl']['dc'] == 'C') {
						rowid.parent().parent().next().next().next().next().children().html(prefix + "Cr " + ledger_bal + suffix);
					} else {
						rowid.parent().parent().next().next().next().next().children().html("");
					}
				}
			});
		} else {
			rowid.parent().parent().next().next().next().next().children().text("");
		}
	});

	/* Recalculate Total */
	$(document).on('click', 'table td .recalculate', function() {
		/* Recalculate Total */
		$('.dr-item:first').trigger('change');
		$('.cr-item:first').trigger('change');
	});

	/* Delete ledger row */
	$(document).on('click', '.deleterow', function() {
		$(this).parent().parent().remove();
		/* Recalculate Total */
		$('.dr-item:first').trigger('change');
		$('.cr-item:first').trigger('change');
	});

	/* Add ledger row */
	$(document).on('click', '.addrow', function() {
		$('#loader').fadeIn();
		var cur_obj = this;
		var third = $('#company_id').val();
		$.ajax({
			url: '<?=base_url("entries/addrow/").$entrytype["restriction_bankcash"]."/"; ?>'+third,
			success: function(data) {
				//console.log(data);
				$(cur_obj).parent().parent().before(data);
				/* Trigger ledger item change */
					$(cur_obj).trigger('change');
				$("tr.ajax-add .ledger-dropdown2").select2({width:'100%'});



				$('#loader').fadeOut();

				$(".ledger-dropdown").select2({
					width:'100%',
					templateResult: formatOutput
				});

				val = $('#company_id').val();
				text = $('#company_id option:selected').text();
				option = "<option value='"+val+"'>"+text+"</option>";
				$('.companies_select:last').html(option);
				setCompaniesSelect();
				setLedgersSelect();
			}
		});
	});

	/* On page load initiate all triggers */
	$('.dc-dropdown').trigger('change');
	$('.ledger-dropdown').trigger('change');
	$('.dr-item:first').trigger('change');
	$('.cr-item:first').trigger('change');

	/* Calculate date range in javascript */
	startDate = new Date(<?php echo strtotime($startDateLimit) * 1000; ?>  + (new Date().getTimezoneOffset() * 60 * 1000));
	endDate = new Date(<?php echo strtotime($endDateLimit) * 1000; ?>  + (new Date().getTimezoneOffset() * 60 * 1000));

	/* Setup jQuery datepicker ui */

	<?php if (!isset($edit_entry_date)) { ?>
		$('#EntryDate').datepicker({
			minDate: startDate,
			maxDate: endDate,
			dateFormat: '<?= $this->mDateArray[1]; ?>',
			numberOfMonths: 1,
			onClose: function(selectedDate) {
				var selectedDate = $(this).datepicker("getDate");
	      var minDate = $(this).datepicker("option", "minDate");
	      var maxDate = $(this).datepicker("option", "maxDate");
	      var currentDate = new Date(); // Fecha actual

	      if (selectedDate < minDate || selectedDate > maxDate) {
	          Command: toastr.error("La fecha ingresada está fuera del rango permitido.", 'Error', {onHidden : function(){}});
	          // Ajustar al formato del datepicker
	          $(this).datepicker("setDate", currentDate);
	      }
	    }
		});
	<?php } ?>

	$(".ledger-dropdown").select2({
		width:'100%',
		templateResult: formatOutput
	});

	$(".ledger-dropdown2").select2({
		width:'100%'
	});

	function formatOutput (optionElement) {
	  if (!optionElement.id) { return optionElement.text; }
	  if (optionElement.element.value < 0) {
	  	var $state = $('<span>' + optionElement.text + '</span>');
	  } else {
	  	var $state = $('<span><strong>' + optionElement.text + '</strong></span>');
	  }
	  return $state;
	};

	$(document).on('change', '#company_id', function(){
		val = $('#company_id').val();
		text = $('#company_id option:selected').text();
		option = "<option value='"+val+"'>"+text+"</option>";
		$('.companies_select').html(option);

		setCompaniesSelect();
		var third = $(this).val();
		$('.third').val(third).trigger('change');
	});
setCompaniesSelect();
setLedgersSelect();
});

function setCompaniesSelect(){
	$('.companies_select').select2({
	    minimumInputLength: 1,
	    width:'100%',
	    ajax: {
	        url: "<?= base_url('entries/companiesoptions') ?>",
	        dataType: 'json',
	        quietMillis: 15,
	        data: function (term, page) {
	            return {
	                term: term,
	                limit: 10
	            };
	        },
	        results: function (data, page) {
	            if (data.results != null) {
	                return {results: data.results};
	            } else {
	                return {results: [{id: '', text: 'No Match Found'}]};
	            }
	        }
	    }
	});
}

function setLedgersSelect(){
	$('.ledger_select').select2({
	    minimumInputLength: 1,
	    width:'100%',
	    ajax: {
	        url: "<?= base_url('entries/ledgersoptions') ?>",
	        dataType: 'json',
	        quietMillis: 15,
	        data: function (term, page) {
	            return {
	                term: term,
	                limit: 10
	            };
	        },
	        results: function (data, page) {
	            if (data.results != null) {
	                return {results: data.results};
	            } else {
	                return {results: [{id: '', text: 'No Match Found'}]};
	            }
	        }
	    }
	});
}

</script>

<?php

$group_names_spanish = array('biller' => 'Facturador', 'customer' => 'Cliente', 'Partner' => 'Cliente', 'supplier' => 'Proveedor', 'creditor' => 'Acreedor');

// var_dump($edit_entry_date);

 ?>

<div class="row wrapper border-bottom white-bg page-heading">
  <div class="col-lg-8">

              <h2 class="box-title"><?= lang('entries_cntrler_edit_h3_title_1'); ?>
	              <select id="entrytype_label">
	              	<?php
					  	foreach($this->DB1->where('entrytypes'.$this->DB1->dbsuffix.'.origin', 1)->get('entrytypes'.$this->DB1->dbsuffix)->result_array() as $etype): ?>
					  		<option value="<?= $etype['label'] ?>" <?= ($entrytype['label'] == $etype['label']) ? "selected='selected'" : "" ?>><?= $etype['name']; ?></option>
					  	<?php endforeach; ?>
	              </select>
	              <?= lang('entries_cntrler_add_h3_title_2'); ?>
              </h2>
    <ol class="breadcrumb">
      <li>
        <a href="login/index">Inicio</a> <?php //En el controlador en la función llamada, se revisa si el usuario está logueado y lo envía a la vista principal según permisos ?>
      </li>
      <li class="active">
        <strong><?php echo $page_title; ?></strong>
      </li>
    </ol>
  </div><!-- /.col -->
  <div class="col-lg-4">
    <div class="title-action">

    </div>
  </div>
</div><!-- /.row -->

<!-- Main content -->
    <div class="row wrapper wrapper-content animated fadeInRight">
      <!-- Small boxes (Stat box) -->
      <div class="box float-e-margins">

        <!-- ./col -->
        <div class="ibox-content contentBackground">
          <div class="col-sm-12">
            <!-- /.box-header -->
            <div class="box-body">
				<div class="entry edit form">
				<?php
					if ($this->mSettings->drcr_toby == 'toby') {
						$dc_options = array(
							'D' => lang('entries_views_addrow_label_dc_toby_D'),
							'C' => lang('entries_views_addrow_label_dc_toby_C'),
						);
					} else {
						$dc_options = array(
							'D' => lang('entries_views_addrow_label_dc_drcr_D'),
							'C' => lang('entries_views_addrow_label_dc_drcr_C'),
						);
					}

					echo form_open('', array('id' => 'add_form'));

					$prefixNumber = '';
					$suffixNumber = '';

					if ( ($entrytype['prefix'] != '') && ($entrytype['suffix'] != '')) {
						$prefixNumber = '<div class="input-group"><span class="input-group-addon">' .
							$entrytype['prefix'] .
							'</span>';
						$suffixNumber = '<span class="input-group-addon">' .
							$entrytype['suffix'] .
							'</span></div>';
					} else if ($entrytype['prefix'] != '') {
						$prefixNumber = '<div class="input-group"><span class="input-group-addon">' .
							$entrytype['prefix'] .
							'</span>';
						$suffixNumber = '</div>';
					} else if ($entrytype['suffix'] != '') {
							$prefixNumber = '<div class="input-group">';
							$suffixNumber = '<span class="input-group-addon">' .
								$entrytype['suffix'] .
								'</span></div>';
					}
					echo "<div class='row'>";
						echo "<div class='col-md-3'>";
							echo '<div class="form-group">';
							echo form_label(lang('entries_views_edit_label_number'), 'number');
							echo $prefixNumber;
							$data = array(
								'id' => "number",
								'type' => "text",
								'name' => "number",
								'class' => "form-control",
								'value' => set_value('number', $entry['number']),
								'readonly' => 'readonly',
							);
							echo form_input($data);
							echo $suffixNumber;
							echo "</div>";
						echo "</div>";
						echo "<div class='col-md-3'>";
							echo '<div class="form-group">';
							echo form_label(lang('entries_views_edit_label_date'), 'date')."<span class='input_required'> *</span>";
							$data = array(
								'id' => "EntryDate",
								'type' => "text",
								'name' => "date",
								'class' => "form-control",
								'value' => set_value('date', $entry['date']),
								// 'readonly' => 'readonly',
							);

							if (isset($edit_entry_date) && $edit_entry_date === FALSE) {
								$data['readonly'] = 'readonly';
							}

							echo form_input($data);
							echo "</div>";
						echo "</div>";
						echo "<div class='col-md-3'>";
							echo '<div class="form-group">';
							echo form_label(lang('entries_views_edit_label_tag'), 'tag_id')."<span class='input_required'> *</span>";
							?>
								<select name="tag_id" class="form-control">
									<option value="0"><?= lang('entries_views_edit_tag_first_option'); ?></option>
									<?php foreach ($tag_options as $tag): ?>
										<option value="<?= $tag['id']; ?>" <?= (($tag['id'] == $entry['tag_id']) or set_value('tag_id')) ? 'selected' : ''?>><?= $tag['title']; ?></option>
									<?php endforeach; ?>
								</select>
							<?php
							echo "</div>";
						echo "</div>";
						echo '<div class="col-xs-3">';
							echo '<div class="form-group">';
							echo form_label(lang('entries_views_add_label_company'), 'companie_id')."<span class='input_required'> *</span>";
							?>

								<select name="company_id" id="company_id" class="companies_select form-control ledger-dropdown2" required>
										<option value="<?= $entry['companies_id'] ?>"><?= $entry['company_name'] ?></option>
								</select>

							<?php
							echo "</div>";
						echo "</div>";


					//echo "<hr class='col-sm-12'>";
					//echo '<div class="col-sm-3">';
					//echo '<div class="form-group">';
					//echo form_label(lang('entries_views_add_items_th_cost_center'), 'companie_id')."<span class='input_required'> *</span>";
					?>

						<!-- <select name="cost_center_id" id="cost_center_id" class="form-control " tabindex="4" required>
							<?php if ($cost_centers): ?>
								<option value=""><?= lang('select') ?></option>
								<?php foreach ($cost_centers as $cost_center): ?>
									<option value="<?= $cost_center['id'] ?>"><?= $cost_center['name'] ?></option>
								<?php endforeach ?>
							<?php else: ?>
								<option value=""><?= lang('cost_centers_not_founded') ?></option>
							<?php endif ?>
						</select> -->

					<?php
					//echo "</div>";
					//echo "</div>";

					echo "</div>";


					echo '<table class="stripped extra">';

					/* Header */
					echo '<tr>';
					if ($this->mSettings->drcr_toby == 'toby') {
						echo '<th style="width:3.5%;">' . (lang('entries_views_add_items_th_toby'))."<span class='input_required'> *</span>" . '</th>';
					} else {
						echo '<th style="width:3.5%;">' . (lang('entries_views_add_items_th_drcr'))."<span class='input_required'> *</span>" . '</th>';
					}
					echo '<th style="width:25.5%;">' . (lang('entries_views_add_items_th_ledger'))."<span class='input_required'> *</span>" . '</th>';
					echo '<th style="width:9.5%;">' . (lang('entries_views_add_items_th_dr_amount')) . ' (' . $this->mAccountSettings->currency_symbol . ')'."<span class='input_required'> *</span>" . '</th>';
					echo '<th style="width:9.5%;">' . (lang('entries_views_add_items_th_cr_amount')) . ' (' . $this->mAccountSettings->currency_symbol . ')'."<span class='input_required'> *</span>" . '</th>';
					echo '<th style="width:15.5%;">' . (lang('entries_views_add_items_th_narration'))."<span class='input_required'> *</span>" . '</th>';
					echo '<th style="width:9.5%;">' . (lang('entries_views_add_items_th_base')) . ' (' . $this->mAccountSettings->currency_symbol . ')' ."<span class='input_required'> *</span>". '</th>';
					echo '<th style="width:13.5%;">' . (lang('entries_views_add_label_company'))."<span class='input_required'> *</span>" . '</th>';
					if ($this->mAccountSettings->cost_center) {
						echo '<th style="width:10%;">' . (lang('entries_views_add_items_th_cost_center'))."<span class='input_required'> *</span>" . '</th>';
					}
					echo '<th style="width:3.5%;">' . (lang('entries_views_add_items_th_actions'))."<span class='input_required'> *</span>" . '</th>';
					echo '</tr>';

					$count = 0;
					/* Intial rows */
					foreach ($curEntryitems as $row => $entryitem) {
						echo '<tr>';

						if (empty($entryitem['dc'])) {
							echo '<td><div class="form-group-entryitem">' . form_dropdown('Entryitem[' . $row . '][dc]', $dc_options, "", array('class' => 'dc-dropdown form-control')) . '</div></td>';
						} else {
							$options = array('D' => lang('entries_views_addrow_label_dc_drcr_D'), 'C' => lang('entries_views_addrow_label_dc_drcr_C'));
							echo '<td><div class="form-group-entryitem">' . form_dropdown('Entryitem[' . $row . '][dc]', $dc_options, $entryitem['dc'], array('class' => 'dc-dropdown form-control')) . '</div></td>';
						}

						if (empty($entryitem['ledger_id'])) {
							?>
							<td>
								<div class="form-group-entryitem">
									<select class="ledger_select ledger-dropdown form-control" name="<?= 'Entryitem[' . $row . '][ledger_id]'; ?>">

									</select>
								</div>
							</td>
							<?php
						} else {
							?>
							<td>
								<div class="form-group-entryitem">
									<select class="ledger_select ledger-dropdown form-control" name="<?= 'Entryitem[' . $row . '][ledger_id]'; ?>">
										<?php if ($ledger_options && isset($ledger_options[$entryitem['ledger_id']])): ?>
											<option value="<?= $entryitem['ledger_id'] ?>"><?= $ledger_options[$entryitem['ledger_id']] ?></option>
										<?php endif ?>
									</select>
								</div>
							</td>
							<?php
						}

						if (empty($entryitem['dr_amount'])) {
							$data = array(
								'type' 	=> "text",
								'name' 	=> 'Entryitem[' . $row . '][dr_amount]',
								'class' =>  'dr-item form-control',
							);
							echo "<td><div class='form-group-entryitem'>";
							echo form_input($data);
							echo "</div></td>";
						} else {
							$data = array(
								'value' => $entryitem['dr_amount'],
								'type' 	=> "text",
								'name' 	=> 'Entryitem[' . $row . '][dr_amount]',
								'class' => 'dr-item form-control',
							);
							echo "<td><div class='form-group-entryitem'>";
							echo form_input($data);
							echo "</div></td>";
						}

						if (empty($entryitem['cr_amount'])) {
							$data = array(
								'type'	=> "text",
								'name' 	=> 'Entryitem[' . $row . '][cr_amount]',
								'class' => 'cr-item form-control',
							);
							echo "<td><div class='form-group-entryitem'>";
							echo form_input($data);
							echo "</div></td>";

						} else {
							$data = array(
								'value' => $entryitem['cr_amount'],
								'type' => "text",
								'name' => 'Entryitem[' . $row . '][cr_amount]',
								'class' =>  'cr-item form-control',
							);
							echo "<td><div class='form-group-entryitem'>";
							echo form_input($data);
							echo "</div></td>";
						}
						$data = array(
							'type'  => "text",
							'value' => $entryitem['narration'],
							'name'  => 'Entryitem[' . $row . '][narration]',
							'class' => 'form-control',
						);
						echo "<td><div class='form-group-entryitem'>";
						echo form_input($data);
						echo "</div></td>";
						/* BASE */
						echo '<td>'; ?>
							<input type="number" class="form-control" name="<?= 'Entryitem['.$row.'][base]' ?>" value="<?= (!empty($entryitem['base']))  ? $entryitem['base'] : 0; ?>">
						<?php echo '</td>';
						/* BASE */
						/* TERCERO */
						echo "<td>"; ?>

						<select name="<?= 'Entryitem['.$row.'][companies_id]' ?>" class="companies_select form-control third ledger-dropdown2" required>
								<option value="<?= $entryitem['companies_id'] ?>"><?= $entryitem['company_name'] ?></option>
						</select>

						<?php
						echo "</td>";
						/* TERCERO */
						if ($this->mAccountSettings->cost_center) {
							/* CENTRO DE COSTO */
							echo "<td>";
							?>

							<select name="<?= 'Entryitem['.$row.'][cost_center_id]' ?>" class="cost_center_select form-control" required>
								<?php if ($cost_centers): ?>
									<option value=""><?= lang('select') ?></option>
									<?php foreach ($cost_centers as $cost_center): ?>
										<option value="<?= $cost_center['id'] ?>" <?= $entryitem['cost_center_id'] == $cost_center['id'] ? 'selected="selected"' : '' ?> ><?= $cost_center['name'] ?></option>
									<?php endforeach ?>
								<?php else: ?>
									<option value=""><?= lang('cost_centers_not_founded') ?></option>
								<?php endif ?>
							</select>

							<?php
							echo "</td>";
							/* CENTRO DE COSTO */
						}
						echo '<td>';
						if ($count > 1) {
							echo '<button class="deleterow btn btn-danger btn-sm" type="button"><span class="glyphicon glyphicon-trash" escape="false"></span></button>';
						}
						// echo '<button class="btn btn-danger deleterow" escape="false"  type="button"><span class="fa fa-trash"></span></button>';
						echo '</td>';
						echo '</tr>';
						$count++;
					}

					/* Total and difference */
					echo '<tr class="bold-text">' . '<td>' . (lang('entries_views_edit_items_td_total')) . '</td>' . '<td>' . '</td>' . '<td id="dr-total">' . '</td>' . '<td id="cr-total">' . '</td>' . '<td >' . '<button class="btn btn-primary recalculate"  type="button" escape="false"><i class="glyphicon glyphicon-refresh"></i></button>' . '</td>' . '<td>' . '</td>' . '<td>' . '<button type="button" class="btn btn-primary addrow" escape="false"><i class="fa fa-plus"></i></button>' . '</td>' . '</tr>';
					echo '<tr class="bold-text">' . '<td>' . (lang('entries_views_edit_items_td_diff')) . '</td>' . '<td>' . '</td>' . '<td id="dr-diff">' . '</td>' . '<td id="cr-diff">' . '</td>' . '<td>' . '</td>' . '<td>' . '</td>' . '<td>' . '</td>' . '</tr>';

					echo '</table>';

					echo '<br />';
					echo '<div class="form-group">';
					echo form_label(lang('entries_views_edit_label_note'), 'notes')."<span class='input_required'> *</span>";
					echo form_textarea('notes', set_value('notes', $entry['notes']), array("rows"=>3, "class"=>"form-control"));
					echo "</div>";

					echo '<div class="form-group">';
					echo '<button type="button" class="btn btn-success" id="btn_submit">'.lang('entries_views_add_label_submit_btn').'</button>';
					echo '<span class="link-pad"></span>';
					echo anchor('entries/index', lang('entries_views_edit_label_cancel_btn'), array('class' => 'btn btn-default'));
					echo '<a></span>';
					echo '</div>';
					echo form_close();
				?>
				</div>
            </div>
          </div>
      </div>
      <!-- /.row -->
    </div>
    <!-- /.content -->
<script type="text/javascript">
		$(document).on('click', '#btn_submit', function(){
			if ($('select.dc-dropdown').length > 0) {
				if ($('#add_form').valid()) {
					$(this).prop('disabled', false);
					setTimeout(function() {
						$('#add_form').submit();
					}, 1100);
				}
			} else {
				Command: toastr.error('La nota no puede registrarse sin detalle.', 'Error', {onHidden : function(){}})
			}
		});
</script>