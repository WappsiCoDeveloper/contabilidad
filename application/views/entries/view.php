<script type="text/javascript">
$(document).ready(function() {

	var entryId = 0;
	$("button#send").click(function() {
		$(".modal-body").hide();
		$(".modal-footer").hide();
		$(".modal-ajax").show();
		$.ajax({
			type: "POST",
			url: '<?php echo base_url("entries/email"); ?>/' + entryId,
			data: $('form#emailSubmit').serialize(),
				success: function(response) {
					msg = JSON.parse(response); console.log(msg);
					if (msg['status'] == 'success') {
						$(".modal-error-msg").html("");
						$(".modal-error-msg").hide();
						$(".modal-body").show();
						$(".modal-footer").show();
						$(".modal-ajax").hide();
						$("#emailModal").modal('hide');
					} else {
						$(".modal-error-msg").html(msg['msg']);
						$(".modal-error-msg").show();
						$(".modal-body").show();
						$(".modal-footer").show();
						$(".modal-ajax").hide();
					}
				},
				error: function() {
					$(".modal-error-msg").html("<?= lang('entries_views_views_email_not_sent_msg') ?>");
					$(".error-msg").show();
					$(".modal-body").show();
					$(".modal-footer").show();
					$(".modal-ajax").hide();
				}
		});
	});

	$('#emailModal').on('show.bs.modal', function(e) {
		$(".modal-error-msg").html("");
		$(".modal-ajax").hide();
		$(".modal-error-msg").hide();
		entryId = $(e.relatedTarget).data('id');
		var entryType = $(e.relatedTarget).data('type');
		var entryNumber = $(e.relatedTarget).data('number');
		$("#emailModelType").html(entryType);
		$("#emailModelNumber").html(entryNumber);
	});
});

</script>

<div class="row wrapper border-bottom white-bg page-heading">
  <div class="col-lg-8">
    <h2><?php echo $page_title; ?></h2>
    <ol class="breadcrumb">
      <li>
        <a href="login/index">Inicio</a> <?php //En el controlador en la función llamada, se revisa si el usuario está logueado y lo envía a la vista principal según permisos ?>
      </li>
      <li class="active">
        <strong><?php echo $page_title; ?></strong>
      </li>
    </ol>
  </div><!-- /.col -->
  <div class="col-lg-4">
    <div class="title-action">
      <div class="btn-group">
        <div class="dropdown">
          <button class="btn btn-success"  type="button" id="accionesGrupo" data-toggle="dropdown" aria-haspopup="true">
             Acciones
            <span class="caret"></span>
          </button>
          <ul class="dropdown-menu pull-right" aria-labelledby="accionesGrupo" style="width: 220%;">
           <?php if ($entry['origin'] == 1 && $entry['state'] == 2): ?>
	           <li>
	              <a href="<?= base_url('entries/edit/').$entrytype['label'].'/'.$entry['id'];?>"><span class="fa fa-pencil"></span>  <?= lang('entries_views_views_td_actions_edit_btn'); ?></a>
	           </li>
	           <li class="cambiarEstado">
					<a href="#approve_entry_modal" data-toggle="modal" data-entrytype="<?= $entrytype['label'] ?>" data-entry="<?= $entry['id'] ?>">
						<?= lang('entries_views_views_td_actions_state') ?>
						<input type="checkbox" data-toggle="toggle" data-on="Aprobado" data-off="Desaprobado" data-size="mini" data-width="70%" />
					</a>
			    </li>
           <?php endif ?>
           <li>
           	  <a href="<?= base_url('entries/export/').$entrytype['label'].'/'.$entry['id'];?>/xls"><span class="fa fa-file-excel-o"></span>  <?=lang('export_to_xls');?></a>
           </li>
           <li>
           	  <a href="<?= base_url('entries/export/').$entrytype['label'].'/'.$entry['id'];?>/pdf"><span class="fa fa-file-pdf-o"></span>  <?=lang('export_to_pdf');?></a>
           </li>
           <?php if ($entry['origin'] == 1 && $entry['state'] == 1): ?>
	           	<li class="cambiarEstado">
			    	<a href="#delete_entry_modal" data-toggle="modal" data-entry="<?= $entry['id'] ?>" data-entrytype="<?= $entrytype['label'] ?>">
			    	 <?= lang('entries_views_views_td_actions_state') ?>
			    		<input type="checkbox" checked data-identry="<?= $entry['id'] ?>" data-toggle="toggle" data-on="Aprobado" data-off="Anulado" data-size="mini" data-width="70%" /> 
			    	</a>
			    </li>
           <?php endif ?>
          </ul>
        </div>
      </div>  
    </div>
  </div>
</div><!-- /.row -->



	<!-- Main content -->
    <section class="row wrapper wrapper-content animated fadeInRight">
      <!-- Small boxes (Stat box) -->
      <div class="ibox float-e-margins">
        
        <div class="ibox-content contentBackground">
          <div class="col-xs-12">
            <div class="box-header with-border">
            	<div class="row">
            		<div class="col-md-12">
              			<!-- <h3 class="box-title"><?= lang('entries_views_views_title') ?></h3>            		</div> -->
            	</div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

            	<div class="row">
            		<div class="col-md-2">
            			<label><?= lang('entries_views_views_label_number') ?></label><br>
            			<h3><?= $this->functionscore->toEntryNumber($entry['number'], $entry['entrytype_id']) ?></h3>
            			<?php if ($entry['state'] == '1'): ?>
            				<p style="margin-left:0.6%;"><span class="label label-primary">Aprobado</span><p>
            			<?php elseif ($entry['state'] == '2'): ?>
            				<p style="margin-left:0.6%;"><span class="label">Desaprobado</span></p>
            			<?php elseif ($entry['state'] == '0'): ?>
            				<p style="margin-left:0.6%;"><span class="label label-warning">Anulada</span></p>
            			<?php endif ?>
            		</div>
            		<div class="col-md-2">
            			<label><?= lang('entries_views_views_label_date') ?></label><br>
            			<h3><?= $this->functionscore->dateFromSql($entry['date']) ?></h3>
            		</div>
            		<div class="col-md-2">
            			<label><?= lang('entries_views_views_entry_type') ?></label><br>
            			<h3><?= $entrytype['name'] ?></h3>
            		</div>
            		<div class="col-md-2">
            			<label><?= lang('entries_views_views_th_companies_id') ?></label><br>
            			<h3><?= $entry['company_name'] ?></h3>
            		</div>
            		<div class="col-md-2">
            			<label><?= lang('entry_origin') ?></label><br>
            			<h3><?= $entry['origin'] == 1 ? 'Contabilidad' : 'Comercial' ?></h3>
            		</div>
            		<div class="col-md-2">
            			<label><?= lang('entry_user') ?></label><br>
            			<h3><?= $created_by ? ucwords(mb_strtolower($created_by->first_name." ".$created_by->last_name)) : "N/A" ?></h3>
            		</div>

            		
            	</div>

              <div>
				<?php
					echo '<br /><br />';
					echo '<table class="table stripped">';
					/* Header */
					echo '<tr>';
					// if ($this->mSettings->drcr_toby == 'toby') {
					// 	echo '<th>' . lang('entries_views_views_th_to_by') . '</th>';
					// } else {
					// 	echo '<th>' . lang('entries_views_views_th_dr_cr') . '</th>';
					// }
					echo '<th>' . lang('entries_views_views_th_ledger') . '</th>';
					echo '<th>' . lang('entries_views_views_th_dr_amount') . ' (' . $this->mAccountSettings->currency_symbol . ')' . '</th>';
					echo '<th>' . lang('entries_views_views_th_cr_amount') . ' (' . $this->mAccountSettings->currency_symbol . ')' . '</th>';
					echo '<th>' . lang('entries_views_views_th_narration') . '</th>';
					echo '<th>' . lang('entries_views_views_th_base') . '</th>';
					echo '<th>' . lang('entries_views_views_th_companies_id') . '</th>';
					if ($this->mAccountSettings->cost_center) {
						echo '<th>' . lang('sidebar_menu_account_settings_cost_centers') . '</th>';
					}
					echo '</tr>';

					/* Intial rows */
					foreach ($curEntryitems as $row => $entryitem) {
						echo '<tr>';

						// echo '<td>';
						// if ($this->mSettings->drcr_toby == 'toby') {
						// 	if ($entryitem['dc'] == 'D') {
						// 		echo lang('entries_views_views_toby_D');
						// 	} else {
						// 		echo lang('entries_views_views_toby_C');
						// 	}
						// } else {
						// 	if ($entryitem['dc'] == 'D') {
						// 		echo lang('entries_views_views_drcr_D');
						// 	} else {
						// 		echo lang('entries_views_views_drcr_C');
						// 	}
						// }
						echo '</td>';

						echo '<td>';
						echo ($entryitem['ledger_name']);
						echo '</td>';

						echo '<td class="text-right">';
						if ($entryitem['dc'] == 'D') {
							echo $this->functionscore->toCurrency('D', $entryitem['dr_amount']);
						} else {
							echo '';
						}
						echo '</td>';

						echo '<td class="text-right">';
						if ($entryitem['dc'] == 'C') {
							echo $this->functionscore->toCurrency('C', $entryitem['cr_amount']);
						} else {
							echo '';
						}
						echo '</td>';
						echo '<td>';
						echo $entryitem['narration'];
						echo '</td>';

						/*BASE*/
						echo '<td class="text-right">';
						echo $entryitem['base'];
						echo '</td>';
						/*BASE*/

						/*TERCERO*/
						echo '<td>';
						$compañia = $entryitem['company_name'];
						echo $compañia;
						echo '</td>';
						if ($this->mAccountSettings->cost_center) {
							/*CENTRO DE COSTO*/
							echo '<td>';
							echo $entryitem['cost_center_id'];
							echo '</td>';
							
							/*CENTRO DE COSTO*/
						}
						/*BASE*/
						echo '</tr>';
					}

					/* Total */
					echo '<tr class="bold-text">' . '' . '<td>' . lang('entries_views_views_td_total') . '</td>' . '<td id="dr-total"  class="text-right">' . $this->functionscore->toCurrency('D', $entry['dr_total']) . '</td>' . '<td id="cr-total"  class="text-right">' . $this->functionscore->toCurrency('C', $entry['cr_total']) . '</td>' . '<td></td>' . '</tr>';

					/* Difference */
					if ($this->functionscore->calculate($entry['dr_total'], $entry['cr_total'], '==')) {
						/* Do nothing */
					} else {
						if ($this->functionscore->calculate($entry['dr_total'], $entry['cr_total'], '>')) {
							echo '<tr class="error-text">' . '<td></td>' . '<td>' . lang('entries_views_views_td_diff') . '</td>' . '<td id="dr-diff">' . $this->functionscore->toCurrency('D', $this->functionscore->calculate($entry['dr_total'], $entry['cr_total'], '-')) . '</td>' . '<td></td>' . '</tr>';
						} else {
							echo '<tr class="error-text">' . '<td></td>' . '<td>' . lang('entries_views_views_td_diff') . '</td>' . '<td></td>' . '<td id="cr-diff">' . $this->functionscore->toCurrency('C', $this->functionscore->calculate($entry['cr_total'], $entry['dr_total'], '-')) . '</td>' . '</tr>';

						}
					}

					echo '</table>';

					echo '<br />';
					// echo lang('entries_views_views_td_tag') . ' : ' . $this->functionscore->showTag($entry['tag_id']);

					echo '<br /><br />';
					?>
					
					<?php
				?>

				<table class="table">
					<tr>
						<th>Nota : </th>
						<td><?= $entry['notes'] ?></td>
					</tr>
				</table>


					<a href="<?= base_url('entries/')?>" class="btn btn-default">  <?= lang('entries_views_views_td_actions_cancel_btn'); ?></a>
				</div>
            </div>
          </div>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->


<div class="modal fade" tabindex="-1" role="dialog" id="delete_entry_modal" aria-labelledby="deleteentry">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><?= lang('entrys_index_delete_entry_alert_title'); ?></h4>
      </div>
        <div class="modal-body">
          <input type="hidden" name="entry_id_delete" id="entry_id_delete">
          <input type="hidden" name="entry_type_delete" id="entry_type_delete">
          <?= lang('entrys_index_delete_entry_alert_text'); ?>
        </div>
        <div class="modal-footer">
          <button class="btn btn-default" data-dismiss="modal"><?=lang('no');?></button>
          <button class="btn btn-danger confirm_delete_entry" ><?=lang('yes');?></button>
        </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div class="modal fade" tabindex="-1" role="dialog" id="approve_entry_modal" aria-labelledby="approveentry">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><?= lang('entrys_index_approve_entry_alert_title'); ?></h4>
      </div>
        <div class="modal-body">
          <input type="hidden" name="entry_id_approve" id="entry_id_approve">
          <input type="hidden" name="entry_type_approve" id="entry_type_approve">
          <?= lang('entrys_index_approve_entry_alert_text'); ?>
        </div>
        <div class="modal-footer">
          <button class="btn btn-default" data-dismiss="modal"><?=lang('no');?></button>
          <button class="btn btn-primary confirm_approve_entry" ><?=lang('yes');?></button>
        </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<script type="text/javascript">

var btnEstado;


$('#delete_entry_modal').on('show.bs.modal', function (event) {
	var button = $(event.relatedTarget);
      btnEstado = button;
  	entry = button.data('entry');
  	entrytype = button.data('entrytype');
  	$('#entry_id_delete').val(entry);
  	$('#entry_type_delete').val(entrytype);
});

$('#delete_entry_modal').on('hide.bs.modal', function (event) {
    btnEstado.find('input').bootstrapToggle('on');
});

$('.confirm_delete_entry').on('click', function(){
  	entry = $('#entry_id_delete').val();
  	entrytype = $('#entry_type_delete').val();
  	window.location.href = "<?= base_url();?>entries/delete/"+entrytype+"/"+entry;
});

$('#approve_entry_modal').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget);
    btnEstado = button;
    entry = button.data('entry');
    entrytype = button.data('entrytype');
    $('#entry_id_approve').val(entry);
    $('#entry_type_approve').val(entrytype);
});

$('#approve_entry_modal').on('hide.bs.modal', function (event) {
    btnEstado.find('input').bootstrapToggle('off');
});

$('.confirm_approve_entry').on('click', function(){
    entry = $('#entry_id_approve').val();
    entrytype = $('#entry_type_approve').val();
    window.location.href = "<?= base_url();?>entries/approve/"+entrytype+"/"+entry;
});

$(document).on('click', '.cambiarEstado', function(event){
    event.stopPropagation();
  });

</script>