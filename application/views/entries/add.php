<script type="text/javascript">
$(document).ready(function() {
	if (localStorage.getItem('eData')) {
		// $('.addrow').attr('disabled', false);
		var entryItems = new Array();
		var eData = JSON.parse(localStorage.getItem('eData'));
		eData.splice(0, 1);
		eData.splice(1, 1);
		$.each(eData, function(index, val){
			if (val.name == 'number') {
				return;
			}
			if ($('input[name="'+val.name+'"]').length == 1) {
				$('input[name="'+val.name+'"]').val(val.value);
			}
			if ($('textarea[name="'+val.name+'"]').length == 1) {
				$('textarea[name="'+val.name+'"]').val(val.value);
			}
			nombre = val.name;
			if (nombre.indexOf("Entryitem") != -1 && ($('select[name="'+val.name+'"]').length != 1 && $('input[name="'+val.name+'"]').length != 1)) {
				if (nombre.indexOf("[dc]") != -1) {
					item = new Array();
					item.push({'dc' : val.value});
				}
				if (nombre.indexOf("[ledger_id]") != -1) {
					item.push({'ledger_id' : val.value});
				}
				if (nombre.indexOf("[cr_amount]") != -1) {
					item.push({'cr_amount' : val.value});
				}
				if (nombre.indexOf("[dr_amount]") != -1) {
					item.push({'dr_amount' : val.value});
				}
				if (nombre.indexOf("[narration]") != -1) {
					item.push({'narration' : val.value});
				}
				if (nombre.indexOf("[base]") != -1) {
					item.push({'base' : val.value});
				}
				if (nombre.indexOf("[companies_id]") != -1) {
					item.push({'companies_id' : val.value});
					entryItems.push(item);
				}
			} else {
				delete eData[index];
			}
		});
		var third = $('#company_id').val();
		$('#loader').fadeIn();
		$.ajax({
			url: '<?=base_url("entries/addrow2") ?>',
			type: 'POST',
			data : {"entryItems" : JSON.stringify(entryItems), 'restriction_bankcash' : '<?= $entrytype["restriction_bankcash"] ?>', 'third' : third},
		}).done(function(data){
			$('.extra tbody').append(data);
			$('#loader').fadeOut();
			$('.dc-dropdown').trigger('change');
			localStorage.removeItem('eData');
			setCompaniesSelect();
			setLedgersSelect();
		});
	} else {
		$('#loader').fadeOut();
		$('.addrow').prop('disabled', true);
	}

	/* javascript floating point operations */
	var jsFloatOps = function(param1, param2, op) {
		param1 = parseFloat(param1);
		param2 = parseFloat(param2);
		var result = 0;
		if (op == '+') {
			result = param1 + param2;
			return Math.round(result * 100) / 100;
		}
		if (op == '-') {
			result = param1 - param2;
			return Math.round(result * 100) / 100;
		}
		if (op == '!=') {
			if (param1 != param2)
				return true;
			else
				return false;
		}
		if (op == '==') {
			if (param1 == param2)
				return true;
			else
				return false;
		}
		if (op == '>') {
			if (param1 > param2)
				return true;
			else
				return false;
		}
		if (op == '<') {
			if (param1 < param2)
				return true;
			else
				return false;
		}
	}

	/* Calculating Dr and Cr total */
	$(document).on('change', '.dr-item', function() {
		var valinserted = $(this).val();
		$("table tr .cr-item:not([disabled])").each(function() {
			if ($(this).val() == '' || $(this).val() == 0) {
				$(this).val(valinserted);
			}

		});
		$("table tr .dr-item:not([disabled])").each(function() {
			if ($(this).val() == '' || $(this).val() == 0) {
				$(this).val(valinserted);
			}

		});
		var drTotal = 0;
		$("table tr .dr-item").each(function() {
			var curDr = $(this).prop('value');
			curDr = parseFloat(curDr);
			if (isNaN(curDr))
				curDr = 0;
			drTotal = jsFloatOps(drTotal, curDr, '+');
			// console.log(curDr);
			// console.log(drTotal);
		});
		$("table tr #dr-total").text(drTotal);
		var crTotal = 0;
		$("table tr .cr-item").each(function() {
			var curCr = $(this).prop('value');
			curCr = parseFloat(curCr);
			if (isNaN(curCr))
				curCr = 0;
			crTotal = jsFloatOps(crTotal, curCr, '+');
		});
		$("table tr #cr-total").text(crTotal);
		if (jsFloatOps(drTotal, crTotal, '==')) {
			$("table tr #dr-total").css("background-color", "#FFFF99");
			$("table tr #cr-total").css("background-color", "#FFFF99");
			$("table tr #dr-diff").text("-");
			$("table tr #cr-diff").text("");
		} else {
			$("table tr #dr-total").css("background-color", "#FFE9E8");
			$("table tr #cr-total").css("background-color", "#FFE9E8");
			if (jsFloatOps(drTotal, crTotal, '>')) {
				$("table tr #dr-diff").text("");
				$("table tr #cr-diff").text(jsFloatOps(drTotal, crTotal, '-'));
			} else {
				$("table tr #dr-diff").text(jsFloatOps(crTotal, drTotal, '-'));
				$("table tr #cr-diff").text("");
			}
		}
	});

	$(document).on('change', '.cr-item', function() {
		var drTotal = 0;
		var valinserted = $(this).val();
		$("table tr .cr-item:not([disabled])").each(function() {
			if ($(this).val() == '' || $(this).val() == 0) {
				$(this).val(valinserted);
			}
		});
		$("table tr .dr-item:not([disabled])").each(function() {
			if ($(this).val() == '' || $(this).val() == 0) {
				$(this).val(valinserted);
			}
		});
		$("table tr .dr-item").each(function() {
			var curDr = $(this).prop('value')
			curDr = parseFloat(curDr);
			if (isNaN(curDr))
				curDr = 0;
			drTotal = jsFloatOps(drTotal, curDr, '+');
		});
		$("table tr #dr-total").text(drTotal);
		var crTotal = 0;
		$("table tr .cr-item").each(function() {
			var curCr = $(this).prop('value')
			curCr = parseFloat(curCr);
			if (isNaN(curCr))
				curCr = 0;
			crTotal = jsFloatOps(crTotal, curCr, '+');
		});
		$("table tr #cr-total").text(crTotal);
		if (jsFloatOps(drTotal, crTotal, '==')) {
			$("table tr #dr-total").css("background-color", "#FFFF99");
			$("table tr #cr-total").css("background-color", "#FFFF99");
			$("table tr #dr-diff").text("-");
			$("table tr #cr-diff").text("");
		} else {
			$("table tr #dr-total").css("background-color", "#FFE9E8");
			$("table tr #cr-total").css("background-color", "#FFE9E8");
			if (jsFloatOps(drTotal, crTotal, '>')) {
				$("table tr #dr-diff").text("");
				$("table tr #cr-diff").text(jsFloatOps(drTotal, crTotal, '-'));
			} else {
				$("table tr #dr-diff").text(jsFloatOps(crTotal, drTotal, '-'));
				$("table tr #cr-diff").text("");
			}
		}
	});

	/* Dr - Cr dropdown changed */
	$(document).on('change', '.dc-dropdown', function() {
		var drValue = $(this).parent().parent().next().next().children().children().prop('value');
		var crValue = $(this).parent().parent().next().next().next().children().children().prop('value');
		if ($(this).parent().parent().next().children().children().val() == "0") {
			return;
		}
		drValue = parseFloat(drValue);
		if (isNaN(drValue))
			drValue = 0;
		crValue = parseFloat(crValue);
		if (isNaN(crValue))
			crValue = 0;
		if ($(this).prop('value') == "D") {
			if (drValue == 0 && crValue != 0) {
				$(this).parent().parent().next().next().children().children().prop('value', crValue);
			}
			$(this).parent().parent().next().next().next().children().children().prop('value', "");
			$(this).parent().parent().next().next().next().children().children().prop('disabled', 'disabled');
			$(this).parent().parent().next().next().children().children().prop('disabled', '');
		} else {
			if (crValue == 0 && drValue != 0) {
				$(this).parent().parent().next().next().next().children().prop('value', drValue);
			}
			$(this).parent().parent().next().next().children().children().prop('value', "");
			$(this).parent().parent().next().next().children().children().prop('disabled', 'disabled');
			$(this).parent().parent().next().next().next().children().children().prop('disabled', '');
		}
		/* Recalculate Total */
		$('.dr-item:first').trigger('change');
		$('.cr-item:first').trigger('change');
	});

	/* Ledger dropdown changed */
	$(document).on('change', '.ledger-dropdown', function() {
		if ($(this).val() == "0") {
			/* Reset and diable dr and cr amount */
			$(this).parent().parent().next().children().children().prop('value', "");
			$(this).parent().parent().next().next().children().children().prop('value', "");
			$(this).parent().parent().next().children().children().prop('disabled', 'disabled');
			$(this).parent().parent().next().next().children().children().prop('disabled', 'disabled');
		} else {
			/* Enable dr and cr amount and trigger Dr/Cr change */
			$(this).parent().parent().next().children().children().prop('disabled', '');
			$(this).parent().parent().next().next().children().children().prop('disabled', '');
			$(this).parent().parent().prev().children().children().trigger('change');
			// if (validar_ledger_vacio($(this)) > 0) {
			// 	Command: toastr.error('Debe diligenciar primero las anteriores cuentas auxiliares.', 'Error', {onHidden : function(){}})
			// 	$(this).val(0).trigger('change');
			// }
		}
		/* Trigger dr and cr change */
		$(this).parent().parent().next().children().children().trigger('change');
		$(this).parent().parent().next().next().children().children().trigger('change');
		var ledgerid = $(this).val();
		var rowid = $(this);
		if (ledgerid > 0) {
			$.ajax({
				url: '<?=base_url("ledgers/cl"); ?>',
				data: 'id=' + ledgerid,
				dataType: 'json',
				success: function(data)
				{
					var ledger_bal = parseFloat(data['cl']['amount']);
					var prefix = '';
					var suffix = '';
					if (data['cl']['status'] == 'neg') {
						prefix = '<span class="error-text">';
						suffix = '</span>';
					}
					if (data['cl']['dc'] == 'D') {
						rowid.parent().parent().next().next().next().next().children().html(prefix + "Dr " + ledger_bal + suffix);
					} else if (data['cl']['dc'] == 'C') {
						rowid.parent().parent().next().next().next().next().children().html(prefix + "Cr " + ledger_bal + suffix);
					} else {
						rowid.parent().parent().next().next().next().next().children().html("");
					}
				}
			});
		} else {
			rowid.parent().parent().next().next().next().next().children().text("");
		}
	});

	/* Recalculate Total */
	$(document).on('click', 'table td .recalculate', function() {
		/* Recalculate Total */
		$('.dr-item:first').trigger('change');
		$('.cr-item:first').trigger('change');
	});

	/* Delete ledger row */
	$(document).on('click', '.deleterow', function() {
		$(this).parent().parent().remove();
		/* Recalculate Total */
		$('.dr-item:first').trigger('change');
		$('.cr-item:first').trigger('change');
	});

	/* Add ledger row */
	$(document).on('click', '.addrow', function() {
		lastnarration = $('input[name$="[narration]"]:last').val();

		cr_diff = $('#cr-diff').text();
		dr_diff = $('#dr-diff').text();

		$('#loader').fadeIn();
		var cur_obj = this;
		var third = $('#company_id').val();
		$.ajax({
			url: '<?=base_url("entries/addrow/").$entrytype["restriction_bankcash"]."/"; ?>'+third,
			success: function(data) {
				//console.log(data);
				$(cur_obj).parent().parent().before(data);
				/* Trigger ledger item change */
					$(cur_obj).trigger('change');
				$('#loader').fadeOut();
				$('input[name$="[narration]"]:last').val(lastnarration);
				if(cr_diff != "" && cr_diff != "-"){
					$('.dc-dropdown:last').val('C');
					$('.cr-item:last').val(cr_diff).trigger('change');
				} else if (dr_diff != "" && dr_diff != "-") {
					$('.dc-dropdown:last').val('D');
					$('.dr-item:last').val(dr_diff).trigger('change');
				}
				val = $('#company_id').val();
				text = $('#company_id option:selected').text();
				option = "<option value='"+val+"'>"+text+"</option>";
				$('.companies_select:last').html(option);
				$('.cost_center_select:last').val($('#cost_center_id').val());
				setCompaniesSelect();
				setLedgersSelect();
			}
		});
	});
	$(document).on('keypress', 'form', function(e){
	    if(e == 13){
	      return false;
	    }
	});
    $(document).on('keypress', 'input', function(e){
        if(e.which == 13){
          return false;
        }
    });
	/* On page load initiate all triggers */
	$('.dc-dropdown').trigger('change');
	$('.ledger-dropdown').trigger('change');
	$('.dr-item:first').trigger('change');
	$('.cr-item:first').trigger('change');
	/* Calculate date range in javascript */
	startDate = new Date(<?php echo strtotime((isset($limit_date) && $isNumbering != 3) ? $limit_date : $this->mAccountSettings->fy_start) * 1000; ?>  + (new Date().getTimezoneOffset() * 60 * 1000));
	endDate = new Date(<?php echo strtotime($this->mAccountSettings->fy_end) * 1000; ?>  + (new Date().getTimezoneOffset() * 60 * 1000));
	/* Setup jQuery datepicker ui */
	$('#EntryDate').datepicker({
			// minDate: startDate,
			maxDate: endDate,
		dateFormat: '<?= $this->mDateArray[1]; ?>',
		numberOfMonths: 1,
		onClose: function(selectedDate) {
			var selectedDate = $(this).datepicker("getDate");
      var minDate = $(this).datepicker("option", "minDate");
      var maxDate = $(this).datepicker("option", "maxDate");
      var currentDate = new Date(); // Fecha actual

      if (selectedDate < minDate || selectedDate > maxDate) {
          Command: toastr.error("La fecha ingresada está fuera del rango permitido.", 'Error', {onHidden : function(){}})
          // Ajustar al formato del datepicker
          $(this).datepicker("setDate", currentDate);
      }
		}
	});
	<?php if ($isNumbering == 1): ?>
	$("#EntryDate").datepicker("option", "minDate", startDate);
	<?php endif ?>
	function formatOutput (optionElement) {
	  if (!optionElement.id) { return optionElement.text; }
	  if (optionElement.element.value < 0) {
	  	var $state = $('<span>' + optionElement.text + '</span>');
	  } else {
	  	var $state = $('<span><strong>' + optionElement.text + '</strong></span>');
	  }
	  return $state;
	};
	$(document).on('change', '#company_id', function(){
		console.log('Cambió tercero general');
		var val = $('#company_id').val();
		text = $('#company_id option:selected').text();
		option = "<option value='"+val+"'>"+text+"</option>";
		$('.companies_select').html(option);
		setCompaniesSelect();
		if (val != "") {
			$('.addrow').removeAttr('disabled');
		} else {
			$('.addrow').attr('disabled', true);
		}

	});
	$(document).on('change', '#cost_center_id', function(){
		val = $(this).val();
		$('.cost_center_select').val(val);
	});
	$(document).on('change', '#entrytype_label', function(){
		var etype = $(this).val();
		$('#loader').fadeIn();
		if (localStorage.getItem('eData')) {
			localStorage.removeItem('eData');
		}
		var dataArr = $('form').serializeArray();
		var data = JSON.stringify(dataArr);
		localStorage.setItem('eData', data);
		setTimeout(function() {
			location.href='<?= base_url() ?>entries/add/'+etype;
		}, 300);
	});
	$(document).on('change', '#number', function(){
		input = $(this);
		isnumbering = $('#isNumbering').val();
		if (isnumbering == 2) {
			etid = $('#entrytype_label option:selected').data('etid');
			number = $('#number').val();
			console.log('id:'+etid+", number:"+number);
			$.ajax({
				url:"<?= base_url() ?>entries/manualconsecutive/"+etid+"/"+number
			}).done(function(data){
				if (data == 1) {
					input.val('').focus();
					Command: toastr.error('Ya existe una nota contable con el mismo tipo y el mismo consecutivo.', 'Error', {onHidden : function(){}})

				} else {
					console.log(data);
				}
			});
		}
	});
	setTimeout(function() {
		setCompaniesSelect();
		setLedgersSelect();
	}, 800);
	<?php if (isset($third_duplicate)): ?>
		$('.addrow').prop('disabled', false);
	<?php endif ?>
});

function setCompaniesSelect(){
	$('.companies_select').select2({
	    minimumInputLength: 1,
	    width:'100%',
	    ajax: {
	        url: "<?= base_url('entries/companiesoptions') ?>",
	        dataType: 'json',
	        quietMillis: 15,
	        data: function (term, page) {
	            return {
	                term: term,
	                limit: 10
	            };
	        },
	        results: function (data, page) {
	            if (data.results != null) {
	                return {results: data.results};
	            } else {
	                return {results: [{id: '', text: 'No Match Found'}]};
	            }
	        }
	    }
	});
}

function setLedgersSelect(){
	$('.ledger_select').select2({
	    minimumInputLength: 1,
	    width:'100%',
	    ajax: {
	        url: "<?= base_url('entries/ledgersoptions') ?>",
	        dataType: 'json',
	        quietMillis: 15,
	        data: function (term, page) {
	            return {
	                term: term,
	                limit: 10
	            };
	        },
	        results: function (data, page) {
	            if (data.results != null) {
	                return {results: data.results};
	            } else {
	                return {results: [{id: '', text: 'No Match Found'}]};
	            }
	        }
	    }
	});
}

function validar_ledger_vacio(select){
	listselect = $('.ledger-dropdown');
	indexselect = select.index(listselect);
	var cnt = 0;
	$.each($('.ledger-dropdown:lt('+indexselect+')'), function(){
		if($(this).val() == 0) cnt++;
	});
	return cnt;
}
</script>
<?php
$group_names_spanish = array('biller' => 'Facturador', 'customer' => 'Cliente', 'Partner' => 'Cliente', 'supplier' => 'Proveedor', 'creditor' => 'Acreedor');
 ?>

<style type="text/css">
	#entrytype_label option {
		font-size:70% !important;
	}
</style>

<script type="text/javascript">$('#loader').fadeIn();</script>
<div class="row wrapper border-bottom white-bg page-heading">
  <div class="col-lg-8">
	<h2 class="box-title"><?= lang('entries_cntrler_add_h3_title_1'); ?>
		<select id="entrytype_label">
			<?php
			foreach($this->DB1->where('entrytypes'.$this->DB1->dbsuffix.'.origin', 1)->get('entrytypes'.$this->DB1->dbsuffix)->result_array() as $etype): ?>
				<option value="<?= $etype['label'] ?>" data-etid="<?= $etype['id'] ?>" <?= ($entrytype['label'] == $etype['label']) ? "selected='selected'" : "" ?>><?= ucfirst(mb_strtolower($etype['name'])); ?></option>
			<?php endforeach; ?>
		</select>
		<?= lang('entries_cntrler_add_h3_title_2'); ?>
	</h2>
    <ol class="breadcrumb">
      <li>
        <a href="login/index">Inicio</a> <?php //En el controlador en la función llamada, se revisa si el usuario está logueado y lo envía a la vista principal según permisos ?>
      </li>
      <li class="active">
        <strong><?php echo $page_title; ?></strong>
      </li>
    </ol>
  </div><!-- /.col -->
  <div class="col-lg-4">
    <div class="title-action">

    </div>
  </div>
</div><!-- /.row -->

<!-- Main content -->
    <section class="row wrapper wrapper-content animated fadeInRight">
      <!-- Small boxes (Stat box) -->
      <div class="ibox float-e-margins">
        <!-- /.col -->
        <div class="ibox-content contentBackground">
          <div class="col-sm-12">
            <div class="box-header with-border">
            </div>
            <!-- /.box-header -->
            <div class="box-body">
				<div class="entry add form">
				<?php
					if ($this->mSettings->drcr_toby == 'toby') {
						$dc_options = array(
							'D' => lang('entries_views_addrow_label_dc_toby_D'),
							'C' => lang('entries_views_addrow_label_dc_toby_C'),
						);
					} else {
						$dc_options = array(
							'D' => lang('entries_views_addrow_label_dc_drcr_D'),
							'C' => lang('entries_views_addrow_label_dc_drcr_C'),
						);
					}
					echo form_open('', array('id' => 'add_form'));
					$prefixNumber = '';
					$suffixNumber = '';
					if (($entrytype['prefix'] != '') && ($entrytype['suffix'] != '')) {
						$prefixNumber = '<div class=\'input-group\'><span class=\'input-group-addon\' id=\'basic-addon1\'>' .
							$entrytype['prefix'] .
							'</span>';
						$suffixNumber = '<span class=\'input-group-addon\' id=\'basic-addon2\'>' .
							$entrytype['suffix'] .
							'</span></div>';
					} else if ($entrytype['prefix'] != '') {
						$prefixNumber = '<div class=\'input-group\'><span class=\'input-group-addon\'>' .
							$entrytype['prefix'] .
							'</span>';
						$suffixNumber = '</div>';
					} else if ($entrytype['suffix'] != '') {
							$prefixNumber = '<div class=\'input-group\'>';
							$suffixNumber = '<span class=\'input-group-addon\'>' .
								$entrytype['suffix'] .
								'</span></div>';
					}
					echo '<div class="row">';
					echo '<div class="col-sm-3">';
					echo '<div class="form-group">';
					echo form_label(lang('entries_views_add_label_number'), 'number');
					echo $prefixNumber;
					$data = array(
						'id' => "number",
						'type' => "text",
						'name' => "number",
						'class' => "form-control",
					);
					if ($isNumbering == "1") {
						$data['readonly'] = "true";
						$data['value'] = $consecutivoEntry;
					} else if ($isNumbering == "2") {
						$data['required'] = "true";
						$data['value'] = $consecutivoEntry;
					} ?>
					<input type="hidden" name="isNumbering" id="isNumbering" value="<?= $isNumbering ?>">
					<?php
					echo form_input($data);
					echo $suffixNumber;
					echo "</div>";
					echo "</div>";
					echo '<div class="col-sm-3">';
					echo '<div class="form-group">';
					echo form_label(lang('entries_views_add_label_date'), 'date')."<span class='input_required'> *</span>";
					$data = array(
						'id' => "EntryDate",
						'type' => "text",
						'name' => "date",
						'class' => "form-control",
						'value' => date('d-M-Y'),
						'tabindex' => '1',
					);
					echo form_input($data);
					echo "</div>";
					echo "</div>";
					echo '<div class="col-sm-3">';
					echo '<div class="form-group">';
					echo form_label(lang('entries_views_add_label_tag'), 'tag_id')."<span class='input_required'> *</span>";
					?>
						<select name="tag_id" class="form-control" tabindex="2">
							<option value="0"><?= lang('entries_views_add_tag_first_option'); ?></option>
							<?php foreach ($tag_options as $tag): ?>
								<option value="<?= $tag['id']; ?>"><?= $tag['title']; ?></option>
							<?php endforeach; ?>
						</select>
					<?php
					echo "</div>";
					echo "</div>";

					echo '<div class="col-sm-3">';
					echo '<div class="form-group">';
					echo form_label(lang('entries_views_add_label_company'), 'companie_id')."<span class='input_required'> *</span>";
					?>
						<select name="company_id" id="company_id" class="companies_select form-control ledger-dropdown2" tabindex="3" required>
							<?php if (isset($third_duplicate)): ?>
								<option value="<?= $third_duplicate ?>"><?= $third_name_duplicate ?></option>
							<?php else: ?>
								<option value=""><?= lang('entries_views_add_company_first_option'); ?></option>
							<?php endif ?>
						</select>
					<?php
					echo "</div>";
					echo "</div>";
					if ($this->mAccountSettings->cost_center) {
						echo "<hr class='col-sm-12'>";
						echo '<div class="col-sm-3">';
						echo '<div class="form-group">';
						echo form_label(lang('entries_views_add_items_th_cost_center'), 'companie_id')."<span class='input_required'> *</span>";
						?>
							<select name="cost_center_id" id="cost_center_id" class="form-control " tabindex="4" required>
								<?php if ($cost_centers): ?>
									<option value=""><?= lang('select') ?></option>
									<?php foreach ($cost_centers as $cost_center): ?>
										<option value="<?= $cost_center['id'] ?>" <?= isset($cost_center_duplicate) && $cost_center_duplicate == $cost_center['id'] ? 'selected="selected"' : '' ?>><?= $cost_center['name'] ?></option>
									<?php endforeach ?>
								<?php else: ?>
									<option value=""><?= lang('cost_centers_not_founded') ?></option>
								<?php endif ?>
							</select>
						<?php
						echo "</div>";
						echo "</div>";
					}
					echo "</div>";
					echo '<table class="stripped extra col-sm-12">';
					echo '<thead>';
					/* Header */
					echo '<tr>';
					if ($this->mSettings->drcr_toby == 'toby') {
						echo '<th style="width:3.5%;">' . (lang('entries_views_add_items_th_toby'))."<span class='input_required'> *</span>" . '</th>';
					} else {
						echo '<th style="width:3.5%;">' . (lang('entries_views_add_items_th_drcr'))."<span class='input_required'> *</span>" . '</th>';
					}
					echo '<th style="width:25.5%;">' . (lang('entries_views_add_items_th_ledger'))."<span class='input_required'> *</span>" . '</th>';
					echo '<th style="width:9.5%;">' . (lang('entries_views_add_items_th_dr_amount')) . ' (' . $this->mAccountSettings->currency_symbol . ')'."<span class='input_required'> *</span>" . '</th>';
					echo '<th style="width:9.5%;">' . (lang('entries_views_add_items_th_cr_amount')) . ' (' . $this->mAccountSettings->currency_symbol . ')'."<span class='input_required'> *</span>" . '</th>';
					echo '<th style="width:15.5%;">' . (lang('entries_views_add_items_th_narration'))."<span class='input_required'> *</span>" . '</th>';
					echo '<th style="width:9.5%;">' . (lang('entries_views_add_items_th_base')) . ' (' . $this->mAccountSettings->currency_symbol . ')'."<span class='input_required'> *</span>" . '</th>';
					echo '<th style="width:15.5%;">' . (lang('entries_views_add_label_company'))."<span class='input_required'> *</span>" . '</th>';
					if ($this->mAccountSettings->cost_center) {
						echo '<th style="width:10%;">' . (lang('entries_views_add_items_th_cost_center'))."<span class='input_required'> *</span>" . '</th>';
					}
					echo '<th style="width:3.5%;">' . (lang('entries_views_add_items_th_actions'))."<span class='input_required'> *</span>" . '</th>';
					echo '</tr>';
					echo '</thead>';
					$tabindex = 4;

					echo '<tbody>';
					/* Intial rows */
					$count = 0;
					// exit(var_dump($curEntryitems));
					foreach ($curEntryitems as $row => $entryitem) {
						echo '<tr>';
						$tabindex++;
						if (empty($entryitem['dc'])) {
							echo '<td><div class="form-group-entryitem">' . form_dropdown('Entryitem[' . $row . '][dc]', $dc_options, "", array('class' => 'dc-dropdown form-control', 'tabindex' => $tabindex)) . '</div></td>';
						} else {
							echo '<td><div class="form-group-entryitem">' . form_dropdown('Entryitem[' . $row . '][dc]', $dc_options, $entryitem['dc'], array('class' => 'dc-dropdown form-control', 'tabindex' => $tabindex)) . '</div></td>';
						}
						$tabindex++;
						if (empty($entryitem['ledger_id'])) {
						?>
							<td>
								<div class="form-group-entryitem">
									<select class="ledger-dropdown ledger_select form-control" name="<?= 'Entryitem[' . $row . '][ledger_id]'; ?>" tabindex='<?= $tabindex ?>' required>
											<option value=""><?= lang('entries_views_add_company_first_option'); ?></option>
									</select>
								</div>
							</td>
							<?php
						} else {
							?>
							<td>
								<div class="form-group-entryitem">
									<select class="ledger-dropdown ledger_select form-control" name="<?= 'Entryitem[' . $row . '][ledger_id]'; ?>" tabindex='<?= $tabindex ?>' required>
											<option value="<?= $entryitem['ledger_id'] ?>"><?= $entryitem['ledgername'] ?></option>
									</select>
								</div>
							</td>
							<?php
						}
						$tabindex++;
						if (empty($entryitem['dr_amount'])) {
							$data = array(
								'type' => "text",
								'name' => 'Entryitem[' . $row . '][dr_amount]',
								'class' =>  'dr-item form-control',
								'min' => 0,
								'required' => true,
								'tabindex' => $tabindex,
							);
							echo "<td><div class='form-group-entryitem'>";
							echo form_input($data);
							echo "</div></td>";
						} else {
							$data = array(
								'value' => $entryitem['dr_amount'],
								'type' => "text",
								'name' => 'Entryitem[' . $row . '][dr_amount]',
								'class' =>  'dr-item form-control',
								'min' => 0,
								'required' => true,
								'tabindex' => $tabindex,
							);
							echo "<td><div class='form-group-entryitem'>";
							echo form_input($data);
							echo "</div></td>";
						}
						$tabindex++;
						if (empty($entryitem['cr_amount'])) {
							$data = array(
								'type' => "text",
								'name' => 'Entryitem[' . $row . '][cr_amount]',
								'class' =>  'cr-item form-control',
								'min' => 0,
								'required' => true,
								'tabindex' => $tabindex,
							);
							echo "<td><div class='form-group-entryitem'>";
							echo form_input($data);
							echo "</div></td>";

						} else {
							$data = array(
								'value' => $entryitem['cr_amount'],
								'type' => "text",
								'name' => 'Entryitem[' . $row . '][cr_amount]',
								'class' =>  'cr-item form-control',
								'min' => 0,
								'required' => true,
								'tabindex' => $tabindex,
							);
							echo "<td><div class='form-group-entryitem'>";
							echo form_input($data);
							echo "</div></td>";
						}
						$tabindex++;
						$data = array(
							'type'  => "text",
							'name'  => 'Entryitem[' . $row . '][narration]',
							'class' => 'form-control narration',
							'value' => ( (isset($entryitem['narration'])) ? $entryitem['narration'] : ""),
							'tabindex' => $tabindex,
							'required' => 'required',

						);
						echo "<td><div class='form-group-entryitem'>";
						echo form_input($data);
						echo "</div></td>";
						/* BASE */
						echo '<td>';
						$tabindex++;
						?>
							<input type="number" class="form-control" name="<?= 'Entryitem['.$row.'][base]' ?>" value="<?= (isset($entryitem['base']) ? $entryitem['base'] : 0) ?>" tabindex='<?= $tabindex ?>' min='0'>
						<?php echo '</td>';
						/* BASE */
						/* TERCERO */
						echo "<td>";
						$tabindex++;
						?>
						<?php if (isset($entryitem['companies_id'])): ?>
							<select name="<?= 'Entryitem['.$row.'][companies_id]' ?>" class="companies_select form-control third ledger-dropdown2" tabindex='<?= $tabindex ?>' required>
								<option value="<?= $entryitem['companies_id'] ?>"><?= $entryitem['company_name'] ?></option>
							</select>
						<?php else: ?>
							<select name="<?= 'Entryitem['.$row.'][companies_id]' ?>" class="companies_select form-control third ledger-dropdown2" tabindex='<?= $tabindex ?>' required>
								<option value=""><?= lang('entries_views_add_company_first_option'); ?></option>
							</select>
						<?php endif ?>
						<?php
						echo "</td>";
						/* TERCERO */
						if ($this->mAccountSettings->cost_center) {
							/* CENTRO DE COSTO */
							echo "<td>";
							$tabindex++;
							?>

							<select name="<?= 'Entryitem['.$row.'][cost_center_id]' ?>" class="cost_center_select form-control" tabindex='<?= $tabindex ?>' required>
								<?php if ($cost_centers): ?>
									<option value=""><?= lang('select') ?></option>
									<?php foreach ($cost_centers as $cost_center): ?>
										<option value="<?= $cost_center['id'] ?>" <?= isset($entryitem['cost_center_id']) && $entryitem['cost_center_id'] == $cost_center['id'] ? 'selected="selected"' : '' ?>><?= $cost_center['name'] ?></option>
									<?php endforeach ?>
								<?php else: ?>
									<option value=""><?= lang('cost_centers_not_founded') ?></option>
								<?php endif ?>
							</select>
							<?php
							echo "</td>";
							/* CENTRO DE COSTO */
						}
						echo '<td>';
						// if ($count > 1) {
							echo '<button class="deleterow btn btn-danger btn-sm" type="button"><span class="glyphicon glyphicon-trash" escape="false"></span></button>';
						// }
						echo '</td>';
						echo '</tr>';
						$count++;
					}
					echo '</tbody>';
					echo '<tfoot>';
					/* Total and difference */
					echo '<tr class="bold-text">' . '<td>' . lang('entries_views_add_items_td_total') . '</td>' . '<td>' . '</td>' . '<td id="dr-total">' . '</td>' . '<td id="cr-total">' . '</td>' . '<td >' . '<button class="recalculate btn btn-primary btn-sm" escape="false" type="button"><i class="glyphicon glyphicon-refresh"></i></button>' . '</td>' . '<td>' . '</td>' . '<td>' .
					'<button class="btn btn-primary btn-sm addrow" '.( (isset($third_duplicate)) ? "" : "disabled").' escape="false" title="Escoja tercero para añadir nueva fila"  type="button"><i class="glyphicon glyphicon-plus"></i></button>' . '</td>' . '</tr>';
					echo '<tr class="bold-text">' . '<td>' . lang('entries_views_add_items_td_diff') . '</td>' . '<td>' . '</td>' . '<td id="dr-diff">' . '</td>' . '<td id="cr-diff">' . '</td>' . '<td>' . '</td>' . '<td>' . '</td>' . '<td>' . '</td>' . '</tr>';
					echo '</tfoot>';
					echo '</table>';

					echo '<br />';
					echo '<div class="form-group">';
					$tabindex++;
					echo form_label(lang('entries_views_add_label_note'), 'note')."<span class='input_required'> *</span>";
					echo "<textarea name='notes' class='form-control' rows='3' tabindex='".$tabindex."'>".((isset($notes_duplicate)) ? $notes_duplicate : "")."</textarea>";
					echo "</div>";
					$tabindex++;
					?>
						<input type="checkbox" name="entry_approved" tabindex='<?= $tabindex ?>' <?= (isset($state_duplicate) && $state_duplicate == 1) ? "checked" : "" ?>> <?= lang('entries_views_add_label_note_approved'); ?>
						<br>
					<?php
					echo '<br />';
					echo '<div class="form-group">';
					echo '<button type="button" class="btn btn-success" id="btn_submit">'.lang('entries_views_add_label_submit_btn').'</button>';
					echo '<span class="link-pad"></span>';
					echo anchor('entries/index', lang('entries_views_add_label_cancel_btn'), array('class' => 'btn btn-default'));
					echo '<a></span>';
					echo '</div>';
					echo form_close();
				?>
				</div>
            </div>
          </div>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
    <script type="text/javascript">
    	function arreglar_tamaño_divs(){
		    var heights = $(".form-group-entryitem").map(function() {
		          return $(this).height();
		      }).get(),
		      maxHeight = Math.max.apply(null, heights);
		      $(".form-group-entryitem").height(maxHeight);
		}
		$(document).on('click', '#btn_submit', function(){
			if ($('select.dc-dropdown').length > 0) {
				if ($('#add_form').valid()) {
					$(this).prop('disabled', false);
					setTimeout(function() {
						$('#add_form').submit();
					}, 1100);
				}
			} else {
				Command: toastr.error('La nota no puede registrarse sin detalle.', 'Error', {onHidden : function(){}})
			}
		});
    </script>