<script type="text/javascript">$('#loader').fadeIn();</script>

<div class="row wrapper border-bottom white-bg page-heading">
  <div class="col-lg-8">
    <h2><?php echo $page_title; ?></h2>
    <ol class="breadcrumb">
      <li>
        <a href="login/index">Inicio</a> <?php //En el controlador en la función llamada, se revisa si el usuario está logueado y lo envía a la vista principal según permisos ?>
      </li>
      <li class="active">
        <strong><?php echo $page_title; ?></strong>
      </li>
    </ol>
  </div><!-- /.col -->
  <div class="col-lg-4">
    <div class="title-action">
       <a href="<?= base_url() ?>thirds/add" class="btn btn-success"><span class="fa fa-plus"></span> Nuevo</a>
       <a href="<?= base_url() ?>thirds/import" class="btn btn-success"><span class="fa fa-upload"></span> Importar terceros</a>
    </div>
  </div>
</div>
<div class="row wrapper wrapper-content animated fadeInRight">
  <div class="ibox float-e-margins">
    <div class="ibox-content contentBackground">
      <div class="col-xs-12">
        <div class="row">
          <div class="col-sm-8">
            <h3><?= $page_title; ?></h3>
          </div>
          <div class="col-sm-4">
            
          </div>
          <div class="col-sm-12">
            <table class="table" id="thirdstable">
              <thead>
                <tr>
                  <th>Nombre</th>
                  <th>N° documento</th>
                  <th>Ciudad</th>
                  <th>E-mail</th>
                  <th>Tipo tercero</th>
                  <th>Acciones</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($thirds as $row => $third): ?>
                  <tr>
                    <td><?= ucwords(mb_strtolower($third['name'])) ?></td>
                    <td><?= $third['vat_no'] ?></td>
                    <td><?= $third['city'] ?></td>
                    <td><?= $third['email'] ?></td>
                    <td><?= isset($groups[$third['group_id']]) && lang($groups[$third['group_id']]) != "" ? lang($groups[$third['group_id']]) : "No registra"; ?></td>
                    <td>
                      <div class="btn-group">
                        <div class="dropdown">
                          <button class="btn btn-success btn-sm" type="button" id="accionesGrupo" data-toggle="dropdown" aria-haspopup="true">
                            Acciones
                            <span class="caret"></span>
                          </button>
                          <ul class="dropdown-menu pull-right" aria-labelledby="accionesGrupo">
                           <li>
                              <a href="<?= base_url() ?>thirds/edit/<?= $third['id'] ?>"><span class="fa fa-pencil"></span> Editar</a>
                           </li>
                           <li>
                            <a href="#delete_third" data-toggle="modal" data-thirdid='<?= $third['id'] ?>'><span class="fa fa-trash"></span> Eliminar</a>
                           </li>
                          </ul>
                        </div>
                      </div>
                    </td>
                  </tr>
                <?php endforeach ?>
              </tbody>
              <tfoot>
                <tr>
                  <th>Nombre</th>
                  <th>N° documento</th>
                  <th>Ciudad</th>
                  <th>E-mail</th>
                  <th>Tipo tercero</th>
                  <th>Acciones</th>
                </tr>
              </tfoot>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="delete_third" aria-labelledby="deletethird">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><?= lang('thirds_index_delete_title'); ?></h4>
      </div>
        <div class="modal-body">
          <input type="hidden" name="third_id_delete" id="third_id_delete">
          <?= lang('thirds_index_delete_text'); ?>
        </div>
        <div class="modal-footer">
          <button class="btn btn-default" data-dismiss="modal"><?=lang('no');?></button>
          <button class="btn btn-danger confirm_delete_third" ><?=lang('yes');?></button>
        </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<script type="text/javascript">

  dataset1 = $('#thirdstable').DataTable({
    /*order: [ 0, 'asc' ],*/
    pageLength: 25,
    responsive: true,
    "order" : [],
     "ordering": false,
    dom : '<"html5buttons" B>lr<"containerBtn"><"inputFiltro"f>tip',
    buttons : [{extend:'excel', title:'Account List', className:'btnExportarExcel', exportOptions: {columns : [0,1,2,3,4]}}],
    oLanguage: {
      sLengthMenu: 'Mostrando _MENU_ registros por página',
      sZeroRecords: 'No se encontraron registros',
      sInfo: 'Mostrando _START_ a _END_ de _TOTAL_ registros',
      sInfoEmpty: 'Mostrando 0 a 0 de 0 registros',
      sInfoFiltered: '(Filtrado desde _MAX_ registros)',
      sSearch:         'Buscar: ',
      oPaginate:{
        sFirst:    'Primero',
        sLast:     'Último',
        sNext:     'Siguiente',
        sPrevious: 'Anterior'
      }
    }
    });
  var btnAcciones = '<div class="dropdown pull-right" id="">'+
  '<button class="btn btn-primary btn-sm btn-outline" type="button" id="accionesTabla" data-toggle="dropdown" aria-haspopup="true">Acciones<span class="caret"></span></button>'+
  '<ul class="dropdown-menu pull-right" aria-labelledby="accionesTabla">'+
  '<li><a onclick="$(\'.btnExportarExcel\').click()"><span class="glyphicon glyphicon-export"></span> <?= lang('export_xls') ?> </a></li>'+
  '</ul>'+
  '</div>';

  $('.containerBtn').html(btnAcciones);


$(document).ready(function(){
   $('#loader').fadeOut();
});

$('#delete_third').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget);
      thirdid = button.data('thirdid');
      $('#third_id_delete').val(thirdid);
});

$('.confirm_delete_third').on('click', function(){
  thirdid = $('#third_id_delete').val();
  window.location.href = "<?= base_url(); ?>thirds/delete/"+thirdid;
});


</script>