<?php $group_names_spanish = array('biller' => 'Facturador', 'customer' => 'Cliente', 'Partner' => 'Cliente', 'supplier' => 'Proveedor'); ?>

<script type="text/javascript">$('#loader').fadeIn();</script>

<style type="text/css">
  .error_msg{
    padding: 0% 1% 1%;
    color: #F44336;
  }

  .op_balance{
    text-align: right;
  }

  .protector {
    height: 100%;
    position: absolute;
    z-index: 999;
    background-color: #00000008;
  }

</style>


<?php 

$num_col = 3;

 ?>

<div class="row wrapper border-bottom white-bg page-heading">
  <div class="col-lg-8">
    <h2><?php echo $page_title; ?></h2>
    <ol class="breadcrumb">
      <li>
        <a href="login/index">Inicio</a> <?php //En el controlador en la función llamada, se revisa si el usuario está logueado y lo envía a la vista principal según permisos ?>
      </li>
      <li class="active">
        <strong><?php echo $page_title; ?></strong>
      </li>
    </ol>
  </div><!-- /.col -->
  <div class="col-lg-4">
    <div class="title-action">
       <!-- <button class="btn btn-success submit_third"><span class="fa fa-check"></span> Enviar</button> -->
    </div>
  </div>
</div>
<div class="relation-example" style="display: none;">
  <div class="relation">
    <div class="col-md-3 form-group">
      <select class="ledger_select relation-ledger-dropdown ledger_id ledger_{pos}" data-pos="{pos}" name="ledger_id[]" required>
          <option value="">Seleccione...</option>
      </select>
    </div>
    <div class="col-md-3 form-group">
      <select class="companies_select relation-ledger-dropdown third_id third_{pos}" data-pos="{pos}" name="third[]" required>
          <option value="">Seleccione...</option>
      </select>
    </div>
    <?php if ($this->mAccountSettings->cost_center): ?>
    <?php 
    $num_col = 2;
     ?>
     <div class="col-md-2 form-group">
        <select class="cost_center_select form-control ledger-dropdown cost_center_id cost_center_{pos}" data-pos="{pos}" name="cost_center_id[]" required>
            <option value="">Seleccione...</option>
            <?php foreach ($cost_centers as $cost_center): ?>
              <option value="<?= $cost_center['id'] ?>"><?= $cost_center['name'] ?></option>
            <?php endforeach ?>
        </select>
     </div>
    <?php endif ?>
    <div class="col-md-<?= $num_col ?> form-group">
      <input type="number" name="op_balance[]" class="form-control op_balance" step="00.01" min="0" required>
    </div>
    <div class="col-md-<?= $num_col ?> form-group">
      <select class="form-control" name="op_balance_dc[]" required>
          <option value="">Seleccione...</option>
          <option value="C">C</option>
          <option value="D">D</option>
      </select>
    </div>
    <div class="col-md-12 error_msg error_{pos}" style="display: none;">
      <em><i class="fa fa-times"></i> <?= lang('thirds_relations_repeated_validation') ?></em>
    </div>
  </div>
</div>

<div class="row wrapper wrapper-content animated fadeInRight">
  <div class="ibox float-e-margins">
    <div class="ibox-content contentBackground">
      <div class="col-md-12">
        <div class="row">
          <?php if ($thirds_relationed == 1): ?>
            <div class="col-sm-12 protector">

            </div>
          <?php endif ?>
          <div class="col-md-3">
            <label><?= lang('ledger_acc_name'); ?></label>
          </div>
          <div class="col-md-3">
            <label><?= lang('thirds_relations_third_label'); ?></label>
          </div>
          <?php if ($this->mAccountSettings->cost_center == 1): ?>
            <div class="col-md-2">
              <label><?= lang('thirds_relations_cost_center'); ?></label>
            </div>
          <?php endif ?>
          <div class="col-md-<?= $num_col ?>">
            <label><?= lang('thirds_relations_op_balance_label'); ?></label>
          </div>
          <div class="col-md-<?= $num_col ?>">
            <label><?= lang('thirds_relations_op_dc_balance_label'); ?></label>
          </div>
          <form class="form" id="new_third" method="POST">
            <div class="col-sm-12 row relations">
              <input type="hidden" name="page" id="page" value="1">
              <?php if (isset($relations_exists)): ?>
              <?php else: ?>
                <div class="relation">
                  <div class="col-md-3 form-group">
                    <select class="form-control ledger-dropdown ledger_id ledger_1" data-pos="1" name="ledger_id[]" required>
                      <?php foreach ($ledgers as $id => $ledger): ?>
                        <option value="<?= $id; ?>" <?= ($id < 0) ? 'disabled' : "" ?> <?= (($this->input->post('ledger_id') == $id) or ($this->uri->segment(4) == $id)) ?'selected':''?>><?= $ledger; ?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                  <div class="col-md-3 form-group">
                    <select class="form-control ledger-dropdown third_id third_1" data-pos="1" name="third[]" required>
                      <option value="">Seleccione...</option>
                      <?php foreach ($thirds as $row => $third): ?>
                        <option value="<?= $third['id']; ?>"><?= $third['name']; ?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                  <?php if ($this->mAccountSettings->cost_center): ?>
                  <?php 
                  $num_col = 2;
                   ?>
                   <div class="col-md-2 form-group">
                      <select class="cost_center_select form-control ledger-dropdown cost_center_id cost_center_1" data-pos="1" name="cost_center_id[]" required>
                          <option value="">Seleccione...</option>
                      </select>
                   </div>
                  <?php endif ?>
                  <div class="col-md-<?= $num_col ?> form-group">
                    <input type="number" name="op_balance[]" class="form-control op_balance" step="00.01" min="0" required>
                  </div>
                  <div class="col-md-<?= $num_col ?> form-group">
                    <select class="form-control" name="op_balance_dc[]" required>
                        <option value="">Seleccione...</option>
                        <option value="C">C</option>
                        <option value="D">D</option>
                    </select>
                  </div>
                  <div class="col-md-12 error_msg error_1" style="display: none;">
                    <em><i class="fa fa-times"></i> <?= lang('thirds_relations_repeated_validation') ?></em>
                  </div>
                </div>
              <?php endif ?>
            </div>
            <?php if ($thirds_relationed == 0): ?>
              <div class="col-sm-3" style="padding-bottom: 2%;">
                <button class="btn btn-success btn-sm btn-outline add_relation calculate_totals block_button" type="button"><span class="fa fa-plus"></span></button>
                <button class="btn btn-danger btn-sm btn-outline delete_relation block_button" type="button"><span class="fa fa-minus"></span></button>
                <button class="btn btn-primary btn-sm btn-outline calculate_totals ctls block_button" data-toggle="tooltip" data-placement="bottom" title="Calcular totales" type="button"><span class="fa fa-calculator"></span></button>
              </div>
              <div class="col-sm-9 pagination_buttons">
                <nav class="pagination_area" aria-label="Page navigation">
                  <ul class="pagination bootpag">

                  </ul>
                </nav>
              </div>
              <div class="col-sm-12" style="padding-top: 2%;">
                <!-- <button class="btn btn-success submit_third" disabled="true"><span class="fa fa-check"></span> Enviar</button> -->
                <input type="submit" name="hanging_submit" value="Guardar temporalmente" class="btn btn-success">
                <input style="display: none;" type="submit" name="normal_submit" value="Guardar definitivamente" id="normal_submit" class="block_button">
                <button data-toggle="modal" href="#modal_normal_submit" type="button" class="btn btn-primary submit_third block_button" disabled="true"> Guardar definitivamente</button>
              </div>
            <?php elseif ($thirds_relationed == 1): ?>
                <button class="btn btn-primary btn-sm btn-outline calculate_totals ctls block_button" data-toggle="tooltip" data-placement="bottom" title="Calcular totales" type="button" style="display: none;"><span class="fa fa-calculator"></span></button>
            <?php endif ?>

          </form>
        </div>
      </div>
      <div class="col-md-8">
        <table class="table">
          <thead>
            <tr>
              <th>Cuenta</th>
              <th>Total calculado</th>
            </tr>
          </thead>
          <tbody id="third_account_table">

          </tbody>
          <tfoot>
            <tr>
              <th>Cuenta</th>
              <th>Total calculado</th>
            </tr>
          </tfoot>
        </table>
      </div>
      <div class="col-md-4">
        <table class="table">
          <thead>
            <tr>
              <th><?= lang('thirds_relations_unbalance'); ?><button class="btn btn-primary btn-xs calculate_totals pull-right block_button" data-toggle="tooltip" data-placement="bottom" title="Calcular totales" type="button"><span class="fa fa-calculator"></span></button></th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td id="descuadre_amount">0</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" tabindex="-1" role="dialog" id="modal_normal_submit" aria-labelledby="deleteaccount">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><?= lang('thirds_index_op_balance_title'); ?></h4>
      </div>
        <div class="modal-body">
          <?= lang('thirds_index_op_balance_text'); ?>
        </div>
        <div class="modal-footer">
          <button class="btn btn-default" data-dismiss="modal"><?=lang('no');?></button>
          <button class="btn btn-primary confirm_normal_submit" ><?=lang('yes');?></button>
        </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div class="modal fade" tabindex="-1" role="dialog" id="modal_delete_relation_saved">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">¿Está seguro de eliminar la relación seleccionada?</h4>
      </div>
      <div class="modal-body">
        <p>Si está seguro de eliminar la relación que seleccionó, de clic en Eliminar</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-danger" id="delete_relation_saved">Eliminar</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">

$(document).ready(function(){
    $(".ledger-dropdown").select2({
        width:'100%'
    });
   // $('#loader').fadeOut();
  $('[data-toggle="tooltip"]').tooltip();

   arreglar_tamaño_divs();

  toastr.options = {
    "closeButton": true,
    "debug": false,
    "progressBar": true,
    "preventDuplicates": false,
    "positionClass": "toast-bottom-right",
    "onclick": null,
    "showDuration": "400",
    "hideDuration": "1000",
    "timeOut": "7000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
  }

  jQuery.extend(jQuery.validator.messages, {//Configuración jquery valid
    required: "Este campo es obligatorio.",
    remote: "Por favor, rellena este campo.",
    email: "Por favor, escribe una dirección de correo válida",
    url: "Por favor, escribe una URL válida.",
    date: "Por favor, escribe una fecha válida.",
    dateISO: "Por favor, escribe una fecha (ISO) válida.",
    number: "Por favor, escribe un número entero válido.",
    digits: "Por favor, escribe sólo dígitos.",
    creditcard: "Por favor, escribe un número de tarjeta válido.",
    equalTo: "Por favor, escribe el mismo valor de nuevo.",
    accept: "Por favor, escribe un valor con una extensión aceptada.",
    maxlength: jQuery.validator.format("Por favor, no escribas más de {0} caracteres."),
    minlength: jQuery.validator.format("Por favor, no escribas menos de {0} caracteres."),
    rangelength: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1} caracteres."),
    range: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1}."),
    max: jQuery.validator.format("Por favor, escribe un valor menor o igual a {0}."),
    min: jQuery.validator.format("Por favor, escribe un valor mayor o igual a {0}.")
  });


    $.ajax({
      url:"<?= base_url('thirds/get_relations/') ?>"+$('#page').val()
    }).done(function(data){
      if (data != '') {
        $('.relations').html(data);
        setCompaniesSelect();
        setLedgersSelect();
        $('.cost_center_select').select2({width : '100%'});
        $('#loader').fadeOut();
        set_pagination_plug();
      }
        unblock_buttons($('#unblock_buttons').val());
    }).fail(function(data){

    });

});

$(document).on('click', '.add_relation', function(){
  $('#loader').fadeIn();
  row = $('.relation-example').html();
  row = row.replace(/{pos}/g, $('.relation').length);
  $('.relations').append(row);
  $(".relations .select2-container:last-child").remove();
  $('.submit_third').attr('disabled', true);
  $('#loader').fadeOut();
  setCompaniesSelect();
  setLedgersSelect();
  $('.cost_center_select').select2({width : '100%'});
  arreglar_tamaño_divs();
});

$(document).on('keyup', '.op_balance', function(){
  $('.submit_third').attr('disabled', true);
});

$(document).on('click', '.delete_relation', function(){
  lastRow = $('.relations .relation:last-child:not(:first-child)');
  if (!lastRow.hasClass('saved')) {
    lastRow.remove();
  } else {
    Command: toastr.error('La fila que intenta eliminar no se puede borrar por este método.', 'Error', {onHidden : function(){
    }});
  }
});

$('#modal_delete_relation_saved').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget);
  var relation_id = button.data('relationid');
  $('#delete_relation_saved').data('idrelation', relation_id);
})

$(document).on('click', '#delete_relation_saved', function(){
  $('#loader').fadeIn();
  $('#modal_delete_relation_saved').modal('hide');
  relation_id = $(this).data('idrelation');
  $.ajax({
    url:"<?= base_url() ?>thirds/delete_relation/"+relation_id,
  }).done(function(data){
    if (data) {
      $('.id_relation_'+relation_id).remove();
      $('#loader').fadeOut();
      Command: toastr.success('Se eliminó con éxito la relación seleccionada.', 'Eliminado', {onHidden : function(){
      }})
      $('.calculate_totals').trigger('click');
    } else {
      console.log(data);
      Command: toastr.error('Ocurrió un error', 'Error', {onHidden : function(){
        $('#loader').fadeOut();
      }})
    }
  }).fail(function(data){
    console.log(data);
      Command: toastr.error('Ocurrió un error', 'Error', {onHidden : function(){
        $('#loader').fadeOut();
      }})
  });
});

$(document).on('change', '.ledger_id', function(){
  select = $(this);
  pos = select.data('pos');
  ledger = select.val();
  third = $('.third_'+pos).val();
  thirds = $('.third_id');
  cost_center = $('.cost_center_'+pos).val();
  cost_centers = $('.cost_center_id');
  valir = 0;
  $.each($('.ledger_id'), function(index, value){
    if (
        value.value == ledger && 
        thirds[index].value == third && 
        ($('select.cost_center_select').length > 0 && cost_centers[index].value == cost_center) && 
        third != '') {
      valir++;
    }
  });
  if (valir > 1) {
    select.select2('val', 0);
    $('.error_'+pos).css('display', '');
    Command: toastr.error('Se escogió el tercero dos veces para una misma cuenta.', 'Error', {onHidden : function(){
    }})
  } else {
    $('.error_'+pos).css('display', 'none');
  }
});

$(document).on('change', '.third_id', function(){
  select = $(this);
  pos = select.data('pos');
  third = select.val();
  ledger = $('.ledger_'+pos).val();
  ledgers = $('.ledger_id');
  cost_center = $('.cost_center_'+pos).val();
  cost_centers = $('.cost_center_id');
  valir = 0;
  $.each($('.third_id'), function(index, value){
    if (
        value.value == third && 
        ledgers[index].value == ledger && 
        ($('select.cost_center_select').length > 0 && cost_centers[index].value == cost_center) && 
        ledger != 0
        ) {
      valir++;
    }
  });
  if (valir > 1) {
    select.select2('val', 0);
    $('.error_'+pos).css('display', '');
    Command: toastr.error('Se escogió el tercero dos veces para una misma cuenta.', 'Error', {onHidden : function(){
    }})
  } else {
    $('.error_'+pos).css('display', 'none');
  }
});


$(document).on('change', '.cost_center_id', function(){
  select = $(this);
  pos = select.data('pos');
  cost_center = select.val();
  third = $('.third_'+pos).val();
  thirds = $('.third_id');
  ledger = $('.ledger_'+pos).val();
  ledgers = $('.ledger_id');
  valir = 0;
  $.each($('.cost_center_id'), function(index, value){
    if (
        value.value == cost_center && 
        thirds[index].value == third && 
        ledgers[index].value == ledger && 
        third != '') {
      valir++;
    }
  });
  if (valir > 1) {
    select.select2('val', 0);
    $('.error_'+pos).css('display', '');
    Command: toastr.error('Se escogió el tercero dos veces para una misma cuenta.', 'Error', {onHidden : function(){
    }})
  } else {
    $('.error_'+pos).css('display', 'none');
  }
});

$(document).on('click', '.calculate_totals', function(){
  datos = $('#new_third').serialize();
  $.ajax({
    url:"<?= base_url() ?>thirds/relationstotal",
    type:"post",
    data : datos,
  }).done(function(data){
    data = JSON.parse(data);
    $('#third_account_table').html(data.row);
    if (data.descuadre === true) {
       Command: toastr.error('Los saldos de las cuentas no están cuadrados.', 'Descuadre de cuentas', {onHidden : function(){
        }})
       $('.submit_third').attr('disabled', true);
    } else if (data.descuadre  === false) {
       $('.submit_third').attr('disabled', false);
    }
    $('#descuadre_amount').html(data.descuadre_amount);
    if ($('.relations .relation:last input[name="op_balance[]"]').val() == "") {
      $('.relations .relation:last input[name="op_balance[]"]').val(data.montonum);
      $('.relations .relation:last select[name="op_balance_dc[]"]').val(data.montodc);
    }
    console.log(data);
  }).fail(function(data){
    console.log(data);
  });
});

$(document).on('click', '.confirm_normal_submit', function(){
  $('#normal_submit').trigger('click');
  $('#modal_normal_submit').modal('hide');
});

$(document).submit('#new_third', function(){
  $('#loader').fadeIn();
});


function arreglar_tamaño_divs(){
    var heights = $(".form-group").map(function() {
          return $(this).height();
      }).get(),
      maxHeight = Math.max.apply(null, heights);
      $(".form-group").height(maxHeight);
}


function setCompaniesSelect(){
  $('.companies_select').select2({
      minimumInputLength: 1,
      width:'100%',
      ajax: {
          url: "<?= base_url('entries/companiesoptions') ?>",
          dataType: 'json',
          quietMillis: 15,
          data: function (term, page) {
              return {
                  term: term,
                  limit: 10
              };
          },
          results: function (data, page) {
              if (data.results != null) {
                  return {results: data.results};
              } else {
                  return {results: [{id: '', text: 'No Match Found'}]};
              }
          }
      }
  });
}

function setLedgersSelect(){
  $('.ledger_select').select2({
      minimumInputLength: 1,
      width:'100%',
      ajax: {
          url: "<?= base_url('entries/ledgersoptions') ?>",
          dataType: 'json',
          quietMillis: 15,
          data: function (term, page) {
              return {
                  term: term,
                  limit: 10
              };
          },
          results: function (data, page) {
              if (data.results != null) {
                  return {results: data.results};
              } else {
                  return {results: [{id: '', text: 'No Match Found'}]};
              }
          }
      }
  });
}

function set_pagination_plug(){
  $.ajax({
    url:"<?= base_url('thirds/pages') ?>"
  }).done(function(data){
      var paginas = data;
      $('.pagination_area').bootpag({
         total: paginas,
         page: 1,
         maxVisible: 20,
         firstLastUse: true
      }).on('page', function(event, num){
              $('.relations').html('');
              $('#loader').fadeIn();
              $.ajax({
                url:"<?= base_url('thirds/get_relations/') ?>"+num
              }).done(function(data){
                $('.relations').html(data);
                setCompaniesSelect();
                setLedgersSelect();
                $('.cost_center_select').select2({width : '100%'});
                unblock_buttons($('#unblock_buttons').val());
                $('#loader').fadeOut();
                setTimeout(function() {$('.calculate_totals').trigger('click');}, 850);
              }).fail(function(data){
              });

          // datos = $('#new_third').serialize();
          // $.ajax({
          //   url:"<?= base_url() ?>thirds/relationstotal",
          //   type:"post",
          //   data : datos,
          // }).done(function(data){
          //   data = JSON.parse(data);
          //   if (data.descuadre === true) {
          //      Command: toastr.error('Los saldos de las cuentas no están cuadrados.', 'Descuadre de cuentas', {onHidden : function(){
          //       }})
          //      $('.submit_third').attr('disabled', true);
          //   } else if (data.descuadre  === false) {
            // }
            // $('#descuadre_amount').html(data.descuadre_amount);
            // if ($('.relations .relation:last input[name="op_balance[]"]').val() == "") {
            //   $('.relations .relation:last input[name="op_balance[]"]').val(data.montonum);
            //   $('.relations .relation:last select[name="op_balance_dc[]"]').val(data.montodc);
            // }
            // console.log(data);
          // }).fail(function(data){
          //   console.log(data);
          // });
      });
  }).fail(function(data){
  });
}

function unblock_buttons(bool){
  if (bool == 1) {
    $('.block_button').prop('disabled', false);
  } else {
    $('.block_button').prop('disabled', true);
  }
}

<?php if (isset($relations_exists)): ?>
  $('.ctls').trigger('click');
<?php endif ?>
</script>