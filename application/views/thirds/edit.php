<script type="text/javascript">$('#loader').fadeIn();</script>

<div class="row wrapper border-bottom white-bg page-heading">
  <div class="col-lg-8">
    <h2><?php echo $page_title; ?></h2>
    <ol class="breadcrumb">
      <li>
        <a href="login/index">Inicio</a> <?php //En el controlador en la función llamada, se revisa si el usuario está logueado y lo envía a la vista principal según permisos ?>
      </li>
      <li class="active">
        <strong><?php echo $page_title; ?></strong>
      </li>
    </ol>
  </div><!-- /.col -->
  <div class="col-lg-4">
    <div class="title-action">
       <button class="btn btn-success submit_third"><span class="fa fa-check"></span> Enviar</button>
    </div>
  </div>
</div>
<div class="row wrapper wrapper-content animated fadeInRight">
  <div class="ibox float-e-margins">
    <div class="ibox-content contentBackground">
      <div class="col-xs-12">
        <div class="row">
          <form class="form" id="edit_third" method="POST">
            <div class="form-group col-sm-4">
              <label><?= lang('thirds_cntrler_add_form_group_id_label') ?></label><span class='input_required'> *</span>
              <select class="form-control" name="group_id" id="group_id" required>
                <option value="">Seleccione...</option>
                <!-- <option value="3" <?= ($third['group_id'] == 3) ? "selected='selected'" : "" ?> data-name="customer"><?= lang('Customer') ?></option>
                <option value="8" <?= ($third['group_id'] == 8) ? "selected='selected'" : "" ?> data-name="employee"><?= lang('Employee') ?></option>
                <option value="4" <?= ($third['group_id'] == 4) ? "selected='selected'" : "" ?> data-name="supplier"><?= lang('Supplier') ?></option> -->

                  <?php foreach ($third_groups as $row => $tgs): ?>
                    <option value="<?= $tgs['id'] ?>" <?= ($third['group_id'] == $tgs['id']) ? "selected='selected'" : "" ?>  data-name="<?= $tgs['name'] ?>"><?= lang($tgs['description']) ?></option>
                  <?php endforeach ?>
                <input type="hidden" name="group_name" id="group_name" <?= (isset($third['group_name'])) ? "value='".$third['group_name']."'" : "" ?>>
              </select>
            </div>
            <div class="form-group col-sm-4">
              <label>Tipo de persona</label><span class='input_required'> *</span>
              <select name="type_person" class="form-control" id="type_person" required>
                <option value="">Seleccione...</option>
                <option value="1" <?= (isset($third['type_person']) && $third['type_person'] == 1) ? "selected='selected'" : "" ?> >Jurídica</option>
                <option value="2" <?= (isset($third['type_person']) && $third['type_person'] == 2) ? "selected='selected'" : "" ?> >Natural</option>
                <option value="3" <?= (isset($third['type_person']) && $third['type_person'] == 3) ? "selected='selected'" : "" ?> >Gran contribuyente</option>
              </select>
            </div>
            <div class="form-group col-sm-4">
              <label>Tipo de Régimen</label><span class='input_required'> *</span>
              <select name="tipo_regimen" class="form-control" required>
                <option value="">Seleccione....</option>
                <option value="1" <?= (isset($third['tipo_regimen']) && $third['tipo_regimen'] == 1) ? "selected='selected'" : "" ?> >No responsable de IVA / Simple</option>
                <option value="2" <?= (isset($third['tipo_regimen']) && $third['tipo_regimen'] == 2) ? "selected='selected'" : "" ?> >Responsable de IVA</option>
                <option value="0" <?= (isset($third['tipo_regimen']) && $third['tipo_regimen'] == 0) ? "selected='selected'" : "" ?> >No Aplica</option>
              </select>
            </div>
            <div class="form-group col-sm-4">
              <label>Tipo de documento</label>
              <select name="tipo_documento" id="tipo_documento" class="form-control" id="tipo_documento" required>
                <option value="">Seleccione...</option>
                <?php foreach ($documenttypes as $row => $dt): ?>
                  <option value="<?= $dt['id'] ?>" <?= ($dt['id'] == $third['tipo_documento']) ? "selected='selected'" : "" ?> ><?= $dt['nombre'] ?></option>
                <?php endforeach ?>
              </select>
            </div>
            <div class="form-group col-sm-4">
              <label><?= lang('thirds_cntrler_add_form_vat_no_label') ?></label><span class='input_required'> *</span>
              <input type="text" name="vat_no" id="vat_no" class="form-control" <?= (isset($third['vat_no'])) ? "value='".$third['vat_no']."'" : "" ?> required>
            </div>
            <div class="form-group col-sm-4 digito-verificacion" style="display: none;">
              <label>Dígito de verificación</label>
              <input type="number" name="digito_verificacion" id="digito_verificacion" class="form-control" value="<?= (isset($third['digito_verificacion'])) ? $third['digito_verificacion'] : '' ?>" readonly>
            </div>
            <div class="juridical_person">
               <div class="form-group col-sm-4">
                <label><?= lang('thirds_cntrler_add_form_name_label') ?></label><span class='input_required'> *</span>
                <input type="text" name="name" id="name" class="form-control" <?= (isset($third['name'])) ? "value='".$third['name']."'" : "" ?> required>
              </div>
            </div>
            <div class="natural_person" style="display: none;">
              <div class="form-group col-sm-3">
                <label>Primer Nombre</label><span class='input_required'> *</span>
                <input type="text" name="first_name" id="first_name" class="form-control required" <?= (isset($third['first_name'])) ? "value='".$third['first_name']."'" : "" ?>>
              </div>
              <div class="form-group col-sm-3">
                <label>Segundo Nombre</label>
                <input type="text" name="second_name" id="second_name" class="form-control" <?= (isset($third['second_name'])) ? "value='".$third['second_name']."'" : "" ?>>
              </div>
              <div class="form-group col-sm-3">
                <label>Primer Apellido</label><span class='input_required'> *</span>
                <input type="text" name="first_lastname" id="first_lastname" class="form-control required" <?= (isset($third['first_lastname'])) ? "value='".$third['first_lastname']."'" : "" ?>>
              </div>
              <div class="form-group col-sm-3">
                <label>Segundo Apellido</label>
                <input type="text" name="second_lastname" id="second_lastname" class="form-control" <?= (isset($third['second_lastname'])) ? "value='".$third['second_lastname']."'" : "" ?>>
              </div>
            </div>
            <div class="form-group col-sm-4">
              <label><?= lang('thirds_cntrler_add_form_postal_code_label') ?></label><span class='input_required'> *</span>
              <input type="text" name="postal_code" class="form-control" <?= (isset($third['postal_code'])) ? "value='".$third['postal_code']."'" : "" ?> required>
            </div>
            <div class="form-group col-sm-4">
              <label><?= lang('thirds_cntrler_add_form_address_label') ?></label><span class='input_required'> *</span>
              <input type="text" name="address" class="form-control" <?= (isset($third['address'])) ? "value='".$third['address']."'" : "" ?> required>
            </div>
            <div class="form-group col-sm-4">
              <label><?= lang('thirds_cntrler_add_form_phone_label') ?></label><span class='input_required'> *</span>
              <input type="text" name="phone" class="form-control" <?= (isset($third['phone'])) ? "value='".$third['phone']."'" : "" ?> required>
            </div>
            <div class="form-group col-sm-4">
              <label><?= lang('thirds_cntrler_add_form_email_label') ?></label><span class='input_required'> *</span>
              <input type="email" name="email" class="form-control" <?= (isset($third['email'])) ? "value='".$third['email']."'" : "" ?> required>
            </div>
            <div class="form-group col-sm-4">
              <label><?= lang('thirds_cntrler_add_form_country_label') ?></label><span class='input_required'> *</span>
              <select class="form-control ledger-dropdown" name="country" id="country" required>
                <option value="">Seleccione...</option>
                <?php foreach ($countries as $row => $country): ?>
                  <option value="<?= $country['NOMBRE'] ?>" data-code="<?= $country['CODIGO'] ?>" <?= (isset($third['country']) && $country['NOMBRE'] == $third['country']) ? "selected='selected'" : "" ?>><?= $country['NOMBRE'] ?></option>
                <?php endforeach ?>
              </select>
            </div>
            <div class="form-group col-sm-4">
              <label><?= lang('thirds_cntrler_add_form_state_label') ?></label><span class='input_required'> *</span>
              <select class="form-control ledger-dropdown" name="state" id="state" required>
                <?php foreach ($states as $key => $state): ?>
                  <option value="<?= $state['DEPARTAMENTO'] ?>" data-code="<?= $state['CODDEPARTAMENTO'] ?>" <?= (isset($third['state']) && $state['DEPARTAMENTO'] == $third['state']) ? "selected='selected'" : "" ?>><?= $state['DEPARTAMENTO'] ?></option>
                <?php endforeach ?>
              </select>
            </div>
            <div class="form-group col-sm-4">
              <label><?= lang('thirds_cntrler_add_form_city_label') ?></label><span class='input_required'> *</span>
              <select class="form-control ledger-dropdown" name="city" id="city" required>
                <?php foreach ($cities as $key => $city): ?>
                  <option value="<?= $city['DESCRIPCION'] ?>" data-code="<?= $city['CODIGO'] ?>"><?= $city['DESCRIPCION'] ?></option>
                <?php endforeach ?>
              </select>
              <input type="hidden" name="city_code" id="city_code" <?= (isset($third['city_code'])) ? "value='".$third['city_code']."'" : "" ?>>
            </div>
            <input type="hidden" name="customer_group_id" value="<?= $customer_group_default ?>">
            <input type="hidden" name="customer_group_name" value="<?= $name_customer_group_default ?>">
            <input type="hidden" name="price_group_id" value="<?= $price_group_default ?>">
            <input type="hidden" name="price_group_name" value="<?= $name_price_group_default ?>">
          </form>
            <div class="col-sm-12">
              <button class="btn btn-success submit_third"><span class="fa fa-check"></span> Enviar</button>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>


<script type="text/javascript">

$(document).ready(function(){
   $('#loader').fadeOut();

   $(".ledger-dropdown").select2({
    width:'100%'
  });

   arreglar_tamaño_divs();

    jQuery.extend(jQuery.validator.messages, {//Configuración jquery valid
    required: "Este campo es obligatorio.",
    remote: "Por favor, rellena este campo.",
    email: "Por favor, escribe una dirección de correo válida",
    url: "Por favor, escribe una URL válida.",
    date: "Por favor, escribe una fecha válida.",
    dateISO: "Por favor, escribe una fecha (ISO) válida.",
    number: "Por favor, escribe un número entero válido.",
    digits: "Por favor, escribe sólo dígitos.",
    creditcard: "Por favor, escribe un número de tarjeta válido.",
    equalTo: "Por favor, escribe el mismo valor de nuevo.",
    accept: "Por favor, escribe un valor con una extensión aceptada.",
    maxlength: jQuery.validator.format("Por favor, no escribas más de {0} caracteres."),
    minlength: jQuery.validator.format("Por favor, no escribas menos de {0} caracteres."),
    rangelength: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1} caracteres."),
    range: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1}."),
    max: jQuery.validator.format("Por favor, escribe un valor menor o igual a {0}."),
    min: jQuery.validator.format("Por favor, escribe un valor mayor o igual a {0}.")
  });

    $('#type_person').trigger('change');

});

$(document).on('change', '#state', function(){
  dpto = $('#state option:selected').data('code');

  $.ajax({
    url:"<?= base_url() ?>thirds/get_cities/"+dpto,
  }).done(function(data){
    $('#city').html(data);
    console.log(data);
  }).fail(function(data){
    console.log(data);
  });

});

$(document).on('change', '#country', function(){
  dpto = $('#country option:selected').data('code');

  $.ajax({
    url:"<?= base_url() ?>thirds/get_states/"+dpto,
  }).done(function(data){
    $('#state').html(data);
    console.log(data);
  }).fail(function(data){
    console.log(data);
  });

});

$(document).on('change', '#city', function(){
  code = $('#city option:selected').data('code');
  $('#city_code').val(code);
});

$(document).on('change', '#type_person', function(){
  tp = $(this).val();

  if (tp == 2) {

    var name_arr = $('#name').val().split(' ');
    console.log(name_arr);

    if (name_arr.length == 2) {
      $('#first_name').val(name_arr[0]);
      $('#first_lastname').val(name_arr[1]);
    } else if (name_arr.length == 3) {
      $('#first_name').val(name_arr[0]);
      $('#first_lastname').val(name_arr[1]);
      $('#second_lastname').val(name_arr[2]);
    } else if (name_arr.length == 4) {
      $('#first_name').val(name_arr[0]);
      $('#second_name').val(name_arr[1]);
      $('#first_lastname').val(name_arr[2]);
      $('#second_lastname').val(name_arr[3]);
    } else if (name_arr.length > 4) {
      $('#first_name').val('');
      $('#second_name').val('');
      $('#first_lastname').val('');
      $('#second_lastname').val('');
    }

    $('.natural_person').css('display', '');

    $.each($('.natural_person').find('.required'), function(){
      $(this).attr('required', true);
    });

    $('.juridical_person').css('display', 'none');

    $.each($('.juridical_person').find('.required'), function(){
      $(this).attr('required', false);
    });
  } else if (tp != 2){
    $('.natural_person').css('display', 'none');

    $.each($('.natural_person').find('.required'), function(){
      $(this).attr('required', false);
    });

    $('.juridical_person').css('display', '');

    $.each($('.juridical_person').find('.required'), function(){
      $(this).attr('required', true);
    });
  }

  arreglar_tamaño_divs();

});

$(document).on('change', '#group_id', function(){
  name = $('#group_id option:selected').data('name');
  $('#group_name').val(name);
});

$(document).on('change', '#customer_group_id', function(){
  name = $('#customer_group_id option:selected').data('name');
  $('#customer_group_name').val(name);
});

$(document).on('change', '#price_group_id', function(){
  name = $('#price_group_id option:selected').data('name');
  $('#price_group_name').val(name);
});

$(document).on('click', '.submit_third', function(){
  if ($('#edit_third').valid()) {
    $('#edit_third').submit();
  } else {
    arreglar_tamaño_divs();
  }
});

$(document).on('change', '#tipo_documento', function(){
  if ($(this).val() == 6) {
    $('.digito-verificacion').css('display', '');
    $('.digito-verificacion input').attr('required', true);
    set_digito_verificacion($('#vat_no').val());
  } else {
    $('.digito-verificacion').css('display', 'none');
    $('.digito-verificacion input').attr('required', false);
  }
});

setTimeout(function() {
  $('#tipo_documento').trigger('change');
}, 800);

$(document).on('keyup', '#vat_no', function(){
  // validar_existe_tercero($('#group_id').val(), $(this).val());
  set_digito_verificacion($(this).val());
});


function set_digito_verificacion(vat_no){
  if ($('#tipo_documento option:selected').text() == "NIT") {
        var DV = calcularDigitoVerificacion(vat_no);
        if (DV > 0) {
          $('#digito_verificacion').val(DV);
        } else {
          $('#digito_verificacion').val(0);
        }
  }
}


function arreglar_tamaño_divs(){

    var heights = $(".form-group").map(function() {
          return $(this).height();
      }).get(),

      maxHeight = Math.max.apply(null, heights);

      $(".form-group").height(maxHeight);

}

function  calcularDigitoVerificacion ( myNit )  {
    var vpri,
        x,
        y,
        z;
    
    // Se limpia el Nit
    myNit = myNit.replace ( /\s/g, "" ) ; // Espacios
    myNit = myNit.replace ( /,/g,  "" ) ; // Comas
    myNit = myNit.replace ( /\./g, "" ) ; // Puntos
    myNit = myNit.replace ( /-/g,  "" ) ; // Guiones
    
    // Se valida el nit
    if  ( isNaN ( myNit ) )  {
      console.log ("El nit/cédula '" + myNit + "' no es válido(a).") ;
      return "" ;
    };
    
    // Procedimiento
    vpri = new Array(16) ; 
    z = myNit.length ;

    vpri[1]  =  3 ;
    vpri[2]  =  7 ;
    vpri[3]  = 13 ; 
    vpri[4]  = 17 ;
    vpri[5]  = 19 ;
    vpri[6]  = 23 ;
    vpri[7]  = 29 ;
    vpri[8]  = 37 ;
    vpri[9]  = 41 ;
    vpri[10] = 43 ;
    vpri[11] = 47 ;  
    vpri[12] = 53 ;  
    vpri[13] = 59 ; 
    vpri[14] = 67 ; 
    vpri[15] = 71 ;

    x = 0 ;
    y = 0 ;
    for  ( var i = 0; i < z; i++ )  { 
      y = ( myNit.substr (i, 1 ) ) ;
      // console.log ( y + "x" + vpri[z-i] + ":" ) ;

      x += ( y * vpri [z-i] ) ;
      // console.log ( x ) ;    
    }

    y = x % 11 ;
    // console.log ( y ) ;

    return ( y > 1 ) ? 11 - y : y ;
  }

</script>