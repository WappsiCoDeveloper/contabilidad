<div class="row wrapper border-bottom white-bg page-heading">
  <div class="col-lg-8">
	<h2 class="box-title"><?= lang('thirds_cntrler_import_h3_title_1'); ?>
		<?= lang('entries_cntrler_add_h3_title_2'); ?>
	</h2>
    <ol class="breadcrumb">
      <li>
        <a href="login/index">Inicio</a> <?php //En el controlador en la función llamada, se revisa si el usuario está logueado y lo envía a la vista principal según permisos ?>
      </li>
      <li class="active">
        <strong><?php echo $page_title; ?></strong>
      </li>
    </ol>
  </div><!-- /.col -->
  <div class="col-lg-4">
    <div class="title-action">

    </div>
  </div>
</div><!-- /.row -->

<!-- Main content -->
    <section class="row wrapper wrapper-content animated fadeInRight">
      <!-- Small boxes (Stat box) -->
      <div class="ibox float-e-margins">
        <!-- /.col -->
        <div class="ibox-content contentBackground">
          <div class="col-sm-12">
            <div class="box-header with-border">
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            	<?php echo form_open_multipart('thirds/import'); ?>
            			<?php if (isset($error_msg)): ?>
			            	<div class="row">
			            		<div class="panel panel-danger">
  											<div class="panel-heading">No se registró ningún tercero por errores encontrados</div>
											  <div class="panel-body">
			                		<?= $error_msg ?>
											  </div>
											</div>
			            	</div>
            			<?php endif ?>
            			<?php if (isset($success_msg)): ?>
			            	<div class="row">
			            		<div class="panel panel-success">
  											<div class="panel-heading">Importado con éxito</div>
											  <div class="panel-body">
			                		<?= $success_msg ?>
											  </div>
											</div>
			            	</div>
            			<?php endif ?>
			            <div class="row">
			                <div class="col-md-6">
			                    <div class="form-group">
			                        <label for="entriesxls"><?= 'Seleccione archivo XLS:'; ?></label>
			                        <input type="file" name="entriesxls" id="entriesxls">
			                    </div>
			                </div>
			                <div class="col-md-6">
			                    <div class="well well-lg">
			                        <a href="<?= base_url(); ?>assets/importacion_terceros.xlsx" class="btn btn-success"><?=lang('entries_importer_sample_button');?></a>
			                    </div>
			                </div>
			            </div>
			            <div class="col-sm-12">
			                <button type="submit" class="btn btn-success pull-right"><?=lang('accounts_importer_submit_button');?></button>
			                <a href="<?= base_url(); ?>accounts/index" id="cancel" name="cancel" class="btn btn-default pull-right" style="margin-right: 5px;"><?=lang('accounts_importer_cancel_button');?></a>
			            </div>
			        <?php echo form_close(); ?>
            </div>
          </div>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->