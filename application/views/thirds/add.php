<script type="text/javascript">$('#loader').fadeIn();</script>

<div class="row wrapper border-bottom white-bg page-heading">
  <div class="col-lg-8">
    <h2><?php echo $page_title; ?></h2>
    <ol class="breadcrumb">
      <li>
        <a href="login/index">Inicio</a> <?php //En el controlador en la función llamada, se revisa si el usuario está logueado y lo envía a la vista principal según permisos ?>
      </li>
      <li class="active">
        <strong><?php echo $page_title; ?></strong>
      </li>
    </ol>
  </div><!-- /.col -->
  <div class="col-lg-4">
    <div class="title-action">
       <button class="btn btn-success submit_third"><span class="fa fa-check"></span> Enviar</button>
    </div>
  </div>
</div>
<div class="row wrapper wrapper-content animated fadeInRight">
  <div class="ibox float-e-margins">
    <div class="ibox-content contentBackground">
      <div class="col-xs-12">
        <div class="row">
          <form class="form" id="new_third" method="POST">
            <div class="form-group col-sm-4">
              <label><?= lang('thirds_cntrler_add_form_group_id_label') ?></label><span class='input_required'> *</span>
              <select class="form-control" name="group_id" id="group_id" required>
                <option value="">Seleccione...</option><!-- 
                <option value="3" data-name="customer"><?= lang('Customer') ?></option>
                <option value="8" data-name="employee"><?= lang('Employee') ?></option>
                <option value="4" data-name="supplier"><?= lang('Supplier') ?></option>
                <option value="9" data-name="creditor"><?= lang('Creditor') ?></option> -->
                <?php foreach ($third_groups as $row => $tgs): ?>
                  <option value="<?= $tgs['id'] ?>" data-name="<?= $tgs['name'] ?>"><?= lang($tgs['description']) ?></option>
                <?php endforeach ?>
                <input type="hidden" name="group_name" id="group_name" <?= (isset($_POST['group_name'])) ? "value='".$_POST['group_name']."'" : "" ?>>
              </select>
            </div>
            <div class="form-group col-sm-4">
              <label>Tipo de persona</label><span class='input_required'> *</span>
              <select name="type_person" class="form-control" id="type_person" required>
                <option value="">Seleccione...</option>
                <option value="1">Jurídica</option>
                <option value="2">Natural</option>
                <option value="3">Gran contribuyente</option>
              </select>
            </div>
            <div class="form-group col-sm-4">
              <label>Tipo de Régimen</label><span class='input_required'> *</span>
              <select name="tipo_regimen" class="form-control" required>
                <option value="">Seleccione....</option>
                <option value="1" >No responsable de IVA / Simple</option>
                <option value="2" >Responsable de IVA</option>
                <option value="0" >No Aplica</option>
              </select>
            </div>
            <div class="form-group col-sm-4">
              <label>Tipo de documento</label>
              <select name="tipo_documento" class="form-control" id="tipo_documento" required>
                <option value="">Seleccione...</option>
                <?php foreach ($documenttypes as $row => $dt): ?>
                  <option value="<?= $dt['id'] ?>"><?= $dt['nombre'] ?></option>
                <?php endforeach ?>
              </select>
            </div>
            <div class="form-group col-sm-4">
              <label><?= lang('thirds_cntrler_add_form_vat_no_label') ?></label><span class='input_required'> *</span>
              <input type="text" name="vat_no" id="vat_no" class="form-control" <?= (isset($_POST['vat_no'])) ? "value='".$_POST['vat_no']."'" : "" ?> required>
            </div>
            <div class="form-group col-sm-4 digito-verificacion" style="display: none;">
              <label>Dígito de verificación</label>
              <input type="number" name="digito_verificacion" id="digito_verificacion" class="form-control" value="0" readonly>
            </div>
            <div class="juridical_person">
               <div class="form-group col-sm-4">
                <label><?= lang('thirds_cntrler_add_form_name_label') ?></label><span class='input_required'> *</span>
                <input type="text" name="name" class="form-control required" <?= (isset($_POST['name'])) ? "value='".$_POST['name']."'" : "" ?>>
              </div>
            </div>
            <div class="natural_person" style="display: none;">
              <div class="form-group col-sm-3">
                <label>Primer Nombre</label><span class='input_required'> *</span>
                <input type="text" name="first_name" class="form-control required">
              </div>
              <div class="form-group col-sm-3">
                <label>Segundo Nombre</label>
                <input type="text" name="second_name" class="form-control">
              </div>
              <div class="form-group col-sm-3">
                <label>Primer Apellido</label><span class='input_required'> *</span>
                <input type="text" name="first_lastname" class="form-control required">
              </div>
              <div class="form-group col-sm-3">
                <label>Segundo Apellido</label>
                <input type="text" name="second_lastname" class="form-control">
              </div>
            </div>
            <div class="form-group col-sm-4">
              <label><?= lang('thirds_cntrler_add_form_address_label') ?></label><span class='input_required'> *</span>
              <input type="text" name="address" class="form-control" <?= (isset($_POST['address'])) ? "value='".$_POST['address']."'" : "" ?> required>
            </div>
            <div class="form-group col-sm-4">
              <label><?= lang('thirds_cntrler_add_form_phone_label') ?></label><span class='input_required'> *</span>
              <input type="text" name="phone" class="form-control" <?= (isset($_POST['phone'])) ? "value='".$_POST['phone']."'" : "" ?> required>
            </div>
            <div class="form-group col-sm-4">
              <label><?= lang('thirds_cntrler_add_form_email_label') ?></label><span class='input_required'> *</span>
              <input type="email" name="email" class="form-control" <?= (isset($_POST['email'])) ? "value='".$_POST['email']."'" : "" ?> required>
            </div>
            <div class="form-group col-sm-4">
              <label><?= lang('thirds_cntrler_add_form_country_label') ?></label><span class='input_required'> *</span>
              <select class="form-control ledger-dropdown" name="country" id="country" required>
                <option value="">Seleccione...</option>
                <?php foreach ($countries as $row => $country): ?>
                  <option value="<?= $country['NOMBRE'] ?>" data-code="<?= $country['CODIGO'] ?>"><?= $country['NOMBRE'] ?></option>
                <?php endforeach ?>
              </select>
            </div>
            <div class="form-group col-sm-4">
              <label><?= lang('thirds_cntrler_add_form_state_label') ?></label><span class='input_required'> *</span>
              <select class="form-control ledger-dropdown" name="state" id="state" required>
                <option value="">Seleccione país</option>
              </select>
            </div>
            <div class="form-group col-sm-4">
              <label><?= lang('thirds_cntrler_add_form_city_label') ?></label><span class='input_required'> *</span>
              <select class="form-control ledger-dropdown" name="city" id="city" required>
                <option value="">Seleccione departamento</option>
              </select>
              <input type="hidden" name="city_code" id="city_code" <?= (isset($_POST['city_code'])) ? "value='".$_POST['city_code']."'" : "" ?>>
            </div>
            <div class="form-group col-sm-4">
              <label><?= lang('thirds_cntrler_add_form_postal_code_label') ?></label><span class='input_required'> *</span>
              <input type="text" name="postal_code" class="form-control postal_code" <?= (isset($_POST['postal_code'])) ? "value='".$_POST['postal_code']."'" : "" ?> readonly>
            </div>
            <input type="hidden" name="customer_group_id" value="<?= $customer_group_default ?>">
            <input type="hidden" name="customer_group_name" value="<?= $name_customer_group_default ?>">
            <input type="hidden" name="price_group_id" value="<?= $price_group_default ?>">
            <input type="hidden" name="price_group_name" value="<?= $name_price_group_default ?>">
          </form>
            <div class="col-sm-12" style="padding-top: 2%">
              <button class="btn btn-success submit_third"><span class="fa fa-check"></span> Enviar</button>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>


<script type="text/javascript">

$(document).ready(function(){
   $('#loader').fadeOut();

   $(".ledger-dropdown").select2({
    width:'100%'
  });

   arreglar_tamaño_divs();

    jQuery.extend(jQuery.validator.messages, {//Configuración jquery valid
    required: "Este campo es obligatorio.",
    remote: "Por favor, rellena este campo.",
    email: "Por favor, escribe una dirección de correo válida",
    url: "Por favor, escribe una URL válida.",
    date: "Por favor, escribe una fecha válida.",
    dateISO: "Por favor, escribe una fecha (ISO) válida.",
    number: "Por favor, escribe un número entero válido.",
    digits: "Por favor, escribe sólo dígitos.",
    creditcard: "Por favor, escribe un número de tarjeta válido.",
    equalTo: "Por favor, escribe el mismo valor de nuevo.",
    accept: "Por favor, escribe un valor con una extensión aceptada.",
    maxlength: jQuery.validator.format("Por favor, no escribas más de {0} caracteres."),
    minlength: jQuery.validator.format("Por favor, no escribas menos de {0} caracteres."),
    rangelength: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1} caracteres."),
    range: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1}."),
    max: jQuery.validator.format("Por favor, escribe un valor menor o igual a {0}."),
    min: jQuery.validator.format("Por favor, escribe un valor mayor o igual a {0}.")
  });
});

$(document).on('change', '#type_person', function(){
  tp = $(this).val();

  if (tp == 2) {
    $('.natural_person').css('display', '');

    $.each($('.natural_person').find('.required'), function(){
      $(this).attr('required', true);
    });

    $('.juridical_person').css('display', 'none');

    $.each($('.juridical_person').find('.required'), function(){
      $(this).attr('required', false);
    });
  } else if (tp != 2){
    $('.natural_person').css('display', 'none');

    $.each($('.natural_person').find('.required'), function(){
      $(this).attr('required', false);
    });

    $('.juridical_person').css('display', '');

    $.each($('.juridical_person').find('.required'), function(){
      $(this).attr('required', true);
    });
  }

  arreglar_tamaño_divs();

});


$(document).on('change', '#state', function(){
  dpto = $('#state option:selected').data('code');

  // $('.postal_code').val($('.postal_code').val()+dpto);

  $.ajax({
    url:"<?= base_url() ?>thirds/get_cities/"+dpto,
  }).done(function(data){
    $('#city').html(data);
    console.log(data);
  }).fail(function(data){
    console.log(data);
  });

});

$(document).on('change', '#country', function(){
  dpto = $('#country option:selected').data('code');

  // $('.postal_code').val(dpto);

  $.ajax({
    url:"<?= base_url() ?>thirds/get_states/"+dpto,
  }).done(function(data){
    $('#state').html(data);
    console.log(data);
  }).fail(function(data){
    console.log(data);
  });

});

$(document).on('change', '#city', function(){
  code = $('#city option:selected').data('code');
  // $('.postal_code').val($('.postal_code').val()+code);
  $('.postal_code').val(code);
  $('#city_code').val(code);
});

$(document).on('change', '#group_id', function(){
  name = $('#group_id option:selected').data('name');
  $('#group_name').val(name);

  validar_existe_tercero($(this).val(), $('#vat_no').val());

});

$(document).on('keyup', '#vat_no', function(){
  validar_existe_tercero($('#group_id').val(), $(this).val());

  if ($('#tipo_documento option:selected').text() == "NIT") {
      var DV = calcularDigitoVerificacion($(this).val());
      if (DV > 0) {
        $('#digito_verificacion').val(DV);
      } else {
        $('#digito_verificacion').val(0);
      }
  }

});

$(document).on('change', '#customer_group_id', function(){
  name = $('#customer_group_id option:selected').data('name');
  $('#customer_group_name').val(name);
});

$(document).on('change', '#price_group_id', function(){
  name = $('#price_group_id option:selected').data('name');
  $('#price_group_name').val(name);
});

$(document).on('click', '.submit_third', function(){
  if ($('#new_third').valid()) {
    $('#new_third').submit();
  } else {
    arreglar_tamaño_divs();
  }
});

$(document).on('change', '#tipo_documento', function(){
  if ($(this).val() == 6) {
    $('.digito-verificacion').css('display', '');
    $('.digito-verificacion input').attr('required', true);
    $('#digito_verificacion').val(calcularDigitoVerificacion($('#vat_no').val()));
  } else {
    $('.digito-verificacion').css('display', 'none');
    $('.digito-verificacion input').attr('required', false);
  }
});


function validar_existe_tercero(perfil, documento){
  $.ajax({
    url:"<?= base_url() ?>thirds/third_exists/"+perfil+"/"+documento,
  }).done(function(data){
    if (data == "1") {
      Command: toastr.error('Existe un tercero con el perfil y número de documento indicado', '¡Error!', {onHidden : function(){}})
      $('.submit_third').attr('disabled', true);
    } else if (data == "0") {
      console.log('NO EXISTE');
      $('.submit_third').attr('disabled', false);
    }
  });
}

function arreglar_tamaño_divs(){

    var heights = $(".form-group").map(function() {
          return $(this).height();
      }).get(),

      maxHeight = Math.max.apply(null, heights);

      $(".form-group").height(maxHeight);

}

function  calcularDigitoVerificacion ( myNit )  {
    var vpri,
        x,
        y,
        z;
    
    // Se limpia el Nit
    myNit = myNit.replace ( /\s/g, "" ) ; // Espacios
    myNit = myNit.replace ( /,/g,  "" ) ; // Comas
    myNit = myNit.replace ( /\./g, "" ) ; // Puntos
    myNit = myNit.replace ( /-/g,  "" ) ; // Guiones
    
    // Se valida el nit
    if  ( isNaN ( myNit ) )  {
      console.log ("El nit/cédula '" + myNit + "' no es válido(a).") ;
      return "" ;
    };
    
    // Procedimiento
    vpri = new Array(16) ; 
    z = myNit.length ;

    vpri[1]  =  3 ;
    vpri[2]  =  7 ;
    vpri[3]  = 13 ; 
    vpri[4]  = 17 ;
    vpri[5]  = 19 ;
    vpri[6]  = 23 ;
    vpri[7]  = 29 ;
    vpri[8]  = 37 ;
    vpri[9]  = 41 ;
    vpri[10] = 43 ;
    vpri[11] = 47 ;  
    vpri[12] = 53 ;  
    vpri[13] = 59 ; 
    vpri[14] = 67 ; 
    vpri[15] = 71 ;

    x = 0 ;
    y = 0 ;
    for  ( var i = 0; i < z; i++ )  { 
      y = ( myNit.substr (i, 1 ) ) ;
      // console.log ( y + "x" + vpri[z-i] + ":" ) ;

      x += ( y * vpri [z-i] ) ;
      // console.log ( x ) ;    
    }

    y = x % 11 ;
    // console.log ( y ) ;

    return ( y > 1 ) ? 11 - y : y ;
  }

</script>