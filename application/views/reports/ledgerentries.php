<script type="text/javascript">
$(document).ready(function() {

	$("#accordion").accordion({
		collapsible: true,
		<?php
			if (isset($options) && $options == false) {
				echo 'active: false';
			}
		?>
	});

	/* Calculate date range in javascript */
	startDate = new Date(<?php echo strtotime($this->mAccountSettings->fy_start) * 1000; ?>  + (new Date().getTimezoneOffset() * 60 * 1000));
	endDate = new Date(<?php echo strtotime($this->mAccountSettings->fy_end) * 1000; ?>  + (new Date().getTimezoneOffset() * 60 * 1000));

	$(document.body).on("change","#ReportLedgerId",function(){
		if(this.value == 0){
			$('#ReportStartdate').prop('disabled', true);
			$('#ReportEnddate').prop('disabled', true);
		} else {
			$('#ReportStartdate').prop('disabled', false);
			$('#ReportEnddate').prop('disabled', false);
		}
	});
	$('#ReportLedgerId').trigger('change');

	/* Setup jQuery datepicker ui */
	$('#ReportStartdate').datepicker({
		minDate: startDate,
		maxDate: endDate,
		dateFormat: '<?php echo $this->mDateArray[1]; ?>',
		numberOfMonths: 1,
		onClose: function(selectedDate) {
			if (selectedDate) {
				$("#ReportEnddate").datepicker("option", "minDate", selectedDate);
			} else {
				$("#ReportEnddate").datepicker("option", "minDate", startDate);
			}
		}
	});
	$('#ReportEnddate').datepicker({
		minDate: startDate,
		maxDate: endDate,
		dateFormat: '<?php echo $this->mDateArray[1]; ?>',
		numberOfMonths: 1,
		onClose: function(selectedDate) {
			if (selectedDate) {
				$("#ReportStartdate").datepicker("option", "maxDate", selectedDate);
			} else {
				$("#ReportStartdate").datepicker("option", "maxDate", endDate);
			}
		}
	});

	$("#ReportLedgerId").select2({width:'100%'});
});
</script>
	<div class="row wrapper border-bottom white-bg page-heading">
	  <div class="col-lg-8">
	    <h2><?php echo $page_title; ?></h2>
	    <ol class="breadcrumb">
	      <li>
	        <a href="login/index">Inicio</a> <?php //En el controlador en la función llamada, se revisa si el usuario está logueado y lo envía a la vista principal según permisos ?>
	      </li>
	      <li class="active">
	        <strong><?php echo $page_title; ?></strong>
	      </li>
	    </ol>
	  </div><!-- /.col -->
	  <div class="col-lg-4">
	  </div>
	</div><!-- /.row -->
    <section class="row wrapper wrapper-content animated fadeInRight">
      <div class="box float-e-margins">
        <div class="ibox-content contentBackground">
          <div class="col-sm-12">
            <div class="box-body">
            	<div class="balancesheet form">
					<?php echo form_open(); ?>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label><?= lang('ledger_acc_name'); ?></label>
								<select class="form-control" id="ReportLedgerId" name="ledger_id">
									<?php foreach ($ledgers as $id => $ledger): ?>
										<option value="<?= $id; ?>" <?= ($id < 0) ? 'disabled' : "" ?> <?= ($this->input->post('ledger_id') == $id) ?'selected':''?>><?= $ledger; ?></option>
									<?php endforeach; ?>
								</select>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label><?= lang('start_date'); ?></label>

			                    <div class="input-group">
								<input id="ReportStartdate" type="text" name="startdate" class="form-control">
			                        <div class="input-group-addon">
			                            <i>
			                                <div class="fa fa-info-circle" data-toggle="tooltip" title="<?= lang('start_date_span') ;?>">
			                                </div>
			                            </i>
			                        </div>
			                    </div>
			                    <!-- /.input group -->
			                </div>
			                <!-- /.form group -->
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label><?= lang('end_date'); ?></label>

			                    <div class="input-group">
								<input id="ReportEnddate" type="text" name="enddate" class="form-control">
			                        <div class="input-group-addon">
			                            <i>
			                                <div class="fa fa-info-circle" data-toggle="tooltip" title="<?= lang('end_date_span') ;?>">
			                                </div>
			                            </i>
			                        </div>
			                    </div>
			                    <!-- /.input group -->
			                </div>
			                <!-- /.form group -->
						</div>
					</div>
					<div class="form-group">
						<input type="reset" name="reset" class="btn btn-primary pull-right" style="margin-left: 5px;" value="<?= lang('clear'); ?>">
						<input type="submit" name="submit" class="btn btn-primary pull-right" value="<?=lang('create_account_submit_button');?>">
						<?php
							if ($this->input->post('ledger_id')){
								$get = $this->input->post('ledger_id');

								if ($this->input->post('startdate')) {
									$get .= "?startdate=". $this->input->post('startdate');

								}
								if ($this->input->post('enddate')) {
									$get .= "&enddate=". $this->input->post('enddate');
								}
						?>
							<a href="<?=base_url();?>/reports/export_ledgerentries/xls/<?= $get; ?>" type="button" name="submit" class="btn btn-primary pull-right"><?=lang('export_to_xls');?></a>
							<a href="<?=base_url();?>/reports/export_ledgerentries/pdf/<?= $get; ?>" type="button" name="submit" class="btn btn-primary pull-right"><?=lang('export_to_pdf');?></a>
						<?php
							}
						?>
					</div>
					<?php form_close();  ?>
				</div>
				<div id="section-to-print">

				<?php if ($showEntries) {  ?>
					<div class="subtitle">
						<?php echo $subtitle; ?>
					</div>
					<div class="row" style="margin-bottom: 10px;">
						<div class="col-md-6">
							<table class="table summary stripped table-condensed">
								<tr>
									<td class="td-fixwidth-summary"><?php echo ('Bank or cash account'); ?></td>
									<td>

										<?php
											if ($ledger_data['type'] == 1) {
												echo lang('yes');
											} else {
												echo lang('no');
											}
										?>
									</td>
								</tr>
								<tr>
									<td class="td-fixwidth-summary"><?php echo ('Notes'); ?></td>
									<td><?php echo ($ledger_data['notes']); ?></td>
								</tr>
							</table>
						</div>
						<div class="col-md-6">
							<table class=" table summary stripped table-condensed">
								<tr>
									<td class="td-fixwidth-summary"><?php echo $opening_title; ?></td>
									<td><?php echo $this->functionscore->toCurrency($op['dc'], $op['amount']); ?></td>
								</tr>
								<tr>
									<td class="td-fixwidth-summary"><?php echo $closing_title; ?></td>
									<td><?php echo $this->functionscore->toCurrency($cl['dc'], $cl['amount']); ?></td>
								</tr>
							</table>
						</div>
					</div>

					<table class="table stripped">

					<tr>
					<th><?php echo lang('entries_views_add_label_date'); ?></th>
					<th><?php echo lang('entries_views_add_label_number'); ?></th>
					<th><?php echo lang('entries_views_add_items_th_ledger'); ?></th>
					<th><?php echo lang('entries_views_index_th_type'); ?></th>
					<th><?php echo lang('entries_views_index_th_tag'); ?></th>
					<th><?php echo lang('entries_views_index_th_debit_amount'); ?><?php echo ' (' . $this->mAccountSettings->currency_symbol . ')'; ?></th>
					<th><?php echo lang('entries_views_index_th_credit_amount'); ?><?php echo ' (' . $this->mAccountSettings->currency_symbol . ')'; ?></th>
					<th><?php echo lang('entries_views_index_th_actions'); ?></th>
					</tr>


					<?php
					/* Show the entries table */
					foreach ($entries as $entry) {
						$et = $this->DB1->where('id', $entry['entrytype_id'])->get('entrytypes'.$this->DB1->dbsuffix)->row_array();
						$entryTypeName = $et['name'];
						$entryTypeLabel = $et['label'];

						echo '<td>' . $this->functionscore->dateFromSql($entry['date']) . '</td>';
						echo '<td>' . ($this->functionscore->toEntryNumber($entry['number'], $entry['entrytype_id'])) . '</td>';
						echo '<td>' . ($this->functionscore->entryLedgers($entry['id'])) . '</td>';
						echo '<td>' . ($entryTypeName) . '</td>';
						echo '<td>' . $this->functionscore->showTag($entry['tag_id'])  . '</td>';
						if ($entry['dc'] == 'D') {
							echo '<td>' . $this->functionscore->toCurrency('D', $entry['amount']) . '</td>';
							echo '<td>' . '</td>';
						} else if ($entry['dc'] == 'C') {
							echo '<td>' . '</td>';
							echo '<td>' . $this->functionscore->toCurrency('C', $entry['amount']) . '</td>';
						} else {
							echo '<td>' . lang('search_views_amounts_td_error') . '</td>';
							echo '<td>' . lang('search_views_amounts_td_error') . '</td>';
						}
						echo '<td>';
						?>
							<a href="<?= base_url();?>entries/view/<?= ($entryTypeLabel); ?>/<?= $entry['entry_id']; ?>" class="no-hover" escape="false"><i class="glyphicon glyphicon-log-in"></i> <?=lang('view');?></a>
							<span class="link-pad"></span>
							<a href="<?= base_url();?>entries/edit/<?= ($entryTypeLabel); ?>/<?= $entry['entry_id']; ?>" class="no-hover" escape="false"><i class="glyphicon glyphicon-edit"></i> <?=lang('edit');?></a>
							<span class="link-pad"></span>
							<a href="<?= base_url();?>entries/delete/<?= ($entryTypeLabel); ?>/<?= $entry['entry_id']; ?>" class="no-hover" escape="false"><i class="glyphicon glyphicon-trash"></i> <?=lang('delete');?></a>
							
						<?php
						echo '</td>';
						echo '</tr>';
					}
					?>
					</table>
				<?php } ?>
				</div>
            </div>
          </div>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->