<?php 
require_once $_SERVER['DOCUMENT_ROOT'].'/contabilidad/vendor/fpdf/fpdf.php';

//$this->SetTextColor( 255, 0, 0); color rojo
//$this->SetTextColor( 0, 0, 0); color negro

// var_dump($POST);

	$totalParientes = [];

	$drcalc = 0;
	$crcalc = 0;

class PDF extends FPDF
  {

    function Header()
    {
      $this->SetFont('Arial','B',10);
      $this->Cell(196, 5 , utf8_decode(lang('page_title_reports_trialbalance')),'',1,'C');
      $this->SetFont('Arial','B',8);
      $this->Cell(32.66, 5, utf8_decode(lang('company')), '', 0, 'C');
      $this->Cell(32.66, 5, utf8_decode($this->settings->name), '', 0, 'C');
      $this->Cell(32.66, 5, utf8_decode(lang('admin_cntrler_create_account_validation_nit')), '', 0, 'C');
      $this->Cell(32.66, 5, utf8_decode($this->settings->nit), '', 1, 'C');
      $this->Cell(32.66, 5, utf8_decode(lang('from')), '', 0, 'C');
      $this->SetFont('Arial','',8);
      $this->Cell(32.66, 5, utf8_decode( ($this->POST['startdate'] != '' ? $this->POST['startdate'] : $this->settings->fy_start) ), '', 0, 'C');
      $this->SetFont('Arial','B',8);
      $this->Cell(32.66, 5, utf8_decode(lang('to')), '', 0, 'C');
      $this->SetFont('Arial','',8);
      $this->Cell(32.66, 5, utf8_decode( ($this->POST['enddate'] != '' ? $this->POST['enddate'] : date('Y-m-d')) ), '', 1, 'C');
      $this->SetFont('Arial','B',8);
      $this->Cell(32.66, 5, utf8_decode(lang('expedition_date')), '', 0, 'C');
      $this->SetFont('Arial','',8);
      $this->Cell(32.66, 5, utf8_decode(date('Y-m-d H:i:s')), '', 1, 'C');
      $this->SetFont('Arial','B',8);

      $this->Cell(73.66, 5 , utf8_decode(lang('accounts_index_account_name')),'LTB',0,'L');
      $this->Cell(22.66, 5 , utf8_decode(lang('entries_views_index_th_type')),'LTB',0,'L');
      $this->Cell(22.66, 5 , utf8_decode(lang('accounts_index_op_balance')),'LTB',0,'L');
      $this->Cell(25.66, 5 , utf8_decode(lang('dr_total')),'LTB',0,'L');
      $this->Cell(25.66, 5 , utf8_decode(lang('cr_total')),'LTB',0,'L');
      $this->Cell(25.66, 5 , utf8_decode(lang('accounts_index_cl_balance')),'LTBR',1,'L');
    }




	function print_account_chart($account, $c = 0, $niveles = NULL, $startcode = NULL, $endcode = NULL, $third_account_trial_balance = false)
	{
		$account = isset($account[0]) ? $account[0] : $account;
		if (empty($account)) {
			return [];
		}
		global $drcalc, $crcalc, $totalParientes;

		/* NIVELES */
		if (isset($_POST['nivelBalancesheet'])) {
			$nivelBalancesheet = $_POST['nivelBalancesheet'];
		} else {
			$nivelBalancesheet = "";
		}


		$bandera = 0;

		if ($nivelBalancesheet != "") {
			for ($i=1; $i <= $nivelBalancesheet ; $i++) { 
				if (in_array($account['code'], $niveles[$i])) {
					$bandera++;
				}
			}
		} else {
			$bandera++;
		}
		/* NIVELES */


		/* FILTRO POR CUENTAS */

		$pcBandera = 0;

		if (($startcode != NULL || $endcode != NULL) && $account['code'] != "") {
			
			$pcBandera = $this->bandera_posicion_cuentas($account['code'], $startcode, $endcode);

			if ($pcBandera != 0 && $account['code'] == $endcode) {

				$totalParientes[$account['code']]['dr_total'] = $account['dr_total'];
				$totalParientes[$account['code']]['cr_total'] = $account['cr_total'];
				
			}

		} else {

			$pcBandera++;

		}

		/* FILTRO POR CUENTAS */


		$clBandera = $this->bandera_closing_balance($account);

	  	$CI =& get_instance();
		$counter = $c;

		if (isset($_POST['third_id']) && $_POST['third_id'] != '') {
		
		} else {
			if (strlen($account['code']) > 0 && $bandera != 0 && ((isset($_POST['hidezero']) && $clBandera != 0) || !isset($_POST['hidezero'])) && ($pcBandera != 0)) {


				$this->Cell(73.66, 5 , utf8_decode($this->maxlengthname($CI->functionscore->toCodeWithName($account['code'], $account['name']))),'B',0,'L', FALSE);
				$this->Cell(22.66, 5 , utf8_decode(lang('accounts_index_td_label_group')),'B',0,'L', FALSE);
				$this->Cell(22.66, 5 , utf8_decode($CI->functionscore->toCurrency(($account['op_total'] < 0 ? 'C' : 'D'), ($account['op_total'] < 0 ? $account['op_total'] * -1 : $account['op_total']))),'B',0,'L', FALSE);
				$this->Cell(25.66, 5 , utf8_decode($CI->functionscore->toCurrency('D', $account['dr_total'])),'B',0,'R', FALSE);
				$this->Cell(25.66, 5 , utf8_decode($CI->functionscore->toCurrency('C', $account['cr_total'])),'B',0,'R', FALSE);
				$this->Cell(25.66, 5 , utf8_decode($CI->functionscore->toCurrency(($account['cl_total'] < 0 ? 'C' : 'D'), ($account['cl_total'] < 0 ? $account['cl_total'] * -1 : $account['cl_total']))),'B',1,'R', FALSE);
			}
		}

		/* Print groups */

		/* Print child ledgers */
		if (isset($account['children_ledgers']) && count($account['children_ledgers']) > 0 && $nivelBalancesheet == "") {
			$counter++;

			foreach ($account['children_ledgers'] as $id => $data) {

				$pcBandera = 0;

				if ($startcode != NULL || $endcode != NULL) {
					$pcBandera = $this->bandera_posicion_cuentas($data['code'], $startcode, $endcode);

					if ($pcBandera == 1) {
						if (isset($totalParientes[$data['parent_id']])) {
							$totalParientes[$data['parent_id']]['dr_total'] += $data['dr_total'];
							$totalParientes[$data['parent_id']]['cr_total'] += $data['cr_total'];
						} else {
							$totalParientes[$data['parent_id']]['dr_total'] = $data['dr_total'];
							$totalParientes[$data['parent_id']]['cr_total'] = $data['cr_total'];
						}
					}

				} else {
					$pcBandera++;
				}

				if ((isset($_POST['hidezero']) && $data['cl_total'] != 0 && $pcBandera != 0) || (!isset($_POST['hidezero']) && $pcBandera != 0)) {
					$this->Cell(73.66, 5 , utf8_decode($this->maxlengthname($CI->functionscore->toCodeWithName($data['code'], $data['name']))),'B',0,'L', FALSE);
					$this->Cell(22.66, 5 , utf8_decode(lang('accounts_index_td_label_ledger')),'B',0,'L', FALSE);
					$this->Cell(22.66, 5 , utf8_decode($CI->functionscore->toCurrency(($data['op_total'] < 0 ? 'C' : 'D'), ($data['op_total'] < 0 ? $data['op_total'] * -1 : $data['op_total']))),'B',0,'L', FALSE);
					$this->Cell(25.66, 5 , utf8_decode($CI->functionscore->toCurrency('D', $data['dr_total'])),'B',0,'R', FALSE);
					$this->Cell(25.66, 5 , utf8_decode($CI->functionscore->toCurrency('C', $data['cr_total'])),'B',0,'R', FALSE);
					$this->Cell(25.66, 5 , utf8_decode($CI->functionscore->toCurrency(($data['cl_total'] < 0 ? 'C' : 'D'), ($data['cl_total'] < 0 ? $data['cl_total'] * -1 : $data['cl_total']))),'B',1,'R', FALSE);
					if ($third_account_trial_balance && isset($third_account_trial_balance[$data['id']])) {
						foreach ($third_account_trial_balance[$data['id']] as $company_id => $company) {

							$this->Cell(73.66, 5 , utf8_decode(" >> ".$company['company_name']." - ".$company['company_vat_no']),'B',0,'L', FALSE);
							$this->Cell(22.66, 5 , utf8_decode(''),'B',0,'L', FALSE);
							$this->Cell(22.66, 5 , utf8_decode($CI->functionscore->toCurrency(($company['op_balance'] < 0 ? 'C' : 'D'), ($company['op_balance'] < 0 ? $company['op_balance'] * -1 : $company['op_balance']))),'B',0,'L', FALSE);
							$this->Cell(25.66, 5 , utf8_decode($CI->functionscore->toCurrency("D", $company['total_dr'])),'B',0,'R', FALSE);
							$this->Cell(25.66, 5 , utf8_decode($CI->functionscore->toCurrency("C", $company['total_cr'])),'B',0,'R', FALSE);
							$this->Cell(25.66, 5 , utf8_decode($CI->functionscore->toCurrency(($company['cl_balance'] < 0 ? 'C' : 'D'), ($company['cl_balance'] < 0 ? $company['cl_balance'] * -1 : $company['cl_balance']))),'B',1,'R', FALSE);
						}
					}
				}
			}
			$counter--;
		}

		/* Print child groups recursively */
		if (isset($account['children_groups'])) {
			foreach ($account['children_groups'] as $id => $data) {
				$counter++;
				$this->print_account_chart($data, $counter, $niveles, $startcode, $endcode, $third_account_trial_balance);
				$counter--;
			}
		}
			
		
	}

	function bandera_closing_balance($data, $bandera = 0) {
		$data = isset($data[0]) ? $data[0] : $data;
		if (!empty($data)) {
			if (isset($data['children_groups']) && count($data['children_groups']) > 0) {
				foreach ($data['children_groups'] as $id => $grupo) {
					if (isset($grupo['children_groups']) && count($grupo['children_groups']) > 0) {
						$bandera = $this->bandera_closing_balance($grupo, $bandera);
					} else if ($grupo['cl_total'] > 0) {
						$bandera++;
					}
				}
			} else if (isset($data['children_ledgers']) && count($data['children_ledgers']) > 0) {
				foreach ($data['children_ledgers'] as $key => $ledger) {
					if ($ledger['cl_total'] > 0) {
						$bandera++;
					}
				}
			} else if ($data['cl_total'] > 0) {
				$bandera++;
			}
		}

		return $bandera;
	}

	function bandera_posicion_cuentas($code, $startcode, $endcode){

		$code2 = $code." ";
		$startcode2 = $startcode." ";
		$endcode2 = $endcode." ";

		if ($startcode == 0) {
			if (strcmp($code2, $endcode2) <= 0) {
				return 1;
			} else {
				return 0;
			}
		} else if ($endcode == 0) {
			if (strcmp($code2, $startcode2) >= 0) {
				return 1;
			} else {
				return 0;
			}
		} else if ($startcode != 0 && $endcode != 0) {
			if (strcmp($code2, $startcode2) >= 0 && strcmp($code2, $endcode2) <= 0) {
				return 1;
			} else {
				return 0;
			}
		}

	}

	function print_space($count)
	{
		$html = '';
		for ($i = 1; $i <= $count; $i++) {
			$html .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
		}
		return $html;
	}

	function maxlengthname($name){
		if (strlen($name) > 44) {
			$name = substr($name, 0, 41);
			$name.="...";
		}

		return $name;
	}

  }


$pdf = new PDF('P', 'mm', 'A4');
$pdf->settings = $settings;
$pdf->POST = $POST;
$pdf->AliasNbPages();
$pdf->SetMargins(7, 7);

//INICIO PDF
$pdf->AddPage();
// $this->functionscore->print_arrays($accountlist);
foreach ($accountlist as $dataRow) {
	$pdf->print_account_chart($dataRow, -1, $niveles, (isset($startcode) ? $startcode : ""), (isset($endcode) ? $endcode : ""), $third_account_trial_balance);
}

if (isset($startcode) || isset($endcode)) {
	global $drcalc, $crcalc, $totalParientes;
	$drcalc = 0;
	$crcalc = 0;
	if (isset($totalParientes)) {
		foreach ($totalParientes as $codigo => $total) {
			$drcalc += $total['dr_total'];
			$crcalc += $total['cr_total'];
		}
	}
	$pdf->Cell(73.66, 5 , utf8_decode('Total'),'',0,'L', FALSE);
	$pdf->Cell(22.66, 5 , utf8_decode(''),'',0,'L', FALSE);
	$pdf->Cell(22.66, 5 , utf8_decode(''),'',0,'L', FALSE);
	$pdf->Cell(25.66, 5 , utf8_decode($this->functionscore->toCurrency('D', $drcalc)),'',0,'R', FALSE);
	$pdf->Cell(25.66, 5 , utf8_decode($this->functionscore->toCurrency('C', $crcalc)),'',0,'R', FALSE);
}

if ($descargar) {
  $pdf->Output("BALANCE_PRUEBAS.pdf", "D");
} else {
  $pdf->Output("BALANCE_PRUEBAS.pdf", "I");
}
