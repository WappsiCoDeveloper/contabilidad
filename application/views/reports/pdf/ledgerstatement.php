<?php 
require_once $_SERVER['DOCUMENT_ROOT'].'/contabilidad/vendor/fpdf/fpdf.php';
class PDF extends FPDF
  {
    function Header()
    {
      $this->SetFont('Arial','B',10);
      // $this->Cell(196, 5 , utf8_decode("LIBRO AUXILIAR POR ".( $this->POST['view_type'] == 1 ? "NIT Y CUENTAS" : "CUENTAS Y NIT" )),'',1,'C');
      $this->Cell(196, 5 , utf8_decode(lang('page_title_reports_ledgerstatement')),'',1,'C');
      $this->SetFont('Arial','B',8);
      $this->Cell(32.66, 5, utf8_decode(lang('company')), '', 0, 'C');
      $this->Cell(32.66, 5, utf8_decode($this->settings->name), '', 0, 'C');
      $this->Cell(32.66, 5, utf8_decode(lang('admin_cntrler_create_account_validation_nit')), '', 0, 'C');
      $this->Cell(32.66, 5, utf8_decode($this->settings->nit), '', 1, 'C');
      $this->Cell(32.66, 5, utf8_decode(lang('from')), '', 0, 'C');
      $this->SetFont('Arial','',8);
      $this->Cell(32.66, 5, utf8_decode( ($this->POST['startdate'] != '' ? $this->POST['startdate'] : $this->settings->fy_start)  ), '', 0, 'C');
      $this->SetFont('Arial','B',8);
      $this->Cell(32.66, 5, utf8_decode(lang('to')), '', 0, 'C');
      $this->SetFont('Arial','',8);
      $this->Cell(32.66, 5, utf8_decode(($this->POST['enddate'] != '' ? $this->POST['enddate'] : date('Y-m-d'))), '', 1, 'C');
      $this->SetFont('Arial','B',8);
      $this->Cell(32.66, 5, utf8_decode(lang('expedition_date')), '', 0, 'C');
      $this->SetFont('Arial','',8);
      $this->Cell(32.66, 5, utf8_decode(date('Y-m-d H:i:s')), '', 1, 'C');
      $this->SetFont('Arial','B',8);
      $this->Cell(15.66, 5 , utf8_decode(lang('entries_views_index_th_number')),'LTB',0,'L');
      $this->Cell(22.66, 5 , utf8_decode(lang('entries_views_index_th_date')),'LTB',0,'L');
      // exit(var_dump($_POST['show_base']));
      if (isset($_POST['show_base']) && $_POST['show_base'] == "TRUE") {
        $this->Cell(68.66, 5 , utf8_decode(lang('entries_views_views_th_narration')),'LTB',0,'L');
        $this->Cell(14, 5 , utf8_decode('Base'),'LTB',0,'L');
        $this->Cell(18.66, 5 , utf8_decode(lang('entries_views_add_items_th_dr_amount')),'LTB',0,'L');
        $this->Cell(18.66, 5 , utf8_decode(lang('entries_views_add_items_th_cr_amount')),'LTB',0,'L');
        $this->Cell(20.66, 5 , utf8_decode(lang('entries_views_index_th_total_balance')),'LTBR',0,'L');
      } else {
        $this->Cell(73.66, 5 , utf8_decode(lang('entries_views_views_th_narration')),'LTB',0,'L');
        $this->Cell(20.66, 5 , utf8_decode(lang('entries_views_add_items_th_dr_amount')),'LTB',0,'L');
        $this->Cell(20.66, 5 , utf8_decode(lang('entries_views_add_items_th_cr_amount')),'LTB',0,'L');
        $this->Cell(25.66, 5 , utf8_decode(lang('entries_views_index_th_total_balance')),'LTBR',0,'L');
      }
      
      $this->Cell(17, 5 , utf8_decode(lang('state')),'LTBR',1,'L');
    }

    function crop_text($text, $length = 50){
      if (strlen($text) > $length) {
          $text = substr($text, 0, ($length-5));
          $text.="...";
          return strip_tags($text);
      }
      return strip_tags($text);
  }
  }
$pdf = new PDF('P', 'mm', 'A4');
$pdf->settings = $settings;
$pdf->POST = $POST;
$pdf->AliasNbPages();
$pdf->SetMargins(7, 7);
//INICIO PDF
$pdf->AddPage();
$pdf->SetFont('Arial','',6);
foreach ($datos as $row => $dato) {
  $pdf->Cell(15.66, 5 , utf8_decode(strip_tags($dato['entry_number'])),'B',0,'L', FALSE);
  $pdf->Cell(22.66, 5 , utf8_decode(strip_tags($dato['entry_date'])),'B',0,'L', FALSE);
  if (isset($_POST['show_base']) && $_POST['show_base'] == "TRUE") {
    $pdf->Cell(68.66, 5 , utf8_decode(strip_tags($pdf->crop_text($dato['narration'], 65))),'B',0,'L', FALSE);
    $pdf->Cell(14, 5 , utf8_decode(strip_tags($dato['base'])),'B',0,'R', FALSE);
    $pdf->Cell(18.66, 5 , utf8_decode(strip_tags($dato['amount_D'])),'B',0,'R', FALSE);
    $pdf->Cell(18.66, 5 , utf8_decode(strip_tags($dato['amount_C'])),'B',0,'R', FALSE);
    $pdf->Cell(20.66, 5 , utf8_decode(strip_tags($dato['saldo'])),'B',0,'R', FALSE);
  } else {
    $pdf->Cell(73.66, 5 , utf8_decode(strip_tags($pdf->crop_text($dato['narration'],65))),'B',0,'L', FALSE);
    $pdf->Cell(20.66, 5 , utf8_decode(strip_tags($dato['amount_D'])),'B',0,'R', FALSE);
    $pdf->Cell(20.66, 5 , utf8_decode(strip_tags($dato['amount_C'])),'B',0,'R', FALSE);
    $pdf->Cell(25.66, 5 , utf8_decode(strip_tags($dato['saldo'])),'B',0,'R', FALSE);
  }
  
  $pdf->Cell(17, 5 , utf8_decode(strip_tags($dato['state'])),'B',1,'L', FALSE);
}
if ($descargar) {
  $pdf->Output("LIBRO_NIT_CUENTAS.pdf", "D");
} else {
  $pdf->Output("LIBRO_NIT_CUENTAS.pdf", "I");
}
