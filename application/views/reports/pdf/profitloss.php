<?php 
require_once $_SERVER['DOCUMENT_ROOT'].'/contabilidad/vendor/fpdf/fpdf.php';

//$this->SetTextColor( 255, 0, 0); color rojo
//$this->SetTextColor( 0, 0, 0); color negro

// var_dump($POST);

class PDF extends FPDF
  {

    function Header()
    {
      $this->SetFont('Arial','B',10);
      $this->Cell(196, 5 , utf8_decode(lang('page_title_reports_profitloss')),'',1,'C');
      $this->SetFont('Arial','B',8);
      $this->Cell(32.66, 5, utf8_decode(lang('company')), '', 0, 'C');
      $this->Cell(32.66, 5, utf8_decode($this->settings->name), '', 0, 'C');
      $this->Cell(32.66, 5, utf8_decode(lang('admin_cntrler_create_account_validation_nit')), '', 0, 'C');
      $this->Cell(32.66, 5, utf8_decode($this->settings->nit), '', 1, 'C');
      $this->Cell(32.66, 5, utf8_decode(lang('from')), '', 0, 'C');
      $this->SetFont('Arial','',8);
      $this->Cell(32.66, 5, utf8_decode( $this->settings->fy_start ), '', 0, 'C');
      $this->SetFont('Arial','B',8);
      $this->Cell(32.66, 5, utf8_decode(lang('to')), '', 0, 'C');
      $this->SetFont('Arial','',8);
      $this->Cell(32.66, 5, utf8_decode( ($this->POST['enddate'] != '' ? $this->POST['enddate'] : date('Y-m-d')) ), '', 1, 'C');
      $this->SetFont('Arial','B',8);
      $this->Cell(32.66, 5, utf8_decode(lang('expedition_date')), '', 0, 'C');
      $this->SetFont('Arial','',8);
      $this->Cell(32.66, 5, utf8_decode(date('Y-m-d H:i:s')), '', 1, 'C');
      $this->SetFont('Arial','B',8);

      $this->Cell(73, 5 , utf8_decode(lang('balance_sheet_gross_expenses')),'LTB',0,'L');
      $this->Cell(25, 5 , utf8_decode(lang('amount')." (".$this->settings->currency_symbol.")"),'LTB',0,'L');
      $this->Cell(73, 5 , utf8_decode(lang('balance_sheet_loe')),'LTB',0,'L');
      $this->Cell(25, 5 , utf8_decode(lang('amount')." (".$this->settings->currency_symbol.")"),'LTBR',1,'L');
    }

	function account_st_short($account, $c = 0, $dc_type = NULL, $niveles = NULL, $cntColumn = NULL, $array = array(), $cntRow = 0)
	{
		$account = isset($account[0]) ? $account[0] : $account;
		if (empty($account)) {
			return [];
		}
		if (isset($_POST['nivelBalancesheet'])) {
			$nivelBalancesheet = $_POST['nivelBalancesheet'];
		} else {
			$nivelBalancesheet = "";
		}


		$bandera = 0;

		if ($nivelBalancesheet != "") {
			for ($i=1; $i <= $nivelBalancesheet ; $i++) { 
				if (in_array($account['code'], $niveles[$i])) {
					$bandera++;
				}
			}
		} else {
			$bandera++;
		}

		$clBandera = $this->bandera_closing_balance($account);

	  	$CI =& get_instance();
		$counter = $c;
		if (strlen($account['code']) > 0 && $bandera != 0 && ((isset($_POST['hidezero']) && $clBandera != 0) || !isset($_POST['hidezero'])))
		{

			$array[$cntRow][$cntColumn] = $CI->functionscore->toCodeWithName($account['code'], $account['name']);
			$array[$cntRow][$cntColumn+1] = $CI->functionscore->toCurrency(($account['cl_total'] < 0 ? 'C' : 'D'), ($account['cl_total'] < 0 ? $account['cl_total'] * -1 : $account['cl_total']));
			$cntRow++;

		}
		if (isset($account['children_groups'])) {
			foreach ($account['children_groups'] as $id => $data)
			{
				$counter++;
				$array = $this->account_st_short($data, $counter, $dc_type, $niveles, $cntColumn, $array, count($array));
				$counter--;
			}
		}
			
		if (isset($account['children_ledgers']) && count($account['children_ledgers']) > 0 && $nivelBalancesheet == "")
		{
			$counter++;
			foreach ($account['children_ledgers'] as $id => $data)
			{

				if ((isset($_POST['hidezero']) && $data['cl_total'] != 0) || !isset($_POST['hidezero'])) {
					$array[$cntRow][$cntColumn] = $CI->functionscore->toCodeWithName($data['code'], $data['name']);
					$array[$cntRow][$cntColumn+1] = $CI->functionscore->toCurrency(($data['cl_total'] < 0 ? 'C' : 'D'), ($data['cl_total'] < 0 ? $data['cl_total'] * -1 : $data['cl_total']));
					$cntRow++;
				}
			}
		$counter--;
		}

		return $array;

	}

	function bandera_closing_balance($data, $bandera = 0) {
		$data = isset($data[0]) ? $data[0] : $data;
		if (!empty($data)) {
			if (isset($data['children_groups']) && count($data['children_groups']) > 0) {
				foreach ($data['children_groups'] as $id => $grupo) {
					if (isset($grupo['children_groups']) && count($grupo['children_groups']) > 0) {
						$bandera = $this->bandera_closing_balance($grupo, $bandera);
					} else if ($grupo['cl_total'] != 0) {
						$bandera++;
					}
				}
			} else if (isset($data['children_ledgers']) && count($data['children_ledgers']) > 0) {
				foreach ($data['children_ledgers'] as $key => $ledger) {
					if ($ledger['cl_total'] != 0) {
						$bandera++;
					}
				}
			} else if ($data['cl_total'] != 0) {
				$bandera++;
			}
		}

		return $bandera;
	}

	function print_space($count)
	{
		$html = '';
		for ($i = 1; $i <= $count; $i++) {
			$html .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
		}
		return $html;
	}

	function maxlengthname($name){
		if (strlen($name) > 43) {
			$name = substr($name, 0, 40);
			$name.="...";
		}

		return $name;
	}
}

$pdf = new PDF('P', 'mm', 'A4');
$pdf->settings = $settings;
$pdf->POST = $POST;
$pdf->AliasNbPages();
$pdf->SetMargins(7, 7);

//INICIO PDF

$superarray = [];

$gross_expenses = $pdf->account_st_short($pandl['gross_expenses'], $c = -1, 'D', $pandl['niveles'], 0);
$superarray = $gross_expenses;
$gross_income = $pdf->account_st_short($pandl['gross_incomes'], $c = -1, 'C', $pandl['niveles'], 2);
$gross_incomes2 = $pdf->account_st_short($pandl['gross_incomes2'], $c = -1, 'C', $pandl['niveles'], 2); 



foreach ($gross_income as $row => $arr) {
	foreach ($arr as $key => $value) {
		$superarray[$row][$key] = $value; 
	}
}

$rowcnt = count($superarray);

foreach ($gross_incomes2 as $row => $arr) {
	foreach ($arr as $key => $value) {
		$superarray[$rowcnt][$key] = $value; 
	}
	$rowcnt++;
}



if (isset($pandl['gross_incomes3'])) {
	$gross_incomes3 = $pdf->account_st_short($pandl['gross_incomes3'], $c = -1, 'C', $pandl['niveles'], 2);

	$rowcnt = count($superarray);

	foreach ($gross_incomes3 as $row => $arr) {
		foreach ($arr as $key => $value) {
			$superarray[$rowcnt][$key] = $value; 
		}
		$rowcnt++;
	}
}

// exit(var_dump($superarray));

$pdf->AddPage();

foreach ($superarray as $row => $array) {
	$pdf->Cell(73, 5 , utf8_decode(isset($array[0]) ? $pdf->maxlengthname($array[0]) : ""),'B',0,'L');
	$pdf->Cell(25, 5 , utf8_decode(isset($array[1]) ? $array[1] : ""),'B',0,'L');
	$pdf->Cell(73, 5 , utf8_decode(isset($array[2]) ? $pdf->maxlengthname($array[2]) : ""),'B',0,'L');
	$pdf->Cell(25, 5 , utf8_decode(isset($array[3]) ? $array[3] : ""),'B',1,'L');
}

// INICIO

$arrayTotales = [];

$gross_total = $pandl['gross_expense_total'];

$arrayTotales[0][0] = lang('profit_loss_tgi');
$arrayTotales[0][1] = $this->functionscore->toCurrency('D', $pandl['gross_expense_total']);

if ($this->functionscore->calculate($pandl['gross_pl'], 0, '>=')) {
	$arrayTotales[1][0] = lang('profit_loss_gp');
	$arrayTotales[1][1] = $this->functionscore->toCurrency('', $pandl['gross_pl']);
	$gross_total = $this->functionscore->calculate($gross_total, $pandl['gross_pl'], '+');
} else {
	$arrayTotales[1][0] = '';
	$arrayTotales[1][1] = '';
}

$arrayTotales[2][0] = lang('profit_loss_t'); 
$arrayTotales[2][1] = $this->functionscore->toCurrency('D', $gross_total);


/*------*/

$gross_total = $pandl['gross_income_total'];

$arrayTotales[0][2] = lang('profit_loss_tge');
$arrayTotales[0][3] = $this->functionscore->toCurrency('C', $pandl['gross_income_total']);

	if ($this->functionscore->calculate($pandl['gross_pl'], 0, '>=')) {
		$arrayTotales[1][2] = '';
		$arrayTotales[1][3] = '';
	} else {
		$arrayTotales[1][2] = lang('profit_loss_glcd');
		$positive_gross_pl = $this->functionscore->calculate($pandl['gross_pl'], 0, 'n');
		$arrayTotales[1][3] = $this->functionscore->toCurrency('', $positive_gross_pl);
		$gross_total = $this->functionscore->calculate($gross_total, $positive_gross_pl, '+');
	}

$arrayTotales[2][2] = lang('profit_loss_t');
$arrayTotales[2][3] = $this->functionscore->toCurrency('C', $gross_total); 
//FIN

$pdf->ln();

foreach ($arrayTotales as $row => $array) {
		$pdf->Cell(73, 5 , utf8_decode(isset($array[0]) ? $array[0] : ""),'B',0,'L');
		$pdf->Cell(25, 5 , utf8_decode(isset($array[1]) ? $array[1] : ""),'B',0,'L');
		$pdf->Cell(73, 5 , utf8_decode(isset($array[2]) ? $array[2] : ""),'B',0,'L');
		$pdf->Cell(25, 5 , utf8_decode(isset($array[3]) ? $array[3] : ""),'B',1,'L');
}	

$cX = $pdf->getX();
$cY = $pdf->getY();

$pdf->setXY($cX, $cY+20);

$pdf->SetFont('Arial','',12);

if ($this->mAccountSettings->accountant_name) {
	$pdf->Cell(63.34, 5, utf8_decode($this->mAccountSettings->accountant_name),'B',0,'L');
	$pdf->Cell(3, 5, utf8_decode(""),'',0,'L');
}
if ($this->mAccountSettings->fiscal_name) {
	$pdf->Cell(63.34, 5, utf8_decode($this->mAccountSettings->fiscal_name),'B',0,'L');
	$pdf->Cell(3, 5, utf8_decode(""),'',0,'L');
}
if ($this->mAccountSettings->representant_name) {
	$pdf->Cell(64.34, 5, utf8_decode($this->mAccountSettings->representant_name),'B',1,'L');
}

$pdf->SetFont('Arial','B',8);

if ($this->mAccountSettings->accountant_name) {
	$pdf->Cell(63.34, 5, utf8_decode("Contador"),'',0,'L');
	$pdf->Cell(3, 5, utf8_decode(""),'',0,'L');
}
if ($this->mAccountSettings->fiscal_name) {
	$pdf->Cell(63.34, 5, utf8_decode("Fiscal Revisor"),'',0,'L');
	$pdf->Cell(3, 5, utf8_decode(""),'',0,'L');
}
if ($this->mAccountSettings->representant_name) {
	$pdf->Cell(64.34, 5, utf8_decode("Representante legal"),'',1,'L');
}


if ($descargar) {
  $pdf->Output("BALANCE_GENERAL.pdf", "D");
} else {
  $pdf->Output("BALANCE_GENERAL.pdf", "I");
}

//añadir "$this->" donde se llaman funciones dentro de la clase. Revisar columnas
