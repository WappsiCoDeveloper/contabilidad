<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/contabilidad/vendor/fpdf/fpdf.php';

class PDF extends FPDF {
    function Header()
    {
      $this->SetFont('Arial','B',10);
      // $this->Cell(196, 5 , utf8_decode("LIBRO AUXILIAR POR ".( $this->POST['view_type'] == 1 ? "NIT Y CUENTAS" : "CUENTAS Y NIT" )),'',1,'C');
      $this->Cell(196, 5 , utf8_decode(sprintf(lang('thirdsaccount_title'), ($this->POST['view_type'] == 1 ? lang('vat_no_&_accounts') : lang('accounts_&_vat_no') ))),'',1,'C');
      $this->SetFont('Arial','B',8);
      $this->Cell(32.66, 5, utf8_decode(lang('company')), '', 0, 'C');
      $this->Cell(32.66, 5, utf8_decode($this->settings->name), '', 0, 'C');
      $this->Cell(32.66, 5, utf8_decode(lang('admin_cntrler_create_account_validation_nit')), '', 0, 'C');
      $this->Cell(32.66, 5, utf8_decode($this->settings->nit), '', 1, 'C');
      $this->Cell(32.66, 5, utf8_decode(lang('from')), '', 0, 'C');
      $this->SetFont('Arial','',8);
      $this->Cell(32.66, 5, utf8_decode( ($this->POST['startdate'] != '' ? $this->POST['startdate'] : $this->settings->fy_start)  ), '', 0, 'C');
      $this->SetFont('Arial','B',8);
      $this->Cell(32.66, 5, utf8_decode(lang('to')), '', 0, 'C');
      $this->SetFont('Arial','',8);
      $this->Cell(32.66, 5, utf8_decode(($this->POST['enddate'] != '' ? $this->POST['enddate'] : date('Y-m-d'))), '', 1, 'C');
      $this->SetFont('Arial','B',8);
      $this->Cell(32.66, 5, utf8_decode(lang('expedition_date')), '', 0, 'C');
      $this->SetFont('Arial','',8);
      $this->Cell(32.66, 5, utf8_decode(date('Y-m-d H:i:s')), '', 1, 'C');
      $this->SetFont('Arial','B',8);

      if (isset($_POST['show_base']) && $_POST['show_base'] == "TRUE") {
        $this->Cell(22.66, 5 , utf8_decode(lang('entries_views_index_th_number')),'LTB',0,'L');
        $this->Cell(22.66, 5 , utf8_decode(lang('entries_views_index_th_date')),'LTB',0,'L');
        $this->Cell(73.66, 5 , utf8_decode(lang('entries_views_views_th_narration')),'LTB',0,'L');
        $this->Cell(16   , 5 , utf8_decode('Base'),'LTB',0,'L');
        $this->Cell(17.66, 5 , utf8_decode(lang('entries_views_add_items_th_dr_amount')),'LTB',0,'L');
        $this->Cell(17.66, 5 , utf8_decode(lang('entries_views_add_items_th_cr_amount')),'LTB',0,'L');
        $this->Cell(25.66, 5 , utf8_decode(lang('entries_views_index_th_total_balance')),'LTBR',1,'L');
      } else {
        $this->Cell(22.66, 5 , utf8_decode(lang('entries_views_index_th_number')),'LTB',0,'L');
        $this->Cell(22.66, 5 , utf8_decode(lang('entries_views_index_th_date')),'LTB',0,'L');
        $this->Cell(73.66, 5 , utf8_decode(lang('entries_views_views_th_narration')),'LTB',0,'L');
        $this->Cell(25.66, 5 , utf8_decode(lang('entries_views_add_items_th_dr_amount')),'LTB',0,'L');
        $this->Cell(25.66, 5 , utf8_decode(lang('entries_views_add_items_th_cr_amount')),'LTB',0,'L');
        $this->Cell(25.66, 5 , utf8_decode(lang('entries_views_index_th_total_balance')),'LTBR',1,'L');
      }
    }

  }


$pdf = new PDF('P', 'mm', 'A4');
$pdf->settings = $settings;
$pdf->POST = $POST;
$pdf->AliasNbPages();
$pdf->SetMargins(7, 7);

//INICIO PDF
$pdf->AddPage();




foreach ($datos as $row => $dato) {
  if ($dato['level'] == 1) {
    $pdf->SetFont('Arial','B',8);
    $pdf->SetFillColor(235, 235, 235);
  } else if ($dato['level'] == 2) {
    $pdf->SetFont('Arial','B',8);
    $pdf->SetFillColor(245, 245, 245);
  } else {
    $pdf->SetFont('Arial','',8);
    $pdf->SetFillColor(255, 255, 255);
  }

  if (isset($_POST['show_base']) && $_POST['show_base'] == "TRUE") {
    $pdf->Cell(22.66, 5 , utf8_decode(strip_tags($dato['entry_number'])),'LTB',0,'L', TRUE);
    $pdf->Cell(22.66, 5 , utf8_decode(strip_tags($dato['entry_date'])),'LTB',0,'L', TRUE);
    $pdf->Cell(73.66, 5 , utf8_decode(strip_tags($dato['narration'])),'LTB',0,'L', TRUE);
    $pdf->Cell(16   , 5 , utf8_decode(strip_tags($dato['base'])),'LTB',0,'R', TRUE);
    $pdf->Cell(17.66, 5 , utf8_decode(strip_tags($dato['amount_D'])),'LTB',0,'R', TRUE);
    $pdf->Cell(17.66, 5 , utf8_decode(strip_tags($dato['amount_C'])),'LTB',0,'R', TRUE);
    $pdf->Cell(25.66, 5 , utf8_decode(strip_tags($dato['saldo'])),'LTBR',1,'R', TRUE);
  } else {
    $pdf->Cell(22.66, 5 , utf8_decode(strip_tags($dato['entry_number'])),'LTB',0,'L', TRUE);
    $pdf->Cell(22.66, 5 , utf8_decode(strip_tags($dato['entry_date'])),'LTB',0,'L', TRUE);
    $pdf->Cell(73.66, 5 , utf8_decode(strip_tags($dato['narration'])),'LTB',0,'L', TRUE);
    $pdf->Cell(25.66, 5 , utf8_decode(strip_tags($dato['amount_D'])),'LTB',0,'R', TRUE);
    $pdf->Cell(25.66, 5 , utf8_decode(strip_tags($dato['amount_C'])),'LTB',0,'R', TRUE);
    $pdf->Cell(25.66, 5 , utf8_decode(strip_tags($dato['saldo'])),'LTBR',1,'R', TRUE);
  }
        
}

if ($descargar) {
  $pdf->Output("LIBRO_NIT_CUENTAS.pdf", "D");
} else {
  $pdf->Output("LIBRO_NIT_CUENTAS.pdf", "I");
}
