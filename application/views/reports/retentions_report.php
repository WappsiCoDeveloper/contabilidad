<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/contabilidad/vendor/fpdf182/fpdf.php';

$pdf = new FPDF('P', 'mm', 'A4');

$table_headers = [
	lang("certificate_withholdings_label_document_type"),
	lang("certificate_withholdings_label_document_number"),
	lang("certificate_withholdings_label_date"),
	lang("certificate_withholdings_label_concept"),
	lang("certificate_withholdings_label_base"),
	lang("certificate_withholdings_label_retention"),
	lang("certificate_withholdings_label_percentage"),
];
$table_column_width = [15, 20, 18, 77, 26, 26, 8];

$pdf->SetTitle("Informe de retenciones");
$pdf->AliasNbPages();
$pdf->SetAutoPageBreak(TRUE, 7);
$pdf->AddPage();

if (!empty($retention_data)) {
	foreach ($retention_data as $retention) {
		$retention_information_order_array[$retention->tipo_retencion][$retention->codigo_cuenta. " - ". $retention->nombre_cuenta][$retention->NIT_CC. " - ".$retention->compania][] = $retention;
	}

	$total_general_bases = $total_general_retention = 0;
	foreach ($retention_information_order_array as $type_retention => $retention_type_retention_array) {
		$pdf->SetFont('Arial','B',8);
      	$pdf->SetDrawColor(58, 132, 197);
	    $pdf->SetTextColor(255, 255, 255);
	    $pdf->SetFillColor(58, 132, 197);
	    $pdf->Cell(0, 10 , utf8_decode("RETENCIÓN EN ". $type_retention), 1, 1, 'L', TRUE);

		$total_type_retention_bases = $total_type_retention_retention = 0;
		foreach ($retention_type_retention_array as $accouting_account => $retention_account_array) {
			$pdf->SetFont('Arial','B',8);
			$pdf->SetDrawColor(217, 237, 247);
			$pdf->SetTextColor(103, 106, 108);
			$pdf->SetFillColor(217, 237, 247);
			$pdf->Cell(0, 10 , utf8_decode($accouting_account),1,1, 'L', TRUE);

			$total_account_bases = $total_account_retention = 0;
			foreach ($retention_account_array as $NIT_CC => $retention_third_party_array) {

				$pdf->SetFont('Arial','B',8);
				$pdf->SetDrawColor(245, 245, 245);
				$pdf->SetTextColor(103, 106, 108);
				$pdf->SetFillColor(245, 245, 245);
				$pdf->Cell(0, 8, utf8_decode($NIT_CC),1, 1, 'L', TRUE);

				$pdf->Ln(1);
				for ($i=0;$i<count($table_headers);$i++) {
					$pdf->SetDrawColor(103, 106, 108);
					$pdf->SetFont('Arial','B',8);
				    $pdf->Cell($table_column_width[$i],7,utf8_decode($table_headers[$i]),"B",0, "C");
				}
				$pdf->Ln();

				$total_third_party_bases = $total_third_party_retention = 0;
				foreach ($retention_third_party_array as $data) {
					$percentage = 0;

					if ($data->base > 0) {
						$percentage = number_format(($data->retencion / $data->base) * 100, 2, ',', '.');
					}

					$pdf->SetDrawColor(103, 106, 108);
					$pdf->SetFont('Arial','',8);

					$pdf->Cell($table_column_width[0], 7, utf8_decode($data->tipo_documento), "B", 0, 'C');
					$pdf->Cell($table_column_width[1], 7, utf8_decode($data->numero_documento), "B", 0, 'C');
					$pdf->Cell($table_column_width[2], 7, utf8_decode($data->fecha_documento), "B", 0, 'C');
					$pdf->Cell($table_column_width[3], 7, utf8_decode(substr(" ". $data->concepto, 0, 48)), "B", 0, 'L');
					$pdf->Cell($table_column_width[4], 7, number_format($data->base, 2, ",", "."), "B", 0, 'R');
					$pdf->Cell($table_column_width[5], 7, number_format($data->retencion, 2, ",", "."), "B", 0, 'R');
					$pdf->Cell($table_column_width[6], 7, utf8_decode($percentage), "B", 1, 'R');

					if ($pdf->getY() > 281) {
						$pdf->SetFont('Arial','B',8);
						$pdf->SetDrawColor(217, 237, 247);
						$pdf->SetTextColor(103, 106, 108);
						$pdf->SetFillColor(217, 237, 247);
						$pdf->Cell(0, 10 , utf8_decode($accouting_account),1,1, 'L', TRUE);

						$pdf->SetFont('Arial','B',8);
						$pdf->SetDrawColor(245, 245, 245);
						$pdf->SetTextColor(103, 106, 108);
						$pdf->SetFillColor(245, 245, 245);
						$pdf->Cell(0, 8, utf8_decode($NIT_CC),1, 1, 'L', TRUE);

						for ($i=0;$i<count($table_headers);$i++) {
							$pdf->SetDrawColor(103, 106, 108);
							$pdf->SetFont('Arial','B',8);
						    $pdf->Cell($table_column_width[$i],7,utf8_decode($table_headers[$i]),"B",0,'C');
						}
						$pdf->Ln();
					}

					$total_third_party_bases += (float) $data->base;
					$total_third_party_retention += (float) $data->retencion;
				}

				$pdf->SetFillColor(235, 235, 235);
				$pdf->SetDrawColor(235, 235, 235);
				$pdf->SetFont('Arial','B',8);
				$pdf->Cell(($table_column_width[0]+$table_column_width[1]+$table_column_width[2]+$table_column_width[3]), 7, "TOTAL TERCERO " . $NIT_CC, "B", 0, 'R', TRUE);
				$pdf->Cell($table_column_width[4], 7, number_format($total_third_party_bases, 2, ",", "."), "B", 0, 'R', TRUE);
				$pdf->Cell($table_column_width[5], 7, number_format($total_third_party_retention, 2, ",", "."), "B", 0, 'R', TRUE);
				$pdf->Cell($table_column_width[6], 7, "", "B", 1, 'R', TRUE);
				$pdf->Ln(1);

				$total_account_bases += $total_third_party_bases;
				$total_account_retention += $total_third_party_retention;
			}

			$pdf->SetFillColor(235, 235, 235);
			$pdf->SetDrawColor(235, 235, 235);
			$pdf->SetFont('Arial','B',8);
			$pdf->Cell(($table_column_width[0]+$table_column_width[1]+$table_column_width[2]+$table_column_width[3]), 7, utf8_decode("TOTAL CUENTA " . $accouting_account), "B", 0, 'R', TRUE);
			$pdf->Cell($table_column_width[4], 7, number_format($total_account_bases, 2, ",", "."), "B", 0, 'R', TRUE);
			$pdf->Cell($table_column_width[5], 7, number_format($total_account_retention, 2, ",", "."), "B", 0, 'R', TRUE);
			$pdf->Cell($table_column_width[6], 7, "", "B", 1, 'R', TRUE);
			$pdf->Ln(1);

			$total_type_retention_bases += $total_account_bases;
			$total_type_retention_retention += $total_account_retention;
		}

		$pdf->SetFillColor(235, 235, 235);
		$pdf->SetDrawColor(235, 235, 235);
		$pdf->SetFont('Arial','B',8);
		$pdf->Cell(($table_column_width[0]+$table_column_width[1]+$table_column_width[2]+$table_column_width[3]), 7, utf8_decode("TOTAL ". $type_retention), "B", 0, 'R', TRUE);
		$pdf->Cell($table_column_width[4], 7, number_format($total_type_retention_bases, 2, ",", "."), "B", 0, 'R', TRUE);
		$pdf->Cell($table_column_width[5], 7, number_format($total_type_retention_retention, 2, ",", "."), "B", 0, 'R', TRUE);
		$pdf->Cell($table_column_width[6], 7, "", "B", 1, 'R', TRUE);
		$pdf->Ln(5);

		$total_general_bases += $total_type_retention_bases;
		$total_general_retention += $total_type_retention_retention;
	}

	$pdf->SetFillColor(235, 235, 235);
	$pdf->SetDrawColor(235, 235, 235);
	$pdf->SetFont('Arial','B',8);
	$pdf->Cell(($table_column_width[0]+$table_column_width[1]+$table_column_width[2]+$table_column_width[3]), 7, utf8_decode("TOTAL GENERAL"), "B", 0, 'R', TRUE);
	$pdf->Cell($table_column_width[4], 7, number_format($total_general_bases, 2, ",", "."), "B", 0, 'R', TRUE);
	$pdf->Cell($table_column_width[5], 7, number_format($total_general_retention, 2, ",", "."), "B", 0, 'R', TRUE);
	$pdf->Cell($table_column_width[6], 7, "", "B", 1, 'R', TRUE);
	$pdf->Ln(5);
}


$pdf->Output("Informe de retenciones.pdf", "I");