<script type="text/javascript">

$(document).ready(function() {

	$("#accordion").accordion({
		collapsible: true,
		<?php
			if (isset($options) && $options == false) {
				echo 'active: false';
			}
		?>
	});

	/* Calculate date range in javascript */
	startDate = new Date(<?php echo strtotime($this->mAccountSettings->fy_start) * 1000; ?>  + (new Date().getTimezoneOffset() * 60 * 1000));
	endDate = new Date(<?php echo strtotime($this->mAccountSettings->fy_end) * 1000; ?>  + (new Date().getTimezoneOffset() * 60 * 1000));

	$(document.body).on("change","#ReportLedgerId",function(){
		if(this.value == 0){
			$('#ReportStartdate').prop('disabled', true);
			$('#ReportEnddate').prop('disabled', true);
		} else {
			$('#ReportStartdate').prop('disabled', false);
			$('#ReportEnddate').prop('disabled', false);
		}
	});
	$('#ReportLedgerId').trigger('change');

	/* Setup jQuery datepicker ui */
	$('#ReportStartdate').datepicker({
		minDate: startDate,
		maxDate: endDate,
		dateFormat: '<?php echo $this->mDateArray[1]; ?>',
		numberOfMonths: 1,
		monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Juv', 'Vie', 'Sáb'],
        dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],
		onClose: function(selectedDate) {
				$("#ReportStartdate").datepicker("option", "minDate", startDate).datepicker("option", "maxDate", endDate);
				$("#ReportEnddate").datepicker("option", "minDate", startDate).datepicker("option", "maxDate", endDate);
		},
		onBlur: function(selectedDate) {
				$("#ReportStartdate").datepicker("option", "minDate", startDate).datepicker("option", "maxDate", endDate);
				$("#ReportEnddate").datepicker("option", "minDate", startDate).datepicker("option", "maxDate", endDate);
		}
	});
	$('#ReportEnddate').datepicker({
		minDate: startDate,
		maxDate: endDate,
		dateFormat: '<?php echo $this->mDateArray[1]; ?>',
		numberOfMonths: 1,
		monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Juv', 'Vie', 'Sáb'],
        dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],
		onClose: function(selectedDate) {
				$("#ReportStartdate").datepicker("option", "minDate", startDate).datepicker("option", "maxDate", endDate);
				$("#ReportEnddate").datepicker("option", "minDate", startDate).datepicker("option", "maxDate", endDate);
		},
		onBlur: function(selectedDate) {
				$("#ReportStartdate").datepicker("option", "minDate", startDate).datepicker("option", "maxDate", endDate);
				$("#ReportEnddate").datepicker("option", "minDate", startDate).datepicker("option", "maxDate", endDate);
		}
	});

	$("#ReportLedgerId").select2({
		width:'100%',
		templateResult: formatOutput
	});

	function formatOutput (optionElement) {
	  if (!optionElement.id) { return optionElement.text; }
	  if (optionElement.element.value < 0) {
	  	var $state = $('<span>' + optionElement.text + '</span>');
	  } else {
	  	var $state = $('<span><strong>' + optionElement.text + '</strong></span>');
	  }
	  return $state;
	};

	$(document).on('keypress', 'form', function(e){   
        if(e == 13){
          return false;
        }
      });

    $(document).on('keypress', 'input', function(e){
        if(e.which == 13){
          return false;
        }
      });

});

$(document).on('click', '.export', function(){

	type_export = $(this).data('export');

	if (type_export == "xls") {
		$('#formledgerstatement').attr('action', 'reports/getEntries/download/xls').attr('target', '_blank').submit();
	} else if (type_export == "pdf") {
		$('#formledgerstatement').attr('action', 'reports/getEntries/download/pdf').attr('target', '_blank').submit();
	} else if (type_export == "pdfprint") {
		$('#formledgerstatement').attr('action', 'reports/getEntries/download/pdfprint').attr('target', '_blank').submit();
	}
	
	setTimeout(function() {
		resetear_action();
	}, 5000);

});

function resetear_action(){
	$('#formledgerstatement').attr('action', 'reports/ledgerstatement').attr('target', '_self');
}
</script>
<style type="text/css">
	.ledger{
		background-color: #ddd;
    	color: #333;
	}

	.table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
	    padding: 3px;
	}
</style>
	<div class="row wrapper border-bottom white-bg page-heading">
	  <div class="col-lg-8">
	    <h2><?php echo $page_title; ?></h2>
	    <ol class="breadcrumb">
	      <li>
	        <a href="login/index">Inicio</a> <?php //En el controlador en la función llamada, se revisa si el usuario está logueado y lo envía a la vista principal según permisos ?>
	      </li>
	      <li class="active">
	        <strong><?php echo $page_title; ?></strong>
	      </li>
	    </ol>
	  </div><!-- /.col -->
	  <div class="col-lg-4">
	  </div>
	</div><!-- /.row -->
    <div class="row wrapper wrapper-content animated fadeInRight">
      <div class="box float-e-margins">
        <div class="ibox-content contentBackground">
          <div class="col-sm-12">
            <div class="box-body">
					<!-- <div id="accordion"> -->
						<!-- <h3>Options</h3> -->
				<div class="balancesheet form">
					<?php echo form_open('', array('id' => 'formledgerstatement')); ?>
    				<input type="hidden" name="op_balance_amount" id="op_balance_amount" value="<?= (isset($op['amount'])) ? $op['amount'] : 0 ?>">
    				<input type="hidden" name="op_balance_dc" id="op_balance_dc" value="<?= (isset($op['dc'])) ? $op['dc'] : "" ?>">
					<div class="row">
						<div class="col-sm-3 form-group">
							<label>Desde : </label>
							<select class="ledger-dropdown form-control" name="groupfrom" id="groupfrom" <?= (isset($_POST['nivelBalancesheet']) && $_POST['nivelBalancesheet'] != "") ? "disabled='disabled'" : "" ?>>
										<option value=""><?= lang('select') ?></option>
										<?php foreach ($ledgers as $ledger): ?>
											<option value="<?= $ledger->id ?>" <?= isset($_POST['groupfrom']) && $ledger->id == $_POST['groupfrom'] ? 'selected="selected" ' : '' ?> > <?= $ledger->code ?> - <?= $ledger->name ?></option>
										<?php endforeach; ?>
							</select>
						</div>
						<div class="col-sm-3 form-group">
							<label>Hasta : </label>
							<select class="ledger-dropdown form-control" name="groupto" id="groupto" <?= (isset($_POST['nivelBalancesheet']) && $_POST['nivelBalancesheet'] != "") ? "disabled='disabled'" : "" ?>>
										<option value=""><?= lang('select') ?></option>
										<?php foreach ($ledgers as $ledger): ?>
											<option value="<?= $ledger->id ?>" <?= isset($_POST['groupto']) && $ledger->id == $_POST['groupto'] ? 'selected="selected" ' : '' ?> > <?= $ledger->code ?> - <?= $ledger->name ?></option>
										<?php endforeach; ?>
							</select>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label><?= lang('start_date'); ?></label>
			                    <div class="input-group">
									<input id="ReportStartdate" type="text" name="startdate" value="<?= (isset($_POST['startdate'])) ? $_POST['startdate'] : "" ;  ?>" class="form-control">
			                        <div class="input-group-addon">
			                            <i>
			                                <div class="fa fa-info-circle" data-toggle="tooltip" title="<?= lang('start_date_span') ;?>">
			                                </div>
			                            </i>
			                        </div>
			                    </div>
			                </div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label><?= lang('end_date') ;?></label>

			                    <div class="input-group">
									<input id="ReportEnddate" type="text" name="enddate" value="<?= (isset($_POST['enddate'])) ? $_POST['enddate'] : "" ;  ?>" class="form-control">
			                        <div class="input-group-addon">
			                            <i>
			                                <div class="fa fa-info-circle" data-toggle="tooltip" title="<?= lang('end_date_span') ;?>">
			                                </div>
			                            </i>
			                        </div>
			                    </div>
			                    <!-- /.input group -->
			                </div>
			                <!-- /.form group -->
						</div>
						<div class="col-md-4">
							<div class="form-group">
							<label><?= lang('entry_type_select') ?></label>
								<select class="form-control ledger-dropdown" id="entry_type" name="entry_type">
									<option value=""><?= lang('entry_type_all') ?></option>
									<?php foreach ($tipo_notas as $id => $entrytype): ?>
										<option value="<?= $entrytype['id'] ?>" <?= (isset($_POST['entry_type']) && $_POST['entry_type'] == $entrytype['id']) ? "selected='selected'" : "" ?>><?= $entrytype['name'] ?></option>
									<?php endforeach; ?>
								</select>
							</div>
						</div>
						<?php if (isset($cost_centers) && $cost_centers): ?>
							<div class="col-md-3">
								<label for="cost_center"><?= lang('cost_center_label'); ?></label>
								<select class="form-control ledger-dropdown" id="cost_center" name="cost_center">
									<option value="">Todos</option>
									<?php foreach ($cost_centers as $cost_center): ?>
										<option value="<?= $cost_center['id'] ?>" <?= (isset($_POST['cost_center']) &&  $_POST['cost_center'] == $cost_center['id']) ? "selected='true'" : "" ?>><?= $cost_center['name'] ?></option>
									<?php endforeach ?>
								</select>
							</div>
						<?php endif ?>
						<div class="col-md-3">
							<div style="margin-top: 30px;margin-bottom: 30px;">
								<input type="checkbox" id="skip_entries_disapproved" name="skip_entries_disapproved" class="checkbox skip" <?= (isset($_POST['skip_entries_disapproved'])) ? "checked" :"" ?> value='TRUE'>
								<label for="skip_entries_disapproved"><?= lang('skip_entries_disapproved'); ?></label>
	                        </div>
						</div>
						<div class="col-md-3">
							<div style="margin-top: 30px;margin-bottom: 30px;">
								<input type="checkbox" id="show_base" name="show_base" class="checkbox skip" <?= (isset($_POST['show_base'])) ? "checked" :"" ?> value='TRUE'>
								<label for="show_base"><?= lang('show_base'); ?></label>
	                        </div>
						</div>
						<div class="col-md-12">
							<!-- <input type="reset" name="reset" class="btn btn-default" style="margin-left: 5px;" value="<?= lang('clear'); ?>"> -->
							<input type="submit" name="submit" class="btn btn-success pull-right" value="<?=lang('create_account_submit_button');?>">
							<?php
								if ($this->input->post('ledger_id')){
									$get = $this->input->post('ledger_id');

									if ($this->input->post('startdate')) {
										$get .= "?startdate=". $this->input->post('startdate');

									}
									if ($this->input->post('enddate')) {
										$get .= "&enddate=". $this->input->post('enddate');
									}
							?>
							<?php
								}
							?>
						</div>
					</div>
					<button class="btn btn-default export" data-export="xls">XLS</button>
					<button class="btn btn-default export" data-export="pdf">PDF</button>
					<button class="btn btn-default export" data-export="pdfprint">Imprimir</button>
					<hr class="col-sm-12">
					<?php form_close();  ?>
				</div>
				<?php if ($showEntries) :  ?>
					<table class="table table-stripped" id="ledgertable">
						<thead>
							<tr>
		            			<th ><?= lang('entries_views_index_th_number'); ?></th>
								<th ><?= lang('entries_views_index_th_date'); ?></th>
								<th ><?= lang('entries_views_views_th_narration'); ?></th>
								<?php if (isset($_POST['show_base']) && $_POST['show_base']): ?>
									<th class="text-right"><?= lang('certificate_withholdings_label_base') ?></th>
								<?php endif ?>
								<th class="text-right"><?= lang('entries_views_index_th_total_amount_D'); ?></th>
								<th class="text-right"><?= lang('entries_views_index_th_total_amount_C'); ?></th>
								<th class="text-right"><?= lang('entries_views_index_th_total_balance'); ?></th>
								<th ><?= lang('entries_views_index_th_state'); ?></th>
							</tr>
						</thead>
						<tbody>
						</tbody>
						<tfoot>
							<tr>
		            			<th style="width: 11.11%;"><?= lang('entries_views_index_th_number'); ?></th>
								<th style="width: 11.11%;"><?= lang('entries_views_index_th_date'); ?></th>
								<th style="width: 26.33%;"><?= lang('entries_views_views_th_narration'); ?></th>
								<?php if (isset($_POST['show_base']) && $_POST['show_base']): ?>
									<th><?= lang('certificate_withholdings_label_base') ?></th>
								<?php endif ?>
								<th style="width: 15.72%;"><?= lang('entries_views_index_th_total_amount_D'); ?></th>
								<th style="width: 15.72%;"><?= lang('entries_views_index_th_total_amount_C'); ?></th>
								<th style="width: 16.00%;"><?= lang('entries_views_index_th_total_balance'); ?></th>
								<th style="width: 05.00%;"><?= lang('entries_views_index_th_state'); ?></th>
							</tr>
						</tfoot>
					</table>
				<?php endif; ?>
            </div>
          </div>
      </div>
      <!-- /.row -->
    </div>
    <!-- /.content -->

<?php if (isset($_POST)): ?>
	<script type="text/javascript">

		dataset1 = $('#ledgertable').DataTable({
		    ajax: {
		        method: 'POST',
		        url: '<?=base_url("reports/getEntries"); ?>',
		        data: {
		        	'groupfrom' 				: $('#groupfrom').val(),
		        	'groupto' 					: $('#groupto').val(),
		        	'startdate' 				: $('#ReportStartdate').val(),
		        	'enddate' 					: $('#ReportEnddate').val(),
		        	'skip_entries_disapproved' 	: $('#skip_entries_disapproved').is(':checked') ? "TRUE" : "FALSE",
		        	'op_balance_amount' 		: $('#op_balance_amount').val(),
		        	'op_balance_dc' 			: $('#op_balance_dc').val(),
		        	'entry_type' 				: $('#entry_type').val(),
		        	'cost_center'				: $('#cost_center').val(),
		        	'show_base' 				: $('#show_base').is(':checked') ? "TRUE" : "FALSE"
		        },
		        error: function(data){
		        	console.log(data.responseText);
		        }
		      },
		    columns:[
		        { data: 'entry_number', "bSortable": false},
		        { data: 'entry_date', "bSortable": false},
		        { data: 'narration', "bSortable": false},
				<?php if (isset($_POST['show_base']) && $_POST['show_base']): ?>
		        	{ data: 'base', className: 'text-right', "bSortable": false},
				<?php endif ?>
		        { data: 'amount_D', className: 'text-right', "bSortable": false},
		        { data: 'amount_C', className: 'text-right', "bSortable": false},
		        { data: 'saldo', className: 'text-right', "bSortable": false},
		        { data: 'state', "bSortable": false},
		      ],
	        order: [],
		    pageLength: 100,
		    responsive: true,
		    dom : '<"html5buttons" B>lr<"containerBtn"><"inputFiltro"f>tip',
    		buttons : [
    					{extend:'excel', title:'ledgerstatement', className:'btnExportarExcel', exportOptions: {columns : [2,3,4,5,6,7,8]}},
    					{extend:'pdf', title:'ledgerstatement', className:'btnExportarPdf', exportOptions: {columns : [2,3,4,5,6,7,8]}},
    				   ],
		    oLanguage: {
		      sLengthMenu: 'Mostrando _MENU_ registros por página',
		      sZeroRecords: 'No se encontraron registros',
		      sInfo: 'Mostrando _START_ a _END_ de _TOTAL_ registros',
		      sInfoEmpty: 'Mostrando 0 a 0 de 0 registros',
		      sInfoFiltered: '(Filtrado desde _MAX_ registros)',
		      sSearch:         'Buscar: ',
		      oPaginate:{
		        sFirst:    'Primero',
		        sLast:     'Último',
		        sNext:     'Siguiente',
		        sPrevious: 'Anterior'
		      },
		    },
		    // columnDefs: [
      //               {
      //                   "targets": [0, 1],
      //                   "visible": false,
      //                   "searchable": false
      //               }
      //           ],
            fnRowCallback: function(nRow, aData, iDisplayIndex){
		        nRow.className = 'viewEntry'+(aData['entry_id'] == '' ? ' ledger' : '');
		        nRow.setAttribute('data-entryLabel', aData.entryLabel);
		        nRow.setAttribute('data-entryid', aData.entry_id);
            },
		    "preDrawCallback": function( settings ) {
					$('#loader').fadeIn();
		      }
		    }).on("draw", function(){

		     $('#loader').fadeOut();

		   });

		     $(document).on('click', '.viewEntry', function(){
		    	 entry_type = $(this).data('entrylabel');
		         entry_id = $(this).data('entryid');
		         window.open('<?= base_url("entries/view/") ?>'+entry_type+'/'+entry_id, '_blank');
		    });

		     $(document).ready(function(){
		     	$('.ledger-dropdown').select2({width:'100%'});
		     });
    </script>

<?php endif ?>