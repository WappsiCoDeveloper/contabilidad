<script type="text/javascript">
$(document).ready(function() {

	/* Calculate date range in javascript */
	startDate = new Date(<?php echo strtotime($this->mAccountSettings->fy_start) * 1000; ?>  + (new Date().getTimezoneOffset() * 60 * 1000));
	endDate = new Date(<?php echo strtotime($this->mAccountSettings->fy_end) * 1000; ?>  + (new Date().getTimezoneOffset() * 60 * 1000));

	$('#ReportLedgerId').trigger('change');

	/* Setup jQuery datepicker ui */
	$('#ReportStartdate').datepicker({
		minDate: startDate,
		maxDate: endDate,
		dateFormat: '<?php echo $this->mDateArray[1]; ?>',
		numberOfMonths: 1,
		monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Juv', 'Vie', 'Sáb'],
        dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],
		onClose: function(selectedDate) {
				$("#ReportStartdate").datepicker("option", "minDate", startDate).datepicker("option", "maxDate", endDate);
				$("#ReportEnddate").datepicker("option", "minDate", startDate).datepicker("option", "maxDate", endDate);
		},
		onBlur: function(selectedDate) {
				$("#ReportStartdate").datepicker("option", "minDate", startDate).datepicker("option", "maxDate", endDate);
				$("#ReportEnddate").datepicker("option", "minDate", startDate).datepicker("option", "maxDate", endDate);
		}
	});
	$('#ReportEnddate').datepicker({
		minDate: startDate,
		maxDate: endDate,
		dateFormat: '<?php echo $this->mDateArray[1]; ?>',
		numberOfMonths: 1,
		monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Juv', 'Vie', 'Sáb'],
        dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],
		onClose: function(selectedDate) {
				$("#ReportStartdate").datepicker("option", "minDate", startDate).datepicker("option", "maxDate", endDate);
				$("#ReportEnddate").datepicker("option", "minDate", startDate).datepicker("option", "maxDate", endDate);
		},
		onBlur: function(selectedDate) {
				$("#ReportStartdate").datepicker("option", "minDate", startDate).datepicker("option", "maxDate", endDate);
				$("#ReportEnddate").datepicker("option", "minDate", startDate).datepicker("option", "maxDate", endDate);
		}
	});

	$("#ReportLedgerId").select2({
		width:'100%',
		templateResult: formatOutput
	});

	function formatOutput (optionElement) {
	  if (!optionElement.id) { return optionElement.text; }
	  if (optionElement.element.value < 0) {
	  	var $state = $('<span>' + optionElement.text + '</span>');
	  } else {
	  	var $state = $('<span><strong>' + optionElement.text + '</strong></span>');
	  }
	  return $state;
	};

	$(document).on('keypress', 'form', function(e){   
        if(e == 13){
          return false;
        }
      });

    $(document).on('keypress', 'input', function(e){
        if(e.which == 13){
          return false;
        }
      });

});
</script>

<style type="text/css">
	.level_1{
		background-color: #0000000d;
	}

	.level_1 > td {
	    padding: 9px !important;
	}

	.level_2{
		font-style: italic;
	}

	.level_3{

	}

	.table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
	    padding: 3px;
	}
</style>

<?php 

$group_names_spanish = array('biller' => 'Facturador', 'customer' => 'Cliente', 'Partner' => 'Cliente', 'supplier' => 'Proveedor');


// var_dump($this->mAccountSettings);
 ?>
 	<div class="row wrapper border-bottom white-bg page-heading">
	  <div class="col-lg-8">
	    <h2><?php echo $page_title; ?></h2>
	    <ol class="breadcrumb">
	      <li>
	        <a href="login/index">Inicio</a> <?php //En el controlador en la función llamada, se revisa si el usuario está logueado y lo envía a la vista principal según permisos ?>
	      </li>
	      <li class="active">
	        <strong><?php echo $page_title; ?></strong>
	      </li>
	    </ol>
	  </div><!-- /.col -->
	  <div class="col-lg-4">
	  </div>
	</div><!-- /.row -->
    <div class="row wrapper wrapper-content animated fadeInRight">
      <div class="box float-e-margins">
        <div class="ibox-content contentBackground">
          <div class="col-sm-12">
            <div class="box-body">
            	<input type="hidden" name="op_balance_amount" id="op_balance_amount" value="<?= (isset($op['amount'])) ? $op['amount'] : 0 ?>">
            	<input type="hidden" name="op_balance_dc" id="op_balance_dc" value="<?= (isset($op['dc'])) ? $op['dc'] : "" ?>">
					<!-- <div id="accordion"> -->
						<!-- <h3>Options</h3> -->
						<div class="balancesheet form">
							<?php echo form_open('', array('id' => 'formthirdsaccounts')); ?>
							<div class="row">
								<div class="col-md-4">
									<div class="form-group">
									<label><?= lang('thirdsaccounts_view_type'); ?></label>
										<select class="form-control" id="view_type" name="view_type" required>
											<option value=""><?= lang('payment_methods_add_first_option') ?></option>
											<option value="1" <?= (isset($_POST['view_type']) && $_POST['view_type'] == 1) ? "selected" : "" ?>>Nit y Cuentas</option>
											<option value="2" <?= (isset($_POST['view_type']) && $_POST['view_type'] == 2) ? "selected" : "" ?>>Cuentas y Nit</option>
											<option value="3" <?= (isset($_POST['view_type']) && $_POST['view_type'] == 3) ? "selected" : "" ?>>Horizontal</option>
										</select>
									</div>
								</div>
								<div class="col-sm-4 form-group">
									<label>Desde : </label>
									<select class="ledger-dropdown form-control" name="groupfrom" id="groupfrom" <?= (isset($_POST['nivelBalancesheet']) && $_POST['nivelBalancesheet'] != "") ? "disabled='disabled'" : "" ?>>
										<option value=""><?= lang('select') ?></option>
										<?php foreach ($ledgers as $ledger): ?>
											<option value="<?= $ledger->id ?>" <?= isset($_POST['groupfrom']) && $ledger->id == $_POST['groupfrom'] ? 'selected="selected" ' : '' ?> > <?= $ledger->code ?> - <?= $ledger->name ?></option>
										<?php endforeach; ?>
									</select>
								</div>
								<div class="col-sm-4 form-group">
									<label>Hasta : </label>
									<select class="ledger-dropdown form-control" name="groupto" id="groupto" <?= (isset($_POST['nivelBalancesheet']) && $_POST['nivelBalancesheet'] != "") ? "disabled='disabled'" : "" ?>>
										<option value=""><?= lang('select') ?></option>
										<?php foreach ($ledgers as $ledger): ?>
											<option value="<?= $ledger->id ?>" <?= isset($_POST['groupto']) && $ledger->id == $_POST['groupto'] ? 'selected="selected" ' : '' ?>> <?= $ledger->code ?> - <?= $ledger->name ?></option>
										<?php endforeach; ?>
									</select>
								</div>
								<div class="col-md-4">
									<div class="form-group">
									<label><?= lang('entries_views_views_th_companies_id'); ?></label>
										<select class="form-control ledger-dropdown" id="third_id" name="third_id">
											<option value="0">Todos</option>
											<?php foreach ($terceros as $company): ?>
												<option value="<?= $company['id']; ?>" <?= (isset($_POST['third_id']) && $_POST['third_id'] == $company['id']) ? "selected" : "" ?>><?= '('.( (isset($group_names_spanish[$company['group_name']])) ? $group_names_spanish[$company['group_name']] : $company['group_name']).') '.$company['name'].' - '.$company['vat_no']; ?></option>
											<?php endforeach; ?>
										</select>
									</div>
								</div>
								<div class="col-md-2">
									<div class="form-group">
										<label><?= lang('start_date'); ?></label>
					                    <div class="input-group">
											<input id="ReportStartdate" type="text" name="startdate" value="<?= (isset($_POST['startdate'])) ? $_POST['startdate'] : "" ;  ?>" class="form-control">
					                        <div class="input-group-addon">
					                            <i>
					                                <div class="fa fa-info-circle" data-toggle="tooltip" title="<?= lang('start_date_span') ;?>">
					                                </div>
					                            </i>
					                        </div>
					                    </div>
					                    <!-- /.input group -->
					                </div>
					                <!-- /.form group -->
								</div>
								<div class="col-md-2">
									<div class="form-group">
										<label><?= lang('end_date') ;?></label>

					                    <div class="input-group">
											<input id="ReportEnddate" type="text" name="enddate" value="<?= (isset($_POST['enddate'])) ? $_POST['enddate'] : "" ;  ?>" class="form-control">
					                        <div class="input-group-addon">
					                            <i>
					                                <div class="fa fa-info-circle" data-toggle="tooltip" title="<?= lang('end_date_span') ;?>">
					                                </div>
					                            </i>
					                        </div>
					                    </div>
					                    <!-- /.input group -->
					                </div>
					                <!-- /.form group -->
								</div>
								<div class="col-md-4">
									<div class="form-group">
									<label><?= lang('entry_type_select') ?></label>
										<select class="form-control" id="entry_type" name="entry_type">
											<option value=""><?= lang('entry_type_all') ?></option>
											<?php foreach ($tipo_notas as $id => $entrytype): ?>
												<option value="<?= $entrytype['id'] ?>" <?= (isset($_POST['entry_type']) && $_POST['entry_type'] == $entrytype['id']) ? "selected='selected'" : "" ?>><?= $entrytype['name'] ?></option>
											<?php endforeach; ?>
										</select>
									</div>
								</div>
								<?php if (isset($cost_centers) && $cost_centers): ?>
									<div class="col-md-3">
										<label for="cost_center"><?= lang('cost_center_label'); ?></label>
										<select class="form-control ledger-dropdown" id="cost_center" name="cost_center">
											<option value="">Todos</option>
											<?php foreach ($cost_centers as $cost_center): ?>
												<option value="<?= $cost_center['id'] ?>" <?= (isset($_POST['cost_center']) &&  $_POST['cost_center'] == $cost_center['id']) ? "selected='true'" : "" ?>><?= $cost_center['name'] ?></option>
											<?php endforeach ?>
										</select>
									</div>
								<?php endif ?>
								<div class="col-md-3">
									<div style="margin-top: 30px;margin-bottom: 30px;">
										<input type="checkbox" id="skip_entries_disapproved" name="skip_entries_disapproved" class="checkbox skip" <?= (isset($_POST['skip_entries_disapproved'])) ? "checked" :"" ?> value='TRUE'>
										<label for="skip_entries_disapproved"><?= lang('skip_entries_disapproved'); ?></label>
			                        </div>
								</div>
								<div class="col-md-3">
									<div style="margin-top: 30px;margin-bottom: 30px;">
										<input type="checkbox" id="show_base" name="show_base" class="checkbox skip" <?= (isset($_POST['show_base'])) ? "checked" :"" ?> value='TRUE'>
										<label for="show_base"><?= lang('show_base'); ?></label>
			                        </div>
								</div>

								<div class="col-md-12">
									<!-- <input type="reset" name="reset" class="btn btn-default" style="margin-left: 5px;" value="<?= lang('clear'); ?>"> -->
									<input type="submit" name="submit" class="btn btn-success" value="<?=lang('create_account_submit_button');?>">
									<?php
										if ($this->input->post('ledger_id')){
											$get = $this->input->post('ledger_id');

											if ($this->input->post('startdate')) {
												$get .= "?startdate=". $this->input->post('startdate');

											}
											if ($this->input->post('enddate')) {
												$get .= "&enddate=". $this->input->post('enddate');
											}
									?>
									<?php
										}
									?>
								</div>
							</div>
							<hr class="col-sm-12">
							<?php form_close();  ?>
							<button class="btn btn-default export" data-export="xls">XLS</button>
							<button class="btn btn-default export" data-export="pdf">PDF</button>
							<button class="btn btn-default export" data-export="pdfprint">Imprimir</button>
						</div>
					<!-- </div> -->
				<?php if (isset($_POST) && !empty($_POST)) :  ?>

				<table class="table table-stripped" id="ledgertable">

					<thead>
						<tr>
							<?php if (isset($_POST) && $_POST['view_type'] == 3): ?>
        				<th><?= lang('entries_views_index_th_vat_no'); ?></th>
								<th><?= lang('entries_views_index_th_third'); ?></th>
							<?php endif ?>
        			<th><?= lang('entries_views_index_th_number'); ?></th>
							<th><?= lang('entries_views_index_th_date'); ?></th>
							<th><?= lang('entries_views_views_th_narration'); ?></th>
							<?php if (isset($_POST['show_base']) && $_POST['show_base']): ?>
								<th style="text-align: right;"><?= lang('certificate_withholdings_label_base'); ?></th>
							<?php endif ?>
							<?php if (isset($_POST) && $_POST['view_type'] == 3): ?>
        				<th><?= lang('entries_views_index_th_total_amount_OP'); ?></th>
							<?php endif ?>
							<th style="text-align: right;"><?= lang('entries_views_index_th_total_amount_D'); ?></th>
							<th style="text-align: right;"><?= lang('entries_views_index_th_total_amount_C'); ?></th>
							<th style="text-align: right;"><?= lang('entries_views_index_th_total_balance'); ?></th>
						</tr>
					</thead>

					<tbody>
					
					</tbody>

					<tfoot>
						<tr>
							<?php if (isset($_POST) && $_POST['view_type'] == 3): ?>
        				<th><?= lang('entries_views_index_th_vat_no'); ?></th>
								<th><?= lang('entries_views_index_th_third'); ?></th>
							<?php endif ?>
        			<th style="width: 11.11%;"><?= lang('entries_views_index_th_number'); ?></th>
							<th style="width: 11.11%;"><?= lang('entries_views_index_th_date'); ?></th>
							<th style="width: 31.66%;"><?= lang('entries_views_views_th_narration'); ?></th>
							<?php if (isset($_POST['show_base']) && $_POST['show_base']): ?>
								<th class="text-right"><?= lang('certificate_withholdings_label_base'); ?></th>
							<?php endif ?>
							<?php if (isset($_POST) && $_POST['view_type'] == 3): ?>
        				<th><?= lang('entries_views_index_th_total_amount_OP'); ?></th>
							<?php endif ?>
							<th style="width: 15.36%;" class="text-right"><?= lang('entries_views_index_th_total_amount_D'); ?></th>
							<th style="width: 15.36%;" class="text-right"><?= lang('entries_views_index_th_total_amount_C'); ?></th>
							<th style="width: 15.00%;" class="text-right"><?= lang('entries_views_index_th_total_balance'); ?></th>
						</tr>
					</tfoot>

					</table>

				<?php endif; ?>
            </div>
          </div>
      </div>
      <!-- /.row -->
    </div>
    <!-- /.content -->

<?php if (isset($_POST) && !empty($_POST)): ?>
	<script type="text/javascript">
		 $('#loader').fadeIn();

		dataset1 = $('#ledgertable').DataTable({
		    ajax: {
		        method: 'POST',
		        url: '<?=base_url("reports/getEntriesTA"); ?>',
		        data: {
		        	'groupfrom' : $('#groupfrom').val(),
		        	'groupto' 	: $('#groupto').val(),
		        	'startdate' : $('#ReportStartdate').val(),
		        	'enddate' : $('#ReportEnddate').val(),
		        	'skip_entries_disapproved' : $('#skip_entries_disapproved').is(':checked') ? "TRUE" : "FALSE",
		        	'op_balance_amount' : $('#op_balance_amount').val(),
		        	'op_balance_dc' : $('#op_balance_dc').val(),
		        	'entry_type' : $('#entry_type').val(),
		        	'view_type' : $('#view_type').val(),
		        	'third_id' : $('#third_id').val(),
		        	'cost_center' : $('#cost_center').val(),
		        	'show_base' : $('#show_base').is(':checked') ? "TRUE" : "FALSE"
		        },
		      },
		    columns:[

						<?php if (isset($_POST) && $_POST['view_type'] == 3): ?>
			        { data: 'vat_no'},
			        { data: 'third_name'},
						<?php endif ?>
		        { data: 'entry_number'},
		        { data: 'entry_date'},
		        { data: 'narration'},
				<?php if (isset($_POST['show_base']) && $_POST['show_base']): ?>
		        	{ data: 'base', className: 'text-right', "bSortable": false},
				<?php endif ?>
						<?php if (isset($_POST) && $_POST['view_type'] == 3): ?>
			        { data: 'op_balance'},
						<?php endif ?>
		        { data: 'amount_D', className: 'text-right'},
		        { data: 'amount_C', className: 'text-right'},
		        { data: 'saldo', className: 'text-right'},

		      ],
	        order: [ 2, 'asc' ],
	        ordering: false,
		    pageLength: 100,
		    responsive: true,
		    dom : '<"html5buttons" B>lr<"containerBtn"><"inputFiltro"f>tip',
    		buttons : [
    					{extend:'excel', title:'ledgerstatement', className:'btnExportarExcel', exportOptions: {columns : [2,3,4,5,6,7]}},
    					{extend:'pdf', title:'ledgerstatement', className:'btnExportarPdf', exportOptions: {columns : [2,3,4,5,6,7]}},
    				   ],
		    oLanguage: {
		      sLengthMenu: 'Mostrando _MENU_ registros por página',
		      sZeroRecords: 'No se encontraron registros',
		      sInfo: 'Mostrando _START_ a _END_ de _TOTAL_ registros',
		      sInfoEmpty: 'Mostrando 0 a 0 de 0 registros',
		      sInfoFiltered: '(Filtrado desde _MAX_ registros)',
		      sSearch:         'Buscar: ',
		      oPaginate:{
		        sFirst:    'Primero',
		        sLast:     'Último',
		        sNext:     'Siguiente',
		        sPrevious: 'Anterior'
		      },
		    },
            fnRowCallback: function(nRow, aData, iDisplayIndex){

            	if (aData.level == 1) {
            		nRow.className = 'level_1';
            	} else if (aData.level == 2) {
            		nRow.className = 'level_2';
            	} else if (aData.level == 3) {
            		nRow.className = 'level_3';
            	} else if (aData.level == 4) {
            		nRow.className = 'level_4';
            	}
		        nRow.setAttribute('data-entryLabel', aData.entryLabel);
		        nRow.setAttribute('data-entryid', aData.entry_id);

            },
		    "preDrawCallback": function( settings ) {
		      }
		    }).on("draw", function(){

		     $('#loader').fadeOut();

		        var btnAcciones = '<div class="dropdown pull-right" id="">'+
		        		'<button class="btn btn-primary btn-sm btn-outline" type="button" id="accionesTabla" data-toggle="dropdown" aria-haspopup="true">Acciones<span class="caret"></span></button>'+
		        		'<ul class="dropdown-menu pull-right" aria-labelledby="accionesTabla">'+
		        		'<li><a onclick="$(\'.btnExportarExcel\').click()"><span class="fa fa-file-excel-o"></span> Exportar a XLS </a></li>'+
		        		'<li><a onclick="$(\'.btnExportarPdf\').click()"><span class="fa fa-file-pdf-o"></span> Exportar a PDF </a></li>'+
		        		'</ul></div>';

				$('.containerBtn').html(btnAcciones);

		   });


		    $(document).on('click', '.export', function(){

		    	type_export = $(this).data('export');

		    	if (type_export == "xls") {
		    		$('#formthirdsaccounts').attr('action', 'reports/getEntriesTA/download/xls').attr('target', '_blank').submit();
		    	} else if (type_export == "pdf") {
		    		$('#formthirdsaccounts').attr('action', 'reports/getEntriesTA/download/pdf').attr('target', '_blank').submit();
		    	} else if (type_export == "pdfprint") {
		    		$('#formthirdsaccounts').attr('action', 'reports/getEntriesTA/download/pdfprint').attr('target', '_blank').submit();
		    	}
		    	
		    	setTimeout(function() {
		    		resetear_action();
		    	}, 5000);

		    });

		   function resetear_action(){
		    	$('#formthirdsaccounts').attr('action', 'reports/thirdsaccounts').attr('target', '_self');
		   }
    </script>
<?php else : ?>
	<script type="text/javascript">
		$('#loader').fadeOut();
	</script>
<?php endif ?>


<script type="text/javascript">
	$(document).ready(function(){
		$('.ledger-dropdown').select2({width:'100%'});
	});
</script>
