<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="row wrapper border-bottom white-bg page-heading no-printable">
    <div class="col-lg-8">
        <h2><?= lang('certificate_withholdings_title'); ?></h2>
    </div>
</div>

<div class="wrapper wrapper-content  animated fadeInRight no-printable">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins border-bottom">
                <div class="ibox-title">
                    <h5>Filtrar</h5>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <form action="<?= base_url("reports/certificate_withholdings") ?>" method="post" id="search_form">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="third_party"><?= lang("certificate_withholdings_label_third_party"); ?></label>
                                            <select class="form-control" name="third_party" id="third_party">
                                                <option value=""><?= lang("select") ?></option>
                                                <?php foreach ($thirds_party_options as $third_party) { ?>
                                                    <option value="<?= $third_party->id_compania; ?>" <?= ($this->input->post("third_party") == $third_party->id_compania) ? "selected" : ""; ?>><?= $third_party->nombre_compania; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="note_type_excluded"><?= lang("certificate_withholdings_label_type_note_to_exclude"); ?></label>
                                            <select class="form-control" name="note_type_excluded" id="note_type_excluded">
                                                <option value=""><?= lang("select"); ?></option>
                                                <?php foreach ($note_types as $note_type) { ?>
                                                    <option value="<?= $note_type->id; ?>" <?= ($this->input->post("note_type_excluded") == $note_type->id) ? "selected" : ""; ?>><?= $note_type->name; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="initial_date"><?= lang("certificate_withholdings_label_initial_date"); ?></label>
                                            <input class="form-control" type="date" name="initial_date" id="initial_date" min="<?= $this->mAccountSettings->fy_start; ?>" max="<?= $this->mAccountSettings->fy_end; ?>" value="<?= $this->input->post("initial_date"); ?>">
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="final_date"><?= lang("certificate_withholdings_label_final_date"); ?></label>
                                            <input class="form-control" type="date" name="final_date" id="final_date" min="<?= $this->mAccountSettings->fy_start; ?>" max="<?= $this->mAccountSettings->fy_end; ?>" value="<?= $this->input->post("final_date"); ?>">
                                        </div>
                                    </div>
                                    <?php if (isset($cost_centers) && $cost_centers): ?>
                                        <div class="col-sm-3 form-group">
                                            <div>
                                                <label for="cost_center"><?= lang('cost_center_label'); ?></label>
                                                <select class="form-control ledger-dropdown" id="cost_center" name="cost_center">
                                                    <option value="">Todos</option>
                                                    <?php foreach ($cost_centers as $cost_center): ?>
                                                        <option value="<?= $cost_center['id'] ?>" <?= (isset($_POST['cost_center']) &&  $_POST['cost_center'] == $cost_center['id']) ? "selected='true'" : "" ?>><?= $cost_center['name'] ?></option>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
                                        </div>
                                    <?php endif ?>
                                    <div class="clearfix"></div>
                                </div>
                            </form>
                            <div class="row">
                                <div class="col-sm-12">
                                    <button class="btn btn-primary" type="button" id="search"><span class="fa fa-search"></span> Buscar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php if ($filtered): ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins border-bottom">
                    <div class="ibox-content">
                        <table class="table table table-striped table-condensed toble-hover" id="thirds_table">
                            <thead>
                                <tr>
                                    <th class="text-center" style="padding-right: 8px;"><input type="checkbox" id="main_check"></th>
                                    <th><?= lang("certificate_withholdings_label_company"); ?></th>
                                    <th><?= lang("certificate_withholdings_label_name"); ?></th>
                                    <th><?= lang("certificate_withholdings_label_NIT/CC"); ?></th>
                                    <th class="col-sm-1 text-center" style="padding-right: 8px;">
                                        <?= lang("certificate_withholdings_label_Retefuente"); ?>
                                        <br>
                                        <input class="certificate_main_check" type="checkbox" id="retention_fuente_main_check" data-type_retention="retention_fuente">
                                    </th>
                                    <th class="col-sm-1 text-center" style="padding-right: 8px;">
                                        <?= lang("certificate_withholdings_label_Reteiva"); ?>
                                        <br>
                                        <input class="certificate_main_check" type="checkbox" id="retention_iva_main_check" data-type_retention="retention_iva">
                                    </th>
                                    <th class="col-sm-1 text-center" style="padding-right: 8px;">
                                        <?= lang("certificate_withholdings_label_Reteica"); ?>
                                        <br>
                                        <input class="certificate_main_check" type="checkbox" id="retention_ica_main_check" data-type_retention="retention_ica">
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($thirds_party as $third_party) { ?>
                                    <tr class="third_party_retention_information" data-third_pary_id="<?= $third_party->id_tercero; ?>" data-third_party_document="<?= $third_party->NIT_CC; ?>" data-third_party_name="<?= $third_party->compania; ?>">
                                        <td class="text-center"><input type="checkbox" class="report_check" value="<?= $third_party->NIT_CC; ?>"></td>
                                        <td style="cursor: pointer;"><?= $third_party->compania; ?></td>
                                        <td style="cursor: pointer;"><?= $third_party->nombre; ?></td>
                                        <td style="cursor: pointer;"><?= $third_party->NIT_CC;?></td>
                                        <th class="text-center"><input type="checkbox" class="retention_fuente_check" value="<?= $third_party->NIT_CC; ?>"></th>
                                        <th class="text-center"><input type="checkbox" class="retention_iva_check" value="<?= $third_party->NIT_CC; ?>"></th>
                                        <th class="text-center"><input type="checkbox" class="retention_ica_check" value="<?= $third_party->NIT_CC; ?>"></th>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    <?php endif ?>
        
</div>

<div class="modal fade" id="third_party_retention_information_modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content" id="third_party_retention_information_modal_content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="retention_information_modal_title"></h4>
            </div>

            <div class="modal-body" id="retention_information_modal_body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?= lang("close"); ?></button>
                <a class="btn btn-primary" id="print_button" role="button" target="_blank"><i class="fa fa-print"></i> <?= lang("print"); ?></a>
            </div>
        </div>
    </div>
</div>

<form action="<?= base_url("reports/print_retention_report") ?>" method="post" target="_blank" id="retention_report_form">
    <input type="hidden" name="third_party_ids" id="third_party_ids">
    <input type="hidden" name="note_type_excluded" id="note_type_excluded" value="<?= $this->input->post("note_type_excluded"); ?>">
    <input type="hidden" name="initial_date" id="initial_date" value="<?= $this->input->post("initial_date") ?>">
    <input type="hidden" name="final_date" id="final_date" value="<?= $this->input->post("final_date") ?>">
</form>

<form action="<?= base_url("reports/print_retention_summary_report") ?>" method="post" target="_blank" id="retention_sumary_report_form">
    <input type="hidden" name="third_party_ids" id="third_party_ids">
    <input type="hidden" name="note_type_excluded" id="note_type_excluded" value="<?= $this->input->post("note_type_excluded"); ?>">
    <input type="hidden" name="initial_date" id="initial_date" value="<?= $this->input->post("initial_date") ?>">
    <input type="hidden" name="final_date" id="final_date" value="<?= $this->input->post("final_date") ?>">
</form>

<form action="<?= base_url("reports/print_retention_certificate") ?>" method="post" target="_blank" id="retention_certificate_form">
    <input type="hidden" name="third_party_ids" id="third_party_ids">
    <input type="hidden" name="note_type_excluded" id="note_type_excluded" value="<?= $this->input->post("note_type_excluded"); ?>">
    <input type="hidden" name="retentions_checked" id="retentions_checked">
    <input type="hidden" name="initial_date" id="initial_date" value="<?= $this->input->post("initial_date") ?>">
    <input type="hidden" name="final_date" id="final_date" value="<?= $this->input->post("final_date") ?>">
</form>

<form action="<?= base_url("reports/printRetentionReportExcel") ?>" method="post" target="_blank" id="printRetentionReportExcelForm">
    <input type="hidden" name="third_party_ids" id="third_party_ids">
    <input type="hidden" name="note_type_excluded" id="note_type_excluded" value="<?= $this->input->post("note_type_excluded"); ?>">
    <input type="hidden" name="initial_date" id="initial_date" value="<?= $this->input->post("initial_date") ?>">
    <input type="hidden" name="final_date" id="final_date" value="<?= $this->input->post("final_date") ?>">
</form>

<script>
    $(document).ready(function() {
        load_third_party();

        $(document).on('change', '#initial_date', function () { change_range_dates(); });
        $(document).on('change', '#final_date', function () { change_range_dates(); });
        $(document).on('click', '#search', function () { validate_inputs(); });
        $(document).on('click', '#thirds_table tbody td:nth-child(2), td:nth-child(3), td:nth-child(4)', function () {
            show_retention_information(
                $(this).parent('.third_party_retention_information').data('third_pary_id'),
                $(this).parent('.third_party_retention_information').data('third_party_document'),
                $(this).parent('.third_party_retention_information').data('third_party_name')
            );
        });
        $(document).on('click', '#export_xls', function() { $('.btnExportarExcel').click(); });
        $(document).on('ifToggled', '#main_check', function() { check_uncheck_retention_report(); });
        $(document).on('ifToggled', '.certificate_main_check', function() { check_uncheck_retention_certificate($(this)); });

        $(document).on('click', '#generate_general_withholding_report', function() { generate_general_retention_report(); });
        $(document).on('click', '#printRetentionReportExcel', function() { printRetentionReportExcel(); });
        $(document).on('click', '#generate_general_withholding_summary_report', function() { generate_general_withholding_summary_report(); });
        $(document).on('click', '#generate_withholding_certificates', function() { generate_withholding_certificates(); });
    });

    function check_uncheck_retention_report()
    {
        if ($('#main_check').is(':checked')) {
            $('.report_check').iCheck('check');
        } else {
            $('.report_check').iCheck('uncheck');
        }
    }

    function check_uncheck_retention_fuente_certificate()
    {
        if ($('#retention_fuente_main_check').is(':checked')) {
            $('.retention_fuente_check').iCheck('check');
        } else {
            $('.retention_fuente_check').iCheck('uncheck');
        }
    }

    function check_uncheck_retention_iva_certificate()
    {
        if ($('#retention_iva_main_check').is(':checked')) {
            $('.retention_iva_check').iCheck('check');
        } else {
            $('.retention_iva_check').iCheck('uncheck');
        }
    }

    function check_uncheck_retention_ica_certificate()
    {
        if ($('#retention_ica_main_check').is(':checked')) {
            $('.retention_ica_check').iCheck('check');
        } else {
            $('.retention_ica_check').iCheck('uncheck');
        }
    }

    function check_uncheck_retention_certificate(chech_input)
    {
        var type_retention = chech_input.data('type_retention');

        console.log(type_retention);

        if (chech_input.is(':checked')) {
            $('.'+ type_retention +'_check').iCheck('check');
        } else {
            $('.'+ type_retention +'_check').iCheck('uncheck');
        }
    }

    function load_third_party()
    {
        $('#thirds_table').DataTable({
            pageLength: -1,
            lengthMenu: [ [-1, 10, 50, 100, 500], ["Todo", 10, 50, 100, 500] ],
            responsive: true,
            order : [],
            ordering: false,
            dom : '<"html5buttons" B>lr<"action_button_container"><"inputFiltro"f>tip',
            buttons : [{extend:'excel', title:'Listado de terceros', className:'btnExportarExcel', exportOptions: {columns : [0,1,2,3,4]}}],
            oLanguage: {
                sLengthMenu: 'Mostrando _MENU_ registros por página',
                sZeroRecords: 'No se encontraron registros',
                sInfo: 'Mostrando _START_ a _END_ de _TOTAL_ registros',
                sInfoEmpty: 'Mostrando 0 a 0 de 0 registros',
                sInfoFiltered: '(Filtrado desde _MAX_ registros)',
                sSearch:         'Buscar: ',
                oPaginate:{
                    sFirst:    'Primero',
                    sLast:     'Último',
                    sNext:     'Siguiente',
                    sPrevious: 'Anterior'
                }
            },
            fnDrawCallback: function() {
                $('input[type="checkbox"],[type="radio"]').iCheck({
                    checkboxClass: 'icheckbox_square-blue',
                    radioClass: 'iradio_square-blue'
                });

                check_uncheck_retention_report();
                check_uncheck_retention_fuente_certificate();
                check_uncheck_retention_iva_certificate();
                check_uncheck_retention_ica_certificate();

                var action_button = '<div class="dropdown pull-right">'+
                                        '<button class="btn btn-primary btn-sm" type="button" data-toggle="dropdown" aria-haspopup="true"><?= lang("actions"); ?> <span class="caret"></span></button>'+
                                        '<ul class="dropdown-menu pull-right">'+
                                            '<li><a id="export_xls"><span class="glyphicon glyphicon-export"></span> <?= lang("export_xls") ?> </a></li>'+
                                            '<li><a id="generate_general_withholding_report"><span class="fa fa-file"></span> <?= lang("certificate_withholdings_label_general_report") ?> </a></li>'+
                                            '<li><a id="printRetentionReportExcel"><span class="fa fa-file"></span> <?= lang("certificate_withholdings_label_general_report_excel") ?> </a></li>'+
                                            '<li><a id="generate_general_withholding_summary_report"><span class="fa fa-file"></span> <?= lang("certificate_withholdings_label_summary_report") ?> </a></li>'+
                                            '<li><a id="generate_withholding_certificates"><span class="fa fa-file"></span> <?= lang("certificate_withholdings_label_generate_certificate") ?> </a></li>'+
                                        '</ul>'+
                                    '</div>';
                $('.action_button_container').html(action_button);
            }
        });
    }

    function change_range_dates()
    {
        var initial_date = $('#initial_date').val();
        var final_data = $('#final_date').val();

        if (initial_date != '') {
            $('#final_date').attr('min', initial_date);
        }

        if (final_data != '') {
            $('#initial_date').attr('max', final_data);
        }
    }

    function validate_inputs()
    {
        if ($('#search_form').valid()) {
            $('#search_form').submit();
        }
    }

    function show_retention_information(third_pary_id, third_party_document, third_party_name)
    {
        $.ajax({
            url: '<?= base_url('reports/get_retention_information'); ?>',
            type: 'POST',
            dataType: 'HTML',
            data: {
                '<?= $this->security->get_csrf_token_name(); ?>': '<?= $this->security->get_csrf_hash() ?>',
                'third_party_id': third_party_document,
                'note_type_excluded': '<?= $this->input->post("note_type_excluded"); ?>',
                'initial_date': '<?= $this->input->post("initial_date"); ?>',
                'final_date': '<?= $this->input->post("final_date"); ?>',
                'cost_center': '<?= $this->input->post("cost_center"); ?>'
            },
        })
        .done(function(data) {
            $('#third_party_retention_information_modal').modal('show');

            $('#retention_information_modal_title').html(third_party_document + ' - '+ third_party_name);
            $('#retention_information_modal_body').html(data);
            $('#print_button').attr('href', '<?= base_url("reports/print_retention_report/"); ?>'+ third_party_document + '/'+ '<?= $this->input->post("note_type_excluded"); ?>' +'/'+ '<?= $this->input->post("initial_date"); ?>' +'/'+ '<?= $this->input->post("final_date"); ?>');
        })
        .fail(function(data) {
            console.log(data.responseText);
        });
    }

    function generate_general_retention_report()
    {
        var checked = [];

        $('.report_check').each(function() {
            if ($(this).is(':checked')) {
                checked.push($(this).val());
            }
        });

        if (checked.length == 0) {
            Command: toastr.error('Debe seleccionar al menos un Tercero.', '¡Error de proceso!');

            return false;
        } else {
            $('#retention_report_form #third_party_ids').val(JSON.stringify(checked));
            $('#retention_report_form').submit();
        }
    }

    function printRetentionReportExcel()
    {
        var checked = [];

        $('.report_check').each(function() {
            if ($(this).is(':checked')) {
                checked.push($(this).val());
            }
        });

        if (checked.length == 0) {
            Command: toastr.error('Debe seleccionar al menos un Tercero.', '¡Error de proceso!');

            return false;
        } else {
            console.log(checked)
            $('#printRetentionReportExcelForm #third_party_ids').val(JSON.stringify(checked));
            $('#printRetentionReportExcelForm').submit();
        }
    }

    function generate_general_withholding_summary_report()
    {
        var checked = [];

        $('.report_check').each(function() {
            if ($(this).is(':checked')) {
                checked.push($(this).val());
            }
        });

        if (checked.length == 0) {
            Command: toastr.error('Debe seleccionar al menos un Tercero.', '¡Error de proceso!');

            return false;
        } else {
            $('#retention_sumary_report_form #third_party_ids').val(JSON.stringify(checked));
            $('#retention_sumary_report_form').submit();
        }
    }

    function generate_withholding_certificates()
    {
        var third_party_checked = [];
        var retention_fuente_checked = [];
        var retention_ica_checked = [];
        var retention_iva_checked = [];

        $('.retention_fuente_check').each(function() {
            if ($(this).is(':checked')) {
                third_party_checked.push($(this).val());
                retention_fuente_checked.push($(this).val());
            }
        });

        $('.retention_iva_check').each(function() {
            if ($(this).is(':checked')) {
                third_party_checked.push($(this).val());
                retention_iva_checked.push($(this).val());
            }
        });

        $('.retention_ica_check').each(function() {
            if ($(this).is(':checked')) {
                third_party_checked.push($(this).val());
                retention_ica_checked.push($(this).val());
            }
        });

        if (third_party_checked.length == 0) {
            Command: toastr.error('Debe seleccionar al menos un Tipo de retención para el Tercero deseado.', '¡Error de proceso!');

            return false;
        } else {
            var third_party_checked = third_party_checked.filter(onlyUnique);
            var retentions_checked = {
                'FUENTE': retention_fuente_checked,
                'IVA': retention_iva_checked,
                'ICA': retention_ica_checked
            };

            $('#retention_certificate_form #third_party_ids').val(JSON.stringify(third_party_checked));
            $('#retention_certificate_form #retentions_checked').val(JSON.stringify(retentions_checked));
            $('#retention_certificate_form').submit();
        }
    }

    function onlyUnique(value, index, self) {
        return self.indexOf(value) === index;
    }
</script>