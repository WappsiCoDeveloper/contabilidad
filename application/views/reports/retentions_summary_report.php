<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/contabilidad/vendor/fpdf182/fpdf.php';

$pdf = new FPDF('P', 'mm', 'A4');

// $table_headers = [
// 	lang("certificate_withholdings_label_document_type"),
// 	lang("certificate_withholdings_label_document_number"),
// 	lang("certificate_withholdings_label_date"),
// 	lang("certificate_withholdings_label_concept"),
// 	lang("certificate_withholdings_label_base"),
// 	lang("certificate_withholdings_label_retention"),
// 	lang("certificate_withholdings_label_percentage"),
// ];
$table_column_width = [130, 30, 30];

$pdf->SetTitle("Informe resumido de retenciones");
$pdf->AliasNbPages();
$pdf->SetAutoPageBreak(TRUE, 7);
$pdf->AddPage();

if (!empty($retention_data)) {
	foreach ($retention_data as $retention) {
		$retention_information_order_array[$retention->tipo_retencion][$retention->codigo_cuenta. " - ". $retention->nombre_cuenta][$retention->NIT_CC. " - ".$retention->compania][] = $retention;
	}

	$total_general_bases = $total_general_retention = 0;
	foreach ($retention_information_order_array as $type_retention => $retention_type_retention_array) {
		$pdf->SetFont('Arial','B',8);
      	$pdf->SetDrawColor(255, 255, 255);
	    $pdf->SetTextColor(255, 255, 255);
	    $pdf->SetFillColor(58, 132, 197);
	    $pdf->Cell($table_column_width[0], 10 , utf8_decode("RETENCIÓN EN ". $type_retention), 1, 0, 'L', TRUE);
	    $pdf->Cell($table_column_width[1], 10 , utf8_decode("BASE"), 1, 0, 'C', TRUE);
	    $pdf->Cell($table_column_width[2], 10 , utf8_decode("BASE"), 1, 1, 'C', TRUE);

		$total_type_retention_bases = $total_type_retention_retention = 0;
		foreach ($retention_type_retention_array as $accouting_account => $retention_account_array) {
			$pdf->SetFont('Arial','B',8);
			$pdf->SetDrawColor(255, 255, 255);
			$pdf->SetTextColor(103, 106, 108);
			$pdf->SetFillColor(217, 237, 247);
			$pdf->Cell(0, 10 , utf8_decode(substr($accouting_account, 0, 74)),1,1, 'L', TRUE);

			$total_account_bases = $total_account_retention = 0;
			foreach ($retention_account_array as $NIT_CC => $retention_third_party_array) {
				$total_third_party_bases = $total_third_party_retention = 0;
				foreach ($retention_third_party_array as $data) {
					$total_third_party_bases += (float) $data->base;
					$total_third_party_retention += (float) $data->retencion;
				}

				$pdf->SetFont('Arial','B',8);
				$pdf->SetDrawColor(255, 255, 255);
				$pdf->SetTextColor(103, 106, 108);
				$pdf->SetFillColor(245, 245, 245);
				$pdf->Cell($table_column_width[0], 8, utf8_decode($NIT_CC),1, 0, 'L', TRUE);
				$pdf->Cell($table_column_width[1], 8, number_format($total_third_party_bases, 2, ",", "."), 1, 0, 'R', TRUE);
				$pdf->Cell($table_column_width[2], 8, number_format($total_third_party_retention, 2, ",", "."), 1, 1, 'R', TRUE);
				$pdf->Ln(1);

				$total_account_bases += $total_third_party_bases;
				$total_account_retention += $total_third_party_retention;
			}

			$pdf->SetFillColor(235, 235, 235);
			$pdf->SetDrawColor(255, 255, 255);
			$pdf->SetFont('Arial','B',8);
			$pdf->Cell($table_column_width[0], 7, utf8_decode("TOTAL CUENTA " . $accouting_account), "B", 0, 'R', TRUE);
			$pdf->Cell($table_column_width[1], 7, number_format($total_account_bases, 2, ",", "."), "B", 0, 'R', TRUE);
			$pdf->Cell($table_column_width[2], 7, number_format($total_account_retention, 2, ",", "."), "B", 1, 'R', TRUE);
			$pdf->Ln(1);

			$total_type_retention_bases += $total_account_bases;
			$total_type_retention_retention += $total_account_retention;
		}

		$pdf->SetFillColor(235, 235, 235);
		$pdf->SetDrawColor(255, 255, 255);
		$pdf->SetFont('Arial','B',8);
		$pdf->Cell($table_column_width[0], 7, utf8_decode("TOTAL RETENCIÓN EN ". $type_retention), "B", 0, 'R', TRUE);
		$pdf->Cell($table_column_width[1], 7, number_format($total_type_retention_bases, 2, ",", "."), "B", 0, 'R', TRUE);
		$pdf->Cell($table_column_width[2], 7, number_format($total_type_retention_retention, 2, ",", "."), "B", 1, 'R', TRUE);
		$pdf->Ln(5);

		$total_general_bases += $total_type_retention_bases;
		$total_general_retention += $total_type_retention_retention;
	}

	$pdf->SetFillColor(235, 235, 235);
	$pdf->SetDrawColor(255, 255, 255);
	$pdf->SetFont('Arial','B',8);
	$pdf->Cell($table_column_width[0], 7, utf8_decode("TOTAL GENERAL"), "B", 0, 'R', TRUE);
	$pdf->Cell($table_column_width[1], 7, number_format($total_general_bases, 2, ",", "."), "B", 0, 'R', TRUE);
	$pdf->Cell($table_column_width[2], 7, number_format($total_general_retention, 2, ",", "."), "B", 1, 'R', TRUE);
	$pdf->Ln(5);
}


$pdf->Output("Informe resumido de retenciones.pdf", "I");