<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/contabilidad/vendor/fpdf182/fpdf.php';

class PDF extends FPDF {
	function Header()
	{
	    $this->Image($_SERVER['DOCUMENT_ROOT'] ."/contabilidad/assets/uploads/companies/". $this->logo, 12, 12, 50);

	    $this->SetFont('Arial', 'B', 10);
	    $this->SetTextColor(51, 51, 51);

	    $Y_header = $this->GetY();
	    $this->Cell(55, 20, "", 0, 0);
	    $X_header = $this->GetX();
	    $this->Cell(70, 4, utf8_decode($this->company_name), 0, 2, 'C');
	    $this->Cell(70, 4, utf8_decode($this->document_number ."-". $this->check_digit ." ". $this->type_regimen), 0, 2, 'C');
	    $this->Cell(70, 4, utf8_decode($this->address), 0, 2, 'C');
	    $this->Cell(70, 4, ucwords(strtolower(utf8_decode($this->city ." - ". $this->estate ." - ". $this->country))), 0, 2, 'C');
	    $this->Cell(70, 4, utf8_decode($this->phone . (!empty($this->phone)) ? " - " : "". $this->email), 0, 0, 'C');
	    $this->Ln(5);

	    $this->SetDrawColor(51, 51, 51);
	    $this->Cell(0, 1, "", "T");
	    $this->Ln(10);
	}
}

$pdf = new PDF('P', 'mm', 'A4');
$pdf->document_number = $this->mComercialAccountSettings->numero_documento;
$pdf->check_digit = $this->mComercialAccountSettings->digito_verificacion;
$pdf->type_regimen = ($this->mComercialAccountSettings->tipo_regimen == 1 ? "Régimen Simplificado" : "Régimen Común");
$pdf->company_name = $this->mComercialAccountSettings->razon_social;
$pdf->address = $this->mComercialAccountSettings->direccion;
$pdf->estate = $this->mComercialAccountSettings->departamento;
$pdf->email = $this->mComercialAccountSettings->default_email;
$pdf->country = $this->mComercialAccountSettings->pais;
$pdf->city = $this->mComercialAccountSettings->ciudad;
$pdf->phone = $this->mAccountSettings->phone;
$pdf->logo = $this->mAccountSettings->logo;
$table_headers = [
	lang("certificate_withholdings_label_concept"),
	lang("certificate_withholdings_label_base"),
	lang("certificate_withholdings_label_retention")
];
$table_column_width = [128, 30, 30];


$pdf->SetTitle("Certificado de retenciones");
$pdf->AliasNbPages();
$pdf->SetAutoPageBreak(TRUE, 7);

$pdf->SetTextColor(51, 51, 51);

foreach ($retention_certificate as $third_party => $certificate_third_party_array) {
	$pdf->AddPage();

	$pdf->SetFont('Arial', 'B', 12);
	$pdf->Cell(0, 20, utf8_decode(strtoupper("Certificado de retenciones")), 0, 1, 'C');
	$pdf->SetFont('Arial', 'B', 10);
	$pdf->Cell(0, 20, utf8_decode(strtoupper("De acuerdo con lo establecido en el Artículo 381 del Estatuto Tributario: ")), 0, 1, 'C');
	$pdf->Ln(10);

	// $pdf->SetDrawColor(132, 132, 132);
	// $pdf->SetFillColor(132, 132, 132);


	foreach ($certificate_third_party_array as $type_retention => $retention_data) {
		$pdf->SetFont('Arial', 'B', 12);
		$pdf->Cell(0, 8, utf8_decode(strtoupper("Certificamos")), 0, 1, 'C');
		$pdf->Ln(5);

		$pdf->SetFont('Arial', 'B', 10);
		// $pdf->Cell(0, 8, utf8_decode("Que durante el periodo ". date("Y", strtotime($this->mAccountSettings->fy_start)) . " se efectuó ". strtoupper("RetenciÓn en ". $type_retention) ." a: "), 0, 1, 'C');
		$pdf->Cell(0, 8, utf8_decode("Que durante el periodo $dateRange se efectuó ". strtoupper("RetenciÓn en ". $type_retention) ." a: "), 0, 1, 'C');
		$pdf->Ln(5);

		$pdf->SetFont('Arial', 'B', 12);
		$pdf->Cell(0, 8, utf8_decode(strtoupper($third_party)), 0, 1, 'C');
		$pdf->Ln(10);

		$pdf->SetFont('Arial', '', 10);
		$pdf->Cell(0, 8, utf8_decode("Sobre pagos o abonos en cuenta, por los siguientes conceptos: "), 0, 1, 'C');
		$pdf->Ln(5);

		for ($i=0;$i<count($table_headers);$i++) {
			$pdf->SetDrawColor(103, 106, 108);
			$pdf->SetFont('Arial','B',8);
		    $pdf->Cell($table_column_width[$i],7,utf8_decode($table_headers[$i]),"B",0,'C');
		}
		$pdf->Ln();

		foreach ($retention_data as $account => $data) {
			if ($data["base"] == 0 && $data["retention"] == 0) {

			} else {
				$pdf->SetFont('Arial','',8);
				$pdf->Cell($table_column_width[0], 7, utf8_decode($account), "B", 0, 'L');
				$pdf->Cell($table_column_width[1], 7, utf8_decode(number_format($data["base"], 2 , ",", ".")), "B", 0, 'C');
				$pdf->Cell($table_column_width[2], 7, utf8_decode(number_format($data["retention"], 2 , ",", ".")), "B", 1, 'C');
			}
		}
		$pdf->Ln();

		$pdf->SetFont('Arial', '', 9);
		$pdf->Cell(0, 5, utf8_decode("Esta ". strtoupper("RetenciÓn en ". $type_retention) . " fue pagada a la Administración de impuestos de  ". strtoupper($settings_account->ciudad. ", ". $settings_account->departamento ." - ". $settings_account->pais . ".")), 0, 1);
		$pdf->Cell(0, 5, utf8_decode("Según decreto 380 de 1995 (Parágrafo del Artículo 7), estos certificados no exigen tener la firma autógrafa."), 0, 0);

		$pdf->Ln(20);
	}
}

$pdf->Output("Certificado de retenciones.pdf", "I");