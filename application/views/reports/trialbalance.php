<?php
$totalParientes = [];
$drcalc = 0;
$crcalc = 0;
function print_account_chart($account, $c = 0, $niveles = NULL, $startcode = NULL, $endcode = NULL, $third_account_trial_balance = NULL)
{
	global $drcalc, $crcalc, $totalParientes;
	/* NIVELES */
	if (isset($_POST['nivelBalancesheet'])) {
		$nivelBalancesheet = $_POST['nivelBalancesheet'];
	} else {
		$nivelBalancesheet = "";
	}
	$bandera = 0;
	if ($nivelBalancesheet != "") {
		for ($i=1; $i <= $nivelBalancesheet ; $i++) { 
			if (in_array($account['code'], $niveles[$i])) {
				$bandera++;
			}
		}
	} else {
		$bandera++;
	}
	/* NIVELES */
	/* FILTRO POR CUENTAS */
	$pcBandera = 0;
	if (($startcode != NULL || $endcode != NULL) && $account['code'] != "") {
		$pcBandera = bandera_posicion_cuentas($account['code'], $startcode, $endcode);
		if ($pcBandera != 0 && $account['code'] == $endcode) {
			$totalParientes[$account['code']]['dr_total'] = $account['dr_total'];
			$totalParientes[$account['code']]['cr_total'] = $account['cr_total'];
		}
	} else {
		$pcBandera++;
	}
	/* FILTRO POR CUENTAS */
	$clBandera = bandera_closing_balance($account);
  $CI =& get_instance();
	$counter = $c;
	if (isset($_POST['third_id']) && $_POST['third_id'] != '') {
		
	} else {
		/* Print groups */
		if (!empty($account)) {
			if (!isset($account['code'])) {
				// exit(var_dump($account));
			}
			if (strlen($account['code']) > 0 && $bandera != 0 && ((isset($_POST['hidezero']) && $clBandera != 0) || !isset($_POST['hidezero'])) && ($pcBandera != 0)) {
				if (strlen($account['code']) == 1) {
					echo '<tr class="tr-group tr-root-group">';
				} else {
					echo '<tr class="tr-group">';
				}
				echo '<td class="td-group">';
				// echo print_space($counter);
				echo '<b>'.($CI->functionscore->toCodeWithName($account['code'], $account['name'])).'</b>';
				echo '</td>';
				echo '<td>'.lang('accounts_index_td_label_group').'</td>';
				if ($account['op_total'] < 0) {
					$na = "negative-amount";
				} else {
					$na = '';
				}
				echo '<td class="text-right '.$na.'">';
				echo $CI->functionscore->toCurrency(($account['op_total'] < 0 ? 'C' : 'D'), ($account['op_total'] < 0 ? $account['op_total'] * -1 : $account['op_total']));
				echo '</td>';
				echo '<td class="text-right">' . $CI->functionscore->toCurrency('D', $account['dr_total']) . '</td>';
				if ($account['cr_total'] > 0) {
					$na = "negative-amount";
				} else {
					$na = '';
				}
				echo '<td class="text-right '.$na.'">' . $CI->functionscore->toCurrency('C', $account['cr_total']) . '</td>';
				if ($account['cl_total'] < 0) {
					$na = "negative-amount";
				} else {
					$na = '';
				}
				echo '<td class="text-right '.$na.'">';
				echo $CI->functionscore->toCurrency(($account['cl_total'] < 0 ? 'C' : 'D'), ($account['cl_total'] < 0 ? $account['cl_total'] * -1 : $account['cl_total']));
				echo '</td>';
				echo '</tr>';
			}
		} else {
			exit(var_dump($account));
		}
	}
	/* Print child ledgers */
	if (isset($account['children_ledgers']) && count($account['children_ledgers']) > 0 && $nivelBalancesheet == "") {
		$counter++;
		foreach ($account['children_ledgers'] as $id => $data) {
			$pcBandera = 0;
			if ($startcode != NULL || $endcode != NULL) {
				$pcBandera = bandera_posicion_cuentas($data['code'], $startcode, $endcode);
				if ($pcBandera == 1) {
					if (isset($totalParientes[$data['parent_id']])) {
						$totalParientes[$data['parent_id']]['dr_total'] += $data['dr_total'];
						$totalParientes[$data['parent_id']]['cr_total'] += $data['cr_total'];
					} else {
						$totalParientes[$data['parent_id']]['dr_total'] = $data['dr_total'];
						$totalParientes[$data['parent_id']]['cr_total'] = $data['cr_total'];
					}
				}
			} else {
				$pcBandera++;
			}
			if ((isset($_POST['hidezero']) && ($data['cl_total'] != 0 || $data['cr_total'] > 0 || $data['dr_total'] > 0) && $pcBandera != 0) || (!isset($_POST['hidezero']) && $pcBandera != 0)) {
				echo '<tr class="tr-ledger">';
				echo '<td class="td-ledger">';
				// echo print_space($counter);
				echo anchor('reports/ledgerstatement/ledgerid/'.$data['id'], $CI->functionscore->toCodeWithName($data['code'], $data['name']));
				echo '</td>';
				echo '<td>'.lang('accounts_index_td_label_ledger').'</td>';
				if ($data['op_total'] < 0) {
					$na = "negative-amount";
				} else {
					$na = '';
				}
				echo '<td class="text-right '.$na.'">';
				echo $CI->functionscore->toCurrency(($data['op_total'] < 0 ? 'C' : 'D'), ($data['op_total'] < 0 ? $data['op_total'] * -1 : $data['op_total']));
				echo '</td>';
				echo '<td class="text-right">' . $CI->functionscore->toCurrency('D', $data['dr_total']) . '</td>';
				if ($data['cr_total'] > 0) {
					$na = "negative-amount";
				} else {
					$na = '';
				}
				echo '<td class="text-right '.$na.'">' . $CI->functionscore->toCurrency('C', $data['cr_total']) . '</td>';
				// $drcalc = $drcalc + $data['dr_total'];
				// $crcalc = $crcalc + $data['cr_total'];
				if ($data['cl_total'] < 0) {
					$na = "negative-amount";
				} else {
					$na = '';
				}
				echo '<td class="text-right '.$na.'">';
				echo $CI->functionscore->toCurrency(($data['cl_total'] < 0 ? 'C' : 'D'), ($data['cl_total'] < 0 ? $data['cl_total'] * -1 : $data['cl_total']));
				echo '</td>';
				echo '</tr>';
				if ($third_account_trial_balance && isset($third_account_trial_balance[$data['id']])) {
					foreach ($third_account_trial_balance[$data['id']] as $company_id => $company) {
						echo "<tr>
								<td>".print_space(1).$data['code']." - ".$company['company_vat_no']." - ".$company['company_name']."</td>
								<td></td>
								<td class='text-right'>".$CI->functionscore->toCurrency(($company['op_balance'] < 0 ? 'C' : 'D'), ($company['op_balance'] < 0 ? $company['op_balance'] * -1 : $company['op_balance']))."</td>
								<td class='text-right'>".$CI->functionscore->toCurrency('D', $company['total_dr'])."</td>
								<td class='text-right'>".$CI->functionscore->toCurrency('C', $company['total_cr'])."</td>
								<td class='text-right'>".$CI->functionscore->toCurrency(($company['cl_balance'] < 0 ? 'C' : 'D'), ($company['cl_balance'] < 0 ? $company['cl_balance'] * -1 : $company['cl_balance']))."</td>
							  </tr>";
					}
				}
			}
		}
		$counter--;
	}
	/* Print child groups recursively */
	if (isset($account['children_groups'])) {
		foreach ($account['children_groups'] as $id => $data) {
			$counter++;
			print_account_chart($data, $counter, $niveles, $startcode, $endcode, $third_account_trial_balance);
			$counter--;
		}
	}
		
}

function bandera_closing_balance($data, $bandera = 0) {
	$data = isset($data[0]) ? $data[0] : $data;
	if (isset($data['children_groups']) && count($data['children_groups']) > 0) {
		foreach ($data['children_groups'] as $id => $grupo) {
			if (isset($grupo['children_groups']) && count($grupo['children_groups']) > 0) {
				$bandera = bandera_closing_balance($grupo, $bandera);
			} else if ($grupo['cl_total'] != 0 || $grupo['cr_total'] > 0 || $grupo['dr_total'] > 0) {
				$bandera++;
			}
		}
	} else if (isset($data['children_ledgers']) && count($data['children_ledgers']) > 0) {
		foreach ($data['children_ledgers'] as $key => $ledger) {
			if ($ledger['cl_total'] != 0 || $ledger['cr_total'] > 0 || $ledger['dr_total'] > 0) {
				$bandera++;
			}
		}
	} else if ($data['cl_total'] != 0 || $data['cr_total'] > 0 || $data['dr_total'] > 0) {
		$bandera++;
	}
	return $bandera;
}

function bandera_posicion_cuentas($code, $startcode, $endcode){

	$code2 = (String) $code;
	$startcode2 = (String) $startcode;
	$endcode2 = (String) $endcode;
	if ($startcode == 0) {
		if (strcmp($code2, $endcode2) <= 0) {
			return 1;
		} else {
			return 0;
		}
	} else if ($endcode == 0) {
		if (strcmp($code2, $startcode2) >= 0) {
			return 1;
		} else {
			return 0;
		}
	} else if ($startcode != 0 && $endcode != 0) {
		if (strcmp($code2, $startcode2) >= 0 && strcmp($code2, $endcode2) <= 0) {
			return 1;
		} else {
			return 0;
		}
	}
}

function print_space($count)
{
	$html = '';
	for ($i = 1; $i <= $count; $i++) {
		$html .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
	}
	return $html;
}
?>

<script type="text/javascript">
	$(document).ready(function() {
	$("#accordion").accordion({
		collapsible: true,
		<?php
			if (isset($options) && $options == false) {
				echo 'active: false';
			}
		?>
		});
		/* Calculate date range in javascript */
	startDate = new Date(<?php echo strtotime($this->mAccountSettings->fy_start) * 1000; ?>  + (new Date().getTimezoneOffset() * 60 * 1000));
	endDate = new Date(<?php echo strtotime($this->mAccountSettings->fy_end) * 1000; ?>  + (new Date().getTimezoneOffset() * 60 * 1000));
	$('#BalancesheetStartdate').datepicker({
		minDate: startDate,
		maxDate: endDate,
		dateFormat: '<?php echo $this->mDateArray[1]; ?>',
		numberOfMonths: 1,
		monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Juv', 'Vie', 'Sáb'],
        dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],
		onClose: function(selectedDate) {
			$("#BalancesheetEnddate").datepicker("option", "minDate", startDate).datepicker("option", "maxDate", endDate);
			$("#BalancesheetStartdate").datepicker("option", "minDate", startDate).datepicker("option", "maxDate", endDate);
		},
		onBlur: function(selectedDate) {
			$("#BalancesheetEnddate").datepicker("option", "minDate", startDate).datepicker("option", "maxDate", endDate);
			$("#BalancesheetStartdate").datepicker("option", "minDate", startDate).datepicker("option", "maxDate", endDate);
		}
	});
	$('#BalancesheetEnddate').datepicker({
		minDate: startDate,
		maxDate: endDate,
		dateFormat: '<?php echo $this->mDateArray[1]; ?>',
		numberOfMonths: 1,
		monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Juv', 'Vie', 'Sáb'],
        dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],
		onClose: function(selectedDate) {
			$("#BalancesheetEnddate").datepicker("option", "minDate", startDate).datepicker("option", "maxDate", endDate);
			$("#BalancesheetStartdate").datepicker("option", "minDate", startDate).datepicker("option", "maxDate", endDate);
		},
		onBlur: function(selectedDate) {
			$("#BalancesheetEnddate").datepicker("option", "minDate", startDate).datepicker("option", "maxDate", endDate);
			$("#BalancesheetStartdate").datepicker("option", "minDate", startDate).datepicker("option", "maxDate", endDate);
		}
	});
	$(".ledger-dropdown").select2({
		width:'100%',
		templateResult: formatOutput
	});
	function formatOutput (optionElement) {
	  if (!optionElement.id) { return optionElement.text; }
	  if (optionElement.element.value < 0) {
	  	var $state = $('<span>' + optionElement.text + '</span>');
	  } else {
	  	var $state = $('<span><strong>' + optionElement.text + '</strong></span>');
	  }
	  return $state;
	};
	});
	$(document).on('keypress', 'form', function(e){   
    if(e == 13){
      return false;
    }
  });
  $(document).on('keypress', 'input', function(e){
    if(e.which == 13){
      return false;
    }
  });
  $(document).on('change', '#groupfrom', function(){
  	var groupfrom = $(this).val();
  		groupto = $('#groupto').val();

  	if (groupfrom != 0 || groupto != 0) {
  		$('#nivelBalancesheet').val('').change();
  		$('#nivelBalancesheet option:not(:selected)').prop('disabled', true);
  		$('#nivelBalancesheet').css('background-color', '#eeeeee');
  	} else {
  		$('#nivelBalancesheet option:not(:selected)').prop('disabled', false);
  		$('#nivelBalancesheet').css('background-color', 'white');
  	}
  });
  $(document).on('change', '#groupto', function(){
  	var groupfrom = $('#groupfrom').val();
  		groupto = $(this).val();

  	if (groupfrom != 0 || groupto != 0) {
  		$('#nivelBalancesheet').val('').change();
  		$('#nivelBalancesheet option:not(:selected)').prop('disabled', true);
  		$('#nivelBalancesheet').css('background-color', '#eeeeee');
  	} else {
  		$('#nivelBalancesheet option:not(:selected)').prop('disabled', false);
  		$('#nivelBalancesheet').css('background-color', 'white');
  	}
  });
  $(document).on('change', '#nivelBalancesheet', function(){
  	var nivel = $(this).val();
  	if (nivel == "") {
  		$('#groupfrom').prop('disabled', false);
  		$('#groupto').prop('disabled', false);
  	} else {
  		$('#groupfrom').prop('disabled', true);
  		$('#groupto').prop('disabled', true);
  	}
  });
  $(document).on('click', '.export', function(){
  	type_export = $(this).data('export');
  	if (type_export == "xls") {
  		$('#formtrialbalance').attr('action', 'reports/trialbalance/download/xls').attr('target', '_blank').submit();
  	} else if (type_export == "pdf") {
  		$('#formtrialbalance').attr('action', 'reports/trialbalance/download/pdf').attr('target', '_blank').submit();
  	} else if (type_export == "pdfprint") {
  		$('#formtrialbalance').attr('action', 'reports/trialbalance/download/pdfprint').attr('target', '_blank').submit();
  	}
  	setTimeout(function() {
  		resetear_action();
  	}, 5000);
  });

	function resetear_action(){
		$('#formtrialbalance').attr('action', 'reports/trialbalance').attr('target', '_self');
	}

</script>

	<div class="row wrapper border-bottom white-bg page-heading">
	  <div class="col-lg-8">
	    <h2><?php echo $page_title; ?></h2>
	    <ol class="breadcrumb">
	      <li>
	        <a href="login/index">Inicio</a> <?php //En el controlador en la función llamada, se revisa si el usuario está logueado y lo envía a la vista principal según permisos ?>
	      </li>
	      <li class="active">
	        <strong><?php echo $page_title; ?></strong>
	      </li>
	    </ol>
	  </div><!-- /.col -->
	  <div class="col-lg-4">
	  </div>
	</div><!-- /.row -->

<?php
	/* Show difference in opening balance */
	if (isset($options) && $options && $is_opdiff) {
		echo '<div><div role="alert" class="alert alert-danger">' .
			sprintf(lang('accounts_index_label_difference_bw_balance'), $this->functionscore->toCurrency($opdiff['opdiff_balance_dc'], $opdiff['opdiff_balance'])) . '</div></div>';
	}
	/* Show difference in account */
	if (isset($options) && $options && (($this->Owner && $account_list->cl_total > 0) || (!$this->Owner && $account_list->cl_total > 0.1))) {
		echo '<div><div role="alert" class="alert alert-danger">' .
			sprintf(lang('trial_balance_closing_balance_difference'), $this->functionscore->toCurrency(($account_list->cl_total < 0 ? 'C':'D'), ($account_list->cl_total < 0 ? $account_list->cl_total * -1 : $account_list->cl_total))) . '</div></div>';
	}
	?>
    <section class="row wrapper wrapper-content animated fadeInRight">
      <div class="box float-e-margins">
        <div class="ibox-content contentBackground">
          <div class="col-sm-12">
            <div class="box-body">
            	<div id="accordion">
					<h3>Options</h3>
					<div class="balancesheet form">
					<?php echo form_open('', array('id' => 'formtrialbalance')); ?>
						<div class="row">
							<div class="col-md-3">
								<div style="margin-top: 30px;">
									<input type="checkbox" id="BalancesheetOpening" name="opening" class="checkbox skip" <?= (isset($_POST['opening'])) ? "checked" :"" ?>>
									<label for="BalancesheetOpening"><?= lang('show_op_bs_title'); ?></label>
		                        </div>
							</div>
							<div class="col-md-3">
								<div style="margin-top: 30px;">
									<input type="checkbox" id="hidezero" name="hidezero" class="checkbox skip" <?= (isset($_POST['hidezero'])) ? "checked" :"" ?>>
									<label for="hidezero"><?= lang('hide_zero_accounts'); ?></label>
		                        </div>
							</div>
							<div class="col-md-3">
								<div style="margin-top: 30px;">
									<input type="checkbox" id="skip_entries_disapproved" name="skip_entries_disapproved" class="checkbox skip" <?= (isset($_POST['skip_entries_disapproved'])) ? "checked" :"" ?> value='TRUE'>
									<label for="skip_entries_disapproved"><?= lang('skip_entries_disapproved'); ?></label>
		                        </div>
							</div>
							<div class="col-md-2">
								<div>
									<label for="nivelBalancesheet"><?= lang('nivel_balance_sheet_span'); ?></label>
									<select class="form-control" id="nivelBalancesheet" name="nivelBalancesheet">
										<option value="">Todos</option>
										<?php foreach ($this->niveles  as $numnivel => $nivel): ?>
											<?php 

											if (isset($_POST['groupfrom']) && isset($_POST['groupto']) && ( $_POST['groupfrom'] != 0 || $_POST['groupto'] != 0 )) {
												$disabled = "disabled = 'true'";
											} else {
												$disabled = "";
											}
											 ?>
											<option value="<?= $numnivel ?>" <?= (isset($_POST['nivelBalancesheet']) &&  $_POST['nivelBalancesheet'] == $numnivel) ? "selected='true'" : "" ?> <?= $disabled ?> ><?= $numnivel ?></option>
										<?php endforeach ?>
									</select>
		                        </div>
							</div>
							<div class="col-md-2">
								<div class="form-group">
									<label><?= lang('start_date'); ?></label>
				                    <div class="input-group">
										<input id="BalancesheetStartdate" type="text" name="startdate" class="form-control" value="<?= (isset($_POST['startdate'])) ? $_POST['startdate'] : $this->functionscore->dateFromSql($this->mAccountSettings->fy_start) ?>">
				                        <div class="input-group-addon">
				                            <i>
				                                <div class="fa fa-info-circle" data-toggle="tooltip" title="<?= lang('start_date_span');?>">
				                                </div>
				                            </i>
				                        </div>
				                    </div>
				                    <!-- /.input group -->
				                </div>
				                <!-- /.form group -->
							</div>
							<div class="col-md-2">
								<div class="form-group">
									<label><?= lang('end_date'); ?></label>
				                    <div class="input-group">
										<input id="BalancesheetEnddate" type="text" name="enddate" class="form-control" value="<?= (isset($_POST['enddate'])) ? $_POST['enddate'] : $this->functionscore->dateFromSql($this->end_date_filter) ?>">
				                        <div class="input-group-addon">
				                            <i>
				                                <div class="fa fa-info-circle" data-toggle="tooltip" title="<?= lang('end_date_span');?>">
				                                </div>
				                            </i>
				                        </div>
				                    </div>
				                    <!-- /.input group -->
				                </div>
				                <!-- /.form group -->
							</div>
							<?php if (isset($cost_centers) && $cost_centers): ?>
								<div class="col-md-3">
									<div>
										<label for="cost_center"><?= lang('cost_center_label'); ?></label>
										<select class="form-control ledger-dropdown" id="cost_center" name="cost_center">
											<option value="">Todos</option>
											<?php foreach ($cost_centers as $cost_center): ?>
												<option value="<?= $cost_center['id'] ?>" <?= (isset($_POST['cost_center']) &&  $_POST['cost_center'] == $cost_center['id']) ? "selected='true'" : "" ?>><?= $cost_center['name'] ?></option>
											<?php endforeach ?>
										</select>
			                        </div>
								</div>
							<?php endif ?>
							<div class="col-md-3">
								<div class="form-group">
								<label><?= lang('entries_views_views_th_companies_id'); ?></label>
									<select class="form-control ledger-dropdown" id="third_id" name="third_id">
										<option value="">Seleccione...</option>
										<option value="0" <?= (isset($_POST['third_id']) && $_POST['third_id'] == 0 && $_POST['third_id'] != "") ? "selected" : "" ?>>Todos</option>
										<?php foreach ($terceros as $company): ?>
											<option value="<?= $company['id']; ?>" <?= (isset($_POST['third_id']) && $_POST['third_id'] == $company['id']) ? "selected" : "" ?>><?= '('.( (isset($group_names_spanish[$company['group_name']])) ? $group_names_spanish[$company['group_name']] : $company['group_name']).') '.$company['name'].' - '.$company['vat_no']; ?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
							<hr class="col-sm-10">
							<div class="col-sm-5 form-group">
								<label>Desde : </label>
								<select class="ledger-dropdown form-control" name="groupfrom" id="groupfrom" <?= (isset($_POST['nivelBalancesheet']) && $_POST['nivelBalancesheet'] != "") ? "disabled='disabled'" : "" ?>>
									<?php foreach ($ledger_options as $id => $ledger): ?>
										<option value="<?= $id; ?>" <?= (isset($_POST['groupfrom']) && $_POST['groupfrom'] == $id) ? "selected" : ""; ?>><?= $ledger; ?></option>
									<?php endforeach; ?>
								</select>
							</div>
							<div class="col-sm-5 form-group">
								<label>Hasta : </label>
								<select class="ledger-dropdown form-control" name="groupto" id="groupto" <?= (isset($_POST['nivelBalancesheet']) && $_POST['nivelBalancesheet'] != "") ? "disabled='disabled'" : "" ?>>
									<?php foreach ($ledger_options as $id => $ledger): ?>
										<option value="<?= $id; ?>" <?= (isset($_POST['groupto']) && $_POST['groupto'] == $id) ? "selected" : ""; ?>><?= $ledger; ?></option>
									<?php endforeach; ?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<input type="submit" name="submit" class="btn btn-success pull-right submit" value="<?= lang('create_account_submit_button'); ?>">
						</div>
						<?php form_close();  ?>
					</div>
				</div>
	          	<div class="btn-group" role="group">
					<button class="btn btn-default export" data-export="xls">XLS</button>
					<button class="btn btn-default export" data-export="pdf">PDF</button>
					<button class="btn btn-default export" data-export="pdfprint">Imprimir</button>
				</div>
				<div id="section-to-print">
				<div class="subtitle" style="text-align: center;">
					<b><?php echo $subtitle; ?></b>
				</div>
				<?php if (isset($validation_message)): ?>
					<div class="subtitle" style="text-align: center;">
						<em class="text-danger"><?= $validation_message ?></em>
					</div>
				<?php endif ?>
				<?php if (isset($options) && $options): ?>
					<table class="table stripped">
					<thead>
						<?php
							echo '<th class="text-center">' . lang('accounts_index_account_name') . '</th>';
							echo '<th class="text-center">' . lang('entries_views_index_th_type') . '</th>';
							echo '<th class="text-center">' . lang('accounts_index_op_balance') . ' (' . $this->mAccountSettings->currency_symbol . ')' . '</th>';
							echo '<th class="text-center">' . lang('dr_total') . ' (' . $this->mAccountSettings->currency_symbol . ')' . '</th>';
							echo '<th class="text-center">' . lang('cr_total') . ' (' . $this->mAccountSettings->currency_symbol . ')' . '</th>';
							echo '<th class="text-center">' . lang('accounts_index_cl_balance') . ' (' . $this->mAccountSettings->currency_symbol . ')' . '</th>';
							?>
					</thead>
					<?php
					foreach ($accountlist as $accountRow) {
						print_account_chart($accountRow, -1, $this->niveles, (isset($startcode) ? $startcode : ""), (isset($endcode) ? $endcode : ""), $third_account_trial_balance);
					}
					if (empty($_POST) || isset($_POST['groupfrom']) && $_POST['groupfrom'] == 0 && isset($_POST['groupto']) && $_POST['groupto']  == 0 || !isset($_POST['groupfrom']) && !isset($_POST['groupto'])) {

						if ($this->functionscore->calculate($account_list->dr_total, $account_list->cr_total, '==')) {
							echo '<tr class="bold-text ok-text">';
						} else {
							echo '<tr class="bold-text error-text">';
						}
						echo '<th>' . lang('entries_views_add_items_td_total') . '</th>';
						echo '<td></td>';
						echo '<td class="text-right">' . $this->functionscore->toCurrency(($account_list->op_total < 0 ? 'C' : 'D'), ($account_list->op_total < 0 ? $account_list->op_total * -1 : $account_list->op_total)) . '</td>';
						echo '<td class="text-right">' . $this->functionscore->toCurrency('D', $account_list->dr_total) . '</td>';
						echo '<td class="text-right negative-amount">' . $this->functionscore->toCurrency('C', $account_list->cr_total) . '</td>';
						echo '<td class="text-right">'.$this->functionscore->toCurrency(($account_list->cl_total < 0 ? 'C' : 'D'), ($account_list->cl_total < 0 ? $account_list->cl_total * -1 : $account_list->cl_total)).' ';
						if ($account_list->cl_total == 0) {
							echo '<span class="fa fa-check-circle" style="color:#3c763d;"></span></td>';
						} else {
							if ((($this->Owner && $account_list->cl_total > 0) || (!$this->Owner && $account_list->cl_total > 0.1))) {
									echo '<span class="fa fa-times-circle" style="color:#f92c2c;"></span></td>';
							} else {
									echo '<span class="fa fa-check-circle" style="color:#3c763d;"></span></td>';
							}
						}
						echo '</tr>';
					} else if (isset($_POST['groupfrom']) && $_POST['groupfrom'] != 0 || isset($_POST['groupto']) && $_POST['groupto'] != 0 ) {
						global $drcalc, $crcalc, $totalParientes;
						if (isset($totalParientes)) {
							foreach ($totalParientes as $codigo => $total) {
								$drcalc += $total['dr_total'];
								$crcalc += $total['cr_total'];
							}
						}
						if ($this->functionscore->calculate($drcalc, $crcalc, '==')) {
							echo '<tr class="bold-text ok-text">';
						} else {
							echo '<tr class="bold-text error-text">';
						}
						echo '<th>' . lang('entries_views_add_items_td_total') . '</th>';
						echo '<td></td>';
						echo '<td class="text-right">' . $this->functionscore->toCurrency(($account_list->op_total < 0 ? 'C' : 'D'), ($account_list->op_total < 0 ? $account_list->op_total * -1 : $account_list->op_total)) . '</td>';
						echo '<td class="text-right">' . $this->functionscore->toCurrency('D', $account_list->dr_total) . '</td>';
						echo '<td class="text-right negative-amount">' . $this->functionscore->toCurrency('C', $account_list->cr_total) . '</td>';
						echo '<td class="text-right">'.$this->functionscore->toCurrency(($account_list->cl_total < 0 ? 'C' : 'D'), ($account_list->cl_total < 0 ? $account_list->cl_total * -1 : $account_list->cl_total)).' ';
						if ($account_list->cl_total == 0) {
							echo '<span class="fa fa-check-circle" style="color:#3c763d;"></span></td>';
						} else {
							echo '<span class="fa fa-times-circle" style="color:#f92c2c;"></span></td>';
						}
						echo '</tr>';
					}
					?>
				</table>
				<?php endif ?>
				
</div>
            </div>
          </div>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->

    <script type="text/javascript">
    	$('.ledger-dropdown').select2({width:'100%'});
    </script>