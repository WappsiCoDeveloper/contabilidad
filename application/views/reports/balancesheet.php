<?php

function account_st_short($account, $c = 0, $dc_type = NULL, $niveles = NULL)
{
	$account = isset($account[0]) ? $account[0] : $account;
	if (isset($_POST['nivelBalancesheet'])) {
		$nivelBalancesheet = $_POST['nivelBalancesheet'];
	} else {
		$nivelBalancesheet = "";
	}
	$bandera = 0;
	if ($nivelBalancesheet != "") {
		for ($i=1; $i <= $nivelBalancesheet ; $i++) {
			if (in_array($account['code'], $niveles[$i])) {
				$bandera++;
			}
		}
	} else {
		$bandera++;
	}
	$clBandera = bandera_closing_balance($account);
  $CI =& get_instance();
	$counter = $c;
	if (!empty($account)) {
		if (strlen($account['code']) > 0 && $bandera != 0 && ((isset($_POST['hidezero']) && $clBandera != 0) || !isset($_POST['hidezero'])))
		{
			echo '<tr class="tr-group">';
			echo '<td class="td-group">';
			echo '<b>'.($CI->functionscore->toCodeWithName($account['code'], $account['name'])).'</b>';
			echo '</td>';
			if ($account['cl_total'] < 0) {
				$na = "negative-amount";
			} else {
				$na = "";
			}
			echo '<td class="text-right '.$na.'">';
			echo '<b>'.$CI->functionscore->toCurrency(($account['cl_total'] < 0 ? 'C' : 'D'), ($account['cl_total'] < 0 ? ($account['cl_total'] * -1) : $account['cl_total'])).'</b>';
			echo '</td>';
			echo '</tr>';
		}
		if (isset($account['children_groups']) && count($account['children_groups']) > 0) {
			foreach ($account['children_groups'] as $id => $data)
			{
				$counter++;
				account_st_short($data, $counter, $dc_type, $niveles);
				$counter--;
			}
		}
			
		if (isset($account['children_ledgers']) && count($account['children_ledgers']) > 0 && $nivelBalancesheet == "")
		{
			$counter++;
			foreach ($account['children_ledgers'] as $id => $data)
			{
				if ((isset($_POST['hidezero']) && $data['cl_total'] != 0) || !isset($_POST['hidezero'])) {
					echo '<tr class="tr-ledger">';
					echo '<td class="td-ledger">';
					echo anchor('reports/ledgerstatement/ledgerid/'.$data['id'], $CI->functionscore->toCodeWithName($data['code'], $data['name']));
					echo '</td>';
					if ($data['cl_total'] < 0) {
						$na = "negative-amount";
					} else {
						$na = "";
					}
					echo '<td class="text-right '.$na.'">';
					echo $CI->functionscore->toCurrency(($data['cl_total'] < 0 ? 'C' : 'D'), ($data['cl_total'] < 0 ? ($data['cl_total'] * -1) : $data['cl_total']));
					echo '</td>';
					echo '</tr>';
				}
			}
		$counter--;
		}
	}
}

function bandera_closing_balance($data, $bandera = 0) {
	$data = isset($data[0]) ? $data[0] : $data;
	if (!empty($data)) {
		if (isset($data['children_groups']) && count($data['children_groups']) > 0) {
			foreach ($data['children_groups'] as $id => $grupo) {
				if (isset($grupo['children_groups']) && count($grupo['children_groups']) > 0) {
					$bandera = bandera_closing_balance($grupo, $bandera);
				} else if ($grupo['cl_total'] != 0) {
					$bandera++;
				}
			}
		} else if (isset($data['children_ledgers']) && count($data['children_ledgers']) > 0) {
			foreach ($data['children_ledgers'] as $key => $ledger) {
				if ($ledger['cl_total'] != 0) {
					$bandera++;
				}
			}
		} else if ($data['cl_total'] != 0) {
			$bandera++;
		}
	}
	return $bandera;
}

function print_space($count)
{
	$html = '';
	for ($i = 1; $i <= $count; $i++) {
		$html .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
	}
	return $html;
}

?>

<script type="text/javascript">

	$("#BalancesheetOpening").select2();

$(document).ready(function() {
	$(function () {
	  $('[data-toggle="tooltip"]').tooltip()
	})

	$("#accordion").accordion({
		collapsible: true,
		<?php
			if (isset($options) && $options == false) {
				echo 'active: false';
			}
		?>
	});

	$('.show-tooltip').tooltip({trigger: 'manual'}).tooltip('show');

	/* Calculate date range in javascript */
	startDate = new Date(<?php echo strtotime($this->mAccountSettings->fy_start) * 1000; ?>  + (new Date().getTimezoneOffset() * 60 * 1000));
	endDate = new Date(<?php echo strtotime($this->mAccountSettings->fy_end) * 1000; ?>  + (new Date().getTimezoneOffset() * 60 * 1000));


	$('#BalancesheetOpening').on('ifChanged', function(event) {
	    if (event.target.checked) {
			$('#BalancesheetStartdate').prop('disabled', true);
			$('#BalancesheetEnddate').prop('disabled', true);
		} else {
			$('#BalancesheetStartdate').prop('disabled', false);
			$('#BalancesheetEnddate').prop('disabled', false);
		}
	});
	/* Setup jQuery datepicker ui */
	$('#BalancesheetStartdate').datepicker({
		minDate: startDate,
		maxDate: endDate,
		dateFormat: '<?php echo $this->mDateArray[1]; ?>',
		numberOfMonths: 1,
		onClose: function(selectedDate) {
			if (selectedDate) {
				$("#BalancesheetEnddate").datepicker("option", "minDate", selectedDate);
			} else {
				$("#BalancesheetEnddate").datepicker("option", "minDate", startDate);
			}
		}
	});
	$('#BalancesheetEnddate').datepicker({
		minDate: startDate,
		maxDate: endDate,
		dateFormat: '<?php echo $this->mDateArray[1]; ?>',
		numberOfMonths: 1,
		language: 'ES',
		monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Juv', 'Vie', 'Sáb'],
        dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],
		onClose: function(selectedDate) {
			$("#BalancesheetEnddate").datepicker("option", "minDate", startDate).datepicker("option", "maxDate", endDate);
		},
		onBlur: function(selectedDate) {
			$("#BalancesheetEnddate").datepicker("option", "minDate", startDate).datepicker("option", "maxDate", endDate);
		}
	});
	$('.submit').on('click', function(){
		$('#loader').fadeIn();
	});
	$(document).on('keypress', 'form', function(e){
        if(e == 13){
          return false;
        }
      });
    $(document).on('keypress', 'input', function(e){
        if(e.which == 13){
          return false;
        }
      });
});

$(".ledger-dropdown").select2({width:'100%'});
	$(document).on('click', '.export', function(){
		type_export = $(this).data('export');
		if (type_export == "xls") {
			$('#formbalancesheet').attr('action', 'reports/balancesheet/download/xls').attr('target', '_blank').submit();
		} else if (type_export == "pdf") {
			$('#formbalancesheet').attr('action', 'reports/balancesheet/download/pdf').attr('target', '_blank').submit();
		} else if (type_export == "pdfprint") {
			$('#formbalancesheet').attr('action', 'reports/balancesheet/download/pdfprint').attr('target', '_blank').submit();
		}
		setTimeout(function() {
			resetear_action();
		}, 5000);
	});

	function resetear_action(){
		$('#formbalancesheet').attr('action', 'reports/balancesheet').attr('target', '_self');
	}
</script>
	<div class="row wrapper border-bottom white-bg page-heading">
	  <div class="col-lg-8">
	    <h2><?php echo $page_title; ?></h2>
	    <ol class="breadcrumb">
	      <li>
	        <a href="login/index">Inicio</a> <?php //En el controlador en la función llamada, se revisa si el usuario está logueado y lo envía a la vista principal según permisos ?>
	      </li>
	      <li class="active">
	        <strong><?php echo $page_title; ?></strong>
	      </li>
	    </ol>
	  </div><!-- /.col -->
	  <div class="col-lg-4">
	  </div>
	</div><!-- /.row -->
	<?php
	/* Show difference in opening balance */
	if (isset($options) && $bsheet['is_opdiff']) {
		echo '<div><div role="alert" class="alert alert-danger">' .
			sprintf(lang('accounts_index_label_difference_bw_balance'), $this->functionscore->toCurrency($bsheet['opdiff']['opdiff_balance_dc'], $bsheet['opdiff']['opdiff_balance'])) . '</div></div>';
	}

	/* Show difference in liabilities and assets total */
	if (isset($options) && $this->functionscore->calculate($bsheet['final_liabilities_total'], $bsheet['final_assets_total'], '!=')) {
		$final_total_diff = $this->functionscore->calculate($bsheet['final_liabilities_total'], $bsheet['final_assets_total'], '-');
		echo '<div><div role="alert" class="alert alert-danger">' .
			sprintf(lang('balance_sheet_tla_diff'), $this->functionscore->toCurrency('X', $final_total_diff))
			.
			'</div></div>';
	}
	?>

    <div class="row wrapper wrapper-content animated fadeInRight">
      <div class="ibox float-e-margins">
        <div class="ibox-content contentBackground">
          <div class="col-sm-12">
            <div class="box-body">
				<div id="accordion">
					<h3>Options</h3>
					<div class="balancesheet form">
					<?php echo form_open('', array('id' => 'formbalancesheet')); ?>
						<div class="row">
							<div class="col-md-3">
								<div>
									<label for="nivelBalancesheet"><?= lang('nivel_balance_sheet_span'); ?></label>
									<select class="form-control ledger-dropdown" id="nivelBalancesheet" name="nivelBalancesheet">
										<option value="">Todos</option>
										<?php if ($this->niveles): ?>
											<?php foreach ($this->niveles as $numnivel => $nivel): ?>
												<option value="<?= $numnivel ?>" <?= (isset($_POST['nivelBalancesheet']) &&  $_POST['nivelBalancesheet'] == $numnivel) ? "selected='true'" : "" ?>><?= $numnivel ?></option>
											<?php endforeach ?>
										<?php endif ?>
									</select>
                </div>
							</div>

							<div class="col-md-3">
								<div class="form-group">
									<label><?= lang('end_date'); ?></label>

				                    <div class="input-group">
										<input id="BalancesheetEnddate" type="text" name="enddate" class="form-control" value="<?= (isset($_POST['enddate'])) ? $_POST['enddate'] : $this->functionscore->dateFromSql($this->end_date_filter) ?>">
				                        <div class="input-group-addon">
				                            <i>
				                                <div class="fa fa-info-circle" data-toggle="tooltip" title="<?= lang('end_date_span');?>">
				                                </div>
				                            </i>
				                        </div>
				                    </div>
				                    <!-- /.input group -->
				                </div>
				                <!-- /.form group -->
							</div>

							<?php if (isset($cost_centers) && $cost_centers): ?>
								<div class="col-md-3">
									<div>
										<label for="cost_center"><?= lang('cost_center_label'); ?></label>
										<select class="form-control ledger-dropdown" id="cost_center" name="cost_center">
											<option value="">Todos</option>
											<?php foreach ($cost_centers as $cost_center): ?>
												<option value="<?= $cost_center['id'] ?>" <?= (isset($_POST['cost_center']) &&  $_POST['cost_center'] == $cost_center['id']) ? "selected='true'" : "" ?>><?= $cost_center['name'] ?></option>
											<?php endforeach ?>
										</select>
			                        </div>
								</div>
							<?php endif ?>

							<hr class="col-md-11">

							<div class="col-md-3">
								<div style="margin-top: 30px;">
									<input type="checkbox" id="BalancesheetOpening" name="opening" class="checkbox skip" <?= (isset($_POST['opening'])) ? "checked" :"" ?>>
									<label for="BalancesheetOpening"><?= lang('show_op_bs_title'); ?></label>
		                        </div>
							</div>

							<div class="col-md-3">
								<div style="margin-top: 30px;">
									<input type="checkbox" id="hidezero" name="hidezero" class="checkbox skip" <?= (isset($_POST['hidezero'])) ? "checked" :"" ?>>
									<label for="hidezero"><?= lang('hide_zero_accounts'); ?></label>
		                        </div>
							</div>

							<div class="col-md-3">
								<div style="margin-top: 30px;">
									<input type="checkbox" id="skip_entries_disapproved" name="skip_entries_disapproved" class="checkbox skip" <?= (isset($_POST['skip_entries_disapproved'])) ? "checked" :"" ?> value='TRUE'>
									<label for="skip_entries_disapproved"><?= lang('skip_entries_disapproved'); ?></label>
		                        </div>
							</div>
						</div>
						<div class="form-group">
							<input type="submit" name="submit" class="btn btn-success pull-right submit" value="<?= lang('create_account_submit_button'); ?>">
						</div>
						<?php form_close();  ?>
					</div>
				</div>
				<br />

				<div class="btn-group" role="group">
					<button class="btn btn-default export" data-export="xls">XLS</button>
					<button class="btn btn-default export" data-export="pdf">PDF</button>
					<button class="btn btn-default export" data-export="pdfprint">Imprimir</button>
				</div>
				<?php if (isset($options)): ?>
					<div id="section-to-print">
						<div class="subtitle text-center">
							<b><?php echo $subtitle ?></b>
						</div>

						<table class="table">
						<!-- Liabilities and Assets -->
						<tr>
							<!-- Assets -->
							<td class="table-top width-50">
								<table class="table stripped">
									<tr>
										<th style="width: 50%;"><?php echo lang('balance_sheet_assets'); ?></th>
										<th class="text-right" style="width: 50%;"><?php echo ('Monto'); ?><?php echo ' (' . $this->mAccountSettings->currency_symbol . ')'; ?></th>
									</tr>
									<?php echo account_st_short($bsheet['assets'], $c = -1, 'D', $this->niveles); ?>
								</table>
							</td>

							<!-- Liabilities -->
							<td class="table-top width-50">
								<table class="table stripped">
									<tr>
										<th style="width: 50%;"><?php echo lang('balance_sheet_loe'); ?></th>
										<th style="width: 50%;" class="text-right"><?php echo ('Monto'); ?><?php echo ' (' . $this->mAccountSettings->currency_symbol . ')'; ?></th>
									</tr>
									<?php echo account_st_short($bsheet['liabilities'], $c = -1, 'C', $this->niveles); ?>

									<?php echo account_st_short($bsheet['liabilities2'], $c = -1, 'C', $this->niveles); ?>
								</table>
							</td>
						</tr>

						<tr>
							<!-- Assets Calculations -->
							<td class="table-top width-50">
								<div class="report-tb-pad"></div>
								<table class="table stripped">
									<?php
									/* Assets Total */
									if ($this->functionscore->calculate($bsheet['assets_total'], 0, '>=')) {
										echo '<tr class="bold-text">';
										echo '<th>' . lang('balance_sheet_total_assets') . '</th>';
										echo '<td class="text-right">' . $this->functionscore->toCurrency('D', $bsheet['assets_total']) . '</td>';
										echo '</tr>';
									} else {
										if ($bsheet['assets_total'] < 0) {
											$na = "negative-amount";
										} else {
											$na = "";
										}
										echo '<tr class=" bold-text ">';
										echo '<th>' . lang('balance_sheet_total_assets') . '</th>';
										echo '<td class="text-right '.$na.'" data-toggle="tooltip" data-placement="top" title="Esperando balance Débito">' . $this->functionscore->toCurrency('D', $bsheet['assets_total']) . '</td>';
										echo '</tr>';
									}
									?>
									<tr class="bold-text">
										<?php
										/* Net loss */
										if ($this->functionscore->calculate($bsheet['pandl'], 0, '>=')) {
											echo '<td>&nbsp</td>';
											echo '<td>&nbsp</td>';
										} else {
											echo '<td>' . lang('balance_sheet_net_loss') . '</td>';
											$positive_pandl = $this->functionscore->calculate($bsheet['pandl'], 0, 'n');
											echo '<td class="text-right">' . $this->functionscore->toCurrency('D', $positive_pandl) . '</td>';
										}
										?>
										</tr>
										<?php
										/* Difference in opening balance */
										if ($bsheet['is_opdiff']) {
											echo '<tr class="bold-text error-text">';
											/* If diff in opening balance is Dr */
											if ($bsheet['opdiff']['opdiff_balance_dc'] == 'D') {
												echo '<td>' . lang('balance_sheet_diff_opp') . '</td>';
												echo '<td class="text-right">' . $this->functionscore->toCurrency('D', $bsheet['opdiff']['opdiff_balance']) . '</td>';
											} else {
												echo '<td>&nbsp</td>';
												echo '<td>&nbsp</td>';
											}
											echo '</tr>';
										}
										?>

										<?php
										/* Total */
										if ($this->functionscore->calculate($bsheet['final_liabilities_total'],
											$bsheet['final_assets_total'], '==')) {
											echo '<tr class="bold-text bg-filled">';
										} else {
											echo '<tr class="bold-text error-text bg-filled">';
										}

										if ($bsheet['final_assets_total'] < 0) {
											$na = "negative-amount";
										} else {
											$na = "";
										}

										echo '<th>' . lang('balance_sheet_total') . '</th>';
										echo '<td class="text-right '.$na.'">' .
											$this->functionscore->toCurrency('D', $bsheet['final_assets_total']) .
											'</td>';
										echo '</tr>';
										?>
									</table>
								</td>

								<!-- Liabilities Calculations -->
								<td class="table-top width-50">
									<div class="report-tb-pad"></div>
									<table class="table stripped">
										<?php
										/* Liabilities Total */
										if ($this->functionscore->calculate($bsheet['liabilities_total'], 0, '>=')) {

											if ($bsheet['liabilities_total'] > 0) {
												$na = "negative-amount";
											} else {
												$na = "";
											}

											echo '<tr class="bold-text">';
											echo '<th>' . lang('balance_sheet_tloe') . '</th>';
											echo '<td class="text-right '.$na.'">' . $this->functionscore->toCurrency('C', $bsheet['liabilities_total']) . '</td>';
											echo '</tr>';
										} else {
											echo '<tr class=" bold-text">';
											echo '<th>' . lang('balance_sheet_tloe') . '</th>';
											echo '<td class="text-right" data-toggle="tooltip" data-placement="top" title="Esperando balance Crédito">' . $this->functionscore->toCurrency('C', $bsheet['liabilities_total']) . '</td>';
											echo '</tr>';
										}
										?>
										<tr class="bold-text">
											<?php
											/* Net profit */
											if ($this->functionscore->calculate($bsheet['pandl'], 0, '>=')) {

												if ($bsheet['pandl'] > 0) {
													$na = "negative-amount";
												} else {
													$na = "";
												}

												echo '<td>' . lang('balance_sheet_net_profit') . '</td>';
												echo '<td class="text-right '.$na.'">' . $this->functionscore->toCurrency('C', $bsheet['pandl']) . '</td>';
											} else {
												echo '<td>&nbsp</td>';
												echo '<td>&nbsp</td>';
											}
											?>
										</tr>
										<?php
										/* Difference in opening balance */
										if ($bsheet['is_opdiff']) {
											echo '<tr class="bold-text error-text">';
											/* If diff in opening balance is Cr */
											if ($bsheet['opdiff']['opdiff_balance_dc'] == 'C') {
												if ($bsheet['opdiff']['opdiff_balance'] > 0) {
													$na = "negative-amount";
												} else {
													$na = "";
												}
												echo '<td>' . lang('balance_sheet_diff_opp') . '</td>';
												echo '<td class="text-right '.$na.'">' . $this->functionscore->toCurrency('C', $bsheet['opdiff']['opdiff_balance']) . '</td>';
											} else {
												echo '<td>&nbsp</td>';
												echo '<td>&nbsp</td>';
											}
											echo '</tr>';
										}
										?>

										<?php
										/* Total */
										if ($this->functionscore->calculate($bsheet['final_liabilities_total'],
											$bsheet['final_assets_total'], '==')) {
											echo '<tr class="bold-text bg-filled">';
										} else {
											echo '<tr class="bold-text error-text bg-filled">';
										}
										echo '<th>' . lang('balance_sheet_total') . '</th>';

										if ($bsheet['final_liabilities_total'] > 0) {
											$na = "negative-amount";
										} else {
											$na = "";
										}

										echo '<td class="text-right">' .
											$this->functionscore->toCurrency('C', $bsheet['final_liabilities_total']) .
											'</td>';
										echo '</tr>';
										?>
									</table>
								</td>
							</tr>
							<?php if (isset($bsheet['operationalcosts']) || isset($bsheet['operationalcosts2'])): ?>
								<tr class="bold-text">
									<th colspan="2" style="text-align: center;">
										<h3><?= lang('balance_sheet_title_operational_costs'); ?></h3>
									</th>
								</tr>
								<tr> <?php // costos operacionales ?>
									<?php if (isset($bsheet['operationalcosts'])): ?>
										<td class="table-top width-50"> <?php // Grupo clase 8 ?>
											<table class="table stripped">
												<?php if (isset($bsheet['operationalcosts'])): ?>
													<?php echo account_st_short($bsheet['operationalcosts'], $c = -1, 'D', $this->niveles); ?>
												<?php endif ?>
											</table>
										</td>
									<?php endif ?>
									<?php if (isset($bsheet['operationalcosts2'])): ?>
										<td class="table-top width-50"> <?php // Grupo clase 9 ?>
											<table class="table stripped">
												<?php if (isset($bsheet['operationalcosts2'])): ?>
													<?php echo account_st_short($bsheet['operationalcosts2'], $c = -1, 'C', $this->niveles); ?>
												<?php endif ?>
											</table>
										</td>
									<?php endif ?>
								</tr>
								<tr>
									<?php if (isset($bsheet['operationalcosts'])): ?>
										<td class="table-top width-50">
											<table class="table stripped">
												<tr>
													<th><?= lang('balance_sheet_total_operational_cost'); ?> </th>
													<?php

													if ($bsheet['operationalcosts_total'] < 0) {
														$na = "negative-amount";
													} else {
														$na = "";
													}

													 ?>
													<td class="text-right <?= $na ?>"><?= $this->functionscore->toCurrency('D', $bsheet['operationalcosts_total']) ?></td>
												</tr>
											</table>
										</td>
									<?php endif ?>
									<?php if (isset($bsheet['operationalcosts2'])): ?>
										<td class="table-top width-50">
											<table class="table stripped">
												<tr>
													<th><?= lang('balance_sheet_total_operational_cost_2'); ?></th>
													<?php

													if ($bsheet['operationalcosts2_total'] > 0) {
														$na = "negative-amount";
													} else {
														$na = "";
													}

													 ?>
													<td class="text-right <?= $na ?>"><?= $this->functionscore->toCurrency('C', $bsheet['operationalcosts2_total']) ?></td>
												</tr>
											</table>
										</td>
									<?php endif ?>
								</tr>
							<?php endif ?>
						</table>
					</div>
					
				<?php endif ?>
            </div>
          </div>
      </div>
    </div>

    <?php

    // var_dump($this->niveles);

     ?>