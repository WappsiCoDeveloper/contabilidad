<?php
function account_st_short($account, $c = 0, $dc_type = NULL, $niveles = NULL)
{
	$account = isset($account[0]) ? $account[0] : $account;
	if (isset($_POST['nivelBalancesheet'])) {
		$nivelBalancesheet = $_POST['nivelBalancesheet'];
	} else {
		$nivelBalancesheet = "";
	}
	$bandera = 0;
	if ($nivelBalancesheet != "") {
		for ($i=1; $i <= $nivelBalancesheet ; $i++) {
			if (!empty($account) && in_array($account['code'], $niveles[$i])) {
				$bandera++;
			}
		}
	} else {
		$bandera++;
	}
	$clBandera = bandera_closing_balance($account);
  $CI =& get_instance();
	$counter = $c;
	if (!empty($account)) {
		if (strlen($account['code']) > 0 && $bandera != 0 && ((isset($_POST['hidezero']) && $clBandera != 0) || !isset($_POST['hidezero'])))
		{
			echo '<tr class="tr-group">';
			echo '<td class="td-group">';
			echo '<b>'.($CI->functionscore->toCodeWithName($account['code'], $account['name'])).'</b>';
			echo '</td>';
			if ($account['cl_total'] < 0) {
				$na = "negative-amount";
			} else {
				$na = "";
			}
			echo '<td class="text-right '.$na.'">';
			echo '<b>'.$CI->functionscore->toCurrency(($account['cl_total'] < 0 ? 'C' : 'D'), ($account['cl_total'] < 0 ? ($account['cl_total'] * -1) : $account['cl_total'])).'</b>';
			echo '</td>';
			echo '</tr>';
		}
		if (isset($account['children_groups']) && count($account['children_groups']) > 0) {
			foreach ($account['children_groups'] as $id => $data)
			{
				$counter++;
				account_st_short($data, $counter, $dc_type, $niveles);
				$counter--;
			}
		}
			
		if (isset($account['children_ledgers']) && count($account['children_ledgers']) > 0 && $nivelBalancesheet == "")
		{
			$counter++;
			foreach ($account['children_ledgers'] as $id => $data)
			{
				if ((isset($_POST['hidezero']) && $data['cl_total'] > 0) || !isset($_POST['hidezero'])) {
					echo '<tr class="tr-ledger">';
					echo '<td class="td-ledger">';
					echo anchor('reports/ledgerstatement/ledgerid/'.$data['id'], $CI->functionscore->toCodeWithName($data['code'], $data['name']));
					echo '</td>';
					if ($data['cl_total'] < 0) {
						$na = "negative-amount";
					} else {
						$na = "";
					}
					echo '<td class="text-right '.$na.'">';
					echo $CI->functionscore->toCurrency(($data['cl_total'] < 0 ? 'C' : 'D'), ($data['cl_total'] < 0 ? ($data['cl_total'] * -1) : $data['cl_total']));
					echo '</td>';
					echo '</tr>';
				}
			}
		$counter--;
		}
	}
}

function bandera_closing_balance($data, $bandera = 0) {
	$data = isset($data[0]) ? $data[0] : $data;
	if (!empty($data)) {
		if (isset($data['children_groups']) && count($data['children_groups']) > 0) {
			foreach ($data['children_groups'] as $id => $grupo) {
				if (isset($grupo['children_groups']) && count($grupo['children_groups']) > 0) {
					$bandera = bandera_closing_balance($grupo, $bandera);
				} else if ($grupo['cl_total'] != 0) {
					$bandera++;
				}
			}
		} else if (isset($data['children_ledgers']) && count($data['children_ledgers']) > 0) {
			foreach ($data['children_ledgers'] as $key => $ledger) {
				if ($ledger['cl_total'] != 0) {
					$bandera++;
				}
			}
		} else if ($data['cl_total'] != 0) {
			$bandera++;
		}
	}
	return $bandera;
}
function print_space($count)
{
	$html = '';
	for ($i = 1; $i <= $count; $i++) {
		$html .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
	}
	return $html;
}
$gross_total = 0;
$positive_gross_pl = 0;
$net_expense_total = 0;
$net_income_total = 0;
$positive_net_pl = 0;
?>
<script type="text/javascript">
$(document).ready(function() {
	$("#accordion").accordion({
		collapsible: true,
		<?php
			if (isset($options) && $options == false) {
				echo 'active: false';
			}
		?>
	});
	$(function () {
	  $('[data-toggle="tooltip"]').tooltip()
	})
	//* Calculate date range in javascript */
	startDate = new Date(<?php echo strtotime($this->mAccountSettings->fy_start) * 1000; ?>  + (new Date().getTimezoneOffset() * 60 * 1000));
	endDate = new Date(<?php echo strtotime($this->mAccountSettings->fy_end) * 1000; ?>  + (new Date().getTimezoneOffset() * 60 * 1000));
	$('#ProfitlossOpening').on('ifChanged', function(event) {
	    if (event.target.checked) {
			$('#ProfitlossStartdate').prop('disabled', true);
			$('#ProfitlossEnddate').prop('disabled', true);
		} else {
			$('#ProfitlossStartdate').prop('disabled', false);
			$('#ProfitlossEnddate').prop('disabled', false);
		}
	});
	/* Setup jQuery datepicker ui */
	$('#ProfitlossStartdate').datepicker({
		minDate: startDate,
		maxDate: endDate,
		dateFormat: '<?php echo $this->mDateArray[1]; ?>',
		numberOfMonths: 1,
		monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Juv', 'Vie', 'Sáb'],
        dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],
		onClose: function(selectedDate) {
			$("#ProfitlossEnddate").datepicker("option", "minDate", startDate).datepicker("option", "maxDate", endDate);
			$("#ProfitlossStartdate").datepicker("option", "minDate", startDate).datepicker("option", "maxDate", endDate);
		},
		onBlur: function(selectedDate) {
			$("#ProfitlossEnddate").datepicker("option", "minDate", startDate).datepicker("option", "maxDate", endDate);
			$("#ProfitlossStartdate").datepicker("option", "minDate", startDate).datepicker("option", "maxDate", endDate);
		}
	});
	$('#ProfitlossEnddate').datepicker({
		minDate: startDate,
		maxDate: endDate,
		dateFormat: '<?php echo $this->mDateArray[1]; ?>',
		numberOfMonths: 1,
		monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Juv', 'Vie', 'Sáb'],
        dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],
		onClose: function(selectedDate) {
			$("#ProfitlossStartdate").datepicker("option", "minDate", startDate).datepicker("option", "maxDate", endDate);
			$("#ProfitlossEnddate").datepicker("option", "minDate", startDate).datepicker("option", "maxDate", endDate);
		},
		onBlur: function(selectedDate) {
			$("#ProfitlossStartdate").datepicker("option", "minDate", startDate).datepicker("option", "maxDate", endDate);
			$("#ProfitlossEnddate").datepicker("option", "minDate", startDate).datepicker("option", "maxDate", endDate);
		}
	});
	$('.submit').on('click', function(){
		$('#loader').fadeIn();
	});
	$(document).on('keypress', 'form', function(e){
        if(e == 13){
          return false;
        }
      });
  $(document).on('keypress', 'input', function(e){
    if(e.which == 13){
      return false;
    }
  });
});
$(".ledger-dropdown").select2({width:'100%'});
	$(document).on('click', '.export', function(){
		type_export = $(this).data('export');
		if (type_export == "xls") {
			$('#formprofitloss').attr('action', 'reports/profitloss/download/xls').attr('target', '_blank').submit();
		} else if (type_export == "pdf") {
			$('#formprofitloss').attr('action', 'reports/profitloss/download/pdf').attr('target', '_blank').submit();
		} else if (type_export == "pdfprint") {
			$('#formprofitloss').attr('action', 'reports/profitloss/download/pdfprint').attr('target', '_blank').submit();
		}
		setTimeout(function() {
			resetear_action();
		}, 5000);
	});
	function resetear_action(){
		$('#formprofitloss').attr('action', 'reports/profitloss').attr('target', '_self');
	}
</script>
	<div class="row wrapper border-bottom white-bg page-heading">
	  <div class="col-lg-8">
	    <h2><?php echo $page_title; ?></h2>
	    <ol class="breadcrumb">
	      <li>
	        <a href="login/index">Inicio</a> <?php //En el controlador en la función llamada, se revisa si el usuario está logueado y lo envía a la vista principal según permisos ?>
	      </li>
	      <li class="active">
	        <strong><?php echo $page_title; ?></strong>
	      </li>
	    </ol>
	  </div><!-- /.col -->
	  <div class="col-lg-4">
	  </div>
	</div><!-- /.row -->
    <div class="row wrapper wrapper-content animated fadeInRight">
      <div class="box float-e-margins">
        <div class="ibox-content contentBackground">
          <div class="col-sm-12">
            <div class="box-body">
				<div id="accordion">
					<h3>Options</h3>
					<div class="profitandloss form">
						<?php echo form_open('', array('id' => 'formprofitloss')); ?>
						<div class="row">
							<div class="col-md-3">
								<div style="margin-top: 30px;">
									<input type="checkbox" id="BalancesheetOpening" name="opening" class="checkbox skip" <?= (isset($_POST['opening'])) ? "checked" :"" ?>>
									<label for="BalancesheetOpening"><?= lang('show_op_bs_title'); ?></label>
		                        </div>
							</div>
							<div class="col-md-3">
								<div style="margin-top: 30px;">
									<input type="checkbox" id="hidezero" name="hidezero" class="checkbox skip" <?= (isset($_POST['hidezero'])) ? "checked" :"" ?>>
									<label for="hidezero"><?= lang('hide_zero_accounts'); ?></label>
		                        </div>
							</div>
							<div class="col-md-3">
								<div style="margin-top: 30px;">
									<input type="checkbox" id="skip_entries_disapproved" name="skip_entries_disapproved" class="checkbox skip" <?= (isset($_POST['skip_entries_disapproved'])) ? "checked" :"" ?> value='TRUE'>
									<label for="skip_entries_disapproved"><?= lang('skip_entries_disapproved'); ?></label>
		                        </div>
							</div>
							<div class="col-md-3">
								<div>
									<label for="nivelBalancesheet"><?= lang('nivel_balance_sheet_span'); ?></label>
									<select class="form-control ledger-dropdown" id="nivelBalancesheet" name="nivelBalancesheet">
										<option value="">Todos</option>
										<?php foreach ($this->niveles as $numnivel => $nivel): ?>
											<option value="<?= $numnivel ?>" <?= (isset($_POST['nivelBalancesheet']) &&  $_POST['nivelBalancesheet'] == $numnivel) ? "selected='true'" : "" ?>><?= $numnivel ?></option>
										<?php endforeach ?>
									</select>
		                        </div>
							</div>
							<hr class="col-sm-11">
							<div class="col-md-3">
								<div class="form-group">
									<label><?= lang('start_date'); ?></label>
				                    <div class="input-group">
										<input id="ProfitlossStartdate" type="text" name="startdate" class="form-control" value="<?= (isset($_POST['startdate'])) ? $_POST['startdate'] : $this->functionscore->dateFromSql($this->mAccountSettings->fy_start) ?>">
				                        <div class="input-group-addon">
				                            <i>
				                                <div class="fa fa-info-circle" data-toggle="tooltip" title="<?= lang('start_date_span');?>">
				                                </div>
				                            </i>
				                        </div>
				                    </div>
				                    <!-- /.input group -->
				                </div>
				                <!-- /.form group -->
							</div>
							<div class="col-md-3">
								<div class="form-group">
									<label><?= lang('end_date'); ?></label>
				                    <div class="input-group">
										<input id="ProfitlossEnddate" type="text" name="enddate" class="form-control" value="<?= (isset($_POST['enddate'])) ? $_POST['enddate'] : $this->functionscore->dateFromSql($this->end_date_filter) ?>">
				                        <div class="input-group-addon">
				                            <i>
				                                <div class="fa fa-info-circle" data-toggle="tooltip" title="<?= lang('end_date_span');?>">
				                                </div>
				                            </i>
				                        </div>
				                    </div>
				                    <!-- /.input group -->
				                </div>
				                <!-- /.form group -->
							</div>
							<?php if (isset($cost_centers) && $cost_centers): ?>
								<div class="col-md-3">
									<div>
										<label for="cost_center"><?= lang('cost_center_label'); ?></label>
										<select class="form-control ledger-dropdown" id="cost_center" name="cost_center">
											<option value="">Todos</option>
											<?php foreach ($cost_centers as $cost_center): ?>
												<option value="<?= $cost_center['id'] ?>" <?= (isset($_POST['cost_center']) &&  $_POST['cost_center'] == $cost_center['id']) ? "selected='true'" : "" ?>><?= $cost_center['name'] ?></option>
											<?php endforeach ?>
										</select>
			                        </div>
								</div>
							<?php endif ?>
						</div>
						<div class="form-group">
							<input type="submit" name="submit" class="btn btn-success pull-right submit" value="<?= lang('create_account_submit_button'); ?>">
						</div>
						<?php form_close();  ?>
					</div>
				</div>
				<br />
				<div class="btn-group" role="group">
					<button class="btn btn-default export" data-export="xls">XLS</button>
					<button class="btn btn-default export" data-export="pdf">PDF</button>
					<button class="btn btn-default export" data-export="pdfprint">Imprimir</button>
				</div>
				<div id="section-to-print">
					<div class="subtitle text-center">
					<b><?php echo $subtitle ?></b>
				</div>

				<?php if (isset($options) && $options): ?>
					<table class="table">
						<tr>
							<!-- Gross Expenses -->
							<td class="table-top width-50">
								<table class="table stripped">
									<tr>
										<th style="width: 50%;"><?php echo lang('profit_loss_gi'); ?></th>
										<th style="width: 50%;" class="text-right"><?php echo lang('amount'); ?><?php echo 'Monto (' . $this->mAccountSettings->currency_symbol . ')'; ?></th>
									</tr>
									<?php echo account_st_short($pandl['gross_expenses'], $c = -1, 'D', $this->niveles); ?>
								</table>
							</td>
							<!-- Gross Incomes -->
							<td class="table-top width-50">
								<table class="table stripped">
									<tr>
										<th style="width: 50%;"><?php echo lang('profit_loss_ge'); ?></th>
										<th style="width: 50%;" class="text-right"><?php echo lang('amount'); ?><?php echo 'Monto (' . $this->mAccountSettings->currency_symbol . ')'; ?></th>
									</tr>
									<?php echo account_st_short($pandl['gross_incomes'], $c = -1, 'C', $this->niveles); ?>
									<?php echo account_st_short($pandl['gross_incomes2'], $c = -1, 'C', $this->niveles); ?>
									<?php if (isset($pandl['gross_incomes3'])): ?>
										<?php echo account_st_short($pandl['gross_incomes3'], $c = -1, 'C', $this->niveles); ?>
									<?php endif ?>
								</table>
							</td>
						</tr>
						<tr>
							<td class="table-top width-50">
								<div class="report-tb-pad"></div>
								<table class="table stripped">
									<?php
									/* Gross Expense Total */
									$gross_total = $pandl['gross_expense_total'];
									if ($this->functionscore->calculate($pandl['gross_expense_total'], 0, '>=')) {
										echo '<tr class="bold-text">';
										echo '<th>' . lang('profit_loss_tgi') . '</th>';
										echo '<td class="text-right">' . $this->functionscore->toCurrency('D', $pandl['gross_expense_total']) . '</td>';
										echo '</tr>';
									} else {
										if ($pandl['gross_expense_total'] < 0) {
											$na = "negative-amount";
										} else {
											$na = "";
										}
										echo '<tr class=" bold-text">';
										echo '<th>' . lang('profit_loss_tgi') . '</th>';
										echo '<td class="text-right '.$na.'" data-toggle="tooltip" data-placement="top" title="Experando balance Débito">' . $this->functionscore->toCurrency('D', $pandl['gross_expense_total']) . '</td>';
										echo '</tr>';
									}
									?>
									<tr class="bold-text">
										<?php
										/* Gross Profit C/D */
										if ($this->functionscore->calculate($pandl['gross_pl'], 0, '>=')) {
											echo '<td>' . lang('profit_loss_gp') . '</td>';
											echo '<td class="text-right">' . $this->functionscore->toCurrency('', $pandl['gross_pl']) . '</td>';
											$gross_total = $this->functionscore->calculate($gross_total, $pandl['gross_pl'], '+');
										} else {
											echo '<td>&nbsp</td>';
											echo '<td>&nbsp</td>';
										}
										?>
									</tr>
									<tr class="bold-text bg-filled">
										<th><?php echo lang('profit_loss_t'); ?></th>
										<?php
										if ($gross_total < 0) {
											$na = "negative-amount";
										} else {
											$na = "";
										}
										 ?>
										<td class="text-right <?= $na ?>"><?php echo $this->functionscore->toCurrency('D', $gross_total); ?></td>
									</tr>
								</table>
							</td>
							<td class="table-top width-50">
								<div class="report-tb-pad"></div>
								<table class="table stripped">
									<?php
									/* Gross Income Total */
									$gross_total = $pandl['gross_income_total'];
									if ($this->functionscore->calculate($pandl['gross_income_total'], 0, '>=')) {
										if ($pandl['gross_income_total'] > 0) {
											$na = "negative-amount";
										} else {
											$na = "";
										}
										echo '<tr class="bold-text">';
										echo '<th>' . lang('profit_loss_tge') . '</th>';
										echo '<td class="text-right '.$na.'">' . $this->functionscore->toCurrency('C', $pandl['gross_income_total']) . '</td>';
										echo '</tr>';
									} else {
										echo '<tr class=" bold-text">';
										echo '<th>' . lang('profit_loss_tge') . '</th>';
										echo '<td class="text-right " data-toggle="tooltip" data-placement="top" title="Esperando balance Crédito">' . $this->functionscore->toCurrency('C', $pandl['gross_income_total']) . '</td>';
										echo '</tr>';
									}
									?>
									<tr class="bold-text">
										<?php
										/* Gross Loss C/D */
										if ($this->functionscore->calculate($pandl['gross_pl'], 0, '>=')) {
											echo '<td>&nbsp</td>';
											echo '<td>&nbsp</td>';
										} else {
											echo '<td>' . lang('profit_loss_glcd') . '</td>';
											$positive_gross_pl = $this->functionscore->calculate($pandl['gross_pl'], 0, 'n');
											echo '<td class="text-right">' . $this->functionscore->toCurrency('', $positive_gross_pl) . '</td>';
											$gross_total = $this->functionscore->calculate($gross_total, $positive_gross_pl, '+');
										}
										?>
									</tr>
									<tr class="bold-text bg-filled">
										<th><?php echo lang('profit_loss_t'); ?></th>
										<?php
										if ($gross_total > 0) {
											$na = "negative-amount";
										} else {
											$na = "";
										}
										 ?>
										<td class="text-right <?= $na ?>"><?php echo $this->functionscore->toCurrency('C', $gross_total); ?></td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				<?php endif ?>
					
				</div>
      </div>
    </div>
  </div>
<!-- /.row -->
</div>
<!-- /.content -->