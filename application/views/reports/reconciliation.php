<script type="text/javascript">
$(document).ready(function() {

	$("#accordion").accordion({
		collapsible: true,
		<?php
			if (isset($options) && $options == false) {
				echo 'active: false';
			}
		?>
	});

	$(document.body).on("change","#ReportLedgerId",function(){
		if(this.value == 0){
			$('#ReportStartdate').prop('disabled', true);
			$('#ReportEnddate').prop('disabled', true);
		} else {
			$('#ReportStartdate').prop('disabled', false);
			$('#ReportEnddate').prop('disabled', false);
		}
	});
	$('#ReportLedgerId').trigger('change');

	/* Calculate date range in javascript */
	startDate = new Date(<?php echo strtotime($this->mAccountSettings->fy_start) * 1000; ?>  + (new Date().getTimezoneOffset() * 60 * 1000));
	endDate = new Date(<?php echo strtotime($this->mAccountSettings->fy_end) * 1000; ?>  + (new Date().getTimezoneOffset() * 60 * 1000));

	/* Setup jQuery datepicker ui */
	$('#ReportStartdate').datepicker({
		minDate: startDate,
		maxDate: endDate,
		dateFormat: '<?php echo $this->mDateArray[1]; ?>',
		numberOfMonths: 1,
		language: 'ES',
		monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Juv', 'Vie', 'Sáb'],
        dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],
		onClose: function(selectedDate) {
			if (selectedDate) {
				$("#ReportEnddate").datepicker("option", "minDate", selectedDate);
			} else {
				$("#ReportEnddate").datepicker("option", "minDate", startDate);
			}
		}
	});
	$('#ReportEnddate').datepicker({
		minDate: startDate,
		maxDate: endDate,
		dateFormat: '<?php echo $this->mDateArray[1]; ?>',
		numberOfMonths: 1,
		language: 'ES',
		monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Juv', 'Vie', 'Sáb'],
        dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],
		onClose: function(selectedDate) {
			if (selectedDate) {
				$("#ReportStartdate").datepicker("option", "maxDate", selectedDate);
			} else {
				$("#ReportStartdate").datepicker("option", "maxDate", endDate);
			}
		}
	});

	/* Setup jQuery datepicker ui */

	var ReportLedgerId = $("#ReportLedgerId").select2({
		width:'100%',
		placeholder: "Please select a country"
	});
	// if( $('#ReportLedgerId').has('option').length < 2) {
	// 	ReportLedgerId.select2({
	// 		placeholder: 'qwerty'
	// 	});
	// }

$(document).on('click', '#submitform', function(){

	$('#loader').fadeIn();

	var datos = $('#ledgertable input').serialize();
		datos = datos+"&submitrec=1";

	$.ajax({
		url : "<?= base_url().'reports/reconciliation' ?>",
		type : "post",
		data : datos,
		success : function(data){
			location.reload();
		}
	});

});

});
</script>
	<div class="row wrapper border-bottom white-bg page-heading">
	  <div class="col-lg-8">
	    <h2><?php echo $page_title; ?></h2>
	    <ol class="breadcrumb">
	      <li>
	        <a href="login/index">Inicio</a> <?php //En el controlador en la función llamada, se revisa si el usuario está logueado y lo envía a la vista principal según permisos ?>
	      </li>
	      <li class="active">
	        <strong><?php echo $page_title; ?></strong>
	      </li>
	    </ol>
	  </div><!-- /.col -->
	  <div class="col-lg-4">
	  </div>
	</div><!-- /.row -->
    <div class="row wrapper wrapper-content animated fadeInRight">
      <div class="box float-e-margins">
        <div class="ibox-content contentBackground">
          <div class="col-sm-12">
            <div class="box-body">

            	<div class="reconciliation form">
					<?= form_open(base_url().'reports/reconciliation', array('id' => 'formReconciliation', 'method' => 'post', 'ccept-charset' => 'utf-8')); ?>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label><?= lang('ledger_acc_name'); ?></label>
									<select class="form-control" id="ReportLedgerId" name="ledger_id">
										<option></option>
										<?php foreach ($ledgers as $id => $ledger): ?>
											<option value="<?= $id; ?>" <?= ($id < 0) ? 'disabled' : "" ?> <?= ($this->input->post('ledger_id') == $id) ?'selected':''?>><?= $ledger; ?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>

							<div class="col-md-3">
								<div class="form-group">
									<label><?= lang('start_date'); ?></label>
				                    <div class="input-group">
										<input id="ReportStartdate" type="text" name="startdate" class="form-control">
				                        <div class="input-group-addon">
				                            <i>
				                                <div class="fa fa-info-circle" data-toggle="tooltip" title="<?=lang('start_date_span');?>">
				                                </div>
				                            </i>
				                        </div>
				                    </div>
				                    <!-- /.input group -->
				                </div>
				                <!-- /.form group -->
							</div>

							<div class="col-md-3">
								<div class="form-group">
									<label><?= lang('end_date') ;?></label>
				                    <div class="input-group">
										<input id="ReportEnddate" type="text" name="enddate" class="form-control">
				                        <div class="input-group-addon">
				                            <i>
				                                <div class="fa fa-info-circle" data-toggle="tooltip" title="<?=lang('end_date_span');?>">
				                                </div>
				                            </i>
				                        </div>
				                    </div>
				                    <!-- /.input group -->
				                </div>
				                <!-- /.form group -->
							</div>
						</div>
						<div class="form-group">
							<label><input id="" type="checkbox" name="showall" id="ReportShowAll" <?= (isset($_POST['showall'])) ? "checked" : ""; ?> class="form-control">    <?= lang('show_all_entries');?></label>
						</div>
						<div class="form-group">
							<input type="submit" name="submit_ledger" class="btn btn-success" value="<?=lang('create_account_submit_button');?>">
						</div>
				</div>

				<?php if ($showEntries) { ?>
					<div class="subtitle">
						<?php echo $subtitle; ?>
					</div>
					<div class="row" style="margin-bottom: 10px;">
						<div class="col-md-6">
							<table class="table summary stripped table-condensed">
								<tr>
									<th class="td-fixwidth-summary"><?php echo ('Bank or cash account'); ?></th>
									<td>

										<?php
											if ($ledger_data['type'] == 1) {
												echo lang('yes');
											} else {
												echo lang('no');
											}
										?>
									</td>
								</tr>
								<tr>
									<th class="td-fixwidth-summary"><?php echo ('Notes'); ?></th>
									<td><?php echo ($ledger_data['notes']); ?></td>
								</tr>
							</table>
						</div>
						<div class="col-md-6">	
							<table class="table summary stripped table-condensed">
								<tr>
									<th class="td-fixwidth-summary"><?php echo $opening_title; ?></th>
									<td><?php echo $this->functionscore->toCurrency($op['dc'], $op['amount']); ?></td>
								</tr>
								<tr>
									<th class="td-fixwidth-summary"><?php echo $closing_title; ?></th>
									<td><?php echo $this->functionscore->toCurrency($cl['dc'], $cl['amount']); ?></td>
								</tr>
								<tr>
									<th><?php echo ('Debit ') . $recpending_title; ?></th>
									<td><?php echo $this->functionscore->toCurrency('D', $rp['dr_total']); ?></td>
								</tr>
								<tr>
									<th><?php echo ('Credit ') . $recpending_title; ?></th>
									<td><?php echo $this->functionscore->toCurrency('C', $rp['cr_total']); ?></td>
								</tr>
							</table>
						</div>
					</div>
					<table class="table stripped" id="ledgertable">
						<thead>
							<tr>
		            			<th>id</th>
		            			<th>Label</th>
		            			<th><?= lang('entries_views_index_th_number'); ?></th>
								<th><?= lang('entries_views_index_th_date'); ?></th>
								<th><?= lang('entries_views_index_th_note'); ?></th>
								<th><?= lang('entries_views_index_th_type'); ?></th>
								<th><?= lang('entries_views_index_th_tag'); ?></th>
								<th><?= lang('entries_views_index_th_debit_amount'); ?></th>
								<th><?= lang('entries_views_index_th_credit_amount'); ?></th>
		            			<th><?= lang('entries_views_index_th_state'); ?></th>
							</tr>
						</thead>
						<tbody>
							
						</tbody>
						<tfoot>
							<tr>
		            			<th>id</th>
		            			<th>Label</th>
		            			<th><?= lang('entries_views_index_th_number'); ?></th>
								<th><?= lang('entries_views_index_th_date'); ?></th>
								<th><?= lang('entries_views_index_th_note'); ?></th>
								<th><?= lang('entries_views_index_th_type'); ?></th>
								<th><?= lang('entries_views_index_th_tag'); ?></th>
								<th><?= lang('entries_views_index_th_debit_amount'); ?></th>
								<th><?= lang('entries_views_index_th_credit_amount'); ?></th>
		            			<th><?= lang('entries_views_index_th_state'); ?></th>
							</tr>
						</tfoot>
					</table>
					<br />

					<?php
						echo form_hidden('submitrec', 1);
					?>
					<div class="form-group">
						<button type="button" id="submitform" class="btn btn-primary"><?=lang('Reconcile');?></button>
					</div>
				<?php } ?>
				<?= form_close(); ?>
            </div>
          </div>
      </div>
      <!-- /.row -->
    </div>
    <!-- /.content -->


<?php if (isset($_POST)): ?>
<script type="text/javascript">
	dataset1 = $('#ledgertable').DataTable({
	    ajax: {
	        method: 'POST',
	        url: '<?=base_url("reports/getEntriesReconciliation"); ?>',
	        data: {
	        	'ledger_id' : $('#ReportLedgerId').val(),
	        	'startdate' : $('#ReportStartdate').val(),
	        	'enddate' : $('#ReportEnddate').val(),
	        	'showall' : <?= (isset($_POST['showall'])) ? "1" : "0" ?>,
	        },
	      },
	    columns:[
	        { data: 'entry_id'},
	        { data: 'entryTypeLabel'},
	        { data: 'number'},
	        { data: 'date'},
	        { data: 'note'},
	        { data: 'type'},
	        { data: 'tag'},
	        { data: 'dr'},
	        { data: 'cr'},
	        { data: 'form'},
	      ],
        order: [ 0, 'asc' ],
        order: [ 1, 'desc' ],
	    pageLength: 25,
	    responsive: true,
	    dom : '<"html5buttons" B>lr<"containerBtn"><"inputFiltro"f>tip',
		buttons : [
					{extend:'excel', title:'ledgerstatement', className:'btnExportarExcel', exportOptions: {columns : [2,3,4,5,6,7,8]}},
					{extend:'pdf', title:'ledgerstatement', className:'btnExportarPdf', exportOptions: {columns : [2,3,4,5,6,7,8]}},
				   ],
	    oLanguage: {
	      sLengthMenu: 'Mostrando _MENU_ registros por página',
	      sZeroRecords: 'No se encontraron registros',
	      sInfo: 'Mostrando _START_ a _END_ de _TOTAL_ registros',
	      sInfoEmpty: 'Mostrando 0 a 0 de 0 registros',
	      sInfoFiltered: '(Filtrado desde _MAX_ registros)',
	      sSearch:         'Buscar: ',
	      oPaginate:{
	        sFirst:    'Primero',
	        sLast:     'Último',
	        sNext:     'Siguiente',
	        sPrevious: 'Anterior'
	      },
	    },
	    columnDefs: [
                {
                    "targets": [0, 1],
                    "visible": false,
                    "searchable": false
                }
            ],
	    "preDrawCallback": function( settings ) {
				$('#loader').fadeIn();
	      }
	    }).on("draw", function(){

	     $('#loader').fadeOut();


	        $('#ledgertable tbody tr td:nth-child(-n+7)').on('click', function(){
	          var tr = $(this).parent();
	          var datas = dataset1.row(tr).data();
	          
	          entry_type = datas.entryTypeLabel;
	          entry_id = datas.entry_id;
	          window.open('<?= base_url("entries/view/") ?>'+entry_type+'/'+entry_id, '_blank');

	        });

	        $('.recdate').datepicker({
				minDate: startDate,
				maxDate: endDate,
				dateFormat: '<?php echo $this->mDateArray[1]; ?>',
				numberOfMonths: 1,
				language: 'ES',
				monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
		        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
		        dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Juv', 'Vie', 'Sáb'],
		        dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],
				onClose: function(selectedDate) {
					if (selectedDate) {
						$("#ReportEnddate").datepicker("option", "minDate", selectedDate);
					} else {
						$("#ReportEnddate").datepicker("option", "minDate", startDate);
					}
				}
			});

	        var btnAcciones = '<div class="dropdown pull-right" id="">'+
	        		'<button class="btn btn-primary btn-sm btn-outline" type="button" id="accionesTabla" data-toggle="dropdown" aria-haspopup="true">Acciones<span class="caret"></span></button>'+
	        		'<ul class="dropdown-menu pull-right" aria-labelledby="accionesTabla">'+
	        		'<li><a onclick="$(\'.btnExportarExcel\').click()"><span class="fa fa-file-excel-o"></span> Exportar a XLS </a></li>'+
	        		'<li><a onclick="$(\'.btnExportarPdf\').click()"><span class="fa fa-file-pdf-o"></span> Exportar a PDF </a></li>'+
	        		'</ul></div>';

			$('.containerBtn').html(btnAcciones);

	   });
</script>
<?php endif ?>

