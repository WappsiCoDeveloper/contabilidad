
<div class="row wrapper border-bottom white-bg page-heading">
  	<div class="col-lg-12">
    	<br>
    	<ol class="breadcrumb">
      		<li>
        		<a href="login/index">Inicio</a> 
      		</li> 				
      		<li >  
			  	<a href="<?php echo $titles['url']; ?>"><?php echo $titles['title']; ?></a> 
      		</li>
			<li class="active">
        		<strong><?php echo $page_title; ?></strong>
      		</li>
    	</ol>
  	</div><!-- /.col -->
</div>

<div class="row wrapper border-bottom white-bg page-heading">
  	<div class="col-lg-12">
    	<h2>Formato: <strong><?php echo $Formats[0]['name']; ?> </strong> </h2>
		<p><?php echo $Formats[0]['description']; ?></p>
  	</div><!-- /.col -->
</div>


<div class="row wrapper wrapper-content animated fadeInRight">
	<div class="ibox float-e-margins">
		<div class="ibox-content contentBackground">
			<div class="row">
				<div class="col-lg-8">
					<h2>Campos Contenido </h2>
				</div>
				<div class="col-lg-4 text-right">
					<button class="btn btn-success" href="#modalAddForms" data-toggle="modal" id="AddForms"><span class="fa fa-plus "></span> <?= lang('Exogenous_Formats_content_add'); ?></button>
				</div>
			</div>
			<br>
			<table class="table" id="tableFormats">
				<thead>
					<tr>
						<th><?= lang('Exogenous_Formats_content_Items_name'); ?></th>
						<th><?= lang('Exogenous_Formats_content_Items_size'); ?></th>
						<th><?= lang('Exogenous_Formats_content_Items_obs'); ?></th>
						<th><?= lang('Exogenous_Formats_content_Items_order'); ?></th>
						<th><?= lang('Exogenous_Formats_content_Items_atrib'); ?></th>
						<th><?= lang('Exogenous_Formats_content_Items_type'); ?></th>
						<th><?= lang('dashboard_accountlist_table_actions') ?></th>
					</tr>
				</thead>
				<tbody>
					<?php $order = 0;?>
					<?php foreach ($contents as $content): ?>
						<?php if($order < $content['order_by']){ $order = $content['order_by']; } ?>
						<tr>
							<td class="text-center"> <?php echo $content['name']; ?></td>
							<td class="text-center"> <?php echo $content['size']; ?></td>
							<td class="text-left"> <?php echo $content['observation']; ?></td>
							<td class="text-left"> <?php echo $content['order_by']; ?></td>
							<td class="text-center"> <?php echo $content['attribute']; ?></td>
							<td class="text-center"> <?php echo $content['nameType']; ?></td>
							<td class="text-center"> 
								<div class="btn-group">
								  	<div class="dropdown">
										<button class="btn btn-success btn-sm" type="button" id="accionesProducto" data-toggle="dropdown" aria-haspopup="true">
											Acciones
											<span class="caret"></span>
										</button>
										<ul class="dropdown-menu pull-right" aria-labelledby="accionesProducto" style="width:220%;">
											<li>
												<a href="#modalEditFormats" data-toggle="modal" data-id="<?php echo $content['id']; ?>" 
												data-size="<?php echo $content['size']; ?>" 
												data-order="<?php echo $content['order_by']; ?>" 
												data-name="<?php echo $content['name']; ?>" 
												data-obs="<?php echo $content['observation']; ?>" 
												data-attribute="<?php echo $content['attribute']; ?>" 
												data-nameType="<?php echo $content['nameType']; ?>" class="no-hover" id="editcostcenter">
													<i class="fa fa-pencil"></i> &nbsp; <?= lang('entries_views_index_th_actions_edit_btn') ?>
												</a>
											</li>
										</ul> 
									</div>
								</div>
							</td>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		</div>
	</div>
</div>


<div class="modal fade" tabindex="-1" role="dialog" id="modalAddForms" aria-labelledby="AddForms">
  	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form id="formAddForms" method="post" action="Exogenous/CreateFormatsContent" autocomplete="off" >
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title"><?= lang('Exogenous_Formats_content_add'); ?></h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label><?= lang('Exogenous_Formats_content_Items_name') ?></label>
						<input type="text" name="name" id="name" class="form-control" required>
					</div>
					<div class="form-group">
						<label><?= lang('Exogenous_Formats_content_Items_size') ?></label>
						<input type="text" name="size" id="size" class="form-control" required>
					</div>
					<div class="form-group">
						<label><?= lang('Exogenous_Formats_content_Items_obs') ?></label>
						<input type="text" name="obs" id="obs" class="form-control" >
					</div>
					<div class="form-group">
						<label><?= lang('Exogenous_Formats_content_Items_order') ?></label>
						<input type="text" name="order" id="order" class="form-control" required value="<?php echo $order+1;?>">
					</div>
					<div class="form-group">
						<label><?= lang('Exogenous_Formats_content_Items_type') ?></label>
						<select class="form-control" name="type" id="type" required>
							<option value=""><?= lang('select_a_ledger') ?></option>  
							<?php foreach ($types as $type): ?>
								<option value=" <?php echo $type['id']; ?>"> <?php echo $type['name']; ?></option>
							<?php endforeach; ?>							
						</select>
					</div>
					<div class="form-group">
						<label><?= lang('Exogenous_Formats_content_Items_atrib') ?></label>
						<input type="text" name="atrib" id="atrib" class="form-control" required>
					</div>
				</div>
				<div class="modal-footer">
					<input type="hidden" name="cod_format" id="cod_format" value="<?php echo $Formats[0]['id']; ?>">
					<input type="hidden" name="format" id="format" value="<?php echo $Formats[0]['code']; ?>">
					<button class="btn btn-default" data-dismiss="modal"><?=lang('cancel');?></button>
					<button type="submit" class="btn btn-success submit_cost_center" ><?=lang('send');?></button>
				</div>
			</form>
		</div><!-- /.modal-content -->
  	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" tabindex="-1" role="dialog" id="modalEditFormats" aria-labelledby="EditFormats">
  	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form id="formEditFormats" method="post" action="Exogenous/EditFormatsContent" autocomplete="off" >
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title"><?= lang('Exogenous_Formats_content_edit'); ?></h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label><?= lang('Exogenous_Formats_content_Items_name') ?></label>
						<input type="text" name="Editname" id="Editname" class="form-control" required>
					</div>
					<div class="form-group">
						<label><?= lang('Exogenous_Formats_content_Items_size') ?></label>
						<input type="text" name="Editsize" id="Editsize" class="form-control" required>
					</div>
					<div class="form-group">
						<label><?= lang('Exogenous_Formats_content_Items_obs') ?></label>
						<input type="text" name="Editobs" id="Editobs" class="form-control" >
					</div>
					<div class="form-group">
						<label><?= lang('Exogenous_Formats_content_Items_order') ?></label>
						<input type="text" name="Editorder" id="Editorder" class="form-control" required>
					</div>
					<div class="form-group">
						<label><?= lang('Exogenous_Formats_content_Items_type') ?></label>
						<select class="form-control" name="Edittype" id="Edittype" required >
							<option value=""><?= lang('select_a_ledger') ?></option>  
							<?php foreach ($types as $type): ?>
								<option value="<?php echo $type['id']; ?>"> <?php echo $type['name']; ?></option>
							<?php endforeach; ?>							
						</select>
					</div>
					<div class="form-group">
						<label><?= lang('Exogenous_Formats_content_Items_atrib') ?></label>
						<input type="text" name="Editatrib" id="Editatrib" class="form-control" required>
					</div>
				</div>
				<div class="modal-footer">
					<input type="hidden" name="Editcod_format" id="Editcod_format" value="<?php echo $Formats[0]['id']; ?>">
					<input type="hidden" name="Editformat" id="Editformat" value="<?php echo $Formats[0]['code']; ?>">
					<input type="hidden" name="EditIdConten" id="EditIdConten" value="<?php echo $Formats[0]['code']; ?>">
					<button class="btn btn-default" data-dismiss="modal"><?=lang('cancel');?></button>
					<button type="submit" class="btn btn-success submit_cost_center" ><?=lang('send');?></button>
				</div>
			</form>
		</div><!-- /.modal-content -->
  	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<script type="text/javascript" >
	var dataset1 = $('#tableFormats').DataTable({
    pageLength: 25,
    responsive: true,
	order :[[3,'asc']],
    dom : '<"html5buttons" B>lr<"containerBtn"><"inputFiltro"f>tip',
    buttons : [{extend:'excel', title:'Payment Methods', className:'btnExportarExcel', exportOptions: {columns : [0,1,2]}}],
    oLanguage: {
      sLengthMenu: 'Mostrando _MENU_ registros por página',
      sZeroRecords: 'No se encontraron registros',
      sInfo: 'Mostrando _START_ a _END_ de _TOTAL_ registros',
      sInfoEmpty: 'Mostrando 0 a 0 de 0 registros',
      sInfoFiltered: '(Filtrado desde _MAX_ registros)',
      sSearch:         'Buscar: ',
      oPaginate:{
        sFirst:    'Primero',
        sLast:     'Último',
        sNext:     'Siguiente',
        sPrevious: 'Anterior'
      }
    }
    });

	$(document).ready(function() {
		
	});

	$('#modalEditFormats').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget);
		id = button.data('id');
		size = button.data('size');
		name = button.data('name');
		order = button.data('order');
		attribute = button.data('attribute');
		nameType = button.data('nameType');
		obs = button.data('obs');
		
		$('#Editsize').val(size);
		$('#Editname').val(name);
		$('#Editobs').val(obs);
		$('#Editorder').val(order);
		$('#Editatrib').val(attribute);
		$('#EditIdConten').val(id);
		$("#Edittype option[value="+nameType+"]").attr('selected', 'selected');
		$("#Edittype").val(nameType);
	});
</script>

<!-- /.content -->