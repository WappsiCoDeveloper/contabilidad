
<div class="row wrapper border-bottom white-bg page-heading">
  	<div class="col-lg-8">
    	<h2><?php echo $page_title; ?></h2>
    	<ol class="breadcrumb">
      		<li>
        		<a href="login/index">Inicio</a> <?php //En el controlador en la función llamada, se revisa si el usuario está logueado y lo envía a la vista principal según permisos ?>
      		</li>
      		<li class="active">
        		<strong><?php echo $page_title; ?></strong>
      		</li>
    	</ol>
  	</div><!-- /.col -->
  	<div class="col-lg-4">
    	<div class="title-action">
		<button class="btn btn-success" href="#modalAddForms" data-toggle="modal" id="AddForms"><span class="fa fa-plus "></span> <?= lang('Exogenous_Formats_add'); ?></button>
    	</div>
  	</div>
</div>

<div class="row wrapper wrapper-content animated fadeInRight">
	<div class="ibox float-e-margins">
		<div class="ibox-content contentBackground">
			<table class="table" id="tableFormats">
				<thead>
					<tr>
						<th><?= lang('Exogenous_Formats_Items_code') ?></th>
						<th><?= lang('Exogenous_Formats_Items_version') ?></th>
						<th><?= lang('Exogenous_Formats_Items_name') ?></th>
						<th><?= lang('Exogenous_Formats_Items_description') ?></th>
						<th><?= lang('Exogenous_Formats_Items_min_amount') ?></th>
						<th><?= lang('Exogenous_Formats_Items_use_tercero') ?></th>
						<th><?= lang('dashboard_accountlist_table_actions') ?></th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($Formats as $Format): ?>
					<?php if($Format['use_tercero']==1) $use = lang('yes'); else $use = lang('no');  ?>
						<tr>
							<td class="text-center"> <?php echo $Format['code']; ?></td>
							<td class="text-center"> <?php echo $Format['version']; ?></td>
							<td class="text-left"> <?php echo $Format['name']; ?></td>
							<td class="text-left"> <?php echo $Format['description']; ?></td>
							<td class="text-center"> <?php echo $Format['min_amount']; ?></td>
							<td class="text-center"> <?php echo $use; ?></td>
							<td class="text-center"> 
								<div class="btn-group">
								  	<div class="dropdown">
										<button class="btn btn-success btn-sm" type="button" id="accionesProducto" data-toggle="dropdown" aria-haspopup="true">
											Acciones
											<span class="caret"></span>
										</button>
										<ul class="dropdown-menu pull-right" aria-labelledby="accionesProducto" style="width:220%;">
											<li>
												<a href="#modalEditFormats" data-toggle="modal" data-id="<?php echo $Format['id']; ?>" data-code="<?php echo $Format['code']; ?>" data-version="<?php echo $Format['version']; ?>" data-name="<?php echo $Format['name']; ?>" data-description="<?php echo $Format['description']; ?>" data-min_amount="<?php echo $Format['min_amount']; ?>" data-use="<?php echo $Format['use_tercero']; ?>" class="no-hover" id="editcostcenter">
													<i class="fa fa-pencil"></i> &nbsp; <?= lang('entries_views_index_th_actions_edit_btn') ?>
												</a>
											</li>
											<li>
												<a href="Exogenous/getFormats/<?php echo $Format['code']; ?>" class="no-hover" >
													<i class="fa fa-eye"></i> &nbsp; <?= lang('edit_permission_view_label') ?>
												</a>
											</li>
										</ul> 
									</div>
								</div>
							</td>
						</tr>
					<?php endforeach; ?>
				</tbody>
				<tfoot>
					<tr>
						<th><?= lang('Exogenous_Formats_Items_code') ?></th>
						<th><?= lang('Exogenous_Formats_Items_version') ?></th>
						<th><?= lang('Exogenous_Formats_Items_name') ?></th>
						<th><?= lang('Exogenous_Formats_Items_description') ?></th>
						<th><?= lang('Exogenous_Formats_Items_min_amount') ?></th>
						<th><?= lang('Exogenous_Formats_Items_use_tercero') ?></th>
						<th><?= lang('dashboard_accountlist_table_actions') ?></th>
					</tr>
				</tfoot>
			</table>
		</div>
	</div>
</div>


<div class="modal fade" tabindex="-1" role="dialog" id="modalAddForms" aria-labelledby="AddForms">
  	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form id="formAddForms" method="post" action="Exogenous/CreateFormats" autocomplete="off" >
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title"><?= lang('Exogenous_Formats_add'); ?></h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label><?= lang('Exogenous_Formats_Items_code') ?></label>
						<input type="text" name="code" id="code" class="form-control" required>
						<span class="text-danger code_exists" style="display: none;"><?= lang('cost_centers_add_code_exists') ?></span>
					</div>
					<div class="form-group">
						<label><?= lang('Exogenous_Formats_Items_version') ?></label>
						<input type="number" name="version" id="version" class="form-control" required>
					</div>
					<div class="form-group">
						<label><?= lang('Exogenous_Formats_Items_name') ?></label>
						<input type="text" name="name" id="name" class="form-control" required>
					</div>
					<div class="form-group">
						<label><?= lang('Exogenous_Formats_Items_description') ?></label>
						<input type="text" name="description" id="description" class="form-control" required>
					</div>
					<div class="form-group">
						<label><?= lang('Exogenous_Formats_Items_min_amount') ?></label>
						<input type="number" name="min_amount" id="min_amount" class="form-control" required>
					</div>
					<div class="form-group">
						<label><?= lang('Exogenous_Formats_Items_use_tercero') ?></label>
						<label class="radio-inline">
							<input type="radio" name="use_tercero" id="use_tercero1" value="1" checked> <?= lang('yes') ?>
						</label>
						<label class="radio-inline">
							<input type="radio" name="use_tercero" id="use_tercero2" value="0" > <?= lang('no') ?>
						</label>
					</div>
				</div>
				<div class="modal-footer">
					<button class="btn btn-default" data-dismiss="modal"><?=lang('cancel');?></button>
					<button type="submit" class="btn btn-success submit_cost_center" ><?=lang('send');?></button>
				</div>
			</form>
		</div><!-- /.modal-content -->
  	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->



<div class="modal fade" tabindex="-1" role="dialog" id="modalEditFormats" aria-labelledby="EditFormats">
  	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form id="formEditFormats" method="post" action="Exogenous/EditFormats" autocomplete="off" >
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title"><?= lang('Exogenous_Formats_add'); ?></h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label><?= lang('Exogenous_Formats_Items_code') ?></label>
						<input type="text" name="code" id="Editcode" class="form-control" required>
						<span class="text-danger code_exists" style="display: none;"><?= lang('cost_centers_add_code_exists') ?></span>
					</div>
					<div class="form-group">
						<label><?= lang('Exogenous_Formats_Items_version') ?></label>
						<input type="number" name="version" id="Editversion" class="form-control" required>
					</div>
					<div class="form-group">
						<label><?= lang('Exogenous_Formats_Items_name') ?></label>
						<input type="text" name="name" id="Editname" class="form-control" required>
					</div>
					<div class="form-group">
						<label><?= lang('Exogenous_Formats_Items_description') ?></label>
						<input type="text" name="description" id="Editdescription" class="form-control" required>
					</div>
					<div class="form-group">
						<label><?= lang('Exogenous_Formats_Items_min_amount') ?></label>
						<input type="number" name="min_amount" id="Editmin_amount" class="form-control" required>
					</div>
					<div class="form-group">
						<label><?= lang('Exogenous_Formats_Items_use_tercero') ?></label>
						<label class="radio-inline">
							<input type="radio" name="use_tercero" id="Edituse_tercero1" value="1" checked> <?= lang('yes') ?>
						</label>
						<label class="radio-inline">
							<input type="radio" name="use_tercero" id="Edituse_tercero2" value="0" > <?= lang('no') ?>
						</label>
					</div>
				</div>
				<div class="modal-footer">
					<input type="hidden" name="cod_formats" id="cod_formats" valeu="">
					<button class="btn btn-default" data-dismiss="modal"><?=lang('cancel');?></button>
					<button type="submit" class="btn btn-success submit_cost_center" ><?=lang('send');?></button>
				</div>
			</form>
		</div><!-- /.modal-content -->
  	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<script type="text/javascript" >
	var dataset1 = $('#tableFormats').DataTable({
    pageLength: 25,
    responsive: true,
    dom : '<"html5buttons" B>lr<"containerBtn"><"inputFiltro"f>tip',
    buttons : [{extend:'excel', title:'Payment Methods', className:'btnExportarExcel', exportOptions: {columns : [0,1,2]}}],
    oLanguage: {
      sLengthMenu: 'Mostrando _MENU_ registros por página',
      sZeroRecords: 'No se encontraron registros',
      sInfo: 'Mostrando _START_ a _END_ de _TOTAL_ registros',
      sInfoEmpty: 'Mostrando 0 a 0 de 0 registros',
      sInfoFiltered: '(Filtrado desde _MAX_ registros)',
      sSearch:         'Buscar: ',
      oPaginate:{
        sFirst:    'Primero',
        sLast:     'Último',
        sNext:     'Siguiente',
        sPrevious: 'Anterior'
      }
    }
    });

	$(document).ready(function() {
		$('#use_tercero1').on('ifChecked', function(event){
		});
		$('#use_tercero2').on('ifChecked', function(event){
		});
	});

	$('#modalEditFormats').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget);
		id = button.data('id');
		version = button.data('version');
		name = button.data('name');
		description = button.data('description');
		code = button.data('code');
		use = button.data('use');
		min_amount = button.data('min_amount');
		$('#Editcode').val(code);
		$('#Editversion').val(version);
		$('#Editname').val(name);
		$('#Editdescription').val(description);
		$('#Editmin_amount').val(min_amount);
		$('#cod_formats').val(id);
		if(use==1) $('#Edituse_tercero1').iCheck('check');
		else $('#Edituse_tercero2').iCheck('check');


	});
</script>

<!-- /.content -->