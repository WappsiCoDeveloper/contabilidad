<style type="text/css">
    .site_logo{
        content: url("<?= base_url() ?>assets/uploads/logo.png");
        width: 70%;padding: 6% 0% 5.9%;
    }

    @media (max-width: 1000px){
        .site_logo{
        content: url("<?= base_url() ?>assets/uploads/mini_logo.png");
        }

        body.mini-navbar .nav-header > .site_logo{
            padding: 6% 0% 5.9% !important;
        }
    }

    body.mini-navbar .nav-header > .site_logo{
        content: url("<?= base_url() ?>assets/uploads/mini_logo.png");
        padding: 0;
    }
</style>

<div id="wrapper">
    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav metismenu" id="side-menu">
                <li class="nav-header" style="padding: 0; text-align: center;">
                    <img class="site_logo">
                </li>

                <li style="text-align: center;">
                    <div  class="dropdown drpdwn-profile">
                        <b class="dropdown-toggle" data-toggle="dropdown" id="menu_user_profile" style="cursor: pointer; padding: 10px 15px;">
                            <img src="<?= base_url(); ?>assets/uploads/users/<?= $current_user->image; ?>" class="user-img-profile" alt="User Image">
                            <span class="user-name-profile"><?= $current_user->first_name . ' ' . $current_user->last_name; ?> <b class="caret"></b></span>
                        </b>

                        <ul class="dropdown-menu" aria-labelledby="menu_user_profile" style="color: black;">
                            <li>
                                <?php if ($this->ion_auth->is_admin()) { ?>
                                    <a href="<?= base_url()."admin/edit_user/".$this->session->userdata('user_id'); ?>" class="dropdown-item"><span class="fa fa-pencil"></span>   <?=lang('main_header_user_dropdown_profile_btn_label');?></a>
                                <?php }else{ ?>
                                    <a href="#updateimage_modal" data-toggle="modal" class="dropdown-item"><span class="fa fa-pencil"></span>   <?=lang('main_header_user_dropdown_updateuserimage_btn_label');?></a>
                                <?php } ?>
                            </li>
                            <li>
                                <a href="<?= base_url('login/logout'); ?>" class="dropdown-item"><span class="fa fa-sign-out"></span>   <?=lang('main_header_user_dropdown_logout_btn_label');?></a>
                            </li>
                        </ul>
                    </div>
                </li>

                <?php foreach ($menu as $parent => $parent_params) { ?>
                    <?php if ((array_key_exists($parent_params['url'], $page_auth) && $page_auth[$parent_params['url']] == 1 ) || !array_key_exists($parent_params['url'], $page_auth)) { ?>
                        <?php if (empty($parent_params['children'])) { ?>
                            <?php if($parent == 'label' or $parent == 'label1') { ?>
                                <li class="active" title="<?= $parent_params['name']; ?>"><a><i class="fa fa-bars"></i><span><?php echo $parent_params['name']; ?></span></a></li>
                            <?php } else { ?>
                                <li>
                                    <a href="<?php echo $parent_params['url']; ?>"><i class="<?php echo $parent_params['icon']; ?>"></i> <span><?php echo $parent_params['name']; ?></span><span class="fa fa-arrow"></span></a>
                                </li>
                            <?php } ?>
                        <?php } else { ?>
                            <li>
                                <a><i class="<?php echo $parent_params['icon']; ?>"></i> <span class="nav-label"><?php echo $parent_params['name']; ?></span><span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level collapse">
                                    <?php if (strpos($parent_params['url'], '/') !== false) {
                                        $parent_params['url'] = substr_replace($parent_params['url'], '', strpos($parent_params['url'], '/'), strlen($parent_params['url']));
                                    } ?>

                                    <?php foreach ($parent_params['children'] as $name => $url) {
                                        $child_url = $parent_params['url'].'/'.$url;

                                        if ((array_key_exists($child_url, $page_auth) && $page_auth[$child_url] == 1 ) || !array_key_exists($child_url, $page_auth)) { ?>
                                            <li><a href="<?php echo $child_url ?>"><i class="fa fa-circle-o"></i> <?php echo $name; ?></a></li>
                                        <?php } ?>
                                    <?php } ?>
                                </ul>
                            </li>
                        <?php } ?>
                    <?php } ?>
                <?php } ?>
            </ul>
        </div>
    </nav>

    <div id="page-wrapper" class="gray-bg dashbard-1">
        <div class="row">
            <div class="" id="loader">
                <div class="" id="loaderFondo">
                    <div class="" id="loaderContenedor">
                        <i class="fa fa-cog fa-spin fa-3x fa-fw"></i>
                        <span class="sr-only">Loading...</span>
                    </div>
                </div>
            </div>
        </div>

        <div class="row border-bottom">
            <nav class="navbar navbar-static-top" role="navigation"  style="margin-bottom: 0">
                <div class="navbar-header">
                    <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="theme/#"><i class="fa fa-bars"></i></a>
                    <span class="title_software" href="theme/#">Contabilidad</span>
                </div>
                <ul class="nav navbar-top-links navbar-right">
                    <li class="company_name" style="text-align: right; float: right;">
                        <a href="<?= base_url('user/activate'); ?>" class="m-r-sm text-muted welcome-message wasdasd" style="color:white; <?= ($this->session->userdata('active_account')) ? "padding: 10px 10px;" : ""; ?>" >
                            <?php if ($this->session->userdata('active_account')): ?>
                                <img src="<?php if(isset($this->mAccountSettings)){ echo base_url().'assets/uploads/companies/'.$this->mAccountSettings->logo; }else{ echo '';} ?>"  alt="Logo" style="width: 40px;">
                                <?= $this->session->userdata('active_account')->name ?>
                                 - <?= date('Y', strtotime($this->session->userdata('active_account')->fy_end)) ?>
                            <?php else: ?>
                                <?= lang('main_header_active_account_NONE'); ?>
                            <?php endif ?>
                        </a>
                    </li>
                </ul>
            </nav>
        </div>

        <?php if (!$this->ion_auth->is_admin()) { ?>
            <div class="modal fade" tabindex="-1" role="dialog" id="updateimage_modal" aria-labelledby="updateimage">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title"><?= lang('edit_user_modal_title'); ?></h4>
                        </div>

                        <?= form_open_multipart('' , array('id' => 'changeimage_form')); ?>
                        <div class="modal-body">
                            <div class="msg"></div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <div style="margin-left: 20px;">
                                        <label><?=lang('edit_user_userimageupdate_label');?></label>
                                        <input type="file" name="image" id="image" />
                                    </div>

                                    <div style="margin-top: 20px; text-align: center;">
                                        <img id="image_preview" src="" style="max-width: 100%; height: auto;" />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal"><?=lang('edit_user_modal_cancel_btn_label');?></button>
                            <button type="submit" name="uploadimage" class="btn btn-primary"><?=lang('edit_user_modal_submit_btn_label');?></button>
                        </div>

                        <?= form_close(); ?>
                    </div>
                </div>
            </div>

            <script type="text/javascript">
                function readURL(input) {
                    if (input.files && input.files[0]) {
                        var reader = new FileReader();
                        reader.onload = function (e) {
                            $('#image_preview').attr('src', e.target.result);
                        }

                        reader.readAsDataURL(input.files[0]);
                    }
                }

                $("#image").change(function(){ readURL(this); });

                $('#changeimage_form').submit(function(event) {
                    event.preventDefault();
                    var userid = <?= $current_user->id; ?>;
                    var data = new FormData();

                    jQuery.each(jQuery('#image')[0].files, function(i, file) {
                        data.append('userimageupdate', file);
                    });

                    jQuery.ajax({
                        url:"<?= base_url(); ?>admin/updateuserimage/"+userid,
                        data: data,
                        cache: false,
                        contentType: false,
                        processData: false,
                        dataType: "json",
                        type: 'POST',
                        success:function(data) {
                            var msg = '';

                            if(data){
                                if (data.status == 'success') {
                                    msg = '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert">&times;</a><?= lang('strong_success_label'); ?>'+ data.msg +'</div><br>';
                                }else{
                                    msg = '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert">&times;</a><?= lang('strong_error_label'); ?>'+ data.msg +'</div><br>';
                                }
                            }

                            $('.msg').html(msg);
                            $('#updateimage_modal').animate({ scrollTop: 0 }, 'fast');
                        }
                    });
                });
            </script>
        <?php } ?>