<style type="text/css">
    .dl-horizontal{ overflow: auto; }
</style>

<div id="right-sidebar" class="animated">
    <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 100%;">
        <div class="sidebar-container" style="overflow: hidden; width: auto; height: 100%;">
            <div class="tab-content">
                <div id="tab-1" class="tab-pane active">
                    <div class="sidebar-title">
                        <h3><i class="fa fa-comments-o"></i> <?=lang('right_sidebar_menu_activity_log');?></h3>
                    </div>
                    <div>
                        <?php if (count($logs) <= 0) { ?>
                            <div class="sidebar-message">
                                <div class="media-body">
                                    <a>Nothing here.</a>
                                </div>
                            </div>
                        <?php } else { ?>
                            <div class="sidebar-message">
                                <div class="media-body">
                                    <a>
                                        <?php foreach ($logs as $row => $data) { ?>
                                            <?php if ($row >= 24) { ?>
                                                <dd>
                                                    <a href=".base_url('accounts/log')." class='btn btn-default btn-link pull-right'><?= lang('right_sidebar_menu_view_all_log'); ?></a>
                                                </dd>
                                            <?php
                                                break;
                                                }
                                            ?>

                                            <dt><?= $data['date'] ?></dt>
                                            <dd><?= $data['message']?></dd>
                                        <?php } ?>
                                    </a>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>

                <div class="box-header with-border">
                    <i class=""></i>
                </div>

            </div>
        </div>
    </div>
</div>

<div class="control-sidebar-bg"></div>
