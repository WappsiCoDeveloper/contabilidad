  <footer class="footer">
    <div class="pull-right hidden-xs">
      <b><?=lang('footer_version_accounting');?></b> <?= $settings->version; ?>
      <?php if (isset($this->mComercialAccountSettings) && $this->mComercialAccountSettings): ?>
      	, <b><?=lang('footer_version_comercial');?></b> <?= $this->mComercialAccountSettings->version; ?>

      	<?php if (isset($this->mComercialAccountSettings) && $settings->version != $this->mComercialAccountSettings->version): ?>
      		<b class="text-danger"> <span class="fa fa-exclamation"></span> </b>
      	<?php endif ?>

      <?php endif ?>
    </div>
    <strong><?=lang('footer_copyright_1');?><?= date('Y'); ?> <?= $settings->sitename; ?></strong><?=lang('footer_copyright_2');?>
  </footer>