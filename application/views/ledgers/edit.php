
<?php

function getlastparent($id){

	$CI =& get_instance();

	$resultados = $CI->DB1->where('parent_id', $id)->get('groups'.$CI->DB1->dbsuffix);

	if ($resultados->num_rows() == 0) {
		return "1";
	} else {
		return "0";
	}

}


 ?>

<script type="text/javascript">
$(document).ready(function() {
	$("#LedgerGroupId").select2({width:'100%'});
});
</script>

<style type="text/css">
.select2-container--default .select2-results__option {
	font-weight: bold;
	color: #333;
}
</style>

<!-- Main content -->
    <div class="row wrapper wrapper-content animated fadeInRight">
      <!-- Small boxes (Stat box) -->
      <div class="ibox float-e-margins">

        <!-- ./col -->
        <div class="ibox-content contentBackground">
          <div class="col-xs-12">
            <div class="box-header with-border">
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            	<?= validation_errors('<div class="alert alert-danger">', '</div>'); ?>
				<div class="ledgers add form">
					<?php
						echo form_open(); ?>

						<div class="row">
							<div class="col-md-5">
								<div class="form-group">
									<label><?= lang('ledgers_views_edit_label_parent_group'); ?></label><span class='input_required'> *</span>
									<select id="LedgerGroupId" name="group_id" class="form-control ledger-dropdown" onchange="getLedgerNumber()">
										<?php
										$prevParent = "";
										foreach ($parents as $id => $parent): ?>
											<?php if ((isset($niveles[4]) && in_array($id, $niveles[4])) || (isset($niveles[5]) && in_array($id, $niveles[5])) || getlastparent($id) == 1){
												$disabled = "";
											 } else {
											 	$disabled = "disabled";
											 }
											 ?>
												<option value="<?= $id ?>" <?= $disabled ?> <?= ($ledger['group_id'] == $id) ? "selected='selected'" : "" ?> > <?= $parent ?> </option>
										<?php endforeach ?>
									</select>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
									<label><?= lang('ledgers_views_edit_label_ledger_code'); ?></label><span class='input_required'> *</span>
									<input type="text" name="code" class="form-control" value="<?= set_value('code', $ledger['code']); ?>" >
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label><?= lang('ledgers_views_edit_label_ledger_name'); ?></label><span class='input_required'> *</span>
									<input type="text" name="name" class="form-control" value="<?= set_value('name', $ledger['name']); ?>" >
								</div>
							</div>
						</div>

						<div class="form-group">
							<label><?= lang('ledgers_views_edit_label_op_blnc'); ?></label><span class='input_required'> *</span>
							<div class="row">
								<div class="col-md-12">
									<div class="row">
										<div class="col-md-4">
											<input type="text" name="op_balance_dc" class="form-control" value="<?=  $ledger['op_balance_dc'] ?>" readonly>
										</div>
										<div class="col-md-8">
											<div class="form-group">
		                    <div class="input-group">
														<input type="number" name="op_balance" class="form-control" value="<?= set_value('op_balance_dc', $ledger['op_balance']); ?>" readonly>
		                        <span class="input-group-addon">
                                	<div class="fa fa-info-circle" data-toggle="tooltip" title="<?= lang('ledgers_views_edit_op_blnc_tooltip'); ?>">
	                                </div>
		                        </span>
		                    </div>
			                </div>
										</div>
									</div>
								</div>
							</div>
						</div>

					<div class="row">
						<div class="col-md-4">
							<div class="form-group">
									<input type="checkbox" name="type" class="form-control" <?= ($ledger['type'] or set_value('type')) ? 'checked' : '' ?>><label> <?= "&nbsp" ?> <?= lang('ledgers_views_edit_label_bank_cash_account'); ?></label>
			                        <span class="">
		                                <div class="fa fa-info-circle" data-toggle="tooltip" title="<?= lang('ledgers_views_edit_bank_cash_account_tooltip'); ?>">
		                                </div>
			                        </span>
			                </div>
			                <!-- /.form group -->
			                <div class="form-group">
								<input type="checkbox" name="reconciliation" class="form-control" <?= ($ledger['reconciliation'] or set_value('reconciliation')) ? 'checked' : '' ?>><label> <?= "&nbsp" ?> <?= lang('entries_views_edit_label_reconciliation'); ?></label>
		                        <span class="">
	                                <div class="fa fa-info-circle" data-toggle="tooltip" title="<?= lang('ledgers_views_edit_reconciliation_tooltip'); ?>">
	                                </div>
		                        </span>
			                </div>
			                <!-- /.form group -->
			                <div class="form-group">
			                    	<input type="checkbox" name="cost_center" class="form-control" <?= ($ledger['cost_center'] or set_value('cost_center')) ? 'checked' : '' ?>>
			                    	<label><?= lang('entries_views_add_label_cost_center'); ?></label>
			                        <span class="">
		                                <div class="fa fa-info-circle" data-toggle="tooltip" title="<?= lang('ledgers_views_edit_cost_center_tooltip'); ?>">
		                                </div>
			                        </span>
			                    <!-- /.input group -->
			                </div>
			                <!-- /.form group -->
						</div>
						<div class="col-md-8">
							<div class="form-group">
								<label><?= lang('ledgers_views_edit_label_notes'); ?></label>
							<textarea name="notes" rows="3" class="form-control"><?= set_value('notes', $ledger['notes']); ?></textarea>
							</div>
						</div>
					</div>
				</div>
            </div>
            <div class="box-footer">
            	<div class="form-group">
					<input type="submit" name="Submit" value="<?= lang('ledgers_views_add_label_submit_btn'); ?>" class="btn btn-success">
					<a href="<?=base_url(); ?>accounts" class="btn btn-default" style="margin-right: 5px;"><?= lang('ledgers_views_edit_label_cancel_btn'); ?></a>
				</div>
		    </div>
		    <?= form_close(); ?>
			</div>
        </div>
    </div>
<!-- /.content -->