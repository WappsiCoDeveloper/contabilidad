
<?php

function getlastparent($id){

	$CI =& get_instance();

	$resultados = $CI->DB1->where('parent_id', $id)->get('groups'.$CI->DB1->dbsuffix);

	if ($resultados->num_rows() == 0) {
		return "1";
	} else {
		return "0";
	}

}


 ?>


<script type="text/javascript">
$(document).ready(function() {
	$('#LedgerGroupId').trigger('change');
	$("#LedgerGroupId").select2({width:'100%'});


	$(document).on('keypress', 'form', function(e){
        if(e == 13){
          return false;
        }
      });

    $(document).on('keypress', 'input', function(e){
        if(e.which == 13){
          return false;
        }
      });
});

	function getLedgerNumber() {
		var id = $("#LedgerGroupId option:selected").val()
		$.ajax({
	    	type:"POST",
	        url: "<?=base_url(); ?>" + "ledgers/getNextCode",
	    	data: { id }
	    }).done(function(msg){
	    	$('#l_code').val(msg);
	    });
	}
</script>

<style type="text/css">
.select2-container--default .select2-results__option {
	font-weight: bold;
	color: #333;
}
</style>

<!-- Main content -->
    <div class="row wrapper wrapper-content animated fadeInRight">
      <!-- Small boxes (Stat box) -->
      <div class="ibox float-e-margins">

        <!-- ./col -->
        <div class="ibox-content contentBackground">
          <div class="col-xs-12">
              <h3 class="box-title"><?= lang('ledgers_views_add_title'); ?></h3>
            <!-- /.box-header -->
            <div class="box-body">
            	<!-- <?= validation_errors('<div class="alert alert-danger">', '</div>'); ?> -->

				<div class="ledgers add form">
					<?php
						echo form_open(); ?>
						<div class="row">
							<div class="col-md-5">
								<div class="form-group">
									<label><?= lang('ledgers_views_add_label_parent_group'); ?></label><span class='input_required'> *</span>
									<!-- <?= form_dropdown('group_id', $parents, set_value('group_id'), array('id' => 'LedgerGroupId', 'class'=>'form-control', 'onchange'=>"getLedgerNumber()")); ?> -->
									<select id="LedgerGroupId" name="group_id" class="form-control ledger-dropdown" onchange="getLedgerNumber()">
										<?php
										$prevParent = "";
										foreach ($parents as $id => $parent): ?>
											<?php if ((isset($niveles[4]) && in_array($id, $niveles[4])) || (isset($niveles[5]) && in_array($id, $niveles[5])) || getlastparent($id) == 1){

												$disabled = "";

											 } else {
											 	$disabled = "disabled";
											 }


											 ?>
												<option value="<?= $id ?>" <?= $disabled ?> <?= (isset($_POST['group_id']) && $_POST['group_id'] == $id) ? "selected='selected'" : "" ?> > <?= $parent ?> </option>
										<?php endforeach ?>
									</select>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
									<label><?= lang('ledgers_views_add_label_ledger_code'); ?></label><span class='input_required'> *</span>
									<input type="text" name="code" id="l_code" value="<?= set_value('code');?>" class="form-control">
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label><?= lang('ledgers_views_add_label_ledger_name'); ?></label><span class='input_required'> *</span>
									<input type="text" name="name" value="<?= set_value('name');?>" class="form-control">
								</div>
							</div>
						</div>

						<div class="form-group">
							<label><?= lang('ledgers_views_add_label_op_blnc'); ?></label><span class='input_required'> *</span>
							<div class="row">
								<div class="col-md-12">
									<div class="row">
										<div class="col-md-4">
											<?= form_dropdown('op_balance_dc', array('D' => lang('entries_views_addrow_label_dc_drcr_D'), 'C' => lang('entries_views_addrow_label_dc_drcr_C')), set_value('op_balance_dc'), array('class'=>'form-control')); ?>
										</div>
										<div class="col-md-8">
											<div class="form-group">
							                    <div class="input-group">
													<input type="number" value="0" name="op_balance" class="form-control" min='0' readonly>
							                        <div class="input-group-addon">
							                            <i>
						                                	<div class="fa fa-info-circle" data-toggle="tooltip" title="<?= lang('ledgers_views_add_op_blnc_tooltip'); ?>">
							                                </div>
							                            </i>
							                        </div>
							                    </div>
							                    <!-- /.input group -->
							                </div>
							                <!-- /.form group -->
										</div>
									</div>
								</div>
							</div>
						</div>

					<div class="row">
						<div class="col-md-4">
							<div class="form-group">
									<input  type="checkbox" name="type" class="form-control" <?= set_value('type') ? 'checked' : '' ?>>
									<label><?= lang('ledgers_views_add_label_bank_cash_account'); ?></label>
									<span class="">
		                                <div class="fa fa-info-circle" data-toggle="tooltip" title="<?= lang('ledgers_views_edit_bank_cash_account_tooltip'); ?>">
		                                </div>
			                        </span>
								<!-- /.input group -->
			                </div>
			                <!-- /.form group -->
			                <div class="form-group">
			                    	<input type="checkbox" name="reconciliation" class="form-control" <?= set_value('reconciliation') ? 'checked' : '' ?>>
			                    	<label><?= lang('entries_views_add_label_reconciliation'); ?></label>
			                        <span class="">
		                                <div class="fa fa-info-circle" data-toggle="tooltip" title="<?= lang('ledgers_views_edit_reconciliation_tooltip'); ?>">
		                                </div>
			                        </span>
			                    <!-- /.input group -->
			                </div>
			                <!-- /.form group -->
			                <div class="form-group">
			                    	<input type="checkbox" name="cost_center" class="form-control" <?= set_value('cost_center') ? 'checked' : '' ?>>
			                    	<label><?= lang('entries_views_add_label_cost_center'); ?></label>
			                        <span class="">
		                                <div class="fa fa-info-circle" data-toggle="tooltip" title="<?= lang('ledgers_views_edit_cost_center_tooltip'); ?>">
		                                </div>
			                        </span>
			                    <!-- /.input group -->
			                </div>
			                <!-- /.form group -->
						</div>
						<div class="col-md-8">
							<div class="form-group">
								<label><?= lang('ledgers_views_add_label_notes'); ?></label>
								<textarea name="notes" rows="3" class="form-control"><?= set_value('notes') ?></textarea>
							</div>
						</div>
					</div>
				</div>
            </div>
            <div class="box-footer">
            	<div class="form-group">
					<input type="submit" name="Submit" value="<?= lang('ledgers_views_add_label_submit_btn'); ?>" class="btn btn-success">
					<a href="<?=base_url(); ?>accounts" class="btn btn-default" style="margin-right: 5px;"><?= lang('ledgers_views_add_label_cancel_btn'); ?></a>
				</div>
		    </div>
		    <?= form_close(); ?>
        </div>
    </div>
    <!-- /.row -->
</div>
<!-- /.content -->