<div class="row wrapper border-bottom white-bg page-heading">
  <div class="col-lg-8">
    <h2><?php echo $page_title; ?></h2>
    <ol class="breadcrumb">
      <li>
        <a href="login/index">Inicio</a> <?php //En el controlador en la función llamada, se revisa si el usuario está logueado y lo envía a la vista principal según permisos ?>
      </li>
      <li class="active">
        <strong><?php echo $page_title; ?></strong>
      </li>
    </ol>
  </div><!-- /.col -->
  <div class="col-lg-4">
    <div class="title-action">
      <button class="btn btn-success" onclick="window.location.href = '<?= base_url('admin/create_account');?>';"><span class="fa fa-plus"></span>  <?= lang('dashboard_create_account_label'); ?></button>
    </div>
  </div>
</div><!-- /.row -->

<div class="row wrapper wrapper-content animated fadeInRight">
  <div class="ibox float-e-margins">
    <div class="ibox-content contentBackground">
    <div class="col-sm-12">
        <div class="table-responsive">
          <table id="companylist" class="table table-striped custom-table" width="100%" cellspacing="0">
            <thead>
            <tr>
              <th><?= lang('companylist_table_label'); ?></th>
              <th><?= lang('companylist_table_db_name'); ?></th>
              <th><?= lang('companylist_table_db_host'); ?></th>
              <th><?= lang('companylist_table_db_prefix'); ?></th>
              <th><?= lang('companylist_table_db_suffix'); ?></th>
            </tr>
            </thead>
            <tbody>
            <?php if ($accounts) : ?>
            <?php foreach ($accounts as $account) : ?>
              <tr>
                <td><?= $account->label; ?></td>
                <td><?= $account->db_database; ?></td>
                <td><?= $account->db_host; ?></td>
                <td><?= $account->db_prefix; ?></td>
                <td><?= $account->db_suffix; ?></td>
              </tr>
          <?php endforeach; ?>
          <?php endif; ?>
            </tbody>
            <tfoot>
            <tr>
              <th><?= lang('companylist_table_label'); ?></th>
              <th><?= lang('companylist_table_db_name'); ?></th>
              <th><?= lang('companylist_table_db_host'); ?></th>
              <th><?= lang('companylist_table_db_prefix'); ?></th>
              <th><?= lang('companylist_table_db_suffix'); ?></th>
            </tr>
            </tfoot>
          </table>
        </div>
      </div>
  </div>
</div>

<script type="text/javascript">
  $(document).ready(function() {

    $('#companylist').DataTable({
    "paging": true,
    "lengthChange": true,
    "searching": true,
    "ordering": true,
    "info": true,
    "autoWidth": true,
    dom: 'Bfrtip',
      buttons: [
          'create'
      ]
    });

  });
</script>