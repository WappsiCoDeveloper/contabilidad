<div class="row wrapper border-bottom white-bg page-heading">
  <div class="col-lg-8">
    <h2><?php echo $page_title; ?></h2>
    <ol class="breadcrumb">
      <li>
        <a href="login/index">Inicio</a> <?php //En el controlador en la función llamada, se revisa si el usuario está logueado y lo envía a la vista principal según permisos ?>
      </li>
      <li class="active">
        <strong><?php echo $page_title; ?></strong>
      </li>
    </ol>
  </div><!-- /.col -->
  <div class="col-lg-4">
    <div class="title-action">
      <a href="admin/create_group" class="btn btn-success"><span class="fa fa-plus"></span>  <?=lang('user_permission_add_group_btn');?></a>
    </div>
  </div>
</div><!-- /.row -->
<!-- Main content -->
<div class="row wrapper wrapper-content animated fadeInRight">
  <!-- Small boxes (Stat box) -->
  <div class="ibox float-e-margins">
    <div class="ibox-content contentBackground">
      <div class="col-sm-12">
        <!-- /.box-header -->
        <div class="table-responsive">
            <table class="table table-striped" id="userGroup"  width="100%" cellspacing="0" style="padding-bottom: 5%;">
              <thead>
                <th><?= lang('user_permission_table_id'); ?></th>
                <th><?= lang('user_permission_table_group'); ?></th>
                <th><?= lang('user_permission_table_action'); ?></th>
              </thead>
              <tbody>
                <?php foreach ($permissions as $key => $permission): ?>
                  <tr>
                    <td><strong><?= $permission->id; ?></strong></td>
                    <td><?= $permission->description; ?></td>
                    <td>
                      <div class="btn-group">
                        <div class="dropdown">
                          <button class="btn btn-success btn-sm" type="button" id="accionesGrupo" data-toggle="dropdown" aria-haspopup="true">
                            Acciones
                            <span class="caret"></span>
                          </button>
                          <ul class="dropdown-menu" aria-labelledby="accionesGrupo">
                           <li>
                              <a href="<?= base_url(); ?>admin/delete_group/<?= $permission->gp_id; ?>"><span class="fa fa-trash"></span>  <?=lang('user_permission_delete_group_tooltip');?></a>
                           </li>
                           <li>
                              <a href="<?= base_url(); ?>admin/edit_group/<?= $permission->gp_id; ?>"><span class="fa fa-pencil"></span>  <?=lang('user_permission_edit_group_tooltip');?></a>
                           </li>
                           <li>
                              <a href="<?= base_url(); ?>admin/edit_permission/<?= $permission->gp_id; ?>"><span class="fa fa-cogs"></span>  <?=lang('user_permission_edit_group_permission_tooltip');?></a>
                           </li>
                          </ul>
                        </div>
                      </div>
                    </td>
                  </tr>
                <?php endforeach; ?>
              </tbody>
             <tfoot>
                <th><?= lang('user_permission_table_id'); ?></th>
                <th><?= lang('user_permission_table_group'); ?></th>
                <th><?= lang('user_permission_table_action'); ?></th>
              </tfoot>
            </table>
        </div>
      </div>
  </div>
  <!-- /.row -->
</div>
<!-- /.content -->
<script type="text/javascript">
    $(document).ready(function() {

      $('#userGroup').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      dom: 'Bfrtip',
        buttons: [
            'create',
        ],
      "columnDefs": [
        { "width": "10px", "targets": 0 },
        { "width": "20px", "targets": 2 }
      ]
      });
    });
</script>