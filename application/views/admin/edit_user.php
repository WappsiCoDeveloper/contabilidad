<script type="text/javascript">
  $(document).on('keypress', 'form', function(e){   
        if(e == 13){
          return false;
        }
      });

    $(document).on('keypress', 'input', function(e){
        if(e.which == 13){
          return false;
        }
      });
</script>
<div class="row wrapper border-bottom white-bg page-heading">
  <div class="col-lg-8">
    <h2><?php echo $page_title; ?></h2>
    <ol class="breadcrumb">
      <li>
        <a href="login/index">Inicio</a> <?php //En el controlador en la función llamada, se revisa si el usuario está logueado y lo envía a la vista principal según permisos ?>
      </li>
      <li class="active">
        <strong><?php echo $page_title; ?></strong>
      </li>
    </ol>
  </div><!-- /.col -->
  <div class="col-lg-4">
    <div class="title-action">
      <button class="btn btn-success" onclick="$('#form_edit_user').submit();"><span class="fa fa-check"></span>  <?= lang('edit_user_modal_submit_btn_label'); ?></button>
    </div>
  </div>
</div><!-- /.row -->
<div class="row wrapper wrapper-content animated fadeInRight">
  <div class="ibox float-e-margins">
    <div class="ibox-content contentBackground">
      <div class="row">
        <div class="col-sm-12 section-img-profile">
          <?php $attributes = array('id' => 'updateuserimage');
              echo form_open_multipart('' ,$attributes); ?>
          <div class="fileinput fileinput-new" data-provides="fileinput" style="width: 200px;">
            <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 200px;">
              <img src="<?= base_url(); ?>assets/uploads/users/<?= $current_user->image; ?>" alt="User Image" style="width: 100% !important;">
            </div>
            <div>
              <span class="btn btn-success btn-file btn-profile-img btn-profile-img-edit">
                <span class="fileinput-new">
                  <i class="fa fa-pencil"></i>
                </span>
              <span class="fileinput-exists">
                <i class="fa fa-pencil"></i>
              </span>
              <input type="file" name="userimageupdate" id="userimageupdate" /></span>
              <a href="#" class="btn btn-danger fileinput-exists btn-profile-img btn-profile-img-remove" data-dismiss="fileinput"><i class="fa fa-trash"></i></a>
              <button type="submit" name="uploadimage" class="btn btn-primary fileinput-exists btn-profile-img btn-profile-img-submit"><i class="fa fa-check"></i></button>
            </div>
          </div>
          <?php echo form_close(); ?>
        </div>
        <hr class="col-sm-11">
      </div>
    <?php echo form_open(uri_string(), ' id="form_edit_user" ');?>
        <div class="row">
          <div class="col-md-4">
            <p>
              <?php echo lang('edit_user_fname_label', 'first_name');?><span class='input_required'> *</span> <br />
              <?php echo form_input($first_name);?>
            </p>

            <p>
              <?php echo lang('edit_user_lname_label', 'last_name');?><span class='input_required'> *</span> <br />
              <?php echo form_input($last_name);?>
            </p>

            <p>
              <?php echo lang('edit_user_company_label', 'company');?><span class='input_required'> *</span> <br />
              <?php echo form_input($company);?>
            </p>
          </div>
          <div class="col-md-4">
            <p>
              <?php echo lang('edit_user_phone_label', 'phone');?><span class='input_required'> *</span> <br />
              <?php echo form_input($phone);?>
            </p>
            <p>
              <?php echo lang('edit_user_password_label', 'password');?><span class='input_required'> *</span> <br />
              <?php echo form_input($password);?>
            </p>
            <p>
              <?php echo lang('edit_user_password_confirm_label', 'password_confirm');?><span class='input_required'> *</span><br />
              <?php echo form_input($password_confirm);?>
            </p>
          </div>
          <div class="col-md-4">
            <h3><?php echo lang('edit_user_groups_heading');?><span class='input_required'> *</span></h3>
              <?php foreach ($groups as $group):?>
                <label class="radio">
                  <?php
                    $gID=$group['id'];
                    $checked = null;
                    $item = null;
                    foreach($currentGroups as $grp) {
                        if ($gID == $grp->id) {
                            $checked= ' checked="checked"';
                        break;
                        }
                    }
                  ?>
                <input type="radio" name="groups[]" value="<?php echo $group['id'];?>"<?php echo $checked;?>>
                <?php echo htmlspecialchars($group['description'],ENT_QUOTES,'UTF-8');?>
                </label>
              <?php endforeach?>
          </div>
        </div>
        <div class="row" style="margin-bottom: 30px;">
          <div class="col-md-12">
            <h3><?=lang('create_user_accessible_accounts_label');?><span class='input_required'> *</span></h3>
            <div class="row">
              <?php foreach ($accounts as $account):?>
                <div class="col-md-4"> 
                  <?php if (isset($account->cost_centers)): ?>
                    <div class="row">
                      <div class="col-md-4">
                  <?php endif ?>
                        <label>
                          <input type="checkbox" name="accounts[]" class="account" value="<?= $account->id; ?>" <?= isset($accessibleAccounts[$account->id]) ? 'checked="checked"' : '' ?>>
                          <?= (ucfirst(mb_strtolower($account->label))); ?>
                        </label>
                  <?php if (isset($account->cost_centers)): ?>
                      </div>
                      <div class="col-md-8">
                        <select name="cost_centers[<?= $account->id; ?>][]" class="form-control select" data-placeholder="<?= sprintf(lang('create_user_accessible_cost_centers_placeholder'), ucfirst(mb_strtolower($account->label))) ?>" data-disable="<?= isset($accessibleAccounts[$account->id]) ? 0 : 1 ?>" multiple>
                        <?php foreach ($account->cost_centers as $cc): ?>
                          <option value="<?= $cc->id ?>" <?= isset($accesible_cost_centers[$account->id][$cc->id]) ? 'selected="selected"' : '' ?> ><?= ucfirst(mb_strtolower($cc->name)) ?></option>
                        <?php endforeach ?>
                        </select>
                      </div>
                    </div>
                  <?php endif ?>
                </div>
              <?php endforeach?>
            </div>  
          </div>
        </div>
    <div class="col-sm-12" style="padding-top: 2%; padding-bottom: 2%;">
      <button type="submit" class="btn btn-success"><?=lang('edit_user_submit_btn');?></button>
      <a href="<?= base_url(); ?>admin/users/" id="cancel" name="cancel" class="btn btn-default" style="margin-right: 5px;"><?=lang('edit_user_cancel_btn');?></a>
    </div>
    <?php echo form_close();?>
      
  </div>
</div>
    

<script type="text/javascript">
  function readURL(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = function (e) {
        $('#previewImage').attr('src', e.target.result);
      }
      reader.readAsDataURL(input.files[0]);
    }
  }

$("#userimageupdate").change(function(){
  readURL(this);
});

$('#updateuserimage').submit(function(event){  
  event.preventDefault();
  var userid = <?= $user->id; ?>;
  var data = new FormData();
  jQuery.each(jQuery('#userimageupdate')[0].files, function(i, file) {
    data.append('userimageupdate', file);
  });
  console.log(data);
  jQuery.ajax({  
    url:"<?= base_url(); ?>admin/updateuserimage/"+userid,  
    data: data,
    cache: false,
    contentType: false,
    processData: false,
    dataType: "json",
    type: 'POST',
    success:function(data){
      var alert = '';
      if(data){
        if (data.status == 'success') {
          $('.user-img-profile').attr('src', '<?= base_url(); ?>assets/uploads/users/<?= $current_user->image; ?>');
         Command: toastr.success('Se actualizó correctamente la imagen de perfil.', 'Actualizado con éxito', {onHidden : function(){
          }})
        }else{
          Command: toastr.error('Ocurrió un error al actualizar.', 'Error al actualizar', {onHidden : function(){
            console.log(data.msg);
          }})
        }      
      }
    }  
  });  
});



  $('#accessibleAccounts').select2({
    placeholder: "<?=lang('create_user_accessibleAccounts_placeholder');?>"
  });

  $(document).ready(function(){
    $('.select').each(function(index, select){
      placeholder_text = $(this).data('placeholder');
      if ($(this).data('disable')) {
        disable_select = true;
      } else {
        disable_select = false;
      }
      $(this).select2({
        placeholder : placeholder_text,
        disabled : disable_select
      });
    });
  });


  $(document).on('ifChecked', '.account', function(){
    index = $(this).index('.account');
    $('.select').eq(index).select2({disabled : false});
  });

  $(document).on('ifUnchecked', '.account', function(){
    index = $(this).index('.account');
    $('.select').eq(index).select2({disabled : true});
  });

</script>