<script type="text/javascript">
  $(document).on('keypress', 'form', function(e){   
        if(e == 13){
          return false;
        }
      });

    $(document).on('keypress', 'input', function(e){
        if(e.which == 13){
          return false;
        }
      });
</script>

<section class="row wrapper wrapper-content animated fadeInRight">
  <div class="ibox float-e-margins">
    <div class="ibox-content contentBackground">


<?php echo form_open(uri_string());?>

      <p>
            <?php echo lang('edit_group_name_label', 'group_name');?> <br />
            <?php echo form_input($group_name);?>
      </p>

      <p>
            <?php echo lang('edit_group_desc_label', 'group_description');?> <br />
            <?php echo form_textarea($group_description);?>
      </p>

    <div class="box-footer">
      <button type="submit" class="btn btn-success"><?=lang('edit_group_submit_button');?></button>
      <a href="<?= base_url(); ?>admin/groups" id="cancel" name="cancel" class="btn btn-default" style="margin-right: 5px;"><?=lang('edit_group_cancel_button');?></a>
    </div>
<?php echo form_close();?>
</div>
</div>
</section>