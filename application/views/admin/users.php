<div class="row wrapper border-bottom white-bg page-heading">
  <div class="col-lg-8">
    <h2><?php echo $page_title; ?></h2>
    <ol class="breadcrumb">
      <li>
        <a href="">Inicio</a>
      </li>
      <li class="active">
        <strong><?php echo $page_title; ?></strong>
      </li>
    </ol>
  </div><!-- /.col -->
  <div class="col-lg-4">
    <div class="title-action">
      <div class="btn-group">
        <div class="dropdown">
          <button class="btn btn-success  type="button" id="accionesGrupo" data-toggle="dropdown" aria-haspopup="true">
            <span class="fa fa-plus"></span> Nuevo
            <span class="caret"></span>
          </button>
          <ul class="dropdown-menu pull-right" aria-labelledby="accionesGrupo">
           <li>
              <a href="<?= base_url(); ?>admin/create_user"><span class="fa fa-plus"></span>  <?= lang('index_create_user_btn_label'); ?></a>
           </li>
           <li>
              <a href="<?= base_url(); ?>admin/create_group"><span class="fa fa-plus"></span>  <?= lang('index_create_group_btn_label'); ?></a>
           </li>
          </ul>
        </div>
      </div> 
    </div>
  </div>
</div><!-- /.row -->
    <div class="row wrapper wrapper-content animated fadeInRight">
      <div class="ibox float-e-margins">
        <div class="ibox-content contentBackground">
        <div class="col-sm-12">
            <!-- /.box-header -->
            <div class="box-body">
              <table id="userlist" class="table table-striped custom-table" width="100%" cellspacing="0" style="padding-bottom: 5%;">
                <thead>
                <tr>
                  <th><?php echo lang('index_fname_th');?></th>
        					<th><?php echo lang('index_lname_th');?></th>
                  <th><?php echo lang('index_email_th');?></th>
        					<th><?php echo lang('companies');?></th>
        					<th><?php echo lang('index_groups_th');?></th>
        					<th><?php echo lang('index_status_th');?></th>
        					<th><?php echo lang('index_action_th');?></th>
                </tr>
                </thead>
                <tbody>
                <?php if ($users) : ?>
                <?php foreach ($users as $user):?>
        				<tr>
  		            <td><?php echo htmlspecialchars($user->first_name,ENT_QUOTES,'UTF-8');?></td>
  		            <td><?php echo htmlspecialchars($user->last_name,ENT_QUOTES,'UTF-8');?></td>
  		            <td><?php echo htmlspecialchars($user->email,ENT_QUOTES,'UTF-8');?></td>
                  <td><?php 
                        $databaseName = '';
                        $arrayNames = $databases[$user->id]; 
                        if (is_array($arrayNames)) {
                          foreach ($arrayNames as $key => $value) {
                            $databaseName .= $value['name'] .',  ';
                          }
                        }
                        echo htmlspecialchars(trim($databaseName,', '),ENT_QUOTES,'UTF-8');
                        ?>  
                  </td>
        					<td>
        						<?php foreach ($user->groups as $group):?>
        							<?php echo anchor("admin/edit_permission/".$group->id, '<span class="label label-default">'.htmlspecialchars($group->description,ENT_QUOTES,'UTF-8').'</span>', "data-toggle='tooltip' title='".lang('index_groups_anchor_tooltip')."'") ;?><br />
        		                <?php endforeach?>
        					</td>
                  <?php

                  if ($user->active) {
                    $href = "admin/deactivate/".$user->id;
                    $class = "label label-success";
                    $label = lang('index_active_link');
                  }else{
                    $href = "admin/activate/". $user->id;
                    $class = "label label-warning";
                    $label = lang('index_inactive_link');
                  }
                  ?>
        					<td><a href="<?= $href; ?>"><span class="<?= $class; ?>"><?= $label; ?></span></a></td>
        					<td>
                    <div class="btn-group">
                      <div class="dropdown">
                        <button class="btn btn-success btn-sm" type="button" id="accionesGrupo" data-toggle="dropdown" aria-haspopup="true">
                          Acciones
                          <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="accionesGrupo">
                         <li>
                          <a href="<?= base_url(); ?>admin/edit_user/<?= $user->id; ?>"><span class="fa fa-pencil"></span> Editar</a>
                         </li>
                         <li>
                          <a href="#delete_user_modal" data-toggle="modal" data-user="<?= $user->id; ?>"><span class="fa fa-trash"></span> Eliminar</a>
                          <!-- <a ><span class="fa fa-trash"></span> Eliminar</a> -->
                         </li>
                        </ul>
                      </div>
                    </div>
                  </td>
        				</tr>
        				<?php endforeach;?>
              	<?php endif; ?>
                </tbody>
                <tfoot>
                <tr>
                  <th><?php echo lang('index_fname_th');?></th>
        					<th><?php echo lang('index_lname_th');?></th>
        					<th><?php echo lang('index_email_th');?></th>
                  <th><?php echo lang('companies');?></th>
        					<th><?php echo lang('index_groups_th');?></th>
        					<th><?php echo lang('index_status_th');?></th>
        					<th><?php echo lang('index_action_th');?></th>
                </tr>
                </tfoot>
              </table>
            </div>
          </div>
      </div>
    </div>


<div class="modal fade" tabindex="-1" role="dialog" id="delete_user_modal" aria-labelledby="deleteuser">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><?= lang('users_index_delete_user_alert_title'); ?></h4>
      </div>
        <div class="modal-body">
          <input type="hidden" name="user_id_delete" id="user_id_delete">
          <?= lang('users_index_delete_user_alert_text'); ?>
        </div>
        <div class="modal-footer">
          <button class="btn btn-default" data-dismiss="modal"><?=lang('no');?></button>
          <button class="btn btn-danger confirm_delete_user" ><?=lang('yes');?></button>
        </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">

    $(document).ready(function() {

      $('#userlist').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      dom: 'Bfrtip',
        buttons: [
            'create',
            'newGroup'
        ]
      });
    });

$('#delete_user_modal').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget);
      user = button.data('user');
      $('#user_id_delete').val(user);
      console.log("id usuario : "+user);
});

$('.confirm_delete_user').on('click', function(){
  user = $('#user_id_delete').val();
  window.location.href = "<?= base_url(); ?>admin/delete_user/"+user;
});

</script>