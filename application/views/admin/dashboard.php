<style type="text/css">
  .panel-info-box:hover{
        color: #0000006e;
  }
</style>
<div class="row wrapper border-bottom white-bg page-heading">
  <div class="col-lg-8">
    <h2><?php echo $page_title; ?></h2>
    <ol class="breadcrumb">
      <li>
        <a href="">Inicio</a>
      </li>
      <li class="active">
        <strong><?php echo $page_title; ?></strong>
      </li>
    </ol>
  </div><!-- /.col -->
  <div class="col-lg-4">
    <div class="title-action">
      <button class="btn btn-success" onclick="window.location.href = '<?= base_url('admin/create_account');?>';"><span class="fa fa-plus"></span>  <?= lang('dashboard_create_account_label'); ?></button>
    </div>
  </div>
  
</div><!-- /.row -->
  <div class="row wrapper wrapper-content animated fadeInRight">
    <div class="ibox float-e-margins">
      <div class="ibox-content contentBackground">
    <div class="col-md-12 row">   
    </div>
    <!-- ./col-md-4 -->
    <div class="col-md-12 row">
      <!-- TABLE: LATEST ORDERS -->
        <div class="col-sm-12">
            <table id="accounts" class="table">
              <thead>
                <tr>
                  <th><?= lang('dashboard_accountlist_table_label'); ?></th>
                  <th><?= lang('dashboard_accountlist_table_name'); ?></th>
                  <th><?= lang('settings_views_cf_label_db_name'); ?></th>
                  <th><?= lang('settings_views_cf_label_db_prefix'); ?></th>
                  <th><?= lang('settings_views_cf_label_db_suffix'); ?></th>
                  <th><?= lang('dashboard_accountlist_table_fiscal_year'); ?></th>
                  <th><?= lang('dashboard_accountlist_table_status'); ?></th>
                  <th><?= lang('dashboard_accountlist_table_actions'); ?></th>
                </tr>
              </thead>
              <tbody>
                <?php
                if ($accounts) {
                  foreach ($accounts as $account) {
                    $href = base_url()."user/activate/".$account->id;
                    if ($active_account_id == $account->id) {
                      $href = base_url()."user/deactivate/".$account->id;
                      if ($account->account_locked == 1) {
                        $label = lang('dashboard_accountlist_table_status_label_active_locked');
                        $title = lang('dashboard_accountlist_table_status_tooltip_title_active_locked');
                        $class = 'label label-warning';
                      }else{
                        $label = lang('dashboard_accountlist_table_status_label_active');
                        $title = lang('dashboard_accountlist_table_status_tooltip_title_active');
                        $class = 'label label-success';
                      }
                    }elseif ($account->account_locked == 1) {
                      $label = lang('dashboard_accountlist_table_status_label_locked_inactive');
                      $title = lang('dashboard_accountlist_table_status_tooltip_title_locked_inactive');
                      $class = 'label label-danger';
                    }else{
                      $label = lang('dashboard_accountlist_table_status_label_inactive');
                      $title = lang('dashboard_accountlist_table_status_tooltip_title_inactive');
                      $class = 'label label-default';
                    }
                ?>
                  <tr>
                    <td><?= "<em>".$account->label."</em>"; ?></td>
                    <td><?= $account->name; ?></td>
                    <td><?= $account->db_database; ?></td>
                    <td><?= $account->db_prefix; ?></td>
                    <td><?= $account->db_suffix; ?></td>
                    <td>
                      <?= "<strong>".$this->functionscore->dateFromSql($account->fy_start).lang('dashboard_accountlist_table_fiscal_year_to').$this->functionscore->dateFromSql($account->fy_end)."</strong>"; ?>
                    </td>
                    <td data-toggle="tooltip" data-container="body" title="<?= $title; ?>">
                      <a href="<?= $href; ?>"><span class="<?= $class; ?>"><?= $label; ?></span></a>
                    </td>  
                    <td>
                      <div class="btn-group">
                        <button type="button" class="btn btn-success dropdown-toggle btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          <?= lang('dashboard_accountlist_table_actions'); ?> <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu">
                          <li><a href="<?= base_url() ?>admin/edit_account/<?= $account->id ?>"><span class="fa fa-pencil"></span> Editar</a></li>
                        </ul>
                      </div>
                    </td>                  
                  </tr>
                <?php } } ?>
              </tbody>
              <tfoot>
                <tr>
                  <th><?= lang('dashboard_accountlist_table_label'); ?></th>
                  <th><?= lang('dashboard_accountlist_table_name'); ?></th>
                  <th><?= lang('settings_views_cf_label_db_name'); ?></th>
                  <th><?= lang('settings_views_cf_label_db_prefix'); ?></th>
                  <th><?= lang('settings_views_cf_label_db_suffix'); ?></th>
                  <th><?= lang('dashboard_accountlist_table_fiscal_year'); ?></th>
                  <th><?= lang('dashboard_accountlist_table_status'); ?></th>
                  <th><?= lang('dashboard_accountlist_table_actions'); ?></th>
                </tr>
              </tfoot>
            </table>
          <!-- /.table-responsive -->
        </div>
        <div class="col-sm-12">
          <p style="font-size: 18px;"><?= lang('dashboard_accountlist_table_note'); ?></p>
        </div>
    </div>
  </div>
</div>
</div>
<script>
  $(document).ready(function() {
     dataset1 = $('#accounts').DataTable({
      /*order: [ 0, 'asc' ],*/
      pageLength: 1000,
      responsive: true,
      "order" : [],
       "ordering": false,
      dom : '<"html5buttons" B><"containerBtn"><"inputFiltro"f>',
      buttons : [{extend:'excel', title:'Companies List', className:'btnExportarExcel', exportOptions: {columns : [0,1,2,3,4]}}],
      oLanguage: {
        sLengthMenu: 'Mostrando _MENU_ registros por página',
        sZeroRecords: 'No se encontraron registros',
        sInfo: 'Mostrando _START_ a _END_ de _TOTAL_ registros',
        sInfoEmpty: 'Mostrando 0 a 0 de 0 registros',
        sInfoFiltered: '(Filtrado desde _MAX_ registros)',
        sSearch:         'Buscar: ',
        oPaginate:{
          sFirst:    'Primero',
          sLast:     'Último',
          sNext:     'Siguiente',
          sPrevious: 'Anterior'
        }
      }
      });
    var btnAcciones = '<div class="dropdown pull-right" id="">'+
    '<button class="btn btn-primary btn-sm btn-outline" type="button" id="accionesTabla" data-toggle="dropdown" aria-haspopup="true">Acciones<span class="caret"></span></button>'+
    '<ul class="dropdown-menu pull-right" aria-labelledby="accionesTabla">'+
    '<li><a onclick="$(\'.btnExportarExcel\').click()"><span class="glyphicon glyphicon-export"></span> <?= lang('export_xls') ?> </a></li>'+
    '</ul>'+
    '</div>';

    $('.containerBtn').html(btnAcciones);

    $(".sidebar-toggle").click(function() {
      setTimeout(function() {
        datatables.columns.adjust().draw();    
      },310);
    });
    $( ".main-sidebar" ).hover(function() {
      setTimeout(function() {
        datatables.columns.adjust().draw();    
      },310);
    });
  });
</script>
