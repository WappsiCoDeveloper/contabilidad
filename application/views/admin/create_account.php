<div class="row wrapper border-bottom white-bg page-heading">
  <div class="col-lg-8">
    <h2><?php echo $page_title; ?></h2>
    <ol class="breadcrumb">
      <li>
        <a href="">Inicio</a>
      </li>
      <li class="active">
        <strong><?php echo $page_title; ?></strong>
      </li>
    </ol>
  </div><!-- /.col -->
  <div class="col-lg-4">
    <div class="title-action">
      <button class="btn btn-success" onclick="$('#form_new_account').submit();"><span class="fa fa-check"></span>  <?=lang('create_account_submit_button');?></button>
    </div>
  </div>
</div><!-- /.row -->

<div class="row wrapper wrapper-content animated fadeInRight">
  <div class="ibox float-e-margins">
    <div class="ibox-content contentBackground">
        <div class="box-body">
        <?php echo form_open_multipart("admin/create_account", " id='form_new_account' ");?>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label><?php echo lang('create_account_label');?></label><span class='input_required'> *</span>

                        <div class="input-group">
                            <?php echo form_input($label);?>

                            <div class="input-group-addon">
                                <i>
                                    <div class="fa fa-info-circle" data-toggle="tooltip" title="<?php echo lang('create_account_label_note');?>">
                                    </div>
                                </i>
                            </div>
                        </div>
                        <!-- /.input group -->
                    </div>
                    <!-- /.form group -->
                    <p>
                        <label><?php echo lang('create_account_name');?></label><span class='input_required'> *</span>
                        <?php echo form_input($name);?>
                    </p>
                    <p>
                        <label><?php echo lang('create_account_address');?></label><span class='input_required'> *</span>
                        <?php echo form_input($address);?>
                    </p>
                    <p>
                        <label><?= lang('create_account_type_account'); ?></label><span class='input_required'> *</span>
                        <select class="form-control" name="type_account">
                            <option value="">Seleccione...</option>
                            <option value="1">NIF</option>
                            <option value="2">2649</option>
                        </select>
                    </p>
                </div>
                <div class="col-md-4">
                    <p>
                        <label><?php echo lang('create_account_email');?></label><span class='input_required'> *</span>
                        <?php echo form_input($email);?>
                    </p>
                    <div class="form-group">
                        <label><?php echo lang('create_account_decimal_places');?></label><span class='input_required'> *</span>

                        <div class="input-group">
                            <?php echo form_input($decimal_place);?>

                            <div class="input-group-addon">
                                <i>
                                    <div class="fa fa-info-circle" data-toggle="tooltip" title="<?php echo lang('create_account_decimal_places_note');?>">
                                    </div>
                                </i>
                            </div>
                        </div>
                        <!-- /.input group -->
                    </div>
                    <!-- /.form group -->
                    <p>
                        <label><?php echo lang('create_account_currency_symbol');?></label><span class='input_required'> *</span>
                        <?php echo form_input($currency);?>
                    </p>  
                    <div>
                        <label>Nit</label><span class='input_required'> *</span>
                        <input type="text" name="nit" class="form-control">
                    </div>              
                </div>

                <div class="col-md-4">
                    <div class="row">
                        <div class="col-md-6">
                            <p>
                                <label><?php echo lang('create_account_financial_year_start');?></label><span class='input_required'> *</span>
                                <?php echo form_input($fiscal_start);?>
                            </p>
                            <div class="form-group">
                                <label><?php echo lang('create_account_currency_format');?></label><span class='input_required'> *</span>
                                <select class="form-control" name="currency_format" id="currency_format">
                                    <option value="##,###.##"><?= lang('create_account_currency_format_option_1');?></option>
                                    <option value="##,##.##"><?= lang('create_account_currency_format_option_2');?></option>
                                    <option value="###,###.##"><?= lang('create_account_currency_format_option_3');?></option>
                                </select>
                            </div>
                            <!-- /.form group -->
                        </div>
                        <div class="col-md-6">
                            <p>
                                <label><?php echo lang('create_account_financial_year_end');?></label><span class='input_required'> *</span>
                                <?php echo form_input($fiscal_end);?>
                            </p>
                            <div class="form-group">
                                <label><?php echo lang('create_account_date_format');?></label><span class='input_required'> *</span>
                                <select class="form-control" name="date_format" id="date_format">
                                    <option value="d-M-Y|dd-M-yy"><?= lang('create_account_date_format_option_1');?></option>
                                    <option value="M-d-Y|M-dd-yy"><?= lang('create_account_date_format_option_2');?></option>
                                    <option value="Y-M-d|yy-M-dd"><?= lang('create_account_date_format_option_3');?></option>
                                </select>
                            </div>
                            <!-- /.form group -->
                        </div>
                    </div>
                    <p>
                        <div style="margin-left: 20px;">
                            <label><?= lang('create_account_logo_upload_label'); ?></label>
                            <input type="file" name="companylogoUpload" id="companylogoUpload">
                        </div>  
                    </p>
                    <div>
                        <label>Teléfono</label><span class='input_required'> *</span>
                        <input type="text" name="phone" class="form-control">
                    </div> 
                </div>
            </div>

            
          <div class="col-md-12" style="padding-bottom: 2%;">
            <label>
              <input type="checkbox" name="modulary" class="form-control" value="1"><?= lang('create_account_pos_modulary'); ?></label>
          </div>
            <div class="row">
                <div class="col-md-12">
                  <h2><?php echo lang('create_account_database_settings_heading');?></h2>
                </div>
                <div class="col-md-4">
                    <label><?php echo lang('create_account_database_name');?></label><span class='input_required'> *</span>
                    <?php echo form_input($db_name);?>
                </div>
                <div class="col-md-4">
                    <label><?php echo lang('create_account_database_host');?></label><span class='input_required'> *</span>
                    <?php echo form_input($db_host);?>
                </div>
                <div class="col-md-4">
                    <label><?php echo lang('create_account_database_login');?></label><span class='input_required'> *</span>
                    <?php echo form_input($db_username);?>
                </div>
                <div class="col-md-4">
                    <label><?php echo lang('create_account_database_password');?></label><span class='input_required'> *</span>
                    <?php echo form_input($db_password);?>
                </div>
            </div>
        </div>
        <div class="box-footer">
            <button type="submit" class="btn btn-success"><?=lang('create_account_submit_button');?></button>
            <a href="<?= base_url(); ?>admin" id="cancel" name="cancel" class="btn btn-default" style="margin-right: 5px;"><?=lang('create_account_cancel_button');?></a>
        </div>
        <?php echo form_close();?>
    </div>
  </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {


        /* Setup jQuery datepicker ui */
        $('#fiscal_start').datepicker({
            dateFormat: $("#date_format").val().split('|')[1],  /* Read the Javascript date format value */
            numberOfMonths: 1,
            beforeShow: function() {
                setTimeout(function(){
                    $('.ui-datepicker').css('z-index', 99999999999999);
                }, 0);
            },
            onClose: function(selectedDate) {
                $("#fiscal_end").datepicker("option", "minDate", selectedDate);
            }
        });
        $('#fiscal_end').datepicker({
            dateFormat: $("#date_format").val().split('|')[1],  /* Read the Javascript date format value */
            numberOfMonths: 1,
            beforeShow: function() {
                setTimeout(function(){
                    $('.ui-datepicker').css('z-index', 99999999999999);
                }, 0);
            },
            onClose: function(selectedDate) {
                $("#fiscal_start").datepicker("option", "maxDate", selectedDate);
            }
        });

        $("#date_format").change(function() {
            /* Read the Javascript date format value */
            dateFormat = $(this).val().split('|')[1];
            $("#fiscal_start").datepicker("option", "dateFormat", dateFormat);
            $("#fiscal_end").datepicker("option", "dateFormat", dateFormat);
        });


        $(document).on('keypress', 'form', function(e){   
        if(e == 13){
          return false;
        }
      });

    $(document).on('keypress', 'input', function(e){
        if(e.which == 13){
          return false;
        }
      });
    
    });
</script>