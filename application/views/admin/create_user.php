<script type="text/javascript">
  $(document).on('keypress', 'form', function(e){   
        if(e == 13){
          return false;
        }
      });

    $(document).on('keypress', 'input', function(e){
        if(e.which == 13){
          return false;
        }
      });
</script>

<div class="row wrapper border-bottom white-bg page-heading">
  <div class="col-lg-8">
    <h2><?php echo $page_title; ?></h2>
    <ol class="breadcrumb">
      <li>
        <a href="">Inicio</a>
      </li>
      <li class="active">
        <strong><?php echo $page_title; ?></strong>
      </li>
    </ol>
  </div><!-- /.col -->
  <div class="col-lg-4">
    <div class="title-action">
      <button class="btn btn-success" onclick="$('#form_new_user').submit();"><span class="fa fa-check"></span>  <?=lang('create_user_submit_btn');?></button>
    </div>
  </div>
</div><!-- /.row -->
<div class="row wrapper wrapper-content animated fadeInRight">
  <div class="ibox float-e-margins">
    <div class="ibox-content contentBackground">
    <?php echo form_open_multipart("admin/create_user", " id='form_new_user' ");?>
        <div class="row">
          <div class="col-md-12">
            <div style="margin: 20px;">
              <div class="col-sm-12 section-img-profile">
              <div class="fileinput fileinput-new" data-provides="fileinput" style="width: 200px;">
                <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 200px;">
                </div>
                <div>
                  <span class="btn btn-success btn-file btn-profile-img btn-profile-img-edit"><span class="fileinput-new"><i class="fa fa-upload"></i></span><span class="fileinput-exists"><i class="fa fa-pencil"></i></span><input type="file" name="uploadprofilepicture" id="uploadprofilepicture"></span>
                  <a href="#" class="btn btn-danger fileinput-exists btn-profile-img btn-profile-img-remove" data-dismiss="fileinput"><i class="fa fa-trash"></i></a>
                </div>
              </div>
            </div>
            </div>
          </div>
          <div class="col-md-4">
            <p>
              <?php echo lang('create_user_fname_label', 'first_name');?><span class='input_required'> *</span> <br />
              <?php echo form_input($first_name);?>
            </p>
            <p>
              <?php echo lang('create_user_lname_label', 'last_name');?><span class='input_required'> *</span> <br />
              <?php echo form_input($last_name);?>
            </p>
            <p>
              <?php echo lang('create_user_company_label', 'company');?><span class='input_required'> *</span> <br />
              <?php echo form_input($company);?>
            </p>
            <p>
              <?php echo lang('create_user_phone_label', 'phone');?><span class='input_required'> *</span> <br />
              <?php echo form_input($phone);?>
            </p>
          </div>
          <div class="col-md-4">
            <p>
              <?php echo lang('create_user_email_label', 'email');?><span class='input_required'> *</span> <br />
              <?php echo form_input($email);?>
            </p>
            <p>
              <?php echo lang('create_user_username_label', 'username');?><span class='input_required'> *</span> <br />
              <?php echo form_input($username);?>
            </p>
            <p>
              <?php echo lang('create_user_password_label', 'password');?><span class='input_required'> *</span> <br />
              <?php echo form_input($password);?>
            </p>
            <p>
              <?php echo lang('create_user_password_confirm_label', 'password_confirm');?><span class='input_required'> *</span> <br />
              <?php echo form_input($password_confirm);?>
            </p>
          </div>
          <div class="col-md-4">
            <h3><?php echo lang('edit_user_groups_heading');?><span class='input_required'> *</span></h3>
            <?php foreach ($groups as $group):?>
              <label class="radio">
              <input type="radio" name="groups" value="<?php echo $group['id'];?>" required>
              <?php echo htmlspecialchars($group['description'],ENT_QUOTES,'UTF-8');?>
              </label>
            <?php endforeach?>
          </div>
      </div>
        <div class="row" style="margin-bottom: 30px;">
          <div class="col-md-12">
            <h3><?=lang('create_user_accessible_accounts_label');?><span class='input_required'> *</span></h3>
            <div class="row">
              <?php foreach ($accounts as $account):?>
                <div class="col-md-4"> 
                  <?php if (isset($account->cost_centers)): ?>
                    <div class="row">
                      <div class="col-md-4">
                  <?php endif ?>
                        <label>
                          <input type="checkbox" name="accounts[]" class="account" value="<?= $account->id; ?>">
                          <?= (ucfirst(mb_strtolower($account->label))); ?>
                        </label>
                  <?php if (isset($account->cost_centers)): ?>
                      </div>
                      <div class="col-md-8">
                        <select name="cost_centers[<?= $account->id; ?>][]" class="form-control select" data-placeholder="<?= sprintf(lang('create_user_accessible_cost_centers_placeholder'), ucfirst(mb_strtolower($account->label))) ?>" multiple>
                        <?php foreach ($account->cost_centers as $cc): ?>
                          <option value="<?= $cc->id ?>"><?= ucfirst(mb_strtolower($cc->name)) ?></option>
                        <?php endforeach ?>
                        </select>
                      </div>
                    </div>
                  <?php endif ?>
                </div>
              <?php endforeach?>
            </div>  
          </div>
        </div>
      <div class="box-footer">
        <button type="submit" class="btn btn-success"><?=lang('create_user_submit_btn');?></button>
        <a href="<?= base_url(); ?>admin/users/" id="cancel" name="cancel" class="btn btn-default" style="margin-right: 5px;"><?=lang('create_user_cancel_btn');?></a>
      </div>
    <?php echo form_close();?>
    </div>
  </div>
</div>
<script type="text/javascript">
  $(document).ready(function(){
    $('.select').each(function(index, select){
      placeholder_text = $(this).data('placeholder');
      $(this).select2({
        placeholder : placeholder_text,
        disabled : true
      });
    });
  });


  $(document).on('ifChecked', '.account', function(){
    index = $(this).index('.account');
    $('.select').eq(index).select2({disabled : false});
  });

  $(document).on('ifUnchecked', '.account', function(){
    index = $(this).index('.account');
    $('.select').eq(index).select2({disabled : true});
  });

</script>
