
<script type="text/javascript">
  $(document).on('keypress', 'form', function(e){   
        if(e == 13){
          return false;
        }
      });

    $(document).on('keypress', 'input', function(e){
        if(e.which == 13){
          return false;
        }
      });
</script>

<section class="content">
  <div class="box">
    <div class="box-body">


<?php echo form_open("admin/create_group");?>

      <p>
            <?php echo lang('create_group_name_label', 'group_name');?> <br />
            <?php echo form_input($group_name);?>
      </p>

      <p>
            <?php echo lang('create_group_desc_label', 'description');?> <br />
            <?php echo form_textarea($description);?>
      </p>

    <div class="box-footer">
      <button type="submit" class="btn btn-primary"><?=lang('create_account_submit_button');?></button>
      <a href="<?= base_url(); ?>admin/user_permissions/" id="cancel" name="cancel" class="btn btn-default" style="margin-right: 5px;"><?=lang('create_account_cancel_button');?></a>
    </div>
<?php echo form_close();?>
</div>
</div>
</section>