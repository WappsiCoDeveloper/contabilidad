<?php
class Admin_Controller extends MY_Controller {

	public function __construct()
	{
		parent::__construct();

		if ($this->verify_login($this->mUri) !== true) {
			$this->session->set_flashdata('warning', lang('login_verification_warning'));
			redirect($this->verify_login($this->mUri));
		}

		if (!$this->verify_active_account()) {
			if ($this->mCtrler !== 'admin' && $this->mCtrler !== 'user') {
				$this->session->set_flashdata('error', lang('admin_controller_active_account_not_verify_error'));
				redirect('user/activate');
			}
		}

		if ($this->verify_active_account()) {
			$this->DB1 = $this->load->database($_SESSION['active_account_config'], TRUE);
			$this->DB1->reconnect();

			$this->mAccountSettings = $this->DB1->get('settings'.$this->DB1->dbsuffix)->row();
			$this->mComercialAccountSettings = $this->DB1->get('settings')->row();
			$this->mDateArray = explode('|', $this->mAccountSettings->date_format);
			$this->data['logs'] = array();

			if ($this->mViewLog && ($this->mUri === 'accounts/log' || $this->mUri === 'dashboard/index')) {

				if ($this->mUri === 'dashboard/index') {
					$this->DB1->limit(25);
				}

				$this->DB1->order_by('date', 'desc');
				$logs = $this->DB1->get('logs'.$this->DB1->dbsuffix)->result_array();
				$this->data['logs'] = $logs;
			}
			$fy_end_year = date('Y', strtotime($this->mAccountSettings->fy_end));
			if ($fy_end_year == date('Y')) {
				$start_date_filter = date('Y-m-01');
				$end_date_filter = date('Y-m-d');
			} else {
				if ($fy_end_year < date('Y')) {
				  $start_date_filter = date($fy_end_year.'-12-01');
				  $end_date_filter = date($fy_end_year.'-12-31');
				} else if ($fy_end_year > date('Y')) {
				  $start_date_filter = date($fy_end_year.'-01-01');
				  $end_date_filter = date($fy_end_year.'-01-31');
				}
			}
			$this->start_date_filter = $start_date_filter;
			$this->end_date_filter = $end_date_filter;
			$this->niveles = $this->get_groups_levels();
			// $this->functionscore->print_arrays($this->niveles);
		}
	}

	public function reconnect_persistent_database()
	{
		if (isset($this->DB1) && $this->DB1->pconnect) {
			$this->load->database();
        	$this->DB1->reconnect();
        }
	}
	
	public function get_groups_levels(){
		$query = "SELECT
					IF(GP23_2.id IS NOT NULL AND GP23_3.id IS NULL, 2,
						IF(GP23_3.id IS NOT NULL AND GP23_4.id IS NULL, 3,
							IF(GP23_4.id IS NOT NULL AND GP23_5.id IS NULL, 4,
								IF(GP23_5.id IS NOT NULL AND GP23_6.id IS NULL, 5,
									IF(GP23_6.id IS NOT NULL AND GP23_7.id IS NULL, 6,
										IF(GP23_7.id IS NOT NULL AND GP23_8.id IS NULL, 7, 1)))))) as level,
					GP23.code
					FROM {$this->DB1->dbprefix('groups')}{$this->DB1->dbsuffix} GP23
						LEFT JOIN {$this->DB1->dbprefix('groups')}{$this->DB1->dbsuffix} GP23_2 ON GP23_2.id = GP23.parent_id
						LEFT JOIN {$this->DB1->dbprefix('groups')}{$this->DB1->dbsuffix} GP23_3 ON GP23_3.id = GP23_2.parent_id
						LEFT JOIN {$this->DB1->dbprefix('groups')}{$this->DB1->dbsuffix} GP23_4 ON GP23_4.id = GP23_3.parent_id
						LEFT JOIN {$this->DB1->dbprefix('groups')}{$this->DB1->dbsuffix} GP23_5 ON GP23_5.id = GP23_4.parent_id
						LEFT JOIN {$this->DB1->dbprefix('groups')}{$this->DB1->dbsuffix} GP23_6 ON GP23_6.id = GP23_5.parent_id
						LEFT JOIN {$this->DB1->dbprefix('groups')}{$this->DB1->dbsuffix} GP23_7 ON GP23_7.id = GP23_6.parent_id
						LEFT JOIN {$this->DB1->dbprefix('groups')}{$this->DB1->dbsuffix} GP23_8 ON GP23_8.id = GP23_7.parent_id
					GROUP BY GP23.code";
		$levels=$this->DB1->query($query)->result();
		$niveles = [];
		foreach ($levels as $row_level) {
			$niveles[$row_level->level][] = $row_level->code;
		}
		// $this->functionscore->print_arrays($niveles);
		return $niveles;
	}
}

