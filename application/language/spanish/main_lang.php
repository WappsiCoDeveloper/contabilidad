<?php
/*
|--------------------------------------------------------------------------
| General labels
|--------------------------------------------------------------------------
*/
$lang['export_xls'] = 'Exportar a XLS';
$lang["actions"] = "Acciones";
$lang["print"] = "Imprimir";
$lang["close"] = "Cerrar";

// login verification warning Alert
$lang['login_verification_warning'] 		= 'Por favor ingresar para continuar';


// Page Titles
// Admin
$lang['page_title_admin_index'] 					= 'Compañías';
$lang['page_title_admin_settings'] 					= 'Configuración';
$lang['page_title_admin_users'] 					= 'Lista de Usuarios';
$lang['page_title_admin_create_user'] 				= 'Nuevo Usuario';
$lang['page_title_admin_edit_user'] 				= 'Editar Usuario';
$lang['page_title_admin_accounts'] 					= 'Lista de Compañías';
$lang['page_title_admin_create_account'] 			= 'Nueva Compañía';
$lang['page_title_admin_groups'] 					= 'Lista de Grupos de Usuario';
$lang['page_title_admin_create_group'] 				= 'Nuevo Grupo de Usuario';
$lang['page_title_admin_edit_group'] 				= 'Editar Grupo de Usuario';
$lang['page_title_admin_edit_permission'] 			= 'Editar Permisos de Grupo';
$lang['page_title_admin_log'] 						= 'Resumen de Actividades';

// User
$lang['page_title_user_activate'] 					= 'Activar/Desactivar Cuenta';

// Accounts Dashboard
$lang['page_title_dashboard_index'] 				= 'Resumen de Cuentas Contables';

// Accounts
$lang['page_title_accounts_index'] 					= 'Plan de Cuentas';

// Entries
$lang['page_title_entries_index'] 					= 'Lista de Asientos Contables';
$lang['page_title_entries_add'] 					= 'Nuevo Asiento Contable';
$lang['page_title_entries_edit'] 					= 'Editar Asiento Contable';
$lang['page_title_entries_view'] 					= 'Ver Asiento Contable';
$lang['page_title_entries_import'] 					= 'Importar asientos';

// Groups
$lang['page_title_groups_add'] 						= 'Nuevo Grupo de Cuentas';
$lang['page_title_groups_edit'] 					= 'Editar Grupo';

// Ledgers
$lang['page_title_ledgers_add'] 					= 'Nueva Cuenta Auxiliar';
$lang['page_title_ledgers_edit'] 					= 'Editar Auxiliar';

// Reports
$lang['page_title_reports_balancesheet'] 			= 'Balance General';
$lang['page_title_reports_profitloss'] 				= 'Ganancias y Pérdidas';
$lang['page_title_reports_trialbalance'] 			= 'Balance de Prueba';
$lang['page_title_reports_ledgerstatement'] 		= 'Informe Auxiliar de Cuentas';
$lang['page_title_reports_ledgerentries'] 			= 'Informe Asientos Contables';
$lang['page_title_reports_reconciliation'] 			= 'Informe de Conciliación';
$lang['page_title_reports_third_and_accounts'] 		= 'Informe Auxiliar de cuentas/terceros.';
$lang['select_a_ledger'] 							= 'Por favor seleccione:';

//Account_Settings
$lang['page_title_account_settings_cf'] 			= 'Cierre de Período';
$lang['page_title_account_settings_main'] 			= 'Configuración de Cuenta Activa';
$lang['page_title_account_settings_entrytype'] 		= 'Tipos de documentos Contables';
$lang['page_title_account_settings_lock'] 			= 'Bloquear Cuenta Activa';
$lang['page_title_account_settings_printer'] 		= 'Configuración de Impresora';
$lang['page_title_account_settings_tags'] 			= 'Etiquetas de Cuentas';
$lang['page_title_account_settings_email'] 			= 'Configuración de Correo';

// Login
$lang['page_title_login_index'] 					= 'Ingreso al Sistema';

//Search
$lang['page_title_search_index'] 					= 'Búsqueda Avanzada';

//Thirds
$lang['page_title_thirds_index'] 					= 'Terceros';
$lang['page_title_thirds_add'] 						= 'Añadir tercero';
$lang['page_title_thirds_edit'] 					= 'Editar tercero';
$lang['page_title_thirds_import'] 					= 'Importar terceros';
$lang['page_title_thirds_relations'] 				= 'Relación balance de apertura terceros y cuentas auxiliares';
	//Thirds messages
	$lang['thirds_not_permited_message']						= 'No tiene permiso para acceder a esta página.';
	$lang['thirds_not_specified_message']						= 'No se especificó un tercero.';
	$lang['thirds_not_found_message']							= 'No se encontró el tercero especificado.';
	$lang['thirds_cntrler_add_third_created_successfully']		= 'Se creó con éxito el tercero.';
	$lang['thirds_cntrler_add_third_created_unsuccessfully']	= 'No se pudo crear el tercero.';
	$lang['thirds_cntrler_add_third_updated_successfully']		= 'Se actualizó con éxito el tercero.';
	$lang['thirds_cntrler_add_third_updated_unsuccessfully']	= 'No se pudo actualizar el tercero.';
	//validation labels
	$lang['thirds_cntrler_add_form_validation_group_id_label']  = 'Perfil de tercero';
	$lang['thirds_cntrler_add_form_validation_customer_group_id_label']  = 'Grupo de clientes';
	$lang['thirds_cntrler_add_form_validation_name_label']  = 'Nombre';
	$lang['thirds_cntrler_add_form_validation_company_label']  = 'Compañía';
	$lang['thirds_cntrler_add_form_validation_vat_no_label']  = 'Número de documento';
	$lang['thirds_cntrler_add_form_validation_address_label']  = 'Dirección';
	$lang['thirds_cntrler_add_form_validation_country_label']  = 'País';
	$lang['thirds_cntrler_add_form_validation_state_label']  = 'Departamento';
	$lang['thirds_cntrler_add_form_validation_city_label']  = 'Ciudad';
	$lang['thirds_cntrler_add_form_validation_postal_code_label']  = 'Código postal';
	$lang['thirds_cntrler_add_form_validation_phone_label']  = 'Teléfono';
	$lang['thirds_cntrler_add_form_validation_email_label']  = 'Correo electrónico';
	$lang['thirds_cntrler_add_form_validation_price_group_label']  = 'Grupo de precios';

	//Form labels
	$lang['thirds_cntrler_add_form_group_id_label']  = 'Tipo de tercero';
	$lang['thirds_cntrler_add_form_name_label']  = 'Nombre';
	$lang['thirds_cntrler_add_form_vat_no_label']  = 'Número de documento/NIT';
	$lang['thirds_cntrler_add_form_address_label']  = 'Dirección';
	$lang['thirds_cntrler_add_form_country_label']  = 'País';
	$lang['thirds_cntrler_add_form_state_label']  = 'Departamento';
	$lang['thirds_cntrler_add_form_city_label']  = 'Ciudad';
	$lang['thirds_cntrler_add_form_postal_code_label']  = 'Código postal';
	$lang['thirds_cntrler_add_form_phone_label']  = 'Teléfono';
	$lang['thirds_cntrler_add_form_email_label']  = 'Correo electrónico';



// Page Titles End

// My_Controller Start

// [$this->mMenu]
// Sidebar Menu (Admin)
$lang['sidebar_menu_label1'] 								= 'Menú de Administración';
$lang['sidebar_menu_admin_home'] 							= 'Inicio';
$lang['sidebar_menu_admin_thirds'] 							= 'Terceros';
$lang['sidebar_menu_admin_accounts'] 						= 'Compañías';
$lang['sidebar_menu_admin_accounts_child_manage'] 			= 'Lista de Compañías';
$lang['sidebar_menu_admin_accounts_child_create'] 			= 'Nueva Compañía';
$lang['sidebar_menu_admin_users'] 							= 'Usuarios';
$lang['sidebar_menu_admin_users_child_manage'] 				= 'Lista de Usuarios';
$lang['sidebar_menu_admin_users_child_create'] 				= 'Nuevo Usuario';
$lang['sidebar_menu_admin_users_child_groups'] 				= 'Grupos y Permisos';
$lang['sidebar_menu_admin_settings'] 						= 'Configuración General';

//Sidebar Menu (Accounts)
$lang['sidebar_menu_label'] 								= 'Menú de Compañías';
$lang['sidebar_menu_home'] 									= 'Inicio';
$lang['sidebar_menu_accounts'] 								= 'Plan de Cuentas';
$lang['sidebar_menu_entries'] 								= 'Asientos Contables';
$lang['sidebar_menu_search'] 								= 'Buscar';
$lang['sidebar_menu_reports'] 								= 'Informes';
$lang['sidebar_menu_reports_child_balancesheet'] 			= 'Balance General';
$lang['sidebar_menu_reports_child_profitloss'] 				= 'Ganancias y Pérdidas';
$lang['sidebar_menu_reports_child_trialbalance'] 			= 'Balance de Prueba';
$lang['sidebar_menu_reports_child_ledgerstatement'] 		= 'Auxiliar de Cuentas';
$lang['sidebar_menu_reports_child_ledgerentries'] 			= 'Asientos Contables';
$lang['sidebar_menu_reports_child_reconciliation'] 			= 'Conciliación';
$lang['sidebar_menu_reports_child_third_and_accounts'] 		= 'Auxiliar de cuentas/terceros';
$lang['sidebar_menu_reports_child_certificate_withholdings']= 'Certificado de retenciones';
$lang['sidebar_menu_account_settings'] 						= 'Configuración';
$lang['sidebar_menu_account_settings_child_main'] 			= 'Compañía';
$lang['sidebar_menu_account_settings_child_cf'] 			= 'Cierre de Período';
$lang['sidebar_menu_account_settings_child_email'] 			= 'Correo';
$lang['sidebar_menu_account_settings_child_printer']		= 'Impresora';
$lang['sidebar_menu_account_settings_child_entrytypes']		= 'Tipos de documentos';
$lang['sidebar_menu_account_settings_child_tags'] 			= 'Etiquetas';
$lang['sidebar_menu_account_settings_child_lock'] 			= 'Bloqueo de Cuenta';
$lang['sidebar_menu_account_settings_accounts_parameter'] 	= 'Parámetros de cuentas';
$lang['sidebar_menu_account_settings_payment_methods'] 		= 'Medios de pago';
$lang['sidebar_menu_account_settings_cost_centers'] 		= 'Centros de costos';
$lang['sidebar_menu_thirds_child_relations'] 				= 'Saldos terceros';
$lang['sidebar_menu_thirds_child_admin'] 					= 'Lista de terceros';
// [$this->mMenu] [End]

// WARNINGS
$lang['my_controller_admin_warning'] 						= 'Debe ser un <strong><em>Administrador</em></strong> para acceder a la página de <strong><em>Admin</em></strong>.';
$lang['my_controller_not_permitted_warning']				= 'No cuenta con permiso para acceder <strong>%s</strong> a la página.';

// ERRORS
$lang['my_controller_group_permissions_not_found_error']	= '<strong>Error 404 </strong> Permisos de Grupo no encontrado';

// My_Controller [End]


// Admin_Controller [Start]
// ERRORS
$lang['admin_controller_active_account_not_verify_error']			= 'Por favor activar una Compañía/Año para continuar';
$lang['admin_controller_no_users_found_error']						= '<strong>Error 404</strong> No se encontraron Usuarios';
$lang['admin_controller_create_account_image_not_uploaded_error']	= 'La imagen no ha sido cargada';

// Admin_Controller [End]


// _partials Start
// Navbar Main Header
// active account
$lang['main_header_active_account'] 							= 'Compañía activa : ';
$lang['main_header_active_account_NONE'] 						= 'Sin compañía activa';

// user dropsown
$lang['main_header_user_dropdown_profile_btn_label'] 			= 'Perfil';
$lang['main_header_user_dropdown_logout_btn_label'] 			= 'Salir';
$lang['main_header_user_dropdown_updateuserimage_btn_label'] 	= 'Cambiar Avatar';
$lang['main_header_user_dropdown_member_since'] 				= 'Miembro desde<br><strong style="font-size: 14px;"> %s </strong> - <em style="font-size: 14px;"> %s </em>';

// Sidebar Menu Header
$lang['sidebar_account_not_active_msg'] 						= "<strong>No hay Compañía Activa</strong><br><a href='%s'  style='color: #3c8dbc;'> Haga click acá</a> para seleccionar una Compañía";

// Right Sidebar
$lang['right_sidebar_menu_activity_log'] 						= 'Resumen de Actividades';
$lang['right_sidebar_menu_view_all_log'] 						= 'Click para ver todas las actividades';

// _partials End


// Admin Start
// Views
// admin/dashboard
$lang['dashboard_create_account_label'] 									= 'Nueva';
$lang['dashboard_create_account_label_description'] 						= 'Crear una nueva Compañía ';
$lang['dashboard_manage_account_label'] 									= 'Lista de Compañías';
$lang['dashboard_manage_account_label_description'] 						= 'Administrar Compañías y estados ';
$lang['dashboard_manage_user_label'] 										= 'Lista de Usuarios';
$lang['dashboard_manage_user_label_description'] 							= 'Administrar Usuarios y Permisos ';
$lang['dashboard_general_settings_label'] 									= 'Configuración General';
$lang['dashboard_general_settings_label_description'] 						= 'Configuración General de Wappsi Contabilidad ';
$lang['dashboard_accountlist_table_heading'] 								= 'Lista de Años/Compañías';
$lang['dashboard_accountlist_table_sub_heading'] 							= 'Año/Compañía activa : ';
$lang['dashboard_accountlist_table_sub_heading_option'] 					= 'Ninguna';
$lang['dashboard_accountlist_table_label'] 									= 'Etiqueta';
$lang['dashboard_accountlist_table_name'] 									= 'Nombre';
$lang['dashboard_accountlist_table_fiscal_year'] 							= 'Año Fiscal';
$lang['dashboard_accountlist_table_status'] 								= 'Estado';
$lang['dashboard_accountlist_table_actions'] 								= 'Acciones';
$lang['dashboard_accountlist_table_status_tooltip_title_active'] 			= 'Hacer clic para desactivar';
$lang['dashboard_accountlist_table_status_label_active'] 					= 'Activo';
$lang['dashboard_accountlist_table_status_tooltip_title_active_locked']		= 'Hacer clic para desactivar';
$lang['dashboard_accountlist_table_status_label_active_locked'] 			= 'Activo y bloqueado';
$lang['dashboard_accountlist_table_status_tooltip_title_inactive'] 			= 'Hacer clic para activar';
$lang['dashboard_accountlist_table_status_label_inactive'] 					= 'Inactivo';
$lang['dashboard_accountlist_table_status_tooltip_title_locked_inactive'] 	= 'Cuenta bloqueada. Hacer clic para activar.';
$lang['dashboard_accountlist_table_status_label_locked_inactive'] 			= 'Bloqueada';
$lang['dashboard_accountlist_table_note'] 									= '<strong>Nota:</strong> <em>Si desea usar múltiples cuentas simultaneamente, debe usar un navegador diferente para cada una.</em>';
$lang['dashboard_accountlist_table_fiscal_year_to'] 						= '</strong> a <strong>';

// admin/accounts
$lang['accounts_heading'] 								= 'Lista de compañías';
$lang['companylist_table_label'] 						= 'Etiqueta';
$lang['companylist_table_db_type'] 						= 'Tipo BD';
$lang['companylist_table_db_name'] 						= 'Nombre BD';
$lang['companylist_table_db_host'] 						= 'Host BD';
$lang['companylist_table_db_port'] 						= 'Puerto BD';
$lang['companylist_table_db_prefix'] 					= 'Prefijo BD';
$lang['companylist_table_db_suffix'] 					= 'Sufijo BD';
$lang['accounts_add_btn'] 								= 'Crear nueva compañía';

// admin/create_account
$lang['create_account_heading']                     	= 'Crear Compañía';
$lang['create_account_subheading']                  	= 'Por favor, ingrese la información de la compañía.';
$lang['create_account_label']                       	= 'Etiqueta';
$lang['create_account_label_note']                  	= "Nota: Se recomienda usar una etiqueta descriptiva la cual incluya un nombre corto y el año actual, ejemplo : 'ejemplo20142105'.";

$lang['create_account_name']                     		= 'Nombre de compañía';
$lang['create_account_currency_symbol']             	= 'Símbolo de moneda';
$lang['create_account_email']                       	= 'Correo';
$lang['create_account_address']                     	= 'Dirección';
$lang['create_account_decimal_places']              	= 'N° de posiciones decimales';
$lang['create_account_type_account']                    = 'Tipo de estructura de cuentas';

$lang['create_account_decimal_places_note']         	= 'Nota: Estas opciones no se pueden cambiar después.';

$lang['create_account_financial_year_start']        	= 'Inicio de año fiscal';
$lang['create_account_financial_year_end']        		= 'Finalización de año fiscal';
$lang['create_account_currency_format']        			= 'Formato de moneda';
$lang['create_account_currency_format_option_1']        = '12,345.78';
$lang['create_account_currency_format_option_2']        = '12,34.56';
$lang['create_account_currency_format_option_3']        = '123,456.78';
$lang['create_account_currency_format_note']        	= '<strong>Nota:</strong> Check the wiki if you want to create a custom format for currency.';
$lang['create_account_date_format']        				= 'Formato de fecha';
$lang['create_account_date_format_option_1']        	= 'Día-Mes-Año';
$lang['create_account_date_format_option_2']        	= 'Mes-Día-Año';
$lang['create_account_date_format_option_3']        	= 'Año-Mes-Día';
$lang['create_account_database_prefix']        			= 'Prefijo de base de datos';
$lang['create_account_database_suffix']        			= 'Sufijo de base de datos';
$lang['create_account_database_prefix_note']        	= 'Nota: Database table prefix to use (optional). All tables for this account will be created with this prefix, useful if you have only one database available and want to use multiple accounts.';
$lang['create_account_logo_upload_label'] 				= 'Seleccione imagen para subir';
$lang['create_account_database_settings_heading']   	= 'Configuración de base de datos';
$lang['create_account_database_type']        			= 'Tipo de base de datos';
$lang['create_account_database_name']        			= 'Nombre de base de datos';
$lang['create_account_database_host']              	 	= 'Host de base de datos';
$lang['create_account_database_port']           		= 'Puerto de base de datos';
$lang['create_account_database_login']            		= 'Nombre de usuario de base de datos';
$lang['create_account_database_password']         		= 'Contraseña de usuario de base de datos';
$lang['create_account_database_confirm_password']   	= 'Confirme la contraseña';
$lang['create_account_use_persistent_connection']   	= 'Usar conexión persistente';
$lang['create_account_use_persistent_connection_label'] = 'Si';
$lang['create_account_pos_modulary'] 					= 'Modular con POS';
$lang['create_account_cost_center'] 					= 'Manejo de centro de costos';
$lang['create_account_submit_button']            		= 'Enviar';
$lang['create_account_cancel_button']            		= 'Cancelar';

// admin/edit_permission
$lang['edit_permission_heading'] 								= 'Permisos de Usuario';
$lang['edit_permission_module_name_label'] 						= 'Nombre de módulo';
$lang['edit_permission_permissions_label'] 						= 'Permisos';
$lang['edit_permission_view_label'] 							= 'Ver';
$lang['edit_permission_add_label'] 								= 'Crear';
$lang['edit_permission_edit_label'] 							= 'Editar';
$lang['edit_permission_delete_label'] 							= 'Eliminar';
$lang['edit_permission_miscellaneous_label'] 					= 'Otros';
$lang['edit_permission_accounts_label'] 						= 'Compañías';
$lang['edit_permission_admin_log_label'] 						= ' Ver registro de actividades';
$lang['edit_permission_search_index_label'] 					= ' Buscar - Página principal';
$lang['edit_permission_dashboard_label'] 						= ' Dashboard';
$lang['edit_permission_entries_label'] 							= 'Asientos contables';
$lang['edit_permission_entries_view_single_entry_label'] 		= ' Asientos contables - Ver info de un asiento contable';
$lang['edit_permission_entries_approve_entry_label'] 			= ' Asientos contables - Aprobar un asiento contable';
$lang['edit_permission_groups_label'] 							= 'Grupos';
$lang['edit_permission_ledgers_label'] 							= 'Auxiliar de cuentas';
$lang['edit_permission_thirds_label'] 							= 'Terceros';
$lang['edit_permission_account_settings_label'] 				= 'Configuración de compañía';
$lang['edit_permission_account_settings_index_label'] 			= ' Configuración de compañía - Index';
$lang['edit_permission_account_settings_main_label'] 			= ' Configuración de compañía - Principal';
$lang['edit_permission_account_settings_cf_label'] 				= ' Configuración de compañía - Cf';
$lang['edit_permission_account_settings_email_label'] 			= ' Configuración de compañía - Email';
$lang['edit_permission_account_settings_printer_label'] 		= ' Configuración de compañía - Impresión';
$lang['edit_permission_account_settings_tags_label'] 			= ' Configuración de compañía - Etiquetas';
$lang['edit_permission_account_settings_entrytypes_label'] 		= ' Configuración de compañía - Tipos de documento contable';
$lang['edit_permission_account_settings_lock_label'] 			= ' Configuración de compañía - Bloqueo';
$lang['edit_permission_account_settings_accounts_parameter_label'] 			= ' Configuración de compañía - Parámetros de cuentas';
$lang['edit_permission_account_settings_payment_methods_label'] 			= ' Configuración de compañía - Medios de pago';
$lang['edit_permission_report_label'] 							= 'Reportes';
$lang['edit_permission_report_index_label'] 					= ' Reportes - Index';
$lang['edit_permission_report_balancesheet_label'] 				= ' Reportes - Balance General';
$lang['edit_permission_report_profitloss_label'] 				= ' Reportes - Ganancias y pérdidas';
$lang['edit_permission_report_trialbalance_label'] 				= ' Reportes - Balance de pruebas';
$lang['edit_permission_report_ledger_statement_label'] 			= ' Reportes - Auxiliar de cuentas';
$lang['edit_permission_report_ledger_entries_label'] 			= ' Reportes - Asientos Contables';
$lang['edit_permission_report_reconciliation_label'] 			= 'Informe de  Conciliación';
$lang['edit_permission_report_thirdsaccounts_label'] 			= 'Reportes - Auxiliar de cuentas/terceros';
$lang['edit_permission_submit_btn'] 							= 'Enviar';

// admin/settings
$lang['admin_settings_sitename_label'] 							= 'Nombre de entorno';
$lang['admin_settings_sitename_placeholder'] 					= 'Ingrese nombre de entorno';
$lang['admin_settings_date_format_label'] 						= 'Formato de fecha';
$lang['admin_settings_date_format_option_1'] 					= 'Día-Mes-Año';
$lang['admin_settings_date_format_option_2'] 					= 'Mes-Día-Año';
$lang['admin_settings_date_format_option_3'] 					= 'Año-Mes-Día';
$lang['admin_settings_entry_form_label'] 						= 'Tipo de formulario de entrada';
$lang['admin_settings_entry_form_1'] 							= 'Forma tradicional';
$lang['admin_settings_entry_form_2'] 							= 'Forma de entrada rápida';
$lang['admin_settings_in_entries_use_label'] 					= 'Usar en Asientos contables';
$lang['admin_settings_in_entries_use_option_1'] 				= 'D / C';
$lang['admin_settings_in_entries_use_option_2'] 				= 'To / By';
$lang['admin_settings_in_entries_use_tooltip'] 					= "Ya sea usar D/C o To/By en las Asientos contables.";
$lang['admin_settings_enable_logging_label'] 					= ' Activar registro de actividades';
$lang['admin_settings_enable_logging_tooltip'] 					= 'Nota: Registro de actividades sobre las compañias, éstos se visualizan en el Dashboard de compañías.';
$lang['admin_settings_email_settings_heading'] 					= 'Configuración de Email';
$lang['admin_settings_email_protocol_label'] 					= 'Protocolo de Email';
$lang['admin_settings_email_protocol_option_1'] 				= 'SMTP';
$lang['admin_settings_email_protocol_option_2'] 				= 'Función de correo';
$lang['admin_settings_smtp_username_label'] 					= 'Usuario SMTP';
$lang['admin_settings_smtp_username_placeholder'] 				= 'Ingrese usuario';
$lang['admin_settings_smtp_host_label'] 						= 'Host SMTP';
$lang['admin_settings_smtp_host_placeholder'] 					= 'Ingrese el Host';
$lang['admin_settings_smtp_password_label'] 					= 'Contraseña SMTP';
$lang['admin_settings_smtp_password_placeholder'] 				= 'Ingrese la cotnraseña';
$lang['admin_settings_smtp_port_label'] 						= 'Puerto SMTP';
$lang['admin_settings_smtp_port_placeholder'] 					= 'Ingrese puerto';
$lang['admin_settings_smtp_tls_label'] 							= 'Usar TLS';
$lang['admin_settings_email_from_label'] 						= 'Email';
$lang['admin_settings_email_from_placeholder'] 					= 'Ingrese Email';
$lang['admin_settings_submit_btn'] 								= 'Actualizar';
$lang['admin_settings_cancel_btn'] 								= 'Cancelar';

// admin/users

$lang['users_create_user_label'] 										= 'Nuevo usuario';  // 22/10/2018

// admin/user_permission
$lang['user_permission_heading']								= 'Grupos & permisos';
$lang['user_permission_table_id']								= 'ID';
$lang['user_permission_table_group']							= 'Grupo';
$lang['user_permission_table_action']							= 'Acciones';
$lang['user_permission_edit_group_permission_tooltip']			= 'Editar permisos';
$lang['user_permission_edit_group_tooltip']						= 'Editar grupo';
$lang['user_permission_delete_group_tooltip']					= 'Eliminar grupo';
$lang['user_permission_add_group_btn']							= 'Crear';

// admin/create_user
$lang['admin_cntrler_uploadprofilepicture_validation'] 			= 'Seleccione imagen de perfil';

// Controller
// create Account
$lang['admin_cntrler_create_account_validation_label'] = 'Etiqueta';
$lang['admin_cntrler_create_account_validation_name'] = 'Nombre';
$lang['admin_cntrler_create_account_validation_nit'] = 'Nit';
$lang['admin_cntrler_create_account_validation_phone'] = 'Teléfono';
$lang['admin_cntrler_create_account_validation_address'] = 'Dirección';
$lang['admin_cntrler_create_account_validation_email'] = 'Email';
$lang['admin_cntrler_create_account_validation_currency'] = 'Moneda';
$lang['admin_cntrler_create_account_validation_currency_format'] = 'Formato de moneda';
$lang['admin_cntrler_create_account_validation_decimal_place'] = 'Posiciones decimales';
$lang['admin_cntrler_create_account_validation_date_format'] = 'Formato de fecha';
$lang['admin_cntrler_create_account_validation_fiscal_start'] = 'Inicio de año fiscal';
$lang['admin_cntrler_create_account_validation_fiscal_end'] = 'Find e año fiscal';
$lang['admin_cntrler_create_account_validation_db_type'] = 'Tipo de BD';
$lang['admin_cntrler_create_account_validation_db_name'] = 'Nombre de BD';
$lang['admin_cntrler_create_account_validation_db_host'] = 'Host de BD';
$lang['admin_cntrler_create_account_validation_db_port'] = 'Puerto de BD';
$lang['admin_cntrler_create_account_validation_db_username'] = 'Usuario de BD';
$lang['admin_cntrler_create_account_validation_db_password'] = 'Contraseña de BD';

// edit permission
$lang['admin_cntrler_edit_permission_validation_group_id'] = 'ID de grupo';
$lang['admin_cntrler_edit_permission_validation_account_index'] = 'Compañía - Index';
$lang['admin_cntrler_edit_permission_validation_admin_log'] = 'Admin - log';
$lang['admin_cntrler_edit_permission_validation_dashboard_index'] = 'Dashboard - Index';
$lang['admin_cntrler_edit_permission_validation_entries_view'] = 'Asientos contables - Ver';
$lang['admin_cntrler_edit_permission_validation_entries_add'] = 'Asientos contables - Crear';
$lang['admin_cntrler_edit_permission_validation_entries_edit'] = 'Asientos contables - Editar';
$lang['admin_cntrler_edit_permission_validation_entries_delete'] = 'Asientos contables - Eliminar';
$lang['admin_cntrler_edit_permission_validation_entries_view_single'] = 'Asientos contables - Sólo ver';
$lang['admin_cntrler_edit_permission_validation_entries_approve'] = 'Asientos contables - Aprobar asiento contable';
$lang['admin_cntrler_edit_permission_validation_search_index'] = 'Búsqueda - Index';
$lang['admin_cntrler_edit_permission_validation_groups_add'] = 'Grupo de cuentas - Crear';
$lang['admin_cntrler_edit_permission_validation_groups_edit'] = 'Grupo de cuentas - Edit';
$lang['admin_cntrler_edit_permission_validation_groups_delete'] = 'Grupo de cuentas - Delete';
$lang['admin_cntrler_edit_permission_validation_thirds_index'] = 'Terceros - Index';
$lang['admin_cntrler_edit_permission_validation_thirds_add'] = 'Terceros - Crear';
$lang['admin_cntrler_edit_permission_validation_thirds_edit'] = 'Terceros - Editar';
$lang['admin_cntrler_edit_permission_validation_thirds_delete'] = 'Terceros - Eliminar';
$lang['admin_cntrler_edit_permission_validation_ledgers_add'] = 'Cuentas auxiliares - Crear';
$lang['admin_cntrler_edit_permission_validation_ledgers_edit'] = 'Cuentas auxiliares - Editar';
$lang['admin_cntrler_edit_permission_validation_ledgers_delete'] = 'Cuentas auxiliares - Eliminar';
$lang['admin_cntrler_edit_permission_validation_account_settings_index'] = 'Configuración de compañía - Index';
$lang['admin_cntrler_edit_permission_validation_account_settings_ main'] = 'Configuración de compañía - Main';
$lang['admin_cntrler_edit_permission_validation_account_settings_cf'] = 'Configuración de compañía - Cf';
$lang['admin_cntrler_edit_permission_validation_account_settings_email'] = 'Configuración de compañía - Email';
$lang['admin_cntrler_edit_permission_validation_account_settings_printer'] = 'Configuración de compañía - Impresión';
$lang['admin_cntrler_edit_permission_validation_account_settings_tags'] = 'Configuración de compañía - Etiquetas';
$lang['admin_cntrler_edit_permission_validation_account_settings_entrytypes'] = 'Configuración de compañía - Tipos de documentos contables';
$lang['admin_cntrler_edit_permission_validation_account_settings_lock_account'] = 'Configuración de compañía - Bloqueo';
$lang['admin_cntrler_edit_permission_validation_account_settings_accounts_parameter'] = 'Configuración de compañía - Parámetros de cuenta';
$lang['admin_cntrler_edit_permission_validation_account_settings_payment_methods'] = 'Configuración de compañía - Medios de pago';
$lang['admin_cntrler_edit_permission_validation_reports_index'] = 'Reportes - Index';
$lang['admin_cntrler_edit_permission_validation_reports_balancesheet'] = 'Reportes - Balance general';
$lang['admin_cntrler_edit_permission_validation_reports_profit_loss'] = 'Reportes - Ganancias y pérdidas';
$lang['admin_cntrler_edit_permission_validation_reports_trialbalance'] = 'Reportes - Balance de prueba';
$lang['admin_cntrler_edit_permission_validation_reports_ledgerstatement'] = 'Reportes - Auxiliar de cuentas';
$lang['admin_cntrler_edit_permission_validation_reports_ledgerentries'] = 'Reportes - Asientos contables';
$lang['admin_cntrler_edit_permission_validation_reports_reconciliation'] = 'Informe de Conciliación';
$lang['admin_cntrler_edit_permission_validation_reports_thirdsaccounts'] = 'Reportes - Auxiliar de cuentas/terceros';

// message
$lang['admin_cntrler_update_settings_success']					= 'Actualizado correctamente';
$lang['admin_cntrler_delete_user_success']						= 'Usuario eliminado correctamente';
$lang['admin_cntrler_edit_user_update_image_success']			= 'Imagen guardada correctamente';
$lang['admin_cntrler_update_settings_success']					= 'Actualizado correctamente';
$lang['admin_cntrler_account_created_successfully']				= 'Compañía creada correctamente';
$lang['admin_cntrler_account_updated_successfully']				= 'Compañía actualizada correctamente';
$lang['admin_cntrler_permission_updated_successfully']			= 'Permisos actualizados correctamente';
$lang['admin_cntrler_create_group_success']						= 'Please update %s Group Permissions';
$lang['admin_cntrler_update_settings_success']					= 'Actualizado correctamente';

//error
$lang['admin_cntrler_update_settings_error']					= 'Se presentó un error, por favor intente de nuevo';
$lang['admin_cntrler_delete_user_error']						= 'Se presentó un error, por favor intente de nuevo';
$lang['admin_cntrler_uploadprofilepicture_error']				= 'No se pudo subir la imagen, por favor intente de nuevo';
$lang['admin_cntrler_edit_user_update_image_empty']				= 'Seleccione la imagen';
$lang['admin_cntrler_edit_user_update_image_error']				= 'No se guardó la imagen';
$lang['admin_cntrler_update_settings_error']					= 'Se presentó un error, por favor intente de nuevo';
$lang['admin_cntrler_edit_group_id_empty_error'] 				= "Error interno, por favor intente de nuevo";

// warning
$lang['admin_cntrler_check_db_warning']							= 'No se pudo conectar a la base de datos. Por favor revise la configuración de conexión.';
$lang['admin_cntrler_database_already_exist_warning'] 					= 'Ya existe una tabla con el nombre "%s" en la base de datos "%s". Por favor use una diferente o intente con un prefijo nuevo.';

// Admin End


// Accounts Start

// Views
// accounts/index
$lang['accounts_index_heading'] 						= 'Plan de cuentas';
$lang['accounts_index_account_code'] 					= 'Código de cuenta';
$lang['accounts_index_account_name'] 					= 'Nombre de cuenta';
$lang['accounts_index_type'] 							= 'Tipo';
$lang['accounts_index_op_balance'] 						= 'O/P Balance';
$lang['accounts_index_cl_balance'] 						= 'C/L Balance';
$lang['accounts_index_action'] 							= 'Acciones';
$lang['accounts_index_add_parent_group_btn'] 			= 'Crear nueva clase';
$lang['accounts_index_add_group_btn'] 					= 'Crear nuevo grupo';
$lang['accounts_index_add_ledger_btn'] 					= 'Crear nuevo auxiliar';
$lang['accounts_index_import_from_csv_btn'] 			= 'Importar desde CSV';
$lang['accounts_index_label_difference_bw_balance']		= 'Existe una diferencia en el balance de apertura de %s';
$lang['accounts_index_edit_btn'] 						= ' Editar';
$lang['accounts_index_delete_btn'] 						= ' Eliminar';
$lang['accounts_index_td_label_group'] 					= 'Grupo';
$lang['accounts_index_td_label_ledger'] 				= 'Auxiliar';
$lang['accounts_index_delete_ledger_alert'] 			= '¿Está seguro de eliminar el auxiliar?';
$lang['accounts_index_delete_group_alert'] 				= '¿Está seguro de eliminar el grupo?';
    //nuevos
$lang['accounts_index_action_add'] 						= 'Nuevo';

// accounts/log
$lang['accounts_log_heading']							= 'Registro de actividades';

// accounts/importer
$lang['accounts_importer_heading']  					= 'Importar plan de cuentas';
$lang['accounts_importer_sample_button']  				= 'Descargar CSV modelo';
$lang['accounts_importer_submit_button']  				= 'Enviar';
$lang['accounts_importer_cancel_button']  				= 'Cancelar';

// accounts/mapper
$lang['accounts_map_csv_heading']  						= 'Estructurando plan de cuentas';
$lang['accounts_map_csv_option_placeholder']  			= 'Seleccione...';
$lang['accounts_map_csv_submit_button']  				= 'Enviar';
$lang['accounts_map_csv_cancel_button']  				= 'Cancelar';
$lang['accounts_mapper_account_type']  					= 'i.e. Auxiliar o Grupo';
$lang['accounts_mapper_affects_gross']  				= 'Boolean: 0 or 1';
$lang['accounts_mapper_name']  							= 'Nombre de cuenta auxiliar o grupo';
$lang['accounts_mapper_code']  							= 'Separador: -';
$lang['accounts_mapper_opening_balance']  				= 'Balance de apertura de cuenta auxiliar';
$lang['accounts_mapper_debit_credit']  					= 'i.e. d : debit, c : credit';
$lang['accounts_mapper_bank_cash']  					= 'boolean: 0 or 1';
$lang['accounts_mapper_reconciliation']  				= 'boolean: 0 or 1';
$lang['accounts_mapper_notes']  						= 'Related Notes';


// Alerts
// messages
$lang['accounts_exporter_exported_successfully'] = 'Se añadieron %s grupos y %s cuentas auxiliares.';

// Accounts End


// Entries Start
// Views
// entries/add
$lang['entries_views_add_label_number'] 					= 'Número';
$lang['entries_views_add_label_date'] 						= 'Fecha';
$lang['entries_views_add_label_tag'] 						= 'Etiqueta';
$lang['entries_views_add_label_company'] 					= 'Tercero';
$lang['entries_views_add_items_narration_placeholder'] 		= 'Descripción';
$lang['entries_views_add_items_amount_placeholder'] 		= 'Monto';
$lang['entries_views_add_items_addentry_btn_tooltip'] 		= 'Crear asiento contable';


$lang['entries_views_add_tag_first_option'] 			= 'Ninguna';
$lang['entries_views_add_company_first_option'] 		= 'Seleccione...';
$lang['entries_views_add_items_th_toby'] 				= 'To/By';
$lang['entries_views_add_items_th_drcr'] 				= 'D/C';
$lang['entries_views_add_items_th_ledger'] 				= 'Cuenta auxiliar';
$lang['entries_views_add_items_th_dr_amount'] 			= 'Monto D';
$lang['entries_views_add_items_th_cr_amount'] 			= 'Monto C';
$lang['entries_views_add_items_th_narration'] 			= 'Descripción';
$lang['entries_views_add_items_th_cur_balance'] 		= 'Balance actual';
$lang['entries_views_add_items_th_base'] 				= 'Base';
$lang['entries_views_add_items_th_actions'] 			= 'Acciones';
$lang['entries_views_add_items_th_cost_center'] 	    = 'Centro de costo';
$lang['entries_views_add_items_td_total'] 				= 'Total';
$lang['entries_views_add_items_td_diff'] 				= 'Diferencia';
$lang['entries_views_add_label_note'] 					= 'Nota ';
$lang['entries_views_add_label_note_approved'] 			= 'Asiento contable aprobado';
$lang['entries_views_add_label_submit_btn'] 			= 'Enviar';
$lang['entries_views_add_label_cancel_btn'] 			= 'Cancelar';

// entries/addrow
$lang['entries_views_addrow_label_dc_toby_D'] 				= 'By';
$lang['entries_views_addrow_label_dc_toby_C'] 				= 'To';
$lang['entries_views_addrow_label_dc_drcr_D'] 				= 'D';
$lang['entries_views_addrow_label_dc_drcr_C'] 				= 'C';

// entries/edit
$lang['entries_views_edit_label_number'] 				= 'Número';
$lang['entries_views_edit_label_date'] 					= 'Fecha';
$lang['entries_views_edit_label_tag'] 					= 'Eriqueta';
$lang['entries_views_edit_tag_first_option'] 			= 'Ninguna';
$lang['entries_views_edit_items_th_toby'] 				= 'To/By';
$lang['entries_views_edit_items_th_drcr'] 				= 'D/C';
$lang['entries_views_edit_items_th_ledger'] 			= 'Cuenta auxiliar';
$lang['entries_views_edit_items_th_dr_amount'] 			= 'Monto D';
$lang['entries_views_edit_items_th_cr_amount'] 			= 'Monto C';
$lang['entries_views_edit_items_th_narration'] 			= 'Descripción';
$lang['entries_views_edit_items_th_cur_balance'] 		= 'Balance actual';
$lang['entries_views_edit_items_th_actions'] 			= 'Acciones';
$lang['entries_views_edit_items_td_total'] 				= 'Total';
$lang['entries_views_edit_items_td_diff'] 				= 'Diferencia';
$lang['entries_views_edit_label_note'] 					= 'Nota';
$lang['entries_views_edit_label_submit_btn'] 			= 'Enviar';
$lang['entries_views_edit_label_cancel_btn'] 			= 'Cancelar';

// entries/index
$lang['entries_views_index_title'] 					= 'Asientos contables';
$lang['entries_views_index_add_entry_btn'] 			= ' Nueva ';
$lang['entries_views_index_th_date'] 				= 'Fecha';
$lang['entries_views_index_th_number'] 				= 'Numero';
$lang['entries_views_index_th_ledger'] 				= 'Cuenta auxiliar';
$lang['entries_views_index_th_note'] 				= 'Nota';
$lang['entries_views_index_th_type'] 				= 'Tipo';
$lang['entries_views_index_th_tag'] 				= 'Etiqueta';
$lang['entries_views_index_th_third'] 				= 'Tercero';
$lang['entries_views_index_th_vat_no'] 				= 'NIT';
$lang['entries_views_index_th_debit_amount'] 		= 'Monto de débito';
$lang['entries_views_index_th_credit_amount'] 		= 'Monto de crédito';
$lang['entries_views_index_th_total_amount_OP'] 	= 'OP BALANCE';
$lang['entries_views_index_th_total_amount_D'] 		= 'Débito';
$lang['entries_views_index_th_total_amount_C'] 		= 'Crédito';
$lang['entries_views_index_th_total_balance'] 		= 'Saldo';
$lang['entries_views_index_th_total_entry_item_num'] 	= 'N° Item';
$lang['entries_views_index_th_state'] 		        = 'Estado';
$lang['entries_views_index_th_actions'] 			= 'Acciones';
$lang['entries_views_index_th_actions_view_btn'] 	= ' Ver';
$lang['entries_views_index_th_actions_duplicate_btn'] 	= ' Duplicar';
$lang['entries_views_index_th_actions_edit_btn'] 	= ' Editar';
$lang['entries_views_index_th_actions_delete_btn'] 	= ' Anular';

// entries/view
$lang['entries_views_views_label_number'] 			= 'Número';
$lang['entries_views_views_label_date'] 			= 'Fecha';
$lang['entries_views_views_entry_type'] 			= 'Tipo de nota';
$lang['entries_views_views_email_not_sent_msg'] 	= 'Error al enviar email.';
$lang['entries_views_views_title'] 					= 'Catálogo de cuentas';
$lang['entries_views_views_th_to_by'] 				= 'De/Para';
$lang['entries_views_views_th_dr_cr'] 				= 'D/C';
$lang['entries_views_views_th_ledger'] 				= 'Auxiliar';
$lang['entries_views_views_th_dr_amount'] 			= 'Monto Débito';
$lang['entries_views_views_th_cr_amount'] 			= 'Monto Crédito';
$lang['entries_views_views_th_narration'] 			= 'Descripción';
$lang['entries_views_views_th_base'] 				= 'Base';
$lang['entries_views_views_th_companies_id'] 		= 'Tercero';
$lang['entries_views_views_toby_D'] 				= 'De';
$lang['entries_views_views_toby_C'] 				= 'Para';
$lang['entries_views_views_drcr_D'] 				= 'D';
$lang['entries_views_views_drcr_C'] 				= 'C';
$lang['entries_views_views_td_total'] 				= 'Total';
$lang['entries_views_views_td_diff'] 				= 'Diferencia';
$lang['entries_views_views_td_tag'] 				= 'Etiqueta';
$lang['entries_views_views_td_actions_state'] 		= 'Estado : ';
$lang['entries_views_views_td_actions_approve_btn'] = 'Aprobar';
$lang['entries_views_views_td_actions_edit_btn'] 	= 'Editar';
$lang['entries_views_views_td_actions_delete_btn'] 	= 'Eliminar';
$lang['entries_views_views_td_actions_cancel_btn'] 	= 'Cancelar';

// Controller
// form validation add
$lang['entries_cntrler_add_form_validation_number_label'] 				= 'Número';
$lang['entries_cntrler_add_form_validation_date_label'] 				= 'Fecha';
$lang['entries_cntrler_add_form_validation_tag_label'] 					= 'Etiqueta';
$lang['entries_cntrler_add_form_validation_entryitem_dc_label'] 		= 'Monto D o C';
$lang['entries_cntrler_add_form_validation_entryitem_ledger_id_label'] 	= 'Cuenta auxiliar';
$lang['entries_cntrler_add_title'] 										= 'Crear asiento contable %s';
$lang['entries_cntrler_add_h3_title_1'] 								= 'Añadir asiento contable';
$lang['entries_cntrler_import_h3_title_1'] 								= 'Importar asientos contables';
$lang['entries_cntrler_add_h3_title_2'] 								= '';
$lang['dashboard_entries'] 												= 'Asientos contables';
$lang['dashboard_account_list'] 										= 'Plan de cuentas';
$lang['dashboard_reports'] 												= 'Informes';
$lang['dashboard_thirds'] 												= 'Terceros';

//Thirds op balance relations accounts

$lang['thirds_relations_unbalance']										= "Diferencia D/C";
$lang['thirds_relations_third_label']									= "Tercero";
$lang['thirds_relations_op_balance_label']								= "Balance de apertura";
$lang['thirds_relations_op_dc_balance_label']							= "Balance de apertura D/C";
$lang['thirds_relations_repeated_validation']							= "No se puede escoger el mismo tercero más de una vez para una misma cuenta auxiliar o viceversa.";
$lang['thirds_relations_cost_center']									= "Centro de costo";


// form validation edit
$lang['entries_cntrler_edit_form_validation_number'] 						= 'Número';
$lang['entries_cntrler_edit_form_validation_date'] 							= 'Fecha';
$lang['entries_cntrler_edit_form_validation_tag'] 							= 'Etiqueta';
$lang['entries_cntrler_edit_form_validation_entryitem_dc_label'] 			= 'Monto D o C';
$lang['entries_cntrler_edit_form_validation_entryitem_ledger_id_label'] 	= 'Cuenta auxiliar';
$lang['entries_cntrler_edit_title'] 										= 'Crear asiento contable %s';

// add_log
// entries/add
$lang['entries_cntrler_add_log'] 		= 'Se registró la entrada %s con número %s';


// entries/edit
$lang['entries_cntrler_edit_log'] 		= 'Se editó la entrada %s con número %s';


// entries/delete
$lang['entries_cntrler_delete_log'] 	= 'Se eliminó la entrada %s con número %s';

// entries/approve
$lang['entries_cntrler_approve_log'] 	= 'Se aprobó la entrada %s con número %s';

// Alerts
// form validation alerts
$lang['entries_cntrler_invalid_ledger_form_validation_alert'] 						= 'La cuenta auxiliar seleccionada es inválida';
$lang['entries_cntrler_restriction_bankcash_4_form_validation_alert'] 				= 'Sólo auxiliares de banco o efectivo están permitidas para este tipo de asiento contable.';
$lang['entries_cntrler_restriction_bankcash_5_form_validation_alert'] 				= 'Cuentas auxiliares de banco o efectivo no están permitidas para este tipo de asiento contable.';
$lang['entries_cntrler_dr_cr_total_not_equal_form_validation_alert'] 				= 'Montos débito y crédito no coinciden.';
$lang['entries_cntrler_restriction_bankcash_2_not_valid_dc_form_validation_alert'] 	= 'Al menos una cuenta auxiliar de banco o efectivo debe estar del lado débito para este tipo de asiento contable.';
$lang['entries_cntrler_restriction_bankcash_3_not_valid_dc_form_validation_alert'] 	= 'Al menos una cuenta auxiliar de banco o efectivo debe estar del lado crédito para este tipo de asiento contable.';
$lang['entries_cntrler_entry_number_required_form_validation_alert'] 				= 'El número de la asiento contable no puede estar vacío.';

// messages
$lang['entries_cntrler_add_entry_created_successfully'] 			= 'Asiento contable %s con número "%s" creada con éxito.';
$lang['entries_cntrler_edit_entry_updated_successfully'] 			= 'Asiento contable %s con número "%s" actualizada con éxito.';
$lang['entries_cntrler_delete_entry_deleted_successfully'] 			= 'Asiento contable %s con número "%s" anulada con éxito.';
$lang['entries_cntrler_delete_entry_approved_successfully'] 		= 'Asiento contable %s con número "%s" aprobada con éxito.';

// error
$lang['entries_cntrler_entrytype_not_found_error'] 					= 'No se encontró el tipo de asiento contable..';
$lang['entries_cntrler_add_entry_not_created_error'] 				= 'No se pudo crear la asiento contable, intente de nuevo.';
$lang['entries_cntrler_entrytype_not_specified_error'] 				= 'Tipo de asiento contable no especificado.';
$lang['entries_cntrler_entry_not_found_error'] 						= 'Asiento contable no encontrada.';
$lang['entries_cntrler_edit_account_locked_error'] 					= 'No se pueden realizar cambios, la cuenta está bloqueada.';
$lang['entries_cntrler_edit_entry_not_updated_error'] 				= 'No se pudo actualizar la asiento contable, intente de nuevo.';
$lang['entries_cntrler_edit_entry_no_from_conta'] 					= 'No se puede editar la asiento contable.';
$lang['entries_cntrler_delete_entry_no_from_conta'] 				= 'No se puede anular la asiento contable.';
$lang['entries_cntrler_approve_entry_no_from_conta'] 				= 'No se puede aprobar la asiento contable.';

// Entries End


//  Groups Start
// Views
// groups/add
$lang['groups_views_add_title'] 					= 'Añadir grupo';
$lang['groups_views_add_label_parent_group'] 		= 'Cuenta padre';
$lang['groups_views_add_label_group_code'] 			= 'Código de cuenta';
$lang['groups_views_add_label_group_name'] 			= 'Nombre de cuenta';
$lang['groups_views_add_label_affects'] 			= 'Afecta : ';
$lang['groups_views_add_label_gross_profit_loss']	= 'Ganancias y pérdidas brutas ';
$lang['groups_views_add_label_net_profit_loss'] 	= 'Ganancias y pérdidas netas ';
$lang['groups_views_add_note'] 						= 'Nota: Las afectaciones a las Ganancias o pérdidas netas o brutas se reflejan en el estado final de pérdidas y ganancias.';
$lang['entries_views_add_label_submit_btn'] 		= 'Enviar';
$lang['entries_views_add_label_cancel_btn'] 		= 'Cancelar';

// groups/addParent

$lang['groups_parent_views_add_title'] 					= 'Añadir clase';

// groups/edit
$lang['groups_views_edit_title'] 					= 'Editar Grupo';
$lang['groups_views_edit_label_parent_group'] 		= 'Cuenta padre';
$lang['groups_views_edit_label_group_code'] 		= 'Código de cuenta';
$lang['groups_views_edit_label_group_name'] 		= 'Nombre de cuenta';
$lang['groups_views_edit_label_affects'] 			= 'Afecta a : ';
$lang['groups_views_edit_label_gross_profit_loss']	= 'Ganancias y pérdidas brutas ';
$lang['groups_views_edit_label_net_profit_loss'] 	= 'Ganancias y pérdidas netas ';
$lang['groups_views_edit_note'] 					= 'Nota: Las afectaciones a las Ganancias o pérdidas netas o brutas se reflejan en el estado final de pérdidas y ganancias.';
$lang['entries_views_edit_label_submit_btn'] 		= 'Enviar';
$lang['entries_views_edit_label_cancel_btn'] 		= 'Cancelar';


// Controler
// groups/add
$lang['groups_cntrler_add_form_validation_label_parent_group'] 		= 'Grupo clase';
$lang['groups_cntrler_add_form_validation_label_name'] 				= 'Nombre';
$lang['groups_cntrler_add_form_validation_label_code']				= 'Código';
$lang['groups_cntrler_add_form_validation_label_affects_gross'] 	= 'Afecta Bruto';
$lang['groups_cntrler_add_label_add_log'] 							= 'Grupo creado : ';


// Alerts
// success
$lang['groups_cntrler_add_group_created_successfully'] 		= 'Grupo "%s" creado.';

// groups/edit
$lang['groups_cntrler_edit_form_validation_label_parent_group'] 		= 'Grupo clase';
$lang['groups_cntrler_edit_form_validation_label_name'] 				= 'Nombre';
$lang['groups_cntrler_edit_form_validation_label_code']					= 'Código';
$lang['groups_cntrler_edit_form_validation_label_affects_gross'] 		= 'Afecta Bruto';
$lang['groups_cntrler_edit_label_add_log'] 								= 'Grupo editado : ';

// Alerts
// success
$lang['groups_cntrler_edit_group_updated_successfully'] 		= 'Se actualizó el grupo "%s".';

// error
$lang['groups_cntrler_edit_group_not_specified_error'] 					= 'No se especificó el grupo.';
$lang['groups_cntrler_edit_group_not_found_error'] 						= 'No se encontró el grupo.';
$lang['groups_cntrler_edit_basic_account_permission_denied_error'] 		= 'No se puede editar grupos de cuentas básicos.';
$lang['groups_cntrler_edit_account_locked_error'] 						= 'No se peuden realizar cambios, la cuenta está bloqueada.';
$lang['groups_cntrler_edit_account_parent_group_same_error'] 			= 'El grupo padre y el grupo de cuentas no pueden ser iguales.';
$lang['groups_cntrler_add_account_parent_error_code'] 					= 'Ya no es posible crear un nuevo grupo clase.';

// groups/delete
$lang['groups_cntrler_delete_label_add_log'] 								= 'Grupo eliminado : ';

// Alerts
// success
$lang['groups_cntrler_delete_group_deleted_successfully'] 		= 'Grupo "%s" eliminado.';

// error
$lang['groups_cntrler_delete_group_not_specified_error'] 					= 'No se especificó una cuenta para borrar.';
$lang['groups_cntrler_delete_group_not_found_error'] 						= 'No se encontró la cuenta para borrar.';
$lang['groups_cntrler_delete_basic_account_permission_denied_error'] 		= 'No se puede eliminar una cuenta clase.';
$lang['groups_cntrler_delete_child_group_exists_error'] 					= 'La cuenta seleccionada no se puede eliminar porque tiene una o más cuentas secundarias relacionadas.';
$lang['groups_cntrler_delete_child_ledger_exists_error'] 					= 'La cuenta seleccionada no se puede eliminar porque tiene una o más cuentas auxiliares relacionadas.';

// Groups End


//  Ledgers Start
// Views
// ledgers/add
$lang['ledgers_views_add_title'] 						= 'Crear cuenta auxiliar';
$lang['ledgers_views_add_label_parent_group'] 			= 'Grupo padre';
$lang['ledgers_views_add_label_ledger_code'] 			= 'Código de cuenta auxiliar';
$lang['ledgers_views_add_label_ledger_name'] 			= 'Nombre de cuenta auxiliar';
$lang['ledgers_views_add_label_op_blnc'] 				= 'Balance de apertura';
$lang['ledgers_views_add_op_blnc_tooltip']				= "Nota: Los Activos & Gastos siempre tienen balance Débito (D) y los Pasivos & Ingresos siempre tienen balance Crédito (C).";
$lang['ledgers_views_add_label_bank_cash_account'] 		= 'Cuenta de bancaria o de efectivo';
$lang['ledgers_views_add_bank_cash_account_tooltip'] 	= "Nota: Seleccione si la cuenta auxiliar es de bancaria o de efectivo.";
$lang['entries_views_add_label_reconciliation'] 		= 'Conciliación';
$lang['entries_views_add_label_cost_center'] 			= 'Usa centros de costo';
$lang['ledgers_views_add_reconciliation_tooltip'] 		= 'Nota : Si se marcó para conciliación, la cuenta puede ser conciliada desde Informe de Conciliación.';
$lang['ledgers_views_add_label_notes'] 					= 'Notas';
$lang['ledgers_views_add_label_submit_btn'] 			= 'Enviar';
$lang['ledgers_views_add_label_cancel_btn'] 			= 'Cancelar';

// ledgers/edit
$lang['ledgers_views_edit_title'] 						= 'Editar cuenta auxiliar';
$lang['ledgers_views_edit_label_parent_group'] 			= 'Cuenta clase';
$lang['ledgers_views_edit_label_ledger_code'] 			= 'Código de cuenta auxiliar';
$lang['ledgers_views_edit_label_ledger_name'] 			= 'Nombre de cuenta auxiliar';
$lang['ledgers_views_edit_label_op_blnc'] 				= 'Balance de apertura';
$lang['ledgers_views_edit_op_blnc_tooltip']				= "Nota: Los activos y gastos siempre tienen balance de débito (D) y los pasivos e ingresos siempre tienen balance de crédito (C).";
$lang['ledgers_views_edit_label_bank_cash_account'] 	= 'Cuenta bancaria o de efectivo';
$lang['ledgers_views_edit_bank_cash_account_tooltip'] 	= "Nota: Seleccione si la cuenta es bancaria o de efectivo.";
$lang['entries_views_edit_label_reconciliation'] 		= 'Conciliación';
$lang['ledgers_views_edit_reconciliation_tooltip'] 		= 'Nota : Si es seleccionado, la cuenta se puede conciliar desde Reportes > Reconciliación.';
$lang['ledgers_views_edit_cost_center_tooltip'] 		= 'Indique si esta cuenta auxiliar será usada para los informes de centros de costo.';
$lang['ledgers_views_edit_label_notes'] 				= 'Notas';
$lang['ledgers_views_add_label_submit_btn'] 			= 'Enviar';
$lang['ledgers_views_edit_label_cancel_btn'] 			= 'Cancelar';


// Controler
// ledgers/add
$lang['ledgers_cntrler_add_form_validation_label_name'] 			= 'Nombre';
$lang['ledgers_cntrler_add_form_validation_label_group_id'] 		= 'Grupo';
$lang['ledgers_cntrler_add_form_validation_label_op_balance_dc']	= 'Balance de apertura, D/C';
$lang['ledgers_cntrler_add_form_validation_label_op_balance'] 		= 'Balance de apertura, monto';
$lang['ledgers_cntrler_add_form_validation_label_code'] 			= 'Código';
$lang['ledgers_cntrler_add_label_add_log'] 							= 'Cuenta auxiliar creada : ';

// Alerts
// success
$lang['ledgers_cntrler_add_ledger_created_successfully'] 			= 'Auxiliar "%s" creado con éxito.';
$lang['ledgers_cntrler_add_ledger_entries_exists'] 					= 'No se puede indicar balance de apertura por que ya hay Asientos contables registradas.';
$lang['ledgers_cntrler_edit_ledger_entries_exists'] 				= 'No se puede modificar el balance de apertura por que ya hay Asientos contables registradas.';

// ledgers/edit
$lang['ledgers_cntrler_edit_form_validation_label_name'] 			= 'Nombre';
$lang['ledgers_cntrler_edit_form_validation_label_group_id'] 		= 'Grupo';
$lang['ledgers_cntrler_edit_form_validation_label_op_balance_dc']	= 'Balance de apertura, D/C';
$lang['ledgers_cntrler_edit_form_validation_label_op_balance'] 		= 'Balance de apertur, monto';
$lang['ledgers_cntrler_edit_form_validation_label_code'] 			= 'Código';
$lang['ledgers_cntrler_edit_label_add_log'] 						= 'Cuenta auxiliar actualizada : ';

// Alerts
// success
$lang['ledgers_cntrler_edit_ledger_updated_successfully'] 				= 'Cuenta auxiliar "%s" actualizada con éxito.';

// error
$lang['ledgers_cntrler_edit_ledger_not_specified_error'] 				= 'No se especificó ninguna cuenta auxiliar.';
$lang['ledgers_cntrler_edit_ledger_not_found_error'] 					= 'No se encontró la cuenta auxiliar.';
$lang['ledgers_cntrler_edit_account_locked_error'] 						= 'No se pueden realizar cambios, la cuenta está bloqueada.';

// ledgers/delete
$lang['ledgers_cntrler_delete_label_add_log'] 							= 'Cuenta auxiliar eliminada : ';

// Alerts
// success
$lang['ledgers_cntrler_delete_ledger_deleted_successfully'] 				= 'Cuenta auxiliar "%s" eliminada.';

// error
$lang['ledgers_cntrler_delete_ledger_not_specified_error'] 				= 'No se especificó ningún auxiliar.';
$lang['ledgers_cntrler_delete_ledger_not_found_error'] 					= 'No se encontró el auxiliar.';
$lang['ledgers_cntrler_delete_entries_exist_error'] 	= 'La cuenta auxiliar no se puede borrar por que tiene Asientos contables activas.';
$lang['ledgers_cntrler_delete_op_balance_error'] 	= 'La cuenta auxiliar no se puede eliminar por que tiene un balance de apertura mayor a 0.';
// Ledgers End

//Accounts parameter Start
	//index
	$lang['accounts_parameter_index_title'] 				= 'Parametrización de cuentas';
	$lang['accounts_parameter_index_add'] 					= 'Nuevo';
	$lang['accounts_parameter_index_th_type'] 				= 'Tipo';
	$lang['accounts_parameter_index_th_type_mov'] 			= 'Tipo de movimiento';
	$lang['accounts_parameter_index_th_tax'] 				= 'Impuesto';
	$lang['accounts_parameter_index_th_category'] 			= 'Categoría';
	$lang['accounts_parameter_index_th_ledger'] 			= 'Cuenta auxiliar';
	$lang['accounts_parameter_index_modal_delete_title'] 	= '¿Está seguro de eliminar el parámetro seleccionado?';
	$lang['accounts_parameter_index_modal_delete_text'] 	= 'Este proceso es irreversible, por favor asegúrese de que el parámetro escogido es el correcto.';
	//add
	$lang['accounts_parameter_add_title'] 					= 'Añadir nuevo parámetro';
	$lang['accounts_parameter_add_submit'] 					= 'Enviar';
	$lang['accounts_parameter_add_first_option'] 			= 'Seleccione...';
	$lang['accounts_parameter_add_label_ledger'] 			= 'Cuenta auxiliar ';
	$lang['accounts_parameter_add_label_tax'] 				= 'Impuesto ';
	$lang['accounts_parameter_add_label_category'] 			= 'Categoría ';
	$lang['accounts_parameter_add_label_entry_type'] 		= 'Tipo de entrada ';
	$lang['accounts_parameter_add_label_type'] 				= 'Tipo de parámetro ';
	$lang['accounts_parameter_add_success'] 				= 'Se creó con éxito el parámetro.';
	$lang['accounts_parameter_add_unsuccess'] 				= 'No se creó el parámetro, ocurrió un error.';
	$lang['accounts_parameter_add_parameter_exists'] 		= 'No se puede crear el parámetro por qué ya existe otro con las mismas condiciones.';
	//edit
	$lang['accounts_parameter_edit_title'] 					= 'Editar parámetro de cuenta';
	$lang['accounts_parameter_edit_not_set'] 				= 'No se especificó un parámetro.';
	$lang['accounts_parameter_edit_not_found'] 				= 'No se encontró el parámetro.';
	$lang['accounts_parameter_edit_success'] 				= 'Se actualizó con éxito el parámetro.';
	$lang['accounts_parameter_edit_unsuccess'] 				= 'No se actualizó el parámetro, ocurrió un error.';
	$lang['accounts_parameter_edit_parameter_exists'] 		= 'No se puede actualizar el parámetro por qué ya existe otro con las mismas condiciones.';
	//delete
	$lang['accounts_parameter_delete_success'] 				= 'Se eliminó con éxito el parámetro.';
	$lang['accounts_parameter_delete_unsuccess'] 			= 'No se eliminó el parámetro, ocurrió un error.';
//Accounts parameter End
//Payment methods Start
	//index
	$lang['payment_methods_index_title'] 				= 'Medios de pago';
	$lang['payment_methods_index_add'] 					= 'Nuevo';
	$lang['payment_methods_index_th_type'] 				= 'Tipo';
	$lang['payment_methods_index_th_entity'] 			= 'Entidad';
	$lang['payment_methods_index_th_ledger_receipt'] 	= 'Cuenta auxiliar de Venta';
	$lang['payment_methods_index_th_ledger_payment'] 	= 'Cuenta auxiliar de Compra';
	$lang['payment_methods_index_th_actions'] 			= 'Acciones';
	$lang['payment_methods_index_modal_delete_title'] 	= '¿Está seguro de eliminar el método seleccionado?';
	$lang['payment_methods_index_modal_delete_text'] 	= 'Este proceso es irreversible, por favor asegúrese de que el método de pago escogido es el correcto.';
	//add
	$lang['payment_methods_add_title'] 					= 'Añadir nuevo método de pago';
	$lang['payment_methods_add_label_type'] 			= 'Tipo';
	$lang['payment_methods_add_label_entity'] 			= 'Entidad';
	$lang['payment_methods_add_label_ledger_receipt'] 	= 'Auxiliar de Venta';
	$lang['payment_methods_add_label_ledger_payment'] 	= 'Auxiliar de Compra';
	$lang['payment_methods_add_submit'] 				= 'Enviar';
	$lang['payment_methods_add_first_option'] 			= 'Seleccione...';
	/*labels eliminar este comentario*/
	$lang['payment_methods_add_success'] 				= 'Se creó con éxito el método de pago.';
	$lang['payment_methods_add_unsuccess'] 				= 'No se creó el método de pago, ocurrió un error.';
	$lang['payment_methods_add_parameter_exists'] 		= 'No se puede crear el método de pago por qué ya existe otro con las mismas condiciones.';
	//edit
	$lang['payment_methods_edit_title'] 				= 'Editar método de pago';
	$lang['payment_methods_edit_not_set'] 				= 'No se especificó un método de pago.';
	$lang['payment_methods_edit_not_found'] 			= 'No se encontró el método de pago.';
	$lang['payment_methods_edit_success'] 				= 'Se actualizó con éxito el método de pago.';
	$lang['payment_methods_edit_unsuccess'] 			= 'No se actualizó el método de pago, ocurrió un error.';
	$lang['payment_methods_edit_parameter_exists'] 		= 'No se puede actualizar el método de pago por qué ya existe otro con las mismas condiciones.';
	//delete
	$lang['payment_methods_delete_success'] 			= 'Se eliminó con éxito el método de pago.';
	$lang['payment_methods_delete_unsuccess'] 			= 'No se eliminó el método de pago, ocurrió un error.';
//Payment methods End

// User Start
// Views
// user/activate
$lang['user_views_activate_label_title'] 					= 'Seleccione año/compañía para activar';
$lang['user_views_activate_label_sub_title'] 				= '<strong>Compañía activa: <em style="font-size: 18px; padding-left: 30px"> %s </em></strong>';
$lang['user_views_activate_label_subtitle_NONE'] 			= 'No hay compañía activa.';
$lang['user_views_activate_label_thead_label'] 				= 'Label';
$lang['user_views_activate_label_thead_name'] 				= 'Nombre';
$lang['user_views_activate_label_thead_fiscal_year'] 		= 'Año fiscal';
$lang['user_views_activate_label_thead_status'] 			= 'Estado';
$lang['user_views_activate_label_active_locked'] 			= 'Activa y bloqueada';
$lang['user_views_activate_label_active_locked_tooltip'] 	= 'Clic para desactivar';
$lang['user_views_activate_label_active'] 					= 'Activar';
$lang['user_views_activate_label_active_tooltip'] 			= 'Clic para desactivar';
$lang['user_views_activate_label_locked'] 					= 'Bloqueada';
$lang['user_views_activate_label_locked_tooltip'] 			= 'La cuenta está bloqueada, clic para desbloquear.';
$lang['user_views_activate_label_inactive'] 				= 'Inactiva';
$lang['user_views_activate_label_inactive_tooltip'] 		= 'Clic para activar.';
$lang['user_views_activate_label_td_fy_year_to'] 			= 'to';
$lang['user_views_activate_label_tfoot_label'] 				= 'Label';
$lang['user_views_activate_label_tfoot_name'] 				= 'Nombre';
$lang['user_views_activate_label_tfoot_fiscal_year'] 		= 'Año fiscal';
$lang['user_views_activate_label_tfoot_status'] 			= 'Estado';
$lang['user_views_activate_note_box_footer'] 				= '<strong>Nota:</strong> <em> Si deseas usar múltiples cuentas al mismo tiempo, por favor use diferentes navegadores.</em>';

// user/dashboard
$lang['user_views_dashboard_error_alert_php_bc_math_lib_missing'] 	= 'PHP BC Math library is missing. Please check the "Wiki" section in Help on how to fix it.';


// Controller
// user/activate
// Alerts
// error
$lang['user_cntrler_activate_account_not_found_error'] = 'No se encontró la cuenta, por favor intente de nuevo.';

// warning
$lang['user_cntrler_activate_no_accounts_found_warning'] = 'Por favor, cree una cuenta para continuar.';


// user/activator
// Alerts
// success
$lang['user_cntrler_activator_activate_success'] = 'Se activó con éxito la compañía.';

// warning
$lang['user_cntrler_activator_db_con_warning'] = 'Cound not connect to database. Please, check your database settings.';

// user/deactivate
// Alerts
// success
$lang['user_cntrler_dectivate_successful'] = 'Se desactivó con éxito';

// error
$lang['user_cntrler_dectivate_error'] = 'Cannot deactivate Account/Year, please try again.';

// User End


// Search Start
// Views
// search/index
$lang['search_views_title']							= 'Búsqueda avanzada';
$lang['search_views_label_from'] 					= 'Desde';
$lang['search_views_label_to'] 						= 'a';
$lang['search_views_legend_ledgers'] 				= 'Cuentas auxiliares';
$lang['search_views_legend_entrytype']				= 'Tipos de documento contable';
$lang['search_views_legend_entry_number'] 			= 'Número de asiento contable';
$lang['search_views_entry_number_equal'] 			= 'Igual a';
$lang['search_views_entry_number_less_equal'] 		= 'Menor o igual a';
$lang['search_views_entry_number_greater_equal'] 	= 'Mayor o igual a';
$lang['search_views_entry_number_between'] 			= 'Entre';
$lang['search_views_legend_amount'] 				= 'Monto';
$lang['search_views_label_dr_or_cr'] 				= 'D o C';
$lang['search_views_dr_or_cr_option_any'] 			= '(Alguna)';
$lang['search_views_dr_or_cr_option_dr'] 			= 'D';
$lang['search_views_dr_or_cr_option_cr'] 			= 'C';
$lang['search_views_label_condition'] 				= 'Condición';
$lang['search_views_condition_equal'] 				= 'Igual a';
$lang['search_views_condition_less_equal'] 			= 'Menor o igual a';
$lang['search_views_condition_greater_equal'] 		= 'Mayor o igual a';
$lang['search_views_condition_between'] 			= 'Entre';
$lang['search_views_label_amount'] 					= 'Monto';
$lang['search_views_label_amount_in_between'] 		= 'Monto <small>Entre</small>';
$lang['search_views_legend_date'] 					= 'Fecha';
$lang['search_views_legend_tags'] 					= 'Etiquetas';
$lang['search_views_legend_narration_contains'] 	= 'La descripción contiene';
$lang['search_views_search_btn'] 					= 'Buscar';
$lang['search_views_th_date'] 						= 'Fecha';
$lang['search_views_th_number'] 					= 'Número';
$lang['search_views_th_ledger'] 					= 'Cuenta auxiliar';
$lang['search_views_th_type'] 						= 'Tipo';
$lang['search_views_th_tag']						= 'Etiqueta';
$lang['search_views_th_dr_amount'] 					= 'Monto débito';
$lang['search_views_th_cr_amount'] 					= 'Monto crédito';
$lang['search_views_th_actions'] 					= 'Acciones';
$lang['search_views_amounts_td_error'] 				= 'ERROR';

// Search End


// Account Settings Start
// Views
// settings/main
$lang['settings_views_main_label_account_settings'] 	= 'Configuración de cuenta';
$lang['settings_views_main_label_company_name'] 		= 'Nombre de compañía';
$lang['settings_views_main_label_address'] 				= 'Dirección';
$lang['settings_views_main_label_email'] 				= 'Correo electrónico';
$lang['settings_views_main_label_currency_symbol'] 		= 'Símbolo de moneda';
$lang['settings_views_main_label_currency_format'] 		= 'Formato de moneda';
$lang['settings_views_main_label_date_format'] 			= 'Formato de fecha';
$lang['settings_views_main_date_format_1'] 				= 'Día-Mes-Año';
$lang['settings_views_main_date_format_2'] 				= 'Mes-Día-Año';
$lang['settings_views_main_date_format_3'] 				= 'Año-Mes-Día';
$lang['settings_views_main_label_financial_year_start'] = 'Inicio de año financiero';
$lang['settings_views_main_label_financial_year_end'] 	= 'Fin de año financiero';
$lang['settings_views_main_label_nit'] 					= 'Nit Compañía';
$lang['settings_views_main_label_phone'] 				= 'Teléfono Compañía';
$lang['settings_views_main_label_update_logo'] 			= 'Subir logo';
$lang['settings_views_main_label_submit'] 				= 'Enviar';
$lang['settings_views_main_modal_title'] 				= 'Subir Logo';
$lang['settings_views_main_modal_label_select_image'] 	= 'Seleccione imagen';
$lang['settings_views_main_modal_btn_close'] 			= 'Cerrar';
$lang['settings_views_main_modal_btn_upload'] 			= 'Subir';


// settings/cf
$lang['settings_views_cf_title'] 						= 'Cierre de año fiscal';
$lang['settings_views_cf_subtitle'] 					= 'Detalles de año fiscal actual : ';
$lang['settings_views_cf_label_name'] 					= 'Nombre de compañía';
$lang['settings_views_cf_label_email'] 					= 'Correo electrónico';
$lang['settings_views_cf_label_currency']				= 'Moneda';
$lang['settings_views_cf_label_fiscal_year'] 			= 'Año financiero';
$lang['settings_views_cf_label_status'] 				= 'Estado';
$lang['settings_views_cf_label_unlocked']				= 'Desbloqueada';
$lang['settings_views_cf_label_locked'] 				= 'Bloqueada';
$lang['settings_views_cf_label_label'] 					= 'Label';
$lang['settings_views_cf_label_database'] 				= 'Base de datos';
$lang['settings_views_cf_label_tooltip'] 				= 'Nota:Es recomendado usar un label descriptivo, ejemplo : "sample20142015", éste debería incluir el año y el nombre de la compañía.';
$lang['settings_views_cf_label_company_name'] 			= 'Nombre de compañía';
$lang['settings_views_cf_label_fy_start'] 				= 'Inicio de año financiero';
$lang['settings_views_cf_label_fy_end'] 				= 'Finalización de año financiero';
$lang['settings_views_cf_label_date_format'] 			= 'Formato de fecha';
$lang['settings_views_cf_date_format_option_1'] 		= 'Día-Mes-Año';
$lang['settings_views_cf_date_format_option_2'] 		= 'Mes-Día-Año';
$lang['settings_views_cf_date_format_option_3'] 		= 'Año-Mes-Día';
$lang['settings_views_cf_label_db_settings'] 			= 'Configuración DB';
$lang['settings_views_cf_label_db_type'] 				= 'Tipo  DB';
$lang['settings_views_cf_db_type_option_mysql'] 		= 'MySQL';
$lang['settings_views_cf_label_db_name'] 				= 'Nombre DB';
$lang['settings_views_cf_label_db_schema'] 				= 'Esquema DB';
$lang['settings_views_cf_label_db_host'] 				= 'Host DB';
$lang['settings_views_cf_label_db_port'] 				= 'Puerto DB';
$lang['settings_views_cf_label_db_username'] 			= 'Usuario DB';
$lang['settings_views_cf_label_db_password'] 			= 'Contraseña DB';
$lang['settings_views_cf_label_db_prefix'] 				= 'Prefijo DB';
$lang['settings_views_cf_label_db_suffix'] 				= 'Sufijo DB';
$lang['settings_views_cf_prefix_tooltip'] 				= 'Nota: El uso de un prefijo para la base de datos es opcional. Todas las tablas se crearán con dicho prefijo, úselo sólo si sólo dispone de una base de datos y quiere usar múltiples cuentas.';
$lang['settings_views_cf_label_use_persistent_conn'] 	= 'Usar conexión persistente.';
$lang['settings_views_cf_use_persistent_conn_yes'] 		= ' Si';
$lang['settings_views_cf_btn_submit'] 					= 'Enviar';

// settings/email
$lang['settings_views_email_title'] 									= 'Email Settings';
$lang['settings_views_email_checkbox_use_default_email_settings'] 		= 'Use default email settings';
$lang['settings_views_email_use_default_email_settings_tooltip'] 		= 'Note : If selected the default email settings in the Administer > General Settings will be used.';
$lang['settings_views_email_label_email_protocol'] 						= 'Email protocol';
$lang['settings_views_email_email_protocol_option_smtp'] 				= 'SMTP';
$lang['settings_views_email_email_protocol_option_mail_function'] 		= 'MAIL Funtion';
$lang['settings_views_email_label_smtp_host'] 							= 'SMTP HOST';
$lang['settings_views_email_label_smtp_port'] 							= 'SMTP PORT';
$lang['settings_views_email_label_smtp_email'] 							= 'Email';
$lang['settings_views_email_label_smtp_password'] 						= 'SMTP Password';
$lang['settings_views_email_label_smtp_username'] 						= 'SMTP Username';
$lang['settings_views_email_btn_submit'] 								= 'Enviar';
$lang['settings_views_email_label_smtp_host_placeholder'] 				= 'Enter Host';
$lang['settings_views_email_label_smtp_port_placeholder'] 				= 'Enter Port';
$lang['settings_views_email_label_smtp_email_placeholder'] 				= 'Enter Email';
$lang['settings_views_email_label_smtp_password_placeholder'] 			= 'Enter Password';
$lang['settings_views_email_label_smtp_username_placeholder'] 			= 'Enter Username';
$lang['settings_views_email_label_use_tls'] 							= ' Use TLS';

// settings/entrytypes
$lang['settings_views_entrytypes_form_validation_msg'] 		= ' must be filled out';
$lang['settings_views_entrytypes_modal_label_label'] 				= 'Abreviación';
$lang['settings_views_entrytypes_modal_label_name'] 				= 'Nombre';
$lang['settings_views_entrytypes_modal_label_description'] 		= 'Descripción';
$lang['settings_views_entrytypes_modal_label_numbering'] 			= 'Numeración';
$lang['settings_views_entrytypes_modal_numbering_option_1'] 		= 'Automática';
$lang['settings_views_entrytypes_modal_numbering_option_2'] 		= 'Manual (obligatoria)';
$lang['settings_views_entrytypes_modal_numbering_option_3'] 		= 'Manual (opcional)';
$lang['settings_views_entrytypes_modal_label_perfix'] 			= 'Prefijo';
$lang['settings_views_entrytypes_modal_label_suffix'] 			= 'Sufijo';
$lang['settings_views_entrytypes_modal_label_zero_padding'] 		= 'Ceros a la izquierda';
$lang['settings_views_entrytypes_modal_label_origin'] 				= 'Origen del tipo de nota';
$lang['settings_views_entrytypes_modal_label_origin_checkbox'] 		= 'Módulo contabilidad';
$lang['settings_views_entrytypes_modal_label_restrictions'] 		= 'Restricciones';
$lang['settings_views_entrytypes_modal_restrictions_option_1'] 	= 'Sin restricciones';
$lang['settings_views_entrytypes_modal_restrictions_option_2'] 	= 'Al menos una cuenta de banco o efectivo debe estar en posición Débito.';
$lang['settings_views_entrytypes_modal_restrictions_option_3'] 	= 'Al menos una cuenta de banco o efectivo debe estar en posición Crédito.';
$lang['settings_views_entrytypes_modal_restrictions_option_4'] 	= 'Sólo cuentas Bancarias o de efectivo deben estar en ambas posiciones, Débito o Crédito.';
$lang['settings_views_entrytypes_modal_restrictions_option_5'] 	= 'Sólo cuentas que NO son Bancarias o de efectivo deben estar en ambas posiciones, Débito o Crédito.';
$lang['settings_views_entrytypes_title'] 					= 'Entry Types';
$lang['settings_views_entrytypes_btn_add'] 					= 'Nuevo';
$lang['settings_views_entrytypes_thead_label'] 				= 'Label';
$lang['settings_views_entrytypes_thead_name'] 				= 'Name';
$lang['settings_views_entrytypes_thead_description'] 		= 'Description';
$lang['settings_views_entrytypes_thead_prefix'] 			= 'Prefix';
$lang['settings_views_entrytypes_thead_suffix'] 			= 'Suffix';
$lang['settings_views_entrytypes_thead_zero_padding'] 		= 'Zero Padding';
$lang['settings_views_entrytypes_thead_actions'] 			= 'Actions';
$lang['settings_views_entrytypes_tfoot_label'] 				= 'Label';
$lang['settings_views_entrytypes_tfoot_name'] 				= 'Name';
$lang['settings_views_entrytypes_tfoot_description'] 		= 'Description';
$lang['settings_views_entrytypes_tfoot_prefix'] 			= 'Prefix';
$lang['settings_views_entrytypes_tfoot_suffix'] 			= 'Suffix';
$lang['settings_views_entrytypes_tfoot_zero_padding'] 		= 'Zero Padding';
$lang['settings_views_entrytypes_tfoot_actions'] 			= 'Actions';
$lang['settings_views_entrytypes_modal_footer_btn_cancel'] 	= 'Cancelar';
$lang['settings_views_entrytypes_modal_footer_btn_submit'] 	= ' Enviar';
$lang['settings_views_entrytypes_btn_save'] 					= 'Save Entry Type';
$lang['settings_views_entrytypes_added'] 					= 'Tipo de nota creado con éxito';
$lang['settings_views_entrytypes_not_added'] 				= 'Entry don\'t added';
$lang['settings_views_entrytypes_update'] 					= 'Entry Type edited successfully';
$lang['settings_views_entrytypes_deleted'] 					= 'Entry Type deleted successfully';
$lang['settings_views_entrytypes_deleted_error'] 			= 'error deleting Entry Type';
$lang['settings_views_entrytypes_edit'] 					= 'Edit Entry Type';


// settings/printer

$lang['settings_views_printer_title'] = 'Configuración de Impresora';
$lang['settings_views_printer_legend_paper_size'] = 'Tamaño de papel';
$lang['settings_views_printer_label_height'] = 'Alto';
$lang['settings_views_printer_label_inches'] = 'Pulgadas';
$lang['settings_views_printer_label_width'] = 'Ancho';
$lang['settings_views_printer_legend_output'] = 'Salida';
$lang['settings_views_printer_label_orientation'] = 'Orientación';
$lang['settings_views_printer_option_portrait'] = 'Vertical';
$lang['settings_views_printer_option_landscape'] = 'Horizontal';
$lang['settings_views_printer_legend_output_format'] = 'Formato de Salida';
$lang['settings_views_printer_option_html'] = 'HTML';
$lang['settings_views_printer_option_text'] = 'Texto';
$lang['settings_views_printer_legend_paper_margin'] = 'Margen';
$lang['settings_views_printer_label_top'] = 'Arriba';
$lang['settings_views_printer_label_bottom'] = 'Abajo';
$lang['settings_views_printer_label_left'] = 'Izquierda';
$lang['settings_views_printer_label_right'] = 'Derecha';
$lang['settings_views_printer_btn_submit'] = 'Enviar';

// settings/tags
$lang['settings_views_tags_title'] = 'Tags';
$lang['settings_views_tags_title'] = 'Tags';
$lang['settings_views_tags_title'] = 'Tags';
$lang['settings_views_tags_title'] = 'Tags';
$lang['settings_views_tags_title'] = 'Tags';
$lang['settings_views_tags_title'] = 'Tags';
$lang['settings_views_tags_title'] = 'Tags';
$lang['settings_views_tags_title'] = 'Tags';
$lang['settings_views_tags_title'] = 'Tags';
$lang['settings_views_tags_title'] = 'Tags';
$lang['settings_views_tags_title'] = 'Tags';
$lang['settings_views_tags_title'] = 'Tags';
$lang['settings_views_tags_title'] = 'Tags';
$lang['settings_views_tags_title'] = 'Tags';
$lang['settings_views_tags_title'] = 'Tags';
$lang['settings_views_tags_title'] = 'Tags';
$lang['settings_views_tags_title'] = 'Tags';


// settings/lock

// Controller
// account_settings/main
$lang['account_settings_cntrler_main_form_validation_label_name'] 			= 'Name';
$lang['account_settings_cntrler_main_form_validation_label_address'] 		= 'Address';
$lang['account_settings_cntrler_main_form_validation_label_email'] 			= 'Email';
$lang['account_settings_cntrler_main_form_validation_label_cur_symbol'] 	= 'Currency Symbol';
$lang['account_settings_cntrler_main_form_validation_label_cur_format'] 	= 'Currency Format';
$lang['account_settings_cntrler_main_form_validation_label_fy_start'] 		= 'Fiscal Year Start';
$lang['account_settings_cntrler_main_form_validation_label_fy_end'] 		= 'Fiscal Year End';
$lang['account_settings_cntrler_main_form_validation_label_date_format'] 	= 'Date Format';

// Alerts
// success
$lang['account_settings_cntrler_main_update_success'] 						= 'Changes Successfully Saved';

// error
$lang['account_settings_cntrler_main_failed_update_entries_beyond_fy_dates_error'] 	= 'No se puede actualizar la configuración, hay %s notas contables fuera del rango del año fiscal configurado';

// warning
$lang['account_settings_cntrler_main_account_locked_warning'] 				= 'No es posible realizar cambios por que el año fiscal activo está bloqueado.';


// account_settings/updateLogo
// Alerts
// success
$lang['account_settings_cntrler_updateLogo_update_success'] 			= 'Image Saved Successfully';

// error
$lang['account_settings_cntrler_updateLogo_update_error'] 				= 'Image not Saved';

// warning
$lang['account_settings_cntrler_updateLogo_file_not_selected_warning'] 	= 'Please Select a File';


// account_settings/deactivate
$lang['account_settings_cntrler_cf_form_validation_label_label'] 		= 'Label';
$lang['account_settings_cntrler_cf_form_validation_label_name'] 		= 'Name';
$lang['account_settings_cntrler_cf_form_validation_label_date_format'] 	= 'Date Format';
$lang['account_settings_cntrler_cf_form_validation_label_fiscal_start'] = 'Fiscal Year Start';
$lang['account_settings_cntrler_cf_form_validation_label_fiscal_end'] 	= 'Fiscal Year End';
$lang['account_settings_cntrler_cf_form_validation_label_db_type'] 		= 'DB Type';
$lang['account_settings_cntrler_cf_form_validation_label_db_name'] 		= 'DB Name';
$lang['account_settings_cntrler_cf_form_validation_label_db_host'] 		= 'DB Host';
$lang['account_settings_cntrler_cf_form_validation_label_db_port'] 		= 'DB Port';
$lang['account_settings_cntrler_cf_form_validation_label_db_username'] 	= 'DB Username';
$lang['account_settings_cntrler_cf_form_validation_label_db_password']	= 'DB Password';

// Alerts
// success
$lang['account_settings_cntrler_cf_add_success'] = 'Año Fiscal creado con éxito.';
$lang['account_settings_cntrler_cf_update_success'] = 'Se actualizaron los saldos del nuevo año fiscal.';

// error
$lang['account_settings_cntrler_cf_form_validation_error_custom'] 		= 'Financial year start date cannot be after end date.';
$lang['account_settings_cntrler_cf_form_validation_error_custom1'] 		= 'Cound not connect to database. Please, check your database settings.';
$lang['account_settings_cntrler_cf_form_validation_error_custom2'] 		= 'Table with the same name as "%s" already existsin the "%s" database. Please, use another database or use a different prefix.';


// account_settings/email
$lang['account_settings_cntrler_email_form_validation_label_email_protocol'] 	= 'Email Protocol';
$lang['account_settings_cntrler_email_form_validation_label_smtp_host'] 		= 'SMTP HOST';
$lang['account_settings_cntrler_email_form_validation_label_smtp_port'] 		= 'SMTP Port';
$lang['account_settings_cntrler_email_form_validation_label_smtp_username'] 	= 'SMTP Username';
$lang['account_settings_cntrler_email_form_validation_label_smtp_password'] 	= 'SMTP Password';
$lang['account_settings_cntrler_email_form_validation_label_email_from'] 		= 'Email From';
$lang['account_settings_cntrler_email_form_validation_label_use_default'] 		= 'Use default email';

// Alerts
// success
$lang['account_settings_cntrler_email_success'] = 'Account Email Settings Updated.';


// account_settings/printer
$lang['account_settings_cntrler_printer_form_validation_label_height'] 		= 'height';
$lang['account_settings_cntrler_printer_form_validation_label_width'] 		= 'width';
$lang['account_settings_cntrler_printer_form_validation_label_top'] 		= 'top';
$lang['account_settings_cntrler_printer_form_validation_label_bottom'] 		= 'bottom';
$lang['account_settings_cntrler_printer_form_validation_label_left'] 		= 'left';
$lang['account_settings_cntrler_printer_form_validation_label_right'] 		= 'right';
$lang['account_settings_cntrler_printer_form_validation_label_orientation'] = 'orientation';
$lang['account_settings_cntrler_printer_form_validation_label_output'] 		= 'output';

// Alerts
// success
$lang['account_settings_cntrler_printer_success'] 		= 'Account Printer Settings Updated.';


// account_settings/tags
$lang['account_settings_cntrler_lock_add_log_locked'] 	= 'Account locked';
$lang['account_settings_cntrler_lock_add_log_unlocked'] = 'Account unlocked';

// Alerts
// success
$lang['account_settings_cntrler_lock_success_locked'] 	= 'Account Locked.';
$lang['account_settings_cntrler_lock_success_unlocked'] = 'Account Unlocked.';

// Account Settings End


// Reports Start
// Views
// reports/index



// Controller
// reports/index


// Alerts
// success

// error

// warning


// reports/logout


// Alerts
// success

// error

// warning


// reports/deactivate



// Alerts
// success

// error

// warning


// Reports End


$lang['strong_success_label'] 		= 'Version';
$lang['strong_error_label'] 		= 'Version';


//Footer
$lang['footer_version'] 		= 'Version';
$lang['footer_copyright_1']     = 'Copyright &copy; ';
$lang['footer_copyright_2'] 	= ' All rights reserved.';

$lang['entry_title'] 	= 'Entry';
$lang['balance'] 	= 'Balance';
$lang['curr_opening_balance'] 	= 'Current opening balance';
$lang['curr_closing_balance'] 	= 'Current closing balance';
$lang['invalid_ledger'] 	= 'La selección de cuenta auxiliar es inválida.';
$lang['invalid_filter_code'] 	= 'Filtrado de códigos inválido.';
$lang['ledger_not_found'] 	= 'Ledger not found';
$lang['yes'] 	= 'Si';
$lang['no'] 	= 'No';


$lang['opening_balance_sheet_as_on'] 	= 'Opening Balance Sheet as on %s';
$lang['balance_sheet_from_to'] 	= 'Balance general generado desde el %s al %s';
$lang['balance_sheet_from'] 	= 'Balance general generado desde el %s';
$lang['closing_balance_sheet_as_on'] 	= 'Closing Balance Sheet as on %s';
$lang['balance_sheet_invalid_end_date_fy_start'] 	= 'La fecha de finalización no puede ser menor a la de inicio de año fiscal, ni mayor a la de finalización de año fiscal.';
$lang['balance_sheet_until'] 	= 'Balance general generado hasta %s';


$lang['profit_loss_title'] 			= 'Ganancias y pérdidas';
$lang['profit_loss_subtitle'] 		= 'Trading and Profit & Loss Statement';

$lang['opening_profit_loss_as_on'] 	= 'Apertura de Ganancias & Pérdidas en la fecha %s';
$lang['profit_loss_from_to'] 		= 'Ganancias y pérdidas desde %s hasta %s';
$lang['profit_loss_from'] 			= 'Ganancias y pérdidas desde %s';
$lang['profit_loss_until'] 			= 'Ganancias y pérdidas hasta %s';
$lang['closing_profit_loss_as_on'] 	= 'Cierre de Ganancias y pérdidas en la fecha %s';
$lang['opening_trial_balance_as_on'] 	= 'Apertura de balance de prueba en la fecha %s';
$lang['trial_balance_from_to'] 		= 'Balance de prueba desde %s hasta %s';
$lang['trial_balance_from'] 		= 'Balance de prueba desde %s';
$lang['trial_balance_until'] 		= 'Balance de prueba hasta %s';
$lang['closing_trial_balance_as_on'] 	= 'Cierre de balance de prueba en la fecha %s';
$lang['ledger_statement_from_to'] 	= 'Estado de cuentas desde %s hasta %s';
$lang['ledger_statement_until'] 	= 'Estado de cuentas hasta %s';
$lang['ledger_entries_from_to'] 	= 'Asientos contables para %s desde %s hasta %s';
$lang['opening_balance_as_on'] 	= 'Balance de apertura  en la fecha %s';
$lang['closing_balance_as_on'] 	= 'Balance de cierre en la fecha %s';

$lang['please_select'] 	= 'Please select';
$lang['no_reconciled_ledgers_found'] 	= 'No Reconciled Ledgers Found...';
$lang['invalid_reconciliation_date'] 	= 'Fecha de conciliación inválida';
$lang['reconciliation_successs'] 	= 'Conciliación realizada con éxito';
$lang['reconciliation_unsuccess'] 	= 'No se realizó la conciliación';

$lang['ledger_not_found_failed_op_balance'] 	= 'Ledger not found. Failed to calculate opening balance.';
$lang['third_not_found_failed_op_balance'] 	= 'Third not found. Failed to calculate opening balance.';
$lang['no_records_found'] 	= 'No records found';

$lang['reconciliation_for_from_to'] 	= 'Conciliación para %s desde %s a %s';
$lang['reconciliation_from_to'] 	= 'Conciliación desde %s a %s';

$lang['export_to_pdf'] 	= 'Exportar a .PDF';
$lang['export_to_xls'] 	= 'Exportar a .XLS';
$lang['export_to_csv'] 	= 'Exportar a .CSV';
$lang['print']			= 'Imprimir';
$lang['delete']			= 'Eliminar';


$lang['lock_account_title']			= 'Bloqueo de cuenta';
$lang['lock_account_btn']			= 'Bloquear cuenta';
$lang['lock_account_span']			= 'Nota : Una vez se haya bloqueado la cuenta, no se podrán hacer cambios en la misma.';

// tags

$lang['tag_deleted_success']		= 'Tag Deleted';
$lang['tag_deleted_error']			= 'Error deleting Tag';
$lang['tag_name']					= 'Tag Name';
$lang['tag_color']					= 'Tag Color';
$lang['tag_backgroud']				= 'Tag Backgroud';
$lang['tags_title']					= 'Tags';
$lang['tag_add']					= 'Add Tag';
$lang['tag_edit']					= 'Edit Tag';
$lang['tag_save']					= 'Save Tag';
$lang['tag_added']					= 'Tag Added';
$lang['tag_saved']					= 'Tag Saved';


$lang['dashboard_company_details']			= 'Current Year/Company Details';
$lang['dashboard_company_details']			= 'Current Year/Company Details';
$lang['dashboard_bc_summary']				= 'Bank &amp; Cash Summary';
$lang['dashboard_b_summary']				= 'Balance Summary';
$lang['recent_activity']					= 'Recent activity';

// Balance Sheet Report
$lang['balance_sheet_assets'] = 'Activos (D)';
$lang['balance_sheet_total_assets'] = 'Total activos';
$lang['balance_sheet_total'] = 'Total';
$lang['balance_sheet_net_loss'] = 'Ganancias y pérdidas (Pérdida del ejercicio)';
$lang['balance_sheet_diff_opp'] = 'Diferencia de balance de apertura';
$lang['balance_sheet_total'] = 'Total';
$lang['balance_sheet_loe'] = 'Pasivos (C)';
$lang['balance_sheet_tloe'] = 'Total pasivos + patrimonio';
$lang['balance_sheet_net_profit'] = 'Ganancias y pérdidas (Ganancia del ejercicio)';
$lang['balance_sheet_diff_opp_of'] = 'Hay una diferencia de balance de apertura de %s';
$lang['balance_sheet_tla_diff'] = 'Hay una diferencia en el total de activos y pasivos de %s';

$lang['balance_sheet_total_operational_cost'] = 'Total Cuentas de órden deudoras';
$lang['balance_sheet_total_operational_cost_2'] = 'Total Cuentas de órden acreedoras';
$lang['balance_sheet_title_operational_costs'] = 'Cuentas de orden';

// Profit Loss Report

$lang['profit_loss_ge']					= 'Gastos (C)';
$lang['profit_loss_da']					= 'Monto (D)';
$lang['profit_loss_tge']				= 'Total de gastos';
$lang['profit_loss_gp']					= 'Ganancias (D)';
$lang['profit_loss_t']					= 'Total';
$lang['profit_loss_gi']					= 'Ingresos (D)';
$lang['profit_loss_ca']					= 'Monto (C)';
$lang['profit_loss_tgi']				= 'Total de ingresos';
$lang['profit_loss_glcd']				= 'Pérdidas (C)';
$lang['profit_loss_ne']					= 'Gastos netos (D)';
$lang['profit_loss_te']					= 'Total de gastos';
$lang['profit_loss_glbd']				= 'Pérdidas B/D';
$lang['profit_loss_np']					= 'Ganancias netas';
$lang['profit_loss_ni']					= 'Ingresos netos (C)';
$lang['profit_loss_ti']					= 'Total ingresos';
$lang['profit_loss_gpbd']					= 'Ganancias B/D';
$lang['profit_loss_nl']					= 'Pérdida neta';

$lang['profit_loss_gi_dr']					= 'Ingreso (D)';
$lang['profit_loss_gi_cr']					= 'Ingreso (C)';



$lang['trial_balance_total_debit']					= 'Total Debit';
$lang['trial_balance_total_credit']					= 'Total Credit';
$lang['balace_sheet_expecting_pos_dr']					= 'Expecting positive D Balance';
$lang['balace_sheet_expecting_pos_cr']					= 'Expecting positive C Balance';


$lang['options']					= 'Opciones';
$lang['thirdsaccounts_view_type']	= 'Tipo de informe';
$lang['ledger_acc_name']				= 'Cuenta auxiliar';
$lang['show_op_bs_title']			= 'Sólo balance de apertura';
$lang['hide_zero_accounts']			= 'Ocultar cuentas en 0';
$lang['skip_entries_disapproved']	= 'Omitir Asientos contables desaprobadas';
$lang['nivel_balance_sheet_span']	= 'Nivel de grupos';
$lang['start_date']					= 'Fecha inicial';
$lang['end_date']					= 'Fecha final';
$lang['entry_type_select']			= 'Tipo de asiento contable';
$lang['entry_type_all']				= 'Todas';

$lang['start_date_span']					= 'Nota : Deje el campo de fecha inicial vacío si quiere que se realice el filtrado desde el inicio del año fiscal.';
$lang['end_date_span']					= 'Nota : Deje el campo de fecha final vacío si quiere que se realice el filtrado hasta el final del año fiscal.';
$lang['clear']					= 'Limpiar formulario';

$lang['view']					= 'View';
$lang['edit']					= 'Edit';
$lang['delete']					= 'Delete';
$lang['show_all_entries']					= 'Ver todas las Asientos contables';
$lang['reconciliation_data'] 		= 'Fecha de conciliación';
$lang['Reconcile'] 		= 'Conciliar';
$lang['cr_total'] 		= 'Total Créditos';
$lang['dr_total'] 		= 'Total Débitos';
$lang['language'] 		= 'Lenguage';

// $lang['form_validation_is_db1_unique'] = 'The {field} field must contain a unique value.';
// $lang['form_validation_amount_okay'] = 'Invalid amount specified. Maximum {param} decimal places allowed';
$lang['dashboard_company_role'] 		= 'Rol de usuario';
$lang['user_dashboard_account_info_title'] 		= 'Información de compañía activa';
//


//Acciones

//Usuario

$lang['users_index_delete_user_alert_title'] = '¿Está seguro de eliminar este usuario?';
$lang['users_index_delete_user_alert_text'] = 'Este proceso es irreversible, por favor asegúrese si el usuario seleccionado es el correcto.';

//accounts

$lang['accounts_index_delete_account_alert_title'] = '¿Está seguro de eliminar esta cuenta?';
$lang['accounts_index_delete_account_alert_text'] = 'Este proceso es irreversible, por favor asegúrese si la cuenta seleccionada es la correcta.';

//entries

$lang['entrys_index_delete_entry_alert_title'] = '¿Está seguro de anular esta asiento contable?';
$lang['entrys_index_delete_entry_alert_text'] = 'Este proceso es irreversible, por favor asegúrese si la asiento contable seleccionada es la correcta.';

$lang['entrys_index_approve_entry_alert_title'] = '¿Está seguro de aprobar esta asiento contable?';
$lang['entrys_index_approve_entry_alert_text'] = 'Si está seguro de que la nota seleccionada es la correcta, de clic en <b>Si</b>.';


//entrytypes

$lang['accounts_index_delete_entrytype_alert_title'] = '¿Está seguro de eliminar este tipo de asiento contable?';
$lang['accounts_index_delete_entrytype_alert_text'] = 'Este proceso es irreversible, por favor asegúrese si el tipo de nota seleccionado es el correcto.';

//tags

$lang['accounts_index_delete_tag_alert_title'] = '¿Está seguro de eliminar esta etiqueta?';
$lang['accounts_index_delete_tag_alert_text'] = 'Este proceso es irreversible, por favor asegúrese si la etiqueta seleccionada es la correcto.';

//CF

$lang['cf_modal_title'] = 'Cierre de año fiscal';
$lang['cf_modal_text'] = '¿Está seguro de realizar el cierre del año fiscal?';

//Thirds

$lang['thirds_index_delete_title'] = '¿Está seguro de eliminar el tercero?';
$lang['thirds_index_delete_text'] = 'Este proceso es irreversible, por favor asegúrese si el tercero seleccionado es el correcto.';

//Thirds

$lang['thirds_index_op_balance_title'] = '¿Está seguro de guardar los balances?';
$lang['thirds_index_op_balance_text'] = 'Este proceso es irreversible y no se podrán modificar los datos después, por favor asegúrese si los datos son correctos.';


//Toastr alerts

$lang['toast_success_title']			= "¡Correcto!";
$lang['toast_error_title']			= "¡Error!";
$lang['toast_warning_title']			= "¡Atención!";
$lang['receipt']			= "Recibo";

//Third groups
$lang['Employee'] = 'Empleado';
$lang['Customer'] = 'Cliente';
$lang['Supplier'] = 'Proveedor';
$lang['Creditor'] = 'Acreedor';


$lang['company'] = "Compañía";
$lang['from'] = "Desde";
$lang['to'] = "Hasta";
$lang['expedition_date'] = "Fecha de expedición";

$lang['thirdsaccount_title'] = "Libro auxiliar por %s";
$lang['vat_no_&_accounts'] = "NIT Y CUENTAS";
$lang['accounts_&_vat_no'] = "CUENTAS Y NIT";
$lang['state'] = "Estado";

$lang['select'] = "Seleccione...";

$lang['cannot_delete_payment_method_used'] = "No se puede eliminar el método de pago por que ya se han registrados pagos con este medio";

$lang['cancel'] = 'Cancelar';
$lang['send'] = 'Enviar';


$lang['cost_centers_index_title'] = 'Centros de costos';
$lang['cost_centers_index_th_code'] = 'Código';
$lang['cost_centers_index_th_name'] = 'Nombre';
$lang['cost_centers_index_th_actions'] = 'Acciones';
$lang['cost_centers_index_add'] = 'Agregar centro de costo';
$lang['cost_centers_index_edit'] = 'Editar centro de costo';
$lang['cost_centers_add_label_code'] = 'Código';
$lang['cost_centers_add_label_name'] = 'Nombre';
$lang['cost_centers_add_code_exists'] = 'Ya existe un centro de costo con este código';

$lang['cost_centers_not_founded'] = 'No hay ningún centro de costo definido';

$lang['cost_center_label'] = 'Centro de costo';

$lang['account_parameter_method'] = 'Método de configuración de cuentas';
$lang['account_parameter_method_tax'] = 'Configuración por tarifa de IVA';
$lang['account_parameter_method_category'] = 'Configuración por categoría de producto';

$lang['transfer_profit_or_loss_balances'] = 'Trasladar saldos de ganancia o pérdida';


$lang['cf_profit_loss_gp']					= 'Ganancias (C)';
$lang['cf_profit_loss_glcd']				= 'Pérdidas (D)';

$lang['entry_items_without_cost_centers'] = 'Existen notas contables sin centro de costos definido.';
$lang['entry_items_without_ledgers'] = 'Existen notas contables sin una cuenta auxiliar válida.';
$lang['entry_items_without_companies'] = 'Existen notas contables sin un tercero válido.';
$lang['companies_opbalance_without_cost_center'] = 'Existen balances iniciales con centros de costos inválidos.';
$lang['companies_opbalance_without_companies'] = 'Existen balances iniciales con terceros inválidos.';
$lang['companies_opbalance_without_ledgers'] = 'Existen balances iniciales con cuentas auxiliares inválidas.';
$lang['opening_balance_not_transferred'] = 'Los balances de apertura no fueron transferidos al año nuevo';
$lang['exist_entries_disapproved'] = 'Existen notas contables desaprobadas';
$lang['exist_entries_with_invalid_date'] = 'Existen notas contables con fecha inválida';

$lang['create_account_accountant_name'] = 'Nombre del Contador';
$lang['create_account_fiscal_name'] = 'Nombre del Revisor fiscal';
$lang['create_account_representant_name'] = 'Nombre del Representante';
$lang['signatures'] = 'Firmas (Dejar en blanco si no se muestra)';

//Sidebar Menu (Exogena)
$lang['Exogenous'] 								= 'Exógena';
$lang['Exogenous_menu_Formats'] 				= 'Formatos';
$lang['Exogenous_menu_Formats_detall'] 			= 'Detalle de Formatos';
//Sidebar Fin Menu (Exogena)

/*  Exogena */

$lang['Exogenous_Formats_add'] 					= 'Agregar Formatos';
$lang['Exogenous_Formats_Items_code'] 			= 'Código';
$lang['Exogenous_Formats_Items_version'] 		= 'Versión';
$lang['Exogenous_Formats_Items_name'] 			= 'Nombre';
$lang['Exogenous_Formats_Items_description'] 	= 'Descripcion';
$lang['Exogenous_Formats_Items_min_amount'] 	= 'Monto mínimo';
$lang['Exogenous_Formats_Items_use_tercero'] 	= 'Uso Tercero';

$lang['Exogenous_Formats_message_add'] 			= 'Formato Creado con Exito';
$lang['Exogenous_Formats_message_add_error'] 	= 'No es posible crear Formato';
$lang['Exogenous_Formats_message_edit'] 		= 'Formato Editado con Exito';
$lang['Exogenous_Formats_message_edit_error'] 	= 'No es posible editar Formato';

$lang['Exogenous_Formats_content_add'] 					= 'Agregar Contenido';
$lang['Exogenous_Formats_content_edit'] 				= 'Editar Contenido';
$lang['Exogenous_Formats_content_Items_name'] 			= 'Nombre';
$lang['Exogenous_Formats_content_Items_size'] 			= 'Size';
$lang['Exogenous_Formats_content_Items_obs'] 			= 'Observacion';
$lang['Exogenous_Formats_content_Items_order'] 			= 'Order';
$lang['Exogenous_Formats_content_Items_atrib'] 			= 'Atributo';
$lang['Exogenous_Formats_content_Items_type'] 			= 'Tipo';

$lang['Exogenous_Formats_content_message_add'] 			= 'Contenido Creado con Exito';
$lang['Exogenous_Formats_content_message_add_error'] 	= 'No es posible crear Contenido';
$lang['Exogenous_Formats_content_message_edit'] 		= 'Contenido Editado con Exito';
$lang['Exogenous_Formats_content_message_edit_error'] 	= 'No es posible editar Contenido';


/* Fin Exogena */

/*
|--------------------------------------------------------------------------
| Certificate withholdings
|--------------------------------------------------------------------------
|
*/
$lang["certificate_withholdings_title"] = "Certificado de retenciones";

$lang["certificate_withholdings_label_type_note_to_exclude"] = "Tipo de nota a excluir";
$lang["certificate_withholdings_label_generate_certificate"] = "Generar certificados";
$lang["certificate_withholdings_label_general_report_excel"] = "Informe general Excel";
$lang["certificate_withholdings_label_summary_report"] = "Informe general resumido";
$lang["certificate_withholdings_label_general_report"] = "Informe general";
$lang["certificate_withholdings_label_initial_date"] = "Fecha Inicial";
$lang["certificate_withholdings_label_final_date"] = "Fecha Final";
$lang["certificate_withholdings_label_Retefuente"] = "Retefuente";
$lang["certificate_withholdings_label_third_party"] = "Tercero";
$lang["certificate_withholdings_label_company"] = "Compañia";
$lang["certificate_withholdings_label_Reteiva"] = "Reteiva";
$lang["certificate_withholdings_label_Reteica"] = "Reteica";
$lang["certificate_withholdings_label_NIT/CC"] = "NIT/CC";
$lang["certificate_withholdings_label_name"] = "Nombre";
$lang["certificate_withholdings_label_document_type"] = "Tipo doc.";
$lang["certificate_withholdings_label_document_number"] = "Número doc.";
$lang["certificate_withholdings_label_date"] = "Fecha";
$lang["certificate_withholdings_label_concept"] = "Concepto";
$lang["certificate_withholdings_label_base"] = "Base";
$lang["certificate_withholdings_label_retention"] = "Retención";
$lang["certificate_withholdings_label_percentage"] = "%";

$lang['create_user_accessible_cost_centers_placeholder'] = 'Seleccione centros de costo para %s';
$lang['create_user_accessible_accounts_label'] = 'Seleccione las compañías que manejará el usuario';


$lang['step'] = 'Paso de cierre';
$lang['action'] = 'Acción';
$lang['status'] = 'Estado';
$lang['pending'] = 'Pendiente';
$lang['processing'] = 'En proceso';
$lang['done'] = 'Realizado';
$lang['year_creation'] = 'Creación de año';
$lang['initial_balance_transfer'] = 'Traslado de saldos de terceros';
$lang['initial_balance_transfer_again'] = 'Volver a trasladar saldos de terceros';
$lang['submit'] = 'Enviar';
$lang['progress'] = 'Progreso';
$lang['continue'] = 'Continuar';
$lang['redo'] = 'Rehacer';

$lang['year_creation_title'] = 'Creación de año nuevo';
$lang['year_creation_text'] = '¿Está seguro de iniciar el proceso de creación de año?, el proceso no puede ser interrumpido y éste puede tardar unos minutos.';
$lang['initial_balance_transfer_title'] = 'Traslado de saldos de terceros';
$lang['initial_balance_transfer_text'] = '¿Está seguro de iniciar el proceso de traslado de saldos de terceros?, el proceso no puede ser interrumpido y éste puede tardar unos minutos.';
$lang['initial_balance_transfer_again_title'] = 'Volver a trasladar saldos de terceros';
$lang['initial_balance_transfer_again_text'] = '¿Está seguro de volver a trasladar saldos de terceros?, el proceso no puede ser interrumpido y éste puede tardar unos minutos.';

$lang['footer_version_accounting'] = 'Versión contabilidad';
$lang['footer_version_comercial'] = 'Versión compañía activa';
$lang['incorrect_version'] = 'Versiones incompatibles';

$lang['invalid_company_logo'] = 'No se ha subido un logo para la compañía, por favor, suba uno';

$lang['trial_balance_closing_balance_difference']		= 'Existe una diferencia en el balance de cierre de %s';
$lang['exist_mismatched_entries'] = 'Existen asientos contables descuadrados';
$lang['show_base'] = 'Ver base';
$lang['entry_group_view'] = 'Ver agrupada';
$lang['default_loading_accounts_index'] = 'Carga por defecto de plan de cuentas';
$lang['ledgers_cntrler_delete_entries_parametrized_error'] = 'Cuenta usada para parametrización de %s';
$lang['with_closing_balance'] = 'Con balance de cierre';
$lang['without_closing_balance'] = 'Sin balance de cierre';
$lang['entries_views_index_import_entries'] = 'Importar asientos';
$lang['entries_importer_sample_button']  				= 'Descargar XLS modelo';
$lang['thirds_cntrler_import_h3_title_1']  			= 'Importar terceros';
$lang['entry_origin'] = "Origen";
$lang['entry_user'] = "Creado por";
?>