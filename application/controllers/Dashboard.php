<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends Admin_Controller {
	public function __construct() {
        parent::__construct();
        $this->load->model('reports_model');
    }   
    
	public function index() {
		
		//conexión persistente
		$this->reconnect_persistent_database();

		/* Cash and bank sumary */
		$ledgers = $this->DB1->where('type', 1)->get('ledgers'.$this->DB1->dbsuffix)->result_array();
		$ledgersCB = array();
		foreach ($ledgers as $ledger) {
			$ledgersCB[] = array(
				'name' => ($this->functionscore->toCodeWithName($ledger['code'], $ledger['name'])),
				'balance' => $this->ledger_model->closingBalance($ledger['id']),
			);
		}
		$this->data['ledgers'] = $ledgersCB;
		$this->load->library('AccountList');
		/* Account summary */
		$assets = new AccountList();
		$assets->Group = &$this->Group;
		$assets->Ledger = &$this->Ledger;
		$assets->only_opening = false;
		$assets->start_date = null;
		$assets->end_date = null;
		$assets->affects_gross = -1;
		$assets->start(1);

		$liabilities = new AccountList();
		$liabilities->Group = &$this->Group;
		$liabilities->Ledger = &$this->Ledger;
		$liabilities->only_opening = false;
		$liabilities->start_date = null;
		$liabilities->end_date = null;
		$liabilities->affects_gross = -1;
		$liabilities->start(2);

		$liabilities2 = new AccountList();
		$liabilities2->Group = &$this->Group;
		$liabilities2->Ledger = &$this->Ledger;
		$liabilities2->only_opening = false;
		$liabilities2->start_date = null;
		$liabilities2->end_date = null;
		$liabilities2->affects_gross = -1;
		$liabilities2->start(3);

		if ($liabilities->cl_total_dc == $liabilities2->cl_total_dc) {
			$liabilities_total_amount =  $this->functionscore->calculate($liabilities->cl_total, $liabilities2->cl_total, '+');
			$liabilities_total_dc = $liabilities->cl_total_dc;
		} else {

			$liabilities_total_amount =  $this->functionscore->calculate($liabilities->cl_total, $liabilities2->cl_total, '-');

			if ($this->functionscore->calculate($liabilities_total_amount, 0, '<')) {
				$liabilities_total_dc = "C";
			} else {
				$liabilities_total_dc = "D";
			}


		}

		$income = new AccountList();
		$income->Group = &$this->Group;
		$income->Ledger = &$this->Ledger;
		$income->only_opening = false;
		$income->start_date = null;
		$income->end_date = null;
		$income->affects_gross = -1;
		$income->start(4);

		$expense = new AccountList();
		$expense->Group = &$this->Group;
		$expense->Ledger = &$this->Ledger;
		$expense->only_opening = false;
		$expense->start_date = null;
		$expense->end_date = null;
		$expense->affects_gross = -1;
		$expense->start(5);

		$expense2 = new AccountList();
		$expense2->Group = &$this->Group;
		$expense2->Ledger = &$this->Ledger;
		$expense2->only_opening = false;
		$expense2->start_date = null;
		$expense2->end_date = null;
		$expense2->affects_gross = -1;
		$expense2->start(6);

		if ($expense->cl_total_dc == $expense2->cl_total_dc) {
			$expense_total_amount = $this->functionscore->calculate($expense->cl_total, $expense2->cl_total, '+');
			$expense_total_dc = $expense->cl_total_dc; 
		} else {
			$expense_total_amount = $this->functionscore->calculate($expense->cl_total, $expense2->cl_total, '-');

			if ($this->functionscore->calculate($expense_total_amount, 0, '<')) {
				$expense_total_dc = "C";
			} else {
				$expense_total_dc = "D";
			}

		}

		//*PROFIT LOSS AND OPEN BALANCE DIFFERENCE FOR ASSETS AND LIABILITIES*//
			$profitloss_total = $this->functionscore->calculate($income->cl_total, $expense_total_amount, '-');
			$profitloss_total_positive = $this->functionscore->calculate($profitloss_total, 0, 'n');

			if ($this->functionscore->calculate($profitloss_total, 0, '<')) {
				if ($assets->cl_total_dc == "C") {
					$assets->cl_total = $this->functionscore->calculate($assets->cl_total, $profitloss_total_positive, '-');
				} else if ($assets->cl_total_dc == "D") {
					$assets->cl_total = $this->functionscore->calculate($assets->cl_total, $profitloss_total_positive, '+');
				}
			} else {
				if ($liabilities_total_dc == "C") {
					$liabilities_total_amount = $this->functionscore->calculate($liabilities_total_amount, $profitloss_total, '-');
				} else if ($liabilities_total_dc == "D") {
					$liabilities_total_amount = $this->functionscore->calculate($liabilities_total_amount, $profitloss_total, '+');
				}
			}

			$opdiff = $this->ledger_model->getOpeningDiff();

			if ($opdiff['opdiff_balance_dc'] == "C") {
				if ($liabilities_total_dc == "C") {
					$liabilities_total_amount = $this->functionscore->calculate($liabilities_total_amount, $opdiff['opdiff_balance'], '+');
				} else if ($liabilities_total_dc == "D") {
					$liabilities_total_amount = $this->functionscore->calculate($liabilities_total_amount, $opdiff['opdiff_balance'], '-');
				}
			} else if ($opdiff['opdiff_balance_dc'] == "D") {
				if ($assets->cl_total_dc = "C") {
					$assets->cl_total = $this->functionscore->calculate($assets->cl_total, $opdiff['opdiff_balance'], '-');
				} else if ($assets->cl_total_dc = "D") {
					$assets->cl_total = $this->functionscore->calculate($assets->cl_total, $opdiff['opdiff_balance'], '+');
				}
			}
		//*PROFIT LOSS AND OPEN BALANCE DIFFERENCE FOR ASSETS AND LIABILITIES*//

		//*ASSETS AND LIABILITIES FOR PROFIT AND LOSS*//
			$gross_pl = $this->functionscore->calculate($income->cl_total, $expense_total_amount, '-');
			$gross_pl_positive = $this->functionscore->calculate($gross_pl, 0, 'n');

			if ($this->functionscore->calculate($gross_pl, 0, '>')) {
				if ($income->cl_total_dc == "C") {
					$income->cl_total = $this->functionscore->calculate($income->cl_total, $gross_pl, '-');
				} else if ($income->cl_total_dc == "D") {
					$income->cl_total = $this->functionscore->calculate($income->cl_total, $gross_pl, '+');
				}
			} else {
				if ($expense_total_dc == "C") {
					$expense_total_amount = $this->functionscore->calculate($expense_total_amount, $gross_pl_positive, '+');
				} else if ($expense_total_dc == "D") {
					$expense_total_amount = $this->functionscore->calculate($expense_total_amount, $gross_pl_positive, '-');
				}
			}
		//*ASSETS AND LIABILITIES FOR PROFIT AND LOSS*//

		$accsummary = array(
			'assets_total_dc' => $assets->cl_total_dc,
			'assets_total' => $assets->cl_total,
			'liabilities_total_dc' => $liabilities_total_dc,
			'liabilities_total' => $liabilities_total_amount,
			'income_total_dc' => $income->cl_total_dc,
			'income_total' => $income->cl_total,
			'expense_total_dc' => $expense_total_dc,
			'expense_total' => $expense_total_amount,
		);
		$this->data['accsummary'] = $accsummary;

		$users = [];

		$users_data = $this->db->get('users')->result_array();

		foreach ($users_data as $row => $user) {
			$users[$user['id']] = $user;
		}

		$this->data['users'] = $users;

		// render page
		$this->render('user/dashboard');

	}
	public function getIncomeExpenseChart()
	{
		$json_ie_stats = array(
			'Income'=> $this->reports_model->getTotalPeriodical(3),
			'Expense'=> $this->reports_model->getTotalPeriodical(4),
		);
		$this->functionscore->send_json($json_ie_stats);
	}

}
