<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Groups extends Admin_Controller {
	public function __construct() {
        parent::__construct();
		$this->load->model('site');
    }   

/**
 * add method
 *
 * @return void
 */
	public function add() {

		if ($this->mAccountSettings->account_locked == 1) {
			$this->session->set_flashdata('warning', lang('account_settings_cntrler_main_account_locked_warning'));
			redirect('dashboard');
		}

		$this->mPageTitle = lang('groups_views_add_title');
		$this->data['title'] = lang('groups_views_add_title');

		$this->form_validation->set_rules('parent_id', lang('groups_cntrler_add_form_validation_label_parent_group'), 'required');
		$this->form_validation->set_rules('name', lang('groups_cntrler_add_form_validation_label_name'), 'required');
		$this->form_validation->set_rules('code', lang('groups_cntrler_add_form_validation_label_code'), 'is_db1_unique[groups'.$this->DB1->dbsuffix.'.code]|required'); //
		// $this->form_validation->set_rules('affects_gross', lang('groups_cntrler_add_form_validation_label_affects_gross'), 'required');

		if ($this->form_validation->run() == FALSE) {
            
            $this->load->library('GroupTree');

			/* Create list of parent groups */
			$parentGroups = new GroupTree();
			$parentGroups->Group = &$this->Group;
			$parentGroups->current_id = -1;
			$parentGroups->build(0);
			$parentGroups->toList($parentGroups, -1);
			$this->data['parents'] = $parentGroups->groupList;

			$this->niveles = [];

			foreach ($parentGroups->children_groups as $id => $children_group) {

				$this->levels_groups_ledgers($children_group);

			}

			$this->data['niveles'] = $this->niveles;
			
			// render page
			$this->render('groups/add');

        } else {

        	$codigo = $this->input->post('code');

        	$codedbledger = $this->DB1->where('ledgers'.$this->DB1->dbsuffix.'.code', $codigo)->get('ledgers'.$this->DB1->dbsuffix);
        	if ($codedbledger->num_rows() > 0) {
        		$this->session->set_flashdata('warning', 'Ya existe una cuenta auxiliar con el código indicado.');
        		redirect('groups/add');
        	}

        	$parentcode = $this->DB1->select('CHAR_LENGTH(code) AS length')
        							->from('groups'.$this->DB1->dbsuffix)
        							->where('id', $this->input->post('parent_id'))->get();

        	$parentcode = $parentcode->row_array();

        	$parentcodelength = $parentcode['length'];


        	if ($parentcodelength > strlen($this->input->post('code'))) {
        		$this->session->set_flashdata('warning', 'El código del nuevo grupo, no puede ser más corto que el del grupo padre.');
        		redirect('groups/add');
        	}

			$data = array(
				'parent_id' => $this->input->post('parent_id'),
				'name' => $this->input->post('name'),
				'code' => $this->input->post('code'),
				'affects_gross' => 1,
			);
			
			/* Save group */
			if ($this->mAccountSettings->year_closed == 1) {
				$new_config = $this->site->get_new_connection();
				if ($this->check_database($new_config)) {
					$DB2 = $this->load->database($new_config, TRUE);
					$q = $DB2->select('(MAX(id) + 1) AS new_id')->get('groups'.$DB2->dbsuffix);
					if ($q->num_rows() > 0) {
						$new_id = $q->row();
						$new_id = $new_id->new_id;
						$q2 = $this->DB1->get_where('groups'.$this->DB1->dbsuffix, ['id' => $new_id]);
						if ($q2->num_rows() > 0) {
							$this->session->set_flashdata('error', 'No se creó el grupo, inconsistencia de datos entre años');
							redirect('accounts/index');
						} else {
							$data['id'] = $new_id;
							$this->DB1->insert('groups'.$this->DB1->dbsuffix, $data);
							$DB2->insert('groups'.$DB2->dbsuffix, $data);
						}
					}
				} else {
					redirect('settings/cf');
				}
			} else {
				$this->DB1->insert('groups'.$this->DB1->dbsuffix, $data);
			}
			$this->settings_model->add_log(lang('groups_cntrler_add_label_add_log') . $this->input->post('name'), 1);
			$this->session->set_flashdata('message', sprintf(lang('groups_cntrler_add_group_created_successfully'), $this->input->post('name')));
			redirect('groups/add');
        }
	}

	public function addParent() {

		if ($this->mAccountSettings->account_locked == 1) {
			$this->session->set_flashdata('warning', lang('account_settings_cntrler_main_account_locked_warning'));
			redirect('dashboard');
		}

		$this->mPageTitle = lang('groups_parent_views_add_title');
		$this->data['title'] = lang('groups_parent_views_add_title');

		$this->form_validation->set_rules('name', lang('groups_cntrler_add_form_validation_label_name'), 'required');
		$this->form_validation->set_rules('code', lang('groups_cntrler_add_form_validation_label_code'), 'is_db1_unique[groups'.$this->DB1->dbsuffix.'.code]|required');
		// $this->form_validation->set_rules('affects_gross', lang('groups_cntrler_add_form_validation_label_affects_gross'), 'required');

		if ($this->form_validation->run() == FALSE) {
            
            $this->load->library('GroupTree');

			/* Create list of parent groups */
			$parentGroups = new GroupTree();
			$parentGroups->Group = &$this->Group;
			$parentGroups->current_id = -1;
			$parentGroups->build(0);
			$parentGroups->toList($parentGroups, -1);
			$this->data['parents'] = $parentGroups->groupList;

			if ($this->getLastParent(2) > 9) {
				$this->session->set_flashdata('error', lang('groups_cntrler_add_account_parent_error_code'));
			}

			
			// render page
			$this->render('groups/addParent');

        } else {

        	$data = array(
				'parent_id' => 0,
				'name' => $this->input->post('name'),
				'code' => $data['code'] = $this->input->post('code'),
				'affects_gross' => 1,
			);
			
			/* Save group */
			$this->DB1->insert('groups'.$this->DB1->dbsuffix, $data);
			$this->settings_model->add_log(lang('groups_cntrler_add_label_add_log') . $this->input->post('name'), 1);
			$this->session->set_flashdata('message', sprintf(lang('groups_cntrler_add_group_created_successfully'), $this->input->post('name')));
			redirect('groups/addParent');

        }
		
	}

	/**
 * edit method
 *
 * @throws NotFoundException
 * @throws ForbiddenException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {

		if ($this->mAccountSettings->account_locked == 1) {
			$this->session->set_flashdata('warning', lang('account_settings_cntrler_main_account_locked_warning'));
			redirect('dashboard');
		}

	    $original_value = $this->DB1->where('id', $id)->from('groups'.$this->DB1->dbsuffix)->get()->row()->code ;
	    if($this->input->post('code') != $original_value) {
	       $is_unique =  'is_db1_unique[groups'.$this->DB1->dbsuffix.'.code]';
	    } else {
	       $is_unique =  '';
	    }

		/* Check for valid group */
		if (empty($id)) {
			$this->session->set_flashdata('error', lang('groups_cntrler_edit_group_not_specified_error'));
			redirect('accounts');
		}
		$group = $this->DB1->where('id',$id)->get('groups'.$this->DB1->dbsuffix)->row_array();
		if (!$group) {
			$this->session->set_flashdata('error', lang('groups_cntrler_edit_group_not_found_error'));
			redirect('accounts');
		}
		// if ($id <= 4) {
		// 	$this->session->set_flashdata('error', lang('groups_cntrler_edit_basic_account_permission_denied_error'));
		// 	redirect('accounts');
		// }


		$this->form_validation->set_rules('parent_id', lang('groups_cntrler_edit_form_validation_label_parent_group'), 'required');
		$this->form_validation->set_rules('name', lang('groups_cntrler_edit_form_validation_label_name'), 'required');
		$this->form_validation->set_rules('code', lang('groups_cntrler_edit_form_validation_label_code'), 'required');
		// $this->form_validation->set_rules('affects_gross', lang('groups_cntrler_edit_form_validation_label_affects_gross'), 'required');

		if ($this->form_validation->run() == FALSE) {
            $this->load->library('GroupTree');
			/* Create list of parent groups */
			$parentGroups = new GroupTree();
			$parentGroups->Group = &$this->Group;
			$parentGroups->current_id = $id;
			$parentGroups->build(0);
			$parentGroups->toList($parentGroups, -1);
			$this->data['parents'] = $parentGroups->groupList;
			$this->data['group'] = $group;

			$this->niveles = [];

			foreach ($parentGroups->children_groups as $id => $children_group) {

				$this->levels_groups_ledgers($children_group);

			}

			$this->data['niveles'] = $this->niveles;
			// render page
		$this->render('groups/edit');
        } else {
        	/* Check if acccount is locked */
			if ($this->mAccountSettings->account_locked == 1) {
				$this->session->set_flashdata('error', lang('groups_cntrler_edit_account_locked_error'));
				redirect('accounts');
			}

			/* Check if group and parent group are not same */
			if ($id == $this->input->post('parent_id')) {
				$this->session->set_flashdata('error', lang('groups_cntrler_edit_account_parent_group_same_error'));
				redirect('accounts');
			}

			$prevcodecons = $this->DB1->where('groups'.$this->DB1->dbsuffix.'.id', $id)->get('groups'.$this->DB1->dbsuffix)->row_array();
			$prevcode = $prevcodecons['code'];

			if ($this->input->post('code') != $prevcode) { //revisamos si el código cambió.
				$codigo = $this->input->post('code');
				$codedbgroup = $this->DB1->where('groups'.$this->DB1->dbsuffix.'.code', $codigo)->get('groups'.$this->DB1->dbsuffix);
				if ($codedbgroup->num_rows() > 0) {
					$this->session->set_flashdata('warning', 'Ya existe un grupo con el código indicado.');
					redirect('groups/edit/'.$id);
				} //si ya existe un grupo con el nuevo código indicado, indicamos el error.

				$codedbledger = $this->DB1->where('ledgers'.$this->DB1->dbsuffix.'.code', $codigo)->get('ledgers'.$this->DB1->dbsuffix);
				if ($codedbledger->num_rows() > 0) {
					$this->session->set_flashdata('warning', 'Ya existe una cuenta auxiliar con el código indicado.');
					redirect('groups/edit/'.$id);
				}//si ya existe un auxiliar con el nuevo código indicado, indicamos el error.

				$childrengroups = $this->DB1->where('groups'.$this->DB1->dbsuffix.'.parent_id', $id)->get('groups'.$this->DB1->dbsuffix);
				if ($childrengroups->num_rows() > 0) {
					$this->session->set_flashdata('error', 'No se puede editar el código del grupo por que tiene grupos hijos.');
					redirect('groups/edit/'.$id);
				}//si el grupo tiene grupos hijos no se puede editar el código, indicamos el error.

				$childrenledgers = $this->DB1->where('ledgers'.$this->DB1->dbsuffix.'.group_id', $id)->get('ledgers'.$this->DB1->dbsuffix);
				if ($childrenledgers->num_rows() > 0) {
					$this->session->set_flashdata('error', 'No se puede editar el código del grupo por que tiene cuentas auxiliares.');
					redirect('groups/edit/'.$id);
				}//si el grupo tiene cuentas auxiliares no se puede editar el código, indicamos el error.

			}

			

			$data = array(
				'parent_id' => $this->input->post('parent_id'),
				'name' => $this->input->post('name'),
				'code' => $data['code'] = $this->input->post('code'),
				'affects_gross' => 1,
			);

			if ($this->mAccountSettings->year_closed == 1) {
				$new_config = $this->site->get_new_connection();
				if ($this->check_database($new_config)) {
					$DB2 = $this->load->database($new_config, TRUE);
					$q = $this->DB1->get_where('groups'.$this->DB1->dbsuffix, ['id'=>$id]);
					$q2 = $DB2->get_where('groups'.$DB2->dbsuffix, ['id'=>$id]);
					if ($q->num_rows() > 0 && $q2->num_rows() > 0) {
						$curr_year_data = $q->row();
						$prev_year_data = $q2->row();
						if ($curr_year_data->code == $prev_year_data->code) {
							$this->DB1->update('groups'.$this->DB1->dbsuffix, $data, ['id' => $id]);
							$DB2->update('groups'.$DB2->dbsuffix, $data, ['id' => $id]);
						} else {
							$this->session->set_flashdata('error', 'No se pudo editar, los datos entre años no coinciden');
							redirect('groups/edit/'.$id);
						}
					} else {
						$this->session->set_flashdata('error', 'No se pudo editar, los datos entre años no coinciden');
						redirect('groups/edit/'.$id);
					}
				} else {
					redirect('settings/cf');
				}
			} else {
				$this->DB1->update('groups'.$this->DB1->dbsuffix, $data, ['id' => $id]);
			}
			$this->settings_model->add_log(lang('groups_cntrler_edit_label_add_log') . $this->input->post('name'), 1);
			$this->session->set_flashdata('message', sprintf(lang('groups_cntrler_edit_group_updated_successfully'), $this->input->post('name')));
			redirect('accounts');
        }

	}

	/**
	 * delete method
	 *
	 * @throws NotFoundException
	 * @throws MethodNotAllowedException
	 * @param string $id
	 * @return void
	 */
		public function delete($id = null) {

			if ($this->mAccountSettings->account_locked == 1) {
				$this->session->set_flashdata('warning', lang('account_settings_cntrler_main_account_locked_warning'));
				redirect('dashboard');
			}

			/* Check if valid id */
			if (empty($id)) {
				$this->session->set_flashdata('error', lang('groups_cntrler_delete_group_not_specified_error'));
				redirect('accounts');
			}

			/* Check if group exists */
			$group = $this->DB1->where('id',$id)->get('groups'.$this->DB1->dbsuffix)->row_array();
			if (!$group) {
				$this->session->set_flashdata('error', lang('groups_cntrler_delete_group_not_found_error'));
				redirect('accounts');
			}

			// /* Check if group can be deleted */
			// if (strlen($group['code']) == 1) {
			// 	$this->session->set_flashdata('error', lang('groups_cntrler_delete_basic_account_permission_denied_error'));
			// 	redirect('accounts');
			// }

			/* Check if any child groups exists */
			$this->DB1->where('groups'.$this->DB1->dbsuffix.'.parent_id', $id);
			$q = $this->DB1->get('groups'.$this->DB1->dbsuffix);
			if ($q->num_rows() > 0) {
				$this->session->set_flashdata('error', lang('groups_cntrler_delete_child_group_exists_error'));
				redirect('accounts');
			}

			/* Check if any child ledgers exists */
			$this->DB1->where('ledgers'.$this->DB1->dbsuffix.'.group_id', $id);
			$q = $this->DB1->get('ledgers'.$this->DB1->dbsuffix);
			if ($q->num_rows() > 0) {
				$this->session->set_flashdata('error', lang('groups_cntrler_delete_child_ledger_exists_error'));
				redirect('accounts');
			}

			/* Delete group */
			$this->DB1->delete('groups'.$this->DB1->dbsuffix, array('id' => $id));
			$this->settings_model->add_log(lang('groups_cntrler_delete_label_add_log') . $group['name'], 1);
			$this->session->set_flashdata('message', sprintf(lang('groups_cntrler_delete_group_deleted_successfully'), $group['name']));
			redirect('accounts');

		}

	public function getNextCode() {
		$id = isset($_POST['id']) ? $_POST['id'] : null;
		if ($id) {
			$this->DB1->where('id', $id);
			$p_group_code = $this->DB1->get('groups'.$this->DB1->dbsuffix)->row()->code;
			// print_r($p_group_code);
			$this->DB1->where('id !=', $id);
			$this->DB1->where('parent_id', $id);
			$this->DB1->like('code', $p_group_code);
			$q = $this->DB1->get('groups'.$this->DB1->dbsuffix)->result();
			// print_r($q);
			if ($q) {
				$last = end($q);
				$last = $last->code;
				$l_array = explode('-', $last);
				$new_index = end($l_array);
				$new_index += 1;
				$new_index = sprintf("%02d", $new_index);
				echo $new_index;
			}else{
				echo $p_group_code."01";
			}
		}

	}

	public function getLastParent($typeanswer = 1) {
		$this->DB1->where('parent_id', '0');
		$p_group_code = $this->DB1->get('groups'.$this->DB1->dbsuffix)->result();
		// print_r($p_group_code);
		// print_r($q);
		if ($p_group_code) {
			$last = end($p_group_code);
			$last = $last->code;
			$last += 1;
			$answer = $last;
		}else{
			$answer = "1";
		}

		if ($typeanswer == 1) {
			echo $answer;
		} else if ($typeanswer == 2) {
			return $answer;
		}

	}

	public function levels_groups_ledgers($array, $level = 1){
	    $this->niveles[$level][] = $array->id;
	    // Revisar y agregar los ledgers del grupo actual
	    if (isset($array->children_ledgers)) {
	        foreach ($array->children_ledgers as $ledger) {
	            $this->niveles[$level + 1][] = $ledger['code'];
	        }
	    }
	    // Recorrer los subgrupos del grupo actual
	    if (!empty($array->children_groups)) {
	        foreach ($array->children_groups as $subgroup) {
	            $this->levels_groups_ledgers($subgroup, $level + 1);
	        }
	    }
	}

}



