<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ledgers extends Admin_Controller {
	public function __construct() {
        parent::__construct();
		$this->load->model('site');
    }

    /**
 * add method
 *
 * @return void
 */
	public function add() {

		if ($this->mAccountSettings->account_locked == 1) {
			$this->session->set_flashdata('warning', lang('account_settings_cntrler_main_account_locked_warning'));
			redirect('dashboard');
		}

		$allowed = $this->mAccountSettings->decimal_places;
		$this->form_validation->set_rules('name', lang('ledgers_cntrler_add_form_validation_label_name'), 'required');
		$this->form_validation->set_rules('group_id', lang('ledgers_cntrler_add_form_validation_label_group_id'), 'required');
		$this->form_validation->set_rules('op_balance_dc', lang('ledgers_cntrler_add_form_validation_label_op_balance_dc'), 'required');
		$this->form_validation->set_rules('op_balance', lang('ledgers_cntrler_add_form_validation_label_op_balance'), "amount_okay[$allowed]");
		$this->form_validation->set_rules('code', lang('ledgers_cntrler_add_form_validation_label_code'), 'is_db1_unique[ledgers'.$this->DB1->dbsuffix.'.code]');

		if ($this->form_validation->run() == FALSE) {
			$this->load->library('GroupTree');
			/* Create list of parent groups */
			$parentGroups = new GroupTree();
			$parentGroups->Group = &$this->Group;
			$parentGroups->current_id = -1;
			$parentGroups->build(0);
			$parentGroups->toList($parentGroups, -1);
			$this->data['parents'] = $parentGroups->groupList;
			$this->niveles = [];
			$this->get_levels();
			$this->data['niveles'] = $this->niveles;
			$entries = $this->DB1->get('entries'.$this->DB1->dbsuffix);
			if ($entries->num_rows() > 0) {
				$this->data['entries'] = true;
				$this->session->set_flashdata('error', lang('ledgers_cntrler_add_ledger_entries_exists'));
			} else {
				$this->data['entries'] = false;
			}
			// render page
			$this->render('ledgers/add');
        } else {
        	$data = array(
				'code' => NULL,
				'op_balance' => 0,
				'name' => $this->input->post('name'),
				'group_id' => $this->input->post('group_id'),
				'op_balance_dc' => $this->input->post('op_balance_dc'),
				'notes' => $this->input->post('notes'),
				'reconciliation' => 0,
				'cost_center' => 0,
				'type' => 0,
			);

			if (!empty($this->input->post('reconciliation'))) {
				$data['reconciliation'] = 1;
			}
			if (!empty($this->input->post('cost_center'))) {
				$data['cost_center'] = 1;
			}
			if (!empty($this->input->post('code'))) {
				$data['code'] = $this->input->post('code');
			}
			if (!empty($this->input->post('type'))) {
				$data['type'] = 1;
			}
			if (!empty($this->input->post('op_balance'))) {
				$data['op_balance'] = $this->input->post('op_balance');
			}
			/* Count number of decimal places */

			$groupdbcode = $this->DB1->where('groups'.$this->DB1->dbsuffix.'.code', $this->input->post('code'))->get('groups'.$this->DB1->dbsuffix);

			if ($groupdbcode->num_rows() > 0) {
				$this->session->set_flashdata('warning', 'El código indicado para la cuenta auxiliar ya existe para un grupo');
				redirect('ledgers/add');
			} else {
				$this->session->set_flashdata('message', sprintf(lang('ledgers_cntrler_add_ledger_created_successfully'), $this->input->post('name')));

				if ($this->mAccountSettings->year_closed == 1) {
					// exit('A');
					$new_config = $this->site->get_new_connection();
					if ($this->check_database($new_config)) {
						$DB2 = $this->load->database($new_config, TRUE);
						$q = $DB2->select('(MAX(id) + 1) AS new_id')->get('ledgers'.$DB2->dbsuffix);
						if ($q->num_rows() > 0) {
							$new_id = $q->row();
							$new_id = $new_id->new_id;
							$q2 = $this->DB1->get_where('ledgers'.$this->DB1->dbsuffix, ['id' => $new_id]);
							if ($q2->num_rows() > 0) {
								$this->session->set_flashdata('error', 'No se creó la cuenta auxiliar, inconsistencia de datos entre años');
								redirect('accounts/index');
							} else {
								$data['id'] =$new_id;
								$this->DB1->insert('ledgers'.$this->DB1->dbsuffix, $data);
								$DB2->insert('ledgers'.$DB2->dbsuffix, $data);
							}
						}
					} else {
						redirect('settings/cf');
					}
				} else {
					$this->DB1->insert('ledgers'.$this->DB1->dbsuffix, $data);
				}
				$this->settings_model->add_log(lang('ledgers_cntrler_add_label_add_log') . $this->input->post('name'), 1);
				redirect('ledgers/add');
			}
        }
	}

	public function get_levels(){

		$this->load->library('AccountList');
		$accountlist = new AccountList();
		$accountlist->Group = &$this->Group;
		$accountlist->Ledger = &$this->Ledger;
		$accountlist->only_opening = true;
		$accountlist->affects_gross = -1;
		$accountlist->start(0, 1);
		foreach ($accountlist->children_groups as $id => $children_group) {
			$this->levels_groups_ledgers($children_group);
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {

		if ($this->mAccountSettings->account_locked == 1) {
			$this->session->set_flashdata('warning', lang('account_settings_cntrler_main_account_locked_warning'));
			redirect('dashboard');
		}

		/* Check for valid group */
		if (empty($id)) {
			$this->session->set_flashdata('error', lang('ledgers_cntrler_edit_ledger_not_specified_error'));
			redirect('accounts');
		}
		$ledger = $this->DB1->where('id', $id)->get('ledgers'.$this->DB1->dbsuffix)->row_array();
		if (!$ledger) {
			$this->session->set_flashdata('error', lang('ledgers_cntrler_edit_ledger_not_found_error'));
			redirect('accounts');
		}
		$original_value = $ledger['code'] ;
	    if($this->input->post('code') != $original_value) {
	       $is_unique =  'is_db1_unique[ledgers'.$this->DB1->dbsuffix.'.code]';
	    } else {
	       $is_unique =  '';
	    }

		$allowed = $this->mAccountSettings->decimal_places;
		$this->form_validation->set_rules('name', 'ledgers_cntrler_edit_form_validation_label_name', 'required');
		$this->form_validation->set_rules('group_id', 'ledgers_cntrler_edit_form_validation_label_group_id', 'required');
		$this->form_validation->set_rules('op_balance_dc', 'ledgers_cntrler_edit_form_validation_label_op_balance_dc', 'required');
		$this->form_validation->set_rules('op_balance', 'ledgers_cntrler_edit_form_validation_label_op_balance', "amount_okay[$allowed]");
		$this->form_validation->set_rules('code', 'ledgers_cntrler_edit_form_validation_label_code', $is_unique);

		if ($this->form_validation->run() == FALSE) {
			$this->load->library('GroupTree');
			/* Create list of parent groups */
			$parentGroups = new GroupTree();
			$parentGroups->Group = &$this->Group;
			$parentGroups->current_id = -1;
			$parentGroups->build(0);
			$parentGroups->toList($parentGroups, -1);
			$this->data['parents'] = $parentGroups->groupList;

			$this->data['ledger'] = $ledger;
			$this->niveles = [];
			$this->get_levels();
			$this->data['niveles'] = $this->niveles;
			
			// echo "<pre>";
			// var_dump($this->niveles);
			// echo "</pre>";
			// exit();
			$entries = $this->DB1->get('entries'.$this->DB1->dbsuffix);
			if ($entries->num_rows() > 0) {
				$this->data['entries'] = true;
				$this->session->set_flashdata('error', lang('ledgers_cntrler_edit_ledger_entries_exists'));
			} else {
				$this->data['entries'] = false;
			}
			$this->render('ledgers/edit');
        } else {
        	/* Check if acccount is locked */
			if ($this->mAccountSettings->account_locked == 1) {
				$this->session->set_flashdata('error', lang('ledgers_cntrler_edit_account_locked_error'));
				redirect('accounts');
			}
			$data = array(
				'code' => NULL,
				'op_balance' => 0,
				'name' => $this->input->post('name'),
				'group_id' => $this->input->post('group_id'),
				'op_balance_dc' => $this->input->post('op_balance_dc'),
				'notes' => $this->input->post('notes'),
				'reconciliation' => 0,
				'cost_center' => 0,
				'type' => 0,
			);
			if (!empty($this->input->post('reconciliation'))) {
				$data['reconciliation'] = 1;
			}
			if (!empty($this->input->post('cost_center'))) {
				$data['cost_center'] = 1;
			}
			if (!empty($this->input->post('code'))) {
				$data['code'] = $this->input->post('code');
			}
			if (!empty($this->input->post('type'))) {
				$data['type'] = 1;
			}
			if (!empty($this->input->post('op_balance'))) {
				$data['op_balance'] = $this->input->post('op_balance');
			}
			if ($this->input->post('code') != $original_value) { //revisamos si el código cambió.
				$codigo = $this->input->post('code');
				$codedbgroup = $this->DB1->where('groups'.$this->DB1->dbsuffix.'.code', $codigo)->get('groups'.$this->DB1->dbsuffix);
				if ($codedbgroup->num_rows() > 0) {
					$this->session->set_flashdata('warning', 'Ya existe un grupo con el código indicado.');
					redirect('ledgers/edit/'.$id);
				} //si ya existe un grupo con el nuevo código indicado, indicamos el error.
			}
			if ($this->mAccountSettings->year_closed == 1) {
				$new_config = $this->site->get_new_connection();
				if ($this->check_database($new_config)) {
					$DB2 = $this->load->database($new_config, TRUE);
					$q = $this->DB1->get_where('ledgers'.$this->DB1->dbsuffix, ['id'=>$id]);
					$q2 = $DB2->get_where('ledgers'.$DB2->dbsuffix, ['id'=>$id]);
					if ($q->num_rows() > 0 && $q2->num_rows() > 0) {
						$curr_year_data = $q->row();
						$prev_year_data = $q2->row();
						if ($curr_year_data->code == $prev_year_data->code) {
							$this->DB1->update('ledgers'.$this->DB1->dbsuffix, $data, ['id' => $id]);
							$DB2->update('ledgers'.$DB2->dbsuffix, $data, ['id' => $id]);
						} else {
							$this->session->set_flashdata('error', 'No se pudo editar, los datos entre años no coinciden');
							redirect('ledgers/edit/'.$id);
						}
					} else {
						$this->session->set_flashdata('error', 'No se pudo editar, los datos entre años no coinciden');
						redirect('ledgers/edit/'.$id);
					}
				} else {
					redirect('settings/cf');
				}
			} else {
				$this->DB1->update('ledgers'.$this->DB1->dbsuffix, $data, ['id' => $id]);
			}
			$this->session->set_flashdata('message', sprintf(lang('ledgers_cntrler_edit_ledger_updated_successfully'), $ledger['name']));
			$this->settings_model->add_log(lang('ledgers_cntrler_edit_label_add_log') . $this->input->post('name'), 1);
			redirect('accounts');
        }

	}


	/**
	 * delete method
	 *
	 * @throws NotFoundException
	 * @throws MethodNotAllowedException
	 * @param string $id
	 * @return void
	 */
	public function delete($id = null) {

		if ($this->mAccountSettings->account_locked == 1) {
			$this->session->set_flashdata('warning', lang('account_settings_cntrler_main_account_locked_warning'));
			redirect('dashboard');
		}

		/* Check if valid id */
		if (empty($id)) {
			$this->session->set_flashdata('error', lang('ledgers_cntrler_delete_ledger_not_specified_error'));
			redirect('accounts');
		}

		/* Check if ledger exists */
		$ledger = $this->DB1->where('id', $id)->get('ledgers'.$this->DB1->dbsuffix)->row_array();
		if (!$ledger) {
			$this->session->set_flashdata('error', lang('ledgers_cntrler_delete_ledger_not_found_error'));
			redirect('accounts');
		}

		// Se revisa si tiene balance de apertura

		if ($ledger['op_balance'] > 0) {
			$this->session->set_flashdata('error', lang('ledgers_cntrler_delete_op_balance_error'));
			redirect('accounts');
		}

		/* Check if any entry item using this ledger still exists */
		$this->DB1->where('entryitems'.$this->DB1->dbsuffix.'.ledger_id', $id);
		$q = $this->DB1->get('entryitems'.$this->DB1->dbsuffix, 1);
		if ($q->num_rows() > 0) {
			$this->session->set_flashdata('error', lang('ledgers_cntrler_delete_entries_exist_error'));
			redirect('accounts');
		}

		if ($this->DB1->get_where('withholdings', ['account_id'=>$id])->num_rows() > 0) {
			$this->session->set_flashdata('error', sprintf(lang('ledgers_cntrler_delete_entries_parametrized_error'), 'retenciones'));
			redirect('accounts');
		}

		if ($this->DB1->get_where('expense_categories', ("(tax_ledger_id = ".$id." OR tax_2_ledger_id = ".$id.")"))->num_rows() > 0) {
			$this->session->set_flashdata('error', sprintf(lang('ledgers_cntrler_delete_entries_parametrized_error'), 'Categoría de gastos'));
			redirect('accounts');
		}

		if ($this->DB1->get_where('payment_methods'.$this->DB1->dbsuffix, ("(receipt_ledger_id = ".$id." OR payment_ledger_id = ".$id.")"))->num_rows() > 0) {
			$this->session->set_flashdata('error', sprintf(lang('ledgers_cntrler_delete_entries_parametrized_error'), 'Métodos de pago'));
			redirect('accounts');
		}

		if ($this->DB1->get_where('payments', ['concept_ledger_id'=>$id])->num_rows() > 0) {
			$this->session->set_flashdata('error', sprintf(lang('ledgers_cntrler_delete_entries_parametrized_error'), 'Conceptos en pagos'));
			redirect('accounts');
		}

		$this->DB1->delete('ledgers'.$this->DB1->dbsuffix, array('id' => $id));
		$this->settings_model->add_log(lang('ledgers_cntrler_delete_label_add_log') . $ledger['name'], 1);

		$this->session->set_flashdata('message', sprintf(lang('ledgers_cntrler_delete_ledger_deleted_successfully'), $ledger['name']));
		redirect('accounts');
	}

	/**
	 * closing balance method
	 *
	 * Return closing balance for the ledger
	 *
	 * @return void
	 */
	public function cl($id = null) {

		/* Read ledger id from url get request */
		if ($id == null) {
			$id = (int)$this->input->get('id');
		}

		/* Check if valid id */
		if (!$id) {
			// $this->data['cl'] = array('cl' => array('dc' => '', 'amount' => 0));
			echo json_encode(0);
			return;
		}

		/* Check if ledger exists */
		$this->DB1->where('id', $id);
		$ledger = $this->DB1->get('ledgers'.$this->DB1->dbsuffix)->row_array();
		if (!$ledger) {
			$cl = array('cl' => array('dc' => '', 'amount' => ''));
		}else{
			$cl = $this->ledger_model->closingBalance($id);
			$status = 'ok';
			if ($ledger['type'] == 1) {
				if ($cl['dc'] == 'C') {
					$status = 'neg';
				}
			}

			/* Return closing balance */
			$cl = array('cl' =>
					array(
						'dc' => $cl['dc'],
						'amount' => $cl['amount'],
						'status' => $status,
					)
			);
		}
		echo json_encode($cl);
	}

	public function getNextCode() {
		$id = $_POST['id'];
		$this->DB1->where('id', $id);
		$p_group_code = $this->DB1->get('groups'.$this->DB1->dbsuffix)->row()->code;
		$this->DB1->where('group_id', $id);
		$q = $this->DB1->get('ledgers'.$this->DB1->dbsuffix)->result();
		if ($q) {
			$last = end($q);
			$last = $last->code;
			$l_array = explode('-', $last);
			$new_index = end($l_array);
			$new_index += 1;
			$new_index = sprintf("%04d", $new_index);
			echo $new_index;
		}else{
			echo $p_group_code."0001";
		}

	}

	public function levels_groups_ledgers($array, $level = 1){
	    $this->niveles[$level][] = $array->id;
	    // Revisar y agregar los ledgers del grupo actual
	    if (isset($array->children_ledgers)) {
	        foreach ($array->children_ledgers as $ledger) {
	            $this->niveles[$level + 1][] = $ledger['code'];
	        }
	    }
	    // Recorrer los subgrupos del grupo actual
	    if (!empty($array->children_groups)) {
	        foreach ($array->children_groups as $subgroup) {
	            $this->levels_groups_ledgers($subgroup, $level + 1);
	        }
	    }
	}
}



