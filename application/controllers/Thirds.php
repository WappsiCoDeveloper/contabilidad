<?php

/**
 *
 */
class Thirds extends Admin_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('site');
	}

	public function index(){
		// set page title
		$this->mPageTitle = lang('page_title_thirds_index');
		$this->data['title'] = lang('page_title_thirds_index');
		// render page
		$groups = $this->DB1->get('groups')->result_array();
		foreach ($groups as $row => $group) {
			$g_m[$group['id']] = $group['description'];
		}
		// var_dump($g_m);
		$this->data['groups'] = $g_m;
		$third_groups = $this->DB1->like('name', 'customer')->or_like('name', 'employee')->or_like('name', 'supplier')->or_like('name', 'creditor')->get('groups')->result_array();
		$wherearr = [];
		foreach ($third_groups as $row => $tg) {
			$wheraarr['group_id'] = $tg['id'];
		}
		$thirds = $this->DB1->where($wherearr)->order_by('name', 'asc')->get('companies')->result_array();
		$this->data['thirds'] = $thirds;
		$this->render('thirds/index');

	}


	public function add(){
		// set page title
		$this->mPageTitle = lang('page_title_thirds_add');
		$this->data['title'] = lang('page_title_thirds_add');
		// render page
		$third_groups = $this->DB1->like('name', 'customer')->or_like('name', 'employee')->or_like('name', 'supplier')->or_like('name', 'creditor')->get('groups')->result_array();
		$this->data['third_groups'] = $third_groups;
		$countries = $this->DB1->get('countries')->result_array();
		$this->data['countries'] = $countries;
		$documenttypes = $this->DB1->get('documentypes');
		if ($documenttypes->num_rows() > 0 && $documenttypes !== FALSE) {
			$this->data['documenttypes'] = $documenttypes->result_array();
		} else {
			$this->session->set_flashdata('error', 'No existen tipos de documentos especificados');
			redirect('thirds');
		}
		$pos_settings = $this->DB1->get('settings');
		if ($pos_settings->num_rows() > 0 && $pos_settings !== FALSE) {
			$pos_settings = $pos_settings->row_array();
			$this->data['customer_group_default'] = $pos_settings['customer_group'];
			$this->data['price_group_default'] = $pos_settings['price_group'];

			$name_customer_group_default = $this->DB1->where('id', $pos_settings['customer_group'])->get('customer_groups')->row_array();
			$this->data['name_customer_group_default'] = $name_customer_group_default['name'];
			$name_price_group_default = $this->DB1->where('id', $pos_settings['price_group'])->get('price_groups')->row_array();
			$this->data['name_price_group_default'] = $name_price_group_default['name'];

		} else {
			$this->session->set_flashdata('error', 'No se encontró la configuración de POS, para grupo precios y clientes predeterminados.');
			redirect('thirds');
		}
		if ($this->input->method() == "post") {
			$this->form_validation->set_rules('vat_no', lang('thirds_cntrler_add_form_validation_vat_no_label'), 'required');
			$this->form_validation->set_rules('address', lang('thirds_cntrler_add_form_validation_address_label'), 'required');
			$this->form_validation->set_rules('country', lang('thirds_cntrler_add_form_validation_country_label'), 'required');
			$this->form_validation->set_rules('state', lang('thirds_cntrler_add_form_validation_state_label'), 'required');
			$this->form_validation->set_rules('city', lang('thirds_cntrler_add_form_validation_city_label'), 'required');
			// $this->form_validation->set_rules('postal_code', lang('thirds_cntrler_add_form_validation_postal_code_label'), 'required');
			$this->form_validation->set_rules('phone', lang('thirds_cntrler_add_form_validation_phone_label'), 'required');
			$this->form_validation->set_rules('email', lang('thirds_cntrler_add_form_validation_email_label'), 'required');
			$this->form_validation->set_rules('tipo_regimen', lang('thirds_cntrler_add_form_validation_email_label'), 'required');
			$this->form_validation->set_rules('tipo_documento', lang('thirds_cntrler_add_form_validation_email_label'), 'required');
			$this->form_validation->set_rules('email', lang('thirds_cntrler_add_form_validation_email_label'), 'required');
			if ($this->form_validation->run() === TRUE) {
				$data_input = [];
				foreach ($this->input->post() as $input => $value) {
					$data_input[$input] = $value;
				}
				if ($this->input->post('type_person') == 2) {
					$data_input['name'] = $this->input->post('first_name')." ".$this->input->post('second_name')." ".$this->input->post('first_lastname')." ".$this->input->post('second_lastname');

					$data_input['company'] = $this->input->post('first_name')." ".$this->input->post('second_name')." ".$this->input->post('first_lastname')." ".$this->input->post('second_lastname');

				} else if ($this->input->post('type_person') != 2) {

					$data_input['company'] = $this->input->post('name');

				}
				$data_input['commercial_register'] = '';
				$data_input['document_code'] = 0;
				$data_input['location'] = '';
				if ($this->mAccountSettings->year_closed == 1) {
					$new_config = $this->site->get_new_connection();
					if ($this->check_database($new_config)) {
						$DB2 = $this->load->database($new_config, TRUE);
						$q = $DB2->select('(MAX(id) + 1) AS new_id')->get('companies');
						if ($q->num_rows() > 0) {
							$new_id = $q->row();
							$new_id = $new_id->new_id;
							$q2 = $this->DB1->get_where('companies', ['id' => $new_id]);
							if ($q2->num_rows() > 0) {
								$this->session->set_flashdata('error', 'No se creó el tipo de documento, inconsistencia de datos entre años');
								redirect('thirds');
							} else {
								$data_input['id'] = $new_id;
								$DB2->insert('companies', $data_input);
								$this->DB1->insert('companies', $data_input);
							}
						}
					} else {
						redirect('settings/cf');
					}
				} else {
					$this->DB1->insert('companies', $data_input);
				}
				$this->session->set_flashdata('message', lang('thirds_cntrler_add_third_created_successfully'));
				redirect('thirds');
			}
		}
		$this->render('thirds/add');
	}

	public function relations(){

		$defined = $this->DB1->get('settings'.$this->DB1->dbsuffix)->row_array();
		$this->data['thirds_relationed'] = $defined['thirds_relationed'];
		$this->mPageTitle = lang('page_title_thirds_relations');
		$this->data['title'] = lang('page_title_thirds_relations');

		$relations_exists = $this->DB1->get('companies_opbalance'.$this->DB1->dbsuffix);

		if ($relations_exists->num_rows() > 0) {
			$this->data['relations_exists'] = $relations_exists->result_array();
		}

		if ($this->mAccountSettings->cost_center == 1) {
			$cost_centers = $this->DB1->query('SELECT * FROM '.$this->DB1->dbprefix.'cost_centers'.$this->DB1->dbsuffix);
            if ($cost_centers->num_rows()) {
            	$cost_centers = $cost_centers->result_array();
            	$this->data['cost_centers'] = $cost_centers;
            }
		}

		if ($this->input->method() == 'post') {

			if ($this->input->post('id_relation')) {
				$re = $this->input->post('id_relation');
			}

			$ledgers = $this->input->post('ledger_id');
			$thirds = $this->input->post('third');
			$cost_center_id = $this->input->post('cost_center_id');
			$op_balances = $this->input->post('op_balance');
			$op_balances_dc = $this->input->post('op_balance_dc');

			$third_ledger_valid = [];

			$insert_data = [];
			$update_data = [];

			$totales_account = [];
			$sql_not_in = "(";

			foreach ($ledgers as $id => $valor) {

				if (!isset($totales_account[$valor][$op_balances_dc[$id]])) {
					$totales_account[$valor][$op_balances_dc[$id]] = $op_balances[$id];
				} else if (isset($totales_account[$valor][$op_balances_dc[$id]])) {
					$totales_account[$valor][$op_balances_dc[$id]] += $op_balances[$id];
				}

				if (isset($re[$id])) {
					$update_data[$re[$id]] = array(
						'id_companies' => $thirds[$id],
						'id_ledgers' => $valor,
						'cost_center_id' => isset($cost_center_id[$id]) ? $cost_center_id[$id] : NULL,
						'op_balance' => $op_balances[$id],
						'op_balance_dc' => $op_balances_dc[$id],
					);

					$sql_not_in .= $re[$id].", ";
				} else{
					$insert_data[$id] = array(
						'id_companies' => $thirds[$id],
						'id_ledgers' => $valor,
						'cost_center_id' => isset($cost_center_id[$id]) ? $cost_center_id[$id] : NULL,
						'op_balance' => $op_balances[$id],
						'op_balance_dc' => $op_balances_dc[$id],
					);
				}
			}
			$sql_not_in = trim($sql_not_in, ", ");
			$sql_not_in.=")";

			if ($this->input->post('normal_submit') && !$this->input->post('hanging_submit')) {

				$op_total = $this->DB1->query("select * from ".$this->DB1->dbprefix."companies_opbalance".$this->DB1->dbsuffix." WHERE id NOT IN ".$sql_not_in);
				if ($op_total->num_rows() > 0) {
					foreach (($op_total->result()) as $row) {
						if (!isset($totales_account[$row->id_ledgers][$row->op_balance_dc])) {
							$totales_account[$row->id_ledgers][$row->op_balance_dc] = $row->op_balance;
						} else if (isset($totales_account[$row->id_ledgers][$row->op_balance_dc])) {
							$totales_account[$row->id_ledgers][$row->op_balance_dc] += $row->op_balance;
						}
					}
				}

				foreach ($totales_account as $id_ledger => $op) {

					$total_C_D = $this->functionscore->calculate( ( (isset($op['D'])) ? $op['D'] : 0), ( (isset($op['C'])) ? $op['C'] : 0), '-');

					if ($total_C_D < 0) {
						$ledger_op_amount = $total_C_D * -1;
						$ledger_op_dc = "C";
					} else if ($total_C_D >= 0) {
						$ledger_op_amount = $total_C_D ;
						$ledger_op_dc = "D";
					}

					$this->DB1->where('id', $id_ledger)->update('ledgers'.$this->DB1->dbsuffix, array('op_balance' => $ledger_op_amount, 'op_balance_dc' => $ledger_op_dc));
				}

				$this->DB1->where('1=1')->update('settings'.$this->DB1->dbsuffix, array('thirds_relationed' => 1));
				$this->session->set_flashdata('message', 'Actualizado con éxito.');
				redirect('thirds/relations');
			}
			// exit(var_dump(count($insert_data)));

			if (count($insert_data) > 0 && count($update_data) > 0) {

				$mensaje = "Los datos ";
				$warning = false;

				$insert = $this->DB1->insert_batch('companies_opbalance'.$this->DB1->dbsuffix, $insert_data);
				if ($insert > 0) {
					$mensaje .= "nuevos se insertaron correctamente ";
				} else {
					$mensaje .= "nuevos NO se insertaron ";
					$warning = true;
				}

				$actualizadas = 0;

				foreach ($update_data as $id => $data) {
					$this->DB1->where('id', $id)->update('companies_opbalance'.$this->DB1->dbsuffix, $data);
					if ($this->DB1->affected_rows()) {
						$actualizadas++;
					}
				}

				if ($actualizadas == 0) {
					$mensaje .= "y no se detectó ningún cambio en los existentes";
					$warning = true;
				} else if ($actualizadas >= 0) {
					$mensaje .= "y se actualizaron los cambios en los existentes";
				}

				$this->session->set_flashdata(($warning ? 'warning' : 'message'), $mensaje);

			} else if (count($insert_data) > 0) {
				$insert = $this->DB1->insert_batch('companies_opbalance'.$this->DB1->dbsuffix, $insert_data);
				if ($insert > 0) {
					$this->session->set_flashdata('message', 'Se guardaron con éxito.');
				} else {
					$this->session->set_flashdata('error', 'Hubo un error al actualizar');
				}
			} else if (count($update_data) > 0) {
				$actualizadas = 0;

				if (count($update_data) > 0) {
					foreach ($update_data as $id => $data) {
						$this->DB1->where('id', $id)->update('companies_opbalance'.$this->DB1->dbsuffix, $data);
						if ($this->DB1->affected_rows()) {
							$actualizadas++;
						}
					}
				}

				if ($actualizadas == 0) {
					$this->session->set_flashdata('warning', 'Sin cambios detectados.');
				} else if ($actualizadas >= 0) {
					$this->session->set_flashdata('message', 'Actualizado con éxito.');
				}

			}

			redirect('thirds/relations');

		} else {
			$this->render('thirds/relations');
		}
	}

	public function relationstotal(){
		if ($this->input->method() == "post") {

			$ledgers = $this->input->post('ledger_id');
			$thirds = $this->input->post('third');
			$op_balances = $this->input->post('op_balance');
			$op_balances_dc = $this->input->post('op_balance_dc');
			$id_relation = $this->input->post('id_relation');
			$sql_not_in = "(";

			$third_ledger_valid = [];

			$totales = [];

			$update_data = [];

			if ($ledgers && count($ledgers) > 0) {
				foreach ($ledgers as $id => $valor) {

					if (isset($id_relation[$id])) {
						$sql_not_in.=$id_relation[$id].", ";

						$update_data[$id_relation[$id]] = array(
							'id_companies' => $thirds[$id],
							'id_ledgers' => $valor,
							'op_balance' => $op_balances[$id],
							'op_balance_dc' => $op_balances_dc[$id],
						);
					}

					if ($valor == 0) {
						continue;
					}

					if (isset($totales[$valor][$op_balances_dc[$id]])) {

						$totales[$valor][$op_balances_dc[$id]] += $op_balances[$id];

					} else if (!isset($totales[$valor][$op_balances_dc[$id]])) {

						$totales[$valor][$op_balances_dc[$id]] = $op_balances[$id];

					}

				}
			}

			$sql_not_in = trim($sql_not_in, ", ");
			$sql_not_in.=")";

			$rows = "";
			$descuadre = 0;
			$total_C = 0;
			$total_D = 0;

			foreach ($totales as $account => $total) {

				$op_balance = [];

				$ledger = $this->DB1->where('id', $account)->get('ledgers'.$this->DB1->dbsuffix)->row_array();

				$total_D += ( (isset($total['D'])) ? $total['D'] : 0);
				$total_C += ( (isset($total['C'])) ? $total['C'] : 0);

				$amount_ledger = $this->functionscore->calculate(( (isset($total['D'])) ? $total['D'] : 0), ( (isset($total['C'])) ? $total['C'] : 0), '-');

				if ($amount_ledger < 0) {
					$op_balance['amount'] = $amount_ledger * -1;
					$op_balance['dc'] = "C";
				} else if ($amount_ledger >= 0) {
					$op_balance['amount'] = $amount_ledger;
					$op_balance['dc'] = "D";
				}

				$rows .= "<tr>
							<td>".$ledger['code']." - ".$ledger['name']."</td>
							<td>".$this->functionscore->toCurrency('D',$amount_ledger)."</td>
						</tr>";

			}

			if (count($id_relation) > 0) {
				$op_total = $this->DB1->query("select SUM(op_balance) AS total_c, 0 AS total_d from ".$this->DB1->dbprefix."companies_opbalance".$this->DB1->dbsuffix." WHERE op_balance_dc = 'C' AND id NOT IN ".$sql_not_in."
								UNION
							   select 0 AS total_c, SUM(op_balance) AS total_d from ".$this->DB1->dbprefix."companies_opbalance".$this->DB1->dbsuffix." WHERE op_balance_dc = 'D' AND id NOT IN ".$sql_not_in);
				if ($op_total->num_rows() > 0) {
					foreach (($op_total->result()) as $row) {
						$total_D += $row->total_d;
						$total_C += $row->total_c;
					}
				}
			}

			$total_C_D = $this->functionscore->calculate($total_D, $total_C, '-');

			if ($total_C_D != 0) {
				$descuadre = TRUE;
				$descuadre_amount = $this->functionscore->toCurrency('D',$total_C_D);
			} else if ($total_C_D == 0) {
				$descuadre = FALSE;
				$descuadre_amount = 0;

				if (count($update_data) > 0) {
					foreach ($update_data as $id => $data) {
						$this->DB1->where('id', $id)->update('companies_opbalance'.$this->DB1->dbsuffix, $data);
					}
				}

			}

			$montonum = abs($total_C_D);
			$montodc = ( ($total_C_D < 0) ? 'D' : 'C');

			$result = array(
				'row' => $rows,
				'descuadre' => $descuadre,
				'descuadre_amount' => $descuadre_amount,
				'montonum' => $montonum,
				'montodc' => $montodc
				);

			echo json_encode($result);

		}
	}


	public function edit($third_id = NULL){

		if ($third_id != NULL) {
				$third = $this->DB1->where('companies.id', $third_id)->get('companies');
				if ($third->num_rows() > 0) {
					// set page title
					$this->mPageTitle = lang('page_title_thirds_edit');
					$this->data['title'] = lang('page_title_thirds_edit');

					$third = $third->row_array();
					$this->data['third'] = $third;

					$countries = $this->DB1->get('countries')->result_array();
					$this->data['countries'] = $countries;

					$states = $this->DB1->like('states.DEPARTAMENTO', $third['state'])->get('states')->result_array();
					$this->data['states'] = $states;

					$cities = $this->DB1->where('cities.CODIGO', $third['city_code'])->get('cities')->result_array();
					$this->data['cities'] = $cities;

					$third_groups = $this->DB1->like('name', 'customer')->or_like('name', 'employee')->or_like('name', 'supplier')->or_like('name', 'creditor')->get('groups')->result_array();
					$this->data['third_groups'] = $third_groups;

					$documenttypes = $this->DB1->get('documentypes');
					if ($documenttypes->num_rows() > 0 && $documenttypes !== FALSE) {
						$this->data['documenttypes'] = $documenttypes->result_array();
					} else {
						$this->session->set_flashdata('error', 'No existen tipos de documentos especificados');
						redirect('thirds');
					}

					$pos_settings = $this->DB1->get('settings');
					if ($pos_settings->num_rows() > 0 && $pos_settings !== FALSE) {
						$pos_settings = $pos_settings->row_array();
						$this->data['customer_group_default'] = $pos_settings['customer_group'];
						$this->data['price_group_default'] = $pos_settings['price_group'];

						$name_customer_group_default = $this->DB1->where('id', $pos_settings['customer_group'])->get('customer_groups')->row_array();
						$this->data['name_customer_group_default'] = $name_customer_group_default['name'];
						$name_price_group_default = $this->DB1->where('id', $pos_settings['price_group'])->get('price_groups')->row_array();
						$this->data['name_price_group_default'] = $name_price_group_default['name'];

					} else {
						$this->session->set_flashdata('error', 'No se encontró la configuración de POS, para grupo precios y clientes predeterminados.');
						redirect('thirds');
					}


					if ($this->input->method() == "post") {
						$this->form_validation->set_rules('group_id', lang('thirds_cntrler_add_form_validation_group_id_label'), 'required');
						$this->form_validation->set_rules('vat_no', lang('thirds_cntrler_add_form_validation_vat_no_label'), 'required');
						$this->form_validation->set_rules('address', lang('thirds_cntrler_add_form_validation_address_label'), 'required');
						$this->form_validation->set_rules('country', lang('thirds_cntrler_add_form_validation_country_label'), 'required');
						$this->form_validation->set_rules('state', lang('thirds_cntrler_add_form_validation_state_label'), 'required');
						$this->form_validation->set_rules('city', lang('thirds_cntrler_add_form_validation_city_label'), 'required');
						// $this->form_validation->set_rules('postal_code', lang('thirds_cntrler_add_form_validation_postal_code_label'), 'required');
						$this->form_validation->set_rules('phone', lang('thirds_cntrler_add_form_validation_phone_label'), 'required');
						$this->form_validation->set_rules('email', lang('thirds_cntrler_add_form_validation_email_label'), 'required');


						if ($this->form_validation->run() === TRUE) {
							$data_input = [];

							foreach ($this->input->post() as $input => $value) {
								$data_input[$input] = $value;
							}

							if ($this->input->post('type_person') == 2) {
								$data_input['name'] = $this->input->post('first_name')." ".$this->input->post('second_name')." ".$this->input->post('first_lastname')." ".$this->input->post('second_lastname');

								$data_input['company'] = $this->input->post('first_name')." ".$this->input->post('second_name')." ".$this->input->post('first_lastname')." ".$this->input->post('second_lastname');

							} else if ($this->input->post('type_person') != 2) {

								$data_input['company'] = $this->input->post('name');

							}

							if ($this->mAccountSettings->year_closed == 1 && $this->mAccountSettings->years_database_management == 2) {
								$new_config = $this->site->get_new_connection();
								if ($this->check_database($new_config)) {
									$DB2 = $this->load->database($new_config, TRUE);
									$q = $this->DB1->get_where('companies', ['id'=>$third_id]);
									$q2 = $DB2->get_where('companies', ['id'=>$third_id]);
									if ($q->num_rows() > 0 && $q2->num_rows() > 0) {
										$curr_year_data = $q->row();
										$prev_year_data = $q2->row();
										if ($curr_year_data->vat_no == $prev_year_data->vat_no) {
											$this->DB1->update('companies', $data_input, ['id' => $third_id]);
											$DB2->update('companies', $data_input, ['id' => $third_id]);
										} else {
											$this->session->set_flashdata('error', 'No se pudo editar, los datos entre años no coinciden');
											redirect('thirds/edit/'.$third_id);
										}
									} else {
										$this->session->set_flashdata('error', 'No se pudo editar, los datos entre años no coinciden');
										redirect('thirds/edit/'.$third_id);
									}
								} else {
									redirect('settings/cf');
								}
							} else {
								$this->DB1->update('companies', $data_input, ['id'=>$third_id]);
							}
							$this->session->set_flashdata('message', lang('thirds_cntrler_add_third_updated_successfully'));
							redirect('thirds');
						}
					}
					$this->render('thirds/edit');
				} else {
					$this->session->set_flashdata('error', lang('thirds_not_found_message'));
					redirect('thirds');
				}
		} else {
			$this->session->set_flashdata('error', lang('thirds_not_specified_message'));
			redirect('thirds');
		}
	}

	public function delete($third_id = NULL){
		if ($third_id != NULL) {
			$entryitems = $this->DB1->where('entryitems'.$this->DB1->dbsuffix.'.companies_id', $third_id)->get('entryitems'.$this->DB1->dbsuffix);
			if ($entryitems->num_rows() == 0) {
				$deleted = $this->DB1->delete('companies', array('id' => $third_id));
				if ($deleted) {
					$this->session->set_flashdata('message', 'Se eliminó con éxito el tercero seleccionado.');
				} else {
					$this->session->set_flashdata('error', 'Error al eliminar al tercero seleccionado.');
				}
			} else {
				$this->session->set_flashdata('error', 'No se puede eliminar al tercero seleccionado.');
			}

			redirect('thirds');

		} else {
			$this->session->set_flashdata('error', lang('thirds_not_specified_message'));
			redirect('thirds');
		}
	}

	public function get_states($country = NULL){
		if ($country != NULL) {
			$states = $this->DB1->where('states.PAIS', $country)->get('states');
			if ($states->num_rows() > 0) {

				$states = $states->result_array();

				$options = "<option value=''>Seleccione...</option>";

				foreach ($states as $row => $st) {
					$options.="<option value='".$st['DEPARTAMENTO']."' data-code='".$st['CODDEPARTAMENTO']."'>".$st['DEPARTAMENTO']."</option>";
				}

				echo $options;
			} else {
				echo "<option value=''>No se encontró departamentos</option>";
			}
		}
	}

	public function get_cities($dpto = NULL){
		if ($dpto != NULL) {
			$cities = $this->DB1->where('cities.CODDEPARTAMENTO', $dpto)->get('cities');
			if ($cities->num_rows() > 0) {

				$cities = $cities->result_array();

				$options = "<option value=''>Seleccione...</option>";

				foreach ($cities as $row => $cd) {
					$options.="<option value='".$cd['DESCRIPCION']."' data-code='".$cd['CODIGO']."'>".$cd['DESCRIPCION']."</option>";
				}

				echo $options;
			} else {
				echo "<option value=''>No se encontró ciudades</option>";
			}
		}
	}


	public function third_exists($perfil = 0, $documento = 0){
		if ($perfil != 0 && $documento != 0) {
			$third_exist = $this->DB1->where(array('group_id' => $perfil, 'vat_no' => $documento))->get('companies');
			if ($third_exist->num_rows() > 0) {
				echo "1";
			} else {
				echo "0";
			}
		}
	}

	public function delete_relation($id){

		$deleted = $this->DB1->delete('companies_opbalance'.$this->DB1->dbsuffix, array('id' => $id));

		echo $deleted;

	}


	public function get_relations($page){

		if ($page == 1) {
			$inicio = 0;
		} else {
			$inicio = ($page-1)*100;
		}

		$max_limits = 100;
		// $max_limits = 10;

		$recons = $this->DB1->limit($max_limits, $inicio)->get('companies_opbalance'.$this->DB1->dbsuffix);
		$relations_exists = false;
		if ($recons->num_rows() > 0) {
			$relations_exists = $recons->result_array();
		}

		$ledgers = $this->DB1->query('SELECT * FROM '.$this->DB1->dbprefix.'ledgers'.$this->DB1->dbsuffix)->result_array();
		$ledger_options = [];

		foreach ($ledgers as $ledger) {
			$ledger_options[$ledger['id']] = $ledger['code']." - ".ucfirst(mb_strtolower($ledger['name']));
		}

		$this->data['ledger_options'] = $ledger_options;

		$relations = '<input type="hidden" name="page" id="page" value="'.($page+1).'">';

		if (($this->pages(true) == $page) || !$relations_exists) {
			$unblock_buttons = true;
		} else {
			$unblock_buttons = false;
		}

		$relations .= '<input type="hidden" name="unblock_buttons" id="unblock_buttons" value="'.$unblock_buttons.'">';

		if ($relations_exists) {
	        foreach ($relations_exists as $row => $relation){
	          	$relations.= '
	          			<input type="hidden" name="id_relation[]" value="'. ($relation['id']) .'">
	                    <div class="relation saved id_relation_'. ($relation['id']) .'">
	                      <div class="col-md-3 form-group">
	                        <select class="ledger_select form-control ledger-dropdown ledger_id ledger_'. ($row+1) .'" data-pos="'. ($row+1) .'" name="ledger_id[]" required>';
	                          if ($ledger_options && isset($ledger_options[$relation['id_ledgers']])) {
	                          	$relations.="<option value='".($relation['id_ledgers'])."'>".($ledger_options[$relation['id_ledgers']])."</option>";
	                          }
	          	$relations.='</select>
	                      </div>
	                      <div class="col-md-3 form-group">
	                        <select class="companies_select form-control ledger-dropdown third_id third_'. ($row+1) .'" data-pos="'. ($row+1) .'" name="third[]" required>';

	                        $company = $this->DB1->get_where('companies', array('id' => $relation['id_companies']));
	                        if ($company->num_rows() > 0) {
	                        	$company = $company->row();
	                        	$relations.="<option value='".($relation['id_companies'])."'>".($company->company)."</option>";
	                        }

	            $relations.='</select>
	                      </div>';
	            $num_col = 3;
	            if ($this->mAccountSettings->cost_center == 1) {
	            $num_col = 2;
		            $relations .= '<div class="col-md-2 form-group">
		                        	<select class="cost_center_select form-control ledger-dropdown cost_center_id cost_center_'. ($row+1) .'" data-pos="'. ($row+1) .'" name="cost_center_id[]" required>
		                                <option value="">Seleccione...</option>';
	                    $cost_centers = $this->DB1->query('SELECT * FROM '.$this->DB1->dbprefix.'cost_centers'.$this->DB1->dbsuffix);
	                    if ($cost_centers->num_rows()) {
	                    	$cost_centers = $cost_centers->result_array();
	                    	foreach ($cost_centers as $cost_center) {
	                    		$relations .= '<option value="'.$cost_center['id'].'"  '.($relation['cost_center_id'] == $cost_center['id'] ? 'selected="selected"' : '').'>'.$cost_center['name'].'</option>';
	                    	}
	                    }

	                    $relations .='</select>
		            			   </div>';
	            }


	            $relations.='<div class="col-md-'.$num_col.' form-group">
	                        <input type="number" name="op_balance[]" class="form-control op_balance" value="'. ($relation['op_balance']) .'" step="00.01" min="0" required>
	                      </div>
	                      <div class="col-md-'.$num_col.' form-group" style="display: inline-flex;">
	                          <div style="width: 90%;">
	                            <select class="form-control" name="op_balance_dc[]" required>
	                                <option value="">Seleccione...</option>
	                                <option value="C" '. ((($relation['op_balance_dc'] == "C")) ? 'selected="selected"':'').'>C</option>
	                                <option value="D" '. ((($relation['op_balance_dc'] == "D")) ? 'selected="selected"':'').'>D</option>
	                            </select>
	                          </div>
	                          <div style="width: 10%;">
	                            <button type="button" class="btn btn-sm btn-outline btn-danger" data-relationid="'. ($relation['id']) .'" data-toggle="modal" data-target="#modal_delete_relation_saved"><span class="fa fa-trash"></span></button>
	                          </div>
	                      </div>
	                      <div class="col-md-12 error_msg error_'. ($row+1) .'" style="display: none;">
	                        <em><i class="fa fa-times"></i> '. (lang('thirds_relations_repeated_validation')) .'</em>
	                      </div>
	                    </div>';
	        }
		} else {
			// $relations .= '<div class="col-md-12 text-center" style="padding-bottom:5%;"><b class="text-danger font-italic">Sin registros</b></div>';
		}

        echo $relations;

	}

	public function pages($return = false){
		$recons = $this->DB1->get('companies_opbalance'.$this->DB1->dbsuffix);
		$num = $recons->num_rows();
		$paginas = $num / 100;
		if ($return) {
			return ceil($paginas);
		} else {
	    	echo ceil($paginas);
		}
	}

	public function import() {
		
        if (isset($_FILES['entriesxls'])) {
		    $this->load->library('excel');
	        // Cargar el archivo Excel
	        $filePath = $_FILES['entriesxls']['tmp_name'];
	        $objPHPExcel = PHPExcel_IOFactory::load($filePath);
            $hojas = $objPHPExcel->getWorksheetIterator();
	        $import_entries = [];
	        $error = false;
	        $error_msg = "";
	        $import_date = date('Y-m-d H:i:s');
	        $entry_types = [];
            foreach ($hojas as $key => $hoja) {
            	if ($key > 0) {
            		continue;
            	}
                $data = $hoja->toArray();
                unset($data[0]);
	        	$linea = 1;
                foreach ($data as $data_row) {
                    if (empty($data_row[0])) {
                        continue;
                    }
                    $column = 0;
                    $third_type = [
                    			"Cliente" => "customer",
                    			"Proveedor" => "supplier",
                    			"Vendedor" => "seller",
                    			"Acreedor" => "creditor",
                    			"Empleado" => "employee",
                    				];
                    $document_type = [
                    			"Registro Civil" => "civil_registry",
								"Tarjeta Identidad" => "identity_card",
								"Cedula de ciudadanía" => "citizenship_card",
								"Tarjeta de inmigración" => "immigration_card",
								"Cedula de extranjería" => "immigration_id",
								"Nit" => "nit",
								"Pasaporte" => "passport",
								"Documento de identificación extranjero" => "foreign_identification_document",
								"NUIP" => "nuip",
								"Permiso especial de permanencia" => "special_residence_permit",
                    				];
                    $Tipo_tercero = $data_row[$column];$column++;
					$Primer_Nombre = $data_row[$column];$column++;
					$Segundo_Nombre = $data_row[$column];$column++;
					$Primer_Apellido = $data_row[$column];$column++;
					$Segundo_Apellido = $data_row[$column];$column++;
					$Nombre_comercial = $data_row[$column];$column++;
					$Tipo_de_documento = $data_row[$column];$column++;
					$Numero_de_documento = $data_row[$column];$column++;
					$Digito_Verificacion = $data_row[$column];$column++;
					$Direccion = $data_row[$column];$column++;
					$Correo = $data_row[$column];$column++;
					$Pais = $data_row[$column];$column++;
					$Departamento = $data_row[$column];$column++;
					$Ciudad = $data_row[$column];$column++;

                    $error_msg.= ($error_msg != "" ? "\n</br>  " : "" )." Fila No ".$linea." : " ;
					if (
						$Tipo_tercero == "" ||
						$Tipo_de_documento == "" ||
						$Numero_de_documento == "" ||
						$Direccion == "" ||
						$Correo == "" ||
						$Pais == "" ||
						$Departamento == "" ||
						$Ciudad == "" ||
						($Nombre_comercial == "" && (
								$Primer_Nombre == "" || 
								$Segundo_Nombre == "" || 
								$Primer_Apellido == "" || 
								$Segundo_Apellido == ""
							))
						) {
                    	$error = true;
                    	$error_msg .= "Verifique que los campos obligatorios marcados con (*) estén diligenciados;";
                    	continue;
					}

                    $third_type_data = $this->site->get_third_type($third_type[$Tipo_tercero]);
                    if (!$third_type_data) {
                    	$error = true;
                    	$error_msg .= "El tipo de tercero ".$Tipo_tercero." no existe en el sistema;";
                    }
                    $document_type_data = $this->site->get_document_type($document_type[$Tipo_de_documento]);
                    if (!$document_type_data) {
                    	$error = true;
                    	$error_msg .= "El tipo de documento ".$Tipo_de_documento." no existe en el sistema;";
                    }
                    $country = $this->site->get_country($Pais);
                    if (!$country) {
                    	$error = true;
                    	$error_msg .= "El país ".$Pais." no existe en el sistema;";
                    }
                    $state = $this->site->get_state($Departamento);
                    if (!$state) {
                    	$error = true;
                    	$error_msg .= "El departamento ".$Departamento." no existe en el sistema;";
                    }
                    $city = $this->site->get_city($Ciudad);
                    if (!$city) {
                    	$error = true;
                    	$error_msg .= "La ciudad ".$Ciudad." no existe en el sistema;";
                    }

                    if ($this->site->validate_third_exists($Numero_de_documento, $third_type[$Tipo_tercero])) {
                    	$error = true;
                    	$error_msg .= "Ya existe un tercero con mismo numero de documento y tipo de tercero en el sistema;";
                    }

                    if (!$error) {
                    	$item_arr = 
	                    	[
								'group_id' => $third_type_data->id,
								'group_name' => $third_type[$Tipo_tercero],
								'first_name' => $Primer_Nombre ? $Primer_Nombre : " ",
								'second_name' => $Segundo_Nombre ? $Segundo_Nombre : " ",
								'first_lastname' => $Primer_Apellido ? $Primer_Apellido : " ",
								'second_lastname' => $Segundo_Apellido ? $Segundo_Apellido : " ",
								'company' => $Nombre_comercial ? $Nombre_comercial : " ",
								'tipo_documento' => $document_type_data->id,
								'vat_no' => $Numero_de_documento,
								'digito_verificacion' => $Digito_Verificacion,
								'address' => $Direccion,
								'email' => $Correo,
								'country' => $Pais,
								'state' => $Departamento,
								'city' => $Ciudad,
								'name' => $Primer_Nombre." ".($Segundo_Nombre ? $Segundo_Nombre : " ")." ".$Primer_Apellido." ".$Segundo_Apellido,
								'document_code' => $document_type_data->codigo_doc,
	                    	];
	                    $import_companies[] = $item_arr;
                    }
                    $linea++;
                }
                if ($linea == 1) {
                	$error = true;
	        		$error_msg .= "Archivo sin datos diligenciados";
                }
            }
            if ($error) {
				$this->data['title'] = lang('thirds_cntrler_import_h3_title_1');
				$this->data['error_msg'] = $error_msg;
				$this->render('thirds/import');
            } else {
            	$insert_count = 0;
            	foreach ($import_companies as $company) {
	            	$this->DB1->insert('companies', $company);
	            	$insert_count++;
	            }
				$this->data['title'] = lang('thirds_cntrler_import_h3_title_1');
				$this->data['success_msg'] = 'Se insertaron '.$insert_count.' terceros correctamente';
				$this->render('thirds/import');
            }
		} else {
			$this->data['title'] = lang('thirds_cntrler_import_h3_title_1');
			$this->render('thirds/import');
		}
	}

}




 ?>