<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reports extends Admin_Controller {
	public $row = 3;

	public function __construct()
    {
        parent::__construct();
        $this->mBodyClass .= ' sidebar-collapse';
        $this->prefijodbpos = $this->DB1->dbprefix;
        $this->load->model('reports_model');
        $this->load->model('settings_model');
        $this->load->model('ledger_model');
        $this->load->model("entryTypes_model");
    }

	public function index() {
		redirect($_SERVER['HTTP_REFERER']);
	}

/**
 * balancesheet method
 *
 * @return void
 */
	public function balancesheet($download = NULL, $format = NULL, $datos = NULL) { //Se añade 3er variable para recibir los datos de la URL en exportar (PDF o XLS).
		// set page title
		if ($this->mAccountSettings->cost_center) {
			if ($this->user_cost_centers) {
				foreach ($this->user_cost_centers as $key => $value) {
					if ($key == 0) {
						$this->DB1->where('id', $value);
					} else {
						$this->DB1->or_where('id', $value);
					}
				}
			}
			$cost_centers_data = $this->DB1->get('cost_centers'.$this->DB1->dbsuffix);
			if ($cost_centers_data->num_rows() > 0) {
				$cost_centers_data = $cost_centers_data->result_array();
				foreach ($cost_centers_data as $row => $cost_center_data) {
					$cost_centers[] = $cost_center_data;
				}
			} else {
				$cost_centers = false;
			}
			$this->data['cost_centers'] = $cost_centers;
		}
	
		if ($this->input->method() == 'post') { //Si se realizó filtro de búsqueda

			
			$only_opening = false;
			$this->skip_entries_disapproved = false;
			$cost_center = null;

			$this->data['options'] = true;
			if (!empty($this->input->post('opening'))) {
				$only_opening = true;
				/* Sub-title*/
				$this->data['subtitle'] = sprintf(lang('balance_sheet_until'), $this->functionscore->dateFromSql(date('Y-m-d')));
			} else {
				if ($this->input->post('hidezero')) {
					$this->hidezero = $this->input->post('hidezero');
				}
				if ($this->input->post('nivelBalancesheet')) {
					$this->nivelBalancesheet = $this->input->post('nivelBalancesheet');
				}
				if ($this->input->post('skip_entries_disapproved')) {
					$this->skip_entries_disapproved = $this->input->post('skip_entries_disapproved');
				}
				if ($this->input->post('cost_center')) {
					$cost_center = $this->input->post('cost_center');
				}
				if ($this->input->post('enddate')) {
					$enddate = $this->functionscore->dateToSQL($this->input->post('enddate'));
					if ($enddate < $this->mAccountSettings->fy_start || $enddate > $this->mAccountSettings->fy_end) {
						$enddate = $this->mAccountSettings->fy_end;
						$this->session->set_flashdata('error', lang('balance_sheet_invalid_end_date_fy_start'));
						$this->data['subtitle'] = sprintf(lang('balance_sheet_until'), $this->functionscore->dateFromSql($this->functionscore->dateToSQL(date('Y-M-d'))));
					} else {
						$this->data['subtitle'] = sprintf(lang('balance_sheet_until'), $this->functionscore->dateFromSql($this->functionscore->dateToSQL($this->input->post('enddate'))));
					}
				}
			}

			$this->mPageTitle = lang('page_title_reports_balancesheet');
			$this->data['title'] = lang('page_title_reports_balancesheet');
			$startdate = $this->mAccountSettings->fy_start;
			$enddate = isset($enddate) && $enddate ? $enddate : $this->mAccountSettings->fy_end;

			/**********************************************************************/
			/*********************** BALANCESHEET CALCULATIONS ********************/
			/**********************************************************************/
			 //Array donde se almacenaran los códigos de cada grupo con su nivel, estructura [nivel][num autoincrementable]
			$this->load->library('AccountList');
			/* Liabilities */
			$account_list = new AccountList();
			$account_list->Group = &$this->Group;
			$account_list->Ledger = &$this->Ledger;
			$account_list->only_opening = $only_opening;
			$account_list->start_date = $startdate;
			$account_list->end_date = $enddate;
			$account_list->cost_center = $cost_center;
			$account_list->skip_entries_disapproved = $this->skip_entries_disapproved;
			$account_list->affects_gross = -1;
			$account_list->start();

			// $this->functionscore->print_arrays($liabilities);
			$liabilities = $account_list->buildTree($account_list->result_account_list, '0_G', 2, 1);
			$bsheet['liabilities'] = $liabilities;
			$bsheet['liabilities_total'] = ((isset($liabilities[0]['cl_total']) ? $liabilities[0]['cl_total'] : 0) * -1);

			$liabilities2 = $account_list->buildTree($account_list->result_account_list, '0_G', 3, 1);
			$bsheet['liabilities2'] = $liabilities2;
			$bsheet['liabilities2_total'] = ((isset($liabilities2[0]['cl_total']) ? $liabilities2[0]['cl_total'] : 0) * -1);
			$bsheet['liabilities_total'] = $this->functionscore->calculate($bsheet['liabilities_total'], $bsheet['liabilities2_total'], '+'); /* Para no afectar otras funcionalidades, se suman, el total del pasivo 1 con el total del pasivo 2 a la variable ya existente, liabilities_total */

			$assets = $account_list->buildTree($account_list->result_account_list, '0_G', 1, 1);
			$bsheet['assets'] = $assets;
			$bsheet['assets_total'] = (isset($assets[0]['cl_total']) ? $assets[0]['cl_total'] : 0);

			$operationalcosts = $account_list->buildTree($account_list->result_account_list, '0_G', 8, 1);
			$bsheet['operationalcosts'] = $operationalcosts;
			$bsheet['operationalcosts_total'] = (isset($operationalcosts[0]['cl_total']) ? $operationalcosts[0]['cl_total'] : 0);

			$operationalcosts2 = $account_list->buildTree($account_list->result_account_list, '0_G', 9, 1);
			$bsheet['operationalcosts2'] = $operationalcosts2;
			$bsheet['operationalcosts2_total'] = ((isset($operationalcosts2[0]['cl_total']) ? $operationalcosts2[0]['cl_total'] : 0) * -1);

			/* Fin Costos operacionales */
			$bsheet['niveles'] = $this->niveles;
			/* Profit and loss calculations */
			$income = $account_list->buildTree($account_list->result_account_list, '0_G', 4, 1);
			$income_total = (isset($income[0]['cl_total']) ? $income[0]['cl_total'] : 0);
			$expense = $account_list->buildTree($account_list->result_account_list, '0_G', 5, 1);
			$expense_total = (isset($expense[0]['cl_total']) ? $expense[0]['cl_total'] : 0);
			$expense2 = $account_list->buildTree($account_list->result_account_list, '0_G', 6, 1);
			$expense2_total = (isset($expense2[0]['cl_total']) ? $expense2[0]['cl_total'] : 0);
			$expense3 = $account_list->buildTree($account_list->result_account_list, '0_G', 7, 1);
			$expense3_total = (isset($expense3[0]['cl_total']) ? $expense3[0]['cl_total'] : 0);
			$expense_total = ($expense_total + $expense2_total + $expense3_total);
			$bsheet['pandl'] = (($income_total + $expense_total)) * -1;
			/* Difference in opening balance */
			$bsheet['opdiff'] = $this->ledger_model->getOpeningDiff();
			if ($this->functionscore->calculate($bsheet['opdiff']['opdiff_balance'], 0, '==')) {
				$bsheet['is_opdiff'] = false;
			} else {
				$bsheet['is_opdiff'] = true;
			}
			/**** Final balancesheet total ****/
			$bsheet['final_liabilities_total'] = $bsheet['liabilities_total'];
			$bsheet['final_assets_total'] = $bsheet['assets_total'];
			/* If net profit add to liabilities, if net loss add to assets */
			if ($this->functionscore->calculate($bsheet['pandl'], 0, '>=')) {
				$bsheet['final_liabilities_total'] = $this->functionscore->calculate(
					$bsheet['final_liabilities_total'],
					$bsheet['pandl'], '+');
			} else {
				$positive_pandl = $this->functionscore->calculate($bsheet['pandl'], 0, 'n');
				$bsheet['final_assets_total'] = $this->functionscore->calculate(
					$bsheet['final_assets_total'],
					$positive_pandl, '+');
			}
			/**
			 * If difference in opening balance is Dr then subtract from
			 * assets else subtract from liabilities
			 */
			if ($bsheet['is_opdiff']) {
				if ($bsheet['opdiff']['opdiff_balance_dc'] == 'D') {
					$bsheet['final_assets_total'] = $this->functionscore->calculate(
						$bsheet['final_assets_total'],
						$bsheet['opdiff']['opdiff_balance'], '+');
				} else {
					$bsheet['final_liabilities_total'] = $this->functionscore->calculate(
						$bsheet['final_liabilities_total'],
						$bsheet['opdiff']['opdiff_balance'], '+');
				}
			}
			$this->data['bsheet'] = $bsheet;
			
			if ($download === 'download') {
				if ($format=='pdf') {
	                $this->data['settings'] = $this->mAccountSettings;
	                $this->data['POST'] = $_POST;
	                $this->data['descargar'] = 1;
	                $this->load->view('reports/pdf/balancesheet', $this->data);
	            }
	            if ($format=='pdfprint') {
	                $this->data['settings'] = $this->mAccountSettings;
	                $this->data['POST'] = $_POST;
	                $this->data['descargar'] = 0;
	                $this->load->view('reports/pdf/balancesheet', $this->data);
	            }
	            if ($format=='xls') {
	            	$filename = lang('page_title_reports_balancesheet');
					$this->load->library('excel');
		            $this->excel->setActiveSheetIndex(0);
		            $styleArray = array(
		                'borders' => array(
		                    'allborders' => array(
		                        'style' => PHPExcel_Style_Border::BORDER_THIN
		                    )
		                )
		            );
		            $this->excel->getDefaultStyle()->applyFromArray($styleArray);
					$this->excel->getActiveSheet()->setTitle('balancesheet');
		            $this->excel->getActiveSheet()->SetCellValue('A1', $this->data['subtitle']);
		            $this->excel->getActiveSheet()->mergeCells('A1:C1');
		            $this->excel->getActiveSheet()->SetCellValue('A2', lang('balance_sheet_assets'));
		            $this->excel->getActiveSheet()->SetCellValue('B2', lang('entries_views_edit_items_th_drcr'));
		            $this->excel->getActiveSheet()->SetCellValue('C2', lang('search_views_legend_amount') . ' (' . $this->mAccountSettings->currency_symbol . ')');
		            $this->account_st_short($bsheet['assets'], $c = -1, $this, 'D');
		            $this->excel->getActiveSheet()->SetCellValue('A'.($this->row), lang('balance_sheet_loe'));
		            $this->excel->getActiveSheet()->SetCellValue('B'.($this->row), lang('entries_views_edit_items_th_drcr'));
		            $this->excel->getActiveSheet()->SetCellValue('C'.($this->row), lang('search_views_legend_amount') . ' (' . $this->mAccountSettings->currency_symbol . ')');
		            $this->row++;
		            $this->account_st_short($bsheet['liabilities'], $c = -1, $this, 'C');
		            $this->row++;
		            $this->account_st_short($bsheet['liabilities2'], $c = -1, $this, 'C');
		            $this->row++;
		            if ($this->functionscore->calculate($bsheet['assets_total'], 0, '>=')) {
		            	$this->excel->getActiveSheet()->SetCellValue('A'.($this->row), lang('balance_sheet_total_assets'));
			    		//Obtenemos el monto
			    		$monto = $this->functionscore->toCurrency('D', $bsheet['assets_total']);
			    		$montDC = $this->functionscore->drawAmount($monto);
			    		$prefijo = $montDC[0];
			    		$monto = $montDC[1];
			    		// DR/CR
			    		$this->excel->getActiveSheet()->SetCellValue('B'.($this->row), $prefijo);
						// Amount in Account
			    		$this->excel->getActiveSheet()->SetCellValue('C'.($this->row), $monto);
			    		$this->excel->getActiveSheet()->getStyle('C'.$this->row)->getNumberFormat()->setFormatCode("#,##0.00");
					} else {
						$this->excel->getActiveSheet()->SetCellValue('A'.($this->row), lang('balance_sheet_total_assets'));
			    		//Obtenemos el monto
			    		$monto = $this->functionscore->toCurrency('D', $bsheet['assets_total']);
			    		$montDC = $this->functionscore->drawAmount($monto);
			    		$prefijo = $montDC[0];
			    		$monto = $montDC[1];
			    		// DR/CR
			    		$this->excel->getActiveSheet()->SetCellValue('B'.($this->row), $prefijo);
						// Amount in Account
			    		$this->excel->getActiveSheet()->SetCellValue('C'.($this->row), $monto);
			    		$this->excel->getActiveSheet()->getStyle('C'.$this->row)->getNumberFormat()->setFormatCode("#,##0.00");
		            	$header = 'A'.$this->row.':C'.$this->row;
					    $this->excel->getActiveSheet()->getStyle($header)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('ff3300');
					    $style = array(
					        'font' => array('bold' => true,),
					    );
					    $this->excel->getActiveSheet()->getStyle($header)->applyFromArray($style);
					}
		            $this->row++;
					$header = 'A'.$this->row.':C'.$this->row;
		            $style = array(
				        'font' => array('bold' => true,),
				    );
				    $this->excel->getActiveSheet()->getStyle($header)->applyFromArray($style);
					if ($this->functionscore->calculate($bsheet['pandl'], 0, '>=')) {
						$this->excel->getActiveSheet()->SetCellValue('A'.($this->row), ' ');
						$this->excel->getActiveSheet()->SetCellValue('B'.($this->row), ' ');
					} else {
						$this->excel->getActiveSheet()->SetCellValue('A'.($this->row), lang('balance_sheet_net_loss'));
						$positive_pandl = $this->functionscore->calculate($bsheet['pandl'], 0, 'n');
						//Obtenemos el monto
			    		$monto = $this->functionscore->toCurrency('D', $positive_pandl);
			    		$montDC = $this->functionscore->drawAmount($monto);
			    		$prefijo = $montDC[0];
			    		$monto = $montDC[1];
			    		// DR/CR
			    		$this->excel->getActiveSheet()->SetCellValue('B'.($this->row), $prefijo);
						// Amount in Account
			    		$this->excel->getActiveSheet()->SetCellValue('C'.($this->row), $monto);
			    		$this->excel->getActiveSheet()->getStyle('C'.$this->row)->getNumberFormat()->setFormatCode("#,##0.00");
					}
					if ($bsheet['is_opdiff']) {
		            	$this->row++;
						$header = 'A'.$this->row.':C'.$this->row;
					    $this->excel->getActiveSheet()->getStyle($header)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('ff3300');
					    $style = array(
					        'font' => array('bold' => true,),
					    );
					    $this->excel->getActiveSheet()->getStyle($header)->applyFromArray($style);
						/* If diff in opening balance is Dr */
						if ($bsheet['opdiff']['opdiff_balance_dc'] == 'D') {
							$this->excel->getActiveSheet()->SetCellValue('A'.($this->row), lang('balance_sheet_diff_opp'));
							//Obtenemos el monto
				    		$monto =  $this->functionscore->toCurrency('D', $bsheet['opdiff']['opdiff_balance']);
				    		$montDC = $this->functionscore->drawAmount($monto);
				    		$prefijo = $montDC[0];
				    		$monto = $montDC[1];
				    		// DR/CR
				    		$this->excel->getActiveSheet()->SetCellValue('B'.($this->row), $prefijo);
							// Amount in Account
				    		$this->excel->getActiveSheet()->SetCellValue('C'.($this->row), $monto);
				    		$this->excel->getActiveSheet()->getStyle('C'.$this->row)->getNumberFormat()->setFormatCode("#,##0.00");
						} else {
							$this->excel->getActiveSheet()->SetCellValue('A'.($this->row), ' ');
							$this->excel->getActiveSheet()->SetCellValue('B'.($this->row), ' ');
						}
					}
		            $this->row++;
		            if ($this->functionscore->calculate($bsheet['final_liabilities_total'], $bsheet['final_assets_total'], '==')) {
		            	// Add Backgorund Grey

						$header = 'A'.$this->row.':C'.$this->row;
					    $this->excel->getActiveSheet()->getStyle($header)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('cccdd2');

		            	$style = array(
					        'font' => array('bold' => true,),
					    );
					    $this->excel->getActiveSheet()->getStyle($header)->applyFromArray($style);
					} else {
						$header = 'A'.$this->row.':C'.$this->row;
					    $this->excel->getActiveSheet()->getStyle($header)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('ff3300');
					    $style = array(
					        'font' => array('bold' => true,),
					    );
					    $this->excel->getActiveSheet()->getStyle($header)->applyFromArray($style);
					}
					$this->excel->getActiveSheet()->SetCellValue('A'.($this->row), lang('balance_sheet_total'));
					//Obtenemos el monto
		    		$monto =  $this->functionscore->toCurrency('D', $bsheet['final_assets_total']);
		    		$montDC = $this->functionscore->drawAmount($monto);
		    		$prefijo = $montDC[0];
		    		$monto = $montDC[1];
		    		// DR/CR
		    		$this->excel->getActiveSheet()->SetCellValue('B'.($this->row), $prefijo);
					// Amount in Account
		    		$this->excel->getActiveSheet()->SetCellValue('C'.($this->row), $monto);
		    		$this->excel->getActiveSheet()->getStyle('C'.$this->row)->getNumberFormat()->setFormatCode("#,##0.00");
		            $this->row++;
		            $this->row++;
					$header = 'A'.$this->row.':C'.$this->row;
		            $style = array(
				        'font' => array('bold' => true,),
				    );
				    $this->excel->getActiveSheet()->getStyle($header)->applyFromArray($style);
					if ($this->functionscore->calculate($bsheet['liabilities_total'], 0, '>=')) {
		            	$this->excel->getActiveSheet()->SetCellValue('A'.($this->row), lang('balance_sheet_tloe'));
		            	//Obtenemos el monto
			    		$monto =  $this->functionscore->toCurrency('C', $bsheet['liabilities_total']);
			    		$montDC = $this->functionscore->drawAmount($monto);
			    		$prefijo = $montDC[0];
			    		$monto = $montDC[1];
			    		// DR/CR
			    		$this->excel->getActiveSheet()->SetCellValue('B'.($this->row), $prefijo);
						// Amount in Account
			    		$this->excel->getActiveSheet()->SetCellValue('C'.($this->row), $monto);
			    		$this->excel->getActiveSheet()->getStyle('C'.$this->row)->getNumberFormat()->setFormatCode("#,##0.00");
					} else {
						$this->excel->getActiveSheet()->SetCellValue('A'.($this->row), lang('balance_sheet_tloe'));
		            	//Obtenemos el monto
			    		$monto =  $this->functionscore->toCurrency('C', $bsheet['liabilities_total']);
			    		$montDC = $this->functionscore->drawAmount($monto);
			    		$prefijo = $montDC[0];
			    		$monto = $montDC[1];
			    		// DR/CR
			    		$this->excel->getActiveSheet()->SetCellValue('B'.($this->row), $prefijo);
						// Amount in Account
			    		$this->excel->getActiveSheet()->SetCellValue('C'.($this->row), $monto);
			    		$this->excel->getActiveSheet()->getStyle('C'.$this->row)->getNumberFormat()->setFormatCode("#,##0.00");

		            	$header = 'A'.$this->row.':C'.$this->row;
					    $this->excel->getActiveSheet()->getStyle($header)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('ff3300');
					    $style = array(
					        'font' => array('bold' => true,),
					    );
					    $this->excel->getActiveSheet()->getStyle($header)->applyFromArray($style);
					}
		            $this->row++;
					$header = 'A'.$this->row.':C'.$this->row;
		            $style = array(
				        'font' => array('bold' => true,),
				    );
				    $this->excel->getActiveSheet()->getStyle($header)->applyFromArray($style);
					if ($this->functionscore->calculate($bsheet['pandl'], 0, '>=')) {
						$this->excel->getActiveSheet()->SetCellValue('A'.($this->row), lang('balance_sheet_net_profit'));
						//Obtenemos el monto
			    		$monto =  $this->functionscore->toCurrency('C', $bsheet['pandl']);
			    		$montDC = $this->functionscore->drawAmount($monto);
			    		$prefijo = $montDC[0];
			    		$monto = $montDC[1];
			    		// DR/CR
			    		$this->excel->getActiveSheet()->SetCellValue('B'.($this->row), $prefijo);
						// Amount in Account
			    		$this->excel->getActiveSheet()->SetCellValue('C'.($this->row), $monto);

			    		$this->excel->getActiveSheet()->getStyle('C'.$this->row)->getNumberFormat()->setFormatCode("#,##0.00");
					} else {
						$this->excel->getActiveSheet()->SetCellValue('A'.($this->row), ' ');
						$this->excel->getActiveSheet()->SetCellValue('B'.($this->row), ' ');
					}
					if ($bsheet['is_opdiff']) {
		            	$this->row++;
						$header = 'A'.$this->row.':C'.$this->row;
					    $this->excel->getActiveSheet()->getStyle($header)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('ff3300');
					    $style = array(
					        'font' => array('bold' => true,),
					    );
					    $this->excel->getActiveSheet()->getStyle($header)->applyFromArray($style);
						/* If diff in opening balance is Dr */
						if ($bsheet['opdiff']['opdiff_balance_dc'] == 'C') {
							$this->excel->getActiveSheet()->SetCellValue('A'.($this->row), lang('balance_sheet_diff_opp'));
							//Obtenemos el monto
				    		$monto =  $this->functionscore->toCurrency('C', $bsheet['opdiff']['opdiff_balance']);
				    		$montDC = $this->functionscore->drawAmount($monto);
				    		$prefijo = $montDC[0];
				    		$monto = $montDC[1];
				    		// DR/CR
				    		$this->excel->getActiveSheet()->SetCellValue('B'.($this->row), $prefijo);
							// Amount in Account
				    		$this->excel->getActiveSheet()->SetCellValue('C'.($this->row), $monto);

				    		$this->excel->getActiveSheet()->getStyle('C'.$this->row)->getNumberFormat()->setFormatCode("#,##0.00");
						} else {
							$this->excel->getActiveSheet()->SetCellValue('A'.($this->row), ' ');
							$this->excel->getActiveSheet()->SetCellValue('B'.($this->row), ' ');
						}
					}
		        	$this->row++;
					if ($this->functionscore->calculate($bsheet['final_liabilities_total'],
						$bsheet['final_assets_total'], '==')) {
						$header = 'A'.$this->row.':C'.$this->row;
					    $this->excel->getActiveSheet()->getStyle($header)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('cccdd2');

						$style = array(
					        'font' => array('bold' => true,),
					    );
					    $this->excel->getActiveSheet()->getStyle($header)->applyFromArray($style);
						// ADD GREY BG
					} else {
						$header = 'A'.$this->row.':C'.$this->row;
					    $this->excel->getActiveSheet()->getStyle($header)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('ff3300');
					    $style = array(
					        'font' => array('bold' => true,),
					    );
					    $this->excel->getActiveSheet()->getStyle($header)->applyFromArray($style);
					}
					$this->excel->getActiveSheet()->SetCellValue('A'.($this->row), lang('balance_sheet_total'));
					//Obtenemos el monto
		    		$monto =  $this->functionscore->toCurrency('C', $bsheet['final_liabilities_total']);
		    		$montDC = $this->functionscore->drawAmount($monto);
		    		$prefijo = $montDC[0];
		    		$monto = $montDC[1];
		    		// DR/CR
		    		$this->excel->getActiveSheet()->SetCellValue('B'.($this->row), $prefijo);
					// Amount in Account
		    		$this->excel->getActiveSheet()->SetCellValue('C'.($this->row), $monto);
		    		$this->excel->getActiveSheet()->getStyle('C'.$this->row)->getNumberFormat()->setFormatCode("#,##0.00");
					/* COSTOS OPERACIONALES */
					$this->row++;
					$this->row++;
					$this->excel->getActiveSheet()->SetCellValue('A'.($this->row), lang('balance_sheet_title_operational_costs'));
		            $this->excel->getActiveSheet()->mergeCells('A'.($this->row).':B'.($this->row));
		            $header = 'A'.$this->row.':C'.$this->row;
				    $this->excel->getActiveSheet()->getStyle($header)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('cccdd2');
					$style = array(
				        'font' => array('bold' => true,),
				    );
				    $this->excel->getActiveSheet()->getStyle($header)->applyFromArray($style);
				    $this->row++;
					$this->row++;
					$this->account_st_short($bsheet['operationalcosts'], $c = -1, $this, 'D');
					$this->row++;
					$this->account_st_short($bsheet['operationalcosts2'], $c = -1, $this, 'C');
					$this->row++;
					$this->excel->getActiveSheet()->SetCellValue('A'.($this->row), lang('balance_sheet_total_operational_cost'));
					$this->excel->getActiveSheet()->SetCellValue('B'.($this->row), $this->functionscore->toCurrency('D', $bsheet['operationalcosts_total']));
					$this->row++;
					$this->row++;
					$this->excel->getActiveSheet()->SetCellValue('A'.($this->row), lang('balance_sheet_total_operational_cost_2'));
					$this->excel->getActiveSheet()->SetCellValue('B'.($this->row), $this->functionscore->toCurrency('C', $bsheet['operationalcosts2_total']));
					/* COSTOS OPERACIONALES */
					$this->excel->getActiveSheet()->getStyle('C1:C'.$this->row)->getNumberFormat()->setFormatCode("#,##0.00");
		            $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(80);
		            $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
		            $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
	                header('Content-Type: application/vnd.ms-excel');
	                header('Content-Disposition: attachment;filename="' . $filename . '.xls"');
	                header('Cache-Control: max-age=0');
	                $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
	                $objWriter->save('php://output');
	                exit();
	            }
			}
		} else {
			$this->data['subtitle'] = sprintf(lang('balance_sheet_until'), $this->functionscore->dateFromSql(date('Y-m-d')));
		}
		if (!$download) {
			$this->render('reports/balancesheet');
		}
	}

	function account_st_short($account, $c, $THIS, $dc_type) {
		$account = isset($account[0]) ? $account[0] : $account;
		if (empty($account)) {
			return true;
		}
		$bandera = 0;
		if (isset($this->nivelBalancesheet) && $this->nivelBalancesheet != "") {
			for ($i=1; $i <= $this->nivelBalancesheet ; $i++) {
				if (in_array($account['code'], $this->niveles[$i])) {
					$bandera++;
				}
			}
		} else {
			$this->nivelBalancesheet = "";
			$bandera++;
		}
		$clBandera = $this->bandera_closing_balance($account);
		$counter = $c;
		if (!isset($account['code'])) {
			exit(var_dump($account));
		}
		if (strlen($account['code']) > 0 && $bandera != 0 && ((isset($this->hidezero)  && $clBandera != 0) || !isset($this->hidezero))) {
			if (($dc_type == 'D' && $account['cl_total'] < 0 || $dc_type == 'C' && $account['cl_total'] > 0)) {
				$header = 'A'.$this->row.':C'.$this->row;
			    $this->excel->getActiveSheet()->getStyle($header)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('ff3300');
			    $style = array(
			        'font' => array('bold' => true,),
			    );
			    $this->excel->getActiveSheet()->getStyle($header)->applyFromArray($style);
			}
			// Name of Account
    		$this->excel->getActiveSheet()->SetCellValue('A'.$this->row, $this->functionscore->toCodeWithName($account['code'], $account['name']));
    		//Obtenemos el monto
    		$monto = $this->functionscore->toCurrency(($account['cl_total'] < 0 ? 'C' : 'D'), ($account['cl_total'] < 0 ? $account['cl_total'] * -1 : $account['cl_total']));
    		$montDC = $this->functionscore->drawAmount($monto);
    		$prefijo = $montDC[0];
    		$monto = $montDC[1];
    		// DR/CR
    		$this->excel->getActiveSheet()->SetCellValue('B'.($this->row), $prefijo);
			// Amount in Account
    		$this->excel->getActiveSheet()->SetCellValue('C'.($this->row), $monto);
    		$this->excel->getActiveSheet()->getStyle('C'.$this->row)->getNumberFormat()->setFormatCode("#,##0.00");
			$this->row++;
		}
		if (isset($account['children_ledgers'])) {
			foreach ($account['children_groups'] as $id => $data) {
				$counter++;
				$this->account_st_short($data, $counter, $THIS, $dc_type);
				$counter--;
			}
		}
			
		if (isset($account['children_ledgers']) && count($account['children_ledgers']) > 0 && $this->nivelBalancesheet == "") {
			$counter++;
			foreach ($account['children_ledgers'] as $id => $data) {
				if ((isset($this->hidezero) && ($data['cl_total'] != 0 || $data['cr_total'] > 0 || $data['dr_total'] > 0)) || !isset($this->hidezero)) {
					if (($dc_type == 'D' && $data['cl_total'] < 0 || $dc_type == 'C' && $data['cl_total'] > 0)) {
						$header = 'A'.$this->row.':C'.$this->row;
					    $this->excel->getActiveSheet()->getStyle($header)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('ff3300');
					    $style = array(
					        'font' => array('bold' => true,),
					    );
					    $this->excel->getActiveSheet()->getStyle($header)->applyFromArray($style);
					}
					// Name of Account
		    		$this->excel->getActiveSheet()->SetCellValue('A'.$this->row, $this->functionscore->toCodeWithName($data['code'], $data['name']));
		    		//Obtenemos el monto
		    		$monto = $this->functionscore->toCurrency(($data['cl_total'] < 0 ? 'C' : 'D'), ($data['cl_total'] < 0 ? $data['cl_total'] * -1 : $data['cl_total']));
		    		$montDC = $this->functionscore->drawAmount($monto);
		    		$prefijo = $montDC[0];
		    		$monto = $montDC[1];
		    		// DR/CR
		    		$this->excel->getActiveSheet()->SetCellValue('B'.($this->row), $prefijo);
					// Amount in Account
		    		$this->excel->getActiveSheet()->SetCellValue('C'.($this->row), $monto);

		    		$this->excel->getActiveSheet()->getStyle('C'.$this->row)->getNumberFormat()->setFormatCode("#,##0.00");

					$this->row++;
				}
			}
			$counter--;
		}
		$header = 'C3:C'.$this->row;
			    $style = array(
			        'alignment' => array(
			            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
			        )
		);
		$this->excel->getActiveSheet()->getStyle($header)->applyFromArray($style);
	}

	function print_space($count)
	{
		$html = '';
		for ($i = 1; $i <= $count; $i++) {
			$html .= '      ';
		}
		return $html;
	}

	/**
 * profitloss method
 *
 * @return void
 */
	public function profitloss($download = NULL, $format = NULL, $datos = NULL) {
		// set page title
		$this->mPageTitle = lang('page_title_reports_profitloss');
		$this->data['title'] = lang('profit_loss_title');
		$this->data['subtitle'] = lang('profit_loss_subtitle');

		if ($this->mAccountSettings->cost_center) {
			if ($this->user_cost_centers) {
				foreach ($this->user_cost_centers as $key => $value) {
					if ($key == 0) {
						$this->DB1->where('id', $value);
					} else {
						$this->DB1->or_where('id', $value);
					}
				}
			}
			$cost_centers_data = $this->DB1->get('cost_centers'.$this->DB1->dbsuffix);
			if ($cost_centers_data->num_rows() > 0) {
				$cost_centers_data = $cost_centers_data->result_array();
				foreach ($cost_centers_data as $row => $cost_center_data) {
					$cost_centers[] = $cost_center_data;
				}
			} else {
				$cost_centers = false;
			}
			$this->data['cost_centers'] = $cost_centers;
		}

		
		if ($this->input->method() == 'post') {
			$startdate = $this->mAccountSettings->fy_start;
			$enddate = $this->mAccountSettings->fy_end;
			$cost_center = null;
			$groupfrom = null;
			$groupto = null;
			$this->skip_op_balance = FALSE;
			$this->skip_entries_disapproved = FALSE;
			$only_opening = false;
			$this->data['options'] = true;
			if (!empty($this->input->post('opening'))) {
				$only_opening = true;
				/* Sub-title*/
				$this->data['subtitle'] = sprintf(lang('profit_loss_until'), $this->functionscore->dateFromSql(date('Y-m-d')));
			} else {
				if ($this->input->post('hidezero')) {
					$this->hidezero = $this->input->post('hidezero');
				}
				if ($this->input->post('startdate')) {
					$startdate = $this->functionscore->dateToSQL($this->input->post('startdate'));
				}
				if ($this->input->post('enddate')) {
					$enddate = $this->functionscore->dateToSQL($this->input->post('enddate'));
				}
				if ($this->input->post('skip_entries_disapproved')) {
					$this->skip_entries_disapproved = $this->input->post('skip_entries_disapproved');
				}
				if ($this->input->post('cost_center')) {
					$cost_center = $this->input->post('cost_center');
				}
				if ( $this->input->post('startdate') && $this->input->post('enddate')) { //Si se indicó fecha de inicio y fin para mostrar
					if ($startdate > $enddate || $startdate > $this->mAccountSettings->fy_end || $startdate < $this->mAccountSettings->fy_start || $enddate > $this->mAccountSettings->fy_end || $enddate < $this->mAccountSettings->fy_start) { //Si la fecha de inicio es mayor a la de finalizacion, mostramos error
						$startdate = $this->mAccountSettings->fy_start;
						$enddate = $this->mAccountSettings->fy_end;
						$this->data['subtitle'] = sprintf(lang('profit_loss_until'), $this->functionscore->dateFromSql(date('Y-m-d')));
						$this->session->set_flashdata('error', 'Las fecha de inicio es menor a la de finalización o las fechas están fuera del rango del año fiscal actual.');
					} else { //si están bien las fechas, seguimos
						$this->data['subtitle'] = sprintf(lang('profit_loss_from_to'),  $this->functionscore->dateFromSql($this->functionscore->dateToSQL($this->input->post('startdate'))), $this->functionscore->dateFromSql($this->functionscore->dateToSQL($this->input->post('enddate'))));
					}
					$this->skip_op_balance = TRUE;
				} else if ( $this->input->post('startdate')) { //Si sólo se indicó fecha de inicio
					if ($startdate > $this->mAccountSettings->fy_end || $startdate < $this->mAccountSettings->fy_start) { //validamos si es menor a la fecha de fin del año fiscal
						$startdate = $this->mAccountSettings->fy_start;
						$this->session->set_flashdata('error', 'La fecha de inicio está fuera del rango del año fiscal actual.');
						$this->data['subtitle'] = sprintf(lang('profit_loss_until'), $this->functionscore->dateFromSql(date('Y-m-d')));
					} else {
						$this->data['subtitle'] = sprintf(lang('profit_loss_from'), $this->functionscore->dateFromSql($this->functionscore->dateToSQL($this->input->post('startdate'))));
					}
					$this->skip_op_balance = TRUE;
				} else if ($this->input->post('enddate')) { //si sólos e indicó fecha de fin
					if ($enddate < $this->mAccountSettings->fy_start) { //validamos si es mayor al inicio del año fiscal
						$enddate = $this->mAccountSettings->fy_end;
						$this->session->set_flashdata('error', lang('balance_sheet_invalid_end_date_fy_start'));
						$this->data['subtitle'] = sprintf(lang('profit_loss_until'), $this->functionscore->dateFromSql($this->functionscore->dateToSQL(date('Y-M-d'))));
					} else {
						$this->data['subtitle'] = sprintf(lang('profit_loss_until'), $this->functionscore->dateFromSql($this->functionscore->dateToSQL($this->input->post('enddate'))));
					}
				}
			}
			

			/**********************************************************************/
			/*********************** GROSS CALCULATIONS ***************************/
			/**********************************************************************/

			$this->load->library('AccountList');
			/* Liabilities */
			$account_list = new AccountList();
			$account_list->Group = &$this->Group;
			$account_list->Ledger = &$this->Ledger;
			$account_list->only_opening = $only_opening;
			$account_list->start_date = $startdate;
			$account_list->end_date = $enddate;
			$account_list->skip_op_balance = true;
			// exit(var_dump($enddate));
			$account_list->cost_center = $cost_center;
			$account_list->skip_entries_disapproved = $this->skip_entries_disapproved;
			$account_list->affects_gross = -1;
			$account_list->start();

			$gross_expenses = $account_list->buildTree($account_list->result_account_list, '0_G', 4, 1);
			$pandl['gross_expenses'] = $gross_expenses;
			$pandl['gross_expense_total'] = (isset($gross_expenses[0]['cl_total']) ? $gross_expenses[0]['cl_total'] : 0);

			$gross_incomes = $account_list->buildTree($account_list->result_account_list, '0_G', 5, 1);
			$pandl['gross_incomes'] = $gross_incomes;
			$pandl['gross_income_total'] = ((isset($gross_incomes[0]['cl_total']) ? $gross_incomes[0]['cl_total'] : 0)) * -1;

			$gross_incomes2 = $account_list->buildTree($account_list->result_account_list, '0_G', 6, 1);
			$pandl['gross_incomes2'] = $gross_incomes2;
			$pandl['gross_income_total2'] = ((isset($gross_incomes2[0]['cl_total']) ? $gross_incomes2[0]['cl_total'] : 0)) * -1;

			$gross_incomes3 = $account_list->buildTree($account_list->result_account_list, '0_G', 7, 1);
			$pandl['gross_incomes3'] = $gross_incomes3;
			$pandl['gross_income_total3'] = ((isset($gross_incomes3[0]['cl_total']) ? $gross_incomes3[0]['cl_total'] : 0)) * -1;

			$pandl['gross_income_total'] = $pandl['gross_income_total'] + $pandl['gross_income_total2'] + $pandl['gross_income_total3'];
			$pandl['niveles'] = $this->niveles;
			/* Calculating Gross P/L */
			$pandl['gross_pl'] = $this->functionscore->calculate($pandl['gross_income_total'], $pandl['gross_expense_total'], '-');

			$this->data['pandl'] = $pandl;

			if ($download === 'download') { //Si se va a exportar el informe de pérdidas y ganancias.
				if ($format=='pdf') {
	                $this->data['settings'] = $this->mAccountSettings;
	                $this->data['POST'] = $_POST;
	                $this->data['descargar'] = 1;
	                $this->load->view('reports/pdf/profitloss', $this->data);
	            }
	            if ($format=='pdfprint') {
	                $this->data['settings'] = $this->mAccountSettings;
	                $this->data['POST'] = $_POST;
	                $this->data['descargar'] = 0;
	                $this->load->view('reports/pdf/profitloss', $this->data);
	            }
	            if ($format=='xls') {
	            	$filename = lang('page_title_reports_profitloss'); //Asignamos nombre al documento
					$this->load->library('excel');
		            $this->excel->setActiveSheetIndex(0);
		            $styleArray = array(
		                'borders' => array(
		                    'allborders' => array(
		                        'style' => PHPExcel_Style_Border::BORDER_THIN
		                    )
		                )
		            );
		            $this->excel->getDefaultStyle()->applyFromArray($styleArray);
					$this->excel->getActiveSheet()->setTitle('profitandloss'); //añadimos el título a la hoja del libro
		            $this->excel->getActiveSheet()->SetCellValue('A1', $this->data['subtitle']); //se imprime el subtítulo del documento
		            $this->excel->getActiveSheet()->mergeCells('A1:B1');
		            $this->excel->getActiveSheet()->SetCellValue('A2', lang('profit_loss_gi'));
		            $this->excel->getActiveSheet()->SetCellValue('B2', lang('search_views_legend_amount') . ' (' . $this->mAccountSettings->currency_symbol . ')');
		            $header = 'A2'.':B2';
		            $style = array(
				        'font' => array('bold' => true,),
				    );
				    $this->excel->getActiveSheet()->getStyle($header)->applyFromArray($style);
		            $this->account_st_short($pandl['gross_expenses'], $c = -1, $this, 'D'); //imprimimos los expenses (gastos)
		            $this->excel->getActiveSheet()->SetCellValue('A'.($this->row), lang('profit_loss_ge'));
		            $this->excel->getActiveSheet()->SetCellValue('B'.($this->row), lang('search_views_legend_amount') . ' (' . $this->mAccountSettings->currency_symbol . ')');
		            $header = 'A'.$this->row.':C'.$this->row;
		            $style = array(
				        'font' => array('bold' => true,),
				    );
				    $this->excel->getActiveSheet()->getStyle($header)->applyFromArray($style);
		            $this->row++;
		            $this->account_st_short($pandl['gross_incomes'], $c = -1, $this, 'C'); //imprimimos los incomes (entradas)
		            $this->row++;
		            $this->account_st_short($pandl['gross_incomes2'], $c = -1, $this, 'C'); //imprimimos los incomes2 (entradas)
		            $this->row++;
		            if (isset($pandl['gross_incomes3'])) {
		            	$this->account_st_short($pandl['gross_incomes3'], $c = -1, $this, 'C'); //imprimimos los incomes3 (entradas)
		            	$this->row++;
		            }
		            /* START TOTAL EXPENSES */
					$header = 'A'.$this->row.':C'.$this->row;
		            $style = array(
				        'font' => array('bold' => true,),
				    );
				    $this->excel->getActiveSheet()->getStyle($header)->applyFromArray($style);
					$this->excel->getActiveSheet()->SetCellValue('A'.($this->row), lang('profit_loss_tgi'));
					$montDC = $this->functionscore->drawAmount($pandl['gross_expense_total']);
		    		$prefijo = $montDC[0];
		    		$monto = $montDC[1];
					$this->excel->getActiveSheet()->SetCellValue('B'.($this->row), $prefijo); // se imprime el total de gastos
					$this->excel->getActiveSheet()->SetCellValue('C'.($this->row), $monto); // se imprime el total de gastos
		            $this->row++;
					$gross_total = $pandl['gross_expense_total'];
					if ($this->functionscore->calculate($pandl['gross_pl'], 0, '>=')) {
						$this->excel->getActiveSheet()->SetCellValue('A'.($this->row), lang('profit_loss_gp'));
						$montDC = $this->functionscore->drawAmount($pandl['gross_pl']);
			    		$prefijo = $montDC[0];
			    		$monto = $montDC[1];
						$this->excel->getActiveSheet()->SetCellValue('B'.($this->row), $prefijo);
						$this->excel->getActiveSheet()->SetCellValue('C'.($this->row), $monto);
						$gross_total = $this->functionscore->calculate($gross_total, $pandl['gross_pl'], '+');
					} else {
						$this->excel->getActiveSheet()->SetCellValue('A'.($this->row), '');
						$this->excel->getActiveSheet()->SetCellValue('B'.($this->row), '');
					}
		            $this->row++;
		            $this->excel->getActiveSheet()->SetCellValue('A'.($this->row), lang('profit_loss_t'));
					$montDC = $this->functionscore->drawAmount($gross_total);
		    		$prefijo = $montDC[0];
		    		$monto = $montDC[1];
					$this->excel->getActiveSheet()->SetCellValue('B'.($this->row), $prefijo);
					$this->excel->getActiveSheet()->SetCellValue('C'.($this->row), $monto);
		            $this->row++;
		            /* END TOTAL EXPENSES */
		            /* START TOTAL INCOMES */
		            $header = 'A'.$this->row.':C'.$this->row;
		            $style = array(
				        'font' => array('bold' => true,),
				    );
				    $this->excel->getActiveSheet()->getStyle($header)->applyFromArray($style);
					$this->excel->getActiveSheet()->SetCellValue('A'.($this->row), lang('profit_loss_tge'));
					$montDC = $this->functionscore->drawAmount($pandl['gross_income_total']);
		    		$prefijo = $montDC[0];
		    		$monto = $montDC[1];
					$this->excel->getActiveSheet()->SetCellValue('B'.($this->row), $prefijo); // se imprime el total de gastos
					$this->excel->getActiveSheet()->SetCellValue('C'.($this->row), $monto); // se imprime el total de gastos
		            $this->row++;
					$gross_total = $pandl['gross_income_total'];
					if ($this->functionscore->calculate($pandl['gross_pl'], 0, '>=')) {
						$this->excel->getActiveSheet()->SetCellValue('A'.($this->row), '');
						$this->excel->getActiveSheet()->SetCellValue('B'.($this->row), '');
					} else {
						$this->excel->getActiveSheet()->SetCellValue('A'.($this->row), lang('profit_loss_glcd'));
						$positive_gross_pl = $this->functionscore->calculate($pandl['gross_pl'], 0, 'n');
						$montDC = $this->functionscore->drawAmount($positive_gross_pl);
			    		$prefijo = $montDC[0];
			    		$monto = $montDC[1];
						$this->excel->getActiveSheet()->SetCellValue('B'.($this->row), $prefijo);
						$this->excel->getActiveSheet()->SetCellValue('C'.($this->row), $monto);
						$gross_total = $this->functionscore->calculate($gross_total, $positive_gross_pl, '+');
					}
		            $this->row++;
		            $this->excel->getActiveSheet()->SetCellValue('A'.($this->row), lang('profit_loss_t'));
					$montDC = $this->functionscore->drawAmount($gross_total);
		    		$prefijo = $montDC[0];
		    		$monto = $montDC[1];
					$this->excel->getActiveSheet()->SetCellValue('B'.($this->row), $prefijo);
					$this->excel->getActiveSheet()->SetCellValue('C'.($this->row), $monto);
		            $this->row++;
		            /* END TOTAL INCOMES */
					$this->excel->getActiveSheet()->getStyle('C1:C'.$this->row)->getNumberFormat()->setFormatCode("#,##0.00");
		            $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(80);
		            $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
		            $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
	                header('Content-Type: application/vnd.ms-excel');
	                header('Content-Disposition: attachment;filename="' . $filename . '.xls"');
	                header('Cache-Control: max-age=0');
	                $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
	                $objWriter->save('php://output');
	                exit();
	            }
			}
		}else{
			if (!is_null($datos)) { //si no se realizó filtrado de búsqueda, verificamos si se enviaron datos por URL para exportar.
				$this->data['options'] = true;
				$datos = urldecode($datos); //decodificamos el array
				$datos = unserialize($datos); //y deserializamos el array
				$this->nivelBalancesheet = $datos['nivelBalancesheet']; //Guardamos en variable global el nivel escogido por el usuario
				if ($datos['opening']) { //verificamos si se indicó que sólo balance de apertura
					$only_opening = true;
				}
				if ($datos['hidezero']) { //verificamos si se indicó que se oculten las cuentas en 0
					$this->hidezero = true;
				}
				if (isset($datos['cost_center']) && $datos['cost_center']) { //verificamos si se indicó que se oculten las cuentas en 0
					$cost_center = $datos['cost_center'];
				}
				if ($datos['startdate']) {
					$startdate = $this->functionscore->dateToSQL($datos['startdate']);
				}
				if ($datos['enddate']) {
					$enddate = $this->functionscore->dateToSQL($datos['enddate']);
				}
				if ($datos['skip_entries_disapproved']) {
					$this->skip_entries_disapproved = $datos['skip_entries_disapproved'];
				}
				if ( $datos['startdate'] && $datos['enddate']) { //Si se indicó fecha de inicio y fin para mostrar
					if ($startdate > $enddate) { //Si la fecha de inicio es mayor a la de finalizacion, mostramos error
						$startdate = $this->mAccountSettings->fy_start;
						$enddate = $this->mAccountSettings->fy_end;
						$this->data['subtitle'] = sprintf(lang('profit_loss_until'), $this->functionscore->dateFromSql(date('Y-m-d')));
						$this->session->set_flashdata('error', 'La fecha de finalización no puede ser menor a la de inicio.');
					} else { //si están bien las fechas, seguimos
						$this->data['subtitle'] = sprintf(lang('profit_loss_from_to'),  $this->functionscore->dateFromSql($this->functionscore->dateToSQL($datos['startdate'])), $this->functionscore->dateFromSql($this->functionscore->dateToSQL($datos['enddate'])));
					}
					$this->skip_op_balance = TRUE;
				} else if ( $datos['startdate']) { //Si sólo se indicó fecha de inicio
					if ($startdate > $this->mAccountSettings->fy_end) { //validamos si es menor a la fecha de fin del año fiscal
						$startdate = $this->mAccountSettings->fy_start;
						$this->session->set_flashdata('error', 'La fecha de inicio no puede ser mayor a la de fecha de finalización del año fiscal.');
						$this->data['subtitle'] = sprintf(lang('profit_loss_until'), $this->functionscore->dateFromSql(date('Y-m-d')));
					} else {
						$this->data['subtitle'] = sprintf(lang('profit_loss_from'), $this->functionscore->dateFromSql($this->functionscore->dateToSQL($datos['startdate'])));
					}
					$this->skip_op_balance = TRUE;
				} else if ($datos['enddate']) { //si sólos e indicó fecha de fin
					if ($enddate < $this->mAccountSettings->fy_start) { //validamos si es mayor al inicio del año fiscal
						$enddate = $this->mAccountSettings->fy_end;
						$this->session->set_flashdata('error', lang('balance_sheet_invalid_end_date_fy_start'));
						$this->data['subtitle'] = sprintf(lang('profit_loss_until'), $this->functionscore->dateFromSql($this->functionscore->dateToSQL(date('Y-M-d'))));
					} else {
						$this->data['subtitle'] = sprintf(lang('profit_loss_until'), $this->functionscore->dateFromSql($this->functionscore->dateToSQL($datos['enddate'])));
					}
				}
			} else { //Si no se recibieron datos para exportar
				// $this->data['options'] = false;
				/* Sub-title*/
				$this->data['subtitle'] = sprintf(lang('profit_loss_until'), $this->functionscore->dateFromSql(date('Y-m-d')));
			}
		}
		if (!$download) {
			// render page
			$this->render('reports/profitloss');
		}
	}
	/**
	 * trialbalance method
	 *
	 *
	 * @return void
	 *
	 *
	 **/
	public function trialbalance($download = NULL, $format = NULL, $datos=NULL) {
		// set page title
		$this->mPageTitle = lang('page_title_reports_trialbalance');
		$this->data['title'] = lang('page_title_reports_trialbalance');
		$this->data['subtitle'] = sprintf(lang('trial_balance_until'), $this->functionscore->dateFromSql($this->mAccountSettings->fy_end));
		$only_opening = false;
		$startdate = $this->mAccountSettings->fy_start;
		$enddate = $this->mAccountSettings->fy_end;
		$cost_center = null;
		$third_id = null;
		$third_account_trial_balance = false;
		$this->skip_entries_disapproved = FALSE;
		/* Difference in opening balance */
		if ($this->mAccountSettings->cost_center) {
			if ($this->user_cost_centers) {
				foreach ($this->user_cost_centers as $key => $value) {
					if ($key == 0) {
						$this->DB1->where('id', $value);
					} else {
						$this->DB1->or_where('id', $value);
					}
				}
			}
			$cost_centers_data = $this->DB1->get('cost_centers'.$this->DB1->dbsuffix);
			if ($cost_centers_data->num_rows() > 0) {
				$cost_centers_data = $cost_centers_data->result_array();
				foreach ($cost_centers_data as $row => $cost_center_data) {
					$cost_centers[] = $cost_center_data;
				}
			} else {
				$cost_centers = false;
			}
			$this->data['cost_centers'] = $cost_centers;
			// exit(var_dump($cost_centers));
		}

		if ($this->input->method() == 'post') { //Si se realizó filtro de búsqueda

			$this->data['options'] = true;
			$this->data['opdiff'] = $this->ledger_model->getOpeningDiff();
			if ($this->functionscore->calculate($this->data['opdiff']['opdiff_balance'], 0, '==')) {
				$this->data['is_opdiff'] = false;
			} else {
				$this->data['is_opdiff'] = true;
			}
			if ($this->input->post('opening')) {
				$only_opening = true;
				/* Sub-title*/
				$this->data['subtitle'] = sprintf(lang('trial_balance_until'), $this->functionscore->dateFromSql(date('Y-m-d')));
			} else {
				if ($this->input->post('hidezero')) {
					$this->hidezero = $this->input->post('hidezero');
				}
				if ($this->input->post('nivelBalancesheet')) {
					$this->nivelBalancesheet = $this->input->post('nivelBalancesheet');
				}
				if ($this->input->post('startdate')) {
					$startdate = $this->functionscore->dateToSQL($this->input->post('startdate'));
				}
				if ($this->input->post('enddate')) {
					$enddate = $this->functionscore->dateToSQL($this->input->post('enddate'));
				}
				if ($this->input->post('cost_center')) {
					$cost_center = $this->input->post('cost_center');
				}
				if ($this->input->post('third_id')) {
					$third_id = $this->input->post('third_id');
				}
				if ($this->input->post('skip_entries_disapproved')) {
					$this->skip_entries_disapproved = $this->input->post('skip_entries_disapproved');
				}
				if ( $this->input->post('startdate') && $this->input->post('enddate')) { //Si se indicó fecha de inicio y fin para mostrar
					if ($startdate > $enddate || $startdate < $this->mAccountSettings->fy_start || $startdate > $this->mAccountSettings->fy_end || $enddate < $this->mAccountSettings->fy_start || $enddate > $this->mAccountSettings->fy_end) { //Si la fecha de inicio es mayor a la de finalizacion, mostramos error
						$startdate = $this->mAccountSettings->fy_start;
						$enddate = $this->mAccountSettings->fy_end;
						$this->data['subtitle'] = sprintf(lang('trial_balance_until'), $this->functionscore->dateFromSql(date('Y-m-d')));
						$this->session->set_flashdata('error', 'La fecha de inicio es menor a la de finalización o las fechas están fuera del rango del año fiscal actual.');
					} else { //si están bien las fechas, seguimos
						$this->data['subtitle'] = sprintf(lang('trial_balance_from_to'), $this->functionscore->dateFromSql($this->functionscore->dateToSQL($this->input->post('startdate'))), $this->functionscore->dateFromSql($this->functionscore->dateToSQL($this->input->post('enddate'))));
					}
				} else if ( $this->input->post('startdate')) { //Si sólo se indicó fecha de inicio
					if ($startdate > $this->mAccountSettings->fy_end || $startdate < $this->mAccountSettings->fy_start) { //validamos si es menor a la fecha de fin del año fiscal
						$startdate = $this->mAccountSettings->fy_start;
						$this->session->set_flashdata('error', 'La fecha de inicio no puede ser mayor a la de fecha de finalización del año fiscal.');
						$this->data['subtitle'] = sprintf(lang('trial_balance_until'), $this->functionscore->dateFromSql(date('Y-m-d')));
					} else {
						$this->data['subtitle'] = sprintf(lang('trial_balance_from'), $this->functionscore->dateFromSql($this->functionscore->dateToSQL($this->input->post('startdate'))));
					}
				} else if ($this->input->post('enddate')) { //si sólos e indicó fecha de fin
					if ($enddate > $this->mAccountSettings->fy_end || $enddate < $this->mAccountSettings->fy_start) { //validamos si es mayor al inicio del año fiscal
						$enddate = $this->mAccountSettings->fy_end;
						$this->session->set_flashdata('error', 'La fecha de finalización no puede ser menor a la de fecha de inicio del año fiscal.');
						$this->data['subtitle'] = sprintf(lang('trial_balance_until'), $this->functionscore->dateFromSql(date('Y-m-d')));
					} else {
						$this->data['subtitle'] = sprintf(lang('trial_balance_until'), $this->functionscore->dateFromSql($this->functionscore->dateToSQL($this->input->post('enddate'))));
					}
				}
				if ($this->input->post('groupfrom') && $this->input->post('groupfrom') != "") {
					if ($this->input->post('groupfrom') < 0) {
						$this->DB1->where('id', abs($this->input->post('groupfrom')));
						$ledger = $this->DB1->get('groups'.$this->DB1->dbsuffix)->row_array();
					} else {
						$this->DB1->where('id', abs($this->input->post('groupfrom')));
						$ledger = $this->DB1->get('ledgers'.$this->DB1->dbsuffix)->row_array();
					}
					$this->startcode = $ledger['code'];
					$this->data['startcode'] = $ledger['code'];
				}
				if ($this->input->post('groupto') && $this->input->post('groupto') != "") {
					if ($this->input->post('groupto') < 0) {
						$this->DB1->where('id', abs($this->input->post('groupto')));
						$ledger = $this->DB1->get('groups'.$this->DB1->dbsuffix)->row_array();
					} else {
						$this->DB1->where('id', abs($this->input->post('groupto')));
						$ledger = $this->DB1->get('ledgers'.$this->DB1->dbsuffix)->row_array();
					}
					$this->endcode = $ledger['code'];
					$this->data['endcode'] = $ledger['code'];
					if (isset($this->data['startcode'])) {
						if ($this->data['endcode']." " < $this->data['startcode']." ") {
							$this->session->set_flashdata('error', lang('invalid_filter_code'));
							unset($this->data['startcode']);
							unset($this->data['endcode']);
						}
					}
				}
			}
			$this->data['third_account_trial_balance'] = NULL;
			if ($this->input->post('third_id') != NULL && $this->input->post('third_id') != '') {
				$sql_third = $this->input->post('third_id') != 0 ? " WHERE EI.companies_id = {$this->input->post('third_id')} " : "";

				if ($startdate) {
						$sql_third.= " ".($sql_third != "" ? " AND " : " WHERE ")." E.date >= '".$startdate."' AND E.date <= '".$enddate."' ";
				}
				if ($cost_center) {
						$sql_third.= " ".($sql_third != "" ? " AND " : " WHERE ")." EI.cost_center_id = ".$cost_center;
				}
				$sql_third_op = $this->input->post('third_id') != 0 ? " WHERE COP.id_companies = {$this->input->post('third_id')} " : "";


				if ($cost_center) {
						$sql_third_op.= " ".($sql_third_op != "" ? " AND " : " WHERE ")." COP.cost_center_id = ".$cost_center;
				}

				$sql_third_op_entries = $this->input->post('third_id') != 0 ? " WHERE EI.companies_id = {$this->input->post('third_id')} " : "";

				if ($startdate) {
						$sql_third_op_entries.= " ".($sql_third_op_entries != "" ? " AND " : " WHERE ")." E.date < '".$startdate."' ";
				}
				if ($cost_center) {
						$sql_third_op_entries.= " ".($sql_third_op_entries != "" ? " AND " : " WHERE ")." EI.cost_center_id = ".$cost_center;
				}

				$sql = "SELECT 
							name,
							vat_no,
							company_id,
						    ledger_id,
							SUM(COALESCE(total_op,0)) as total_op,
							SUM(COALESCE(total_dr,0)) as total_dr,
							SUM(COALESCE(total_cr,0)) as total_cr,
							(SUM(COALESCE(total_op,0)) + SUM(COALESCE(total_cl,0))) as total_cl
						FROM (	SELECT C.name, C.vat_no, C.id as company_id, EI.ledger_id, 
								0  as total_op,
								SUM(COALESCE(IF(EI.dc = 'D', EI.amount, 0), 0)) as total_dr,
								SUM(COALESCE(IF(EI.dc = 'C', EI.amount, 0), 0)) as total_cr,
								SUM(COALESCE(IF(EI.dc = 'C', EI.amount * -1, EI.amount), 0)) as total_cl
							FROM sma_companies C
								LEFT JOIN {$this->DB1->dbprefix('entryitems')}{$this->DB1->dbsuffix} EI ON EI.companies_id = C.id
								LEFT JOIN {$this->DB1->dbprefix('entries')}{$this->DB1->dbsuffix} E ON EI.entry_id = E.id
								{$sql_third}
							GROUP BY EI.companies_id, EI.ledger_id

								UNION

							SELECT C.name, C.vat_no, COP.id_companies, COP.id_ledgers,
								SUM(COALESCE(IF(COP.op_balance_dc = 'C', COP.op_balance * -1, COP.op_balance), 0)) as total_op,
						        0,
								0,
								0
							FROM
								{$this->DB1->dbprefix('companies_opbalance')}{$this->DB1->dbsuffix} COP
							LEFT JOIN {$this->DB1->dbprefix('companies')} C ON C.id = COP.id_companies
								{$sql_third_op}
							GROUP BY COP.id_companies, COP.id_ledgers

							UNION

							SELECT C.name, C.vat_no, C.id as company_id, EI.ledger_id, 
								SUM(COALESCE(IF(EI.dc = 'C', EI.amount * -1, EI.amount), 0))  as total_op,
								0 as total_dr,
								0 as total_cr,
								0 as total_cl
							FROM sma_companies C
								LEFT JOIN {$this->DB1->dbprefix('entryitems')}{$this->DB1->dbsuffix} EI ON EI.companies_id = C.id
								LEFT JOIN {$this->DB1->dbprefix('entries')}{$this->DB1->dbsuffix} E ON EI.entry_id = E.id
								{$sql_third_op_entries}
							GROUP BY EI.companies_id, EI.ledger_id
						) as third_ledgers 
						GROUP BY company_id, ledger_id";
				$result = $this->DB1->query($sql)->result_array();
				$third_filter = $this->input->post('third_id') != 0 ? $this->input->post('third_id') : NULL;
				$third_account_trial_balance = [];
				foreach ($result as $dataRow) {
					$third_account_trial_balance[$dataRow['ledger_id']][$dataRow['company_id']]['company_name'] = $dataRow['name'];
					$third_account_trial_balance[$dataRow['ledger_id']][$dataRow['company_id']]['company_vat_no'] = $dataRow['vat_no'];
					$third_account_trial_balance[$dataRow['ledger_id']][$dataRow['company_id']]['op_balance'] = $dataRow['total_op'];
					$third_account_trial_balance[$dataRow['ledger_id']][$dataRow['company_id']]['cl_balance'] = $dataRow['total_cl'];
					$third_account_trial_balance[$dataRow['ledger_id']][$dataRow['company_id']]['total_dr'] = $dataRow['total_dr'];
					$third_account_trial_balance[$dataRow['ledger_id']][$dataRow['company_id']]['total_cr'] = $dataRow['total_cr'];
				}
				$this->data['third_account_trial_balance'] = $third_account_trial_balance;
			}

			$this->data['DB1'] = $this->DB1;
			 //Array donde se almacenaran los códigos de cada grupo con su nivel, estructura [nivel][num autoincrementable]
			$this->load->library('AccountList');
			$accountlist = new AccountList();
			$accountlist->Group = &$this->Group;
			$accountlist->Ledger = &$this->Ledger;
			$accountlist->only_opening = $only_opening;
			$accountlist->start_date = $startdate;
			$accountlist->end_date = $enddate;
			$accountlist->cost_center = $cost_center;
			$accountlist->third_id = $third_id;
			$accountlist->skip_entries_disapproved = $this->skip_entries_disapproved;
			if (isset($this->data['startcode'])) {
				$accountlist->startcode = $this->data['startcode'];
			}
			if (isset($this->data['endcode'])) {
				$accountlist->endcode = $this->data['endcode'];
			}
			$accountlist->affects_gross = -1;
			$accountlist->start();
			$this->data['account_list'] = $accountlist; //objeto inicial
			$this->data['accountlist'] = $accountlist->account_list; //listado de auxiliares
			// exit(var_dump($accountlist->cl_total));
      		// $this->functionscore->print_arrays($this->data['accountlist']);

			$this->data['niveles'] = $this->niveles;
			/* Ledger selection */
			$ledgers = new LedgerTree(); // initilize ledgers array - LedgerTree Lib
			$ledgers->Group = &$this->Group; // initilize selected ledger groups in ledgers array
			$ledgers->Ledger = &$this->Ledger; // initilize selected ledgers in ledgers array
			$ledgers->current_id = -1; // initilize current group id
			$ledgers->restriction_bankcash = 1;
			$ledgers->build(0); // set ledger id to [NULL] and ledger name to [None]
			$ledgers->toList($ledgers, -1); // create a list of ledgers array
			$this->data['ledger_options'] = $ledgers->ledgerList; // pass ledger list to view page
			
			// $this->data['third_account_trial_balance'] = false;
			// $third_account_trial_balance = false;
		} else{
			 //Array donde se almacenaran los códigos de cada grupo con su nivel, estructura [nivel][num autoincrementable]
			$this->load->library('AccountList');
			$accountlist = new AccountList();
			$accountlist->Group = &$this->Group;
			$accountlist->Ledger = &$this->Ledger;
			$accountlist->affects_gross = -1;
			$accountlist->start();
			$account_list = $accountlist->buildTree($accountlist->result_account_list, '0_G');
			$this->data['accountlist'] = $account_list;
			$this->data['niveles'] = $this->niveles;
			/* Ledger selection */
			$ledgers = new LedgerTree(); // initilize ledgers array - LedgerTree Lib
			$ledgers->Group = &$this->Group; // initilize selected ledger groups in ledgers array
			$ledgers->Ledger = &$this->Ledger; // initilize selected ledgers in ledgers array
			$ledgers->current_id = -1; // initilize current group id
			$ledgers->restriction_bankcash = 1;
			$ledgers->build(0); // set ledger id to [NULL] and ledger name to [None]
			$ledgers->toList($ledgers, -1); // create a list of ledgers array
			$this->data['ledger_options'] = $ledgers->ledgerList; // pass ledger list to view page
			
			$this->data['third_account_trial_balance'] = false;
			$third_account_trial_balance = false;

			$this->data['niveles'] = $this->niveles;
		}
		
			
		// exit(var_dump($_POST));
		if (!$download) {
			if ($validation = $this->settings_model->validate_invalid_entry_items_data()) {
				// exit(var_dump($validation));
				if ($validation['error']) {
					$this->session->set_flashdata('error', $validation['msg']);
					$this->data['validation_message'] = $validation['msg'];
				}
			}
			// render page
			$terceros = $this->DB1->query('SELECT * FROM '.$this->prefijodbpos.'companies')->result_array();
			$this->data['terceros'] = $terceros;
			$this->render('reports/trialbalance');
		}
		if ($download === 'download') {
			if ($format=='pdf') {
                $this->data['settings'] = $this->mAccountSettings;
                $this->data['POST'] = $_POST;
                $this->data['descargar'] = 1;
                $this->load->view('reports/pdf/trialbalance', $this->data);
            }
            if ($format=='pdfprint') {
                $this->data['settings'] = $this->mAccountSettings;
                $this->data['POST'] = $_POST;
                $this->data['descargar'] = 0;
                $this->load->view('reports/pdf/trialbalance', $this->data);
            }
            if ($format=='xls') {
            	$filename = lang('page_title_reports_trialbalance');
				$this->load->library('excel');
	            $this->excel->setActiveSheetIndex(0);
	            $styleArray = array(
	                'borders' => array(
	                    'allborders' => array(
	                        'style' => PHPExcel_Style_Border::BORDER_THIN
	                    )
	                )
	            );
	            $this->excel->getDefaultStyle()->applyFromArray($styleArray);
				$this->excel->getActiveSheet()->setTitle('trialbalance');
	            $this->excel->getActiveSheet()->SetCellValue('A1', $this->data['subtitle']);
	            $this->excel->getActiveSheet()->mergeCells('A1:H1');
	            $this->excel->getActiveSheet()->SetCellValue('A2', lang('accounts_index_account_name'));
	            $this->excel->getActiveSheet()->SetCellValue('B2', lang('accounts_index_op_balance') . ' (' . $this->mAccountSettings->currency_symbol . ')');
	            $this->excel->getActiveSheet()->mergeCells('B2:C2');
	            $this->excel->getActiveSheet()->SetCellValue('D2', lang('dr_total') . ' (' . $this->mAccountSettings->currency_symbol . ')');
	            $this->excel->getActiveSheet()->mergeCells('D2:E2');
	            $this->excel->getActiveSheet()->SetCellValue('F2', lang('cr_total') . ' (' . $this->mAccountSettings->currency_symbol . ')');
	            $this->excel->getActiveSheet()->mergeCells('F2:G2');
	            $this->excel->getActiveSheet()->SetCellValue('H2', lang('accounts_index_cl_balance') . ' (' . $this->mAccountSettings->currency_symbol . ')');
	            $this->excel->getActiveSheet()->mergeCells('H2:I2');
	            $this->totalParientes = [];
	            foreach ($accountlist->account_list as $dataRow) {
	            	$this->print_account_chart($dataRow, $c = -1, $this, $this->niveles, (isset($this->startcode) ? $this->startcode : NULL), (isset($this->endcode) ? $this->endcode : NULL), $third_account_trial_balance);
	            }
	            $this->row++;
	            $this->excel->getActiveSheet()->SetCellValue('A'.($this->row), lang('entries_views_add_items_td_total'));
	            $this->excel->getActiveSheet()->SetCellValue('B'.($this->row), '');
	            if ((!isset($this->startcode) && !isset($this->endcode)) || (isset($this->startcode) && isset($this->endcode) && !$this->startcode && !$this->endcode) || !isset($_POST)) {
	            	$montDC = $this->functionscore->drawAmount($this->functionscore->toCurrency('D', $accountlist->dr_total));
		            $this->excel->getActiveSheet()->SetCellValue('C'.($this->row), $montDC[0]);
		            $this->excel->getActiveSheet()->SetCellValue('D'.($this->row), $montDC[1]);
		            $montDC = $this->functionscore->drawAmount($this->functionscore->toCurrency('C', $accountlist->cr_total));
		            $this->excel->getActiveSheet()->SetCellValue('E'.($this->row), $montDC[0]);
		            $this->excel->getActiveSheet()->SetCellValue('F'.($this->row), $montDC[1]);
		            $montDC = $this->functionscore->drawAmount($this->functionscore->toCurrency($accountlist->cl_total_dc, $accountlist->cl_total));
		            $this->excel->getActiveSheet()->SetCellValue('H'.($this->row), $montDC[0]);
		            $this->excel->getActiveSheet()->SetCellValue('I'.($this->row), $montDC[1]);
				} else if (isset($this->startcode) && isset($this->endcode) && $this->startcode != 0 && $this->endcode != 0 ) {
					$drcalc = 0;
					$crcalc = 0;
					foreach ($this->totalParientes as $codigo => $total) {
								$drcalc += $total['dr_total'];
								$crcalc += $total['cr_total'];
					}
					$montDC = $this->functionscore->drawAmount($this->functionscore->toCurrency('D', $drcalc));
					$this->excel->getActiveSheet()->SetCellValue('C'.($this->row), $montDC[0]);
					$this->excel->getActiveSheet()->SetCellValue('D'.($this->row), $montDC[1]);
					$montDC = $this->functionscore->drawAmount($this->functionscore->toCurrency('C', $crcalc));
		            $this->excel->getActiveSheet()->SetCellValue('E'.($this->row), $montDC[0]);
		            $this->excel->getActiveSheet()->SetCellValue('F'.($this->row), $montDC[1]);
					$this->excel->getActiveSheet()->SetCellValue('G'.($this->row), '');
				}
				$this->excel->getActiveSheet()->getStyle('D'.$this->row)->getNumberFormat()->setFormatCode("#,##0.00");
				$this->excel->getActiveSheet()->getStyle('F'.$this->row)->getNumberFormat()->setFormatCode("#,##0.00");
	            $this->row++;
	            $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(80);
	            $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
	            $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
	            $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
	            $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
	            $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
	            $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
	            $this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
	            $this->excel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
	            $this->excel->getActiveSheet()->getStyle('C3:C'.$this->row)->getNumberFormat()->setFormatCode("#,##0.00");
	            $this->excel->getActiveSheet()->getStyle('E3:E'.$this->row)->getNumberFormat()->setFormatCode("#,##0.00");
	            $this->excel->getActiveSheet()->getStyle('G3:G'.$this->row)->getNumberFormat()->setFormatCode("#,##0.00");
	            $this->excel->getActiveSheet()->getStyle('I3:I'.$this->row)->getNumberFormat()->setFormatCode("#,##0.00");
                header('Content-Type: application/vnd.ms-excel');
                header('Content-Disposition: attachment;filename="' . $filename . '.xls"');
                header('Cache-Control: max-age=0');
                $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
                $objWriter->save('php://output');
                exit();
            }
		}
	}

	function print_account_chart($account, $c, $THIS, $niveles, $startcode = NULL, $endcode = NULL, $third_account_trial_balance = false)
	{
		$account = isset($account[0]) ? $account[0] : $account;
		if (empty($account)) {
			return [];
		}
		$bandera = 0;
		if (isset($this->nivelBalancesheet) && $this->nivelBalancesheet != "") {
			for ($i=1; $i <= $this->nivelBalancesheet ; $i++) {
				if (in_array($account['code'], $this->niveles[$i])) {
					$bandera++;
				}
			}
		} else {
			$this->nivelBalancesheet = "";
			$bandera++;
		}
		$clBandera = $this->bandera_closing_balance($account);
		/* FILTRO POR CUENTAS */
		$pcBandera = 0;
		if (($startcode != NULL || $endcode != NULL) && $account['code'] != "") {
			$pcBandera = $this->bandera_posicion_cuentas($account['code'], $startcode, $endcode);
			if ($pcBandera != 0 && $account['code'] == $endcode) {
				$this->totalParientes[$account['code']]['dr_total'] = $account['dr_total'];
				$this->totalParientes[$account['code']]['cr_total'] = $account['cr_total'];
			}
		} else {
			$pcBandera++;
		}
		/* FILTRO POR CUENTAS */
	  	$CI =& get_instance();
		$counter = $c;
		if ($this->input->post('third_id') && $this->input->post('third_id') != '') {

		} else {
			/* Print groups */
			if (strlen($account['code']) > 0 && $bandera != 0 && ((isset($this->hidezero) && $clBandera != 0) || !isset($this->hidezero)) && ($pcBandera != 0)) {
				// exit(var_dump($this->functionscore->drawAmount($CI->functionscore->toCurrency($account['cl_total_dc'], $account['cl_total']))));
				$this->row++;
				$this->excel->getActiveSheet()->SetCellValue("A".$this->row, $CI->functionscore->toCodeWithName($account['code'], $account['name']));

				$montDC = $this->functionscore->drawAmount($CI->functionscore->toCurrency(($account['op_total'] < 0 ? 'C' : 'D'), ($account['op_total'] < 0 ? $account['op_total'] *-1 : $account['op_total'])));

				$this->excel->getActiveSheet()->SetCellValue("B".$this->row, $montDC[0]);
				$this->excel->getActiveSheet()->SetCellValue("C".$this->row, $montDC[1]);

				$montDC = $this->functionscore->drawAmount($CI->functionscore->toCurrency('D', $account['dr_total']));

				$this->excel->getActiveSheet()->SetCellValue("D".$this->row, $montDC[0]);
				$this->excel->getActiveSheet()->SetCellValue("E".$this->row, $montDC[1]);

				$montDC = $this->functionscore->drawAmount($CI->functionscore->toCurrency('C', $account['cr_total']));

				$this->excel->getActiveSheet()->SetCellValue("F".$this->row, $montDC[0]);
				$this->excel->getActiveSheet()->SetCellValue("G".$this->row, $montDC[1]);

				$montDC = $this->functionscore->drawAmount($CI->functionscore->toCurrency(($account['cl_total'] < 0 ? 'C' : 'D'), ($account['cl_total'] < 0 ? $account['cl_total'] *-1 : $account['cl_total'])));

				$this->excel->getActiveSheet()->SetCellValue("H".$this->row, $montDC[0]);
				$this->excel->getActiveSheet()->SetCellValue("I".$this->row, $montDC[1]);

			}
		}

		/* Print child ledgers */
		if (isset($account['children_ledgers']) && count($account['children_ledgers']) > 0 && $this->nivelBalancesheet == "") {
			$counter++;
			foreach ($account['children_ledgers'] as $id => $data) {
				$pcBandera = 0;
				if ($startcode != NULL || $endcode != NULL) {
					$pcBandera = $this->bandera_posicion_cuentas($data['code'], $startcode, $endcode);
					if ($pcBandera == 1) {
						if (isset($this->totalParientes[$data['parent_id']])) {
							$this->totalParientes[$data['parent_id']]['dr_total'] += $data['dr_total'];
							$this->totalParientes[$data['parent_id']]['cr_total'] += $data['cr_total'];
						} else {
							$this->totalParientes[$data['parent_id']]['dr_total'] = $data['dr_total'];
							$this->totalParientes[$data['parent_id']]['cr_total'] = $data['cr_total'];
						}
					}
				} else {
					$pcBandera++;
				}
				if ((isset($this->hidezero) && ($data['cl_total'] != 0 || $data['cr_total'] > 0 || $data['dr_total'] > 0) && $pcBandera != 0) || (!isset($this->hidezero) && $pcBandera != 0)) {

					$this->row++;

					$this->excel->getActiveSheet()->SetCellValue("A".$this->row, $CI->functionscore->toCodeWithName($data['code'], $data['name']));

					$montDC = $this->functionscore->drawAmount($CI->functionscore->toCurrency(($data['op_total'] < 0 ? 'C' : 'D'), ($data['op_total'] < 0 ? $data['op_total'] *-1 : $data['op_total'])));

					$this->excel->getActiveSheet()->SetCellValue("B".$this->row, $montDC[0]);
					$this->excel->getActiveSheet()->SetCellValue("C".$this->row, $montDC[1]);

					$montDC = $this->functionscore->drawAmount($CI->functionscore->toCurrency('D', $data['dr_total']));

					$this->excel->getActiveSheet()->SetCellValue("D".$this->row, $montDC[0]);
					$this->excel->getActiveSheet()->SetCellValue("E".$this->row, $montDC[1]);

					$montDC = $this->functionscore->drawAmount($CI->functionscore->toCurrency('C', $data['cr_total']));

					$this->excel->getActiveSheet()->SetCellValue("F".$this->row, $montDC[0]);
					$this->excel->getActiveSheet()->SetCellValue("G".$this->row, $montDC[1]);

					$montDC = $this->functionscore->drawAmount($CI->functionscore->toCurrency(($data['cl_total'] < 0 ? 'C' : 'D'), ($data['cl_total'] < 0 ? $data['cl_total'] *-1 : $data['cl_total'])));

					$this->excel->getActiveSheet()->SetCellValue("H".$this->row, $montDC[0]);
					$this->excel->getActiveSheet()->SetCellValue("I".$this->row, $montDC[1]);

					if ($third_account_trial_balance && isset($third_account_trial_balance[$data['id']])) {
						foreach ($third_account_trial_balance[$data['id']] as $company_id => $company) {

							$this->row++;

							$this->excel->getActiveSheet()->SetCellValue("A".$this->row, "    >> ".$data['code']." - ".$company['company_vat_no']." - ".$company['company_name']);

							$montDC = $this->functionscore->drawAmount($CI->functionscore->toCurrency(($company['op_balance'] < 0 ? 'C' : 'D'), ($company['op_balance'] < 0 ? $company['op_balance'] * -1 : $company['op_balance'])));

							$this->excel->getActiveSheet()->SetCellValue("B".$this->row, $montDC[0]);
							$this->excel->getActiveSheet()->SetCellValue("C".$this->row, $montDC[1]);

							$montDC = $this->functionscore->drawAmount($CI->functionscore->toCurrency('D', $company['total_dr']));

							$this->excel->getActiveSheet()->SetCellValue("D".$this->row, $montDC[0]);
							$this->excel->getActiveSheet()->SetCellValue("E".$this->row, $montDC[1]);

							$montDC = $this->functionscore->drawAmount($CI->functionscore->toCurrency('C', $company['total_cr']));

							$this->excel->getActiveSheet()->SetCellValue("F".$this->row, $montDC[0]);
							$this->excel->getActiveSheet()->SetCellValue("G".$this->row, $montDC[1]);

							$montDC = $this->functionscore->drawAmount($CI->functionscore->toCurrency(($company['cl_balance'] < 0 ? 'C' : 'D'), ($company['cl_balance'] < 0 ? $company['cl_balance'] * -1 : $company['cl_balance'])));

							$this->excel->getActiveSheet()->SetCellValue("H".$this->row, $montDC[0]);
							$this->excel->getActiveSheet()->SetCellValue("I".$this->row, $montDC[1]);

						}
					}
				}
					
			}
			$counter--;

		}

		/* Print child groups recursively */
		if (isset($account['children_groups'])) {
			foreach ($account['children_groups'] as $id => $data) {
				$counter++;
				$this->print_account_chart($data, $counter, $THIS, $niveles, $startcode, $endcode, $third_account_trial_balance);
				$counter--;
			}
		}
	}

	/**
	 * ledgerstatement method
	 *
	 * @return void
	 */
	public function ledgerstatement($download = NULL, $format = NULL) {
		// set page title
		$this->mPageTitle = lang('page_title_reports_ledgerstatement');
		$this->data['title'] = lang('page_title_reports_ledgerstatement');
		
		$ledgers = $this->ledger_model->getAllLedgers();
		$this->data['ledgers'] = $ledgers;

		$this->data['showEntries'] = false;
		$this->data['options'] = false;
		$this->skip_entries_disapproved = false;
		$tipo_notas = $this->DB1->get('entrytypes'.$this->DB1->dbsuffix)->result_array();
		$this->data['tipo_notas'] = $tipo_notas;

		$ledgerId = null;
		$show = true;

		if ($this->mAccountSettings->cost_center) {

			if ($this->user_cost_centers) {
				foreach ($this->user_cost_centers as $key => $value) {
					if ($key == 0) {
						$this->DB1->where('id', $value);
					} else {
						$this->DB1->or_where('id', $value);
					}
				}
			}

			$cost_centers_data = $this->DB1->get('cost_centers'.$this->DB1->dbsuffix);
			if ($cost_centers_data->num_rows() > 0) {
				$cost_centers_data = $cost_centers_data->result_array();
				foreach ($cost_centers_data as $row => $cost_center_data) {
					$cost_centers[] = $cost_center_data;
				}
			} else {
				$cost_centers = false;
			}

			$this->data['cost_centers'] = $cost_centers;
		}

		if ($this->input->method() == 'post') {
			if ($this->input->post('groupfrom') && $this->input->post('groupfrom') != "") {
				if ($this->input->post('groupfrom') < 0) {
					$this->DB1->where('id', abs($this->input->post('groupfrom')));
					$ledger = $this->DB1->get('groups'.$this->DB1->dbsuffix)->row_array();
				} else {
					$this->DB1->where('id', abs($this->input->post('groupfrom')));
					$ledger = $this->DB1->get('ledgers'.$this->DB1->dbsuffix)->row_array();
				}
				$startcode = $ledger['code'];
			}

			if ($this->input->post('groupto') && $this->input->post('groupto') != "") {
				if ($this->input->post('groupto') < 0) {
					$this->DB1->where('id', abs($this->input->post('groupto')));
					$ledger = $this->DB1->get('groups'.$this->DB1->dbsuffix)->row_array();
				} else {
					$this->DB1->where('id', abs($this->input->post('groupto')));
					$ledger = $this->DB1->get('ledgers'.$this->DB1->dbsuffix)->row_array();
				}
				$endcode = $ledger['code'];
			}

			$this->data['showEntries'] = true;
		}

		$this->render('reports/ledgerstatement');
	}
	/**
 * ledgerentries method
 *
 * @return void
 */
	public function ledgerentries($show = true, $ledgerId = NULL) {
		// set page title
		$this->mPageTitle = lang('page_title_reports_ledgerentries');
		$this->data['title'] = lang('page_title_reports_ledgerentries');

		/* Create list of ledgers to pass to view */
		$ledgers = new LedgerTree();
		$ledgers->Group = &$this->Group;
		$ledgers->Ledger = &$this->Ledger;
		$ledgers->current_id = -1;
		$ledgers->restriction_bankcash = 1;
		$ledgers->build(0);
		$ledgers->toList($ledgers, -1);

		$this->data['ledgers'] = $ledgers->ledgerList;

		if ($this->input->method() == 'post') {
			if (empty($this->input->post('ledger_id'))) {
				$this->session->set_flashdata('error', lang('invalid_ledger'));
				redirect('reports/ledgerentries');
			}
			$ledgerId = $this->input->post('ledger_id');
		}
		$this->data['showEntries'] = false;
		$this->data['options'] = false;


		if ($ledgerId) {
			/* Check if ledger exists */
			$this->DB1->where('id', $ledgerId);
			$ledger = $this->DB1->get('ledgers'.$this->DB1->dbsuffix)->row_array();

			if (!$ledger) {
				$this->session->set_flashdata('error', lang('ledger_not_found'));
				redirect('reports/ledgerentries');
			}

			$this->data['ledger_data'] = $ledger;


			/* Set the approprite search conditions */
			$conditions = array();
			$conditions['entryitems'.$this->DB1->dbsuffix.'.ledger_id'] = $ledgerId;

			/* Set the approprite search conditions if custom date is selected */
			$startdate = $this->mAccountSettings->fy_start;
			$enddate = $this->mAccountSettings->fy_end;

			$this->data['options'] = true;

			if (!empty($this->input->post('startdate'))) {
				/* TODO : Validate date */
				$startdate = $this->functionscore->dateToSql($this->input->post('startdate'));
				$conditions['entries'.$this->DB1->dbsuffix.'.date >='] = $startdate;
			}

			if (!empty($this->input->post('enddate'))) {
				/* TODO : Validate date */
				$enddate = $this->functionscore->dateToSql($this->input->post('enddate'));
				$conditions['entries'.$this->DB1->dbsuffix.'.date <='] = $enddate;
			}

			if (!empty($this->input->post('entry_type'))) {
				$entry_type = $this->input->post('entry_type');
				$conditions['entries'.$this->DB1->dbsuffix.'.entrytype_id'] = $entry_type;
			} else {
				$entry_type = NULL;
			}

			// echo "ENTRY TYPE : ".$this->input->post('entry_type');


			/* Sub-title*/
			if (!empty($this->input->post('startdate')) && !empty($this->input->post('enddate'))) {
				$this->data['subtitle'] = sprintf(lang('ledger_entries_from_to'),
					($ledger['name']),
					$this->functionscore->dateFromSql($this->functionscore->dateToSQL($this->input->post('startdate'))),
					$this->functionscore->dateFromSql($this->functionscore->dateToSQL($this->input->post('enddate')))
				);
			} else if (!empty($this->input->post('startdate'))) {
				$this->data['subtitle'] = sprintf(lang('ledger_entries_from_to'),
					($ledger['name']),
					$this->functionscore->dateFromSql($this->functionscore->dateToSQL($this->input->post('startdate'))),
					$this->functionscore->dateFromSql($this->mAccountSettings->fy_end)
				);
			} else if (!empty($this->input->post('enddate'))) {
				$this->data['subtitle'] = sprintf(lang('ledger_entries_from_to'),
					($ledger['name']),
					$this->functionscore->dateFromSql($this->mAccountSettings->fy_start),
					$this->functionscore->dateFromSql($this->input->post('enddate'))
				);
			}else{
				$this->data['subtitle'] = sprintf(lang('ledger_entries_from_to'),
					($this->functionscore->toCodeWithName($ledger['code'], $ledger['name'])),
					$this->functionscore->dateFromSql($this->mAccountSettings->fy_start),
					$this->functionscore->dateFromSql($this->mAccountSettings->fy_end)
				);

			}
			/* Opening and closing titles */
			if (is_null($startdate)) {
				$this->data['opening_title'] = sprintf(lang('opening_balance_as_on'),
					$this->functionscore->dateFromSql($this->mAccountSettings->fy_start));
			} else {
				$this->data['opening_title'] = sprintf(lang('opening_balance_as_on'),
					$this->functionscore->dateFromSql($startdate));
			}
			if (is_null($enddate)) {
				$this->data['closing_title'] = sprintf(lang('closing_balance_as_on'),
					$this->functionscore->dateFromSql($this->mAccountSettings->fy_end));

			} else {
				$this->data['closing_title'] = sprintf(lang('closing_balance_as_on'),
					$this->functionscore->dateFromSql($enddate));
			}


			/* Calculating opening balance */
			$op = $this->ledger_model->openingBalance($ledgerId, $startdate, FALSE, FALSE, $entry_type);
			$this->data['op'] = $op;

			/* Calculating closing balance */
			$cl = $this->ledger_model->closingBalance($ledgerId, null, $enddate, FALSE, FALSE, $entry_type);
			$this->data['cl'] = $cl;

			/* Calculate current page opening balance */
			$current_op = $op;
			$this->DB1->where($conditions)->select('entries'.$this->DB1->dbsuffix.'.id, entries'.$this->DB1->dbsuffix.'.tag_id, entries'.$this->DB1->dbsuffix.'.entrytype_id, entries'.$this->DB1->dbsuffix.'.number, entries'.$this->DB1->dbsuffix.'.date, entries'.$this->DB1->dbsuffix.'.dr_total, entries'.$this->DB1->dbsuffix.'.cr_total, entryitems'.$this->DB1->dbsuffix.'.narration, entryitems'.$this->DB1->dbsuffix.'.entry_id, entryitems'.$this->DB1->dbsuffix.'.ledger_id, entryitems'.$this->DB1->dbsuffix.'.amount, entryitems'.$this->DB1->dbsuffix.'.dc, entryitems'.$this->DB1->dbsuffix.'.reconciliation_date')->join('entryitems'.$this->DB1->dbsuffix, 'entries'.$this->DB1->dbsuffix.'.id = entryitems'.$this->DB1->dbsuffix.'.entry_id', 'left')->order_by('entries'.$this->DB1->dbsuffix.'.date', 'asc');

			$this->data['entries'] = $this->DB1->get('entries'.$this->DB1->dbsuffix)->result_array();
			/* Set the current page opening balance */
			$this->data['current_op'] = $current_op;

			/* Pass varaibles to view which are used in Helpers */
			$this->data['allTags'] = $this->DB1->get('tags'.$this->DB1->dbsuffix)->result_array();
			$this->data['showEntries'] = true;
		}

		if ($show) {
			// render page
			$this->render('reports/ledgerentries');
		}else{
			return array(
				'ledgers' 	=> $this->data['ledgers'],
				'showEntries' => $this->data['showEntries'],
				'ledger_data' => $this->data['ledger_data'],
				'subtitle' 	=> $this->data['subtitle'],
				'opening_title' => $this->data['opening_title'],
				'closing_title' => $this->data['closing_title'],
				'op' 			=> $this->data['op'],
				'cl' 			=> $this->data['cl'],
				'entries'		=> $this->data['entries'],
				'current_op' 	=> $this->data['current_op'],
				'allTags' 	=> $this->data['allTags'],
			);
		}

	}

	/**
	 * reconciliation method
	 *
	 * @return void
	 */
	public function reconciliation($download = NULL, $format=NULL) {
		// set page title
		$this->mPageTitle = lang('page_title_reports_reconciliation');
		$this->data['title'] = lang('page_title_reports_reconciliation');


		/* Create list of ledgers to pass to view */
		$this->DB1->where('ledgers'.$this->DB1->dbsuffix.'.reconciliation', 1);
		$this->DB1->order_by('ledgers'.$this->DB1->dbsuffix.'.name', 'asc');
		$this->DB1->select('ledgers'.$this->DB1->dbsuffix.'.id, ledgers'.$this->DB1->dbsuffix.'.name, ledgers'.$this->DB1->dbsuffix.'.code');
		$ledgers_q = $this->DB1->get('ledgers'.$this->DB1->dbsuffix)->result_array();
		if ($ledgers_q) {
			$ledgers = array(0 => lang('please_select'));
			foreach ($ledgers_q as $row) {
				$ledgers[$row['id']] = $this->functionscore->toCodeWithName(
					$row['code'], $row['name']
				);
			}
		}else{
			$ledgers = array(0 => lang('no_reconciled_ledgers_found'));
		}
		$this->data['ledgers'] = $ledgers;

		if ($this->input->method() == 'post') {
			/* Ledger selection form submitted */
			if (!empty($this->input->post('submit_ledger'))) {
				if (empty($this->input->post('ledger_id'))) {
					$this->session->set_flashdata('error', lang('invalid_ledger'));
					redirect('reports/reconciliation');
				}
			} else if (!empty($this->input->post('submitrec')) && !empty($this->input->post('ReportRec[]'))) {

				$cntUp = 0; //contador para filas afectadas
				$sicual = 'Sql : ';
				/* Check if acccount is locked */
				if ($this->mAccountSettings->account_locked == 1) {
					$this->session->set_flashdata('error', lang('groups_cntrler_edit_account_locked_error'));
					redirect('reports/reconciliation');
				}

				$datosR = $this->input->post('ReportRec[]');

				/* Reconciliation form submitted */
				foreach ($datosR as $row => $recitem) {

					if (!empty($recitem['recdate'])) {
						$recdate = $this->functionscore->dateToSql($recitem['recdate']);
						if (!$recdate) {
							$this->session->set_flashdata('error', lang('invalid_reconciliation_date'));
							continue;
						}

						$update = $this->DB1->where('id', $recitem['id'])->update('entryitems'.$this->DB1->dbsuffix, array('reconciliation_date'=>$recdate));

						if ($update) { //si se hizo update con éxito aumenta el contador
							$cntUp++;
						} else {

							$sicual .= $this->DB1->last_query()."</br>";

						}
					}

				}

				if ($cntUp > 0) { //si el contador es mayor a 0, enviamos mensaje de éxito
					$this->session->set_flashdata('message', lang('reconciliation_successs'));
				} else { //caso contrario, enviamos mensaje de error
					$this->session->set_flashdata('error', lang('reconciliation_unsuccess')."</br>".$sicual);
				}
			} else {
				redirect('reports/reconciliation');
			}
		}

		$this->data['showEntries'] = false;
		$this->data['options'] = false;

		/* Set the approprite search conditions if custom date is selected */
		$startdate = $this->mAccountSettings->fy_start;
		$enddate = $this->mAccountSettings->fy_end;

		if ($this->input->method() == 'post') {
			$ledgerId = $this->input->post('ledger_id');

			/* Check if ledger exists */
			$this->DB1->where('id', $ledgerId);
			$ledger = $this->DB1->from('ledgers'.$this->DB1->dbsuffix)->get()->row_array();

			if (!$ledger) {
				$this->session->set_flashdata('error', lang('ledger_not_found'));
				redirect('reports/reconciliation');
			}

			$this->data['ledger_data'] = $ledger;


			/* Set the approprite search conditions */
			$conditions = array();
			$conditions['entryitems'.$this->DB1->dbsuffix.'.ledger_id'] = $ledgerId;
			$this->data['options'] = true;

			if (!empty($this->input->post('startdate'))) {
				/* TODO : Validate date */
				$startdate = $this->functionscore->dateToSql($this->input->post('startdate'));
				$conditions['entries'.$this->DB1->dbsuffix.'.date >='] = $startdate;
			}

			if (!empty($this->input->post('enddate'))) {
				/* TODO : Validate date */
				$enddate = $this->functionscore->dateToSql($this->input->post('enddate'));
				$conditions['entries'.$this->DB1->dbsuffix.'.date <='] = $enddate;
			}

			/* Sub-title*/
			if (!empty($this->input->post('startdate')) && !empty($this->input->post('enddate'))) {
				$this->data['subtitle'] = sprintf(lang('reconciliation_for_from_to'),
					($ledger['name']),
					$this->functionscore->dateFromSql($this->functionscore->dateToSQL($this->input->post('startdate'))),
					$this->functionscore->dateFromSql($this->functionscore->dateToSQL($this->input->post('enddate')))
				);
			} else if (!empty($this->input->post('startdate'))) {
				$this->data['subtitle'] = sprintf(lang('reconciliation_for_from_to'),
					($ledger['name']),
					$this->functionscore->dateFromSql($this->functionscore->dateToSQL($this->input->post('startdate'))),
					$this->functionscore->dateFromSql($this->mAccountSettings->fy_end)
				);
			} else if (!empty($this->input->post('enddate'))) {
				$this->data['subtitle'] = sprintf(lang('reconciliation_for_from_to'),
					($ledger['name']),
					$this->functionscore->dateFromSql($this->mAccountSettings->fy_start),
					$this->functionscore->dateFromSql($this->input->post('enddate'))
				);
			}else{
				$this->data['subtitle'] = sprintf(lang('reconciliation_for_from_to'),
					($this->functionscore->toCodeWithName($ledger['code'], $ledger['name'])),
					$this->functionscore->dateFromSql($this->mAccountSettings->fy_start),
					$this->functionscore->dateFromSql($this->mAccountSettings->fy_end)
				);
			}

			if (empty($this->input->post('showall'))) {
				$conditions['entryitems'.$this->DB1->dbsuffix.'.reconciliation_date'] = NULL;
			}
			/* Opening and closing titles */
			if (is_null($startdate)) {
				$this->data['opening_title'] = sprintf(lang('opening_balance_as_on'),
					$this->functionscore->dateFromSql($this->mAccountSettings->fy_start));
			} else {
				$this->data['opening_title'] = sprintf(lang('opening_balance_as_on'),
					$this->functionscore->dateFromSql($startdate));
			}
			if (is_null($enddate)) {
				$this->data['closing_title'] = sprintf(lang('closing_balance_as_on'),
					$this->functionscore->dateFromSql($this->mAccountSettings->fy_end));

			} else {
				$this->data['closing_title'] = sprintf(lang('closing_balance_as_on'),
					$this->functionscore->dateFromSql($enddate));
			}

			/* Reconciliation pending title */
			$this->data['recpending_title'] = '';

			/* Sub-title*/
			if (!is_null($startdate) && !is_null($enddate)) {
				$this->data['recpending_title'] = sprintf(lang('reconciliation_from_to'),
					$this->functionscore->dateFromSql($this->functionscore->dateToSQL($this->input->post('startdate'))),
					$this->functionscore->dateFromSql($this->functionscore->dateToSQL($this->input->post('enddate')))
				);
			} else if (!is_null($this->input->post('startdate'))) {
				$this->data['recpending_title'] = sprintf(lang('reconciliation_from_to'),
					$this->functionscore->dateFromSql($this->functionscore->dateToSQL($this->input->post('startdate'))),
					$this->functionscore->dateFromSql($this->mAccountSettings->fy_end)
				);
			} else if (is_null($this->input->post('enddate'))) {
				$this->data['recpending_title'] = sprintf(lang('reconciliation_from_to'),
					$this->functionscore->dateFromSql($this->mAccountSettings->fy_start),
					$this->functionscore->dateFromSql($this->input->post('enddate'))
				);
			}else{
				$this->data['recpending_title'] = sprintf(lang('reconciliation_from_to'),
					$this->functionscore->dateFromSql($this->mAccountSettings->fy_start),
					$this->functionscore->dateFromSql($this->mAccountSettings->fy_end)
				);
			}

			/* Calculating opening balance */
			$op = $this->ledger_model->openingBalance($ledgerId, $startdate);
			$this->data['op'] = $op;

			/* Calculating closing balance */
			$cl = $this->ledger_model->closingBalance($ledgerId, null, $enddate);
			$this->data['cl'] = $cl;

			/* Calculating reconciliation pending balance */
			$rp = $this->ledger_model->reconciliationPending($ledgerId, $startdate, $enddate);
			$this->data['rp'] = $rp;

			$this->DB1->where($conditions)->select('entryitems'.$this->DB1->dbsuffix.'.id as eiid, entries'.$this->DB1->dbsuffix.'.id , entries'.$this->DB1->dbsuffix.'.tag_id, entries'.$this->DB1->dbsuffix.'.entrytype_id, entries'.$this->DB1->dbsuffix.'.number, entries'.$this->DB1->dbsuffix.'.date, entries'.$this->DB1->dbsuffix.'.dr_total, entries'.$this->DB1->dbsuffix.'.cr_total, entryitems'.$this->DB1->dbsuffix.'.narration, entryitems'.$this->DB1->dbsuffix.'.entry_id, entryitems'.$this->DB1->dbsuffix.'.ledger_id, entryitems'.$this->DB1->dbsuffix.'.amount, entryitems'.$this->DB1->dbsuffix.'.dc, entryitems'.$this->DB1->dbsuffix.'.reconciliation_date')->join('entryitems'.$this->DB1->dbsuffix, 'entries'.$this->DB1->dbsuffix.'.id = entryitems'.$this->DB1->dbsuffix.'.entry_id', 'left')->order_by('entries'.$this->DB1->dbsuffix.'.date', 'asc');
			$this->data['entries'] = $this->DB1->get('entries'.$this->DB1->dbsuffix)->result_array();

			/* Pass varaibles to view which are used in Helpers */
			$this->data['allTags'] = $this->DB1->get('tags'.$this->DB1->dbsuffix)->result_array();
			$this->data['showEntries'] = true;

		}

		// render page
		$this->render('reports/reconciliation');

	}

	public function thirdsaccounts($action = NULL, $format = NULL){
		// set page title
		$this->mPageTitle = lang('page_title_reports_third_and_accounts');
		$this->data['title'] = lang('page_title_reports_third_and_accounts');

		$ledgers = $this->ledger_model->getAllLedgers();
		$this->data['ledgers'] = $ledgers;

		$terceros = $this->DB1->query('SELECT * FROM '.$this->prefijodbpos.'companies')->result_array();
		$this->data['terceros'] = $terceros;

		$tipo_notas = $this->DB1->get('entrytypes'.$this->DB1->dbsuffix)->result_array();
		$this->data['tipo_notas'] = $tipo_notas;

		if ($this->mAccountSettings->cost_center) {

			if ($this->user_cost_centers) {
				foreach ($this->user_cost_centers as $key => $value) {
					if ($key == 0) {
						$this->DB1->where('id', $value);
					} else {
						$this->DB1->or_where('id', $value);
					}
				}
			}

			$cost_centers_data = $this->DB1->get('cost_centers'.$this->DB1->dbsuffix);
			if ($cost_centers_data->num_rows() > 0) {
				$cost_centers_data = $cost_centers_data->result_array();
				foreach ($cost_centers_data as $row => $cost_center_data) {
					$cost_centers[] = $cost_center_data;
				}
			} else {
				$cost_centers = false;
			}

			$this->data['cost_centers'] = $cost_centers;
		}

		if ($this->input->method() == 'post') {
			if ($this->input->post('groupfrom') && $this->input->post('groupfrom') != "") {
				if ($this->input->post('groupfrom') < 0) {
					$this->DB1->where('id', abs($this->input->post('groupfrom')));
					$ledger = $this->DB1->get('groups'.$this->DB1->dbsuffix)->row_array();
				} else {
					$this->DB1->where('id', abs($this->input->post('groupfrom')));
					$ledger = $this->DB1->get('ledgers'.$this->DB1->dbsuffix)->row_array();
				}
				$startcode = $ledger['code'];
			}

			if ($this->input->post('groupto') && $this->input->post('groupto') != "") {
				if ($this->input->post('groupto') < 0) {
					$this->DB1->where('id', abs($this->input->post('groupto')));
					$ledger = $this->DB1->get('groups'.$this->DB1->dbsuffix)->row_array();
				} else {
					$this->DB1->where('id', abs($this->input->post('groupto')));
					$ledger = $this->DB1->get('ledgers'.$this->DB1->dbsuffix)->row_array();
				}
				$endcode = $ledger['code'];
			}

			if ($this->input->post('groupfrom') && $this->input->post('groupto') && (String) $startcode <= (String) $endcode) {
				$this->data['showEntries'] = true;
			} else {
				// $this->session->set_flashdata('error', lang('invalid_ledger'));
				// redirect('reports/thirdsaccounts');
			}
		}

		$this->render('reports/thirdsaccounts');
	}

	public function statement_cashflow() {
		// set page title
		$this->mPageTitle = 'Statement of Cashflow';
		$this->data['title'] = 'Statement of Cashflow';


	}

	public function levels_groups_ledgers($array, $level = 1){
	    $this->niveles[$level][] = $array->id;
	    // Revisar y agregar los ledgers del grupo actual
	    if (isset($array->children_ledgers)) {
	        foreach ($array->children_ledgers as $ledger) {
	            $this->niveles[$level + 1][] = $ledger['code'];
	        }
	    }
	    // Recorrer los subgrupos del grupo actual
	    if (!empty($array->children_groups)) {
	        foreach ($array->children_groups as $subgroup) {
	            $this->levels_groups_ledgers($subgroup, $level + 1);
	        }
	    }
	}

	public function bandera_closing_balance($data, $bandera = 0) {
		$data = isset($data[0]) ? $data[0] : $data;
		if (!empty($data)) {
			if (isset($data['children_groups']) && count($data['children_groups']) > 0) { // Revisamos si el grupo en turno tiene grupos hijos.
				foreach ($data['children_groups'] as $id => $grupo) { //en caso de tener grupos hijos los recorremos.
					if (isset($grupo['children_groups']) && count($grupo['children_groups']) > 0) { //Si el grupo hijo en turno tiene grupos hijos, llamamos a la misma función en la que estamos.
						$bandera = $this->bandera_closing_balance($grupo, $bandera);
					} else if ($grupo['cl_total'] != 0 || $grupo['cr_total'] > 0 || $grupo['dr_total'] > 0) { //En caso de que el grupo hijo en turno no tenga grupos hijos, revisamos si su Closing balance es mayor a 0.
						$bandera++;
					}
				}
			} else if (isset($data['children_ledgers']) && count($data['children_ledgers']) > 0) { // Si no tiene grupos hijos, revisamos si tiene Ledgers (Auxiliares)
				foreach ($data['children_ledgers'] as $key => $ledger) { //En caso de tener Ledgers, los recorremos todos.
					if ($ledger['cl_total'] != 0 || $ledger['cr_total'] > 0 || $ledger['dr_total'] > 0) { //Revisamos si algún ledger tiene su Closing Balance mayor a 0
						$bandera++;
					}
				}
			} else if ($data['cl_total'] != 0 || $data['cr_total'] > 0 || $data['dr_total'] > 0) { //En caso de no tener ni grupos hijos ni auxiliares, revisamos si el Closing Balance (Balance de cierre) es mayor a 0.
				$bandera++;
			}
		}
			

		return $bandera; //Si bandera es mayor a 0, el grupo se muestra.
	}


	function bandera_posicion_cuentas($code, $startcode, $endcode){

		$code2 = $code." ";
		$startcode2 = $startcode." ";
		$endcode2 = $endcode." ";

		if ($startcode == 0) {
			if (strcmp($code2, $endcode2) <= 0) {
				return 1;
			} else {
				return 0;
			}
		} else if ($endcode == 0) {
			if (strcmp($code2, $startcode2) >= 0) {
				return 1;
			} else {
				return 0;
			}
		} else if ($startcode != 0 && $endcode != 0) {
			if (strcmp($code2, $startcode2) >= 0 && strcmp($code2, $endcode2) <= 0) {
				return 1;
			} else {
				return 0;
			}
		}

	}

	function getEntries($action = NULL, $format = NULL){

		// exit(var_dump($_POST));

		if (isset($_POST)) {

			if ($this->input->post('groupfrom') && $this->input->post('groupfrom') != "") {
				if ($this->input->post('groupfrom') < 0) {
					$this->DB1->where('id', abs($this->input->post('groupfrom')));
					$ledger = $this->DB1->get('groups'.$this->DB1->dbsuffix)->row_array();
				} else {
					$this->DB1->where('id', abs($this->input->post('groupfrom')));
					$ledger = $this->DB1->get('ledgers'.$this->DB1->dbsuffix)->row_array();
				}
				$startcode = $ledger['code'];
			} else {
				$startcode = NULL;
			}

			if ($this->input->post('groupto') && $this->input->post('groupto') != "") {
				if ($this->input->post('groupto') < 0) {
					$this->DB1->where('id', abs($this->input->post('groupto')));
					$ledger = $this->DB1->get('groups'.$this->DB1->dbsuffix)->row_array();
				} else {
					$this->DB1->where('id', abs($this->input->post('groupto')));
					$ledger = $this->DB1->get('ledgers'.$this->DB1->dbsuffix)->row_array();
				}
				$endcode = $ledger['code'];
			} else {
				$endcode = NULL;
			}

			if ($this->input->post('skip_entries_disapproved') == "TRUE") {
				$skip_entries_disapproved = TRUE;
			} else {
				$skip_entries_disapproved = FALSE;
			}

			if (($this->input->post('show_base')) && $this->input->post('show_base') == "TRUE") {
				$show_base = TRUE;
			} else {
				$show_base = FALSE;
			}

			if ($this->input->post('startdate') != "") {
				$startdate = $this->functionscore->dateToSQL($this->input->post('startdate'));
			} else {
				$startdate = $this->mAccountSettings->fy_start;
			}

			if ($this->input->post('enddate') != "") {
				$enddate =$this->functionscore->dateToSQL($this->input->post('enddate'));
			} else {
				$enddate = $this->mAccountSettings->fy_end;
			}

			if (($this->input->post('entry_type')) && $this->input->post('entry_type') != "") {
				$entrytype = $this->input->post('entry_type');
			} else {
				$entrytype = NULL;
			}

			if (($this->input->post('cost_center'))  && $this->input->post('cost_center') != 'undefined' && $this->input->post('cost_center') != '') {
				$cost_center = $this->input->post('cost_center');
			} else {
				$cost_center = NULL;
			}

		}

		$op_sql_startcode = $sql_startcode = $startcode ? "code >= '{$startcode}'" : false;
		$op_sql_endcode = $sql_endcode = $endcode ? "code <= '{$endcode}'" : false;
		$sql_startdate = $startdate ? "date >= '{$startdate}'" : false;
		$sql_op_startdate = $startdate ? "date < '{$startdate}'" : false;
		$sql_enddate = $enddate ? "date <= '{$enddate}'" : false;
		$sql_entrytype = $entrytype ? "E.entrytype_id = {$entrytype}" : false;
		$sql_cost_center = $cost_center ? "EI.cost_center_id = {$cost_center}" : false;
		$op_where = $where = "";
		if ($sql_startcode || $sql_endcode || $sql_startdate || $sql_enddate || $sql_entrytype || $sql_cost_center) {
			$where = " WHERE ";
			if ($sql_endcode &&($sql_startcode)) {
				$sql_endcode = " AND ".$sql_endcode;
			}
			if ($sql_startdate &&($sql_startcode || $sql_endcode)) {
				$sql_startdate = " AND ".$sql_startdate;
			}
			if ($sql_op_startdate &&($sql_startcode || $sql_endcode)) {
				$sql_op_startdate = " AND ".$sql_op_startdate;
			}
			if ($sql_enddate &&($sql_startcode || $sql_endcode || $sql_startdate)) {
				$sql_enddate = " AND ".$sql_enddate;
			}
			if ($sql_entrytype &&($sql_startcode || $sql_endcode || $sql_startdate || $sql_enddate)) {
				$sql_entrytype = " AND ".$sql_entrytype;
			}
			if ($sql_cost_center &&($sql_startcode || $sql_endcode || $sql_startdate || $sql_enddate || $sql_entrytype)) {
				$sql_cost_center = " AND ".$sql_cost_center;
			}
		}
		if ($op_sql_startcode || $op_sql_endcode) {
			$op_where = " WHERE ";
			if ($op_sql_endcode && ($op_sql_startcode)) {
				$op_sql_endcode = " AND ".$op_sql_endcode;
			}
		}

		$sql_op_select = $sql_startdate ? "+ COALESCE(total_op_entries.total, 0)" : "";
		$sql_op_entries = $sql_startdate ? "LEFT JOIN 
							(
								SELECT
									EI.ledger_id AS ledger_id,
								    SUM(COALESCE(IF(EI.dc = 'D', EI.amount, EI.amount * -1), 0)) AS total
								FROM {$this->DB1->dbprefix('entryitems')}{$this->DB1->dbsuffix} EI
									LEFT JOIN {$this->DB1->dbprefix('entries')}{$this->DB1->dbsuffix} E ON E.id = EI.entry_id
								    LEFT JOIN {$this->DB1->dbprefix('entrytypes')}{$this->DB1->dbsuffix} ET ON ET.id = E.entrytype_id
									LEFT JOIN {$this->DB1->dbprefix('ledgers')}{$this->DB1->dbsuffix} L ON EI.ledger_id = L.id
									{$where} {$sql_startcode} {$sql_endcode} {$sql_op_startdate} {$sql_entrytype} {$sql_cost_center}
								GROUP BY EI.ledger_id
							) as total_op_entries ON total_op_entries.ledger_id = L.id" : "";

		$sql_ledgers = "SELECT 
							L.id,
						    L.code,
						    L.name,
						    SUM(COALESCE(IF(COP.op_balance_dc = 'D', COP.op_balance, (COP.op_balance*-1)),0)) {$sql_op_select} AS op_balance
						FROM {$this->DB1->dbprefix('ledgers')}{$this->DB1->dbsuffix} L
							LEFT JOIN {$this->DB1->dbprefix('companies_opbalance')}{$this->DB1->dbsuffix} COP ON COP.id_ledgers = L.id
							{$sql_op_entries}
							{$op_where} {$op_sql_startcode} {$op_sql_endcode}
						GROUP BY L.id
						ORDER BY L.code ASC;";

		$sql_ledgers_q = $this->DB1->query($sql_ledgers);
		$sql_ledgers_result = $sql_ledgers_q->result();

		$sql_ledgers_entries = "SELECT
					EI.entry_id AS entry_id,
					L.id AS ledger_id,
				    L.code,
					CONCAT(ET.prefix, E.number) AS reference_no,
				    E.date,
				    EI.narration,
				    EI.base,
				    SUM(COALESCE(IF(EI.dc = 'D', EI.amount, 0), 0)) AS dr_total,
				    SUM(COALESCE(IF(EI.dc = 'C', EI.amount, 0), 0)) AS cr_total,
				    ET.label as entry_label,
				    E.number as entry_number,
				    E.state as entry_state
				FROM {$this->DB1->dbprefix('entryitems')}{$this->DB1->dbsuffix} EI
					LEFT JOIN {$this->DB1->dbprefix('entries')}{$this->DB1->dbsuffix} E ON E.id = EI.entry_id
				    LEFT JOIN {$this->DB1->dbprefix('entrytypes')}{$this->DB1->dbsuffix} ET ON ET.id = E.entrytype_id
					LEFT JOIN {$this->DB1->dbprefix('ledgers')}{$this->DB1->dbsuffix} L ON EI.ledger_id = L.id
					{$where} {$sql_startcode} {$sql_endcode} {$sql_startdate} {$sql_enddate} {$sql_entrytype} {$sql_cost_center}
				GROUP BY EI.id
				ORDER BY L.code ASC, E.date ASC, E.number ASC";

		$sql_ledgers_entries_q = $this->DB1->query($sql_ledgers_entries);
		$sql_ledgers_entries_result = $sql_ledgers_entries_q->result();

		$ledgers_data = [];
		foreach ($sql_ledgers_result as $ledger_row) {
			$ledgers_data[$ledger_row->id] = $ledger_row;
		}

		$data = [];
		$total_cl = 0;
		$total_dr = 0;
		$total_cr = 0;
		$ledger_total = NULL;
		$ledger_total_dr = NULL;
		$ledger_total_cr = NULL;
		$ledger_id = NULL;
		$num = 0;
		foreach ($sql_ledgers_entries_result as $entry_row) {
			if (!$entry_row->ledger_id) {
				continue;
			}
			if ($ledger_id == NULL || $ledger_id != $entry_row->ledger_id) {
				if ($ledger_total !== NULL) {
					$ledger_row = $ledgers_data[$ledger_id];
					unset($ledgers_data[$ledger_id]);
					$data[$num]['entry_id'] = '';
					$data[$num]['entryLabel'] = '';
					$data[$num]['num_item'] = '';
					$data[$num]['entry_number'] =  '';
					$data[$num]['entry_date'] =  '<b>'.$ledger_row->code.'</b>';
					$data[$num]['narration'] = '<b>'.$ledger_row->name.'</b>';
					$data[$num]['amount_D'] = '<b>'.$this->functionscore->toCurrency('D', $ledger_total_dr).'</b>';
					$data[$num]['amount_C'] = '<b>'.$this->functionscore->toCurrency('C', $ledger_total_cr).'</b>';
					$data[$num]['saldo'] = $this->functionscore->toCurrency(($ledger_total < 0 ? 'C' : 'D'), ($ledger_total < 0 ? $ledger_total * -1 : $ledger_total));
					$data[$num]['state'] = '';
					if ($show_base) {
						$data[$num]['base'] = 0;
					}
					$num++;
					$ledger_total = 0;
					$ledger_total_dr = 0;
					$ledger_total_cr = 0;
				}
				$ledger_id = $entry_row->ledger_id;
				$ledger_row = $ledgers_data[$ledger_id];
				$data[$num]['entry_id'] = '';
				$data[$num]['num_item'] = '';
				$data[$num]['entryLabel'] = '';
				$data[$num]['entry_number'] = '';
				$data[$num]['entry_date'] = '<b>'.$ledger_row->code.'</b>';
				$data[$num]['narration'] = '<b>'.$ledger_row->name.'</b>';
				$data[$num]['amount_D'] = '';
				$data[$num]['amount_C'] = '<b>'.lang('accounts_index_op_balance').'</b>';
				$data[$num]['saldo'] = '<b>'.$this->functionscore->toCurrency(($ledger_row->op_balance < 0 ? 'C' : 'D'), ($ledger_row->op_balance < 0 ? $ledger_row->op_balance*-1 : $ledger_row->op_balance)).'</b>';
				$data[$num]['state'] = '';
				if ($show_base) {
					$data[$num]['base'] = 0;
				}
				$ledger_total = $ledger_row->op_balance;
				$total_cl += $ledger_row->op_balance;
				$num++;
			}

			$data[$num]['entry_id'] = $entry_row->entry_id;
			$data[$num]['num_item'] = $num+1;
			$data[$num]['entryLabel'] = $entry_row->entry_label;
			$data[$num]['entry_number'] = $entry_row->reference_no;
			$data[$num]['entry_date'] = '<b>'.$entry_row->date.'</b>';
			$data[$num]['narration'] = '<b>'.$entry_row->narration.'</b>';
			$data[$num]['amount_D'] = $this->functionscore->toCurrency('D', $entry_row->dr_total);
			$data[$num]['amount_C'] = $this->functionscore->toCurrency('C', $entry_row->cr_total);
			$ledger_total += ($entry_row->dr_total - $entry_row->cr_total);
			$ledger_total_cr += ($entry_row->cr_total);
			$ledger_total_dr += ($entry_row->dr_total);
			$total_cr += ($entry_row->cr_total);
			$total_dr += ($entry_row->dr_total);
			$total_cl += ($entry_row->dr_total - $entry_row->cr_total);
			$data[$num]['saldo'] = '<b>'.$this->functionscore->toCurrency(($ledger_total < 0 ? 'C' : 'D'), ($ledger_total < 0 ? $ledger_total * -1 : $ledger_total)).'</b>';
			if ($entry_row->entry_state == '1') {
				$data[$num]['state'] = 'Aprobado';
			} else if ($entry_row->entry_state == '2') {
				$data[$num]['state'] = 'Desaprobado';
			} else if ($entry_row->entry_state == '0') {
				$data[$num]['state'] = 'Anulada';
			}
			if ($show_base) {
				$data[$num]['base'] = $entry_row->base;
			}
			$num++;
		}

		$ledger_row = isset($ledgers_data[$ledger_id]) ? $ledgers_data[$ledger_id] : false;
		if ($ledger_row) {
			$data[$num]['entry_id'] = '';
			$data[$num]['entryLabel'] = '';
			$data[$num]['num_item'] = '';
			$data[$num]['entry_number'] =  '';
			$data[$num]['entry_date'] =  '<b>'.$ledger_row->code.'</b>';
			$data[$num]['narration'] = '<b>'.$ledger_row->name.'</b>';
			$data[$num]['amount_D'] = '<b>'.$this->functionscore->toCurrency('D', $ledger_total_dr).'</b>';
			$data[$num]['amount_C'] = '<b>'.$this->functionscore->toCurrency('C', $ledger_total_cr).'</b>';
			$data[$num]['saldo'] = $this->functionscore->toCurrency(($ledger_total < 0 ? 'C' : 'D'), ($ledger_total < 0 ? $ledger_total * -1 : $ledger_total));
			$data[$num]['state'] = '';
			if ($show_base) {
				$data[$num]['base'] = 0;
			}
			$num++;
		}
		unset($ledgers_data[$ledger_id]);
			

		if (count($ledgers_data) > 0) {
			foreach ($ledgers_data as $ledger_id => $ledger) {
				$op = $this->ledger_model->openingBalance($ledger->id, ( ($startdate) ? $startdate : $this->mAccountSettings->fy_start), FALSE, $skip_entries_disapproved, $entrytype, NULL, $cost_center);
				if ($op['amount'] == 0) {
					continue;
				}
				$data[$num]['entry_id'] = '';
				$data[$num]['entryLabel'] = '';
				$data[$num]['num_item'] = '';
				$data[$num]['entry_number'] = '';
				$data[$num]['entry_date'] = '<b>'.$ledger->code.'</b>';
				$data[$num]['narration'] = '<b>'.$ledger->name.'</b>';
				$data[$num]['amount_D'] = '';
				$data[$num]['amount_C'] = 'O/P BALANCE';
				$data[$num]['saldo'] = '<b>'.$this->functionscore->toCurrency(($ledger->op_balance < 0 ? 'C' : 'D'), ($ledger->op_balance < 0 ? $ledger->op_balance * -1 : $ledger->op_balance)).'</b>';
				$data[$num]['state'] = '';
				if ($show_base) {
					$data[$num]['base'] = 0;
				}
				$num++;
				$data[$num]['entry_id'] = '';
				$data[$num]['entryLabel'] = '';
				$data[$num]['num_item'] = '';
				$data[$num]['entry_number'] = '<b>'.$ledger->code.'</b>';
				$data[$num]['entry_date'] = '<b>'.$ledger->name.'</b>';
				$data[$num]['narration'] = '';
				$data[$num]['amount_D'] = '';
				$data[$num]['amount_C'] = 'C/L BALANCE';
				$data[$num]['saldo'] = '<b>'.$this->functionscore->toCurrency(($ledger->op_balance < 0 ? 'C' : 'D'), ($ledger->op_balance < 0 ? $ledger->op_balance * -1 : $ledger->op_balance)).'</b>';
				$data[$num]['state'] = '';
				if ($show_base) {
					$data[$num]['base'] = 0;
				}
				$total_cl += $ledger->op_balance;
				$num++;
			}
		}

		$data[$num]['entry_id'] = '';
		$data[$num]['entryLabel'] = '';
		$data[$num]['num_item'] = $num;
		$data[$num]['entry_number'] =  '<b>TOTAL INFORME</b>';
		$data[$num]['entry_date'] = '';
		$data[$num]['narration'] = '';
		$data[$num]['amount_D'] = '<b>'.$this->functionscore->toCurrency('D', $total_dr).'</b>';
		$data[$num]['amount_C'] = '<b>'.$this->functionscore->toCurrency('C', $total_cr).'</b>';
		$data[$num]['saldo'] = $this->functionscore->toCurrency(($total_cl < 0 ? 'C' : 'D'), ($total_cl < 0 ? $total_cl * -1 : $total_cl));
		$data[$num]['state'] = '';
		if ($show_base) {
			$data[$num]['base'] = 0;
		}
		$num++;

		if ($action == "download") {

            if ($format=='pdf') {
                $this->data['datos'] = $data;
                $this->data['settings'] = $this->mAccountSettings;
                $this->data['POST'] = $_POST;
                $this->data['descargar'] = 1;
                $this->load->view('reports/pdf/ledgerstatement', $this->data);
            }
            if ($format=='pdfprint') {
                $this->data['datos'] = $data;
                $this->data['settings'] = $this->mAccountSettings;
                $this->data['POST'] = $_POST;
                $this->data['descargar'] = 0;
                $this->load->view('reports/pdf/ledgerstatement', $this->data);
            }
            if ($format=='xls') {

            	$filename = lang('page_title_reports_ledgerstatement');
				$this->load->library('excel');
	            $this->excel->setActiveSheetIndex(0);
	            $styleArray = array(
	                'borders' => array(
	                    'allborders' => array(
	                        'style' => PHPExcel_Style_Border::BORDER_THIN
	                    )
	                )
	            );
	            // $this->excel->getDefaultStyle()->applyFromArray($styleArray);
				$this->excel->getActiveSheet()->setTitle('balancesheet');
	            $this->excel->getActiveSheet()->SetCellValue('A1', $filename);
	            $this->excel->getActiveSheet()->mergeCells('A1:C1');
	            $this->excel->getActiveSheet()->SetCellValue('A2', 'Número');
	            $this->excel->getActiveSheet()->SetCellValue('B2', 'Fecha');
	            $this->excel->getActiveSheet()->SetCellValue('C2', 'Descripción');
	            $letter = 'D';
	            if ($show_base) {
	            	$this->excel->getActiveSheet()->SetCellValue($letter.'2', 'Base');
	            	$letter++;
	            }
	            $this->excel->getActiveSheet()->SetCellValue($letter.'2', 'Monto D');
	            $letter++;
	            $this->excel->getActiveSheet()->SetCellValue($letter.'2', 'Monto C');
	            $letter++;
	            $this->excel->getActiveSheet()->SetCellValue($letter.'2', 'Saldo');
	            $letter++;
	            foreach ($data as $row => $fila) {
	            	$this->row++;
	            	$this->excel->getActiveSheet()->SetCellValue('A'.$this->row, strip_tags($fila['entry_number']));
	            	$this->excel->getActiveSheet()->SetCellValue('B'.$this->row, strip_tags($fila['entry_date']));
	            	$this->excel->getActiveSheet()->SetCellValue('C'.$this->row, strip_tags($fila['narration']));
		            $letter = 'D';
		            if ($show_base) {
		            	$this->excel->getActiveSheet()->SetCellValue($letter.$this->row, $fila['base']);
		            	$letter++;
		            }
	            	$monto_D = $this->functionscore->drawAmount(strip_tags($fila['amount_D']));
	            	$monto_C = $this->functionscore->drawAmount(strip_tags($fila['amount_C']));
	            	$monto_saldo = $this->functionscore->drawAmount(strip_tags($fila['saldo']));
	            	$this->excel->getActiveSheet()->SetCellValue($letter.$this->row, $monto_D[1]);
	           	 	$letter++;
	            	$this->excel->getActiveSheet()->SetCellValue($letter.$this->row, $monto_C[1]);
	           	 	$letter++;
	            	$this->excel->getActiveSheet()->SetCellValue($letter.$this->row, $monto_saldo[1]);
	           	 	$letter++;
	            	if ($fila['entry_id'] == '') {
	            		$this->excel->getActiveSheet()->getStyle("A".$this->row.":F".$this->row)->getFont()->setBold(true);
	            	}
	            }

	            $this->excel->getActiveSheet()->getStyle('D1:D'.$this->row)->getNumberFormat()->setFormatCode("#,##0.00");
	            $this->excel->getActiveSheet()->getStyle('E1:E'.$this->row)->getNumberFormat()->setFormatCode("#,##0.00");
	            $this->excel->getActiveSheet()->getStyle('F1:F'.$this->row)->getNumberFormat()->setFormatCode("#,##0.00");
	            if ($show_base) {
	            	$this->excel->getActiveSheet()->getStyle('G1:G'.$this->row)->getNumberFormat()->setFormatCode("#,##0.00");
	            }
                header('Content-Type: application/vnd.ms-excel');
                header('Content-Disposition: attachment;filename="' . $filename . '.xls"');
                header('Cache-Control: max-age=0');
                $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
                $objWriter->save('php://output');
                exit();

            }

        } else {

        	foreach ($data as $row => $arr) {
				foreach ($arr as $key => $value) {
					$data[$row][$key] = utf8_encode($value);
				}
			}

        	$output = [
			'sEcho' => 1,
			'iTotalRecords' => count($data),
			'iTotalDisplayRecords' => count($data),
			'aaData' => $data
			];

			// echo json_encode($output);
			echo json_encode($output);
        }


	}


	function getEntriesReconciliation(){

		$conditions = array(); // conditions if any

		if (isset($_POST)) {
			if ($this->input->post('ledger_id') != 0) {
				$conditions['entryitems'.$this->DB1->dbsuffix.'.ledger_id'] = $this->input->post('ledger_id');
			} else {
				$conditions['entryitems'.$this->DB1->dbsuffix.'.ledger_id'] = 0;
			}

			if ($this->input->post('startdate') != "") {
				$conditions['entries'.$this->DB1->dbsuffix.'.date >='] = $this->functionscore->dateToSQL($this->input->post('startdate'));
			}

			if ($this->input->post('enddate') != "") {
				$conditions['entries'.$this->DB1->dbsuffix.'.date <='] = $this->functionscore->dateToSQL($this->input->post('enddate'));
			}

			if ($this->input->post('showall') == 0) { //si no se escogió mostrar todos los ledgers, cargamos sólo los que tengan fecha de reconciliación
				$conditions['entryitems'.$this->DB1->dbsuffix.'.reconciliation_date <>'] = '';
			}

		}



		// select all entries
		$this->DB1->select('entryitems'.$this->DB1->dbsuffix.'.ledger_id, entryitems'.$this->DB1->dbsuffix.'.reconciliation_date, entryitems'.$this->DB1->dbsuffix.'.id as eiid, entries'.$this->DB1->dbsuffix.'.*, entrytypes'.$this->DB1->dbsuffix.'.label as entryLabel, entrytypes'.$this->DB1->dbsuffix.'.name as entryName');
		$this->DB1->join('entrytypes'.$this->DB1->dbsuffix, 'entrytypes'.$this->DB1->dbsuffix.'.id = entries'.$this->DB1->dbsuffix.'.entrytype_id');
		$this->DB1->join('entryitems'.$this->DB1->dbsuffix, 'entryitems'.$this->DB1->dbsuffix.'.entry_id = entries'.$this->DB1->dbsuffix.'.id');
		$this->DB1->where($conditions);
		$this->DB1->group_by('entryitems'.$this->DB1->dbsuffix.'.entry_id');
		$query = $this->DB1->get('entries'.$this->DB1->dbsuffix);

		$sicual = $this->DB1->last_query();

		// echo $sicual;
		// pass an array of all entries to view page
		$entries = $query->result_array();

		$data = [];

		$num = 0;

		foreach ($entries as $row => $entry) {

			$entryTypeName = $entry['entryName'];
			$entryTypeLabel = $entry['entryLabel'];

			$data[$num]['entry_id'] = $entry['id'];
			$data[$num]['entryTypeLabel'] = $entry['entryLabel'];
			$data[$num]['date'] = $this->functionscore->dateFromSql($entry['date']);
			$data[$num]['number'] = $this->functionscore->toEntryNumber($entry['number'], $entry['entrytype_id']);
			$data[$num]['note'] = $entry['notes'];
			$data[$num]['type'] = $entryTypeName;
			$data[$num]['tag'] = $this->functionscore->showTag($entry['tag_id']);
			$data[$num]['dr'] = $this->functionscore->toCurrency('D', $entry['dr_total']);
			$data[$num]['cr'] = $this->functionscore->toCurrency('C', $entry['cr_total']);

			if ($entry['state'] == '1') {

				$data[$num]['state'] = '<p><span class="label label-primary">Aprobado</span><p>';

			} else if ($entry['state'] == '2') {

				$data[$num]['state'] = '<p><span class="label">Desaprobado</span></p>';

			} else if ($entry['state'] == '0') {

				$data[$num]['state'] = '<p><span class="label label-warning">Anulada</span></p>';

			}

			if ($entry['reconciliation_date']) {
				$dataSd = array(
					'name' => "ReportRec[".$row."][recdate]",
					'class' => 'recdate',
					'value' => $this->functionscore->dateFromSql($entry['reconciliation_date']),
				);
				$data[$num]['form'] = form_hidden("ReportRec[".$row."][id]", $entry['eiid']).form_input($dataSd);
			} else {
				$dataSd = array(
					'name' => "ReportRec[".$row."][recdate]",
					'class' => 'recdate',
				);
				$data[$num]['form'] = form_hidden("ReportRec[".$row."][id]", $entry['eiid']).form_input($dataSd);
			}

			$num++;

		}


	   $output = [
			'sEcho' => 1,
			'iTotalRecords' => count($data),
			'iTotalDisplayRecords' => count($data),
			'aaData' => $data
		];

		echo json_encode($output);
	}

	function getEntriesTA($action = NULL, $format = NULL){ //thirdsaccounts

		if (isset($_POST)) {

			$op_balance_amount = $this->input->post('op_balance_amount');
			$op_balance_dc = $this->input->post('op_balance_dc');

			$hide_totals = FALSE;

			if ($this->input->post('third_id') != 0) {
				$company = $this->input->post('third_id');
			} else {
				$company = 0;
			}

			$view_type = $this->input->post('view_type');
			
			$joinconditions = "";

			if ($this->input->post('skip_entries_disapproved') == "TRUE") {
				$skip_entries_disapproved = TRUE;
				$hide_totals = TRUE;
			} else {
				$joinconditions.='entries'.$this->DB1->dbsuffix.'.state != 0 ';
				$skip_entries_disapproved = FALSE;
			}

			if ($this->input->post('show_base') == "TRUE") {
				$show_base = TRUE;
			} else {
				$show_base = FALSE;
			}

			if ($this->input->post('startdate') != "") {
				$startdate = $this->functionscore->dateToSQL($this->input->post('startdate'));
				$hide_totals = TRUE;
			} else {
				$startdate = $this->mAccountSettings->fy_start;
				// $startdate = $this->mAccountSettings->fy_start;
			}

			if ($this->input->post('enddate') != "") {
				$enddate =$this->functionscore->dateToSQL($this->input->post('enddate'));
				$hide_totals = TRUE;
			} else {
				$enddate = $this->mAccountSettings->fy_end;
			}

			if ($this->input->post('entry_type') != "") {
				$entrytype = $this->input->post('entry_type');
				$hide_totals = TRUE;
			} else {
				$entrytype = NULL;
			}

			if ($this->input->post('cost_center') && $this->input->post('cost_center') != "") {
				$cost_center = $this->input->post('cost_center');
			} else {
				$cost_center = NULL;
			}

			if ($this->input->post('groupfrom') && $this->input->post('groupfrom') != "") {
				if ($this->input->post('groupfrom') < 0) {
					$this->DB1->where('id', abs($this->input->post('groupfrom')));
					$ledger = $this->DB1->get('groups'.$this->DB1->dbsuffix)->row_array();
				} else {
					$this->DB1->where('id', abs($this->input->post('groupfrom')));
					$ledger = $this->DB1->get('ledgers'.$this->DB1->dbsuffix)->row_array();
				}
				$startcode = $ledger['code'];
			} else {
				$startcode = NULL;
			}

			if ($this->input->post('groupto') && $this->input->post('groupto') != "") {
				if ($this->input->post('groupto') < 0) {
					$this->DB1->where('id', abs($this->input->post('groupto')));
					$ledger = $this->DB1->get('groups'.$this->DB1->dbsuffix)->row_array();
				} else {
					$this->DB1->where('id', abs($this->input->post('groupto')));
					$ledger = $this->DB1->get('ledgers'.$this->DB1->dbsuffix)->row_array();
				}
				$endcode = $ledger['code'];
			} else {
				$endcode = NULL;
			}

		}
		$op_sql_startcode = $sql_startcode = $startcode ? "code >= '{$startcode}'" : false;
		$op_sql_endcode = $sql_endcode = $endcode ? "code <= '{$endcode}'" : false;
		$sql_startdate = $startdate ? "date >= '{$startdate}'" : false;
		$sql_op_startdate = $startdate ? "date < '{$startdate}'" : false;
		$sql_enddate = $enddate ? "date <= '{$enddate}'" : false;
		$sql_entrytype = $entrytype ? "E.entrytype_id = {$entrytype}" : false;
		$sql_cost_center = $cost_center ? "EI.cost_center_id = {$cost_center}" : false;
		$sql_company = $company ? "EI.companies_id = {$company}" : false;
		$cop_sql_company = $company ? "C.id = {$company}" : false;
		$cop_where = $op_where = $where = "";


		$cop_sql_startcode = $startcode ? " AND L.code >= '{$startcode}'" : false;
		$cop_sql_endcode = $endcode ? " AND L.code <= '{$endcode}'" : false;
		$cop_sql_cost_center = $cost_center ? " AND COP.cost_center_id = {$cost_center}" : false;

		if ($sql_startcode || $sql_endcode || $sql_startdate || $sql_enddate || $sql_entrytype || $sql_cost_center || $sql_company) {
			$where = " WHERE ";
			if ($sql_endcode && ($sql_startcode)) {
				$sql_endcode = " AND ".$sql_endcode;
			}
			if ($sql_startdate && ($sql_startcode || $sql_endcode)) {
				$sql_startdate = " AND ".$sql_startdate;
			}
			if ($sql_op_startdate && ($sql_startcode || $sql_endcode)) {
				$sql_op_startdate = " AND ".$sql_op_startdate;
			}
			if ($sql_enddate && ($sql_startcode || $sql_endcode || $sql_startdate)) {
				$sql_enddate = " AND ".$sql_enddate;
			}
			if ($sql_entrytype && ($sql_startcode || $sql_endcode || $sql_startdate || $sql_enddate)) {
				$sql_entrytype = " AND ".$sql_entrytype;
			}
			if ($sql_cost_center && ($sql_startcode || $sql_endcode || $sql_startdate || $sql_enddate || $sql_entrytype)) {
				$sql_cost_center = " AND ".$sql_cost_center;
			}
			if ($sql_company && ($sql_startcode || $sql_endcode || $sql_startdate || $sql_enddate || $sql_entrytype || $sql_cost_center)) {
				$sql_company = " AND ".$sql_company;
			}
		}
		if ($op_sql_startcode || $op_sql_endcode) {
			$op_where = " WHERE ";
			if ($op_sql_endcode && ($op_sql_startcode)) {
				$op_sql_endcode = " AND ".$op_sql_endcode;
			}
		}

		if ($cop_sql_company) {
			$cop_where = " WHERE ";
		}

		$sql_ledgers = "SELECT 
							L.id,
						    L.code,
						    L.name
						FROM {$this->DB1->dbprefix('ledgers')}{$this->DB1->dbsuffix} L
							{$op_where} {$op_sql_startcode} {$op_sql_endcode}
						GROUP BY L.id
						ORDER BY L.code ASC;";

		$sql_ledgers_q = $this->DB1->query($sql_ledgers);
		$sql_ledgers_result = $sql_ledgers_q->result();

		$sql_order_by = $view_type == 1 ? "total_op.id ASC, total_op.ledger_code ASC" : "total_op.ledger_code ASC , total_op.id ASC";
		$sql_op_entries = $sql_startdate ? "UNION 
				SELECT
					EI.companies_id AS id,
				    C.vat_no,
				    C.name as company,
					EI.ledger_id AS ledger_id,
    				EI.companies_id AS id_companies,
    				L.code AS ledger_code,
				    SUM(COALESCE(IF(EI.dc = 'D', EI.amount, EI.amount * -1), 0)) AS op_balance
				FROM {$this->DB1->dbprefix('entryitems')}{$this->DB1->dbsuffix} EI
					LEFT JOIN {$this->DB1->dbprefix('entries')}{$this->DB1->dbsuffix} E ON E.id = EI.entry_id
				    LEFT JOIN {$this->DB1->dbprefix('entrytypes')}{$this->DB1->dbsuffix} ET ON ET.id = E.entrytype_id
					LEFT JOIN {$this->DB1->dbprefix('ledgers')}{$this->DB1->dbsuffix} L ON EI.ledger_id = L.id
					LEFT JOIN {$this->DB1->dbprefix('companies')} C ON EI.companies_id = C.id
					{$where} {$sql_startcode} {$sql_endcode} {$sql_op_startdate} {$sql_entrytype} {$sql_cost_center} {$sql_company}
				GROUP BY EI.companies_id, EI.ledger_id
					" : ""; 


		$sql_companies = "
					SELECT 
					    id,
					    vat_no,
					    company,
					    id_ledgers,
					    id_companies,
					    ledger_code,
					    SUM(COALESCE(op_balance, 0)) AS op_balance
					FROM
					(
						SELECT 
							C.id,
						    C.vat_no,
						    C.name as company,
						    COP.id_ledgers,
						    COP.id_companies,
						    L.code as ledger_code,
						    (COALESCE(IF(COP.op_balance_dc = 'D', COP.op_balance, (COP.op_balance*-1)),0)) AS op_balance
						FROM {$this->DB1->dbprefix('companies')} C
							LEFT JOIN {$this->DB1->dbprefix('companies_opbalance')}{$this->DB1->dbsuffix} COP ON COP.id_companies = C.id
							LEFT JOIN {$this->DB1->dbprefix('ledgers')}{$this->DB1->dbsuffix} L ON L.id = COP.id_ledgers {$cop_sql_cost_center} {$cop_sql_startcode} {$cop_sql_endcode}
						{$cop_where} {$cop_sql_company} 

					{$sql_op_entries}

					) AS total_op
					GROUP BY total_op.id , total_op.ledger_code
					ORDER BY {$sql_order_by};";

		$sql_companies_q = $this->DB1->query($sql_companies);
		$sql_companies_result = $sql_companies_q->result();

		$sql_order_by = $view_type == 1 ? "C.id ASC, L.code ASC" : "L.code ASC, C.id ASC";

		$sql_ledgers_entries = "SELECT
					EI.entry_id AS entry_id,
					EI.companies_id AS companies_id,
					L.id AS ledger_id,
				    L.code,
					CONCAT(ET.prefix, E.number) AS reference_no,
				    E.date,
				    EI.narration,
				    EI.base,
				    SUM(COALESCE(IF(EI.dc = 'D', EI.amount, 0), 0)) AS dr_total,
				    SUM(COALESCE(IF(EI.dc = 'C', EI.amount, 0), 0)) AS cr_total,
				    ET.label as entry_label,
				    E.number as entry_number,
				    E.state as entry_state,
				    C.name as company,
				    C.vat_no as vat_no
				FROM {$this->DB1->dbprefix('entryitems')}{$this->DB1->dbsuffix} EI
					LEFT JOIN {$this->DB1->dbprefix('entries')}{$this->DB1->dbsuffix} E ON E.id = EI.entry_id
				    LEFT JOIN {$this->DB1->dbprefix('entrytypes')}{$this->DB1->dbsuffix} ET ON ET.id = E.entrytype_id
					LEFT JOIN {$this->DB1->dbprefix('ledgers')}{$this->DB1->dbsuffix} L ON EI.ledger_id = L.id
					LEFT JOIN {$this->DB1->dbprefix('companies')} C ON EI.companies_id = C.id
					{$where} {$sql_startcode} {$sql_endcode} {$sql_startdate} {$sql_enddate} {$sql_entrytype} {$sql_cost_center} {$sql_company}
				GROUP BY EI.id
				ORDER BY {$sql_order_by}, E.date ASC, E.number ASC";

		$sql_ledgers_entries_q = $this->DB1->query($sql_ledgers_entries);
		$sql_ledgers_entries_result = $sql_ledgers_entries_q->result();

		$ledgers_data = [];
		foreach ($sql_ledgers_result as $ledger_row) {
			$ledgers_data[$ledger_row->id] = $ledger_row;
			$ledgers_data[$ledger_row->id]->total_op = 0;
		}

		$companies_data = [];
		$total_op = 0;
		foreach ($sql_companies_result as $company_row) {
			if (empty($company_row->ledger_code) || ($startcode && (String) $startcode > (String) $company_row->ledger_code) || ($endcode && (String) $endcode < (String) $company_row->ledger_code)) {
				$company_row->op_balance = 0;
				$company_row->id_ledgers = NULL;
			}
			if (!isset($companies_data[$company_row->id])) {
				$company_row->company = ($company_row->company);
				$companies_data[$company_row->id] = $company_row;
				$companies_data[$company_row->id]->total_op = $company_row->op_balance;
			} else {
				$companies_data[$company_row->id]->total_op += $company_row->op_balance;
			}
			if ($company_row->id_ledgers) {
				$companies_data[$company_row->id]->ledgers_op[$company_row->id_ledgers] = $company_row->op_balance;
				if (isset($ledgers_data[$company_row->id_ledgers])) {
					$ledgers_data[$company_row->id_ledgers]->companies_op[$company_row->id] = $company_row->op_balance;
					$ledgers_data[$company_row->id_ledgers]->total_op += $company_row->op_balance;
				}
			}
			$total_op += $company_row->op_balance;
		}
		// $this->functionscore->print_arrays($companies_data);
		$data = [];
		$total_cl = 0;
		$total_dr = 0;
		$total_cr = 0;
		$ledger_total = NULL;
		$ledger_total_op = NULL;
		$ledger_total_dr = NULL;
		$ledger_total_cr = NULL;
		$company_total = NULL;
		$company_total_dr = NULL;
		$company_total_cr = NULL;
		$ledger_id = NULL;
		$company_id = NULL;
		$num = 0;
		if ($view_type == 1) { // NIT Y CUENTAS
			/**/
			foreach ($sql_ledgers_entries_result as $entry_row) {
				if (!$entry_row->ledger_id) {
					$total_cr += ($entry_row->cr_total);
					$total_dr += ($entry_row->dr_total);
					continue;
				}
				if ($company_id == NULL || $company_id != $entry_row->companies_id) {
					if ($ledger_total !== NULL) {
						$ledger_row = $ledgers_data[$ledger_id];
						$data[$num]['level'] = 2;
						$data[$num]['entry_id'] = '';
						$data[$num]['entryLabel'] = '';
						$data[$num]['num_item'] = '';
						$data[$num]['entry_number'] =  '<b>CL BALANCE</b>';
						$data[$num]['entry_date'] =  '<b>'.$ledger_row->code.'</b>';
						$data[$num]['narration'] = '<b>'.$ledger_row->name.'</b>';
						$data[$num]['amount_D'] = '<b>'.$this->functionscore->toCurrency('D', $ledger_total_dr).'</b>';
						$data[$num]['amount_C'] = '<b>'.$this->functionscore->toCurrency('C', $ledger_total_cr).'</b>';
						$data[$num]['saldo'] = '<b>'.$this->functionscore->toCurrency(($ledger_total < 0 ? 'C' : 'D'), ($ledger_total < 0 ? $ledger_total * -1 : $ledger_total)).'</b>';
						$data[$num]['state'] = '';
						if ($show_base) {
							$data[$num]['base'] = 0;
						}
						$num++;
						$ledger_total = 0;
						$ledger_total_dr = 0;
						$ledger_total_cr = 0;
						if (isset($companies_data[$company_id]->ledgers_op[$ledger_id])) {
							unset($companies_data[$company_id]->ledgers_op[$ledger_id]);
						}
					}
					if ($company_total !== NULL) {
						if (isset($companies_data[$company_id]->ledgers_op) && count($companies_data[$company_id]->ledgers_op) > 0) {
							foreach ($companies_data[$company_id]->ledgers_op as $company_ledger_op_ledger_id => $company_ledger_op) {
								$data[$num]['level'] = 2;
								$data[$num]['entry_id'] = '';
								$data[$num]['entryLabel'] = '';
								$data[$num]['num_item'] = '';
								$data[$num]['entry_number'] =  '<b>OP BALANCE</b>';
								$data[$num]['entry_date'] =  '<b>'.$ledgers_data[$company_ledger_op_ledger_id]->code.'</b>';
								$data[$num]['narration'] = '<b>'.$ledgers_data[$company_ledger_op_ledger_id]->name.'</b>';
								$data[$num]['amount_D'] = '';
								$data[$num]['amount_C'] = '';
								$data[$num]['saldo'] = '<b>'.$this->functionscore->toCurrency(($company_ledger_op < 0 ? 'C' : 'D'), ($company_ledger_op < 0 ? $company_ledger_op * -1 : $company_ledger_op)).'</b>';
								$data[$num]['state'] = '';
								if ($show_base) {
									$data[$num]['base'] = 0;
								}
								$num++;
								$company_total += $company_ledger_op;
							}
						}
						$company_row = isset($companies_data[$company_id]) ? $companies_data[$company_id] : false;
						if ($company_row) {
							$data[$num]['level'] = 1;
							$data[$num]['entry_id'] = '';
							$data[$num]['entryLabel'] = '';
							$data[$num]['num_item'] = '';
							$data[$num]['entry_number'] =  '<b>CL BALANCE</b>';
							$data[$num]['entry_date'] =  '<b>'.$company_row->vat_no.'</b>';
							$data[$num]['narration'] = '<b>'.$company_row->company.'</b>';
							$data[$num]['amount_D'] = '<b>'.$this->functionscore->toCurrency('D', $company_total_dr).'</b>';
							$data[$num]['amount_C'] = '<b>'.$this->functionscore->toCurrency('C', $company_total_cr).'</b>';
							$data[$num]['saldo'] = '<b>'.$this->functionscore->toCurrency(($company_total < 0 ? 'C' : 'D'), ($company_total < 0 ? $company_total * -1 : $company_total)).'</b>';
							$data[$num]['state'] = '';
							if ($show_base) {
								$data[$num]['base'] = 0;
							}
							$num++;
							$company_total = 0;
							$company_total_dr = 0;
							$company_total_cr = 0;
							unset($companies_data[$company_id]);
						}
						$ledger_id = NULL;
					}
					$company_id = $entry_row->companies_id;
					$company_row = isset($companies_data[$company_id]) ? $companies_data[$company_id] : false;
					if ($company_row) {
						$data[$num]['level'] = 1;
						$data[$num]['entry_id'] = '';
						$data[$num]['num_item'] = '';
						$data[$num]['entryLabel'] = '';
						$data[$num]['entry_number'] = '';
						$data[$num]['entry_date'] = '<b>'.$company_row->vat_no.'</b>';
						$data[$num]['narration'] = '<b>'.$company_row->company.'</b>';
						$data[$num]['amount_D'] = '';
						$data[$num]['amount_C'] = '';
						$data[$num]['saldo'] = '<b>'.$this->functionscore->toCurrency(($company_row->total_op < 0 ? 'C' : 'D'), ($company_row->total_op < 0 ? $company_row->total_op * -1 : $company_row->total_op)).'</b>';
						$data[$num]['state'] = '';
						if ($show_base) {
							$data[$num]['base'] = 0;
						}
						$company_total = 0;
						$num++;
					}
				}
				if ($ledger_id == NULL || $ledger_id != $entry_row->ledger_id) {
					if ($ledger_total !== NULL) {
						$ledger_row = isset($ledgers_data[$ledger_id]) ? $ledgers_data[$ledger_id] : NULL;
						if ($ledger_row) {
							$data[$num]['level'] = 2;
							$data[$num]['entry_id'] = '';
							$data[$num]['entryLabel'] = '';
							$data[$num]['num_item'] = '';
							$data[$num]['entry_number'] =  '<b>CL BALANCE</b>';
							$data[$num]['entry_date'] =  '<b>'.$ledger_row->code.'</b>';
							$data[$num]['narration'] = '<b>'.$ledger_row->name.'</b>';
							$data[$num]['amount_D'] = '<b>'.$this->functionscore->toCurrency('D', $ledger_total_dr).'</b>';
							$data[$num]['amount_C'] = '<b>'.$this->functionscore->toCurrency('C', $ledger_total_cr).'</b>';
							$data[$num]['saldo'] = '<b>'.$this->functionscore->toCurrency(($ledger_total < 0 ? 'C' : 'D'), ($ledger_total < 0 ? $ledger_total * -1 : $ledger_total)).'</b>';
							$data[$num]['state'] = '';
							if ($show_base) {
								$data[$num]['base'] = 0;
							}
							$num++;
						}
						$ledger_total = 0;
						$ledger_total_dr = 0;
						$ledger_total_cr = 0;
						if (isset($companies_data[$company_id]->ledgers_op[$ledger_id])) {
							unset($companies_data[$company_id]->ledgers_op[$ledger_id]);
						}
					}
					$ledger_id = $entry_row->ledger_id;
					$ledger_row = isset($ledgers_data[$ledger_id]) ? $ledgers_data[$ledger_id] : false;
					$data[$num]['level'] = 2;
					$data[$num]['entry_id'] = '';
					$data[$num]['num_item'] = '';
					$data[$num]['entryLabel'] = '';
					$data[$num]['entry_number'] = '<b>OP BALANCE</b>';
					$data[$num]['entry_date'] = '<b>'.$ledger_row->code.'</b>';
					$data[$num]['narration'] = '<b>'.$ledger_row->name.'</b>';
					$data[$num]['amount_D'] = '';
					$data[$num]['amount_C'] = '';
					$ledger_company_opbalance = isset($ledger_row->companies_op[$company_id]) ? $ledger_row->companies_op[$company_id] : 0;
					$data[$num]['saldo'] = '<b>'.$this->functionscore->toCurrency(($ledger_company_opbalance < 0 ? 'C' : 'D'), ($ledger_company_opbalance < 0 ? $ledger_company_opbalance * -1 : $ledger_company_opbalance)).'</b>';
					$data[$num]['state'] = '';
					if ($show_base) {
						$data[$num]['base'] = 0;
					}
					$ledger_total = $ledger_company_opbalance;
					$company_total += $ledger_company_opbalance;
					$num++;
				}
				$ledger_data = isset($ledgers_data[$entry_row->ledger_id]) ? $ledgers_data[$entry_row->ledger_id] : false;
				$data[$num]['level'] = 4;
				$data[$num]['entry_id'] = $entry_row->entry_id;
				$data[$num]['num_item'] = $num+1;
				$data[$num]['entryLabel'] = $entry_row->entry_label;
				$data[$num]['entry_number'] = $entry_row->reference_no;
				$data[$num]['entry_date'] = ''.$entry_row->date.'';
				$data[$num]['narration'] = ''.$entry_row->narration.'';
				$data[$num]['amount_D'] = $this->functionscore->toCurrency('D', $entry_row->dr_total);
				$data[$num]['amount_C'] = $this->functionscore->toCurrency('C', $entry_row->cr_total);
				$ledger_total += ($entry_row->dr_total - $entry_row->cr_total);
				$ledger_total_cr += ($entry_row->cr_total);
				$ledger_total_dr += ($entry_row->dr_total);
				$company_total += ($entry_row->dr_total - $entry_row->cr_total);
				$company_total_cr += ($entry_row->cr_total);
				$company_total_dr += ($entry_row->dr_total);
				$total_cr += ($entry_row->cr_total);
				$total_dr += ($entry_row->dr_total);
				$data[$num]['saldo'] = ''.$this->functionscore->toCurrency(($company_total < 0 ? 'C' : 'D'), ($company_total < 0 ? $company_total * -1 : $company_total)).'';
				if ($entry_row->entry_state == '1') {
					$data[$num]['state'] = 'Aprobado';
				} else if ($entry_row->entry_state == '2') {
					$data[$num]['state'] = 'Desaprobado';
				} else if ($entry_row->entry_state == '0') {
					$data[$num]['state'] = 'Anulada';
				}
				if ($show_base) {
					$data[$num]['base'] = $entry_row->base;
				}
				$num++;
			}

			if ($ledger_total != 0) {
				$ledger_row = isset($ledgers_data[$ledger_id]) ? $ledgers_data[$ledger_id] : false;
				$data[$num]['level'] = 2;
				$data[$num]['entry_id'] = '';
				$data[$num]['entryLabel'] = '';
				$data[$num]['num_item'] = '';
				$data[$num]['entry_number'] =  '';
				$data[$num]['entry_date'] =  '<b>'.($ledger_row ? $ledger_row->code : '').'</b>';
				$data[$num]['narration'] = '<b>'.($ledger_row ? $ledger_row->name : '').'</b>';
				$data[$num]['amount_D'] = '<b>'.$this->functionscore->toCurrency('D', $ledger_total_dr).'</b>';
				$data[$num]['amount_C'] = '<b>'.$this->functionscore->toCurrency('C', $ledger_total_cr).'</b>';
				$data[$num]['saldo'] = $this->functionscore->toCurrency(($ledger_total < 0 ? 'C' : 'D'), ($ledger_total < 0 ? $ledger_total * -1 : $ledger_total));
				$data[$num]['state'] = '';
				if ($show_base) {
					$data[$num]['base'] = 0;
				}
				$num++;
				if (isset($companies_data[$company_id]->ledgers_op[$ledger_id])) {
					unset($companies_data[$company_id]->ledgers_op[$ledger_id]);
				}
			}
			if ($company_total != 0) {
				if (isset($companies_data[$company_id]->ledgers_op) && count($companies_data[$company_id]->ledgers_op) > 0) {
					foreach ($companies_data[$company_id]->ledgers_op as $company_ledger_op_ledger_id => $company_ledger_op) {
						$data[$num]['level'] = 2;
						$data[$num]['entry_id'] = '';
						$data[$num]['entryLabel'] = '';
						$data[$num]['num_item'] = '';
						$data[$num]['entry_number'] =  '<b>OP BALANCE</b>';
						$data[$num]['entry_date'] =  '<b>'.$ledgers_data[$company_ledger_op_ledger_id]->code.'</b>';
						$data[$num]['narration'] = '<b>'.$ledgers_data[$company_ledger_op_ledger_id]->name.'</b>';
						$data[$num]['amount_D'] = '';
						$data[$num]['amount_C'] = '';
						$data[$num]['saldo'] = '<b>'.$this->functionscore->toCurrency(($company_ledger_op < 0 ? 'C' : 'D'), ($company_ledger_op < 0 ? $company_ledger_op * -1 : $company_ledger_op)).'</b>';
						$data[$num]['state'] = '';
						if ($show_base) {
							$data[$num]['base'] = 0;
						}
						$num++;
						$company_total += $company_ledger_op;
					}
				}
				$company_row = isset($companies_data[$company_id]) ? $companies_data[$company_id] : false;
				if ($company_row) {
					$data[$num]['level'] = 1;
					$data[$num]['entry_id'] = '';
					$data[$num]['entryLabel'] = '';
					$data[$num]['num_item'] = '';
					$data[$num]['entry_number'] =  '<b>CL BALANCE</b>';
					$data[$num]['entry_date'] =  '<b>'.$company_row->vat_no.'</b>';
					$data[$num]['narration'] = '<b>'.$company_row->company.'</b>';
					$data[$num]['amount_D'] = '<b>'.$this->functionscore->toCurrency('D', $company_total_dr).'</b>';
					$data[$num]['amount_C'] = '<b>'.$this->functionscore->toCurrency('C', $company_total_cr).'</b>';
					$data[$num]['saldo'] = '<b>'.$this->functionscore->toCurrency(($company_total < 0 ? 'C' : 'D'), ($company_total < 0 ? $company_total * -1 : $company_total)).'</b>';
					$data[$num]['state'] = '';
					if ($show_base) {
						$data[$num]['base'] = 0;
					}
					$num++;
					unset($companies_data[$company_id]);
				}
			}
			if (count($companies_data) > 0) {
				foreach ($companies_data as $company_id => $company) {
					if ($company->total_op == 0) {
						continue;
					}
					$company_cr = $company_dr = $company_total = 0;
					$data[$num]['level'] = 1;
					$data[$num]['entry_id'] = '';
					$data[$num]['entryLabel'] = '';
					$data[$num]['num_item'] = '';
					$data[$num]['entry_number'] = '';
					$data[$num]['entry_date'] = '<b>'.$company->vat_no.'</b>';
					$data[$num]['narration'] = '<b>'.$company->company.'</b>';
					$data[$num]['amount_D'] = '';
					$data[$num]['amount_C'] = '';
					$data[$num]['saldo'] = '<b>'.$this->functionscore->toCurrency(($company->total_op < 0 ? 'C' : 'D'), ($company->total_op < 0 ? $company->total_op * -1 : $company->total_op)).'</b>';
					$data[$num]['state'] = '';
					if ($show_base) {
						$data[$num]['base'] = 0;
					}
					$num++;
					if (isset($company->ledgers_op)) {
						foreach ($company->ledgers_op as $company_ledger_op_ledger_id => $company_ledger_op) {
							$data[$num]['level'] = 2;
							$data[$num]['entry_id'] = '';
							$data[$num]['entryLabel'] = '';
							$data[$num]['num_item'] = '';
							$data[$num]['entry_number'] = '<b>OP BALANCE</b>';
							$data[$num]['entry_date'] = isset($ledgers_data[$company_ledger_op_ledger_id]) ? $ledgers_data[$company_ledger_op_ledger_id]->code : "ERROR LEDGER ID ".$company_ledger_op_ledger_id;
							$data[$num]['narration'] = isset($ledgers_data[$company_ledger_op_ledger_id]) ? $ledgers_data[$company_ledger_op_ledger_id]->name : "ERROR LEDGER ID ".$company_ledger_op_ledger_id;
							$data[$num]['amount_D'] = '<b>'.$this->functionscore->toCurrency(($company_ledger_op > 0 ? 'D' : ''), ($company_ledger_op > 0 ? $company_ledger_op : 0)).'</b>';
							$data[$num]['amount_C'] = '<b>'.$this->functionscore->toCurrency(($company_ledger_op < 0 ? 'C' : ''), ($company_ledger_op < 0 ? $company_ledger_op * -1 : 0)).'</b>';
							$data[$num]['state'] = '';
							if ($show_base) {
								$data[$num]['base'] = 0;
							}
							$company_cr += ($company_ledger_op < 0 ? $company_ledger_op * -1 : 0);
							$company_dr += ($company_ledger_op > 0 ? $company_ledger_op : 0);
							$company_total += $company_ledger_op;
							$data[$num]['saldo'] = '<b>'.$this->functionscore->toCurrency(($company_total < 0 ? 'C' : 'D'), ($company_total < 0 ? $company_total * -1 : $company_total)).'</b>';
							$num++;
						}
					}
					$data[$num]['level'] = 1;
					$data[$num]['entry_id'] = '';
					$data[$num]['entryLabel'] = '';
					$data[$num]['num_item'] = '';
					$data[$num]['entry_number'] = '';
					$data[$num]['entry_date'] = '<b>'.$company->vat_no.'</b>';
					$data[$num]['narration'] = '<b>'.$company->company.'</b>';
					$data[$num]['amount_D'] = '<b>'.$this->functionscore->toCurrency('D', $company_dr).'</b>';
					$data[$num]['amount_C'] = '<b>'.$this->functionscore->toCurrency('C', $company_cr).'</b>';
					$data[$num]['saldo'] = '<b>'.$this->functionscore->toCurrency(($company_total < 0 ? 'C' : 'D'), ($company_total < 0 ? $company_total * -1 : $company_total)).'</b>';
					$data[$num]['state'] = '';
					if ($show_base) {
						$data[$num]['base'] = 0;
					}
					$num++;
				}
			}

			$data[$num]['level'] = 1;
			$data[$num]['entry_id'] = '';
			$data[$num]['entryLabel'] = '';
			$data[$num]['num_item'] = $num;
			$data[$num]['entry_number'] =  '<b>TOTAL INFORME</b>';
			$data[$num]['entry_date'] = '';
			$data[$num]['narration'] = '';
			$data[$num]['amount_D'] = '<b>'.$this->functionscore->toCurrency('D', $total_dr).'</b>';
			$data[$num]['amount_C'] = '<b>'.$this->functionscore->toCurrency('C', $total_cr).'</b>';
			$total_cl = ($total_dr - $total_cr) + $total_op;
			$data[$num]['saldo'] = $this->functionscore->toCurrency(($total_cl < 0 ? 'C' : 'D'), ($total_cl < 0 ? $total_cl * -1 : $total_cl));
			$data[$num]['state'] = '';
			if ($show_base) {
				$data[$num]['base'] = 0;
			}
			$num++;
			/**/
		} else if ($view_type == 2) { // CUENTAS Y NIT
			/**/
			foreach ($sql_ledgers_entries_result as $entry_row) {
				if ($ledger_id == NULL || $ledger_id != $entry_row->ledger_id) {
					if ($company_total !== NULL && isset($companies_data[$company_id])) {
						$company_row = $companies_data[$company_id];
						$data[$num]['level'] = 2;
						$data[$num]['entry_id'] = '';
						$data[$num]['entryLabel'] = '';
						$data[$num]['num_item'] = '';
						$data[$num]['entry_number'] =  '<b>CL BALANCE</b>';
						$data[$num]['entry_date'] =  '<b>'.$company_row->vat_no.'</b>';
						$data[$num]['narration'] = '<b>'.$company_row->company.'</b>';
						$data[$num]['amount_D'] = '<b>'.$this->functionscore->toCurrency('D', $company_total_dr).'</b>';
						$data[$num]['amount_C'] = '<b>'.$this->functionscore->toCurrency('C', $company_total_cr).'</b>';
						$data[$num]['saldo'] = '<b>'.$this->functionscore->toCurrency(($company_total < 0 ? 'C' : 'D'), ($company_total < 0 ? $company_total * -1 : $company_total)).'</b>';
						$data[$num]['state'] = '';
						if ($show_base) {
							$data[$num]['base'] = 0;
						}
						unset($ledgers_data[$ledger_id]->companies_op[$company_id]);
						$num++;
					}
					$company_total = 0;
					$company_total_dr = 0;
					$company_total_cr = 0;
					if ($ledger_total !== NULL) {
						if (isset($ledgers_data[$ledger_id]->companies_op) && count($ledgers_data[$ledger_id]->companies_op) > 0) {
							foreach ($ledgers_data[$ledger_id]->companies_op as $ledger_company_op_company_id => $ledger_company_op) {
								$data[$num]['level'] = 2;
								$data[$num]['entry_id'] = '';
								$data[$num]['entryLabel'] = '';
								$data[$num]['num_item'] = '';
								$data[$num]['entry_number'] =  '<b>OP BALANCE</b>';
								$data[$num]['entry_date'] =  '<b>'.$companies_data[$ledger_company_op_company_id]->vat_no.'</b>';
								$data[$num]['narration'] = '<b>'.$companies_data[$ledger_company_op_company_id]->company.'</b>';
								$data[$num]['amount_D'] = '';
								$data[$num]['amount_C'] = '';
								$data[$num]['saldo'] = '<b>'.$this->functionscore->toCurrency(($ledger_company_op < 0 ? 'C' : 'D'), ($ledger_company_op < 0 ? $ledger_company_op * -1 : $ledger_company_op)).'</b>';
								$data[$num]['state'] = '';
								if ($show_base) {
									$data[$num]['base'] = 0;
								}
								$num++;
								$ledger_total += $ledger_company_op;
							}
						}
						$ledger_row = $ledgers_data[$ledger_id];
						$data[$num]['level'] = 1;
						$data[$num]['entry_id'] = '';
						$data[$num]['entryLabel'] = '';
						$data[$num]['num_item'] = '';
						$data[$num]['entry_number'] =  '<b>CL BALANCE</b>';
						$data[$num]['entry_date'] =  '<b>'.$ledger_row->code.'</b>';
						$data[$num]['narration'] = '<b>'.$ledger_row->name.'</b>';
						$data[$num]['amount_D'] = '<b>'.$this->functionscore->toCurrency('D', $ledger_total_dr).'</b>';
						$data[$num]['amount_C'] = '<b>'.$this->functionscore->toCurrency('C', $ledger_total_cr).'</b>';
						$data[$num]['saldo'] = '<b>'.$this->functionscore->toCurrency(($ledger_total < 0 ? 'C' : 'D'), ($ledger_total < 0 ? $ledger_total * -1 : $ledger_total)).'</b>';
						$data[$num]['state'] = '';
						if ($show_base) {
							$data[$num]['base'] = 0;
						}
						$num++;
						$ledger_total = 0;
						$ledger_total_dr = 0;
						$ledger_total_cr = 0;
						unset($ledgers_data[$ledger_id]);
						$company_id = null;
					}
					$ledger_id = $entry_row->ledger_id;
					$ledger_row = $ledgers_data[$ledger_id];
					$data[$num]['level'] = 1;
					$data[$num]['entry_id'] = '';
					$data[$num]['num_item'] = '';
					$data[$num]['entryLabel'] = '';
					$data[$num]['entry_number'] = '';
					$data[$num]['entry_date'] = '<b>'.$ledger_row->code.'</b>';
					$data[$num]['narration'] = '<b>'.$ledger_row->name.'</b>';
					$data[$num]['amount_D'] = '';
					$data[$num]['amount_C'] = '';
					$data[$num]['saldo'] = '<b>'.$this->functionscore->toCurrency(($ledger_row->total_op < 0 ? 'C' : 'D'), ($ledger_row->total_op < 0 ? $ledger_row->total_op * -1 : $ledger_row->total_op)).'</b>';
					$data[$num]['state'] = '';
					if ($show_base) {
						$data[$num]['base'] = 0;
					}
					$ledger_total = 0;
					$num++;
				}
				if ($company_id == NULL || $company_id != $entry_row->companies_id) {
					if ($company_total !== NULL) {
						$company_row = isset($companies_data[$company_id]) ? $companies_data[$company_id] : false;
						if ($company_row) {
							$data[$num]['level'] = 2;
							$data[$num]['entry_id'] = '';
							$data[$num]['entryLabel'] = '';
							$data[$num]['num_item'] = '';
							$data[$num]['entry_number'] =  '<b>CL BALANCE</b>';
							$data[$num]['entry_date'] =  '<b>'.$company_row->vat_no.'</b>';
							$data[$num]['narration'] = '<b>'.$company_row->company.'</b>';
							$data[$num]['amount_D'] = '<b>'.$this->functionscore->toCurrency('D', $company_total_dr).'</b>';
							$data[$num]['amount_C'] = '<b>'.$this->functionscore->toCurrency('C', $company_total_cr).'</b>';
							$data[$num]['saldo'] = '<b>'.$this->functionscore->toCurrency(($company_total < 0 ? 'C' : 'D'), ($company_total < 0 ? $company_total * -1 : $company_total)).'</b>';
							$data[$num]['state'] = '';
							if ($show_base) {
								$data[$num]['base'] = 0;
							}
							$num++;
							$company_total = 0;
							$company_total_dr = 0;
							$company_total_cr = 0;
						}	
						unset($ledgers_data[$ledger_id]->companies_op[$company_id]);
					}
					$company_id = $entry_row->companies_id;
					$company_row = isset($companies_data[$company_id]) ? $companies_data[$company_id] : false;
					if ($company_row) {
						$data[$num]['level'] = 2;
						$data[$num]['entry_id'] = '';
						$data[$num]['num_item'] = '';
						$data[$num]['entryLabel'] = '';
						$data[$num]['entry_number'] = '<b>OP BALANCE</b>';
						$data[$num]['entry_date'] = '<b>'.$company_row->vat_no.'</b>';
						$data[$num]['narration'] = '<b>'.$company_row->company.'</b>';
						$data[$num]['amount_D'] = '';
						$data[$num]['amount_C'] = '';
						$company_ledger_opbalance = isset($company_row->ledgers_op[$ledger_id]) ? $company_row->ledgers_op[$ledger_id] : 0;
						$data[$num]['saldo'] = '<b>'.$this->functionscore->toCurrency(($company_ledger_opbalance < 0 ? 'C' : 'D'), ($company_ledger_opbalance < 0 ? $company_ledger_opbalance * -1 : $company_ledger_opbalance)).'</b>';
						$data[$num]['state'] = '';
						if ($show_base) {
							$data[$num]['base'] = 0;
						}
						$company_total = $company_ledger_opbalance;
						$ledger_total += $company_ledger_opbalance;
						$num++;
					}
				}

				if (!$entry_row->ledger_id) {
					$total_cr += ($entry_row->cr_total);
					$total_dr += ($entry_row->dr_total);
					continue;
				}
				$company_data = isset($companies_data[$entry_row->companies_id]) ? $companies_data[$entry_row->companies_id] : false;
				$data[$num]['level'] = 4;
				$data[$num]['entry_id'] = $entry_row->entry_id;
				$data[$num]['num_item'] = $num+1;
				$data[$num]['entryLabel'] = $entry_row->entry_label;
				$data[$num]['entry_number'] = $entry_row->reference_no;
				$data[$num]['entry_date'] = ''.$entry_row->date.'';
				$data[$num]['narration'] = ''.$entry_row->narration.'';
				$data[$num]['amount_D'] = $this->functionscore->toCurrency('D', $entry_row->dr_total);
				$data[$num]['amount_C'] = $this->functionscore->toCurrency('C', $entry_row->cr_total);
				$ledger_total += ($entry_row->dr_total - $entry_row->cr_total);
				$ledger_total_cr += ($entry_row->cr_total);
				$ledger_total_dr += ($entry_row->dr_total);
				$company_total += ($entry_row->dr_total - $entry_row->cr_total);
				$company_total_cr += ($entry_row->cr_total);
				$company_total_dr += ($entry_row->dr_total);
				$total_cr += ($entry_row->cr_total);
				$total_dr += ($entry_row->dr_total);
				$data[$num]['saldo'] = ''.$this->functionscore->toCurrency(($company_total < 0 ? 'C' : 'D'), ($company_total < 0 ? $company_total * -1 : $company_total)).'';
				if ($entry_row->entry_state == '1') {
					$data[$num]['state'] = 'Aprobado';
				} else if ($entry_row->entry_state == '2') {
					$data[$num]['state'] = 'Desaprobado';
				} else if ($entry_row->entry_state == '0') {
					$data[$num]['state'] = 'Anulada';
				}
				if ($show_base) {
					$data[$num]['base'] = $entry_row->base;
				}
				$num++;
			}
			if ($company_total != 0) {
				$company_row = isset($companies_data[$company_id]) ? $companies_data[$company_id] : false;
				if ($company_row) {
					$data[$num]['level'] = 2;
					$data[$num]['entry_id'] = '';
					$data[$num]['entryLabel'] = '';
					$data[$num]['num_item'] = '';
					$data[$num]['entry_number'] =  '';
					$data[$num]['entry_date'] =  '<b>'.$company_row->vat_no.'</b>';
					$data[$num]['narration'] = '<b>'.$company_row->company.'</b>';
					$data[$num]['amount_D'] = '<b>'.$this->functionscore->toCurrency('D', $company_total_dr).'</b>';
					$data[$num]['amount_C'] = '<b>'.$this->functionscore->toCurrency('C', $company_total_cr).'</b>';
					$data[$num]['saldo'] = $this->functionscore->toCurrency(($company_total < 0 ? 'C' : 'D'), ($company_total < 0 ? $company_total * -1 : $company_total));
					$data[$num]['state'] = '';
					if ($show_base) {
						$data[$num]['base'] = 0;
					}
					$num++;
				}	
				unset($ledgers_data[$ledger_id]->companies_op[$company_id]);
			}
			if ($ledger_total != 0) {
				if (isset($ledgers_data[$ledger_id]->companies_op) && count($ledgers_data[$ledger_id]->companies_op) > 0) {
					foreach ($ledgers_data[$ledger_id]->companies_op as $ledger_company_op_company_id => $ledger_company_op) {
						$data[$num]['level'] = 2;
						$data[$num]['entry_id'] = '';
						$data[$num]['entryLabel'] = '';
						$data[$num]['num_item'] = '';
						$data[$num]['entry_number'] =  '<b>OP BALANCE</b>';
						$data[$num]['entry_date'] =  '<b>'.$companies_data[$ledger_company_op_company_id]->vat_no.'</b>';
						$data[$num]['narration'] = '<b>'.$companies_data[$ledger_company_op_company_id]->company.'</b>';
						$data[$num]['amount_D'] = '';
						$data[$num]['amount_C'] = '';
						$data[$num]['saldo'] = '<b>'.$this->functionscore->toCurrency(($ledger_company_op < 0 ? 'C' : 'D'), ($ledger_company_op < 0 ? $ledger_company_op * -1 : $ledger_company_op)).'</b>';
						$data[$num]['state'] = '';
						if ($show_base) {
							$data[$num]['base'] = 0;
						}
						$num++;
						$ledger_total += $ledger_company_op;
					}
				}
				$ledger_row = $ledgers_data[$ledger_id];
				$data[$num]['level'] = 1;
				$data[$num]['entry_id'] = '';
				$data[$num]['entryLabel'] = '';
				$data[$num]['num_item'] = '';
				$data[$num]['entry_number'] =  '';
				$data[$num]['entry_date'] =  '<b>'.$ledger_row->code.'</b>';
				$data[$num]['narration'] = '<b>'.$ledger_row->name.'</b>';
				$data[$num]['amount_D'] = '<b>'.$this->functionscore->toCurrency('D', $ledger_total_dr).'</b>';
				$data[$num]['amount_C'] = '<b>'.$this->functionscore->toCurrency('C', $ledger_total_cr).'</b>';
				$data[$num]['saldo'] = $this->functionscore->toCurrency(($ledger_total < 0 ? 'C' : 'D'), ($ledger_total < 0 ? $ledger_total * -1 : $ledger_total));
				$data[$num]['state'] = '';
				if ($show_base) {
					$data[$num]['base'] = 0;
				}
				$num++;
				unset($ledgers_data[$ledger_id]);
			}
			if (count($ledgers_data) > 0) {
				foreach ($ledgers_data as $ledger_id => $ledger) {
					if ($ledger->total_op == 0) {
						continue;
					}
					$ledger_cr = $ledger_dr = $ledger_total = 0;
					$data[$num]['level'] = 1;
					$data[$num]['entry_id'] = '';
					$data[$num]['entryLabel'] = '';
					$data[$num]['num_item'] = '';
					$data[$num]['entry_number'] = '';
					$data[$num]['entry_date'] = '<b>'.$ledger->code.'</b>';
					$data[$num]['narration'] = '<b>'.$ledger->name.'</b>';
					$data[$num]['amount_D'] = '';
					$data[$num]['amount_C'] = '';
					$data[$num]['saldo'] = '<b>'.$this->functionscore->toCurrency(($ledger->total_op < 0 ? 'C' : 'D'), ($ledger->total_op < 0 ? $ledger->total_op * -1 : $ledger->total_op)).'</b>';
					$data[$num]['state'] = '';
					if ($show_base) {
						$data[$num]['base'] = 0;
					}
					$num++;
					if (isset($ledger->companies_op)) {
						foreach ($ledger->companies_op as $ledger_company_op_company_id => $ledger_company_op) {
							$data[$num]['level'] = 2;
							$data[$num]['entry_id'] = '';
							$data[$num]['entryLabel'] = '';
							$data[$num]['num_item'] = '';
							$data[$num]['entry_number'] = '<b>OP BALANCE</b>';
							$data[$num]['entry_date'] = $companies_data[$ledger_company_op_company_id]->vat_no;
							$data[$num]['narration'] = $companies_data[$ledger_company_op_company_id]->company;
							$data[$num]['amount_D'] = '<b>'.$this->functionscore->toCurrency(($ledger_company_op > 0 ? 'D' : ''), ($ledger_company_op > 0 ? $ledger_company_op : 0)).'</b>';
							$data[$num]['amount_C'] = '<b>'.$this->functionscore->toCurrency(($ledger_company_op < 0 ? 'C' : ''), ($ledger_company_op < 0 ? $ledger_company_op * -1 : 0)).'</b>';
							$data[$num]['state'] = '';
							if ($show_base) {
								$data[$num]['base'] = 0;
							}
							$ledger_cr += ($ledger_company_op < 0 ? $ledger_company_op * -1 : 0);
							$ledger_dr += ($ledger_company_op > 0 ? $ledger_company_op : 0);
							$ledger_total += $ledger_company_op;
							$data[$num]['saldo'] = '<b>'.$this->functionscore->toCurrency(($ledger_total < 0 ? 'C' : 'D'), ($ledger_total < 0 ? $ledger_total * -1 : $ledger_total)).'</b>';
							$num++;
						}
					}
					$data[$num]['level'] = 1;
					$data[$num]['entry_id'] = '';
					$data[$num]['entryLabel'] = '';
					$data[$num]['num_item'] = '';
					$data[$num]['entry_number'] = '';
					$data[$num]['entry_date'] = '<b>'.$ledger->code.'</b>';
					$data[$num]['narration'] = '<b>'.$ledger->name.'</b>';
					$data[$num]['amount_D'] = '<b>'.$this->functionscore->toCurrency('D', $ledger_dr).'</b>';
					$data[$num]['amount_C'] = '<b>'.$this->functionscore->toCurrency('C', $ledger_cr).'</b>';
					$data[$num]['saldo'] = '<b>'.$this->functionscore->toCurrency(($ledger_total < 0 ? 'C' : 'D'), ($ledger_total < 0 ? $ledger_total * -1 : $ledger_total)).'</b>';
					$data[$num]['state'] = '';
					if ($show_base) {
						$data[$num]['base'] = 0;
					}
					$num++;
				}
			}
			$data[$num]['level'] = 1;
			$data[$num]['entry_id'] = '';
			$data[$num]['entryLabel'] = '';
			$data[$num]['num_item'] = $num;
			$data[$num]['entry_number'] =  '<b>TOTAL INFORME</b>';
			$data[$num]['entry_date'] = '';
			$data[$num]['narration'] = '';
			$data[$num]['amount_D'] = '<b>'.$this->functionscore->toCurrency('D', $total_dr).'</b>';
			$data[$num]['amount_C'] = '<b>'.$this->functionscore->toCurrency('C', $total_cr).'</b>';
			$total_cl = ($total_dr - $total_cr) + $total_op;
			$data[$num]['saldo'] = $this->functionscore->toCurrency(($total_cl < 0 ? 'C' : 'D'), ($total_cl < 0 ? $total_cl * -1 : $total_cl));
			$data[$num]['state'] = '';
			if ($show_base) {
				$data[$num]['base'] = 0;
			}
			$num++;
			/**/
		} else if ($view_type == 3) { // CUENTAS Y NIT
			/**/
			$total_close = 0;
			foreach ($sql_ledgers_entries_result as $entry_row) {
				if (!$entry_row->ledger_id) {
					$total_cr += ($entry_row->cr_total);
					$total_dr += ($entry_row->dr_total);
					continue;
				}
				if ($ledger_id == NULL || $ledger_id != $entry_row->ledger_id) {
					if ($company_total !== NULL) {
						$company_total = 0;
						$company_total_dr = 0;
						$company_total_cr = 0;
						unset($ledgers_data[$ledger_id]->companies_op[$company_id]);
					}
					if ($ledger_total !== NULL) {
						if (isset($ledgers_data[$ledger_id]->companies_op) && count($ledgers_data[$ledger_id]->companies_op) > 0) {
							foreach ($ledgers_data[$ledger_id]->companies_op as $ledger_company_op_company_id => $ledger_company_op) {
								$total_close += $ledger_company_op;
								$data[$num]['vat_no'] = $companies_data[$ledger_company_op_company_id]->vat_no;
								$data[$num]['third_name'] = $companies_data[$ledger_company_op_company_id]->company ;
								$data[$num]['level'] = 4;
								$data[$num]['entry_id'] = '';
								$data[$num]['entryLabel'] = '';
								$data[$num]['num_item'] = '';
								$data[$num]['entry_number'] =  'OP BALANCE';
								$data[$num]['entry_date'] =  '';
								$data[$num]['narration'] = '';
								$data[$num]['op_balance'] = '';
								$data[$num]['amount_D'] = $this->functionscore->toCurrency('D', ($ledger_company_op > 0 ? $ledger_company_op : 0));
								$data[$num]['amount_C'] = $this->functionscore->toCurrency('C', ($ledger_company_op < 0 ? $ledger_company_op * -1 : 0));
								$ledger_total += $ledger_company_op;
								$data[$num]['saldo'] = $this->functionscore->toCurrency(($ledger_total < 0 ? 'C' : 'D'), ($ledger_total < 0 ? $ledger_total * -1 : $ledger_total));
								$data[$num]['state'] = '';
								if ($show_base) {
									$data[$num]['base'] = 0;
								}
								$num++;
								// $ledger_total_dr += ($ledger_company_op > 0 ? $ledger_company_op : 0);
								// $ledger_total_cr += ($ledger_company_op < 0 ? ($ledger_company_op * -1) : 0);
								$ledger_total_op += $ledger_company_op;
							}
						}
						$ledger_row = $ledgers_data[$ledger_id];
						$data[$num]['vat_no'] = '';
						$data[$num]['third_name'] = '';
						$data[$num]['level'] = 1;
						$data[$num]['entry_id'] = '';
						$data[$num]['entryLabel'] = '';
						$data[$num]['num_item'] = '';
						$data[$num]['entry_number'] =  '<b>CL BALANCE</b>';
						$data[$num]['entry_date'] =  '<b>'.$ledger_row->code.'</b>';
						$data[$num]['narration'] = '<b>'.$ledger_row->name.'</b>';
						$data[$num]['op_balance'] = '<b>'.$this->functionscore->toCurrency(($ledger_total_op < 0 ? 'C' : 'D'), ($ledger_total_op < 0 ? $ledger_total_op * -1 : $ledger_total_op)).'</b>';
						$data[$num]['amount_D'] = '<b>'.$this->functionscore->toCurrency('D', $ledger_total_dr).'</b>';
						$data[$num]['amount_C'] = '<b>'.$this->functionscore->toCurrency('C', $ledger_total_cr).'</b>';
						$data[$num]['saldo'] = '<b>'.$this->functionscore->toCurrency(($ledger_total < 0 ? 'C' : 'D'), ($ledger_total < 0 ? $ledger_total * -1 : $ledger_total)).'</b>';
						$data[$num]['state'] = '';
						if ($show_base) {
							$data[$num]['base'] = 0;
						}
						$num++;
						$ledger_total = 0;
						$ledger_total_op = 0;
						$ledger_total_dr = 0;
						$ledger_total_cr = 0;
						unset($ledgers_data[$ledger_id]);
					}
					$ledger_id = $entry_row->ledger_id;
					$ledger_row = $ledgers_data[$ledger_id];
					$data[$num]['vat_no'] = '';
					$data[$num]['third_name'] = '';
					$data[$num]['level'] = 1;
					$data[$num]['entry_id'] = '';
					$data[$num]['num_item'] = '';
					$data[$num]['entryLabel'] = '';
					$data[$num]['entry_number'] = '';
					$data[$num]['entry_date'] = '<b>'.$ledger_row->code.'</b>';
					$data[$num]['narration'] = '<b>'.$ledger_row->name.'</b>';
					$data[$num]['op_balance'] = '';
					$data[$num]['amount_D'] = '';
					$data[$num]['amount_C'] = '';
					$data[$num]['saldo'] = '<b>'.$this->functionscore->toCurrency(($ledger_row->total_op < 0 ? 'C' : 'D'), ($ledger_row->total_op < 0 ? $ledger_row->total_op * -1 : $ledger_row->total_op)).'</b>';
					$data[$num]['state'] = '';
					if ($show_base) {
						$data[$num]['base'] = 0;
					}
					$ledger_total = 0;
					$num++;
				}
				if ($company_id == NULL || $company_id != $entry_row->companies_id) {
					if ($company_total !== NULL) {
						$company_total = 0;
						$company_total_dr = 0;
						$company_total_cr = 0;
						unset($ledgers_data[$ledger_id]->companies_op[$company_id]);
					}
					$company_id = $entry_row->companies_id;
					$company_row = $companies_data[$company_id];
					$company_ledger_opbalance = isset($company_row->ledgers_op[$ledger_id]) ? $company_row->ledgers_op[$ledger_id] : 0;
					$total_close += $company_ledger_opbalance;
					if ($company_ledger_opbalance) {
						$data[$num]['vat_no'] = $company_row->vat_no;
						$data[$num]['third_name'] = $company_row->company;
						$data[$num]['level'] = 4;
						$data[$num]['entry_id'] = '';
						$data[$num]['num_item'] = '';
						$data[$num]['entryLabel'] = '';
						$data[$num]['entry_number'] = 'OP BALANCE';
						$data[$num]['entry_date'] = '';
						$data[$num]['narration'] = '';
						$data[$num]['op_balance'] = $this->functionscore->toCurrency(($company_ledger_opbalance < 0 ? 'C' : 'D'), ($company_ledger_opbalance < 0 ? $company_ledger_opbalance * -1 : $company_ledger_opbalance));
						$data[$num]['amount_D'] = '';
						$data[$num]['amount_C'] = '';
						$ledger_total += $company_ledger_opbalance;
						$data[$num]['saldo'] = ''.$this->functionscore->toCurrency(($ledger_total < 0 ? 'C' : 'D'), ($ledger_total < 0 ? $ledger_total * -1 : $ledger_total)).'';
						$data[$num]['state'] = '';
						if ($show_base) {
							$data[$num]['base'] = 0;
						}
						$num++;
					}
					$company_total = $company_ledger_opbalance;
					$ledger_total_op += $company_ledger_opbalance;
					// $ledger_total_dr += ($company_ledger_opbalance > 0 ? $company_ledger_opbalance : 0);
					// $ledger_total_cr += ($company_ledger_opbalance < 0 ? ($company_ledger_opbalance * -1) : 0);
				}
				$company_data = isset($companies_data[$entry_row->companies_id]) ? $companies_data[$entry_row->companies_id] : false;
				$data[$num]['vat_no'] = $entry_row->vat_no;
				$data[$num]['third_name'] = ($entry_row->company);
				$data[$num]['level'] = 4;
				$data[$num]['entry_id'] = $entry_row->entry_id;
				$data[$num]['num_item'] = $num+1;
				$data[$num]['entryLabel'] = $entry_row->entry_label;
				$data[$num]['entry_number'] = $entry_row->reference_no;
				$data[$num]['entry_date'] = ''.$entry_row->date.'';
				$data[$num]['narration'] = ''.($entry_row->narration).'';
				$data[$num]['op_balance'] = '';
				$data[$num]['amount_D'] = $this->functionscore->toCurrency('D', $entry_row->dr_total);
				$data[$num]['amount_C'] = $this->functionscore->toCurrency('C', $entry_row->cr_total);
				$ledger_total += ($entry_row->dr_total - $entry_row->cr_total);
				$ledger_total_cr += ($entry_row->cr_total);
				$ledger_total_dr += ($entry_row->dr_total);
				$company_total += ($entry_row->dr_total - $entry_row->cr_total);
				$company_total_cr += ($entry_row->cr_total);
				$company_total_dr += ($entry_row->dr_total);
				$total_cr += ($entry_row->cr_total);
				$total_dr += ($entry_row->dr_total);
				$total_close += ($entry_row->dr_total - ($entry_row->cr_total));
				$data[$num]['saldo'] = ''.$this->functionscore->toCurrency(($ledger_total < 0 ? 'C' : 'D'), ($ledger_total < 0 ? $ledger_total * -1 : $ledger_total)).'';
				if ($entry_row->entry_state == '1') {
					$data[$num]['state'] = 'Aprobado';
				} else if ($entry_row->entry_state == '2') {
					$data[$num]['state'] = 'Desaprobado';
				} else if ($entry_row->entry_state == '0') {
					$data[$num]['state'] = 'Anulada';
				} else {
					$data[$num]['state'] = '';
				}
				if ($show_base) {
					$data[$num]['base'] = $entry_row->base;
				}
				$num++;
			}

			if ($company_total != 0) {
				unset($ledgers_data[$ledger_id]->companies_op[$company_id]);
			}
				
			if ($ledger_total != 0) {
				if (isset($ledgers_data[$ledger_id]->companies_op) && count($ledgers_data[$ledger_id]->companies_op) > 0) {
					foreach ($ledgers_data[$ledger_id]->companies_op as $ledger_company_op_company_id => $ledger_company_op) {
						$total_close += $ledger_company_op;
						$data[$num]['vat_no'] = $companies_data[$ledger_company_op_company_id]->vat_no;
						$data[$num]['third_name'] = $companies_data[$ledger_company_op_company_id]->company;
						$data[$num]['level'] = 4;
						$data[$num]['entry_id'] = '';
						$data[$num]['entryLabel'] = '';
						$data[$num]['num_item'] = '';
						$data[$num]['entry_number'] =  '<b>OP BALANCE</b>';
						$data[$num]['entry_date'] =  '';
						$data[$num]['narration'] = '';
						$data[$num]['op_balance'] = $this->functionscore->toCurrency(($ledger_company_op < 0 ? 'C' : 'D'), ($ledger_company_op < 0 ? $ledger_company_op * -1 : $ledger_company_op));
						$data[$num]['amount_D'] = '';
						$data[$num]['amount_C'] = '';
						$data[$num]['saldo'] = '<b>'.$this->functionscore->toCurrency(($ledger_total < 0 ? 'C' : 'D'), ($ledger_total < 0 ? $ledger_total * -1 : $ledger_total)).'</b>';
						$data[$num]['state'] = '';
						if ($show_base) {
							$data[$num]['base'] = 0;
						}
						$num++;
						// $ledger_total_dr += ($ledger_company_op > 0 ? $ledger_company_op : 0);
						// $ledger_total_cr += ($ledger_company_op < 0 ? ($ledger_company_op * -1) : 0);
						$ledger_total_op += $ledger_company_op;
						$ledger_total += $ledger_company_op;
					}
				}
				$ledger_row = $ledgers_data[$ledger_id];
				$data[$num]['vat_no'] = '';
				$data[$num]['third_name'] = '';
				$data[$num]['level'] = 1;
				$data[$num]['entry_id'] = '';
				$data[$num]['entryLabel'] = '';
				$data[$num]['num_item'] = '';
				$data[$num]['entry_number'] =  '';
				$data[$num]['entry_date'] =  '<b>'.$ledger_row->code.'</b>';
				$data[$num]['narration'] = '<b>'.$ledger_row->name.'</b>';
				$data[$num]['op_balance'] = '';
				$data[$num]['amount_D'] = '<b>'.$this->functionscore->toCurrency('D', $ledger_total_dr).'</b>';
				$data[$num]['amount_C'] = '<b>'.$this->functionscore->toCurrency('C', $ledger_total_cr).'</b>';
				$data[$num]['saldo'] = $this->functionscore->toCurrency(($ledger_total < 0 ? 'C' : 'D'), ($ledger_total < 0 ? $ledger_total * -1 : $ledger_total));
				$data[$num]['state'] = '';
				if ($show_base) {
					$data[$num]['base'] = 0;
				}
				$num++;
				unset($ledgers_data[$ledger_id]);
			}

			if (count($ledgers_data) > 0) {
				foreach ($ledgers_data as $ledger_id => $ledger) {
					if ($ledger->total_op == 0) {
						continue;
					}
					$ledger_cr = $ledger_dr = $ledger_total = 0;
					$data[$num]['vat_no'] = '';
					$data[$num]['third_name'] = '';
					$data[$num]['level'] = 1;
					$data[$num]['entry_id'] = '';
					$data[$num]['entryLabel'] = '';
					$data[$num]['num_item'] = '';
					$data[$num]['entry_number'] = '';
					$data[$num]['entry_date'] = '<b>'.$ledger->code.'</b>';
					$data[$num]['narration'] = '<b>'.$ledger->name.'</b>';
					$data[$num]['op_balance'] = '';
					$data[$num]['amount_D'] = '';
					$data[$num]['amount_C'] = '';
					$data[$num]['saldo'] = '<b>'.$this->functionscore->toCurrency(($ledger->total_op < 0 ? 'C' : 'D'), ($ledger->total_op < 0 ? $ledger->total_op * -1 : $ledger->total_op)).'</b>';
					$data[$num]['state'] = '';
					if ($show_base) {
						$data[$num]['base'] = 0;
					}
					$num++;
					if (isset($ledger->companies_op)) {
						foreach ($ledger->companies_op as $ledger_company_op_company_id => $ledger_company_op) {
							$total_close += $ledger_company_op;
							$data[$num]['vat_no'] = $companies_data[$ledger_company_op_company_id]->vat_no;
							$data[$num]['third_name'] = $companies_data[$ledger_company_op_company_id]->company;
							$data[$num]['level'] = 4;
							$data[$num]['entry_id'] = '';
							$data[$num]['entryLabel'] = '';
							$data[$num]['num_item'] = '';
							$data[$num]['entry_number'] = 'OP BALANCE';
							$data[$num]['entry_date'] = '';
							$data[$num]['narration'] = '';
							$data[$num]['op_balance'] = $this->functionscore->toCurrency(($ledger_company_op < 0 ? 'C' : 'D'), ($ledger_company_op < 0 ? $ledger_company_op * -1 : $ledger_company_op));
							$data[$num]['amount_D'] = '';
							$data[$num]['amount_C'] = '';
							$data[$num]['state'] = '';
							if ($show_base) {
								$data[$num]['base'] = 0;
							}
							$ledger_total_op += $ledger_company_op;
							$ledger_total += $ledger_company_op;
							$data[$num]['saldo'] = $this->functionscore->toCurrency(($ledger_total < 0 ? 'C' : 'D'), ($ledger_total < 0 ? $ledger_total * -1 : $ledger_total));
							$num++;
						}
					}
					$data[$num]['vat_no'] = '';
					$data[$num]['third_name'] = '';
					$data[$num]['level'] = 1;
					$data[$num]['entry_id'] = '';
					$data[$num]['entryLabel'] = '';
					$data[$num]['num_item'] = '';
					$data[$num]['entry_number'] = '';
					$data[$num]['entry_date'] = '<b>'.$ledger->code.'</b>';
					$data[$num]['narration'] = '<b>'.$ledger->name.'</b>';
					$data[$num]['op_balance'] = '';
					$data[$num]['amount_D'] = '<b>'.$this->functionscore->toCurrency('D', $ledger_dr).'</b>';
					$data[$num]['amount_C'] = '<b>'.$this->functionscore->toCurrency('C', $ledger_cr).'</b>';
					$data[$num]['saldo'] = '<b>'.$this->functionscore->toCurrency(($total_close < 0 ? 'C' : 'D'), ($total_close < 0 ? $total_close * -1 : $total_close)).'</b>';
					$data[$num]['state'] = '';
					if ($show_base) {
						$data[$num]['base'] = 0;
					}
					$num++;
				}
			}

			$data[$num]['vat_no'] = '';
			$data[$num]['third_name'] = '';
			$data[$num]['level'] = 1;
			$data[$num]['entry_id'] = '';
			$data[$num]['entryLabel'] = '';
			$data[$num]['num_item'] = $num;
			$data[$num]['entry_number'] =  '<b>TOTAL INFORME</b>';
			$data[$num]['entry_date'] = '';
			$data[$num]['narration'] = '';
			$data[$num]['op_balance'] = '';
			$data[$num]['amount_D'] = '<b>'.$this->functionscore->toCurrency('D', $total_dr).'</b>';
			$data[$num]['amount_C'] = '<b>'.$this->functionscore->toCurrency('C', $total_cr).'</b>';
			$total_cl = ($total_dr - $total_cr) + $total_op;
			$data[$num]['saldo'] = $this->functionscore->toCurrency(($total_cl < 0 ? 'C' : 'D'), ($total_cl < 0 ? $total_cl * -1 : $total_cl));
			$data[$num]['state'] = '';
			if ($show_base) {
				$data[$num]['base'] = 0;
			}
			$num++;
			/**/
		}

		if ($action == "download") {
            if ($format=='pdf') {
                $this->load->view('reports/pdf/thirdsaccounts', array('datos' => $data, 'settings' => $this->mAccountSettings, 'POST' => $_POST, 'descargar' => 1));
            }
            if ($format=='pdfprint') {
                $this->load->view('reports/pdf/thirdsaccounts', array('datos' => $data, 'settings' => $this->mAccountSettings, 'POST' => $_POST, 'descargar' => 0));
            }
            if ($format=='xls') {
            	$filename = lang('page_title_reports_third_and_accounts');
				$this->load->library('excel');
	            $this->excel->setActiveSheetIndex(0);
	            $styleArray = array(
	                'borders' => array(
	                    'allborders' => array(
	                        'style' => PHPExcel_Style_Border::BORDER_THIN
	                    )
	                )
	            );
	            // $this->excel->getDefaultStyle()->applyFromArray($styleArray);
				$this->excel->getActiveSheet()->setTitle('balancesheet');
	            $this->excel->getActiveSheet()->SetCellValue('A1', 'Thirds accounts');
	            $this->excel->getActiveSheet()->mergeCells('A1:H1');
	            $letra = 'A';
	            if ($view_type == 3) {
	            	$this->excel->getActiveSheet()->SetCellValue($letra.'2', 'NIT');
	            	$letra++;
	            	$this->excel->getActiveSheet()->SetCellValue($letra.'2', 'Tercero');
	            	$letra++;
	            }
	            $this->excel->getActiveSheet()->SetCellValue($letra.'2', 'Número'); $letra++;
	            $this->excel->getActiveSheet()->SetCellValue($letra.'2', 'Fecha'); $letra++;
	            $this->excel->getActiveSheet()->SetCellValue($letra.'2', 'Descripción'); $letra++;
	            if ($show_base) {
	            	$this->excel->getActiveSheet()->SetCellValue($letra.'2', 'Base');
	            	$letra++;
	            }

	            if ($view_type == 3) {
	            	$this->excel->getActiveSheet()->SetCellValue($letra.'2', 'OP Balance');
	            	$letra++;
	            }
	            $this->excel->getActiveSheet()->SetCellValue($letra.'2', 'Monto D');
	            $letra++;
	            $this->excel->getActiveSheet()->SetCellValue($letra.'2', 'Monto C');
	            $letra++;
	            $this->excel->getActiveSheet()->SetCellValue($letra.'2', 'Saldo');
	            $letra++;
	            foreach ($data as $row => $fila) {
	            	$letra = 'A';
	            	$this->row++;
	            	if ($view_type == 3) {
	            		$this->excel->getActiveSheet()->SetCellValue($letra.$this->row, strip_tags($fila['vat_no']));$letra++;
	            		$this->excel->getActiveSheet()->SetCellValue($letra.$this->row, (($fila['third_name'])));$letra++;
	            	}
	            	$this->excel->getActiveSheet()->SetCellValue($letra.$this->row, strip_tags($fila['entry_number']));$letra++;
	            	$this->excel->getActiveSheet()->SetCellValue($letra.$this->row, strip_tags($fila['entry_date']));$letra++;
	            	$this->excel->getActiveSheet()->SetCellValue($letra.$this->row, strip_tags($fila['narration']));$letra++;
		            if ($show_base) {
		            	$this->excel->getActiveSheet()->SetCellValue($letra.$this->row, $fila['base']);
		            	$letra++;
		            }
	            	$monto_D = $this->functionscore->drawAmount(strip_tags($fila['amount_D']));
	            	$monto_C = $this->functionscore->drawAmount(strip_tags($fila['amount_C']));
	            	$monto_saldo = $this->functionscore->drawAmount(strip_tags($fila['saldo']));
	            	if ($view_type == 3) {
	            		$monto_op = $this->functionscore->drawAmount(strip_tags($fila['op_balance']));
	            		$this->excel->getActiveSheet()->SetCellValue($letra.$this->row, $monto_op[1]);$letra++;
	            	}
	            	$this->excel->getActiveSheet()->SetCellValue($letra.$this->row, $monto_D[1]);
	           	 	$letra++;
	            	$this->excel->getActiveSheet()->SetCellValue($letra.$this->row, $monto_C[1]);
	           	 	$letra++;
	            	$this->excel->getActiveSheet()->SetCellValue($letra.$this->row, $monto_saldo[1]);
	           	 	$letra++;
	            	if ($fila['level'] == 1 || $fila['level'] == 2) {
	            		$this->excel->getActiveSheet()->getStyle("A".$this->row.":F".$this->row)->getFont()->setBold(true);
	            	}
	            }


	            $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(14);
	            $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(12);
	            $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
	            $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(12);
	            $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(12);
	            $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(12);
	            $this->excel->getActiveSheet()->getStyle('D1:D'.$this->row)->getNumberFormat()->setFormatCode("#,##0.00");
	            $this->excel->getActiveSheet()->getStyle('E1:E'.$this->row)->getNumberFormat()->setFormatCode("#,##0.00");
	            $this->excel->getActiveSheet()->getStyle('F1:F'.$this->row)->getNumberFormat()->setFormatCode("#,##0.00");
	            if ($show_base) {
	            	$this->excel->getActiveSheet()->getStyle('G1:G'.$this->row)->getNumberFormat()->setFormatCode("#,##0.00");
	            }
                header('Content-Type: application/vnd.ms-excel');
                header('Content-Disposition: attachment;filename="' . $filename . '.xls"');
                header('Cache-Control: max-age=0');
                $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
                $objWriter->save('php://output');
                exit();
            }
		} else {

			foreach ($data as $row => $arr) {
				foreach ($arr as $key => $value) {
					$data[$row][$key] = ($value);
				}
			}

			$output = [
				'sEcho' => 1,
				'iTotalRecords' => count($data),
				'iTotalDisplayRecords' => count($data),
				'aaData' => $data
				];
			echo json_encode($output);
		}
	}

	public function certificate_withholdings()
	{
		$this->data["thirds_party_options"] = $this->reports_model->get_third_party_options();
		$this->data["note_types"] = $this->entryTypes_model->get_entrytypes();
		$this->data['filtered'] = false;
		if ($this->input->method() == "post") {
			$search_parameters = (object) [
				'initial_date' => $this->input->post("initial_date"),
				'note_type_excluded' => $this->input->post("note_type_excluded"),
				'third_party' => $this->input->post("third_party"),
				'final_date' => $this->input->post("final_date"),
				'cost_center' => $this->input->post("cost_center"),
			];
			$this->data['filtered'] = true;
			$this->data["thirds_party"] = $this->reports_model->get_third_party($search_parameters);
		} else {
			$this->data["thirds_party"] = $this->reports_model->get_third_party();
		}

		if ($this->mAccountSettings->cost_center) {
			if ($this->user_cost_centers) {
				foreach ($this->user_cost_centers as $key => $value) {
					if ($key == 0) {
						$this->DB1->where('id', $value);
					} else {
						$this->DB1->or_where('id', $value);
					}
				}
			}
			$cost_centers_data = $this->DB1->get('cost_centers'.$this->DB1->dbsuffix);
			if ($cost_centers_data->num_rows() > 0) {
				$cost_centers_data = $cost_centers_data->result_array();
				foreach ($cost_centers_data as $row => $cost_center_data) {
					$cost_centers[] = $cost_center_data;
				}
			} else {
				$cost_centers = false;
			}
			$this->data['cost_centers'] = $cost_centers;
		}

		$this->render('reports/certificate_withholdings/index');
	}

	public function get_retention_information()
	{
		$retention_information_array = [];
		$third_party_id = $this->input->post("third_party_id");
		$note_type_excluded = $this->input->post("note_type_excluded");
		$initial_date = $this->input->post("initial_date");
		$final_date = $this->input->post("final_date");
		$cost_center = $this->input->post("cost_center");

		$retention_data = $this->reports_model->get_retention_data($third_party_id, $note_type_excluded, $initial_date, $final_date, $cost_center);

		if (!empty($retention_data)) {
			foreach ($retention_data as $retention) {
				$retention_information_array[$retention->tipo_retencion][$retention->codigo_cuenta. " - ". $retention->nombre_cuenta][$retention->NIT_CC. " - ".$retention->compania][] = $retention;
			}

			$table = '';
			$total_general_bases = $total_general_retention = 0;
			foreach ($retention_information_array as $type_retention => $retention_type_array) {

				$table .= 	'<table class="table table-condensed toble-hover">
								<tr class="bg-success">
									<td colspan="7">'. $type_retention .'</td>
								</tr>';

				$total_type_retention_bases = $total_type_retention_retention = 0;
				foreach ($retention_type_array as $accounting_account => $retention_account_array) {

					$table .= 	'<tr class="bg-info">
									<td colspan="7">'. $accounting_account.'</td>
								</tr>';

					$total_account_bases = $total_account_retention = 0;
					foreach ($retention_account_array as $NIT_CC => $retention_third_party_array) {
						$table .='<tr class="active">
									<td colspan="7">'. $NIT_CC .'</td>
								</tr>

								<tr>
									<th style="width: 50px;">'. lang("certificate_withholdings_label_document_type") .'</th>
									<th style="width: 50px;">'. lang("certificate_withholdings_label_document_number") .'</th>
									<th style="width: 90px;">'. lang("certificate_withholdings_label_date") .'</th>
									<th>'. lang("certificate_withholdings_label_concept") .'</th>
									<th class="text-center" style="width: 125px;">'. lang("certificate_withholdings_label_base") .'</th>
									<th class="text-center" style="width: 125px;">'. lang("certificate_withholdings_label_retention") .'</th>
									<th class="text-center" style="width: 50px;">'. lang("certificate_withholdings_label_percentage") .'</th>
								</tr>';

						$total_third_party_bases = $total_third_party_retention = 0;
						foreach ($retention_third_party_array as $data) {
							$percentage = 0;

							if ($data->base > 0) {
								$percentage = ($data->retencion / $data->base) * 100;
							}

							$table.='<tr>
										<td>'. $data->tipo_documento .'</td>
										<td class="text-center">'. $data->numero_documento .'</td>
										<td>'. $data->fecha_documento .'</td>
										<td>'. $data->concepto .'</td>
										<td class="text-right">'. number_format($data->base, 2, ",", ".") .'</td>
										<td class="text-right">'. number_format($data->retencion, 2, ",", ".") .'</td>
										<td class="text-right">'. number_format($percentage, 2, ',', '.') .'</td>
									</tr>';

							$total_third_party_bases += (float) $data->base;
							$total_third_party_retention += (float) $data->retencion;
						}

						$table .='<tr class="active">
									<td class="text-right" colspan="4"><strong>TOTAL TERCERO '. $NIT_CC .'</strong></td>
									<td class="text-right"><strong>'. number_format($total_third_party_bases, 2, ",", ".") .'</strong></td>
									<td class="text-right"><strong>'. number_format($total_third_party_retention, 2, ",", ".") .'</strong></td>
									<td></td>
								</tr>';

						$total_account_bases += $total_third_party_bases;
						$total_account_retention += $total_third_party_retention;
					}

					$table .=	'<tr class="active">
									<td class="text-right" colspan="4"><strong>TOTAL CUENTA '. $accounting_account .'</strong></td>
									<td class="text-right"><strong>'. number_format($total_account_bases, 2, ",", ".") .'</strong></td>
									<td class="text-right"><strong>'. number_format($total_account_retention, 2, ",", ".") .'</strong></td>
									<td></td>
								</tr>';

					$total_type_retention_bases += $total_account_bases;
					$total_type_retention_retention += $total_account_retention;
				}

				$table .= 		'<tr class="active">
									<td class="text-right" colspan="4"><strong>TOTAL '. $type_retention .'</strong></td>
									<td class="text-right"><strong>'. number_format($total_type_retention_bases, 2, ",", ".") .'</strong></td>
									<td class="text-right"><strong>'. number_format($total_type_retention_retention, 2, ",", ".") .'</strong></td>
									<td></td>
								</tr>';

				$total_general_bases += $total_type_retention_bases;
				$total_general_retention += $total_type_retention_retention;
			}

			$table .= 			'<tr class="active">
									<td class="text-right" colspan="4"><strong>TOTAL GENERAL</strong></td>
									<td class="text-right"><strong>'. number_format($total_general_bases, 2, ",", ".") .'</strong></td>
									<td class="text-right"><strong>'. number_format($total_general_retention, 2, ",", ".") .'</strong></td>
									<td></td>
								</tr>
							</table>';
		}

		echo $table;
	}

	public function print_retention_report($third_party_document = NULL, $note_type_excluded = NULL, $initial_date = NULL, $final_date = NULL)
	{
		$third_party_document = (!empty($third_party_document) ? $third_party_document : json_decode($this->input->post("third_party_ids")));
		$note_type_excluded = (!empty($note_type_excluded) ? $note_type_excluded : $this->input->post("note_type_excluded"));
		$initial_date = (!empty($initial_date) ? $initial_date : $this->input->post("initial_date"));
		$final_date = (!empty($final_date) ? $final_date : $this->input->post("final_date"));

		$this->data["retention_data"] = $this->reports_model->get_retention_data($third_party_document, $note_type_excluded, $initial_date, $final_date);

		$this->load->view('reports/retentions_report', $this->data);
	}

	public function print_retention_summary_report($third_party_id = NULL, $note_type_excluded = NULL, $initial_date = NULL, $final_date = NULL)
	{
		$third_party_id = (!empty($third_party_id) ? $third_party_id : json_decode($this->input->post("third_party_ids")));
		$note_type_excluded = (!empty($note_type_excluded) ? $note_type_excluded : $this->input->post("note_type_excluded"));
		$initial_date = (!empty($initial_date) ? $initial_date : $this->input->post("initial_date"));
		$final_date = (!empty($final_date) ? $final_date : $this->input->post("final_date"));

		$this->data["retention_data"] = $this->reports_model->get_retention_data($third_party_id, $note_type_excluded, $initial_date, $final_date);

		$this->load->view('reports/retentions_summary_report', $this->data);
	}

	public function print_retention_certificate()
	{
		$this->load->model('Companies_model');

		if (!is_file($_SERVER['DOCUMENT_ROOT'] ."/contabilidad/assets/uploads/companies/". $this->mAccountSettings->logo)) {
			$this->session->set_flashdata('error', lang('invalid_company_logo'));
			redirect('account_settings/main');
		}

		$settings_account = $this->settings_model->get_settings_account();
		$third_party_ids = json_decode($this->input->post("third_party_ids"));
		$note_type_excluded = $this->input->post("note_type_excluded");
		$initial_date = $this->input->post("initial_date");
		$final_date = $this->input->post("final_date");
		$retentions_checked = json_decode($this->input->post("retentions_checked"));
		$retention_data = $this->reports_model->get_retention_data($third_party_ids, $note_type_excluded, $initial_date, $final_date);

		$retention_certificate_array = [];

		foreach ($retentions_checked as $type_retention => $third_party) {
			if (!empty($third_party)) {
				foreach ($third_party as $third_id) {
					$total_base = 0;
					$total_retention = 0;

					foreach ($retention_data as $key => $retention) {
						if ($retention->NIT_CC == $third_id && $retention->tipo_retencion == $type_retention) {
							$retention_certificate_array[$retention->compania ." NIT: ". $retention->NIT_CC][$type_retention][$retention->nombre_cuenta]["base"] = 0;
							$retention_certificate_array[$retention->compania ." NIT: ". $retention->NIT_CC][$type_retention][$retention->nombre_cuenta]["retention"] = 0;

						}
					}
				}
			}
		}

		foreach ($retentions_checked as $type_retention => $third_party) {
			if (!empty($third_party)) {
				foreach ($third_party as $third_id) {
					$total_base = 0;
					$total_retention = 0;


					foreach ($retention_data as $key => $retention) {
						if ($retention->NIT_CC == $third_id && $retention->tipo_retencion == $type_retention) {
							$retention_certificate_array[$retention->compania ." NIT: ". $retention->NIT_CC][$type_retention][$retention->nombre_cuenta]["base"] += $retention->base;
							$retention_certificate_array[$retention->compania ." NIT: ". $retention->NIT_CC][$type_retention][$retention->nombre_cuenta]["retention"] += $retention->retencion;
						}
					}
				}
			}
		}

		$this->data["retention_certificate"] = $retention_certificate_array;
		$this->data["settings_account"] = $settings_account;
		$this->data["dateRange"] = $this->getDateRange($initial_date, $final_date);

		$this->load->view('reports/retentions_certificates', $this->data);
	}

	public function printRetentionReportExcel($third_party_document = NULL, $note_type_excluded = NULL, $initial_date = NULL, $final_date = NULL)
	{
		$third_party_document = $third_party_document ?? json_decode($this->input->post("third_party_ids"));
		$note_type_excluded = $note_type_excluded ?? $this->input->post("note_type_excluded");
		$initial_date = $initial_date ?? $this->input->post("initial_date");
		$final_date = $final_date ?? $this->input->post("final_date");

		$retention_data = $this->reports_model->get_retention_data($third_party_document, $note_type_excluded, $initial_date, $final_date);

		$this->load->library('excel');

		$sheet = $this->excel->getActiveSheet();
		$sheet->setTitle('Certificado de retención');

		if (!empty($retention_data)) {
			$table_headers = [
				lang("certificate_withholdings_label_document_type"),
				lang("certificate_withholdings_label_document_number"),
				lang("certificate_withholdings_label_date"),
				lang("certificate_withholdings_label_concept"),
				lang("certificate_withholdings_label_base"),
				lang("certificate_withholdings_label_retention"),
				lang("certificate_withholdings_label_percentage"),
			];

			foreach ($retention_data as $retention) {
				$retention_information_order_array[$retention->tipo_retencion][$retention->codigo_cuenta. " - ". $retention->nombre_cuenta][$retention->NIT_CC. " - ".$retention->compania][] = $retention;
			}

			$styleTypeRetention = [
				'fill' => [
					'type' => PHPExcel_Style_Fill::FILL_SOLID,
					'color' => ['rgb' => '3a84c5'], // Azul
				],
				'font' => [
					'bold' => true,
					'color' => ['rgb' => 'FFFFFF'], // Blanco
				],
			];

			$styleAccountRetention = [
				'fill' => [
					'type' => PHPExcel_Style_Fill::FILL_SOLID,
					'color' => ['rgb' => 'd9edf7'], // Azul claro
				],
				'font' => [
					'bold' => true,
					'color' => ['rgb' => '696c6f'], // Gris
				],
			];

			$styleThirty = [
				'fill' => [
					'type' => PHPExcel_Style_Fill::FILL_SOLID,
					'color' => ['rgb' => 'f5f5f5'], // Azul claro
				],
				'font' => [
					'bold' => true,
					'color' => ['rgb' => '696c6f'], // Gris
				],
			];

			$styleThirtyFooter = [
				'fill' => [
					'type' => PHPExcel_Style_Fill::FILL_SOLID,
					'color' => ['rgb' => 'f5f5f5'], // Azul claro
				],
				'font' => [
					'bold' => true,
					'color' => ['rgb' => '696c6f'], // Gris
				],
				'alignment' => [
					'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT, // Centrar texto
				],
			];

			$total_general_bases = $total_general_retention = 0;
			$row = 1;
			foreach ($retention_information_order_array as $type_retention => $retention_type_retention_array) {
				$sheet->SetCellValue("A$row", "RETENCIÓN EN ". $type_retention);
				$sheet->mergeCells("A$row:G$row");

				$sheet->getStyle("A$row:G$row")->applyFromArray($styleTypeRetention);
				
				$row++;
				$total_type_retention_bases = $total_type_retention_retention = 0;
				foreach ($retention_type_retention_array as $accouting_account => $retention_account_array) {
					$sheet->SetCellValue("A$row", $accouting_account);
					$sheet->mergeCells("A$row:G$row");
					
					$sheet->getStyle("A$row:G$row")->applyFromArray($styleAccountRetention);

					$row++;
					$total_account_bases = $total_account_retention = 0;
					foreach ($retention_account_array as $NIT_CC => $retention_third_party_array) {
						$sheet->SetCellValue("A$row", $NIT_CC);
						$sheet->mergeCells("A$row:G$row");
						$sheet->getStyle("A$row:G$row")->applyFromArray($styleThirty);

						$row++;
						
						$column = "A";
						foreach ($table_headers as $i => $header) {
							$sheet->SetCellValue("$column$row", $header);
							$column++;
						}
						$row++;

						$total_third_party_bases = $total_third_party_retention = 0;
						foreach ($retention_third_party_array as $data) {
							$percentage = 0;

							if ($data->base > 0) {
								$percentage = ($data->retencion / $data->base) * 100;
							}

							$sheet->SetCellValue("A$row", $data->tipo_documento);
							$sheet->SetCellValue("B$row", $data->numero_documento);
							$sheet->SetCellValue("C$row", $data->fecha_documento);
							$sheet->SetCellValue("D$row", $data->concepto);
							$sheet->SetCellValue("E$row", $data->base);
							$sheet->SetCellValue("F$row", $data->retencion);
							$sheet->SetCellValue("G$row", $percentage);
							$row++;

							$total_third_party_bases += (float) $data->base;
							$total_third_party_retention += (float) $data->retencion;          
						}

						$sheet->SetCellValue("A$row", "TOTAL TERCERO $NIT_CC");
						$sheet->mergeCells("A$row:D$row");
						$sheet->getStyle("A$row:G$row")->applyFromArray($styleThirtyFooter);

						$sheet->SetCellValue("E$row", $total_third_party_bases);
						$sheet->SetCellValue("F$row", $total_third_party_retention);
						$row++;

						$total_account_bases += $total_third_party_bases;
						$total_account_retention += $total_third_party_retention;
					}

					$sheet->SetCellValue("A$row", "TOTAL CUENTA $accouting_account");
					$sheet->mergeCells("A$row:D$row");
					$sheet->getStyle("A$row:G$row")->applyFromArray($styleThirtyFooter);

					$sheet->SetCellValue("E$row", $total_account_bases);
					$sheet->SetCellValue("F$row", $total_account_retention);
					$row++;

					$total_type_retention_bases += $total_account_bases;
					$total_type_retention_retention += $total_account_retention;
				}

				$sheet->SetCellValue("A$row", "TOTAL $type_retention");
				$sheet->mergeCells("A$row:D$row");
				$sheet->getStyle("A$row:G$row")->applyFromArray($styleThirtyFooter);

				$sheet->SetCellValue("E$row", $total_type_retention_bases);
				$sheet->SetCellValue("F$row", $total_type_retention_retention);
				$row++;

				$total_general_bases += $total_type_retention_bases;
				$total_general_retention += $total_type_retention_retention;
			}

			$sheet->SetCellValue("A$row", "TOTAL GENERAL $type_retention");
			$sheet->mergeCells("A$row:D$row");
			$sheet->getStyle("A$row:G$row")->applyFromArray($styleThirtyFooter);

			$sheet->SetCellValue("E$row", $total_general_bases);
			$sheet->SetCellValue("F$row", $total_general_retention);
			$row++;

			$this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(8);
			$this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(12);
			$this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(12);
			$this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(50);
			$this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
			$this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(12);
			$this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(6);
			
			
			$this->excel->getActiveSheet()->getStyle('E1:E'.$row)->getNumberFormat()->setFormatCode("#,##0.00");
			$this->excel->getActiveSheet()->getStyle('F1:F'.$row)->getNumberFormat()->setFormatCode("#,##0.00");
			$this->excel->getActiveSheet()->getStyle('G1:G'.$row)->getNumberFormat()->setFormatCode("#,##0.00");
		}

		$filename = lang('certificate_withholdings_label_general_report_excel');
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="' . $filename . '.xls"');
		header('Cache-Control: max-age=0');
		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
		$objWriter->save('php://output');
	}

	private function getDateRange($initialDate, $finalDate) {
		$year = date("Y", strtotime($this->mAccountSettings->fy_start));

		$initialDate = (empty($initialDate)) ? date("$year/01/01") : $initialDate;
		$finalDate = (empty($finalDate)) ? date("$year/12/31") : $finalDate;
		
		$monthNames = [ 1 => "Enero", 2 => "Febrero", 3 => "Marzo", 4 => "Abril",
			5 => "Mayo", 6 => "Junio", 7 => "Julio", 8 => "Agosto", 9 => "Septiembre",
			10 => "Octubre", 11 => "Noviembre", 12 => "Diciembre",
		];
		$dayInitialDate = date("j", strtotime($initialDate));
		$monthInitialDate = $monthNames[date("n", strtotime($initialDate))];

		$dayFinalDate = date("j", strtotime($finalDate));
		$monthFinalDate = $monthNames[date("n", strtotime($finalDate))];
		
		$message = "$dayInitialDate de $monthInitialDate al $dayFinalDate de $monthFinalDate del $year";
		return $message;
	}

	public function utf8ize($d) {
	    if (is_array($d)) {
	        foreach ($d as $k => $v) {
	            $d[$k] = $this->utf8ize($v);
	        }
	    } else if (is_string ($d)) {
	        return utf8_encode($d);
	    }
	    return $d;
	}
}
