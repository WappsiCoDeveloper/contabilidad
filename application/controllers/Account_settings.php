<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account_settings extends Admin_Controller {
	public function __construct() {
        parent::__construct();
		$this->load->model('reports_model');
		$this->load->model('site');
    }

	public function main() {

		//conexión persistente
		$this->reconnect_persistent_database();

		$this->form_validation->set_rules('name', lang('account_settings_cntrler_main_form_validation_label_name'), 'required');
		$this->form_validation->set_rules('address', lang('account_settings_cntrler_main_form_validation_label_address'), 'required');
		$this->form_validation->set_rules('email', lang('account_settings_cntrler_main_form_validation_label_email'), 'required');
		$this->form_validation->set_rules('currency_symbol', lang('account_settings_cntrler_main_form_validation_label_cur_symbol'), 'required');
		$this->form_validation->set_rules('currency_format', lang('account_settings_cntrler_main_form_validation_label_cur_format'), 'required');
		$this->form_validation->set_rules('fy_start', lang('account_settings_cntrler_main_form_validation_label_fy_start'), 'required');
		$this->form_validation->set_rules('fy_end', lang('account_settings_cntrler_main_form_validation_label_fy_end'), 'required');
		$this->form_validation->set_rules('date_format', lang('account_settings_cntrler_main_form_validation_label_date_format'), 'required');
		if ($this->input->method() == 'post'){
			// 8 May 2016
			$this->DB1->where('entries'.$this->DB1->dbsuffix.'.date <', $this->functionscore->dateToSql($this->input->post('fy_start'))); // 9 May 2016
			$this->DB1->or_where('entries'.$this->DB1->dbsuffix.'.date >', $this->functionscore->dateToSql($this->input->post('fy_end')));

			$q = $this->DB1->get('entries'.$this->DB1->dbsuffix);
			if ($q->num_rows() != 0) {
				$this->session->set_flashdata('error', sprintf(lang('account_settings_cntrler_main_failed_update_entries_beyond_fy_dates_error'), $q->num_rows()));
				redirect('account_settings/main');
			}
			/* Check if financial year end is after financial year start */
			$start_date = strtotime($this->input->post('fy_start') . ' 00:00:00');
			$end_date = strtotime($this->input->post('fy_end') . ' 00:00:00');
			if ($start_date >= $end_date) {
				$this->session->set_flashdata('error', 'Failed to update account setting since financial year end should be after financial year start.');
				redirect('account_settings/main');
			}
		}


		if ($this->form_validation->run() == FALSE) {
			// render page
			$this->render('settings/main');
		}else{
			/* Check if acccount is locked */
			if ($this->mAccountSettings->account_locked == 1) {
				$this->session->set_flashdata('warning', lang('account_settings_cntrler_main_account_locked_warning'));
				redirect('account_settings/main');
			}

			if ($this->input->post('modulary')) {
				$modulary = 1;
			} else {
				$modulary = 0;
			}

			if ($this->input->post('cost_center')) {
				$cost_center = 1;
			} else {
				$cost_center = 0;
			}

			$data = array(
				'name' 				=> $this->input->post('name'),
				'address' 			=> $this->input->post('address'),
				'email' 			=> $this->input->post('email'),
				'currency_symbol' 	=> $this->input->post('currency_symbol'),
				'currency_format' 	=> $this->input->post('currency_format'),
				'fy_start' 			=> $this->functionscore->dateToSql($this->input->post('fy_start')),
				'fy_end' 			=> $this->functionscore->dateToSql($this->input->post('fy_end')),
				'date_format' 		=> $this->input->post('date_format'),
				'nit' 				=> $this->input->post('nit'),
				'phone' 			=> $this->input->post('phone'),
				'modulary' 			=> $modulary,
				'cost_center' 		=> $cost_center,
				'account_parameter_method' 		=> $this->input->post('account_parameter_method'),
				'accountant_name' 				=> $this->input->post('accountant_name'),
				'fiscal_name' 					=> $this->input->post('fiscal_name'),
				'representant_name' 			=> $this->input->post('representant_name'),
				'default_loading_accounts_index' 			=> $this->input->post('default_loading_accounts_index'),
			);
			$this->DB1->update('settings'.$this->DB1->dbsuffix, $data);
			$this->session->set_flashdata('message', lang('account_settings_cntrler_main_update_success'));
			redirect('account_settings/main');
		}

	}

	public function updateLogo($label)
	{

		//conexión persistente
		$this->reconnect_persistent_database();

		$data = array('status' => '', 'msg' => '');
		if (empty($_FILES['companylogoupdate']['name'])) {
			$data['status'] = 'error';
			$data['msg'] = lang('account_settings_cntrler_updateLogo_file_not_selected_warning');
    		echo(json_encode($data));
		}else {
			$uploadPath = 'assets/uploads/companies/';
		    $uploadData = '';
		    $extension = substr($_FILES['companylogoupdate']['name'], strrpos($_FILES['companylogoupdate']['name'], "."));

            $config['upload_path'] = $uploadPath;
            $config['allowed_types'] = 'jpg|png';
            $config['file_name'] = $label.$extension;
            $config['overwrite'] = TRUE;
            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            if($this->upload->do_upload('companylogoupdate')){
                $fileData = $this->upload->data();
                $uploadData = $fileData['file_name'];
                $updatedata = array(
					'logo' => (!empty($uploadData) ? $uploadData : '')
				);
                if ($this->DB1->update('settings'.$this->DB1->dbsuffix, $updatedata)) {
                	$data['status'] = 'success';
                	$data['msg'] = lang('account_settings_cntrler_updateLogo_update_success');
        			echo(json_encode($data));
                }else{
                	$data['status'] = 'error';
                	$data['msg'] = lang('account_settings_cntrler_updateLogo_update_error');
        			echo(json_encode($data));
                }
            }else{
            	$data['status'] = 'error';
            	$data['msg'] = $this->upload->display_errors();
        		echo(json_encode($data));
        	}
		}
	}

	/**
	* carry forward to next financial year method
	*
	* @return void
	*/
	public function cf() {

		//conexión persistente
		$this->reconnect_persistent_database();
		/* Check financial year start is before end */
		$fiscalyear = date("Y", strtotime($this->mAccountSettings->fy_end));
		$newfiscalyear = $fiscalyear+1;
		$actualanosufijo = substr($fiscalyear, 2, 4);
		$anosufijo = substr($newfiscalyear, 2, 4);
		$new_label = $this->session->userdata('active_account')->label;
		$new_label = strpos($new_label, $actualanosufijo) ? str_replace($actualanosufijo, $anosufijo, $new_label) : $new_label."_".$anosufijo;
		$new_label = str_replace(" ", "", $new_label);
		$new_label = str_replace(".", "", $new_label);
		$new_label = str_replace(",", "", $new_label);
		$new_label = str_replace("-", "", $new_label);
		$new_label = str_replace("/", "", $new_label);
		$new_label = str_replace("_", "", $new_label);
		$fy_start 	= $newfiscalyear."-01-01";
		$fy_end 	= $newfiscalyear."-12-31";

		if($this->input->method() == 'post'){
			$new_config = $this->site->get_new_connection();
			$ledger_id = $this->input->post('ledger_id');
			$companies_id = $this->input->post('companies_id');
		}

		$this->data['label'] = array(
			'name'  => 'label',
			'id'    => 'label',
			'type'  => 'text',
			'value' => $new_label,
			'class' => 'form-control',
			'readonly' => 'readonly',
		);
		$this->data['name'] = array(
			'name'  => 'name',
			'id'    => 'name',
			'type'  => 'text',
			'value' => $this->mAccountSettings->name,
			'class' => 'form-control',
			'readonly' => 'readonly',
		);
		$this->data['fy_start'] = array(
			'name'  => 'name',
			'id'    => 'name',
			'type'  => 'text',
			'value' => $this->functionscore->dateFromSql($fy_start),
			'class' => 'form-control',
			'disabled' => 'true',
		);
		$this->data['fy_end'] = array(
			'name'  => 'name',
			'id'    => 'name',
			'type'  => 'text',
			'value' => $this->functionscore->dateFromSql($fy_end),
			'class' => 'form-control',
			'disabled' => 'true',
		);
		
		$this->load->library('AccountList');
		$only_opening = false;
		$startdate = null;
		$enddate = null;
		$cost_center = null;
		$groupfrom = null;
		$groupto = null;
		$this->skip_op_balance = FALSE;
		$this->skip_entries_disapproved = FALSE;
		/* Gross P/L : Expenses */
		$gross_expenses = new AccountList();
		$gross_expenses->Group = &$this->Group;
		$gross_expenses->Ledger = &$this->Ledger;
		$gross_expenses->only_opening = $only_opening;
		$gross_expenses->start_date = $startdate;
		$gross_expenses->end_date = $enddate;
		$gross_expenses->cost_center = $cost_center;
		$gross_expenses->affects_gross = 1;
		$gross_expenses->skip_op_balance = $this->skip_op_balance;
		$gross_expenses->skip_entries_disapproved = $this->skip_entries_disapproved;
		$gross_expenses->start(4, 1);
		$pandl['gross_expenses'] = $gross_expenses;
		$pandl['gross_expense_total'] = 0;
		if ($gross_expenses->cl_total_dc == 'D') {
			$pandl['gross_expense_total'] = $gross_expenses->cl_total;
		} else {
			$pandl['gross_expense_total'] = $this->functionscore->calculate($gross_expenses->cl_total, 0, 'n');
		}
		/* Gross P/L : Incomes */
		$gross_incomes = new AccountList();
		$gross_incomes->Group = &$this->Group;
		$gross_incomes->Ledger = &$this->Ledger;
		$gross_incomes->only_opening = $only_opening;
		$gross_incomes->start_date = $startdate;
		$gross_incomes->end_date = $enddate;
		$gross_incomes->cost_center = $cost_center;
		$gross_incomes->affects_gross = 1;
		$gross_incomes->skip_op_balance = $this->skip_op_balance;
		$gross_incomes->skip_entries_disapproved = $this->skip_entries_disapproved;
		$gross_incomes->start(5, 1);
		$pandl['gross_incomes'] = $gross_incomes;
		$pandl['gross_income_total'] = 0;
		if ($gross_incomes->cl_total_dc == 'C') {
			$pandl['gross_income_total'] = $gross_incomes->cl_total;
		} else {
			$pandl['gross_income_total'] = $this->functionscore->calculate($gross_incomes->cl_total, 0, 'n');
		}
		$gross_incomes2 = new AccountList();
		$gross_incomes2->Group = &$this->Group;
		$gross_incomes2->Ledger = &$this->Ledger;
		$gross_incomes2->only_opening = $only_opening;
		$gross_incomes2->start_date = $startdate;
		$gross_incomes2->end_date = $enddate;
		$gross_incomes2->cost_center = $cost_center;
		$gross_incomes2->affects_gross = 1;
		$gross_incomes2->skip_op_balance = $this->skip_op_balance;
		$gross_incomes2->skip_entries_disapproved = $this->skip_entries_disapproved;
		$gross_incomes2->start(6, 1);
		$pandl['gross_incomes2'] = $gross_incomes2;
		$pandl['gross_income_total2'] = 0;
		if ($gross_incomes2->cl_total_dc == 'C') {
			$pandl['gross_income_total2'] = $gross_incomes2->cl_total;
		} else {
			$pandl['gross_income_total2'] = $this->functionscore->calculate($gross_incomes2->cl_total, 0, 'n');
		}
		$pandl['gross_income_total'] = $this->functionscore->calculate($pandl['gross_income_total'], $pandl['gross_income_total2'], '+');
		$gross_incomes3 = new AccountList();
		$gross_incomes3->Group = &$this->Group;
		$gross_incomes3->Ledger = &$this->Ledger;
		$gross_incomes3->only_opening = $only_opening;
		$gross_incomes3->start_date = $startdate;
		$gross_incomes3->end_date = $enddate;
		$gross_incomes3->cost_center = $cost_center;
		$gross_incomes3->affects_gross = 1;
		$gross_incomes3->skip_op_balance = $this->skip_op_balance;
		$gross_incomes3->skip_entries_disapproved = $this->skip_entries_disapproved;
		$gross_incomes3->start(7, 1);
		if (!empty($gross_incomes3)) {
			$pandl['gross_incomes3'] = $gross_incomes3;
			$pandl['gross_income_total3'] = 0;
			if ($gross_incomes3->cl_total_dc == 'C') {
				$pandl['gross_income_total3'] = $gross_incomes3->cl_total;
			} else {
				$pandl['gross_income_total3'] = $this->functionscore->calculate($gross_incomes3->cl_total, 0, 'n');
			}
			$pandl['gross_income_total'] = $this->functionscore->calculate($pandl['gross_income_total'], $pandl['gross_income_total3'], '+');
		}
		/* Calculating Gross P/L */
		$pandl['gross_pl'] = $this->functionscore->calculate($pandl['gross_income_total'], $pandl['gross_expense_total'], '-');
		/**********************************************************************/
		/************************* NET CALCULATIONS ***************************/
		/**********************************************************************/
		/* Net P/L : Expenses */
		$net_expenses = new AccountList();
		$net_expenses->Group = &$this->Group;
		$net_expenses->Ledger = &$this->Ledger;
		$net_expenses->only_opening = $only_opening;
		$net_expenses->start_date = $startdate;
		$net_expenses->end_date = $enddate;
		$net_expenses->cost_center = $cost_center;
		$net_expenses->affects_gross = 0;
		$net_expenses->start(4, 1);
		$pandl['net_expenses'] = $net_expenses;
		$pandl['net_expense_total'] = 0;
		if ($net_expenses->cl_total_dc == 'D') {
			$pandl['net_expense_total'] = $net_expenses->cl_total;
		} else {
			$pandl['net_expense_total'] = $this->functionscore->calculate($net_expenses->cl_total, 0, 'n');
		}
		/* Net P/L : Incomes */
		$net_incomes = new AccountList();
		$net_incomes->Group = &$this->Group;
		$net_incomes->Ledger = &$this->Ledger;
		$net_incomes->only_opening = $only_opening;
		$net_incomes->start_date = $startdate;
		$net_incomes->end_date = $enddate;
		$net_incomes->cost_center = $cost_center;
		$net_incomes->affects_gross = 0;
		$net_incomes->start(5, 1);
		$pandl['net_incomes'] = $net_incomes;
		$pandl['net_income_total'] = 0;
		if ($net_incomes->cl_total_dc == 'C') {
			$pandl['net_income_total'] = $net_incomes->cl_total;
		} else {
			$pandl['net_income_total'] = $this->functionscore->calculate($net_incomes->cl_total, 0, 'n');
		}
		$net_incomes2 = new AccountList();
		$net_incomes2->Group = &$this->Group;
		$net_incomes2->Ledger = &$this->Ledger;
		$net_incomes2->only_opening = $only_opening;
		$net_incomes2->start_date = $startdate;
		$net_incomes2->end_date = $enddate;
		$net_incomes2->cost_center = $cost_center;
		$net_incomes2->affects_gross = 0;
		$net_incomes2->start(6, 1);
		$pandl['net_incomes2'] = $net_incomes2;
		$pandl['net_income_total2'] = 0;
		if ($net_incomes2->cl_total_dc == 'C') {
			$pandl['net_income_total2'] = $net_incomes2->cl_total;
		} else {
			$pandl['net_income_total2'] = $this->functionscore->calculate($net_incomes2->cl_total, 0, 'n');
		}
		$pandl['net_income_total'] += $pandl['net_income_total2'];
		$net_incomes3 = new AccountList();
		$net_incomes3->Group = &$this->Group;
		$net_incomes3->Ledger = &$this->Ledger;
		$net_incomes3->only_opening = $only_opening;
		$net_incomes3->start_date = $startdate;
		$net_incomes3->end_date = $enddate;
		$net_incomes3->cost_center = $cost_center;
		$net_incomes3->affects_gross = 0;
		$net_incomes3->start(7, 1);
		if (!empty($net_incomes3)) {

			$pandl['net_incomes3'] = $net_incomes3;

			$pandl['net_income_total3'] = 0;
			if ($net_incomes3->cl_total_dc == 'C') {
				$pandl['net_income_total3'] = $net_incomes3->cl_total;
			} else {
				$pandl['net_income_total3'] = $this->functionscore->calculate($net_incomes3->cl_total, 0, 'n');
			}

			$pandl['net_income_total'] += $pandl['net_income_total3'];

		}
		/* Calculating Net P/L */
		$pandl['net_pl'] = $this->functionscore->calculate($pandl['net_income_total'], $pandl['net_expense_total'], '-');
		$pandl['net_pl'] = $this->functionscore->calculate($pandl['net_pl'], $pandl['gross_pl'], '+');
		$this->data['pandl'] = $pandl;
		if ($validation = $this->settings_model->validate_invalid_entry_items_data()) {
			if ($validation['error']) {
				$this->data['error_msg'] = $validation['msg'];
				$this->session->set_flashdata('error', $validation['msg']);
			}
		}
		$this->data['user_activities'] = false;
		$ua = $this->DB1->order_by('id', 'desc')->get_where('user_activities', ['type_id'=>5]);
		if ($ua->num_rows() > 0) {
			foreach (($ua->result()) as $row) {
				$ua_row[] = $row;
			}
			$this->data['user_activities'] = $ua_row;
		}
		// render page
		$this->render('settings/cf');
	}

	var $groups_list = array();
	var $ledgers_list = array();

	/**
	 * Extract the list of groups and ledgers from AccountList object
	 * and update the global variables $group_list and $ledger_list
	 */
	public function _extract_groups_ledgers($accountlist, $calculate_closing)
	{

		//conexión persistente
		$this->reconnect_persistent_database();

		if ($accountlist->id != NULL) {
			$group_item = array(
				'id' => $accountlist->id,
				'parent_id' => $accountlist->g_parent_id,
				'name' => $accountlist->name,
				'code' => $accountlist->code,
				'affects_gross' => $accountlist->g_affects_gross,
			);
			array_push($this->groups_list, $group_item);
		}
		foreach ($accountlist->children_ledgers as $row => $data)
		{
			$ledger_item = array(
				'id' => $data['id'],
				'group_id' => $data['l_group_id'],
				'name' => $data['name'],
				'code' => $data['code'],
				'type' => $data['l_type'],
				'reconciliation' => $data['l_reconciliation'],
				'notes' => $data['l_notes'],
			);
			if ($calculate_closing) {
				$ledger_item['op_balance'] = $data['cl_total'];
				$ledger_item['op_balance_dc'] = $data['cl_total_dc'];
			} else {
				$ledger_item['op_balance'] = '0.00';
				$ledger_item['op_balance_dc'] = 'D';
			}
			array_push($this->ledgers_list, $ledger_item);
		}
		foreach ($accountlist->children_groups as $row => $data)
		{
			$this->_extract_groups_ledgers($data, $calculate_closing);
		}
	}

	public function get_opbalance_companies($ledger_id, $companies_id){

		$this->op_balance_companies = [];
		$this->op_balance_ledgers = [];
		$bd_opb_l = []; //OP BALANCE POR LEDGER AÑO ACTUAL
		$profit_loss_transfer = [];
		$profit_loss_transfer_accounts = ['4', '5', '6', '7'];

		$cost_centers = $this->mAccountSettings->cost_center == 1 ? $this->settings_model->get_cost_centers() : false;
		$max_thirds = 5;
		$cnt = 0;
		$close_balance = $this->settings_model->get_close_balance($cost_centers ? true : false);
		foreach ($close_balance as $cb) {		
			if (in_array(substr($cb['ledger_code'], 0, 1), $profit_loss_transfer_accounts) || $cb['id_ledgers'] == $ledger_id) {

				if ($cost_centers) { //NUEVO SI MANEJA CC
					if (isset($profit_loss_transfer[$ledger_id][$cb['cost_center_id']][$cb['op_amount_dc']])) {
						$profit_loss_transfer[$ledger_id][$cb['cost_center_id']][$cb['op_amount_dc']] += $cb['op_amount'];
					} else {
						$profit_loss_transfer[$ledger_id][$cb['cost_center_id']][$cb['op_amount_dc']] = $cb['op_amount'];
					}
				} else {
					if (isset($profit_loss_transfer[$ledger_id][$cb['op_amount_dc']])) {
						$profit_loss_transfer[$ledger_id][$cb['op_amount_dc']] += $cb['op_amount'];
					} else {
						$profit_loss_transfer[$ledger_id][$cb['op_amount_dc']] = $cb['op_amount'];
					}
				}

				continue;
			}
			if ($cb['op_amount'] > 0) {
				$this->op_balance_companies[$cnt] = array(
									'id_companies' => $cb['id_companies'],
									'id_ledgers' => $cb['id_ledgers'],
									'cost_center_id' => $cost_centers ? $cb['cost_center_id'] : NULL,
									'op_balance' => $cb['op_amount'],
									'op_balance_dc' => $cb['op_amount_dc'],
									);
				if (isset($bd_opb_l[$cb['id_ledgers']][$cb['op_amount_dc']])) {
					$bd_opb_l[$cb['id_ledgers']][$cb['op_amount_dc']] += $cb['op_amount'];
				} else {
					$bd_opb_l[$cb['id_ledgers']][$cb['op_amount_dc']] = $cb['op_amount'];
				}
			}
			$cnt++;
		}

		if ($cost_centers) { //NUEVO SI MANEJA CC

			foreach ($profit_loss_transfer[$ledger_id] as $plcc_id => $pl) {
				$profit_loss_total = $this->functionscore->calculate( ( (isset($pl['D'])) ? $pl['D'] : 0), ( (isset($pl['C'])) ? $pl['C'] : 0), '-');

				if ($profit_loss_total < 0) {
					$profit_loss_total_amount = $profit_loss_total * -1;
					$profit_loss_total_dc = 'C';
				} else {
					$profit_loss_total_amount = $profit_loss_total;
					$profit_loss_total_dc = 'D';
				}

				if ($profit_loss_total_amount != 0) {
					$this->op_balance_companies[$cnt] = array(
											'id_companies' => $companies_id,
											'id_ledgers' => $ledger_id,
											'cost_center_id' => $plcc_id,
											'op_balance' => $profit_loss_total_amount,
											'op_balance_dc' => $profit_loss_total_dc,
										);

					if (isset($bd_opb_l[$ledger_id][$profit_loss_total_dc])) {
						$bd_opb_l[$ledger_id][$profit_loss_total_dc] += $profit_loss_total_amount;
					} else {
						$bd_opb_l[$ledger_id][$profit_loss_total_dc] = $profit_loss_total_amount;
					}

				}
				$cnt++;
			}
			
		} else {

			$profit_loss_total = $this->functionscore->calculate( ( (isset($profit_loss_transfer[$ledger_id]['D'])) ? $profit_loss_transfer[$ledger_id]['D'] : 0), ( (isset($profit_loss_transfer[$ledger_id]['C'])) ? $profit_loss_transfer[$ledger_id]['C'] : 0), '-');

			if ($profit_loss_total < 0) {
				$profit_loss_total_amount = $profit_loss_total * -1;
				$profit_loss_total_dc = 'C';
			} else {
				$profit_loss_total_amount = $profit_loss_total;
				$profit_loss_total_dc = 'D';
			}

			if ($profit_loss_total_amount != 0) {
				$this->op_balance_companies[$cnt] = array(
										'id_companies' => $companies_id,
										'id_ledgers' => $ledger_id,
										'cost_center_id' => NULL,
										'op_balance' => $profit_loss_total_amount,
										'op_balance_dc' => $profit_loss_total_dc,
									);

				if (isset($bd_opb_l[$ledger_id][$profit_loss_total_dc])) {
					$bd_opb_l[$ledger_id][$profit_loss_total_dc] += $profit_loss_total_amount;
				} else {
					$bd_opb_l[$ledger_id][$profit_loss_total_dc] = $profit_loss_total_amount;
				}

			}

		}

		foreach ($bd_opb_l as $ledger => $dc) {
			$total_dc = $this->functionscore->calculate( ( (isset($dc['D'])) ? $dc['D'] : 0), ( (isset($dc['C'])) ? $dc['C'] : 0), '-');
			if ($total_dc < 0) {
				$this->op_balance_ledgers[$ledger]['op_balance'] = $total_dc * -1;
				$this->op_balance_ledgers[$ledger]['op_balance_dc'] = 'C';
			} else {
				$this->op_balance_ledgers[$ledger]['op_balance'] = $total_dc;
				$this->op_balance_ledgers[$ledger]['op_balance_dc'] = 'D';
			}
		}

	}

	public function email() {

		//conexión persistente
		$this->reconnect_persistent_database();


		if (!$this->input->post('email_use_default')) {
			$this->form_validation->set_rules('email_protocol', lang('account_settings_cntrler_email_form_validation_label_email_protocol'), 'required');
			$this->form_validation->set_rules('smtp_host', lang('account_settings_cntrler_email_form_validation_label_smtp_host'), 'required');
			$this->form_validation->set_rules('smtp_port', lang('account_settings_cntrler_email_form_validation_label_smtp_port'), 'required');
			$this->form_validation->set_rules('smtp_username', lang('account_settings_cntrler_email_form_validation_label_smtp_username'), 'required');
			$this->form_validation->set_rules('smtp_password', lang('account_settings_cntrler_email_form_validation_label_smtp_password'), 'required');
			$this->form_validation->set_rules('email_from', lang('account_settings_cntrler_email_form_validation_label_email_from'), 'required');
		}else{
			$this->form_validation->set_rules('email_use_default', lang('account_settings_cntrler_email_form_validation_label_use_default'), 'required');
		}
		if ($this->form_validation->run() == FALSE) {
			// render page
			$this->render('settings/email');
		}else{
			$data = array();
			if ($this->input->post('email_use_default')) {
				$data['email_use_default'] = 1;
			}else{
				$data = array(
					'email_protocol'	=> $this->input->post('email_protocol'),
					'email_host' 		=> $this->input->post('smtp_host'),
					'email_port' 		=> $this->input->post('smtp_port'),
					'email_username' 	=> $this->input->post('smtp_username'),
					'email_password' 	=> $this->input->post('smtp_password'),
					'email_from' 		=> $this->input->post('email_from'),
				);
				if ($this->input->post('smtp_tls')) {
					$data['email_tls'] = $this->input->post('smtp_tls');
				}
			}
			$this->DB1->update('settings'.$this->DB1->dbsuffix, $data);
			$this->session->set_flashdata('message', lang('account_settings_cntrler_email_success'));
			redirect('account_settings/email');
		}

	}
	public function printer() {

		//conexión persistente
		$this->reconnect_persistent_database();

		$this->form_validation->set_rules('height', lang('account_settings_cntrler_printer_form_validation_label_height'), 'required');
		$this->form_validation->set_rules('width', lang('account_settings_cntrler_printer_form_validation_label_width'), 'required');
		$this->form_validation->set_rules('top', lang('account_settings_cntrler_printer_form_validation_label_top'), 'required');
		$this->form_validation->set_rules('bottom', lang('account_settings_cntrler_printer_form_validation_label_bottom'), 'required');
		$this->form_validation->set_rules('left', lang('account_settings_cntrler_printer_form_validation_label_left'), 'required');
		$this->form_validation->set_rules('right', lang('account_settings_cntrler_printer_form_validation_label_right'), 'required');
		$this->form_validation->set_rules('orientation', lang('account_settings_cntrler_printer_form_validation_label_orientation'), 'required');
		$this->form_validation->set_rules('output', lang('account_settings_cntrler_printer_form_validation_label_output'), 'required');

		if ($this->form_validation->run() == FALSE) {
			$this->data['account_settings'] = $this->mAccountSettings;
			// render page
			$this->render('settings/printer');
		} else {
			$data = array(
				'print_paper_height' => $this->input->post('height'),
			    'print_paper_width' => $this->input->post('width'),
			    'print_margin_top' => $this->input->post('top'),
			    'print_margin_bottom' => $this->input->post('bottom'),
			    'print_margin_left' => $this->input->post('left'),
			    'print_margin_right' => $this->input->post('right'),
			    'print_orientation' => $this->input->post('orientation'),
			    'print_page_format' => $this->input->post('output'),
			);
			$this->DB1->update('settings'.$this->DB1->dbsuffix, $data);
			$this->session->set_flashdata('message', lang('account_settings_cntrler_printer_success'));
			redirect('account_settings/printer');
		}
	}

	public function tags($action = NULL) {

		//conexión persistente
		$this->reconnect_persistent_database();

		if(!$action){
			// render page
			$this->render('settings/tags');
		}elseif ($action == 'delete') {
			$id = $this->security->xss_clean($this->input->post('id', true));
			$q = $this->DB1->get_where('entries'.$this->DB1->dbsuffix , array('tag_id'=> $id));
			if ($q->num_rows() > 0) {
	        	echo json_encode('false');
			}else{
				$this->DB1->where('id', $id);
				$eliminar = $this->DB1->delete('tags'.$this->DB1->dbsuffix);
				if ($eliminar) {
					echo json_encode('true');
				} else{
					echo json_encode('false');
				}
			}
		}elseif ($action == 'add'){
			$q = $this->DB1->get_where('tags'.$this->DB1->dbsuffix, array('title'=>$this->input->post('tag_name')));
			if($q->num_rows() > 0){
				echo "false";
			}else{
				$data = array(
		            'title' => $this->input->post('tag_name'),
		            'color' => substr($this->input->post('tag_color'), 1),
		            'background' => substr($this->input->post('tag_bg'), 1),
		        );
		        $this->DB1->insert('tags'.$this->DB1->dbsuffix, $data);
		        echo 'true';
			}
		}elseif ($action == 'getByID'){
			$q = $this->DB1->get_where('tags'.$this->DB1->dbsuffix, array('id'=>$this->input->post('id')));
			if($q->num_rows() > 0){
				echo json_encode($q->row());
			}else{
		        echo 'false';
			}
		}elseif ($action == 'edit'){
			$this->DB1->where('id', $this->input->post('id'));
			$data = array(
	            'title' => $this->input->post('tag_name'),
	            'color' => substr($this->input->post('tag_color'), 1),
	            'background' => substr($this->input->post('tag_bg'), 1),
	        );
	        $this->DB1->update('tags'.$this->DB1->dbsuffix, $data);
	        echo 'true';
		}
	}

	public function entrytypes($action = NULL) {


    	$this->mPageTitle = lang('settings_views_entrytypes_title');

		//conexión persistente
		$this->reconnect_persistent_database();

		if(!$action){
			// render page
			$this->render('settings/entrytypes');
		}elseif ($action == 'delete') {
			$id = $this->security->xss_clean($this->input->post('id', true));
			$q = $this->DB1->get_where('entries'.$this->DB1->dbsuffix , array('entrytype_id'=> $id));
			if ($q->num_rows() > 0) {
	        	echo json_encode('false');
			}else{
				$this->DB1->where('id', $id);
				$this->DB1->delete('entrytypes'.$this->DB1->dbsuffix);
	        	echo json_encode('true');
			}

		}elseif ($action == 'getByID'){
			$q = $this->DB1->get_where('entrytypes'.$this->DB1->dbsuffix, array('id'=>$this->input->post('id')));
			if($q->num_rows() > 0){
				echo json_encode($q->row());
			}else{
		        echo 'false';
			}
		}
	}

	public function addentrytype(){
    	$this->mPageTitle = 'Añadir tipo de nota contable';
		if ($this->input->method()=='post') {
			$this->form_validation->set_rules('et_label', lang('settings_views_entrytypes_modal_label_label'), 'required|is_db1_unique[entrytypes'.$this->DB1->dbsuffix.'.label]');
			$this->form_validation->set_rules('et_name', '', 'required');
			$this->form_validation->set_rules('description', '', 'required');
			$this->form_validation->set_rules('prefix', lang('settings_views_entrytypes_modal_label_perfix'), 'required|is_db1_unique[entrytypes'.$this->DB1->dbsuffix.'.prefix]');
			$this->form_validation->set_rules('zero_padding', '', 'required');
			$this->form_validation->set_rules('restriction_bankcash', '', 'required');

			if ($this->form_validation->run()===TRUE) {
				if ($this->input->post('entrytype_origin') && $this->input->post('entrytype_origin')  == 1) {
					$origen = 1;
				} else {
					$origen = 0;
				}

				$data = array(
		            'label' => $this->input->post('et_label'),
		            'name' => $this->input->post('et_name'),
		            'description' => $this->input->post('description'),
		            'numbering' => $this->input->post('numbering'),
		            'prefix' => $this->input->post('prefix'),
		            'suffix' => $this->input->post('suffix'),
		            'zero_padding' => $this->input->post('zero_padding'),
		            'restriction_bankcash' => $this->input->post('restriction_bankcash'),
		            'origin' => $origen,
		            'base_type' => 1,
		        );

		       	if ($this->mAccountSettings->year_closed == 1) {
					$new_config = $this->site->get_new_connection();
					if ($this->check_database($new_config)) {
						$DB2 = $this->load->database($new_config, TRUE);
						$q = $this->DB1->select('(MAX(id) + 1) AS new_id')->get('entrytypes'.$this->DB1->dbsuffix);
						if ($q->num_rows() > 0) {
							$new_id = $q->row();
							$new_id = $new_id->new_id;
							$q2 = $DB2->get_where('entrytypes'.$DB2->dbsuffix, ['id' => $new_id]);
							if ($q2->num_rows() > 0) {
								$this->session->set_flashdata('error', 'No se creó el tipo de documento, inconsistencia de datos entre años');
								redirect('account_settings/entrytypes');
							} else {
								$this->DB1->insert('entrytypes'.$this->DB1->dbsuffix, $data);
								$new_ledger_id = $this->db->insert_id();
								$data['id'] = $new_ledger_id;
								$DB2->insert('entrytypes'.$DB2->dbsuffix, $data);
							}
						}
					} else {
						redirect('settings/cf');
					}
				} else {
					$this->DB1->insert('entrytypes'.$this->DB1->dbsuffix, $data);
				}
				$this->session->set_flashdata('message', lang('settings_views_entrytypes_added'));
		        $this->render('settings/addentrytype');
			} else {
				$this->render('settings/addentrytype');
			}
		} else {
			$this->render('settings/addentrytype');
		}
	}

	public function editentrytype($identrytype = NULL){
    	$this->mPageTitle = 'Editar tipo de nota contable';
		if ($identrytype != NULL) {
			$entrytype = $this->DB1->where('entrytypes'.$this->DB1->dbsuffix.'.id', $identrytype)->get('entrytypes'.$this->DB1->dbsuffix);
			if ($entrytype->num_rows() > 0) {
				$entrytype = $entrytype->row_array();
				$this->data['entrytype'] = $entrytype;
			} else {
				$this->session->set_flashdata('error', 'No se encontró el tipo de NOTA contable.');
				redirect('account_settings/entrytypes');
			}
			if ($this->input->method() == 'post') {
				$this->form_validation->set_rules('et_label', '', 'required');
				$this->form_validation->set_rules('et_name', '', 'required');
				$this->form_validation->set_rules('description', '', 'required');
				$this->form_validation->set_rules('prefix', '', 'required');
				$this->form_validation->set_rules('zero_padding', '', 'required');
				$this->form_validation->set_rules('restriction_bankcash', '', 'required');
				if ($this->form_validation->run()===TRUE) {
					if ($this->input->post('entrytype_origin') && $this->input->post('entrytype_origin')  == 1) {
						$origen = 1;
					} else {
						$origen = 0;
					}
					$data = array(
			            'label' => $this->input->post('et_label'),
			            'name' => $this->input->post('et_name'),
			            'description' => $this->input->post('description'),
			            'numbering' => $this->input->post('numbering'),
			            'prefix' => $this->input->post('prefix'),
			            'suffix' => $this->input->post('suffix'),
			            'zero_padding' => $this->input->post('zero_padding'),
			            'restriction_bankcash' => $this->input->post('restriction_bankcash'),
			            'origin' => $origen,
			            'base_type' => 1,
			        );
			        if ($this->mAccountSettings->year_closed == 1) {
						$new_config = $this->site->get_new_connection();
						if ($this->check_database($new_config)) {
							$DB2 = $this->load->database($new_config, TRUE);
							$q = $this->DB1->get_where('entrytypes'.$this->DB1->dbsuffix, ['id'=>$identrytype]);
							$q2 = $DB2->get_where('entrytypes'.$DB2->dbsuffix, ['id'=>$identrytype]);
							if ($q->num_rows() > 0 && $q2->num_rows() > 0) {
								$curr_year_data = $q->row();
								$prev_year_data = $q2->row();
								if ($curr_year_data->label == $prev_year_data->label) {
									$this->DB1->update('entrytypes'.$this->DB1->dbsuffix, $data, ['id' => $identrytype]);
									$DB2->update('entrytypes'.$DB2->dbsuffix, $data, ['id' => $identrytype]);
								} else {
									$this->session->set_flashdata('error', 'No se pudo editar, los datos entre años no coinciden');
									redirect('account_settings/edit/'.$identrytype);
								}
							} else {
								$this->session->set_flashdata('error', 'No se pudo editar, los datos entre años no coinciden');
								redirect('account_settings/edit/'.$identrytype);
							}
						} else {
							redirect('settings/cf');
						}
					} else {
						$this->DB1->update('entrytypes'.$this->DB1->dbsuffix, $data, ['id' => $identrytype]);
					}
					$this->session->set_flashdata('message', 'Se actualizó con éxito.');
			        redirect('account_settings/entrytypes');
				} else {
					$this->render('settings/editentrytype');
				}
			} else {
				$this->render('settings/editentrytype');
			}
		} else {
			$this->session->set_flashdata('error', 'No se especificó tipo de nota contable.');
			redirect('account_settings/entrytypes');
		}
	}

	public function lock() {

		//conexión persistente
		$this->reconnect_persistent_database();

		/* on POST */
		if ($this->input->method() == 'post') {
			$this->DB1->update('settings'.$this->DB1->dbsuffix, array('account_locked' => $this->input->post('locked')));
			$this->db->where('id', $this->mActiveAccountID)->update('accounts', array('account_locked' => $this->input->post('locked')));
			if ($this->input->post('locked') == 1) {
				$this->settings_model->add_log(lang('account_settings_cntrler_lock_add_log_locked'), 1);
				$this->session->set_flashdata('message', lang('account_settings_cntrler_lock_success_locked'));
				redirect('account_settings/main');
			} else {
				$this->settings_model->add_log(lang('account_settings_cntrler_lock_add_log_unlocked'), 1);
				$this->session->set_flashdata('message' ,lang('account_settings_cntrler_lock_success_unlocked'));
				redirect('account_settings/main');
			}
		} else {
			$this->data['locked'] = $this->mAccountSettings->account_locked;
			// render page
			$this->render('settings/lock');
		}
	}

	// GENERATE THE AJAX TABLE CONTENT //
    public function getAllTags()
    {

		//conexión persistente
		$this->reconnect_persistent_database();

        $this->load->library('datatables');
        $this->datatables
            ->select('id, title, color, background')
            ->from('tags');
        $this->datatables->add_column('actions', '<div class="btn-group">
          <div class="dropdown">
            <button class="btn btn-success btn-sm" type="button" id="accionesGrupo" data-toggle="dropdown" aria-haspopup="true">
              Acciones
              <span class="caret"></span>
            </button>
            <ul class="dropdown-menu pull-right" aria-labelledby="accionesGrupo">
             <li>
             	<a  data-dismiss="modal" id="modify" href="#clientmodal" data-toggle="modal" data-num="$1"><i class="fa fa-pencil"></i> '.lang("entries_views_views_td_actions_edit_btn").'</a>
             </li>
             <li>
              <a data-toggle="modal" href="#delete_tag" data-num="$1"><i class="fa fa-trash-o"></i> '.lang("entries_views_views_td_actions_delete_btn").'</a>
             </li>
            </ul>
          </div>
        </div>', 'id');
        //<a id="delete" data-num="$1"><i class="fa fa-trash-o"></i> '.lang("entries_views_views_td_actions_delete_btn").'</a>
        $this->datatables->unset_column('id');
        echo $this->datatables->generate();
    }

    // GENERATE THE AJAX TABLE CONTENT //
    public function getAllET()
    {

		// //conexión persistente
		// $this->reconnect_persistent_database();

  //       $this->load->library('datatables');
  //       $this->datatables
  //           ->select('id, label, name, description, prefix, suffix, zero_padding, origin')
  //           ->from('entrytypes');
  //       $this->datatables->add_column('actions', '', 'id');
  //       //<a id="delete" data-num="$1"><i class="fa fa-trash-o"></i> '.lang("entries_views_views_td_actions_delete_btn").'</a>
  //       $this->datatables->unset_column('id');
  //       echo $this->datatables->generate();


        $entrytypes = $this->DB1->get('entrytypes'.$this->DB1->dbsuffix)->result_array();
        $num = 0;
        $data = [];
        foreach ($entrytypes as $row => $et) {
        	$data[$num][0] = $et['label'];
        	$data[$num][1] = $et['name'];
        	$data[$num][2] = $et['description'];
        	$data[$num][3] = $et['prefix'];
        	$data[$num][4] = $et['suffix'];
        	$data[$num][5] = $et['zero_padding'];
        	$data[$num][6] = ($et['origin'] == 1) ? "Contabilidad" : "Otros";
        	$data[$num][7] = '<div class="btn-group">
          <div class="dropdown">
            <button class="btn btn-success btn-sm" type="button" id="accionesGrupo" data-toggle="dropdown" aria-haspopup="true">
              Acciones
              <span class="caret"></span>
            </button>
            <ul class="dropdown-menu pull-right" aria-labelledby="accionesGrupo">
             <li>
             	<a href="'.base_url().'account_settings/editentrytype/'.$et['id'].'"><i class="fa fa-pencil"></i> '.lang("entries_views_views_td_actions_edit_btn").'</a>
             </li>
             <li>
              <a href="#delete_entrytype" data-toggle="modal" data-num="'.$et['id'].'"><i class="fa fa-trash-o"></i> '.lang("entries_views_views_td_actions_delete_btn").'</a>
             </li>
            </ul>
          </div>
        </div>';
        $num++;
        }

         $output = [
		'sEcho' => 1,
		'iTotalRecords' => count($data),
		'iTotalDisplayRecords' => count($data),
		'aaData' => $data
		];

		echo json_encode($output);
    }


    //Accounts parameter

    public function accounts_parameter(){
    	$this->mPageTitle = lang('accounts_parameter_index_title');

    	$table_tax_rates = $this->DB1->query("SHOW TABLES LIKE '".$this->DB1->dbprefix."tax_rates'");

    	if ($table_tax_rates->num_rows() == 0) {
    		$this->session->set_flashdata('error', 'La tabla de impuestos no existe.');
    		redirect('dashboard/index');
    	} else {
    		$this->render('settings/accounts_parameter');
    	}


    }

    public function getParameters(){

    	if ($this->mAccountSettings->account_parameter_method == 1) {
	    	$taxes = [];
	    	$taxes2 = [];
	    	$datatax = $this->DB1->query('SELECT * FROM '.$this->DB1->dbprefix.'tax_rates')->result_array();

	    	foreach ($datatax as $key => $tax) {
	    		$taxes[$tax['id']] = $tax['name'];
	    	}
	    	
	    	$datatax = $this->DB1->query('SELECT * FROM '.$this->DB1->dbprefix.'tax_secondary')->result_array();

	    	foreach ($datatax as $key => $tax) {
	    		$taxes2[$tax['id']] = $tax['description'];
	    	}
    	} else if ($this->mAccountSettings->account_parameter_method == 2) {
    		$categories = [];
	    	$datacategory = $this->DB1->query('SELECT * FROM '.$this->DB1->dbprefix.'categories')->result_array();

	    	foreach ($datacategory as $key => $cat) {
	    		$categories[$cat['id']] = $cat['name'];
	    	}
    	}

    	$ledgers = [];
    	$dataledger = $this->DB1->get('ledgers'.$this->DB1->dbsuffix)->result_array();
    	foreach ($dataledger as $key => $ledger) {
    		$ledgers[$ledger['id']] = $ledger['code']." - ".$ledger['name'];
    	}

    	$conditions = [];

    	if ($this->mAccountSettings->account_parameter_method == 1) {
    		$conditions['id_tax !='] = NULL; 
    	} else if ($this->mAccountSettings->account_parameter_method == 2) {
    		$conditions['id_category !='] = NULL; 
    	}

    	$parametros = $this->DB1->get_where('accounts'.$this->DB1->dbsuffix, $conditions);
    	$tipos = array(1 => 'Venta', 2=>'Compra', 3=>'Recaudo a terceros');
    	$data = [];
    	$num = 0;
    	
    	$tipos_mov = array(
			1 =>
				array(
					'Ingreso' => 'Ingreso', 
					'Iva' => 'Iva (Venta)', 
					'Inventario' => 
					'Inventario (Venta)', 
					'Costo' => 'Costo', 
					'Dev vta' => 'Devolución venta', 
					'Iva Dev vta' => 'Iva Devolución venta', 
					'Impto consumo vta' => 
					'Impto consumo venta', 
					'Descuento' => 'Descuento (Venta)', 
					'Impto verde' => 'Impto verde', 
					'Envio vta' => 'Envio venta', 
					'Propina vta' => 'Propina venta', 
					'Autorretencion Debito' => 'Autorretención Cuenta Débito', 
					'Autorretencion Credito' => 'Autorretención Cuenta Crédito'),
		  	2 =>
				array(
					'Inventario' => 'Inventario (Compra)', 
					'Iva' => 'Iva (Compra)', 
					'Dev compra' => 'Devolución compra', 
					'Iva Dev compra' => 'Iva Devolución compra', 
					'Impto consumo compra' => 'Segundo Impuesto', 
					'Descuento' => 'Descuento (Compra)', 
					'Envio compra' => 'Envio compra',
					'Ajuste Importacion' => 'Ajuste Importacion'
				),
			3 =>
				array(
					'Pago' => 'Pago'
				));

    	if ($parametros->num_rows() > 0) {
    		$parametros = $parametros->result_array();
	    	foreach ($parametros as $id => $parametro) {

	    		$data[$num]['type'] = $tipos[$parametro['type']];
	    		$data[$num]['typemov'] = $tipos_mov[$parametro['type']][$parametro['typemov']];
	    		if ($this->mAccountSettings->account_parameter_method == 1) {
	    			if ($parametro['typemov'] != "Impto consumo compra" && isset($taxes[$parametro['id_tax']])) {
	    				$tax_text = $taxes[$parametro['id_tax']];
	    			} else if (isset($taxes2[$parametro['id_tax']])) {
	    				$tax_text = $taxes2[$parametro['id_tax']];
	    			} else {
	    				$tax_text = "N/A";
	    			}
	    			$data[$num]['tax'] = $tax_text;
	    		} else if ($this->mAccountSettings->account_parameter_method == 2) {
	    			$data[$num]['tax'] = (isset($categories[$parametro['id_category']])) ? $categories[$parametro['id_category']] : "N/A";
	    		}
	    		$data[$num]['ledger'] = (isset($ledgers[$parametro['ledger_id']])) ? $ledgers[$parametro['ledger_id']] : "N/A";
	    		$data[$num]['actions'] = '<div class="btn-group">
									  <div class="dropdown">
									    <button class="btn btn-success btn-sm" type="button" id="accionesProducto" data-toggle="dropdown" aria-haspopup="true">
									      Acciones
									      <span class="caret"></span>
									    </button>
									    <ul class="dropdown-menu pull-right" aria-labelledby="accionesProducto" style="width:220%;">
										  	<li>
										  		<a href="'. base_url().'account_settings/editpaccount/'. $parametro['id'] .'" class="no-hover" escape="false"><i class="fa fa-pencil"></i>  '. lang('entries_views_index_th_actions_edit_btn') .'</a>
										  	</li>
										  	<li>
										  		<a href="#modalDeletePAccount" data-toggle="modal" data-paccount="'.$parametro['id'].'" class="no-hover" id="deletepaccount"><i class="fa fa-trash"></i>  '. lang('accounts_index_delete_btn') .'</a>
										  	</li>
										</ul>';

	    		$num++;
	    	}
    	}


    	 $output = [
			'sEcho' => 1,
			'iTotalRecords' => count($data),
			'iTotalDisplayRecords' => count($data),
			'aaData' => $data
		];

		echo json_encode($output);
    }

    public function addpaccount(){

    	$this->mPageTitle = lang('accounts_parameter_add_title');

    	$entrytypes = $this->DB1->get('entrytypes'.$this->DB1->dbsuffix)->result_array();

    	$this->data['entrytypes'] = $entrytypes;

    	/* Create list of ledgers to pass to view */
		$ledgers = new LedgerTree();
		$ledgers->Group = &$this->Group;
		$ledgers->Ledger = &$this->Ledger;
		$ledgers->current_id = -1;
		$ledgers->restriction_bankcash = 1;
		$ledgers->build(0);
		$ledgers->toList($ledgers, -1);

		$this->data['ledgers'] = $ledgers->ledgerList;

		if ($this->mAccountSettings->account_parameter_method == 1) {

			$taxes = $this->settings_model->getTaxes();
			$taxes2 = $this->settings_model->getTaxes2();
			$this->data['taxes'] = $taxes;
			$this->data['taxes2'] = $taxes2;
			
		} else if ($this->mAccountSettings->account_parameter_method == 2) {

			$categories = $this->settings_model->getCategories();
			$this->data['categories'] = $categories;
			
		}

    	if ($this->input->method() == 'post') {

    		$this->form_validation->set_rules('ledger_id', '', 'required');
    		$this->form_validation->set_rules('type', '', 'required');

    		if ($this->form_validation->run() == FALSE) {

    			$this->render('settings/addpaccount');

			} else {

				if ($this->mAccountSettings->account_parameter_method == 1) {
					$existe = $this->DB1->where(
						array('type' => $this->input->post('type'),
								'typemov' => $this->input->post('typemov'),
								'id_tax' => $this->input->post('tax_id')
								))->get('accounts'.$this->DB1->dbsuffix);
				} else if ($this->mAccountSettings->account_parameter_method == 2) {
					$existe = $this->DB1->where(
						array('type' => $this->input->post('type'),
								'typemov' => $this->input->post('typemov'),
								'id_category' => $this->input->post('category_id')
								))->get('accounts'.$this->DB1->dbsuffix);
				}

				if ($existe->num_rows() > 0) {
					$this->session->set_flashdata('error', lang('accounts_parameter_add_parameter_exists'));
						redirect('account_settings/accounts_parameter');
				} else {
					$datos = array(
								'type' => $this->input->post('type'),
								'typemov' => $this->input->post('typemov'),
								'id_tax' => $this->input->post('tax_id'),
								'id_category' => $this->input->post('category_id'),
								'ledger_id' => $this->input->post('ledger_id'),
								);
					$add = $this->DB1->insert('accounts'.$this->DB1->dbsuffix, $datos);
					if ($add) {
						$this->session->set_flashdata('message', lang('accounts_parameter_add_success'));
						redirect('account_settings/addpaccount');
					} else {
						$this->session->set_flashdata('error', lang('accounts_parameter_add_unsuccess'));
						redirect('account_settings/addpaccount');
					}
				}

			}

    	} else {

	    	$this->render('settings/addpaccount');

    	}

    }

    public function editpaccount($parameter = NULL){

    	$this->mPageTitle = lang('accounts_parameter_edit_title');

    	if ($this->mAccountSettings->account_parameter_method == 1) {
			
			$taxes = $this->settings_model->getTaxes();
			$taxes2 = $this->settings_model->getTaxes2();
			$this->data['taxes'] = $taxes;
			$this->data['taxes2'] = $taxes2;
			
		} else if ($this->mAccountSettings->account_parameter_method == 2) {

			$categories = $this->settings_model->getCategories();
			$this->data['categories'] = $categories;
			
		}

    	$entrytypes = $this->DB1->get('entrytypes'.$this->DB1->dbsuffix)->result_array();

    	$this->data['entrytypes'] = $entrytypes;

    	/* Create list of ledgers to pass to view */
		$ledgers = new LedgerTree();
		$ledgers->Group = &$this->Group;
		$ledgers->Ledger = &$this->Ledger;
		$ledgers->current_id = -1;
		$ledgers->restriction_bankcash = 1;
		$ledgers->build(0);
		$ledgers->toList($ledgers, -1);

		$this->data['ledgers'] = $ledgers->ledgerList;

    	if ($parameter != NULL) {
    		$dataparameter = $this->DB1->where('accounts'.$this->DB1->dbsuffix.'.id', $parameter)->get('accounts'.$this->DB1->dbsuffix);

    		if ($dataparameter->num_rows() > 0) {

    			$dataparameter = $dataparameter->row_array();
    			$this->data['parameter'] = $dataparameter;

    			if ($this->input->method() == 'post') {

    				$this->form_validation->set_rules('ledger_id', '', 'required');
    				if ($this->form_validation->run() == FALSE) {

    					redirect('account_settings/editpaccount/'.$parameter);

    				} else {

    					$prevdata = $this->DB1->where('accounts'.$this->DB1->dbsuffix.'.id', $parameter)->get('accounts'.$this->DB1->dbsuffix)->row_array();

    					$revisar = 0;

    					if ($prevdata['type'] != $this->input->post('type') || $prevdata['typemov'] != $this->input->post('typemov')) {

    						if ($this->mAccountSettings->account_parameter_method == 1 && $prevdata['id_tax'] != $this->input->post('tax_id')) {
    							$revisar = 1;
		    					$existe = $this->DB1->where(
								array('type' => $this->input->post('type'),
										'typemov' => $this->input->post('typemov'),
										'id_tax' => $this->input->post('tax_id')
										))->get('accounts'.$this->DB1->dbsuffix);
    						} else if ($this->mAccountSettings->account_parameter_method == 2 && $prevdata['id_category'] != $this->input->post('category_id')) {
    							$revisar = 1;
		    					$existe = $this->DB1->where(
								array('type' => $this->input->post('type'),
										'typemov' => $this->input->post('typemov'),
										'id_category' => $this->input->post('category_id')
										))->get('accounts'.$this->DB1->dbsuffix);
    						}
    					}

						if ($existe->num_rows() > 0 && $revisar == 1) {

							$this->session->set_flashdata('error', lang('accounts_parameter_edit_parameter_exists'));
								redirect('account_settings/accounts_parameter');

						} else {

							$datos = array(
								'ledger_id' => $this->input->post('ledger_id'),
								);
							$update = $this->DB1->where('accounts'.$this->DB1->dbsuffix.'.id', $parameter)->update('accounts'.$this->DB1->dbsuffix, $datos);
							if ($update) {
								$this->session->set_flashdata('message', lang('accounts_parameter_edit_success'));
								redirect('account_settings/accounts_parameter');
							} else {
								$this->session->set_flashdata('error', lang('accounts_parameter_edit_unsuccess'));
								redirect('account_settings/editpaccount/'.$parameter);
							}

						}

    				}

    			} else {

    				$this->render('settings/editpaccount');

    			}

    		} else {
    			$this->session->set_flashdata('error', lang('accounts_parameter_edit_not_found'));
				redirect('account_settings/accounts_parameter');
    		}

    	} else {
    		$this->session->set_flashdata('error', lang('accounts_parameter_edit_not_set'));
			redirect('account_settings/accounts_parameter');
    	}

    }


    public function deletepaccount($parameter = NULL){
    	if ($parameter != NULL) {
    		$eliminar = $this->DB1->delete('accounts'.$this->DB1->dbsuffix, array('id' => $parameter));

    		if ($eliminar) {
    			$this->session->set_flashdata('message', lang('accounts_parameter_delete_success'));
    			redirect('account_settings/accounts_parameter');
    		} else {
    			$this->session->set_flashdata('message', lang('accounts_parameter_delete_unsuccess'));
    			redirect('account_settings/accounts_parameter');
    		}

    	} else {
    		$this->session->set_flashdata('error', lang('accounts_parameter_edit_not_found'));
    		redirect('account_settings/accounts_parameter');
    	}
    }

    //Payment methods

    public function payment_methods(){
    	redirect('dashboard');
    	$this->mPageTitle = lang('payment_methods_index_title');
    	$this->render('settings/payment_methods');
    }

    public function getPaymentsMethods(){
    	$paymentmethods = $this->DB1->get_where('payment_methods'.$this->DB1->dbsuffix, array('entity !=' => 'n/a'))->result_array();
    	$data = [];
    	$num = 0;

    	$ledgers = [];
    	$dataledger = $this->DB1->get('ledgers'.$this->DB1->dbsuffix)->result_array();
    	foreach ($dataledger as $key => $ledger) {
    		$ledgers[$ledger['id']] = $ledger['code']." - ".$ledger['name'];
    	}

    	foreach ($paymentmethods as $key => $pm) {
    		$data[$num]['type'] = $pm['type'];
    		// $data[$num]['entity'] = $pm['entity'];
    		$data[$num]['receipt_ledger'] = (isset($ledgers[$pm['receipt_ledger_id']])) ? $ledgers[$pm['receipt_ledger_id']] : "N/A";
    		$data[$num]['payment_ledger'] = (isset($ledgers[$pm['payment_ledger_id']])) ? $ledgers[$pm['payment_ledger_id']] : "N/A";
    		$data[$num]['actions'] = '<div class="btn-group">
								  <div class="dropdown">
								    <button class="btn btn-success btn-sm" type="button" id="accionesProducto" data-toggle="dropdown" aria-haspopup="true">
								      Acciones
								      <span class="caret"></span>
								    </button>
								    <ul class="dropdown-menu pull-right" aria-labelledby="accionesProducto" style="width:220%;">
									  	<li>
									  		<a href="'. base_url().'account_settings/editpaymentmethod/'. $pm['id'] .'" class="no-hover" escape="false"><i class="fa fa-pencil"></i>  '. lang('entries_views_index_th_actions_edit_btn') .'</a>
									  	</li>
									  	<li>
									  		<a href="#modalDeletePaymentMethod" data-toggle="modal" data-paymentmethod="'.$pm['id'].'" class="no-hover" id="deletepaccount"><i class="fa fa-trash"></i>  '. lang('accounts_index_delete_btn') .'</a>
									  	</li>
									</ul>';

    		$num++;
    	}

    	$output = [
			'sEcho' => 1,
			'iTotalRecords' => count($data),
			'iTotalDisplayRecords' => count($data),
			'aaData' => $data
		];

		echo json_encode($output);
    }

    public function addpaymentmethod(){

    	redirect('dashboard');

    	$this->mPageTitle = lang('payment_methods_add_title');

    	/* Create list of ledgers to pass to view */
		$ledgers = new LedgerTree();
		$ledgers->Group = &$this->Group;
		$ledgers->Ledger = &$this->Ledger;
		$ledgers->current_id = -1;
		$ledgers->restriction_bankcash = 1;
		$ledgers->build(0);
		$ledgers->toList($ledgers, -1);

		$this->data['ledgers'] = $ledgers->ledgerList;

    	if ($this->input->method() == 'post') {
    		$this->form_validation->set_rules('type', '', 'required');
    		$this->form_validation->set_rules('receipt_ledger_id', '', 'required');

    		if ($this->form_validation->run() == FALSE) {

    			$this->render('settings/addpaymentmethod');

    		} else {

    			$existe = $this->DB1->where(
    				array(
    					'type' => $this->input->post('type'),
    					'entity' => $this->input->post('entity')
    				)
    			)->get('payment_methods'.$this->DB1->dbsuffix);

    			if ($existe->num_rows() > 0) {
    				$this->session->set_flashdata('error', lang('payment_methods_add_parameter_exists'));
    				redirect('account_settings/payment_methods');
    			} else {

    				$data = array(
    							'type' => $this->input->post('type'),
    							'entity' => $this->input->post('entity'),
    							'receipt_ledger_id' => $this->input->post('receipt_ledger_id'),
    							'payment_ledger_id' => $this->input->post('payment_ledger_id')
    							);

    				$insert = $this->DB1->insert('payment_methods'.$this->DB1->dbsuffix, $data);

    				if ($insert) {
    					$this->session->set_flashdata('message', lang('payment_methods_add_success'));
    					redirect('account_settings/payment_methods');
    				} else {
    					$this->session->set_flashdata('error', lang('payment_methods_add_unsuccess'));
    					redirect('account_settings/payment_methods');
    				}

    			}

    		}


    	} else {

    		$pm = $this->DB1->get('payment_methods');
    		if ($pm->num_rows()> 0) {
    			foreach (($pm->result()) as $row) {
    				$payment_methods[] = $row;
    			}
    		}

    		if (isset($payment_methods)) {
    			$this->data['payment_methods'] = $payment_methods;
    		}

    		$this->render('settings/addpaymentmethod');
    	}
    }

    public function editpaymentmethod($paymentmethod = NULL){
    	$this->mPageTitle = lang('payment_methods_edit_title');

    	/* Create list of ledgers to pass to view */
		$ledgers = new LedgerTree();
		$ledgers->Group = &$this->Group;
		$ledgers->Ledger = &$this->Ledger;
		$ledgers->current_id = -1;
		$ledgers->restriction_bankcash = 1;
		$ledgers->build(0);
		$ledgers->toList($ledgers, -1);

		$this->data['ledgers'] = $ledgers->ledgerList;

		if ($paymentmethod != NULL) {

			$paymentmethoddata = $this->DB1->where('payment_methods'.$this->DB1->dbsuffix.'.id', $paymentmethod)->get('payment_methods'.$this->DB1->dbsuffix)->row_array();

			$this->data['paymentmethod'] = $paymentmethoddata;

			if ($this->input->method() == 'post') {
				$this->form_validation->set_rules('receipt_ledger_id', '', 'required');

				if ($this->form_validation->run() == FALSE) {
					redirect('account_settings/editpaymentmethod/'.$paymentmethod);
				} else {

					$datosprev = $this->DB1->where('payment_methods'.$this->DB1->dbsuffix.'.id', $paymentmethod)->get('payment_methods'.$this->DB1->dbsuffix)->row_array();

					$revisar = 0;

					if ($datosprev['type'] != $this->input->post('type') || $datosprev['entity'] != $this->input->post('entity')) {
						$revisar = 1;
					}

					$existe = $this->DB1->where(
	    				array(
	    					'type' => $this->input->post('type'),
	    					'entity' => $this->input->post('entity')
	    				)
	    			)->get('payment_methods'.$this->DB1->dbsuffix);

					if ($existe->num_rows() > 0 && $revisar == 1) {
						$this->session->set_flashdata('error', lang('payment_methods_edit_parameter_exists'));
						redirect('account_settings/payment_methods');
					} else {
						$datos = array(
							'receipt_ledger_id' => $this->input->post('receipt_ledger_id'),
							'payment_ledger_id' => $this->input->post('payment_ledger_id'),
							);
						$update = $this->DB1->where('payment_methods'.$this->DB1->dbsuffix.'.id', $paymentmethod)->update('payment_methods'.$this->DB1->dbsuffix, $datos);
						if ($update) {
							$this->session->set_flashdata('message', lang('payment_methods_edit_success'));
							redirect('account_settings/payment_methods');
						} else {
							$this->session->set_flashdata('error', lang('payment_methods_edit_unsuccess'));
							redirect('account_settings/editpaymentmethod/'.$parameter);
						}
					}
				}

			} else {
				$pm = $this->DB1->get('payment_methods');
	    		if ($pm->num_rows()> 0) {
	    			foreach (($pm->result()) as $row) {
	    				$payment_methods[] = $row;
	    			}
	    		}

	    		if (isset($payment_methods)) {
	    			$this->data['payment_methods'] = $payment_methods;
	    		}

				$this->render('settings/editpaymentmethod');
			}

		} else {
			$this->session->set_flashdata('error', lang('payment_methods_edit_not_set'));
			redirect('account_settings/payment_methods');
		}
    }

    public function deletepaymentmethod($paymentmethod = NULL){
    	if ($paymentmethod != NULL) {

			$pm = $this->DB1->get_where('payment_methods'.$this->DB1->dbsuffix, array('id' => $paymentmethod));
			if ($pm->num_rows() > 0) {
				$pm = $pm->row();
			}

			$payments = $this->DB1->get_where('payments', array('paid_by' => $pm->type));
			if ($this->mAccountSettings->modulary && $payments->num_rows() > 0) {
				$this->session->set_flashdata('error', lang('cannot_delete_payment_method_used'));
				redirect('account_settings/payment_methods');
			} else {
				$eliminar = $this->DB1->delete('payment_methods'.$this->DB1->dbsuffix, array('id' => $paymentmethod));
	    		if ($eliminar) {
	    			if ($this->mAccountSettings->modulary && $pm) {
						$this->DB1->delete('payment_methods', array('code' => $pm->type));
	    			}
	    			$this->session->set_flashdata('message', lang('payment_methods_delete_success'));
	    			redirect('account_settings/payment_methods');
	    		} else {
	    			$this->session->set_flashdata('message', lang('payment_methods_delete_unsuccess'));
	    			redirect('account_settings/payment_methods');
	    		}
			}


    	} else {
    		$this->session->set_flashdata('error', lang('payment_methods_edit_not_set'));
			redirect('account_settings/payment_methods');
    	}
    }

    public function cost_centers(){
    	$this->mPageTitle = lang('cost_centers_index_title');
    	$this->render('settings/cost_centers');
    }

    public function getCostCenters(){
    	$costcenters = $this->DB1->get('cost_centers'.$this->DB1->dbsuffix)->result_array();
    	$data = [];
    	$num = 0;

    	foreach ($costcenters as $key => $pm) {
    		$data[$num]['code'] = $pm['code'];
    		$data[$num]['name'] = $pm['name'];
    		$data[$num]['actions'] = '<div class="btn-group">
								  <div class="dropdown">
								    <button class="btn btn-success btn-sm" type="button" id="accionesProducto" data-toggle="dropdown" aria-haspopup="true">
								      Acciones
								      <span class="caret"></span>
								    </button>
								    <ul class="dropdown-menu pull-right" aria-labelledby="accionesProducto" style="width:220%;">
									  	<li>
									  		<a href="#modalEditCostCenter" data-toggle="modal" data-id="'.$pm['id'].'" data-code="'.$pm['code'].'"  data-name="'.$pm['name'].'" class="no-hover" id="editcostcenter"><i class="fa fa-pencil"></i>  '. lang('entries_views_index_th_actions_edit_btn') .'</a>
									  	</li>
									</ul>';

    		$num++;
    	}


									  	// <li>
									  	// 	<a href="#modalDeletePaymentMethod" data-toggle="modal" data-paymentmethod="'.$pm['id'].'" class="no-hover" id="deletepaccount"><i class="fa fa-trash"></i>  '. lang('accounts_index_delete_btn') .'</a>
									  	// </li>

    	$output = [
			'sEcho' => 1,
			'iTotalRecords' => count($data),
			'iTotalDisplayRecords' => count($data),
			'aaData' => $data
		];

		echo json_encode($output);
    }


    public function add_cost_center(){
    	$code = $this->input->post('code');
    	$name = $this->input->post('name');
    	$data = array(
    				'code' => $code,
    				'name' => $name,
    				);

    	$insert = $this->DB1->insert('cost_centers'.$this->DB1->dbsuffix, $data);
		if ($insert) {
			echo true;
		} else {
			echo false;
		}
    }

    public function edit_cost_center(){
    	$id = $this->input->post('id_cost_center');
    	$code = $this->input->post('code');
    	$name = $this->input->post('name');
    	$data = array(
    				'code' => $code,
    				'name' => $name,
    				);

    	$update = $this->DB1->update('cost_centers'.$this->DB1->dbsuffix, $data, array('id' => $id));
		if ($update) {
			echo true;
		} else {
			echo false;
		}

    }

    public function validate_cost_center_code(){
    	$code = $this->input->post('code');
    	$condicion['code'] = $code;
    	if ($this->input->post('id')) {
    		$condicion['id !='] = $this->input->post('id');
    	}
    	$cc = $this->DB1->get_where('cost_centers'.$this->DB1->dbsuffix, $condicion);
    	if ($cc->num_rows() > 0) {
    		echo true;
    	} else {
    		echo false;
    	}
    }


    public function close_year_create_new_year(){
    	// 
		$new_config = $this->site->get_new_connection();
		// exit($this->functionscore->print_arrays($new_config));
    	$fiscalyear = date("Y", strtotime($this->mAccountSettings->fy_end));
		$newfiscalyear = $fiscalyear+1;
		$actualanosufijo = substr($fiscalyear, 2, 4);
		$anosufijo = substr($newfiscalyear, 2, 4);
    	$settings = $this->db->select('of_settings.prefijo_db')->get('of_settings')->row_array();
		$prefijo = $this->DB1->dbprefix;
		$sufijo = "_con".$anosufijo;
		/* Check financial year start is before end */
		$fiscalyear = date("Y", strtotime($this->mAccountSettings->fy_end));
		$newfiscalyear = $fiscalyear+1;
		$actualanosufijo = substr($fiscalyear, 2, 4);
		$anosufijo = substr($newfiscalyear, 2, 4);
		$new_label = $this->session->userdata('active_account')->label;
		$new_label = str_replace($actualanosufijo, $anosufijo, $new_label);
		$new_label = str_replace(" ", "", $new_label);
		$new_label = str_replace(".", "", $new_label);
		$new_label = str_replace(",", "", $new_label);
		$new_label = str_replace("-", "", $new_label);
		$new_label = str_replace("/", "", $new_label);
		$new_label = str_replace("_", "", $new_label);
		$fy_start 	= $newfiscalyear."-01-01";
		$fy_end 	= $newfiscalyear."-12-31";

		$dbexists = $this->db->query('SHOW DATABASES LIKE "%'.$new_config['database'].'%";');
		if ($dbexists->num_rows() > 0) {
			$this->db->query('USE `'.$new_config['database'].'`');
			$tablesexists = $this->db->query('SHOW TABLES LIKE "%'.($new_config['dbprefix']."settings".$new_config['dbsuffix']).'%"');
			if ($tablesexists->num_rows() > 0) {
				$accion = 3;
			} else {
				$accion = 2;
			}
			$this->db->query('USE `'.$this->db->database.'`');
		} else {
			$accion = 1;
		}
		//ACCIONES 1. Crear DB y tablas, 2. Crear sólo tablas, 3. Actualizar saldos
		if ($accion == 1) {
			$this->db->query("CREATE DATABASE IF NOT EXISTS `".$new_config['database']."`;");
		}
		if ($accion <= 2) {
			/* Read old settings */
			$old_account_setting = json_decode(json_encode($this->mAccountSettings), true);
			$old_account_setting['fy_start'] = $fy_start;
			$old_account_setting['fy_end'] = $fy_end;
			if ($this->check_database($new_config)) {
				$DB2 = $this->load->database($new_config, TRUE);
			} else {
				redirect('settings/cf');
			}
			$tables = $this->DB1->query("SELECT TABLE_NAME
                                  FROM information_schema.TABLES
                                 WHERE TABLE_SCHEMA = SCHEMA()
                                   AND TABLE_NAME LIKE '%".$this->DB1->dbprefix."%'
                                   AND TABLE_NAME LIKE '%".$actualanosufijo."%'");
            $DB2->query("SET FOREIGN_KEY_CHECKS = 0;");
            $DB2->query("SET sql_mode = '';");
            if ($tables->num_rows() > 0) {
            	// exit(var_dump($tables->result()));
                foreach ($tables->result() as $tbl_key => $tbl_name) {
                    $create_statement = $this->DB1->query("SHOW CREATE TABLE `".$tbl_name->TABLE_NAME."`");
                    if ($create_statement->num_rows() > 0) {
                        $create_statement = $create_statement->row();
                        if (isset($create_statement->{"Create Table"})) {
                            $DB2->query(str_replace($actualanosufijo, $anosufijo, $create_statement->{"Create Table"}));
                        }
                    }
                }
            }

			$thirds_exists = $DB2->query('SHOW TABLES LIKE "'.$new_config['dbprefix']."companies".'"');
			if ($thirds_exists->num_rows() == 0) {
				$schema = file_get_contents(APPPATH.'config/thirds.Mysql.sql');
				$schema = str_replace("%_PREFIX_%", $new_config['dbprefix'], $schema);
				$sqls = explode(';', $schema);
				array_pop($sqls);
				foreach ($sqls as $sql) {
					$DB2->query($sql.';');
				}
				$DB2->query("INSERT INTO `".$DB2->dbprefix."companies` (SELECT * FROM `".$this->DB1->database."`.`".$this->DB1->dbprefix."companies`)");
				$DB2->query("INSERT INTO `".$DB2->dbprefix."groups` (SELECT * FROM `".$this->DB1->database."`.`".$this->DB1->dbprefix."groups`)");
				$DB2->query("INSERT INTO `".$DB2->dbprefix."countries` (SELECT * FROM `".$this->DB1->database."`.`".$this->DB1->dbprefix."countries`)");
				$DB2->query("INSERT INTO `".$DB2->dbprefix."states` (SELECT * FROM `".$this->DB1->database."`.`".$this->DB1->dbprefix."states`)");
				$DB2->query("INSERT INTO `".$DB2->dbprefix."cities` (SELECT * FROM `".$this->DB1->database."`.`".$this->DB1->dbprefix."cities`)");

			}
			$schema = file_get_contents(APPPATH.'config/Functions.Mysql.sql');
			$schema = str_replace('%_DATABASE_%', $new_config['database'], $schema);
			$schema = str_replace('%_PREFIX_%', $new_config['dbprefix'], $schema);
			$schema = str_replace('%_SUFIX_%', $new_config['dbsuffix'], $schema); //sufix
			write_file(APPPATH.'config/Function_.Mysql.sql', $schema);
			$path = APPPATH.'config/Function_.Mysql.sql';
			if (!empty($schema)) {
				$DB2->query($schema);
			}
			/******* Add initial data ********/
			$this->load->library('AccountList');
			$DB2->query("INSERT INTO `".$DB2->dbprefix."groups".$DB2->dbsuffix."` (SELECT * FROM `".$this->DB1->database."`.`".$this->DB1->dbprefix."groups".$this->DB1->dbsuffix."`)");
			$DB2->query("INSERT INTO `".$DB2->dbprefix."ledgers".$DB2->dbsuffix."` (SELECT * FROM `".$this->DB1->database."`.`".$this->DB1->dbprefix."ledgers".$this->DB1->dbsuffix."`)");
			$DB2->query("INSERT INTO `".$DB2->dbprefix."entrytypes".$DB2->dbsuffix."` (SELECT * FROM `".$this->DB1->database."`.`".$this->DB1->dbprefix."entrytypes".$this->DB1->dbsuffix."`)");
			$DB2->query("INSERT INTO `".$DB2->dbprefix."tags".$DB2->dbsuffix."` (SELECT * FROM `".$this->DB1->database."`.`".$this->DB1->dbprefix."tags".$this->DB1->dbsuffix."`)");
			$DB2->query("INSERT INTO `".$DB2->dbprefix."accounts".$DB2->dbsuffix."` (SELECT * FROM `".$this->DB1->database."`.`".$this->DB1->dbprefix."accounts".$this->DB1->dbsuffix."`)");
			$DB2->query("INSERT INTO `".$DB2->dbprefix."cost_centers".$DB2->dbsuffix."` (SELECT * FROM `".$this->DB1->database."`.`".$this->DB1->dbprefix."cost_centers".$this->DB1->dbsuffix."`)");
			$DB2->query("INSERT INTO `".$DB2->dbprefix."movement_type".$DB2->dbsuffix."` (SELECT * FROM `".$this->DB1->database."`.`".$this->DB1->dbprefix."movement_type".$this->DB1->dbsuffix."`)");
			$DB2->query("INSERT INTO `".$DB2->dbprefix."payment_methods".$DB2->dbsuffix."` (SELECT * FROM `".$this->DB1->database."`.`".$this->DB1->dbprefix."payment_methods".$this->DB1->dbsuffix."`)");
			$DB2->insert('settings'.$new_config['dbsuffix'], $old_account_setting);

			$new_dts = $DB2->like('prefix', $actualanosufijo)->get('entrytypes'.$DB2->dbsuffix);
            if ($new_dts->num_rows() > 0) {
                foreach (($new_dts->result()) as $dt) {
                    $DB2->update('entrytypes'.$DB2->dbsuffix, 
                    	[
                    		'prefix' => str_replace($actualanosufijo, $anosufijo, $dt->prefix),
                    		'name' => str_replace($actualanosufijo, $anosufijo, $dt->name),
                    		'label' => str_replace($actualanosufijo, $anosufijo, $dt->label),
                    		'description' => str_replace($actualanosufijo, $anosufijo, $dt->description),
                    		'numbering' => 1
                    	], ['id' => $dt->id]);
                }
            }

			$new_label = $this->input->post('label');
			/* Only check for valid input data, save later */
			$insert_data = array(
				'label' 			=> $new_label,
				'name' 				=> $this->input->post('name'),
				'fy_start' 			=> $this->functionscore->dateToSql($fy_start),
				'fy_end' 			=> $this->functionscore->dateToSql($fy_end),
				'db_datasource' 	=> $this->DB1->dbdriver,
				'db_database'		=> $DB2->database,
				'db_host' 			=> $this->DB1->hostname,
				'db_port' 			=> $this->DB1->port,
				'db_login' 			=> $this->DB1->username,
				'db_password' 		=> $this->DB1->password,
				'db_prefix' 		=> strtolower($prefijo),
				'db_suffix' 		=> strtolower($sufijo),
				'db_schema' 		=> $this->DB1->schema,
				'db_unixsocket' 	=> '',
				'account_locked' 	=> 0
			);
			if ($this->input->post('persistent')) {
				$insert_data['db_persistent'] = 1;
			} else {
				$insert_data['db_persistent'] = 0;
			}
			$accountd = $this->db->get_where('accounts', ['label' => $new_label]);
			if ($accountd->num_rows() > 0) {
				$this->db->update('accounts', $insert_data, ['label' => $new_label]);
			} else {
				$this->db->insert('accounts', $insert_data);
			}
			$this->DB1->update('settings'.$this->DB1->dbsuffix, ['year_closed' => 1, 'close_year_step' => 1], ['id' => 1]);
			$DB2->update($DB2->dbprefix."ledgers".$DB2->dbsuffix, ['op_balance' => 0], ['id >=' => 0]);
			$this->DB1->insert('user_activities', [
                'date' => date('Y-m-d H:i:s'),
                'type_id' => 5,
                'table_name' => 'closing_year',
                'record_id' => 9999,
                'user_id' => $this->session->userdata('user_id'),
                'module_name' => 'contabilidad',
                'description' => $this->mUser->first_name." ".$this->mUser->last_name." realizó creación de año nuevo contable",
            ]);
		}
		echo json_encode(['response' => 1]);
    }

    public function close_year_initial_balance_transfer(){
    	/* Check financial year start is before end */
    	if ($this->DB1->update('settings'.$this->DB1->dbsuffix, ['close_year_step' => 2], ['id' => 1])) {
			$fiscalyear = date("Y", strtotime($this->mAccountSettings->fy_start));
			$newfiscalyear = $fiscalyear+1;
			$actualanosufijo = substr($fiscalyear, 2, 4);
			$anosufijo = substr($newfiscalyear, 2, 4);
			$new_label = $this->session->userdata('active_account')->label;
			$new_label = str_replace($actualanosufijo, $anosufijo, $new_label);
			$new_label = str_replace(" ", "", $new_label);
			$new_label = str_replace(".", "", $new_label);
			$new_label = str_replace(",", "", $new_label);
			$new_label = str_replace("-", "", $new_label);
			$new_label = str_replace("/", "", $new_label);
			$new_label = str_replace("_", "", $new_label);
			$fy_start 	= $newfiscalyear."-01-01";
			$fy_end 	= $newfiscalyear."-12-31";
			if($this->input->method() == 'post'){
				$new_config = $this->site->get_new_connection();
				$DB2 = $this->load->database($new_config, TRUE);
				$ledger_id = $this->input->post('ledger_id');
				$companies_id = $this->input->post('companies_id');
			}
			$dbexists = $this->db->query('SHOW DATABASES LIKE "%'.$new_config['database'].'%";');
			if ($dbexists->num_rows() > 0) {
				$this->db->query('USE `'.$new_config['database'].'`');
				$tablesexists = $this->db->query('SHOW TABLES LIKE "%'.($new_config['dbprefix']."settings".$new_config['dbsuffix']).'%"');
				if ($tablesexists->num_rows() > 0) {
					$accion = 3;
				} else {
					$accion = 2;
				}
				$this->db->query('USE `'.$this->db->database.'`');
			} else {
				$accion = 1;
			}

			if ($validation = $this->settings_model->validate_invalid_entry_items_data()) {
				if ($validation['error']) {
					$this->session->set_flashdata('error', $validation['msg']."</br>".lang('opening_balance_not_transferred'));
					exit(json_encode(['response' => 0]));
				}
			}

			if ($this->mAccountSettings->close_year_step >= 2) {
				if ($DB2->update($DB2->dbprefix."ledgers".$DB2->dbsuffix, ['op_balance' => 0], ['id >=' => 0])) {
					$DB2->truncate('companies_opbalance'.$DB2->dbsuffix);
					if (!$validation['error']) {
						$this->session->set_flashdata('message', lang('account_settings_cntrler_cf_add_success'));
						$this->get_opbalance_companies($ledger_id, $companies_id);
						foreach ($this->op_balance_ledgers as $ledger_id => $op) {
							$DB2->update($DB2->dbprefix."ledgers".$DB2->dbsuffix, ['op_balance' => $op['op_balance'], 'op_balance_dc' => $op['op_balance_dc']], ['id' => $ledger_id]);
						}
						$DB2->insert_batch('companies_opbalance'.$DB2->dbsuffix, $this->op_balance_companies);
						$this->DB1->update('settings'.$this->DB1->dbsuffix, ['close_year_step' => 3], ['id' => 1]);
						$this->DB1->insert('user_activities', [
			                'date' => date('Y-m-d H:i:s'),
			                'type_id' => 5,
			                'table_name' => 'closing_year',
			                'record_id' => 9999,
			                'user_id' => $this->session->userdata('user_id'),
			                'module_name' => 'contabilidad',
			                'description' => $this->mUser->first_name." ".$this->mUser->last_name." volvió a trasladar saldos",
			            ]);
					}
				}
			} else {
				if (!$validation['error']) {
					$this->session->set_flashdata('message', lang('account_settings_cntrler_cf_add_success'));
					$this->get_opbalance_companies($ledger_id, $companies_id);
					foreach ($this->op_balance_ledgers as $ledger_id => $op) {
						$DB2->update($DB2->dbprefix."ledgers".$DB2->dbsuffix, ['op_balance' => $op['op_balance'], 'op_balance_dc' => $op['op_balance_dc']], ['id' => $ledger_id]);
					}
					$DB2->insert_batch('companies_opbalance'.$DB2->dbsuffix, $this->op_balance_companies);
					$this->DB1->update('settings'.$this->DB1->dbsuffix, ['close_year_step' => 3], ['id' => 1]);
					$this->DB1->insert('user_activities', [
		                'date' => date('Y-m-d H:i:s'),
		                'type_id' => 5,
		                'table_name' => 'closing_year',
		                'record_id' => 9999,
		                'user_id' => $this->session->userdata('user_id'),
		                'module_name' => 'contabilidad',
		                'description' => $this->mUser->first_name." ".$this->mUser->last_name." trasladó saldos",
		            ]);
				}
			}
			
    	}

		unset($DB2);
		echo json_encode(['response' => 1]);
    }

}