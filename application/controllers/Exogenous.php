<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Exogenous extends Admin_Controller {

	public function __construct()
    {
		parent::__construct();
		$this->load->model('Exogenous_model');
    }   
	
	public function index()
	{
		
	}

	public function Formats()
	{
		// set page title
		$this->mPageTitle = lang('Exogenous_menu_Formats');
		$this->data['title'] = lang('Exogenous_menu_Formats');
		$this->data['Formats'] = $this->Exogenous_model->get_formats('');     

		// render page
		$this->render('exogenous/formats');
	}

	public function getFormats($id = '')
	{
		$this->mPageTitle = lang('Exogenous_menu_Formats_detall');
		$this->data['titles'] = array('title' => lang('Exogenous_menu_Formats') , 'url' => 'Exogenous/formats');
		$this->data['Formats'] = $this->Exogenous_model->get_formats($id);    
		$this->data['contents'] = $this->Exogenous_model->get_formats_content($id);  
		$this->data['types'] = $this->Exogenous_model->get_attribute_type();  		
		$this->render('exogenous/formats_content');
	}

	public function CreateFormats()
	{
		$code = $this->input->post('code');
		$version = $this->input->post('version');
		$name = $this->input->post('name');
		$description = $this->input->post('description');
		$min_amount = $this->input->post('min_amount');
		$use_tercero = $this->input->post('use_tercero');

    	$data = array(
			'code' => $code,
			'version' => $version,
			'name' => $name,
			'description' => $description,
			'min_amount' => $min_amount,
			'use_tercero' => $use_tercero,
		);

    	$insert = $this->DB1->insert('exogenous_format'.$this->DB1->dbsuffix, $data);

		if ($insert) {
			$this->session->set_flashdata('message', lang('Exogenous_Formats_message_add'));
    		redirect('Exogenous/formats');
		} else {
			$this->session->set_flashdata('error', lang('Exogenous_Formats_message_add_error'));
    		redirect('Exogenous/formats');
		}
	}

	public function EditFormats()
	{
		$id = $this->input->post('cod_formats');
		$code = $this->input->post('code');
		$version = $this->input->post('version');
		$name = $this->input->post('name');
		$description = $this->input->post('description');
		$min_amount = $this->input->post('min_amount');
		$use_tercero = $this->input->post('use_tercero');

    	$data = array(
			'code' => $code,
			'version' => $version,
			'name' => $name,
			'description' => $description,
			'min_amount' => $min_amount,
			'use_tercero' => $use_tercero,
		);

		$update = $this->DB1->update('exogenous_format'.$this->DB1->dbsuffix, $data, array('id' => $id));

		if ($update) {
			$this->session->set_flashdata('message', lang('Exogenous_Formats_message_edit'));
    		redirect('Exogenous/formats');
		} else {
			$this->session->set_flashdata('error', lang('Exogenous_Formats_message_edit_error'));
    		redirect('Exogenous/formats');
		}
	}

	public function CreateFormatsContent()
	{
		$size = $this->input->post('size');
		$obs = $this->input->post('obs');
		$name = $this->input->post('name');
		$order = $this->input->post('order');
		$atrib = $this->input->post('atrib');
		$type = $this->input->post('type');
		$cod_format = $this->input->post('cod_format');
		$format = $this->input->post('format');

    	$data = array(
			'size' => $size,
			'observation' => $obs,
			'name' => $name,
			'order_by' => $order,
			'attribute' => $atrib,
			'id_type' => $type,
			'id_format' => $cod_format,
		);

    	$insert = $this->DB1->insert('exogenous_content'.$this->DB1->dbsuffix, $data);

		if ($insert) {
			$this->session->set_flashdata('message', lang('Exogenous_Formats_content_message_add'));
    		redirect('Exogenous/getFormats/'.$format);
		} else {
			$this->session->set_flashdata('error', lang('Exogenous_Formats_content_message_add_error'));
    		redirect('Exogenous/getFormats/'.$format);
		}
	}

	public function EditFormatsContent()
	{
		$size = $this->input->post('Editsize');
		$obs = $this->input->post('Editobs');
		$name = $this->input->post('Editname');
		$order = $this->input->post('Editorder');
		$atrib = $this->input->post('Editatrib');
		$type = $this->input->post('Edittype');
		$cod_format = $this->input->post('Editcod_format');
		$format = $this->input->post('Editformat');
		$id = $this->input->post('EditIdConten');

    	$data = array(
			'size' => $size,
			'observation' => $obs,
			'name' => $name,
			'order_by' => $order,
			'attribute' => $atrib,
			'id_type' => $type,
			'id_format' => $cod_format,
		);

		$update = $this->DB1->update('exogenous_content'.$this->DB1->dbsuffix, $data, array('id' => $id));

		if ($update) {
			$this->session->set_flashdata('message', lang('Exogenous_Formats_content_message_edit'));
    		redirect('Exogenous/getFormats/'.$format);
		} else {
			$this->session->set_flashdata('error', lang('Exogenous_Formats_content_message_edit_error'));
    		redirect('Exogenous/getFormats/'.$format); 
		}
	}


}