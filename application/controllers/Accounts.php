<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Accounts extends Admin_Controller {
	public function __construct() {
        parent::__construct(); 
    }   
    
	public function index() {
		
		//conexión persistente
		$this->reconnect_persistent_database();

		$cost_center = $this->input->get('cc');

        $this->mBodyClass .= ' sidebar-collapse';
		
		$this->load->library('AccountList');
		
		$accountlist = new AccountList();
		$accountlist->Group = &$this->Group;
		$accountlist->Ledger = &$this->Ledger;
		$accountlist->start_date = null;
		$accountlist->end_date = null;
		$accountlist->affects_gross = -1;
		$accountlist->cost_center = $cost_center;
		$accountlist->start();
		$this->data['accountlist'] = $accountlist->account_list;
		$opdiff = $this->ledger_model->getOpeningDiff();
		$this->data['opdiff'] = $opdiff;
		if ($this->mAccountSettings->cost_center) {
			if ($this->user_cost_centers) {
				foreach ($this->user_cost_centers as $key => $value) {
					if ($key == 0) {
						$this->DB1->where('id', $value);
					} else {
						$this->DB1->or_where('id', $value);
					}
				}
			}
			$cost_centers_data = $this->DB1->get('cost_centers'.$this->DB1->dbsuffix);
			if ($cost_centers_data->num_rows() > 0) {
				$cost_centers_data = $cost_centers_data->result_array();
				foreach ($cost_centers_data as $row => $cost_center_data) {
					$cost_centers[] = $cost_center_data;
				}
			} else {
				$cost_centers = false;
			}
			$this->data['cost_centers'] = $cost_centers;
		}

		// render page
		$this->render('accounts/index');
	}

	public function log()
	{
		
		//conexión persistente
		$this->reconnect_persistent_database();

		// render log page
		$this->render('accounts/log');
	}

	public function mapper()
	{
		
		//conexión persistente
		$this->reconnect_persistent_database();

		$this->load->library('reader');
		if ($this->input->method() == 'post') {
			$keys = array();
			for($i = 0;$i < $_POST['number_of_keys'];$i++){
				$keys[$_POST['default'.$i]] = $_POST['current'.$i];
			}
			$result = $this->reader->parse_file($_POST['file_path']);
			$this->import($result, $keys);
		}		
	}

	public function uploader()
	{
		
		//conexión persistente
		$this->reconnect_persistent_database();

        if (isset($_FILES['accountcsv'])) {
        	if ($_FILES['accountcsv']['size'] > 0) {
			    $this->load->library('reader');

				$uploadPath = 'assets/uploads/tmp/';

	            $config['upload_path'] = $uploadPath;
	            $config['allowed_types'] = 'text|csv';
	            $config['file_name'] = $_FILES['accountcsv']['name'];
	            $config['overwrite'] = TRUE;
	            $this->load->library('upload', $config);
	            $this->upload->initialize($config);
	            if($this->upload->do_upload('accountcsv')){
	                $fileData = $this->upload->data();
					if ($keys = $this->check_keys($fileData['full_path'])) {
						$result = $this->reader->parse_file($fileData['full_path']);
						$this->import($result, $keys);
					}
	            }else{
	            	if ($this->upload->display_errors()) {
	            		$this->session->set_flashdata('error', $this->upload->display_errors());
	            	}else{
		            	$this->session->set_flashdata('error', lang('admin_cntrler_uploadprofilepicture_error'));
	            	}
	            }
			}
		}else{
			$this->render('accounts/uploader');
		}
	}

	public function check_keys($file_path)
	{
		
		//conexión persistente
		$this->reconnect_persistent_database();


		$default_keys = $this->reader->parse_file(base_url('assets/uploads/import.csv'), true);
		$current_keys = $this->reader->parse_file($file_path, true);
    	if ($default_keys != $current_keys) {
    		$this->data['default_keys'] = $default_keys;
			$this->data['current_keys'] = $current_keys;
			$this->data['file_path'] = $file_path;
    		$this->render('accounts/mapper');
    	}else{
    		$keys = array();
    		foreach ($default_keys as $key => $value) {
    			$keys[$key] = $key;
    		}
    		return $keys;
    	}
	}
	public function import($result, $keys)
	{
		//conexión persistente
		$this->reconnect_persistent_database();

		if (count($result) > 1) {
	    	$g_counter = 0;
	    	$l_counter = 0;
	    	
	    	foreach ($result as $data) {

	    		$parent_code = NULL;
		    	$parent_id = NULL;
				$code=[];
				$cod2 = "";
	    		$cod = $data[$keys['code']];

	    		//Con esta secuencia queda amarrado a que el código de los ledgers y groups, deben conformarse por 3 posiciones de a 2 dígitos y una última posición de a 4 dígitos(XX-XX-XX-XXXX), es decir, no puede tener más de 10 dígitos cualquier código.

	    		for($i = 0; $i <= 6; $i+=2){ //Si un número tiene más de 6 dígitos, el toma los primeros 6 de a 2 y los guarda en una posición, los últimos se guardan en una última posición quedando 00-00-00-0000
		
					if($i == 6){ //Si estamos en la última posición de $i, tomamos los últimos 4.
						$cod2 = substr($cod, $i, ($i+4));	
					} else { //Obtenemos del código del CSV de la posición en turno hasta dos posiciones más, ej del 1 al 2, del 3 al 4;
						$cod2 = substr($cod, $i, 2);	
						if(substr($cod, ($i+2), 2) != ""){ //Averiguamos si en la posición siguiente del $i ya no hay codigo para extraer, en caso de que si haya más código, tomamos el que hemos extraído hasta ahora y lo guardamos para obtener el código padre.
							$parent_code.=$cod2;
						} 
					}
					
					if($cod2 != ""){ //Si en la posición del $i se extrajo código, almacenamos lo extraído en el array
						$code[] = $cod2;
					} else { //Si en la posición del $i ya no se extrajo ningún código, rompemos la secuencia
						
						if($parent_code == "" && count($code) > 0){ //Antes de romper, revisamos si el código padre obtenido hasta ahora es vacío y si el array de códigos tiene al menos una posición, entonces, extraemos el primer dígito del código de la primera posición para almacearlo cómo padre.
							$parent_code = substr($code[0], 0, 1);
						}
						break;	
					}
				}

				if (isset($parent_code)) { //Se consulta el ID en la BD del código padre obtenido para su respectiva relación.
					$this->DB1->where('code', $parent_code);
					$query = $this->DB1->get('groups'.$this->DB1->dbsuffix, 1);
					if ($query->num_rows() == 1) {
						$parent_group = $query->row_array();
						$parent_id = $parent_group['id'];
					}
				}

	    		if(strtolower($data[$keys['account_type']]) == 'group'){
					$insertdata = array(
						'parent_id' => $parent_id,
						'name' => $data[$keys['name']],
						'code' => $data[$keys['code']],
						'affects_gross' => $data[$keys['affects_gross']]
					);
					// /* Save group */
					if ($this->DB1->insert('groups'.$this->DB1->dbsuffix, $insertdata)) {
						$g_counter++;
						$this->settings_model->add_log(lang('groups_cntrler_add_label_add_log') . $data[$keys['name']], 1);
					}
				}
				if (strtolower($data[$keys['account_type']]) == 'ledger') {
					$insertdata = array(
						'code' => $data[$keys['code']],
						'op_balance' => $data[$keys['opening_balance']],
						'name' => $data[$keys['name']],
						'group_id' => $parent_id,
						'op_balance_dc' => $data[$keys['debit_credit']],
						'notes' => $data[$keys['notes']],
						'reconciliation' => $data[$keys['reconciliation']],
						'type' => $data[$keys['bank_cash']],
					);
					/* Count number of decimal places */
					if($this->DB1->insert('ledgers'.$this->DB1->dbsuffix, $insertdata)){
						$this->settings_model->add_log(lang('ledgers_cntrler_add_label_add_log') . $data[$keys['name']], 1);
						$l_counter++;
					}
				}
	    	}
	    	$this->session->set_flashdata('message', sprintf(lang('accounts_exporter_exported_successfully'), $g_counter, $l_counter));
	    	redirect('accounts');
		}
	}

	public function download($file_path=null)
	{
		
		//conexión persistente
		$this->reconnect_persistent_database();

		if ($file_path == null) {
			$file_path = 'import.csv';
		}

		$this->load->helper('download'); //load helper
        $download_path = $file_path;

        if(!empty($download_path)){
		    $data = file_get_contents(base_url() ."assets/uploads/".$download_path); // Read the file's contents
		    $name = $download_path;
		 
		    force_download($name, $data);
		}
	}

	public function export_csv(){

		header('Content-Type: text/csv');
    	header('Content-Disposition: attachment; filename="import.csv";');

		echo "Account Type,Affects Gross,Name,Code,Opening Balance,Debit/Credit,Bank/Cash,Reconciliation,Notes".PHP_EOL;
   
   		$data = $this->DB1->get('groups'.$this->DB1->dbsuffix);

   		foreach ($data->result() as $row)
		{
			$codigo = $row->code;
			$cnt = 0;
			for($i = 2; $i <= 6; $i+=2){
				$codigo = substr_replace($codigo , "", $i+$cnt, 0);
				$cnt++;
			}
			$codigo = trim($codigo, "-");
			// if (strlen($codigo) > 1) {
			// 	$codigo = substr($codigo, 0, 1)."-".$codigo;
			// }
			$namer = str_replace(",", "", $row->name);
			echo "group,".$row->affects_gross.",".$namer.",".$codigo.",null,null,null,null,null".PHP_EOL;
		}

		$data = $this->DB1->get('ledgers'.$this->DB1->dbsuffix);
		foreach ($data->result() as $row)
		{
			$codigo = $row->code;
			$cnt = 0;
			for($i = 2; $i <= 6; $i+=2){
				$codigo = substr_replace($codigo , "", $i+$cnt, 0);
				$cnt++;
			}
			// if (strlen($codigo) > 1) {
			// 	$codigo = substr($codigo, 0, 1)."-".$codigo;
			// }
			echo "ledger,null,".$row->name.",".$codigo.",".$row->op_balance.",".$row->op_balance_dc.",".$row->type.",".$row->reconciliation.",".$row->notes."".PHP_EOL;
		}
	}
}