<?php 

Class Site Extends CI_Model {
	
	public function __construct()
    {
        parent::__construct();
    }

	public function get_new_connection(){
    	$fiscalyear = date("Y", strtotime($this->mAccountSettings->fy_end));
		$newfiscalyear = $fiscalyear+1;
		$actualanosufijo = substr($fiscalyear, 2, 4);
		$anosufijo = substr($newfiscalyear, 2, 4);
    	$settings = $this->db->select('of_settings.prefijo_db')->get('of_settings')->row_array();
		$prefijo = $this->DB1->dbprefix;
		$sufijo = "_con".$anosufijo;
		if ($this->mComercialAccountSettings->years_database_management == 1) {
			$dbname = $this->DB1->database;
		} else {
			if (strpos($this->DB1->database, "_".$actualanosufijo)) {
				$dbname = $this->DB1->database;
				$bdarr = explode("_", $dbname);
				$dbsufijo = "_".$anosufijo;
				$dbname = str_replace("_".$actualanosufijo, "", $dbname);
				$dbname = $dbname.$dbsufijo;
			} else {
				$dbname = $this->DB1->database;
			}
		}
		// exit(var_dump($dbname));
		$new_config['hostname'] = $this->DB1->hostname;
		$new_config['username'] = $this->DB1->username;
		$new_config['password'] = $this->DB1->password;
		$new_config['database'] = $dbname;
		$new_config['dbdriver'] = $this->DB1->dbdriver;
		$new_config['dbprefix'] = strtolower($prefijo);
		$new_config['dbsuffix'] = strtolower($sufijo);
		$new_config['db_debug'] = TRUE;
		$new_config['cache_on'] = FALSE;
		$new_config['cachedir'] = "";
		$new_config['schema'] 	= $this->input->post('db_schema');
		$new_config['port'] 	= $this->DB1->port;
		$new_config['char_set'] = "utf8";
		$new_config['dbcollat'] = "utf8_general_ci";
		if ($this->input->post('persistent')) {
			$new_config['pconnect'] = TRUE;
		} else {
			$new_config['pconnect'] = FALSE;
		}
		return $new_config;
    }

    function get_entry_type_by_label($label){
    	$q = $this->DB1->get_where('entrytypes'.$this->DB1->dbsuffix, ['label' => $label]);
    	if ($q->num_rows() > 0) {
    		return $q->row();
    	}
    	return false;
    }
    function get_third_by_vatno($vatno){
    	$q = $this->DB1->get_where('companies', ['vat_no' => $vatno]);
    	if ($q->num_rows() > 0) {
    		return $q->row();
    	}
    	return false;
    }
    function get_ledger_by_code($ledger_code){
    	$q = $this->DB1->get_where('ledgers'.$this->DB1->dbsuffix, ['code' => $ledger_code]);
    	if ($q->num_rows() > 0) {
    		return $q->row();
    	}
    	return false;
    }
    function get_cost_center_by_code($cost_center_code){
    	$q = $this->DB1->get_where('cost_centers'.$this->DB1->dbsuffix, ['code' => $cost_center_code]);
    	if ($q->num_rows() > 0) {
    		return $q->row();
    	}
    	return false;
    }
    function check_duplicate_entry($third, $number, $entrytype){
    	$q = $this->DB1->get_where('entries'.$this->DB1->dbsuffix, [
    			'entrytype_id' => $entrytype,
    			'number' => $number,
    			'companies_id' => $third,
    			]);
    	if ($q->num_rows() > 0) {
    		return $q->row();
    	}
    	return false;
    }
    function get_third_type($third_type){
    	$q = $this->DB1->get_where('groups', ['name' => $third_type]);
    	if ($q->num_rows() > 0) {
    		return $q->row();
    	}
    	return false;
    }
    function get_document_type($document_type){
    	$q = $this->DB1->get_where('documentypes', ['nombre' => $document_type]);
    	if ($q->num_rows() > 0) {
    		return $q->row();
    	}
    	return false;
    }
    function get_country($country){
    	$q = $this->DB1->get_where('countries', ['NOMBRE' => $country]);
    	if ($q->num_rows() > 0) {
    		return $q->row();
    	}
    	return false;
    }
    function get_state($state){
    	$q = $this->DB1->get_where('states', ['DEPARTAMENTO' => $state]);
    	if ($q->num_rows() > 0) {
    		return $q->row();
    	}
    	return false;
    }
    function get_city($city){
    	$q = $this->DB1->get_where('cities', ['DESCRIPCION' => $city]);
    	if ($q->num_rows() > 0) {
    		return $q->row();
    	}
    	return false;
    }
    function validate_third_exists($vat_no, $third_type){
    	$q = $this->DB1->get_where('companies', ['vat_no' => $vat_no, 'group_name' => $third_type]);
    	if ($q->num_rows() > 0) {
    		return $q->row();
    	}
    	return false;
    }

    function get_created_by($origin, $user_id){
    	if ($origin == 1) {
    		$q = $this->db->get_where('users', ['id' => $user_id]);
    		if ($q->num_rows() > 0) {
    			return $q->row();
    		}
    	} else {
    		$q = $this->DB1->get_where('users', ['id' => $user_id]);
    		if ($q->num_rows() > 0) {
    			return $q->row();
    		}
    	}
    	return false;
    }

}