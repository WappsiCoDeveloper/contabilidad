<?php 

Class Companies_model Extends CI_Model {
	
	public function __construct()
    {
        parent::__construct();
    }

	public function get_company_by_id($company_id){
		
		$company = $this->DB1->get_where('companies', ['id' => $company_id]);

		if ($company->num_rows() > 0) {
			$company = $company->row_array();
			return $company;
		}

		return false;

	}

}