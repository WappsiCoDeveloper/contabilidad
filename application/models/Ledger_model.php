<?php
Class Ledger_model extends CI_Model {

	public function __construct()
    {
        parent::__construct();
    }

/**
 * Calculate opening balance of specified ledger account for the given
 * date range
 *
 * @param1 int ledger id
 * @param2 date start date
 * @param3 skip op balance
 * @param4 skip entries disapproved
 * @param5 filter by entry type
 * @param6 filter by third (companies)
 * @return array D/C, Amount
 */
	function openingBalance($ledger_id, $start_date = null, $skip_op_balance = FALSE, $skip_entries_disapproved = FALSE, $entry_type = NULL, $company = NULL, $cost_center = NULL) {

		if ($skip_op_balance) {

			$op_total = 0;
			$op_total_dc = 'C';

		} else {

			$condicion = '0';

			if (!$start_date) {
				$start_date = $this->mAccountSettings->fy_start;
			}

			$this->DB1->select('
						SUM(
							IF(
								'.$this->DB1->dbprefix.'entryitems'.$this->DB1->dbsuffix.'.dc = "C"
								, IF(
										'.$this->DB1->dbprefix.'entries'.$this->DB1->dbsuffix.'.date < "'.$start_date.'",
										'.$this->DB1->dbprefix.'entryitems'.$this->DB1->dbsuffix.'.amount,
										'.$condicion.'
									)
								, 0
							   )
							) AS c_amount,
						SUM(
							IF(
								'.$this->DB1->dbprefix.'entryitems'.$this->DB1->dbsuffix.'.dc = "D"
								, IF(
										'.$this->DB1->dbprefix.'entries'.$this->DB1->dbsuffix.'.date < "'.$start_date.'",
										'.$this->DB1->dbprefix.'entryitems'.$this->DB1->dbsuffix.'.amount,
										'.$condicion.'
									)
								, 0
							   )
							) AS d_amount
						')
			  ->join('entryitems'.$this->DB1->dbsuffix, 'entryitems'.$this->DB1->dbsuffix.'.entry_id = entries'.$this->DB1->dbsuffix.'.id');

			$conditions = [];

			if ($ledger_id) {
				$conditions['entryitems'.$this->DB1->dbsuffix.'.ledger_id'] = $ledger_id;
			}
			if ($start_date) {
				// $conditions['entries'.$this->DB1->dbsuffix.'.date <'] = $start_date;
			}
			if ($skip_entries_disapproved) {
				$conditions['entries'.$this->DB1->dbsuffix.'.state'] = 1;
			} else {
				$conditions['entries'.$this->DB1->dbsuffix.'.state !='] = 0;
			}

			// if ($cost_center) {
			// 	$conditions['companies_opbalance'.$this->DB1->dbsuffix.'.cost_center_id ='] = $cost_center;
			// }

			if ($entry_type) {
				$conditions['entries'.$this->DB1->dbsuffix.'.entrytype_id ='] = $entry_type;
			}
			if ($company) {
				$conditions['entryitems'.$this->DB1->dbsuffix.'.companies_id ='] = $company;
			}
			if ($cost_center) {
				$conditions['entryitems'.$this->DB1->dbsuffix.'.cost_center_id ='] = $cost_center;
			}

			$this->DB1->where($conditions);

			if ($this->user_cost_centers && !$cost_center) {
				$sql_where = '(';
				foreach ($this->user_cost_centers as $key => $ucc) {
					if ($key == 0) {
						$sql_where .= 'entryitems'.$this->DB1->dbsuffix.'.cost_center_id = '.$ucc;
					} else {
						$sql_where .= ' OR entryitems'.$this->DB1->dbsuffix.'.cost_center_id = '.$ucc;
					}
				}
				$sql_where .= ')';
				$this->DB1->where($sql_where);
			}

			$this->DB1->order_by('entries'.$this->DB1->dbsuffix.'.date', 'asc');
			$entries_result = $this->DB1->get('entries'.$this->DB1->dbsuffix);
			if ($entries_result->num_rows() > 0) {
				$entries_result = $entries_result->row_array();
			}

			$condiciones['id_ledgers'] = $ledger_id;

			if ($company) {
				$condiciones['id_companies'] = $company;
			}

			if ($cost_center) {
				$condiciones['cost_center_id ='] = $cost_center;
			}

			$this->DB1->select('
								SUM(IF(op_balance_dc = "C", op_balance, 0)) AS c_amount,
								SUM(IF(op_balance_dc = "D", op_balance, 0)) AS d_amount
								')->where($condiciones);

			if ($this->user_cost_centers && !$cost_center) {
				$sql_where = '(';
				foreach ($this->user_cost_centers as $key => $ucc) {
					if ($key == 0) {
						$sql_where .= 'cost_center_id = '.$ucc;
					} else {
						$sql_where .= ' OR cost_center_id = '.$ucc;
					}
				}
				$sql_where .= ')';
				$this->DB1->where($sql_where);
			}

			$third_op_balance = $this->DB1->get('companies_opbalance'.$this->DB1->dbsuffix);
			if ($third_op_balance->num_rows() > 0) {
				$third_op_balance = $third_op_balance->row_array();
			}

			$c_amount_total = $this->functionscore->calculate((isset($entries_result['c_amount']) ? $entries_result['c_amount'] : 0), (isset($third_op_balance['c_amount']) ? $third_op_balance['c_amount'] : 0), '+');
			$d_amount_total = $this->functionscore->calculate((isset($entries_result['d_amount']) ? $entries_result['d_amount'] : 0), (isset($third_op_balance['d_amount']) ? $third_op_balance['d_amount'] : 0), '+');

			$op_total = $this->functionscore->calculate($d_amount_total, $c_amount_total, '-');

			if ($op_total < 0) {
				$op_total = abs($op_total);
				$op_total_dc = 'C';
			} else {
				$op_total = $op_total;
				$op_total_dc = 'D';
			}
		}
		return array('dc' => $op_total_dc, 'amount' => $op_total);
	}

	/**
	 * Calculate closing balance of specified ledger account for the given
	 * date range
	 *
	 * @param1 int ledger id
	 * @param2 date start date
	 * @param3 date end date
	 * @param4 skip op balance
	 * @param5 skip entries disapproved
	 * @param6 filter by entry type
	 * @param7 filter by third (companies)
	 * @return array D/C, Amount
	 */


	function closingBalance($id, $start_date = null, $end_date = null, $skip_op_balance = FALSE, $skip_entries_disapproved = FALSE, $entry_type = NULL, $third_id = NULL, $cost_center = NULL) {

		if (empty($id)) {
			show_404();
		}

		$condition_inner = "";

		if ($skip_entries_disapproved == TRUE) {
			$condition_inner .= ' AND entries'.$this->DB1->dbsuffix.'.state = 1';
		} else {
			$condition_inner .= ' AND entries'.$this->DB1->dbsuffix.'.state != 0 AND entries'.$this->DB1->dbsuffix.'.state != 3';
		}

		if ($entry_type != NULL) {
			$condition_inner .= ' AND entries'.$this->DB1->dbsuffix.'.entrytype_id = "'.$entry_type.'"';
		}

		$Entry  	= $this->load->model('Entry_model');
		$Entryitem 	= $this->load->model('EntryItem_model');


		/* Opening balance */


		// exit($third_id." - ".$cost_center);
		$op_total_data = $this->openingBalance($id, $start_date, $skip_op_balance, $skip_entries_disapproved, $entry_type, $third_id, $cost_center);

		$op_total = $op_total_data['amount'];
		$op_total_dc = $op_total_data['dc'];

		$dr_total = 0;
		$cr_total = 0;
		$dr_total_dc = 0;
		$cr_total_dc = 0;

		/* Debit total */
		$dr_conditions = array(
			'entryitems'.$this->DB1->dbsuffix.'.ledger_id' => $id,
			'entryitems'.$this->DB1->dbsuffix.'.dc' => 'D'
		);

		if ($third_id != NULL) {
			$dr_conditions['entryitems'.$this->DB1->dbsuffix.'.companies_id'] = $third_id;
		}

		if ($cost_center != NULL) {
			$dr_conditions['entryitems'.$this->DB1->dbsuffix.'.cost_center_id'] = $cost_center;
		}

		if (!is_null($start_date)) {
			$dr_conditions['entries'.$this->DB1->dbsuffix.'.date >='] = $start_date;
		}
		if (!is_null($end_date)) {
			$dr_conditions['entries'.$this->DB1->dbsuffix.'.date <='] = $end_date;
		}
		$this->DB1->where($dr_conditions);

		if ($this->user_cost_centers && !$cost_center) {
			$sql_where = '(';
			foreach ($this->user_cost_centers as $key => $ucc) {
				if ($key == 0) {
					$sql_where .= 'entryitems'.$this->DB1->dbsuffix.'.cost_center_id = '.$ucc;
				} else {
					$sql_where .= ' OR entryitems'.$this->DB1->dbsuffix.'.cost_center_id = '.$ucc;
				}
			}
			$sql_where .= ')';
			$this->DB1->where($sql_where);
		}
		
		$this->DB1->select('SUM(COALESCE('.$this->DB1->dbprefix('entryitems'.$this->DB1->dbsuffix).'.amount, 0)) as total');
		$this->DB1->join('entries'.$this->DB1->dbsuffix, 'entries'.$this->DB1->dbsuffix.'.id = entryitems'.$this->DB1->dbsuffix.'.entry_id'.$condition_inner, 'inner');
		$total = $this->DB1->get('entryitems'.$this->DB1->dbsuffix)->row_array();

		if (empty($total['total'])) {
			$dr_total = 0;
		} else {
			$dr_total = $total['total'];
		}

		/* Credit total */
		$cr_conditions = array(
			'entryitems'.$this->DB1->dbsuffix.'.ledger_id' => $id,
			'entryitems'.$this->DB1->dbsuffix.'.dc' => 'C'
		);

		if ($third_id != NULL) {
			$cr_conditions['entryitems'.$this->DB1->dbsuffix.'.companies_id'] = $third_id;
		}


		if (!is_null($start_date)) {
			$cr_conditions['entries'.$this->DB1->dbsuffix.'.date >='] = $start_date;
		}
		if (!is_null($end_date)) {
			$cr_conditions['entries'.$this->DB1->dbsuffix.'.date <='] = $end_date;
		}
		
		if ($cost_center != NULL) {
			$cr_conditions['entryitems'.$this->DB1->dbsuffix.'.cost_center_id'] = $cost_center;
		}
		$this->DB1->where($cr_conditions);

		if ($this->user_cost_centers && !$cost_center) {
			$sql_where = '(';
			foreach ($this->user_cost_centers as $key => $ucc) {
				if ($key == 0) {
					$sql_where .= 'entryitems'.$this->DB1->dbsuffix.'.cost_center_id = '.$ucc;
				} else {
					$sql_where .= ' OR entryitems'.$this->DB1->dbsuffix.'.cost_center_id = '.$ucc;
				}
			}
			$sql_where .= ')';
			$this->DB1->where($sql_where);
		}

		$this->DB1->select('SUM(COALESCE('.$this->DB1->dbprefix('entryitems'.$this->DB1->dbsuffix).'.amount, 0)) as total');
		$this->DB1->join('entries'.$this->DB1->dbsuffix, 'entries'.$this->DB1->dbsuffix.'.id = entryitems'.$this->DB1->dbsuffix.'.entry_id'.$condition_inner, 'inner');
		$total = $this->DB1->get('entryitems'.$this->DB1->dbsuffix)->row_array();

		if (empty($total['total'])) {
			$cr_total = 0;
		} else {
			$cr_total = $total['total'];
		}
		/* Add opening balance */
		if ($op_total_dc == 'D') {
			$dr_total_dc = $this->functionscore->calculate($op_total, $dr_total, '+');
			$cr_total_dc = $cr_total;
		} else {
			$dr_total_dc = $dr_total;
			$cr_total_dc = $this->functionscore->calculate($op_total, $cr_total, '+');
		}

		/* $this->calculate and update closing balance */
		$cl = 0;
		$cl_dc = '';
		if ($this->functionscore->calculate($dr_total_dc, $cr_total_dc, '>')) {
			$cl = $this->functionscore->calculate($dr_total_dc, $cr_total_dc, '-');
			$cl_dc = 'D';
		} else if ($this->functionscore->calculate($cr_total_dc, $dr_total_dc, '==')) {
			$cl = 0;
			$cl_dc = $op_total_dc;
		} else {
			$cl = $this->functionscore->calculate($cr_total_dc, $dr_total_dc, '-');
			$cl_dc = 'C';
		}

		return array('dc' => $cl_dc, 'amount' => $cl, 'dr_total' => $dr_total, 'cr_total' => $cr_total);
	}
	/* Return ledger name from id */
	public function getName($id) {
		$ledger = $this->DB1->where('id', $id);
		$ledger = $this->DB1->get('ledgers'.$this->DB1->dbsuffix)->row_array();
		if ($ledger) {
			return $this->functionscore->toCodeWithName($ledger['code'],$ledger['name']);
		} else {
			return('ERROR');
		}
	}


	/* Calculate difference in opening balance */
	public function getOpeningDiff() {
		$total_op = 0;
		$ledgers =  $this->DB1->get('ledgers'.$this->DB1->dbsuffix)->result_array();
		foreach ($ledgers as $row => $ledger)
		{
			if ($ledger['op_balance_dc'] == 'D')
			{
				$total_op = $this->functionscore->calculate($total_op, $ledger['op_balance'], '+');
			} else {
				$total_op = $this->functionscore->calculate($total_op, $ledger['op_balance'], '-');
			}
		}

		/* Dr is more ==> $total_op >= 0 ==> balancing figure is Cr */
		if ($this->functionscore->calculate($total_op, 0, '>=')) {
			return array('opdiff_balance_dc' => 'C', 'opdiff_balance' => $total_op);
		} else {
			return array('opdiff_balance_dc' => 'D', 'opdiff_balance' => $this->functionscore->calculate($total_op, 0, 'n'));
		}
	}


	/**
	 * Calculate reconciliation pending of specified ledger account for the given
	 * date range
	 *
	 * @param1 int ledger id
	 * @param2 date start date
	 * @param3 date end date
	 * @return array Debit_Amount, Credit_Amount
	 */
	function reconciliationPending($id, $start_date = null, $end_date = null) {

		$dr_total = 0;
		$cr_total = 0;

		/* Debit total */
		$dr_conditions = array(
			'entryitems'.$this->DB1->dbsuffix.'.ledger_id' => $id,
			'entryitems'.$this->DB1->dbsuffix.'.dc' => 'D',
			'entryitems'.$this->DB1->dbsuffix.'.reconciliation_date' => null
		);
		if (!is_null($start_date)) {
			$dr_conditions['entries'.$this->DB1->dbsuffix.'.date >='] = $start_date;
		}
		if (!is_null($end_date)) {
			$dr_conditions['entries'.$this->DB1->dbsuffix.'.date <='] = $end_date;
		}

		$this->DB1->where($dr_conditions);
		$this->DB1->select('SUM(COALESCE('.$this->DB1->dbprefix('entryitems'.$this->DB1->dbsuffix).'.amount, 0)) as total');
		$this->DB1->join('entries'.$this->DB1->dbsuffix, 'entries'.$this->DB1->dbsuffix.'.id = entryitems'.$this->DB1->dbsuffix.'.entry_id', 'left');
		$total = $this->DB1->get('entryitems'.$this->DB1->dbsuffix)->row_array();

		if (empty($total['total'])) {
			$dr_total = 0;
		} else {
			$dr_total = $total['total'];
		}

		/* Credit total */
		$cr_conditions = array(
			'entryitems'.$this->DB1->dbsuffix.'.ledger_id' => $id,
			'entryitems'.$this->DB1->dbsuffix.'.dc' => 'C',
			'entryitems'.$this->DB1->dbsuffix.'.reconciliation_date' => null
		);

		if (!is_null($start_date)) {
			$cr_conditions['entries'.$this->DB1->dbsuffix.'.date >='] = $start_date;
		}
		if (!is_null($end_date)) {
			$cr_conditions['entries'.$this->DB1->dbsuffix.'.date <='] = $end_date;
		}

		$this->DB1->where($cr_conditions);
		$this->DB1->select('SUM(COALESCE('.$this->DB1->dbprefix('entryitems'.$this->DB1->dbsuffix).'.amount, 0)) as total');
		$this->DB1->join('entries'.$this->DB1->dbsuffix, 'entries'.$this->DB1->dbsuffix.'.id = entryitems'.$this->DB1->dbsuffix.'.entry_id', 'left');
		$total = $this->DB1->get('entryitems'.$this->DB1->dbsuffix)->row_array();

		if (empty($total['total'])) {
			$cr_total = 0;
		} else {
			$cr_total = $total['total'];
		}

		return array('dr_total' => $dr_total, 'cr_total' => $cr_total);

	}


	/**
 * Calculate opening balance of specified third given
 * date range
 *
 * @param1 int ledger id
 * @param2 date start date
 * @param3 skip op balance
 * @param4 skip entries disapproved
 * @param5 filter by entry type
 * @param6 filter by third (companies)
 * @return array D/C, Amount
 */
	function openingBalanceThird($third_id, $start_date = null, $skip_entries_disapproved = FALSE, $entry_type = NULL, $ledger_id = NULL, $startcode = NULL, $endcode = NULL, $cost_center = NULL) {

		$condicion = '0';

		if (!$start_date) {
			$start_date = $this->mAccountSettings->fy_start;
		}

		$this->DB1->select('
					SUM(
						IF(
							'.$this->DB1->dbprefix.'entryitems'.$this->DB1->dbsuffix.'.dc = "C"
							, IF(
								 '.$this->DB1->dbprefix.'entries'.$this->DB1->dbsuffix.'.date < "'.$start_date.'",
								 '.$this->DB1->dbprefix.'entryitems'.$this->DB1->dbsuffix.'.amount,
								 '.$condicion.'
								 )
							, 0
						   )
						) AS c_amount,
					SUM(
						IF(
							'.$this->DB1->dbprefix.'entryitems'.$this->DB1->dbsuffix.'.dc = "D"
							, IF(
								 '.$this->DB1->dbprefix.'entries'.$this->DB1->dbsuffix.'.date < "'.$start_date.'",
								 '.$this->DB1->dbprefix.'entryitems'.$this->DB1->dbsuffix.'.amount,
								 '.$condicion.'
								 )
							, 0
						   )
						) AS d_amount,
					')
		  ->join('entryitems'.$this->DB1->dbsuffix, 'entryitems'.$this->DB1->dbsuffix.'.entry_id = entries'.$this->DB1->dbsuffix.'.id')
		  ->join('ledgers'.$this->DB1->dbsuffix, 'ledgers'.$this->DB1->dbsuffix.'.id = entryitems'.$this->DB1->dbsuffix.'.ledger_id', 'inner');

		$conditions = [];
		if ($third_id) {
			$conditions['entryitems'.$this->DB1->dbsuffix.'.companies_id'] = $third_id;
		}
		if ($ledger_id) {
			$conditions['entryitems'.$this->DB1->dbsuffix.'.ledger_id'] = $ledger_id;
		}
		if ($start_date) {
			// $conditions['entries'.$this->DB1->dbsuffix.'.date <'] = $start_date;
		}
		if ($skip_entries_disapproved) {
			$conditions['entries'.$this->DB1->dbsuffix.'.state'] = 1;
		} else {
			$conditions['entries'.$this->DB1->dbsuffix.'.state !='] = 0;
		}
		if ($entry_type) {
			$conditions['entries'.$this->DB1->dbsuffix.'.entrytype_id ='] = $entry_type;
		}
		if ($cost_center) {
			$conditions['entryitems'.$this->DB1->dbsuffix.'.cost_center_id ='] = $cost_center;
		}

		// if ($startcode) {
		// 	$conditions['ledgers'.$this->DB1->dbsuffix.'.code >='] = $startcode;
		// }

		// if ($endcode) {
		// 	$conditions['ledgers'.$this->DB1->dbsuffix.'.code <='] = $endcode;
		// }

		$this->DB1->where($conditions);
		$this->DB1->order_by('entries'.$this->DB1->dbsuffix.'.date', 'asc');
		$entries_result = $this->DB1->get('entries'.$this->DB1->dbsuffix);
		if ($entries_result->num_rows() > 0) {
			$entries_result = $entries_result->row_array();
		}

		$condiciones['companies_opbalance'.$this->DB1->dbsuffix.'.id_companies'] = $third_id;

		if ($ledger_id) {
			$condiciones['companies_opbalance'.$this->DB1->dbsuffix.'.id_ledgers'] = $ledger_id;
		}

		if ($cost_center) {
			$condiciones['companies_opbalance'.$this->DB1->dbsuffix.'.cost_center_id ='] = $cost_center;
		}

		if ($startcode) {
			$condiciones['ledgers'.$this->DB1->dbsuffix.'.code >='] = $startcode;
		}

		if ($endcode) {
			$condiciones['ledgers'.$this->DB1->dbsuffix.'.code <='] = $endcode;
		}

		$third_op_balance = $this->DB1->select('
							SUM(IF('.$this->DB1->dbprefix.'companies_opbalance'.$this->DB1->dbsuffix.'.op_balance_dc = "C", '.$this->DB1->dbprefix.'companies_opbalance'.$this->DB1->dbsuffix.'.op_balance, 0)) AS c_amount,
							SUM(IF('.$this->DB1->dbprefix.'companies_opbalance'.$this->DB1->dbsuffix.'.op_balance_dc = "D", '.$this->DB1->dbprefix.'companies_opbalance'.$this->DB1->dbsuffix.'.op_balance, 0)) AS d_amount
							')
				  ->join('ledgers'.$this->DB1->dbsuffix, 'ledgers'.$this->DB1->dbsuffix.'.id = companies_opbalance'.$this->DB1->dbsuffix.'.id_ledgers')
				  ->join('companies', 'companies.id = companies_opbalance'.$this->DB1->dbsuffix.'.id_companies')
				  ->where($condiciones)
				  ->get('companies_opbalance'.$this->DB1->dbsuffix);
		if ($third_op_balance->num_rows() > 0) {
			$third_op_balance = $third_op_balance->row_array();
		}

		$c_amount_total = $this->functionscore->calculate((isset($entries_result['c_amount']) ? $entries_result['c_amount'] : 0), (isset($third_op_balance['c_amount']) ? $third_op_balance['c_amount'] : 0), '+');
		$d_amount_total = $this->functionscore->calculate((isset($entries_result['d_amount']) ? $entries_result['d_amount'] : 0), (isset($third_op_balance['d_amount']) ? $third_op_balance['d_amount'] : 0), '+');

		$op_total = $this->functionscore->calculate($d_amount_total, $c_amount_total, '-');

		if ($op_total < 0) {
			$op_total = abs($op_total);
			$op_total_dc = 'C';
		} else {
			$op_total = $op_total;
			$op_total_dc = 'D';
		}

		return array('dc' => $op_total_dc, 'amount' => $op_total);

	}

	public function getAllLedgers(){
		$ledgers_data = $this->DB1->order_by('CAST('.$this->DB1->dbprefix.'ledgers'.$this->DB1->dbsuffix.'.code AS CHAR) ASC')
								  ->get('ledgers'.$this->DB1->dbsuffix);
		if ($ledgers_data->num_rows() > 0) {
			foreach (($ledgers_data->result()) as $row) {
				$ledgers[] = $row;
			}
			return $ledgers;
		} else {
			return false;
		}
	}

}