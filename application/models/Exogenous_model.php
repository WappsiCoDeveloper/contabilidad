<?php
Class Exogenous_model extends CI_Model {

    public function get_formats($id)
    {
        $this->DB1->select('id');
        $this->DB1->select('code');
        $this->DB1->select('version');
        $this->DB1->select('name');
        $this->DB1->select('description');
        $this->DB1->select('min_amount');
        $this->DB1->select('use_tercero');
        $this->DB1->from('exogenous_format'.$this->DB1->dbsuffix);
        if($id != '') $this->DB1->where('code', $id);
        $consulta = $this->DB1->get();
        $resultado = $consulta->result_array();
        return $resultado ;
    }

    public function get_formats_content($id)
    {
        $this->DB1->select('exogenous_content'.$this->DB1->dbsuffix.'.id');
        $this->DB1->select('exogenous_content'.$this->DB1->dbsuffix.'.name');
        $this->DB1->select('size');
        $this->DB1->select('observation');
        $this->DB1->select('order_by');
        $this->DB1->select('attribute');
        $this->DB1->select('exogenous_attribute_type'.$this->DB1->dbsuffix.'.name as nameType');
        $this->DB1->from('exogenous_content'.$this->DB1->dbsuffix);
        $this->DB1->join('exogenous_format'.$this->DB1->dbsuffix, 'exogenous_format'.$this->DB1->dbsuffix.'.id = exogenous_content'.$this->DB1->dbsuffix.'.id_format');
        $this->DB1->join('exogenous_attribute_type'.$this->DB1->dbsuffix, 'exogenous_attribute_type'.$this->DB1->dbsuffix.'.id = exogenous_content'.$this->DB1->dbsuffix.'.id_type');
        $this->DB1->where('code', $id);
        $this->DB1->order_by('exogenous_content'.$this->DB1->dbsuffix.'.order_by');
        $consulta = $this->DB1->get();
        $resultado = $consulta->result_array();
        return $resultado ;
    }

    public function get_attribute_type()
    {
        $this->DB1->select('id');
        $this->DB1->select('name');
        $this->DB1->select('type');
        $this->DB1->select('length');
        $this->DB1->from('exogenous_attribute_type'.$this->DB1->dbsuffix);
        $consulta = $this->DB1->get();
        $resultado = $consulta->result_array();
        return $resultado ;
    }

}
?>
