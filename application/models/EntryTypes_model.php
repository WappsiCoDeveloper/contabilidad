<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class EntryTypes_model extends CI_Model {

	public function __construct()
    {
        parent::__construct();
    }

	public function get_entrytypes()
	{
		$this->DB1->select("id, UPPER(name) AS name");
		$this->DB1->from("entrytypes".$this->DB1->dbsuffix);
		$this->DB1->order_by("name");
		$response = $this->DB1->get();

		return $response->result();
	}

}

/* End of file EntryTypes_model.php */
/* Location: ./application/models/EntryTypes_model.php */