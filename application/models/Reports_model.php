<?php
Class Reports_model extends CI_Model {
	
	public function __construct()
    {
        parent::__construct();
    }

	/**
	 * Show the entry ledger details
	 */
	public function getTotalPeriodical($id) {
		$q = $this->DB1
			->select('(if(op_balance_dc = "C", cr_total, dr_total)) as total, date')
			->join('ledgers'.$this->DB1->dbsuffix, 'entryitems'.$this->DB1->dbsuffix.'.ledger_id=ledgers'.$this->DB1->dbsuffix.'.id', 'left')
			->join('groups'.$this->DB1->dbsuffix, 'ledgers'.$this->DB1->dbsuffix.'.group_id=groups'.$this->DB1->dbsuffix.'.id', 'left')
			->join('entries'.$this->DB1->dbsuffix, 'entries'.$this->DB1->dbsuffix.'.id=entryitems'.$this->DB1->dbsuffix.'.entry_id', 'left')
			->where('GetAncestry('.$this->DB1->dbprefix('groups'.$this->DB1->dbsuffix).'.id, \''.$this->DB1->dbprefix('groups'.$this->DB1->dbsuffix).'\') = ', $id)
			->group_by('entries'.$this->DB1->dbsuffix.'.id')
			->where("MONTH(".$this->DB1->dbprefix('entries'.$this->DB1->dbsuffix).".date) = MONTH(CURRENT_DATE()) AND YEAR(".$this->DB1->dbprefix('entries'.$this->DB1->dbsuffix).".date) = YEAR(CURRENT_DATE())", NULL, FALSE)
			->from('entryitems'.$this->DB1->dbsuffix)
			->get();

			$number = array();
	        for ($i = 1; $i <= 31; ++$i) {
	            $number[$i] = 0;
	        }

			if ($q->num_rows() > 0) {
				$data = $q->result_array();
				$dated = array();
				foreach ($data as $row) {
					if(array_key_exists($row['date'], $dated)){
				        $dated[$row['date']]['total']	+= $row['total'];
				        $dated[$row['date']]['date']	= $row['date'];
				    } else {
				        $dated[$row['date']]  = $row;
				    }
				}
				foreach ($dated as $row) {
					$id = @date('j', strtotime($row['date']));
		            $number[$id] = $number[$id] + @$row['total'];
				}
			}


			return array_values($number);
	}


	public function getEntriesTA($view_type, $startcode, $endcode, $company, $startdate, $enddate, $skip_entries_disapproved, $entrytype, $cost_center){

		$this->DB1->select('
					entryitems'.$this->DB1->dbsuffix.'.id,
					entryitems'.$this->DB1->dbsuffix.'.entry_id,
					entryitems'.$this->DB1->dbsuffix.'.ledger_id,
					entryitems'.$this->DB1->dbsuffix.'.amount,
					UPPER(dc) AS dc,
					entryitems'.$this->DB1->dbsuffix.'.reconciliation_date,
					entryitems'.$this->DB1->dbsuffix.'.narration,
					entryitems'.$this->DB1->dbsuffix.'.base,
					entryitems'.$this->DB1->dbsuffix.'.companies_id,
					entryitems'.$this->DB1->dbsuffix.'.cost_center_id,
					entryitems'.$this->DB1->dbsuffix.'.registration_date,
					entries'.$this->DB1->dbsuffix.'.date as entry_date,
					entries'.$this->DB1->dbsuffix.'.id as entry_id,
					entries'.$this->DB1->dbsuffix.'.entrytype_id,
					entries'.$this->DB1->dbsuffix.'.number as entry_number,
					entries'.$this->DB1->dbsuffix.'.state,
					ledgers'.$this->DB1->dbsuffix.'.code as ledger_code,
					ledgers'.$this->DB1->dbsuffix.'.name as ledger_name,
					companies.name as companies_name,
					companies.vat_no as companies_vat_no,
					companies.id as entryitem_company_id
					')
		  ->join('entryitems'.$this->DB1->dbsuffix, 'entryitems'.$this->DB1->dbsuffix.'.entry_id = entries'.$this->DB1->dbsuffix.'.id')
		  ->join('companies', 'companies.id = entryitems'.$this->DB1->dbsuffix.'.companies_id')
		  ->join('ledgers'.$this->DB1->dbsuffix, 'ledgers'.$this->DB1->dbsuffix.'.id = entryitems'.$this->DB1->dbsuffix.'.ledger_id', 'inner');

		$conditions = [];
		if ($startcode) {
			$conditions['ledgers'.$this->DB1->dbsuffix.'.code >='] = $startcode;
		}
		if ($endcode) {
			$conditions['ledgers'.$this->DB1->dbsuffix.'.code <='] = $endcode;
		}
		if ($company) {
			$conditions['entryitems'.$this->DB1->dbsuffix.'.companies_id'] = $company;
		}
		if ($startdate) {
			$conditions['entries'.$this->DB1->dbsuffix.'.date >='] = $startdate;
		}
		if ($enddate) {
			$conditions['entries'.$this->DB1->dbsuffix.'.date <='] = $enddate;
		}
		if ($skip_entries_disapproved) {
			$conditions['entries'.$this->DB1->dbsuffix.'.state'] = 1;
		} else {
			$conditions['entries'.$this->DB1->dbsuffix.'.state !='] = 0;
		}
		if ($entrytype) {
			$conditions['entries'.$this->DB1->dbsuffix.'.entrytype_id ='] = $entrytype;
		}
		if ($cost_center) {
			$conditions['entryitems'.$this->DB1->dbsuffix.'.cost_center_id ='] = $cost_center;
		}

		$this->DB1->where($conditions);

		if ($this->user_cost_centers && !$cost_center) {
			$sql_where = '(';
			foreach ($this->user_cost_centers as $key => $ucc) {
				if ($key == 0) {
					$sql_where .= 'entryitems'.$this->DB1->dbsuffix.'.cost_center_id = '.$ucc;
				} else {
					$sql_where .= ' OR entryitems'.$this->DB1->dbsuffix.'.cost_center_id = '.$ucc;
				}
			}
			$sql_where .= ')';
			$this->DB1->where($sql_where);
		}

		if ($view_type == 1) {
			$this->DB1->order_by('companies.vat_no', 'asc');
			$this->DB1->order_by('ledgers'.$this->DB1->dbsuffix.'.code', 'asc');
		} else if ($view_type == 2) {
			$this->DB1->order_by('ledgers'.$this->DB1->dbsuffix.'.code', 'asc');
			$this->DB1->order_by('companies.vat_no', 'asc');
		}
		$this->DB1->order_by('entries'.$this->DB1->dbsuffix.'.date', 'asc');
		$entries_result = $this->DB1->get('entries'.$this->DB1->dbsuffix);
		$entries = [];
		if ($entries_result->num_rows() > 0) {
			foreach (($entries_result->result_array()) as $entry) {
				$entries[] = $entry;
			}
			return $entries;
		}

		return false;
	}

	public function getEntries($startcode, $endcode, $startdate, $enddate, $skip_entries_disapproved, $entrytype, $cost_center){

		$this->DB1->select('
					entryitems'.$this->DB1->dbsuffix.'.id,
					entryitems'.$this->DB1->dbsuffix.'.entry_id,
					entryitems'.$this->DB1->dbsuffix.'.ledger_id,
					entryitems'.$this->DB1->dbsuffix.'.amount,
					UPPER(dc) AS dc,
					entryitems'.$this->DB1->dbsuffix.'.reconciliation_date,
					entryitems'.$this->DB1->dbsuffix.'.narration,
					entryitems'.$this->DB1->dbsuffix.'.base,
					entryitems'.$this->DB1->dbsuffix.'.companies_id,
					entryitems'.$this->DB1->dbsuffix.'.cost_center_id,
					entryitems'.$this->DB1->dbsuffix.'.registration_date,
					entries'.$this->DB1->dbsuffix.'.date as entry_date,
					entries'.$this->DB1->dbsuffix.'.id as entry_id,
					entries'.$this->DB1->dbsuffix.'.entrytype_id,
					entries'.$this->DB1->dbsuffix.'.number as entry_number,
					entries'.$this->DB1->dbsuffix.'.state,
					ledgers'.$this->DB1->dbsuffix.'.code as ledger_code,
					ledgers'.$this->DB1->dbsuffix.'.name as ledger_name
					')
		  ->join('entryitems'.$this->DB1->dbsuffix, 'entryitems'.$this->DB1->dbsuffix.'.entry_id = entries'.$this->DB1->dbsuffix.'.id')
		  ->join('ledgers'.$this->DB1->dbsuffix, 'ledgers'.$this->DB1->dbsuffix.'.id = entryitems'.$this->DB1->dbsuffix.'.ledger_id', 'inner');

		$conditions = [];
		if ($startcode) {
			$conditions['ledgers'.$this->DB1->dbsuffix.'.code >='] = $startcode;
		}
		if ($endcode) {
			$conditions['ledgers'.$this->DB1->dbsuffix.'.code <='] = $endcode;
		}
		if ($startdate) {
			$conditions['entries'.$this->DB1->dbsuffix.'.date >='] = $startdate;
		}
		if ($enddate) {
			$conditions['entries'.$this->DB1->dbsuffix.'.date <='] = $enddate;
		}
		if ($skip_entries_disapproved) {
			$conditions['entries'.$this->DB1->dbsuffix.'.state'] = 1;
		} else {
			$conditions['entries'.$this->DB1->dbsuffix.'.state !='] = 0;
		}
		if ($entrytype) {
			$conditions['entries'.$this->DB1->dbsuffix.'.entrytype_id'] = $entrytype;
		}
		if ($cost_center) {
			$conditions['entryitems'.$this->DB1->dbsuffix.'.cost_center_id'] = $cost_center;
		}

		$this->DB1->where($conditions);

		if ($this->user_cost_centers && !$cost_center) {
			$sql_where = '(';
			foreach ($this->user_cost_centers as $key => $ucc) {
				if ($key == 0) {
					$sql_where .= 'entryitems'.$this->DB1->dbsuffix.'.cost_center_id = '.$ucc;
				} else {
					$sql_where .= ' OR entryitems'.$this->DB1->dbsuffix.'.cost_center_id = '.$ucc;
				}
			}
			$sql_where .= ')';
			$this->DB1->where($sql_where);
		}

		$this->DB1->order_by('ledgers'.$this->DB1->dbsuffix.'.code', 'asc');
		$this->DB1->order_by('entries'.$this->DB1->dbsuffix.'.date', 'asc');
		$this->DB1->order_by('entries'.$this->DB1->dbsuffix.'.id', 'asc');
		$entries_result = $this->DB1->get('entries'.$this->DB1->dbsuffix);
		$entries = [];
		if ($entries_result->num_rows() > 0) {
			foreach (($entries_result->result_array()) as $entry) {
				$entries[] = $entry;
			}
			return $entries;
		}

		return false;
	}

	public function getThirdOPBalance($third_id = NULL, $for_close_year = false){
		//OBTENEMOS LOS TERCEROS QUE TENGAN SALDO Y LOS ALMACENAMOS
		$this->DB1->select('companies.*')
		 	  ->join('companies', 'companies_opbalance'.$this->DB1->dbsuffix.'.id_companies = companies.id', 'inner');
		if ($third_id) {
			$this->DB1->where('companies.id', $third_id);
		}
		if ($for_close_year) {
			$this->DB1->where('companies.initial_accounting_balance_transferred', 0);
		}
		$this->DB1->group_by('companies_opbalance'.$this->DB1->dbsuffix.'.id_companies');
		$this->DB1->order_by('companies.company ASC');
		$companies_result = $this->DB1->get('companies_opbalance'.$this->DB1->dbsuffix);
		$companies = [];
		if ($companies_result->num_rows() > 0) {
			foreach (($companies_result->result_array()) as $companies_data) {
				$companies[$companies_data['id']] = $companies_data;
			}
		}
		//OBTENEMOS TERCEROS QUE ESTÉN INVOLUCRADOS EN NOTAS CONTABLES Y LOS ALMACENAMOS
		$this->DB1->select('companies.*')
			 	  ->join('companies', 'entryitems'.$this->DB1->dbsuffix.'.companies_id = companies.id', 'inner');
		if ($third_id) {
			$this->DB1->where('companies.id', $third_id);
		}
		if ($for_close_year) {
			$this->DB1->where('companies.initial_accounting_balance_transferred', 0);
		}
		$this->DB1->group_by('entryitems'.$this->DB1->dbsuffix.'.companies_id');
		$this->DB1->order_by('companies.company ASC');
 	    $companies_result = $this->DB1->get('entryitems'.$this->DB1->dbsuffix);
		if ($companies_result->num_rows() > 0) {
			foreach (($companies_result->result_array()) as $companies_data) {
				if (!isset($companies[$companies_data['id']])) {
					$companies[$companies_data['id']] = $companies_data;
				}
			}
		}

		$companies = $this->order_array($companies, 'name', 'ASC', 'id');

		if (count($companies) > 0) {
			return $companies;
		} else {
			return false;
		}
	}

	public function getThirdsLedgerOPBalance($ledger_id = NULL, $third_id = NULL, $cost_center = NULL){

		$this->DB1->select('companies.*')
		 	  ->join('companies', 'companies_opbalance'.$this->DB1->dbsuffix.'.id_companies = companies.id', 'inner');
		if ($ledger_id) {
			$this->DB1->where('companies_opbalance'.$this->DB1->dbsuffix.'.id_ledgers', $ledger_id);
		}
		if ($cost_center) {
			$this->DB1->where('companies_opbalance'.$this->DB1->dbsuffix.'.cost_center_id', $cost_center);
		}

		if ($this->user_cost_centers && !$cost_center) {
			$sql_where = '(';
			foreach ($this->user_cost_centers as $key => $ucc) {
				if ($key == 0) {
					$sql_where .= 'companies_opbalance'.$this->DB1->dbsuffix.'.cost_center_id = '.$ucc;
				} else {
					$sql_where .= ' OR companies_opbalance'.$this->DB1->dbsuffix.'.cost_center_id = '.$ucc;
				}
			}
			$sql_where .= ')';
			$this->DB1->where($sql_where);
		}

		$this->DB1->group_by('companies_opbalance'.$this->DB1->dbsuffix.'.id_companies');
		$companies_result = $this->DB1->get('companies_opbalance'.$this->DB1->dbsuffix);

		$companies = [];

		if ($companies_result->num_rows() > 0) {
			foreach (($companies_result->result_array()) as $companies_data) {
				$companies[$companies_data['id']] = $companies_data;
			}
		}


		//OBTENEMOS TERCEROS QUE ESTÉN INVOLUCRADOS EN NOTAS CONTABLES Y LOS ALMACENAMOS
		$this->DB1->select('companies.*')
			 	  ->join('companies', 'entryitems'.$this->DB1->dbsuffix.'.companies_id = companies.id', 'inner');
		if ($third_id) {
			$this->DB1->where('companies.id', $third_id);
		}
		$this->DB1->group_by('entryitems'.$this->DB1->dbsuffix.'.companies_id');
		$this->DB1->order_by('companies.company ASC');
 	    $companies_result = $this->DB1->get('entryitems'.$this->DB1->dbsuffix);
		if ($companies_result->num_rows() > 0) {
			foreach (($companies_result->result_array()) as $companies_data) {
				if (!isset($companies[$companies_data['id']])) {
					$companies[$companies_data['id']] = $companies_data;
				}
			}
		}
		
		if (count($companies) > 0 && $third_id == NULL) {
			return $companies;
		} else {
			return false;
		}
	}

	public function getLedgerOPBalance($startcode = NULL, $endcode = NULL, $company = NULL){
		$this->DB1->select($this->DB1->dbprefix.'ledgers'.$this->DB1->dbsuffix.'.*')
		 	  	  ->join($this->DB1->dbprefix.'companies_opbalance'.$this->DB1->dbsuffix, $this->DB1->dbprefix.'ledgers'.$this->DB1->dbsuffix.'.id = '.$this->DB1->dbprefix.'companies_opbalance'.$this->DB1->dbsuffix.'.id_ledgers', 'inner');
		$this->DB1->group_by('companies_opbalance'.$this->DB1->dbsuffix.'.id_ledgers');
		if ($startcode) {
			$this->DB1->where('ledgers'.$this->DB1->dbsuffix.'.code >=', $startcode);
		}
		if ($endcode) {
			$this->DB1->where('ledgers'.$this->DB1->dbsuffix.'.code <=', $endcode);
		}
		if ($company) {
			$this->DB1->where('companies_opbalance'.$this->DB1->dbsuffix.'.id_companies', $company);
		}
		$this->DB1->order_by('ledgers'.$this->DB1->dbsuffix.'.code ASC');
		$ledgers_result = $this->DB1->get('ledgers'.$this->DB1->dbsuffix);
		$ledgers = [];
		if ($ledgers_result->num_rows() > 0) {
			foreach (($ledgers_result->result_array()) as $ledgers_data) {
				$ledgers[$ledgers_data['id']] = $ledgers_data;
			}
		}
		//OBTENEMOS AUXILIARES QUE ESTÉN INVOLUCRADAS EN NOTAS CONTABLES Y LOS ALMACENAMOS
		$this->DB1->select($this->DB1->dbprefix.'ledgers'.$this->DB1->dbsuffix.'.*')
			 	  ->join('ledgers'.$this->DB1->dbsuffix, 'ledgers'.$this->DB1->dbsuffix.'.id = entryitems'.$this->DB1->dbsuffix.'.ledger_id', 'inner');
		if ($startcode) {
			$this->DB1->where('ledgers'.$this->DB1->dbsuffix.'.code >=', $startcode);
		}
		if ($endcode) {
			$this->DB1->where('ledgers'.$this->DB1->dbsuffix.'.code <=', $endcode);
		}
		if ($company) {
			$this->DB1->where('entryitems'.$this->DB1->dbsuffix.'.companies_id', $company);
		}
		$this->DB1->group_by('entryitems'.$this->DB1->dbsuffix.'.ledger_id');
 	    $ledgers_result = $this->DB1->get('entryitems'.$this->DB1->dbsuffix);
		if ($ledgers_result->num_rows() > 0) {
			foreach (($ledgers_result->result_array()) as $ledgers_data) {
				if (!isset($ledgers[$ledgers_data['id']])) {
					$ledgers[$ledgers_data['id']] = $ledgers_data;
				}
			}
		}
		if (count($ledgers) > 0) {
			return $ledgers;
		} else {
			return false;
		}
	}

	public function getLedgersThirdOPBalance($startcode = NULL, $endcode = NULL, $company = NULL, $cost_center = NULL){
		$this->DB1->select($this->DB1->dbprefix.'ledgers'.$this->DB1->dbsuffix.'.*')
		 	  	  ->join($this->DB1->dbprefix.'companies_opbalance'.$this->DB1->dbsuffix, $this->DB1->dbprefix.'ledgers'.$this->DB1->dbsuffix.'.id = '.$this->DB1->dbprefix.'companies_opbalance'.$this->DB1->dbsuffix.'.id_ledgers', 'inner');
		$this->DB1->group_by('companies_opbalance'.$this->DB1->dbsuffix.'.id_ledgers');
		if ($company) {
			$this->DB1->where('companies_opbalance'.$this->DB1->dbsuffix.'.id_companies', $company);
		}
		if ($cost_center) {
			$this->DB1->where('companies_opbalance'.$this->DB1->dbsuffix.'.cost_center_id', $cost_center);
		}

		if ($this->user_cost_centers && !$cost_center) {
			$sql_where = '(';
			foreach ($this->user_cost_centers as $key => $ucc) {
				if ($key == 0) {
					$sql_where .= 'companies_opbalance'.$this->DB1->dbsuffix.'.cost_center_id = '.$ucc;
				} else {
					$sql_where .= ' OR companies_opbalance'.$this->DB1->dbsuffix.'.cost_center_id = '.$ucc;
				}
			}
			$sql_where .= ')';
			$this->DB1->where($sql_where);
		}

		$this->DB1->order_by('ledgers'.$this->DB1->dbsuffix.'.code ASC');
		$ledgers_result = $this->DB1->get('ledgers'.$this->DB1->dbsuffix);
		$ledgers = [];
		if ($ledgers_result->num_rows() > 0) {
			foreach (($ledgers_result->result_array()) as $ledgers_data) {
				$ledgers[$ledgers_data['id']] = $ledgers_data;
			}
		}
		if (count($ledgers) > 0 && $startcode == NULL && $endcode == NULL) {
			return $ledgers;
		} else {
			return false;
		}
	}

	public function order_array($array, $column, $order, $index_column){
		$order_array = [];
		$final_array = [];
		foreach ($array as $row_key => $row_data) {
			if (!isset($order_array[$row_data[$column]])) {
				$order_array[$row_data[$column]] = $row_data;
			} else {
				$order_array[$row_data[$column]."_".rand(1000, 5000)] = $row_data;
			}
		}
		if ($order == 'ASC') {
			ksort($order_array);
		} else if ($order == 'DESC') {
			krsort($order_array);
		}
		foreach ($order_array as $row_key => $row_data) {
			$final_array[$row_data[$index_column]] = $row_data;
		}
		return $final_array;

	}

	public function get_third_party_options()
	{
		$this->DB1->select("companies.id AS 'id_compania', UPPER(companies.company) AS 'nombre_compania'");
		$this->DB1->from("withholdings");
		$this->DB1->join("entryitems".$this->DB1->dbsuffix." entry_items", "entry_items.ledger_id = withholdings.account_id", "inner");
		$this->DB1->join("ledgers".$this->DB1->dbsuffix." ledgers", "ledgers.id = entry_items.ledger_id", "inner");
		$this->DB1->join("companies companies", "companies.id = entry_items.companies_id", "left");
		$this->DB1->where("withholdings.affects", "P");
		$this->DB1->group_by("companies.vat_no");
		$this->DB1->group_by("companies.name");
		$this->DB1->order_by("companies.company");
		$response = $this->DB1->get();
		return $response->result();
	}

	public function get_third_party($search_parameters = NULL)
	{
		$this->DB1->select("UPPER(companies.company) AS 'compania', UPPER(companies.name) AS 'nombre', companies.vat_no AS 'NIT_CC', companies.id AS 'id_tercero'");
		$this->DB1->from("withholdings");
		$this->DB1->join("entryitems".$this->DB1->dbsuffix." entry_items", "entry_items.ledger_id = withholdings.account_id", "inner");
		$this->DB1->join("entries".$this->DB1->dbsuffix." entries", "entries.id = entry_items.entry_id", "inner");
		$this->DB1->join("entrytypes".$this->DB1->dbsuffix." entrytypes", "entrytypes.id = entries.entrytype_id", "inner");
		$this->DB1->join("ledgers".$this->DB1->dbsuffix." ledgers", "ledgers.id = entry_items.ledger_id", "inner");
		$this->DB1->join("companies companies", "companies.id = entry_items.companies_id", "left");
		$this->DB1->where("withholdings.affects", "P");
		if (!empty($search_parameters->note_type_excluded)) {
			$this->DB1->where("entrytypes.id != ", $search_parameters->note_type_excluded);
		}
		if (! empty($search_parameters->initial_date)) {
			$this->DB1->where("entries.date >= ", $search_parameters->initial_date);
		}
		if (!empty($search_parameters->final_date)) {
			$this->DB1->where("entries.date <= ", $search_parameters->final_date);
		}
		if (!empty($search_parameters->third_party)) {
			$this->DB1->where("companies.id", $search_parameters->third_party);
		}
		if (!empty($search_parameters->cost_center)) {
			$this->DB1->where("entry_items.cost_center_id", $search_parameters->cost_center);
		}

		if ($this->user_cost_centers && empty($search_parameters->cost_center)) {
			$sql_where = '(';
			foreach ($this->user_cost_centers as $key => $ucc) {
				if ($key == 0) {
					$sql_where .= 'entry_items.cost_center_id = '.$ucc;
				} else {
					$sql_where .= ' OR entry_items.cost_center_id = '.$ucc;
				}
			}
			$sql_where .= ')';
			$this->DB1->where($sql_where);
		}
		$this->DB1->group_by("companies.company");
		$this->DB1->group_by("companies.name");
		$this->DB1->group_by("companies.vat_no");
		$this->DB1->order_by("companies.company");
		$response = $this->DB1->get();
		return $response->result();
	}

	public function get_retention_data($company_id, $note_type_excluded = NULL, $initial_date = NULL, $final_date = NULL, $cost_center = NULL)
	{
		$this->DB1->select("
			".$this->DB1->dbprefix('withholdings').".type AS 'tipo_retencion',
			ledgers.code AS 'codigo_cuenta',
			TRIM(BOTH FROM UPPER(ledgers.name)) AS 'nombre_cuenta',
			TRIM(BOTH FROM UPPER(companies.company)) AS 'compania',
			TRIM(BOTH FROM UPPER(companies.name)) AS 'nombre',
			companies.vat_no AS 'NIT_CC',
			companies.id AS 'id_tercero',
			entries.number AS 'numero_documento',
			entries.date AS 'fecha_documento',
			entrytypes.label AS 'tipo_documento',
			entryitems.narration AS 'concepto',
			entryitems.dc,
			IF (entryitems.dc = 'D', (entryitems.base * -1), entryitems.base) AS 'base',
			IF (entryitems.dc = 'D', (entryitems.amount * -1), entryitems.amount) AS 'retencion'");
		$this->DB1->from("withholdings");
		$this->DB1->join("entryitems".$this->DB1->dbsuffix." entryitems", "entryitems.ledger_id = withholdings.account_id", "inner");
		$this->DB1->join("entries".$this->DB1->dbsuffix." entries", "entries.id = entryitems.entry_id", "inner");
		$this->DB1->join("entrytypes".$this->DB1->dbsuffix." entrytypes", "entrytypes.id = entries.entrytype_id", "inner");
		$this->DB1->join("ledgers".$this->DB1->dbsuffix." ledgers", "ledgers.id = entryitems.ledger_id", "inner");
		$this->DB1->join("companies companies", "companies.id = entryitems.companies_id", "left");
		$this->DB1->where("withholdings.affects", "P");
		// $this->DB1->where("companies.vat_no IN (". $company_id .")");
		$this->DB1->where_in("companies.vat_no", $company_id);
		if (!empty($note_type_excluded)) {
			$this->DB1->where("entrytypes.id != ", $note_type_excluded);
		}
		if (!empty($initial_date)) {
			$this->DB1->where("entries.date >= ", $initial_date);
		}
		if (!empty($final_date)) {
			$this->DB1->where("entries.date <= ", $final_date);
		}
		if (!empty($cost_center)) {
			$this->DB1->where("entryitems.cost_center_id <= ", $cost_center);
		}

		if ($this->user_cost_centers && !$cost_center) {
			$sql_where = '(';
			foreach ($this->user_cost_centers as $key => $ucc) {
				if ($key == 0) {
					$sql_where .= 'entryitems.cost_center_id = '.$ucc;
				} else {
					$sql_where .= ' OR entryitems.cost_center_id = '.$ucc;
				}
			}
			$sql_where .= ')';
			$this->DB1->where($sql_where);
		}

		$this->DB1->order_by("companies.company");
		$this->DB1->order_by("withholdings.type");
		$this->DB1->order_by("ledgers.code");
		$this->DB1->order_by("entries.date");
		$response = $this->DB1->get();
		return $response->result();
	}
}