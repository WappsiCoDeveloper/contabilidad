<?php
Class Settings_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getSettings()
    {
        $q = $this->db->get('settings');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function get_settings_account()
    {
        $q = $this->DB1->get("settings");
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function updateSettings($data)
    {
        $q = $this->db->update('settings', $data);
      	if ($q) {
      		return TRUE;
      	}else
            return FALSE;
    }

    public function getAccounts($numrows = false)
    {
        $q = $this->db->get('accounts');

        if ($numrows == true && $q->num_rows() > 0) {
            return true;
        }elseif($q->num_rows() > 0){
            foreach (($q->result()) as $row) {
                $new_config['hostname'] = $this->db->hostname;
                $new_config['username'] = $this->db->username;
                $new_config['password'] = $this->db->password;
                $new_config['database'] = $row->db_database;
                $new_config['dbdriver'] = $this->db->dbdriver;
                $new_config['dbprefix'] = $row->db_prefix;
                $new_config['dbsuffix'] = $row->db_suffix;
                $new_config['db_debug'] = TRUE;
                $new_config['cache_on'] = FALSE;
                $new_config['cachedir'] = "";
                $new_config['schema']   = $this->input->post('db_schema');
                $new_config['port']     = $this->db->port;
                $new_config['char_set'] = "utf8";
                $new_config['dbcollat'] = "utf8_general_ci";
                if ($this->check_database($new_config)) {
                    $DB2 = $this->load->database($new_config, TRUE);
                    $cc = $DB2->get('cost_centers'.$DB2->dbsuffix);
                    if ($cc->num_rows() > 0) {
                        foreach (($cc->result()) as $cc_row) {
                            $row->cost_centers[] = $cc_row;
                        }
                    }
                    $DB2->close();
                }
                $data[] = $row;
            }
            return $data;
        }else{
            return false;
        }
    }

    public function getAccountByID($id)
    {
        $this->db->where('id', $id);
        $q = $this->db->get('accounts');
        return $q->row();
    }

    public function getAccountsOfLoggedInUser()
    {
        $user = $this->session->userdata('user');
        if ($user->all_accounts) {
            $q = $this->db->get('accounts');
            if ($q->num_rows() > 0) {
                foreach (($q->result()) as $row) {
                    $data[] = $row;
                }
                return $data;
            }
        }else{
            $accessible = json_decode($user->accounts);
            $a = 0;
            if ($accessible) {
                while ($a < count($accessible)) {
                    if ($a == 0) {
                        $this->db->where('id', $accessible[$a]);
                    }
                    $this->db->or_where('id', $accessible[$a]);
                    $a++;
                }
            }
            
            $q = $this->db->get('accounts');
            if ($q->num_rows() > 0) {
                foreach (($q->result()) as $row) {
                    $data[] = $row;
                }
                return $data;
            }
            return false;
        }
    }

    public function add_log($message, $level)
    {
        if ($this->mSettings->enable_logging !== 1) {
                return false;
        }
        $now = new DateTime();

        $logentry = array(
                'level'         => $level,
                'date'          => $now->format('Y-m-d H:i:s'),
                'host_ip'       => $_SERVER['REMOTE_ADDR'],
                'user_id'       => $this->session->userdata('user')->id,
                'url'           => current_url(),
                'user_agent'    => $_SERVER['HTTP_USER_AGENT'],
                'message'       => $message,
        );
        $this->DB1->insert('logs'.$this->DB1->dbsuffix, $logentry);
        return true;
    }

    public function getGroupPermissions($id)
    {
        $q = $this->db->get_where('permissions', array('group_id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getTagNameByID($id)
    {
        $q = $this->DB1->get_where('tags'.$this->DB1->dbsuffix, array('id'=>$id));
        if ($q->num_rows() > 0) {
            return $q->row()->title;
        }
        return 'No Tag';
    }

    public function isModulary()
    {
        if (isset($_SESSION['active_account_config'])) {
            $DB3 = $this->load->database($_SESSION['active_account_config'], TRUE);
            $modulary = $DB3->get('settings'.$DB3->dbsuffix);
            if ($modulary->num_rows() > 0) {
                $modulary = $modulary->row_array();
                unset($DB3);
                return $modulary['modulary'];
            } else {
                return false;
                unset($DB3);
            }
        } else {
            return false;
        }
    }

    public function getCategories()
    {
        $categories_result = $this->DB1->where('parent_id', NULL)->or_where('parent_id', 0)->get('categories');
        if ($categories_result->num_rows() > 0) {
            foreach (($categories_result->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function getTaxes()
    {
        $tax_result = $this->DB1->get('tax_rates');
        if ($tax_result->num_rows() > 0) {
            foreach (($tax_result->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function getTaxes2()
    {
        $tax_result = $this->DB1->get('tax_secondary');
        if ($tax_result->num_rows() > 0) {
            foreach (($tax_result->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function get_cost_centers()
    {
        $q = $this->DB1->get('cost_centers'.$this->DB1->dbsuffix);
        if ($q->num_rows() > 0) {
            foreach (($q->result_array()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function validate_invalid_entry_items_data()
    {


        $response = [
                        'msg' => '',
                        'num_validation' => '0',
                        'error' => false,
                    ];


        if ($this->mAccountSettings->cost_center == 1) {
            $cost_centers = $this->settings_model->get_cost_centers();

            if ($cost_centers != false) {
                $x = $this->DB1->select('entryitems'.$this->DB1->dbsuffix.'.id, cost_centers'.$this->DB1->dbsuffix.'.id, cost_centers'.$this->DB1->dbsuffix.'.name')
                                ->join('cost_centers'.$this->DB1->dbsuffix, 'entryitems'.$this->DB1->dbsuffix.'.cost_center_id = cost_centers'.$this->DB1->dbsuffix.'.id', 'left')
                                ->where('cost_centers'.$this->DB1->dbsuffix.'.id IS NULL')
                                ->get('entryitems'.$this->DB1->dbsuffix);
                if ($x->num_rows() > 0) {
                        $response =     [
                                        'msg' => lang('entry_items_without_cost_centers'),
                                        'num_validation' => '1',
                                        'error' => true,
                                    ];
                }
            }
        }


        $y = $this->DB1->select('entryitems'.$this->DB1->dbsuffix.'.id, ledgers'.$this->DB1->dbsuffix.'.id, ledgers'.$this->DB1->dbsuffix.'.name')
                        ->join('ledgers'.$this->DB1->dbsuffix, 'entryitems'.$this->DB1->dbsuffix.'.ledger_id = ledgers'.$this->DB1->dbsuffix.'.id', 'left')
                        ->where('ledgers'.$this->DB1->dbsuffix.'.id IS NULL')
                        ->get('entryitems'.$this->DB1->dbsuffix);

        if ($y->num_rows() > 0) {
            $response =     [
                            'msg' => lang('entry_items_without_ledgers'),
                            'num_validation' => '2',
                            'error' => true,
                        ];
        }

        $z = $this->DB1->select('entryitems'.$this->DB1->dbsuffix.'.id, companies.id, companies.name')
                        ->join('companies', 'entryitems'.$this->DB1->dbsuffix.'.companies_id = companies.id', 'left')
                        ->where('companies.id IS NULL')
                        ->get('entryitems'.$this->DB1->dbsuffix);

        if ($z->num_rows() > 0) {
            $response =     [
                            'msg' => lang('entry_items_without_companies'),
                            'num_validation' => '3',
                            'error' => true,
                        ];
        }

        if ($this->mAccountSettings->cost_center == 1 && $cost_centers != false) {

            $b = $this->DB1->select('companies_opbalance'.$this->DB1->dbsuffix.'.id, cost_centers'.$this->DB1->dbsuffix.'.id, cost_centers'.$this->DB1->dbsuffix.'.name')
                            ->join('cost_centers'.$this->DB1->dbsuffix, 'companies_opbalance'.$this->DB1->dbsuffix.'.cost_center_id = cost_centers'.$this->DB1->dbsuffix.'.id', 'left')
                            ->where('cost_centers'.$this->DB1->dbsuffix.'.id IS NULL')
                            ->get('companies_opbalance'.$this->DB1->dbsuffix);
            if ($b->num_rows() > 0) {
                    $response =     [
                                    'msg' => lang('companies_opbalance_without_cost_center'),
                                    'num_validation' => '4',
                                    'error' => true,
                                ];
            }

        }

        $c = $this->DB1->select('companies_opbalance'.$this->DB1->dbsuffix.'.id, companies.id, companies.name')
                        ->join('companies', 'companies_opbalance'.$this->DB1->dbsuffix.'.id_companies = companies.id', 'left')
                        ->where('companies.id IS NULL')
                        ->get('companies_opbalance'.$this->DB1->dbsuffix);
        if ($c->num_rows() > 0) {
                $response =     [
                                'msg' => lang('companies_opbalance_without_companies'),
                                'num_validation' => '5',
                                'error' => true,
                            ];
        }

        $d = $this->DB1->select('companies_opbalance'.$this->DB1->dbsuffix.'.id, ledgers'.$this->DB1->dbsuffix.'.id, ledgers'.$this->DB1->dbsuffix.'.name')
                        ->join('ledgers'.$this->DB1->dbsuffix, 'companies_opbalance'.$this->DB1->dbsuffix.'.id_ledgers = ledgers'.$this->DB1->dbsuffix.'.id', 'left')
                        ->where('ledgers'.$this->DB1->dbsuffix.'.id IS NULL')
                        ->get('companies_opbalance'.$this->DB1->dbsuffix);
        if ($d->num_rows() > 0) {
                $response =     [
                                'msg' => lang('companies_opbalance_without_cost_center'),
                                'num_validation' => '6',
                                'error' => true,
                            ];
        }

        $a = $this->DB1->get_where('entries'.$this->DB1->dbsuffix, ['state' => 2]);

        if ($a->num_rows() > 0) {
                $response =     [
                                'msg' => lang('exist_entries_disapproved'),
                                'num_validation' => '7',
                                'error' => true,
                            ];
        }

        $a = $this->DB1->get_where('entries'.$this->DB1->dbsuffix, ['date <' => $this->mAccountSettings->fy_start]);

        if ($a->num_rows() > 0) {
                $response =     [
                                'msg' => lang('exist_entries_with_invalid_date'),
                                'num_validation' => '8',
                                'error' => true,
                            ];
        }

        $a = $this->DB1->query("SELECT
                                    EI.entry_id,
                                    SUM(IF(EI.dc = 'C', EI.amount, 0)) as c_amount,
                                    SUM(IF(EI.dc = 'D', EI.amount, 0)) as d_amount
                                FROM ".$this->DB1->dbprefix."entryitems".$this->DB1->dbsuffix." EI
                                    INNER JOIN ".$this->DB1->dbprefix."entries".$this->DB1->dbsuffix." E ON E.id = EI.entry_id
                                    GROUP BY entry_id
                                HAVING c_amount != d_amount;");

        if ($a->num_rows() > 0) {
                $response =     [
                                'msg' => lang('exist_mismatched_entries'),
                                'num_validation' => '9',
                                'error' => true,
                            ];
        }

        return $response;
    }

    public function get_close_balance($cost_center = false){
        if ($cost_center) {
            $q = $this->DB1->query('
                                    SELECT 
                                        id_companies,
                                        id_ledgers,
                                        ledger_code,
                                        cost_center_id,
                                        SUM(c_amount) AS c_amount,
                                        SUM(d_amount) AS d_amount,
                                        IF((SUM(d_amount) - SUM(c_amount)) < 0,
                                            ((SUM(d_amount) - SUM(c_amount)) * - 1),
                                            (SUM(d_amount) - SUM(c_amount))) AS op_amount,
                                        IF((SUM(d_amount) - SUM(c_amount)) < 0, "C", "D") op_amount_dc
                                    FROM
                                        (SELECT 
                                            1 as type,
                                            EI.companies_id AS id_companies,
                                                EI.ledger_id AS id_ledgers,
                                                L.code AS ledger_code,
                                                EI.cost_center_id AS cost_center_id,
                                                SUM(IF(EI.dc = "C", EI.amount, 0)) AS c_amount,
                                                SUM(IF(EI.dc = "D", EI.amount, 0)) AS d_amount
                                        FROM
                                            '.$this->DB1->dbprefix.'entryitems'.$this->DB1->dbsuffix.' EI
                                        INNER JOIN '.$this->DB1->dbprefix.'entries'.$this->DB1->dbsuffix.' E ON E.id = EI.entry_id
                                        INNER JOIN '.$this->DB1->dbprefix.'cost_centers'.$this->DB1->dbsuffix.' CC ON CC.id = EI.cost_center_id
                                        INNER JOIN '.$this->DB1->dbprefix.'ledgers'.$this->DB1->dbsuffix.' L ON L.id = EI.ledger_id
                                        WHERE
                                            E.state = 1
                                            AND E.date >= "'.$this->mAccountSettings->fy_start.'" AND E.date <= "'.$this->mAccountSettings->fy_end.'"
                                        GROUP BY EI.cost_center_id , EI.companies_id , EI.ledger_id 

                                        UNION 

                                        SELECT 
                                            2 as type,
                                            COP.id_companies,
                                                COP.id_ledgers,
                                                L.code AS ledger_code,
                                                COP.cost_center_id,
                                                SUM(IF(COP.op_balance_dc = "C", COP.op_balance, 0)) AS c_amount,
                                                SUM(IF(COP.op_balance_dc = "D", COP.op_balance, 0)) AS d_amount
                                        FROM
                                            '.$this->DB1->dbprefix.'companies_opbalance'.$this->DB1->dbsuffix.' COP
                                        INNER JOIN '.$this->DB1->dbprefix.'ledgers'.$this->DB1->dbsuffix.' L ON L.id = COP.id_ledgers
                                        GROUP BY cost_center_id , id_companies , id_ledgers) AS result_table
                                            INNER JOIN
                                        '.$this->DB1->dbprefix.'companies C ON C.id = result_table.id_companies
                                    GROUP BY cost_center_id , id_companies , id_ledgers;');
            if ($q->num_rows() > 0) {
                foreach (($q->result_array()) as $row) {
                    $data[] = $row;
                }
                return $data;
            }
            return false;
        } else {
            $q = $this->DB1->query('
                                    SELECT 
                                        id_companies,
                                        id_ledgers,
                                        ledger_code,
                                        SUM(c_amount) AS c_amount,
                                        SUM(d_amount) AS d_amount,
                                        IF((SUM(d_amount) - SUM(c_amount)) < 0,
                                            ((SUM(d_amount) - SUM(c_amount)) * - 1),
                                            (SUM(d_amount) - SUM(c_amount))) AS op_amount,
                                        IF((SUM(d_amount) - SUM(c_amount)) < 0, "C", "D") op_amount_dc
                                    FROM
                                        (SELECT 
                                            1 as type,
                                            EI.companies_id AS id_companies,
                                            EI.ledger_id AS id_ledgers,
                                            L.code AS ledger_code,
                                            SUM(IF(EI.dc = "C", EI.amount, 0)) AS c_amount,
                                            SUM(IF(EI.dc = "D", EI.amount, 0)) AS d_amount
                                        FROM
                                            '.$this->DB1->dbprefix.'entryitems'.$this->DB1->dbsuffix.' EI
                                        INNER JOIN '.$this->DB1->dbprefix.'entries'.$this->DB1->dbsuffix.' E ON E.id = EI.entry_id
                                        INNER JOIN '.$this->DB1->dbprefix.'ledgers'.$this->DB1->dbsuffix.' L ON L.id = EI.ledger_id
                                        WHERE
                                            E.state = 1
                                        GROUP BY EI.companies_id , EI.ledger_id 

                                        UNION 

                                        SELECT 
                                            2 as type,
                                            COP.id_companies,
                                            COP.id_ledgers,
                                            L.code AS ledger_code,
                                            SUM(IF(COP.op_balance_dc = "C", COP.op_balance, 0)) AS c_amount,
                                            SUM(IF(COP.op_balance_dc = "D", COP.op_balance, 0)) AS d_amount
                                        FROM
                                            '.$this->DB1->dbprefix.'companies_opbalance'.$this->DB1->dbsuffix.' COP
                                        INNER JOIN '.$this->DB1->dbprefix.'ledgers'.$this->DB1->dbsuffix.' L ON L.id = COP.id_ledgers
                                        GROUP BY id_companies , id_ledgers) AS result_table
                                            INNER JOIN
                                        '.$this->DB1->dbprefix.'companies C ON C.id = result_table.id_companies
                                    GROUP BY id_companies , id_ledgers;');
            if ($q->num_rows() > 0) {
                foreach (($q->result_array()) as $row) {
                    $data[] = $row;
                }
                return $data;
            }
            return false;
        }
    }

    public function check_database($config)
    {
        //  Check if using mysqli driver
        if( $config['dbdriver'] === 'mysqli' )
        {
            // initilize mysqli connection
            @$mysqli = new mysqli( $config['hostname'] , $config['username'] , $config['password'] , $config['database'] );
            // Check database connection
            if( !$mysqli->connect_error )
            {
                // if no connection errors are found close connection and return true
                @$mysqli->close();
                return true;
            }
        }
        // else return false
        return false;
    }
}
?>