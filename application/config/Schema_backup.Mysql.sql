DROP TABLE IF EXISTS `%_PREFIX_%accounts%_SUFIX_%`;

CREATE TABLE `%_PREFIX_%accounts%_SUFIX_%` (
  `id` int(10) UNSIGNED NOT NULL,
  `type` smallint(5) UNSIGNED NOT NULL DEFAULT '1' COMMENT '1 = Venta  2= Compra',
  `typemov` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `id_tax` int(11) NOT NULL DEFAULT '0',
  `ledger_id` bigint(18) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `%_PREFIX_%entries%_SUFIX_%`;

CREATE TABLE `%_PREFIX_%entries%_SUFIX_%` (
  `id` bigint(18) NOT NULL,
  `tag_id` bigint(18) DEFAULT NULL,
  `entrytype_id` bigint(18) NOT NULL,
  `number` bigint(18) DEFAULT NULL,
  `date` date NOT NULL,
  `dr_total` decimal(25,2) NOT NULL DEFAULT '0.00',
  `cr_total` decimal(25,2) NOT NULL DEFAULT '0.00',
  `notes` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `state` smallint(1) NOT NULL DEFAULT '2' COMMENT '1= APROBADO 0=ANULADO  2=DESAPROBADO',
  `companies_id` int(11) NOT NULL DEFAULT '1',
  `origin` smallint(1) NOT NULL DEFAULT '0' COMMENT '1=CONTABILIDAD  0=OTROS'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `%_PREFIX_%entryitems%_SUFIX_%`;

CREATE TABLE `%_PREFIX_%entryitems%_SUFIX_%` (
  `id` bigint(18) NOT NULL,
  `entry_id` bigint(18) NOT NULL,
  `ledger_id` bigint(18) NOT NULL,
  `amount` decimal(25,2) NOT NULL DEFAULT '0.00',
  `dc` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `reconciliation_date` date DEFAULT NULL,
  `narration` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `base` decimal(25,2) NOT NULL DEFAULT '0.00',
  `companies_id` int(11) NOT NULL DEFAULT '1',
  `cost_center_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `%_PREFIX_%entrytypes%_SUFIX_%`;

CREATE TABLE `%_PREFIX_%entrytypes%_SUFIX_%` (
  `id` bigint(18) NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `base_type` int(2) NOT NULL DEFAULT '0',
  `numbering` int(2) NOT NULL DEFAULT '1',
  `prefix` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `suffix` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `zero_padding` int(2) NOT NULL DEFAULT '0',
  `restriction_bankcash` int(2) NOT NULL DEFAULT '1',
  `origin` SMALLINT(1) NOT NULL DEFAULT '1' COMMENT '1 =Modulo contable   0= Otros Modulos'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `%_PREFIX_%groups%_SUFIX_%`;

CREATE TABLE `%_PREFIX_%groups%_SUFIX_%` (
  `id` bigint(18) NOT NULL,
  `parent_id` bigint(18) DEFAULT '0',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `affects_gross` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `%_PREFIX_%ledgers%_SUFIX_%`;

CREATE TABLE `%_PREFIX_%ledgers%_SUFIX_%` (
  `id` bigint(18) NOT NULL,
  `group_id` bigint(18) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `op_balance` decimal(25,2) NOT NULL DEFAULT '0.00',
  `op_balance_dc` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `type` int(2) NOT NULL DEFAULT '0',
  `reconciliation` int(1) NOT NULL DEFAULT '0',
  `notes` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `cost_center` int(1) NULL COMMENT '0. Activo, 1. Inactivo'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `%_PREFIX_%logs%_SUFIX_%`;

CREATE TABLE `%_PREFIX_%logs%_SUFIX_%` (
  `id` bigint(18) NOT NULL,
  `date` datetime NOT NULL,
  `level` int(1) NOT NULL,
  `host_ip` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_agent` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `message` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `%_PREFIX_%movement_type%_SUFIX_%`;

CREATE TABLE `%_PREFIX_%movement_type%_SUFIX_%` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_entrytypes` bigint(18) NOT NULL,
  `movement` varchar(45) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `ledger_id` bigint(18) NOT NULL,
  `offsetting_account` varchar(45) COLLATE utf8_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `%_PREFIX_%payment_methods%_SUFIX_%`;

CREATE TABLE `%_PREFIX_%payment_methods%_SUFIX_%` (
  `id` int(10) UNSIGNED NOT NULL,
  `type` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `entity` varchar(100) COLLATE utf8_unicode_ci DEFAULT '''''',
  `receipt_ledger_id` bigint(18) NOT NULL,
  `payment_ledger_id` bigint(18) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `%_PREFIX_%settings%_SUFIX_%`;

CREATE TABLE `%_PREFIX_%settings%_SUFIX_%` (
  `id` int(1) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fy_start` date NOT NULL,
  `fy_end` date NOT NULL,
  `currency_symbol` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `currency_format` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `decimal_places` int(2) NOT NULL DEFAULT '2',
  `date_format` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `timezone` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `manage_inventory` int(1) NOT NULL DEFAULT '0',
  `account_locked` int(1) NOT NULL DEFAULT '0',
  `email_use_default` int(1) NOT NULL DEFAULT '0',
  `email_protocol` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `email_host` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_port` int(5) NOT NULL,
  `email_tls` int(1) NOT NULL DEFAULT '0',
  `email_username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_from` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `print_paper_height` decimal(10,3) NOT NULL DEFAULT '0.000',
  `print_paper_width` decimal(10,3) NOT NULL DEFAULT '0.000',
  `print_margin_top` decimal(10,3) NOT NULL DEFAULT '0.000',
  `print_margin_bottom` decimal(10,3) NOT NULL DEFAULT '0.000',
  `print_margin_left` decimal(10,3) NOT NULL DEFAULT '0.000',
  `print_margin_right` decimal(10,3) NOT NULL DEFAULT '0.000',
  `print_orientation` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `print_page_format` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `database_version` int(10) DEFAULT NULL,
  `settings` blob,
  `logo` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type_account` SMALLINT(1) NULL DEFAULT '1' COMMENT '1 => Cuentas 2649    2 =>NIFF',
  `modulary` SMALLINT(1) NULL COMMENT '1 => pos-contabilidad  0= no conexion',
  `thirds_relationed` SMALLINT(1) NULL DEFAULT '0' COMMENT '1 = Terceros relacionados a auxiliares, 2 = Terceros sin relacionar'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `%_PREFIX_%tags%_SUFIX_%`;

CREATE TABLE `%_PREFIX_%tags%_SUFIX_%` (
  `id` bigint(18) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `color` char(6) COLLATE utf8_unicode_ci NOT NULL,
  `background` char(6) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `%_PREFIX_%withdraw%_SUFIX_%`;

CREATE TABLE `%_PREFIX_%withdraw%_SUFIX_%` (
  `id` int(10) UNSIGNED NOT NULL,
  `description` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `min_base` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `percentage` decimal(6,3) NOT NULL DEFAULT '0.000',
  `account` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `apply` varchar(1) COLLATE utf8_unicode_ci NOT NULL COMMENT 'S= subtotal I= iva',
  `affects` varchar(1) COLLATE utf8_unicode_ci NOT NULL COMMENT 'C = Credito D=debito',
  `type` varchar(10) COLLATE utf8_unicode_ci NOT NULL COMMENT 'RETENCION RETEIVA RETEICA OTROS'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `%_PREFIX_%withholdings%_SUFIX_%`;

CREATE TABLE `%_PREFIX_%withholdings%_SUFIX_%` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `description` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `min_base` int(10) unsigned NOT NULL DEFAULT '0',
  `percentage` decimal(7,5) NOT NULL DEFAULT '0.00000',
  `account_id` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `apply` varchar(2) COLLATE utf8_unicode_ci NOT NULL COMMENT 'ST = SubTotal, TX = Valor Iva, TO = Total.',
  `affects` varchar(1) COLLATE utf8_unicode_ci NOT NULL COMMENT 'S = Ventas, P = Compras',
  `type` varchar(10) COLLATE utf8_unicode_ci NOT NULL COMMENT 'FUENTE, IVA, ICA, OTRA',
  `code_fe` varchar(2) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Código empleado para facturación electrónica',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `%_PREFIX_%companies_opbalance%_SUFIX_%`;

CREATE TABLE `%_PREFIX_%companies_opbalance%_SUFIX_%` (
 `id` INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
 `id_companies` INTEGER(11) UNSIGNED NOT NULL,
 `id_ledgers` Bigint(18) UNSIGNED NOT NULL,
 `op_balance` DECIMAL(25,2) NOT NULL DEFAULT 0,
 `op_balance_dc` CHAR(1) NOT NULL,
 PRIMARY KEY (`id`)
)
ENGINE = InnoDB;

DROP TABLE IF EXISTS `%_PREFIX_%cost_centers%_SUFIX_%`;

CREATE TABLE `%_PREFIX_%cost_centers%_SUFIX_%` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(55) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(55) COLLATE utf8_unicode_ci NOT NULL,
  `company_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE `%_PREFIX_%accounts%_SUFIX_%`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_accounts_1` (`typemov`),
  ADD KEY `FK_new_accounts_2` (`id_tax`),
  ADD KEY `ledger_id` (`ledger_id`);

ALTER TABLE `%_PREFIX_%entries%_SUFIX_%`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique_id` (`id`),
  ADD KEY `id` (`id`),
  ADD KEY `tag_id` (`tag_id`),
  ADD KEY `entrytype_id` (`entrytype_id`),
  ADD KEY `companies_id` (`companies_id`);

ALTER TABLE `%_PREFIX_%entryitems%_SUFIX_%`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique_id` (`id`),
  ADD KEY `id` (`id`),
  ADD KEY `entry_id` (`entry_id`),
  ADD KEY `ledger_id` (`ledger_id`),
  ADD KEY `companies_id` (`companies_id`);

ALTER TABLE `%_PREFIX_%entrytypes%_SUFIX_%`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique_id` (`id`),
  ADD UNIQUE KEY `label` (`label`),
  ADD KEY `id` (`id`);

ALTER TABLE `%_PREFIX_%groups%_SUFIX_%`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique_id` (`id`),
  ADD UNIQUE KEY `code` (`code`),
  ADD KEY `id` (`id`),
  ADD KEY `parent_id` (`parent_id`);

ALTER TABLE `%_PREFIX_%ledgers%_SUFIX_%`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique_id` (`id`),
  ADD UNIQUE KEY `code` (`code`),
  ADD KEY `id` (`id`),
  ADD KEY `group_id` (`group_id`);

ALTER TABLE `%_PREFIX_%logs%_SUFIX_%`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique_id` (`id`),
  ADD KEY `id` (`id`);

ALTER TABLE `%_PREFIX_%movement_type%_SUFIX_%`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_new_movement_type_1` (`id_entrytypes`);

ALTER TABLE `%_PREFIX_%payment_methods%_SUFIX_%`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%settings%_SUFIX_%`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique_id` (`id`),
  ADD KEY `id` (`id`);


ALTER TABLE `%_PREFIX_%tags%_SUFIX_%`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique_id` (`id`),
  ADD UNIQUE KEY `title` (`title`),
  ADD KEY `id` (`id`);

ALTER TABLE `%_PREFIX_%withdraw%_SUFIX_%`
  ADD PRIMARY KEY (`id`);


ALTER TABLE `%_PREFIX_%accounts%_SUFIX_%`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

ALTER TABLE `%_PREFIX_%entries%_SUFIX_%`
  MODIFY `id` bigint(18) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

ALTER TABLE `%_PREFIX_%entryitems%_SUFIX_%`
  MODIFY `id` bigint(18) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

ALTER TABLE `%_PREFIX_%entrytypes%_SUFIX_%`
  MODIFY `id` bigint(18) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

ALTER TABLE `%_PREFIX_%groups%_SUFIX_%`
  MODIFY `id` bigint(18) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

ALTER TABLE `%_PREFIX_%ledgers%_SUFIX_%`
  MODIFY `id` bigint(18) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

ALTER TABLE `%_PREFIX_%logs%_SUFIX_%`
  MODIFY `id` bigint(18) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

ALTER TABLE `%_PREFIX_%movement_type%_SUFIX_%`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%payment_methods%_SUFIX_%`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

ALTER TABLE `%_PREFIX_%tags%_SUFIX_%`
  MODIFY `id` bigint(18) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%withdraw%_SUFIX_%`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%settings%_SUFIX_%`
  ADD COLUMN `nit` VARCHAR(100) NOT NULL AFTER `thirds_relationed`,
  ADD COLUMN `phone` VARCHAR(100) NOT NULL AFTER `nit`;

ALTER TABLE `%_PREFIX_%settings%_SUFIX_%` 
  ADD COLUMN `cost_center` INT(1) NULL DEFAULT '0' COMMENT '1. Si maneja, 2. No maneja.' AFTER `phone`;

ALTER TABLE `%_PREFIX_%settings%_SUFIX_%` 
  ADD COLUMN `account_parameter_method` INT(1) NULL DEFAULT '1' COMMENT '1. Iva, 2. Categorías' AFTER `cost_center`;

ALTER TABLE `%_PREFIX_%accounts%_SUFIX_%` 
  ADD COLUMN `id_category` INT(11) NULL DEFAULT NULL AFTER `id_tax`,
  CHANGE COLUMN `id_tax` `id_tax` INT(11) NULL DEFAULT NULL ;

ALTER TABLE `%_PREFIX_%companies_opbalance%_SUFIX_%` 
ADD COLUMN `cost_center_id` INT(11) NULL AFTER `op_balance_dc`;

ALTER TABLE `%_PREFIX_%settings%_SUFIX_%` 
ADD COLUMN `year_closed` INT(1) NULL COMMENT '1. Si, 0. No' AFTER `account_parameter_method`;

ALTER TABLE `%_PREFIX_%entrytypes%_SUFIX_%` 
ADD COLUMN `consecutive` INT(11) NULL AFTER `origin`;

ALTER TABLE `%_PREFIX_%settings%_SUFIX_%` 
ADD COLUMN `accountant_name` VARCHAR(60) NULL AFTER `year_closed`,
ADD COLUMN `fiscal_name` VARCHAR(60) NULL AFTER `accountant_name`,
ADD COLUMN `representant_name` VARCHAR(60) NULL AFTER `fiscal_name`;

ALTER TABLE `%_PREFIX_%movement_type%_SUFIX_%` 
ADD COLUMN `document_type_id` INT(11) NULL DEFAULT NULL AFTER `offsetting_account`;

ALTER TABLE `%_PREFIX_%settings%_SUFIX_%` 
ADD COLUMN `close_year_step` INT(11) NULL COMMENT '0. Ningún paso, 1. Año nuevo creado, 2. pasando saldos iniciales de terceros, 3. Saldos iniciales trasladados ' AFTER `representant_name`;