START TRANSACTION;

CREATE TABLE `%_PREFIX_%accounts%_SUFIX_%` (
  `id` int(10) UNSIGNED NOT NULL,
  `type` smallint(5) UNSIGNED NOT NULL DEFAULT 1 COMMENT '1 = Venta  2= Compra',
  `typemov` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `id_tax` int(11) DEFAULT NULL,
  `id_category` int(11) DEFAULT NULL,
  `ledger_id` bigint(18) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `%_PREFIX_%companies_opbalance%_SUFIX_%` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_companies` int(11) UNSIGNED NOT NULL,
  `id_ledgers` bigint(18) UNSIGNED NOT NULL,
  `op_balance` decimal(25,4) NOT NULL DEFAULT 0.00,
  `op_balance_dc` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `cost_center_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

CREATE TABLE `%_PREFIX_%cost_centers%_SUFIX_%` (
  `id` int(11) NOT NULL,
  `code` varchar(55) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(55) COLLATE utf8_unicode_ci NOT NULL,
  `company_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `%_PREFIX_%entries%_SUFIX_%` (
  `id` bigint(18) NOT NULL,
  `tag_id` bigint(18) DEFAULT NULL,
  `entrytype_id` bigint(18) NOT NULL,
  `number` varchar(18) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date` date NOT NULL,
  `dr_total` decimal(25,4) NOT NULL DEFAULT 0.0000,
  `cr_total` decimal(25,4) NOT NULL DEFAULT 0.0000,
  `notes` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `state` smallint(1) NOT NULL DEFAULT 2 COMMENT '1= APROBADO 0=ANULADO  2=DESAPROBADO',
  `companies_id` int(11) NOT NULL DEFAULT 1,
  `origin` smallint(1) NOT NULL DEFAULT 0 COMMENT '1=CONTABILIDAD  0=OTROS',
  `user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `%_PREFIX_%entryitems%_SUFIX_%` (
  `id` bigint(18) NOT NULL,
  `entry_id` bigint(18) NOT NULL,
  `ledger_id` bigint(18) NOT NULL,
  `amount` decimal(25,4) NOT NULL DEFAULT 0.0000,
  `dc` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `reconciliation_date` date DEFAULT NULL,
  `narration` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `base` decimal(25,2) NOT NULL DEFAULT 0.00,
  `companies_id` int(11) NOT NULL DEFAULT 1,
  `cost_center_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `%_PREFIX_%entrytypes%_SUFIX_%` (
  `id` bigint(18) NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `base_type` int(2) NOT NULL DEFAULT 0,
  `numbering` int(2) NOT NULL DEFAULT 1,
  `prefix` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `suffix` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `zero_padding` int(2) NOT NULL DEFAULT 0,
  `restriction_bankcash` int(2) NOT NULL DEFAULT 1,
  `origin` smallint(1) NOT NULL DEFAULT 1 COMMENT '1 =Modulo contable   0= Otros Modulos',
  `consecutive` bigint(11) DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `%_PREFIX_%groups%_SUFIX_%` (
  `id` bigint(18) NOT NULL,
  `parent_id` bigint(18) DEFAULT 0,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `affects_gross` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `%_PREFIX_%ledgers%_SUFIX_%` (
  `id` bigint(18) NOT NULL,
  `group_id` bigint(18) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `op_balance` decimal(25,4) NOT NULL DEFAULT 0.00,
  `op_balance_dc` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `type` int(2) NOT NULL DEFAULT 0,
  `reconciliation` int(1) NOT NULL DEFAULT 0,
  `notes` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `cost_center` int(1) DEFAULT NULL COMMENT '0. Activo, 1. Inactivo'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `%_PREFIX_%logs%_SUFIX_%` (
  `id` bigint(18) NOT NULL,
  `date` datetime NOT NULL,
  `level` int(1) NOT NULL,
  `host_ip` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_agent` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `message` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `%_PREFIX_%movement_type%_SUFIX_%` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_entrytypes` bigint(18) NOT NULL,
  `movement` varchar(45) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `ledger_id` bigint(18) NOT NULL,
  `offsetting_account` varchar(45) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `document_type_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `%_PREFIX_%payment_methods%_SUFIX_%` (
  `id` int(10) UNSIGNED NOT NULL,
  `type` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `entity` varchar(100) COLLATE utf8_unicode_ci DEFAULT '''''',
  `receipt_ledger_id` bigint(18) NOT NULL,
  `payment_ledger_id` bigint(18) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `%_PREFIX_%settings%_SUFIX_%` (
  `id` int(1) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fy_start` date NOT NULL,
  `fy_end` date NOT NULL,
  `currency_symbol` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `currency_format` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `decimal_places` int(2) NOT NULL DEFAULT 2,
  `date_format` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `timezone` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `manage_inventory` int(1) NOT NULL DEFAULT 0,
  `account_locked` int(1) NOT NULL DEFAULT 0,
  `email_use_default` int(1) NOT NULL DEFAULT 0,
  `email_protocol` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `email_host` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_port` int(5) NOT NULL,
  `email_tls` int(1) NOT NULL DEFAULT 0,
  `email_username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_from` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `print_paper_height` decimal(10,3) NOT NULL DEFAULT 0.000,
  `print_paper_width` decimal(10,3) NOT NULL DEFAULT 0.000,
  `print_margin_top` decimal(10,3) NOT NULL DEFAULT 0.000,
  `print_margin_bottom` decimal(10,3) NOT NULL DEFAULT 0.000,
  `print_margin_left` decimal(10,3) NOT NULL DEFAULT 0.000,
  `print_margin_right` decimal(10,3) NOT NULL DEFAULT 0.000,
  `print_orientation` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `print_page_format` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `database_version` int(10) DEFAULT NULL,
  `settings` blob DEFAULT NULL,
  `logo` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type_account` smallint(1) DEFAULT 1 COMMENT '1 => Cuentas 2649    2 =>NIFF',
  `modulary` smallint(1) DEFAULT NULL COMMENT '1 => pos-contabilidad  0= no conexion',
  `thirds_relationed` smallint(1) DEFAULT 0 COMMENT '1 = Terceros relacionados a auxiliares, 2 = Terceros sin relacionar',
  `nit` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `cost_center` int(1) DEFAULT 0 COMMENT '1. Si maneja, 2. No maneja.',
  `account_parameter_method` int(1) DEFAULT 1 COMMENT '1. Iva, 2. Categorías',
  `year_closed` int(1) DEFAULT NULL COMMENT '1. Si, 0. No',
  `accountant_name` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fiscal_name` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `representant_name` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `close_year_step` int(11) DEFAULT NULL COMMENT '0. Ningún paso, 1. Año nuevo creado, 2. pasando saldos iniciales de terceros, 3. Saldos iniciales trasladados '
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

CREATE TABLE `%_PREFIX_%tags%_SUFIX_%` (
  `id` bigint(18) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `color` char(6) COLLATE utf8_unicode_ci NOT NULL,
  `background` char(6) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `%_PREFIX_%withdraw%_SUFIX_%` (
  `id` int(10) UNSIGNED NOT NULL,
  `description` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `min_base` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `percentage` decimal(6,3) NOT NULL DEFAULT 0.000,
  `account` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `apply` varchar(1) COLLATE utf8_unicode_ci NOT NULL COMMENT 'S= subtotal I= iva',
  `affects` varchar(1) COLLATE utf8_unicode_ci NOT NULL COMMENT 'C = Credito D=debito',
  `type` varchar(10) COLLATE utf8_unicode_ci NOT NULL COMMENT 'RETENCION RETEIVA RETEICA OTROS'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE `%_PREFIX_%accounts%_SUFIX_%`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_accounts_1` (`typemov`),
  ADD KEY `FK_new_accounts_2` (`id_tax`),
  ADD KEY `ledger_id` (`ledger_id`);

ALTER TABLE `%_PREFIX_%companies_opbalance%_SUFIX_%`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%cost_centers%_SUFIX_%`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%entries%_SUFIX_%`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique_id` (`id`),
  ADD KEY `id` (`id`),
  ADD KEY `tag_id` (`tag_id`),
  ADD KEY `entrytype_id` (`entrytype_id`),
  ADD KEY `companies_id` (`companies_id`);

ALTER TABLE `%_PREFIX_%entryitems%_SUFIX_%`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique_id` (`id`),
  ADD KEY `id` (`id`),
  ADD KEY `entry_id` (`entry_id`),
  ADD KEY `ledger_id` (`ledger_id`),
  ADD KEY `companies_id` (`companies_id`);

ALTER TABLE `%_PREFIX_%entrytypes%_SUFIX_%`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique_id` (`id`),
  ADD UNIQUE KEY `label` (`label`),
  ADD KEY `id` (`id`);

ALTER TABLE `%_PREFIX_%groups%_SUFIX_%`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique_id` (`id`),
  ADD UNIQUE KEY `code` (`code`),
  ADD KEY `id` (`id`),
  ADD KEY `parent_id` (`parent_id`);

ALTER TABLE `%_PREFIX_%ledgers%_SUFIX_%`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique_id` (`id`),
  ADD UNIQUE KEY `code` (`code`),
  ADD KEY `id` (`id`),
  ADD KEY `group_id` (`group_id`);

ALTER TABLE `%_PREFIX_%logs%_SUFIX_%`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique_id` (`id`),
  ADD KEY `id` (`id`);

ALTER TABLE `%_PREFIX_%movement_type%_SUFIX_%`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_new_movement_type_1` (`id_entrytypes`);

ALTER TABLE `%_PREFIX_%payment_methods%_SUFIX_%`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%settings%_SUFIX_%`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique_id` (`id`),
  ADD KEY `id` (`id`);

ALTER TABLE `%_PREFIX_%tags%_SUFIX_%`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique_id` (`id`),
  ADD UNIQUE KEY `title` (`title`),
  ADD KEY `id` (`id`);

ALTER TABLE `%_PREFIX_%withdraw%_SUFIX_%`
  ADD PRIMARY KEY (`id`);


ALTER TABLE `%_PREFIX_%accounts%_SUFIX_%`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%companies_opbalance%_SUFIX_%`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%cost_centers%_SUFIX_%`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%entries%_SUFIX_%`
  MODIFY `id` bigint(18) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%entryitems%_SUFIX_%`
  MODIFY `id` bigint(18) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%entrytypes%_SUFIX_%`
  MODIFY `id` bigint(18) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%groups%_SUFIX_%`
  MODIFY `id` bigint(18) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%ledgers%_SUFIX_%`
  MODIFY `id` bigint(18) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%logs%_SUFIX_%`
  MODIFY `id` bigint(18) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%movement_type%_SUFIX_%`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%payment_methods%_SUFIX_%`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%tags%_SUFIX_%`
  MODIFY `id` bigint(18) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%withdraw%_SUFIX_%`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%entries%_SUFIX_%` 
ADD COLUMN `registration_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `user_id`;

ALTER TABLE `%_PREFIX_%entryitems%_SUFIX_%` 
ADD COLUMN `registration_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `cost_center_id`;

ALTER TABLE `%_PREFIX_%movement_type%_SUFIX_%`
DROP COLUMN `id_entrytypes`,
DROP INDEX `FK_new_movement_type_1`;

ALTER TABLE `%_PREFIX_%movement_type%_SUFIX_%`
CHANGE COLUMN `document_type_id` `document_type_id` INT(11) NOT NULL ;

COMMIT;