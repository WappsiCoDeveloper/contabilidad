<?php
/**
 * Class to store the entire account tree with the details
 */
class AccountList
{

  public function __construct()
  {
      $this->_ci =& get_instance();
      $this->_ci->load->model('ledger_model');
  }
  
  var $account_list = [];
  var $result_account_list = [];
  var $id = 0;
  var $name = '';
  var $code = '';

  var $g_parent_id = 0;   /* Group specific */
  var $g_affects_gross = 0; /* Group specific */
  var $l_group_id = 0;    /* Ledger specific */
  var $l_type = 0;    /* Ledger specific */
  var $l_reconciliation = 0;  /* Ledger specific */
  var $l_notes = '';    /* Ledger specific */

  var $op_total = 0;
  var $op_total_dc = 'D';
  var $dr_total = 0;
  var $cr_total = 0;
  var $cl_total = 0;
  var $cl_total_dc = 'D';

  var $children_groups = array();
  var $children_ledgers = array();

  var $counter = 0;

  var $only_opening = false;
  var $start_date = null;
  var $end_date = null;
  var $startcode = null;
  var $endcode = null;
  var $affects_gross = -1;

  var $Group = null;
  var $Ledger = null;
  var $skip_op_balance = FALSE; //Mostrar closing balance sin opening balance.
  var $skip_entries_disapproved = FALSE; //Omitir notas contables desaprobadas.
  var $cost_center = NULL;
  var $third_id = NULL;

/**
 * Initializer
 */
  function AccountList()
  {
    return;
  }

/**
 * Setup which group id to start from
 */
  function start($id = NULL, $parent = NULL) //se añade segunda variable $parent, que nos indica si se entra por búsqueda de grupo padre (clase) o no.
  {

    $sql_start_date = $this->start_date ? $this->start_date : $this->_ci->mAccountSettings->fy_start;
    $sql_end_date = $this->end_date ? $this->end_date : $this->_ci->mAccountSettings->fy_end;
    $sql_cost_center = $this->cost_center ? "AND `{$this->_ci->DB1->dbprefix('entryitems')}{$this->_ci->DB1->dbsuffix}`.cost_center_id = {$this->cost_center}" : "";
    $sql_cost_center_COP = $this->cost_center ? "WHERE `{$this->_ci->DB1->dbprefix('companies_opbalance')}{$this->_ci->DB1->dbsuffix}`.cost_center_id = {$this->cost_center}" : "";
    $sql_third = $this->third_id ? "AND `{$this->_ci->DB1->dbprefix('entryitems')}{$this->_ci->DB1->dbsuffix}`.companies_id = {$this->third_id}" : "";
    $sql_third_COP = $this->third_id ? "`{$this->_ci->DB1->dbprefix('companies_opbalance')}{$this->_ci->DB1->dbsuffix}`.id_companies = {$this->third_id}" : "";
    if ($sql_third_COP) {
        if ($sql_cost_center_COP) {
            $sql_third_COP = " AND ".$sql_third_COP;
        } else {
            $sql_third_COP = " WHERE ".$sql_third_COP;
        }
    }
        
    $sql_skip_disapproved = 
          $this->skip_entries_disapproved ? 
                "AND `{$this->_ci->DB1->dbprefix('entries')}{$this->_ci->DB1->dbsuffix}`.`state` = 1" : 
                "AND `{$this->_ci->DB1->dbprefix('entries')}{$this->_ci->DB1->dbsuffix}`.`state` != 0 AND `{$this->_ci->DB1->dbprefix('entries')}{$this->_ci->DB1->dbsuffix}`.`state` != 3";
    $sql_skip_op = $this->skip_op_balance ? "COALESCE(total_ledgers.total, 0)" : "COALESCE(total_ledgers.total, 0) + COALESCE(op_balance.op_total, 0)";

    $sql_op_entries =  !$this->skip_op_balance && $this->start_date ? "LEFT JOIN 
            (
                SELECT 
                    `{$this->_ci->DB1->dbprefix('entryitems')}{$this->_ci->DB1->dbsuffix}`.`ledger_id`,
                    SUM(COALESCE({$this->_ci->DB1->dbprefix('entryitems')}{$this->_ci->DB1->dbsuffix}.amount * (IF(`{$this->_ci->DB1->dbprefix('entryitems')}{$this->_ci->DB1->dbsuffix}`.`dc` = 'C', -1, 1)), 0)) AS total
                FROM
                    `{$this->_ci->DB1->dbprefix('entryitems')}{$this->_ci->DB1->dbsuffix}`
                        INNER JOIN
                    `{$this->_ci->DB1->dbprefix('entries')}{$this->_ci->DB1->dbsuffix}` ON `{$this->_ci->DB1->dbprefix('entries')}{$this->_ci->DB1->dbsuffix}`.`id` = `{$this->_ci->DB1->dbprefix('entryitems')}{$this->_ci->DB1->dbsuffix}`.`entry_id`
                        {$sql_skip_disapproved}
                WHERE
                        `{$this->_ci->DB1->dbprefix('entries')}{$this->_ci->DB1->dbsuffix}`.`date` < '{$sql_start_date}'
                        {$sql_cost_center}
                        {$sql_third}
                GROUP BY `{$this->_ci->DB1->dbprefix('entryitems')}{$this->_ci->DB1->dbsuffix}`.`ledger_id`
            ) AS total_ledgers_op ON total_ledgers_op.ledger_id = account_list.id AND account_list.ledger_group = 'L' " : "";
    $sql_op_entries_select = !$this->skip_op_balance && $this->start_date ? " + COALESCE(total_ledgers_op.total, 0)" : "";
    $sql_only_opening = !$this->skip_op_balance ? ($this->only_opening ? "COALESCE(op_balance.op_total, 0) {$sql_op_entries_select} " : "COALESCE(total_ledgers.total, 0) + COALESCE(op_balance.op_total, 0) {$sql_op_entries_select}") : "COALESCE(total_ledgers.total, 0)";

    $sql = "SELECT 
                CONCAT(parent_id, '_G') AS parent_id, 
                level, 
                ledger_group, 
                CONCAT(id, '_', ledger_group) AS id, 
                code, 
                name, 
                {$sql_only_opening} AS cl_total, 
                COALESCE(op_balance.op_total, 0) {$sql_op_entries_select} AS op_total,
                COALESCE(total_ledgers.dr_total, 0) AS dr_total,
                COALESCE(total_ledgers.cr_total, 0) AS cr_total
            FROM 
            (
                SELECT 
                    G1.parent_id, 
                    IF(G6.id IS NOT NULL, 6, 
                        IF(G5.id  IS NOT NULL, 5, 
                            IF(G4.id  IS NOT NULL, 4, 
                                IF(G3.id  IS NOT NULL, 3, 
                                    IF(G2.id  IS NOT NULL, 2, 
                                        IF(G1.id  IS NOT NULL, 1, 7)))))) AS level, 
                    'G' AS ledger_group,
                    G1.id, G1.code, G1.name FROM {$this->_ci->DB1->dbprefix('groups')}{$this->_ci->DB1->dbsuffix} G1
                LEFT JOIN {$this->_ci->DB1->dbprefix('groups')}{$this->_ci->DB1->dbsuffix} G2 ON G2.id = G1.parent_id
                LEFT JOIN {$this->_ci->DB1->dbprefix('groups')}{$this->_ci->DB1->dbsuffix} G3 ON G3.id = G2.parent_id
                LEFT JOIN {$this->_ci->DB1->dbprefix('groups')}{$this->_ci->DB1->dbsuffix} G4 ON G4.id = G3.parent_id
                LEFT JOIN {$this->_ci->DB1->dbprefix('groups')}{$this->_ci->DB1->dbsuffix} G5 ON G5.id = G4.parent_id
                LEFT JOIN {$this->_ci->DB1->dbprefix('groups')}{$this->_ci->DB1->dbsuffix} G6 ON G6.id = G5.parent_id

                UNION

                SELECT L1.group_id AS parent_id, 2, 'L', L1.id, L1.code, L1.name FROM {$this->_ci->DB1->dbprefix('groups')}{$this->_ci->DB1->dbsuffix} G1
                    INNER JOIN {$this->_ci->DB1->dbprefix('ledgers')}{$this->_ci->DB1->dbsuffix} L1 ON L1.group_id = G1.id AND G1.parent_id = 0
                    
                UNION
                    
                SELECT L2.group_id AS parent_id, 3, 'L', L2.id, L2.code, L2.name FROM {$this->_ci->DB1->dbprefix('groups')}{$this->_ci->DB1->dbsuffix} G1
                    INNER JOIN {$this->_ci->DB1->dbprefix('groups')}{$this->_ci->DB1->dbsuffix} G2 ON G2.parent_id = G1.id AND G1.parent_id = 0
                    INNER JOIN {$this->_ci->DB1->dbprefix('ledgers')}{$this->_ci->DB1->dbsuffix} L2 ON L2.group_id = G2.id

                UNION

                SELECT L3.group_id AS parent_id, 4, 'L', L3.id, L3.code, L3.name FROM {$this->_ci->DB1->dbprefix('groups')}{$this->_ci->DB1->dbsuffix} G1
                    INNER JOIN {$this->_ci->DB1->dbprefix('groups')}{$this->_ci->DB1->dbsuffix} G2 ON G2.parent_id = G1.id AND G1.parent_id = 0
                    INNER JOIN {$this->_ci->DB1->dbprefix('groups')}{$this->_ci->DB1->dbsuffix} G3 ON G3.parent_id = G2.id
                    INNER JOIN {$this->_ci->DB1->dbprefix('ledgers')}{$this->_ci->DB1->dbsuffix} L3 ON L3.group_id = G3.id

                UNION
                    
                SELECT L4.group_id AS parent_id, 5, 'L', L4.id, L4.code, L4.name FROM {$this->_ci->DB1->dbprefix('groups')}{$this->_ci->DB1->dbsuffix} G1
                    INNER JOIN {$this->_ci->DB1->dbprefix('groups')}{$this->_ci->DB1->dbsuffix} G2 ON G2.parent_id = G1.id AND G1.parent_id = 0
                    INNER JOIN {$this->_ci->DB1->dbprefix('groups')}{$this->_ci->DB1->dbsuffix} G3 ON G3.parent_id = G2.id
                    INNER JOIN {$this->_ci->DB1->dbprefix('groups')}{$this->_ci->DB1->dbsuffix} G4 ON G4.parent_id = G3.id
                    INNER JOIN {$this->_ci->DB1->dbprefix('ledgers')}{$this->_ci->DB1->dbsuffix} L4 ON L4.group_id = G4.id

                UNION
                    
                SELECT L5.group_id AS parent_id, 6, 'L', L5.id, L5.code, L5.name FROM {$this->_ci->DB1->dbprefix('groups')}{$this->_ci->DB1->dbsuffix} G1
                    INNER JOIN {$this->_ci->DB1->dbprefix('groups')}{$this->_ci->DB1->dbsuffix} G2 ON G2.parent_id = G1.id AND G1.parent_id = 0
                    INNER JOIN {$this->_ci->DB1->dbprefix('groups')}{$this->_ci->DB1->dbsuffix} G3 ON G3.parent_id = G2.id
                    INNER JOIN {$this->_ci->DB1->dbprefix('groups')}{$this->_ci->DB1->dbsuffix} G4 ON G4.parent_id = G3.id
                    INNER JOIN {$this->_ci->DB1->dbprefix('groups')}{$this->_ci->DB1->dbsuffix} G5 ON G5.parent_id = G4.id
                    INNER JOIN {$this->_ci->DB1->dbprefix('ledgers')}{$this->_ci->DB1->dbsuffix} L5 ON L5.group_id = G5.id

                UNION
                    
                SELECT L6.group_id AS parent_id, 7, 'L', L6.id, L6.code, L6.name FROM {$this->_ci->DB1->dbprefix('groups')}{$this->_ci->DB1->dbsuffix} G1
                    INNER JOIN {$this->_ci->DB1->dbprefix('groups')}{$this->_ci->DB1->dbsuffix} G2 ON G2.parent_id = G1.id AND G1.parent_id = 0
                    INNER JOIN {$this->_ci->DB1->dbprefix('groups')}{$this->_ci->DB1->dbsuffix} G3 ON G3.parent_id = G2.id
                    INNER JOIN {$this->_ci->DB1->dbprefix('groups')}{$this->_ci->DB1->dbsuffix} G4 ON G4.parent_id = G3.id
                    INNER JOIN {$this->_ci->DB1->dbprefix('groups')}{$this->_ci->DB1->dbsuffix} G5 ON G5.parent_id = G4.id
                    INNER JOIN {$this->_ci->DB1->dbprefix('groups')}{$this->_ci->DB1->dbsuffix} G6 ON G6.parent_id = G5.id
                    INNER JOIN {$this->_ci->DB1->dbprefix('ledgers')}{$this->_ci->DB1->dbsuffix} L6 ON L6.group_id = G6.id
            ) AS account_list
            LEFT JOIN 
            (
                SELECT 
                    `{$this->_ci->DB1->dbprefix('entryitems')}{$this->_ci->DB1->dbsuffix}`.`ledger_id`,
                    SUM(COALESCE(IF(`{$this->_ci->DB1->dbprefix('entryitems')}{$this->_ci->DB1->dbsuffix}`.`dc` = 'D', {$this->_ci->DB1->dbprefix('entryitems')}{$this->_ci->DB1->dbsuffix}.amount, 0),0)) AS dr_total,
                    SUM(COALESCE(IF(`{$this->_ci->DB1->dbprefix('entryitems')}{$this->_ci->DB1->dbsuffix}`.`dc` = 'C', {$this->_ci->DB1->dbprefix('entryitems')}{$this->_ci->DB1->dbsuffix}.amount, 0),0)) AS cr_total,
                    SUM(COALESCE({$this->_ci->DB1->dbprefix('entryitems')}{$this->_ci->DB1->dbsuffix}.amount * (IF(`{$this->_ci->DB1->dbprefix('entryitems')}{$this->_ci->DB1->dbsuffix}`.`dc` = 'C', -1, 1)), 0)) AS total
                FROM
                    `{$this->_ci->DB1->dbprefix('entryitems')}{$this->_ci->DB1->dbsuffix}`
                        INNER JOIN
                    `{$this->_ci->DB1->dbprefix('entries')}{$this->_ci->DB1->dbsuffix}` ON `{$this->_ci->DB1->dbprefix('entries')}{$this->_ci->DB1->dbsuffix}`.`id` = `{$this->_ci->DB1->dbprefix('entryitems')}{$this->_ci->DB1->dbsuffix}`.`entry_id`
                        {$sql_skip_disapproved}
                WHERE
                        `{$this->_ci->DB1->dbprefix('entries')}{$this->_ci->DB1->dbsuffix}`.`date` >= '{$sql_start_date}'
                        AND `{$this->_ci->DB1->dbprefix('entries')}{$this->_ci->DB1->dbsuffix}`.`date` <= '{$sql_end_date}'
                        {$sql_cost_center}
                        {$sql_third}
                GROUP BY `{$this->_ci->DB1->dbprefix('entryitems')}{$this->_ci->DB1->dbsuffix}`.`ledger_id`
            ) AS total_ledgers ON total_ledgers.ledger_id = account_list.id AND account_list.ledger_group = 'L'
            {$sql_op_entries}
            LEFT JOIN 
            (
                SELECT 
                    id_ledgers,
                    SUM(COALESCE(op_balance * (IF(op_balance_dc = 'C',-1,1)),0)) AS op_total
                FROM
                    {$this->_ci->DB1->dbprefix('companies_opbalance')}{$this->_ci->DB1->dbsuffix}
                    {$sql_cost_center_COP}
                    {$sql_third_COP}
                GROUP BY id_ledgers
            ) AS op_balance ON op_balance.id_ledgers = account_list.id AND account_list.ledger_group = 'L'
            ORDER BY code ASC;";

    $result = $this->_ci->DB1->query($sql);
    $this->result_account_list = $result->result_array();
    $this->account_list = $this->buildTree($this->result_account_list, '0_G', $id, $parent);
  }
  function buildTree(array $data, $parentId = '0_G', $id = NULL, $parent = NULL) {
      $branch = [];
      foreach ($data as $element) {
          if ($id) {
            if ($parent) {
              if ($element['level'] == '1' && $element['code'] != $id) {
                continue;
              }
            } else {
              $element_id = explode('_', $element['id'])[0];
              if ($element['level'] == '1' && $element_id != $id) {
                continue;
              }
            }
          }
          if ($element['parent_id'] == $parentId) {
              $children = $this->buildTree($data, $element['id']);
              if ($children) {
                  $children_groups = [];
                  $children_ledgers = [];
                  $group_op_total = 0;
                  $group_dr_total = 0;
                  $group_cr_total = 0;
                  $group_cl_total = 0;
                  foreach ($children as $children_row) {
                    $group_op_total += $children_row['op_total'];
                    $group_dr_total += $children_row['dr_total'];
                    $group_cr_total += $children_row['cr_total'];
                    $group_cl_total += $children_row['cl_total'];
                    if ($children_row['ledger_group'] == 'G') {
                      $children_groups[] = $children_row;
                    } else {
                      $children_ledgers[] = $children_row;
                    }
                  }
                  $element['children_groups'] = $children_groups;
                  $element['children_ledgers'] = $children_ledgers;
                  $element['op_total'] = $group_op_total;
                  $element['dr_total'] = $group_dr_total;
                  $element['cr_total'] = $group_cr_total;
                  $element['cl_total'] = $group_cl_total;
              } else {
                $this->op_total += $element['op_total'];
                $this->dr_total += $element['dr_total'];
                $this->cr_total += $element['cr_total'];
                $this->cl_total += $element['cl_total'];
              }
              $element['id'] = explode('_', $element['id'])[0];
              $element['parent_id'] = explode('_', $element['parent_id'])[0];
              $branch[] = $element;
          }
      }
      return $branch;
  }

}